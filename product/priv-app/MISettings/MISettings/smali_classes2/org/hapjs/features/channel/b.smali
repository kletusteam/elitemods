.class public abstract Lorg/hapjs/features/channel/b;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/hapjs/features/channel/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/hapjs/features/channel/b$d;,
        Lorg/hapjs/features/channel/b$b;,
        Lorg/hapjs/features/channel/b$a;,
        Lorg/hapjs/features/channel/b$c;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final b:Lorg/hapjs/features/channel/a/a;

.field private final c:Lorg/hapjs/features/channel/a/b;

.field private d:Landroid/os/HandlerThread;

.field private e:Landroid/os/Handler;

.field private f:I

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Landroid/os/Messenger;

.field private j:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Lorg/hapjs/features/channel/b/a;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lorg/hapjs/features/channel/b;->a:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method protected constructor <init>(Lorg/hapjs/features/channel/a/a;Lorg/hapjs/features/channel/a/b;Landroid/os/HandlerThread;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/hapjs/features/channel/b;->b:Lorg/hapjs/features/channel/a/a;

    iput-object p2, p0, Lorg/hapjs/features/channel/b;->c:Lorg/hapjs/features/channel/a/b;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lorg/hapjs/features/channel/b;->a(I)V

    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lorg/hapjs/features/channel/b;->j:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz p3, :cond_0

    iput-object p3, p0, Lorg/hapjs/features/channel/b;->d:Landroid/os/HandlerThread;

    goto :goto_0

    :cond_0
    sget-object p1, Lorg/hapjs/features/channel/b$c;->a:Landroid/os/HandlerThread;

    iput-object p1, p0, Lorg/hapjs/features/channel/b;->d:Landroid/os/HandlerThread;

    :goto_0
    new-instance p1, Lorg/hapjs/features/channel/a;

    iget-object p2, p0, Lorg/hapjs/features/channel/b;->d:Landroid/os/HandlerThread;

    invoke-virtual {p2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lorg/hapjs/features/channel/a;-><init>(Lorg/hapjs/features/channel/b;Landroid/os/Looper;)V

    iput-object p1, p0, Lorg/hapjs/features/channel/b;->e:Landroid/os/Handler;

    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 0

    invoke-static {p1}, Lorg/hapjs/features/channel/d;->a(Landroid/os/Bundle;)Lorg/hapjs/features/channel/d;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/hapjs/features/channel/b;->b(Lorg/hapjs/features/channel/d;)V

    return-void
.end method

.method static synthetic a(Lorg/hapjs/features/channel/b;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/hapjs/features/channel/b;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Lorg/hapjs/features/channel/b;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/hapjs/features/channel/b;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lorg/hapjs/features/channel/b;Lorg/hapjs/features/channel/d;)Z
    .locals 0

    invoke-direct {p0, p1}, Lorg/hapjs/features/channel/b;->a(Lorg/hapjs/features/channel/d;)Z

    move-result p0

    return p0
.end method

.method private a(Lorg/hapjs/features/channel/d;)Z
    .locals 5

    iget v0, p0, Lorg/hapjs/features/channel/b;->f:I

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Fail to send message, invalid status:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lorg/hapjs/features/channel/b;->f:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v1, p1}, Lorg/hapjs/features/channel/b;->a(ILjava/lang/String;)V

    return v2

    :cond_0
    invoke-virtual {p1}, Lorg/hapjs/features/channel/d;->a()I

    move-result v0

    const/high16 v3, 0x80000

    const/4 v4, 0x5

    if-le v0, v3, :cond_1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Data size must less than 524288 but "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v4, p1}, Lorg/hapjs/features/channel/b;->a(ILjava/lang/String;)V

    return v2

    :cond_1
    iget-object v0, p1, Lorg/hapjs/features/channel/d;->c:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v2

    :goto_0
    const/16 v3, 0x40

    if-le v0, v3, :cond_3

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "File count must less than 64 but "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v4, p1}, Lorg/hapjs/features/channel/b;->a(ILjava/lang/String;)V

    return v2

    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1}, Lorg/hapjs/features/channel/d;->c()Landroid/os/Bundle;

    move-result-object p1

    const-string v2, "content"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object p1

    iput v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lorg/hapjs/features/channel/b;->d(Landroid/os/Message;)Z

    move-result p1

    return p1
.end method

.method private b(ILjava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lorg/hapjs/features/channel/b;->j:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/hapjs/features/channel/b/a;

    if-eqz v1, :cond_0

    invoke-interface {v1, p0, p1, p2}, Lorg/hapjs/features/channel/b/a;->b(Lorg/hapjs/features/channel/i;ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b(Lorg/hapjs/features/channel/d;)V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lorg/hapjs/features/channel/b;->j:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/hapjs/features/channel/b/a;

    if-eqz v1, :cond_0

    invoke-interface {v1, p0, p1}, Lorg/hapjs/features/channel/b/a;->a(Lorg/hapjs/features/channel/i;Lorg/hapjs/features/channel/d;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lorg/hapjs/features/channel/b;->b(ILjava/lang/String;Z)Z

    return-void
.end method

.method private d(Landroid/os/Message;)Z
    .locals 5

    const-string v0, "Remote app died."

    iget-object v1, p0, Lorg/hapjs/features/channel/b;->i:Landroid/os/Messenger;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lorg/hapjs/features/channel/b;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Message;->recycle()V

    :cond_0
    const/4 p1, 0x6

    const-string v0, "Fail to send message, messenger is null."

    invoke-virtual {p0, p1, v0}, Lorg/hapjs/features/channel/b;->a(ILjava/lang/String;)V

    return v2

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "idAtReceiver"

    invoke-virtual {p0}, Lorg/hapjs/features/channel/b;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lorg/hapjs/features/channel/b;->i:Landroid/os/Messenger;

    invoke-virtual {v1, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lorg/hapjs/features/channel/b;->f()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Landroid/os/Message;->recycle()V

    :cond_2
    return v0

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v3, 0x4

    :try_start_1
    invoke-virtual {p0, v3, v0}, Lorg/hapjs/features/channel/b;->a(ILjava/lang/String;)V

    const-string v3, "ChannelBase"

    invoke-static {v3, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lorg/hapjs/features/channel/b;->f()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Landroid/os/Message;->recycle()V

    :cond_3
    return v2

    :goto_0
    invoke-virtual {p0}, Lorg/hapjs/features/channel/b;->f()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p1}, Landroid/os/Message;->recycle()V

    :cond_4
    throw v0
.end method

.method protected static g()Ljava/lang/String;
    .locals 2

    sget-object v0, Lorg/hapjs/features/channel/b;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lorg/hapjs/features/channel/b;->j:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/hapjs/features/channel/b/a;

    if-eqz v1, :cond_0

    invoke-interface {v1, p0}, Lorg/hapjs/features/channel/b/a;->a(Lorg/hapjs/features/channel/i;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a()Lorg/hapjs/features/channel/a/b;
    .locals 1

    iget-object v0, p0, Lorg/hapjs/features/channel/b;->c:Lorg/hapjs/features/channel/a/b;

    return-object v0
.end method

.method protected a(I)V
    .locals 3

    iget v0, p0, Lorg/hapjs/features/channel/b;->f:I

    iput p1, p0, Lorg/hapjs/features/channel/b;->f:I

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    if-ne p1, v1, :cond_0

    invoke-direct {p0}, Lorg/hapjs/features/channel/b;->h()V

    :cond_0
    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    iget p1, p0, Lorg/hapjs/features/channel/b;->g:I

    iget-object v0, p0, Lorg/hapjs/features/channel/b;->h:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lorg/hapjs/features/channel/b;->b(ILjava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected a(ILjava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lorg/hapjs/features/channel/b;->j:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/hapjs/features/channel/b/a;

    if-eqz v1, :cond_0

    invoke-interface {v1, p0, p1, p2}, Lorg/hapjs/features/channel/b/a;->a(Lorg/hapjs/features/channel/i;ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(ILjava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/hapjs/features/channel/b;->a(ILjava/lang/String;ZLorg/hapjs/features/channel/b/b;)V

    return-void
.end method

.method public a(ILjava/lang/String;ZLorg/hapjs/features/channel/b/b;)V
    .locals 2

    new-instance v0, Lorg/hapjs/features/channel/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/hapjs/features/channel/b$a;-><init>(Lorg/hapjs/features/channel/a;)V

    iput p1, v0, Lorg/hapjs/features/channel/b$a;->a:I

    iput-object p2, v0, Lorg/hapjs/features/channel/b$a;->c:Ljava/lang/String;

    iput-boolean p3, v0, Lorg/hapjs/features/channel/b$a;->b:Z

    iput-object p4, v0, Lorg/hapjs/features/channel/b$a;->d:Lorg/hapjs/features/channel/b/b;

    iget-object p1, p0, Lorg/hapjs/features/channel/b;->e:Landroid/os/Handler;

    const/4 p2, 0x2

    invoke-virtual {p1, p2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method protected abstract a(Landroid/os/Message;)V
.end method

.method protected a(Landroid/os/Messenger;)V
    .locals 0

    iput-object p1, p0, Lorg/hapjs/features/channel/b;->i:Landroid/os/Messenger;

    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/hapjs/features/channel/b;->k:Ljava/lang/String;

    return-void
.end method

.method public a(Lorg/hapjs/features/channel/d;Lorg/hapjs/features/channel/b/b;)V
    .locals 2

    new-instance v0, Lorg/hapjs/features/channel/b$d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/hapjs/features/channel/b$d;-><init>(Lorg/hapjs/features/channel/a;)V

    iput-object p1, v0, Lorg/hapjs/features/channel/b$d;->a:Lorg/hapjs/features/channel/d;

    iput-object p2, v0, Lorg/hapjs/features/channel/b$d;->b:Lorg/hapjs/features/channel/b/b;

    iget-object p1, p0, Lorg/hapjs/features/channel/b;->e:Landroid/os/Handler;

    const/4 p2, 0x1

    invoke-virtual {p1, p2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method protected a(Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lorg/hapjs/features/channel/b;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    const/4 p1, 0x1

    return p1
.end method

.method public a(Lorg/hapjs/features/channel/b/a;)Z
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/hapjs/features/channel/b;->j:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, ""

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/hapjs/features/channel/b;->k:Ljava/lang/String;

    return-object v0
.end method

.method protected b(Landroid/os/Message;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "reason"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lorg/hapjs/features/channel/b;->e:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/hapjs/features/channel/b;->l:Ljava/lang/String;

    return-void
.end method

.method protected b(ILjava/lang/String;Z)Z
    .locals 3

    iget v0, p0, Lorg/hapjs/features/channel/b;->f:I

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    if-eq v0, v1, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Fail to close channel, invalid status "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lorg/hapjs/features/channel/b;->f:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v2, p1}, Lorg/hapjs/features/channel/b;->a(ILjava/lang/String;)V

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 v0, 0x3

    if-eqz p3, :cond_1

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    const-string v2, "reason"

    invoke-virtual {p3, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    iput v0, v2, Landroid/os/Message;->what:I

    invoke-virtual {v2, p3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-direct {p0, v2}, Lorg/hapjs/features/channel/b;->d(Landroid/os/Message;)Z

    :cond_1
    const/4 p3, 0x0

    iput-object p3, p0, Lorg/hapjs/features/channel/b;->i:Landroid/os/Messenger;

    iput p1, p0, Lorg/hapjs/features/channel/b;->g:I

    iput-object p2, p0, Lorg/hapjs/features/channel/b;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lorg/hapjs/features/channel/b;->a(I)V

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Channel closed, code:"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", reason:"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "ChannelBase"

    invoke-static {p2, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method public b(Lorg/hapjs/features/channel/b/a;)Z
    .locals 1

    iget-object v0, p0, Lorg/hapjs/features/channel/b;->j:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected abstract c()Ljava/lang/String;
.end method

.method protected c(Landroid/os/Message;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "content"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    iget-object v0, p0, Lorg/hapjs/features/channel/b;->e:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/hapjs/features/channel/b;->l:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lorg/hapjs/features/channel/b;->f:I

    return v0
.end method

.method protected abstract f()Z
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Channel[type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/hapjs/features/channel/b;->b:Lorg/hapjs/features/channel/a/a;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lorg/hapjs/features/channel/a/a;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", androidPkgName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/hapjs/features/channel/b;->b:Lorg/hapjs/features/channel/a/a;

    iget-object v2, v2, Lorg/hapjs/features/channel/a/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, p0, Lorg/hapjs/features/channel/b;->c:Lorg/hapjs/features/channel/a/b;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lorg/hapjs/features/channel/a/b;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", hapPkgName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/hapjs/features/channel/b;->c:Lorg/hapjs/features/channel/a/b;

    iget-object v2, v2, Lorg/hapjs/features/channel/a/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", serverId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/hapjs/features/channel/b;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", clientId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/hapjs/features/channel/b;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
