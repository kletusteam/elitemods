.class Lorg/hapjs/features/channel/l;
.super Lorg/hapjs/features/channel/b;

# interfaces
.implements Lorg/hapjs/features/channel/j;


# instance fields
.field private m:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/hapjs/features/channel/a/a;Lorg/hapjs/features/channel/a/b;Landroid/os/HandlerThread;Z)V
    .locals 0

    invoke-direct {p0, p2, p3, p4}, Lorg/hapjs/features/channel/b;-><init>(Lorg/hapjs/features/channel/a/a;Lorg/hapjs/features/channel/a/b;Landroid/os/HandlerThread;)V

    iput-boolean p5, p0, Lorg/hapjs/features/channel/l;->m:Z

    invoke-virtual {p0, p1}, Lorg/hapjs/features/channel/b;->a(Ljava/lang/String;)V

    invoke-static {}, Lorg/hapjs/features/channel/b;->g()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/hapjs/features/channel/b;->b(Ljava/lang/String;)V

    new-instance p1, Lorg/hapjs/features/channel/k;

    invoke-direct {p1, p0}, Lorg/hapjs/features/channel/k;-><init>(Lorg/hapjs/features/channel/l;)V

    invoke-virtual {p0, p1}, Lorg/hapjs/features/channel/b;->a(Lorg/hapjs/features/channel/b/a;)Z

    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Message;)V
    .locals 3

    invoke-virtual {p0}, Lorg/hapjs/features/channel/b;->e()I

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fail to open channel, invalid status:"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v1, p1}, Lorg/hapjs/features/channel/b;->a(ILjava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/hapjs/features/channel/b;->a(I)V

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Landroid/os/Messenger;

    invoke-virtual {p0, p1}, Lorg/hapjs/features/channel/b;->a(Landroid/os/Messenger;)V

    invoke-virtual {p0, v1}, Lorg/hapjs/features/channel/b;->a(I)V

    return-void
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/hapjs/features/channel/b;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method d(Landroid/os/Message;)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lorg/hapjs/features/channel/b;->a(Ljava/lang/Object;)Z

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object p1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    goto/32 :goto_0

    nop
.end method

.method protected f()Z
    .locals 1

    iget-boolean v0, p0, Lorg/hapjs/features/channel/l;->m:Z

    return v0
.end method

.method h()V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Lorg/hapjs/features/channel/b;->e()I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_1
    const-string v2, "Remote app died."

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {p0, v0, v2, v1}, Lorg/hapjs/features/channel/b;->a(ILjava/lang/String;Z)V

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    const/4 v0, 0x3

    goto/32 :goto_9

    nop

    :goto_6
    if-eq v0, v1, :cond_0

    goto/32 :goto_4

    :cond_0
    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    const/4 v1, 0x2

    goto/32 :goto_6

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_1

    nop

    :goto_a
    if-ne v0, v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_8

    nop

    :goto_b
    const/4 v1, 0x1

    goto/32 :goto_a

    nop
.end method
