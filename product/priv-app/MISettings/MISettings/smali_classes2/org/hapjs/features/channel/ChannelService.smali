.class public Lorg/hapjs/features/channel/ChannelService;
.super Landroid/app/Service;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/hapjs/features/channel/ChannelService$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/hapjs/features/channel/l;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/os/HandlerThread;

.field private c:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/hapjs/features/channel/ChannelService;->a:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Lorg/hapjs/features/channel/ChannelService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lorg/hapjs/features/channel/ChannelService;->c:Landroid/os/Handler;

    return-object p0
.end method

.method public static a(Lorg/hapjs/features/channel/ChannelService$a;Ljava/lang/String;)Landroid/os/Message;
    .locals 4

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    iget-boolean v2, p0, Lorg/hapjs/features/channel/ChannelService$a;->a:Z

    const-string v3, "result"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    iget-object p0, p0, Lorg/hapjs/features/channel/ChannelService$a;->b:Ljava/lang/String;

    const-string v2, "message"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p0

    const-string v1, "idAtServer"

    invoke-virtual {p0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Lorg/hapjs/features/channel/a/b;)Lorg/hapjs/features/channel/ChannelService$a;
    .locals 4

    new-instance v0, Lorg/hapjs/features/channel/ChannelService$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/hapjs/features/channel/ChannelService$a;-><init>(Lorg/hapjs/features/channel/e;)V

    invoke-static {}, Lorg/hapjs/features/channel/h;->a()Lorg/hapjs/features/channel/h;

    move-result-object v1

    iget-boolean v1, v1, Lorg/hapjs/features/channel/h;->c:Z

    const/4 v2, 0x0

    if-nez v1, :cond_0

    iput-boolean v2, v0, Lorg/hapjs/features/channel/ChannelService$a;->a:Z

    const-string p1, "Native app is not ready."

    iput-object p1, v0, Lorg/hapjs/features/channel/ChannelService$a;->b:Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-static {}, Lorg/hapjs/features/channel/h;->a()Lorg/hapjs/features/channel/h;

    move-result-object v1

    iget-object v1, v1, Lorg/hapjs/features/channel/h;->a:Lorg/hapjs/features/channel/h$a;

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    invoke-static {}, Lorg/hapjs/features/channel/h;->a()Lorg/hapjs/features/channel/h;

    move-result-object v1

    iget-object v1, v1, Lorg/hapjs/features/channel/h;->a:Lorg/hapjs/features/channel/h$a;

    invoke-interface {v1, p1}, Lorg/hapjs/features/channel/h$a;->a(Lorg/hapjs/features/channel/a/b;)Z

    move-result p1

    if-eqz p1, :cond_1

    iput-boolean v3, v0, Lorg/hapjs/features/channel/ChannelService$a;->a:Z

    const-string p1, "ok"

    iput-object p1, v0, Lorg/hapjs/features/channel/ChannelService$a;->b:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-boolean v2, v0, Lorg/hapjs/features/channel/ChannelService$a;->a:Z

    const-string p1, "Open request refused."

    iput-object p1, v0, Lorg/hapjs/features/channel/ChannelService$a;->b:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iput-boolean v3, v0, Lorg/hapjs/features/channel/ChannelService$a;->a:Z

    const-string p1, "App checker ignored."

    iput-object p1, v0, Lorg/hapjs/features/channel/ChannelService$a;->b:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/os/Message;)V
    .locals 10

    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "idAtClient"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pkgName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "signature"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v8, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "clientPid"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v9, "ChannelService"

    if-nez v8, :cond_0

    const-string p1, "Fail to handle open channel message, reply to is null."

    invoke-static {v9, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v5, Lorg/hapjs/features/channel/a/b;

    invoke-direct {v5, v0, v1}, Lorg/hapjs/features/channel/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lorg/hapjs/features/channel/a/a;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v6, v1, [Ljava/lang/String;

    invoke-direct {v4, p1, v0, v6}, Lorg/hapjs/features/channel/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lorg/hapjs/features/channel/ChannelService;->a(Lorg/hapjs/features/channel/a/b;)Lorg/hapjs/features/channel/ChannelService$a;

    move-result-object p1

    :try_start_0
    iget-boolean v0, p1, Lorg/hapjs/features/channel/ChannelService$a;->a:Z

    if-eqz v0, :cond_2

    new-instance v0, Lorg/hapjs/features/channel/l;

    iget-object v6, p0, Lorg/hapjs/features/channel/ChannelService;->b:Landroid/os/HandlerThread;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v7

    if-ne v2, v7, :cond_1

    const/4 v2, 0x1

    move v7, v2

    goto :goto_0

    :cond_1
    move v7, v1

    :goto_0
    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lorg/hapjs/features/channel/l;-><init>(Ljava/lang/String;Lorg/hapjs/features/channel/a/a;Lorg/hapjs/features/channel/a/b;Landroid/os/HandlerThread;Z)V

    invoke-virtual {v0}, Lorg/hapjs/features/channel/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lorg/hapjs/features/channel/ChannelService;->a(Lorg/hapjs/features/channel/ChannelService$a;Ljava/lang/String;)Landroid/os/Message;

    move-result-object p1

    new-instance v2, Lorg/hapjs/features/channel/f;

    invoke-direct {v2, p0, v0}, Lorg/hapjs/features/channel/f;-><init>(Lorg/hapjs/features/channel/ChannelService;Lorg/hapjs/features/channel/l;)V

    invoke-virtual {v8}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-interface {v3, v2, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    new-instance v1, Lorg/hapjs/features/channel/g;

    invoke-direct {v1, p0, v0, v8, v2}, Lorg/hapjs/features/channel/g;-><init>(Lorg/hapjs/features/channel/ChannelService;Lorg/hapjs/features/channel/l;Landroid/os/Messenger;Landroid/os/IBinder$DeathRecipient;)V

    invoke-virtual {v0, v1}, Lorg/hapjs/features/channel/b;->a(Lorg/hapjs/features/channel/b/a;)Z

    iget-object v1, p0, Lorg/hapjs/features/channel/ChannelService;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lorg/hapjs/features/channel/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, p2}, Lorg/hapjs/features/channel/l;->d(Landroid/os/Message;)V

    goto :goto_1

    :cond_2
    const-string p2, "-1"

    invoke-static {p1, p2}, Lorg/hapjs/features/channel/ChannelService;->a(Lorg/hapjs/features/channel/ChannelService$a;Ljava/lang/String;)Landroid/os/Message;

    move-result-object p1

    :goto_1
    invoke-virtual {v8, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    const-string p2, "Fail to ack open."

    invoke-static {v9, p2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2
    return-void
.end method

.method private a(Landroid/os/Message;)V
    .locals 3

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "idAtReceiver"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/hapjs/features/channel/ChannelService;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/hapjs/features/channel/l;

    const-string v2, "ChannelService"

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lorg/hapjs/features/channel/b;->c(Landroid/os/Message;)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " receive msg from hap app."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Fail to handle receive message, channel "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " not found"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method static synthetic a(Lorg/hapjs/features/channel/ChannelService;Landroid/content/Context;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/hapjs/features/channel/ChannelService;->a(Landroid/content/Context;Landroid/os/Message;)V

    return-void
.end method

.method static synthetic a(Lorg/hapjs/features/channel/ChannelService;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/hapjs/features/channel/ChannelService;->a(Landroid/os/Message;)V

    return-void
.end method

.method private b(Landroid/os/Message;)V
    .locals 3

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "idAtReceiver"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/hapjs/features/channel/ChannelService;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/hapjs/features/channel/l;

    const-string v2, "ChannelService"

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lorg/hapjs/features/channel/b;->b(Landroid/os/Message;)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " closed by hap app."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Fail to handle close, channel "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " not found"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method static synthetic b(Lorg/hapjs/features/channel/ChannelService;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/hapjs/features/channel/ChannelService;->b(Landroid/os/Message;)V

    return-void
.end method

.method private c(Landroid/os/Message;)V
    .locals 3

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lorg/hapjs/features/channel/ChannelService;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/hapjs/features/channel/l;

    const-string v1, "ChannelService"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/hapjs/features/channel/l;->h()V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "\'s hap app died."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fail to remote app death, channel "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " not found"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method static synthetic c(Lorg/hapjs/features/channel/ChannelService;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/hapjs/features/channel/ChannelService;->c(Landroid/os/Message;)V

    return-void
.end method

.method private d(Landroid/os/Message;)V
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown msg type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    const-string v2, "ChannelService"

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    const/4 v3, -0x1

    iput v3, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "desc"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object p1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {p1, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v1, "Fail to handle unknown msg type."

    invoke-static {v2, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic d(Lorg/hapjs/features/channel/ChannelService;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/hapjs/features/channel/ChannelService;->d(Landroid/os/Message;)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    new-instance p1, Landroid/os/Messenger;

    iget-object v0, p0, Lorg/hapjs/features/channel/ChannelService;->c:Landroid/os/Handler;

    invoke-direct {p1, v0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-virtual {p1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object p1

    return-object p1
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {}, Lorg/hapjs/features/channel/transparentactivity/b;->a()Lorg/hapjs/features/channel/transparentactivity/b;

    move-result-object v0

    invoke-virtual {v0}, Lorg/hapjs/features/channel/transparentactivity/b;->c()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ChannelService"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/hapjs/features/channel/ChannelService;->b:Landroid/os/HandlerThread;

    iget-object v0, p0, Lorg/hapjs/features/channel/ChannelService;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, -0x1

    aput v2, v0, v1

    new-instance v1, Lorg/hapjs/features/channel/e;

    iget-object v2, p0, Lorg/hapjs/features/channel/ChannelService;->b:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, p0, v2, v0}, Lorg/hapjs/features/channel/e;-><init>(Lorg/hapjs/features/channel/ChannelService;Landroid/content/Context;Landroid/os/Looper;[I)V

    iput-object v1, p0, Lorg/hapjs/features/channel/ChannelService;->c:Landroid/os/Handler;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lorg/hapjs/features/channel/ChannelService;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lorg/hapjs/features/channel/ChannelService;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iput-object v1, p0, Lorg/hapjs/features/channel/ChannelService;->b:Landroid/os/HandlerThread;

    invoke-static {}, Lorg/hapjs/features/channel/transparentactivity/b;->a()Lorg/hapjs/features/channel/transparentactivity/b;

    move-result-object v0

    invoke-virtual {v0}, Lorg/hapjs/features/channel/transparentactivity/b;->d()V

    return-void
.end method
