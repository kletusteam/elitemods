.class public final Le/H;
.super Ljava/lang/Object;

# interfaces
.implements Le/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/H$a;,
        Le/H$b;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0000\u0018\u0000 \'2\u00020\u0001:\u0002&\'B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016J\u0008\u0010\u0017\u001a\u00020\u0000H\u0016J\u0010\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0008\u0010\u001b\u001a\u00020\u001cH\u0016J\u0006\u0010\u001d\u001a\u00020\u001cJ\u0008\u0010\u001e\u001a\u00020\u0007H\u0016J\u0008\u0010\u001f\u001a\u00020\u0007H\u0016J\u0006\u0010 \u001a\u00020!J\u0008\u0010\"\u001a\u00020\u0005H\u0016J\u0008\u0010#\u001a\u00020$H\u0016J\u0006\u0010%\u001a\u00020!R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lokhttp3/RealCall;",
        "Lokhttp3/Call;",
        "client",
        "Lokhttp3/OkHttpClient;",
        "originalRequest",
        "Lokhttp3/Request;",
        "forWebSocket",
        "",
        "(Lokhttp3/OkHttpClient;Lokhttp3/Request;Z)V",
        "getClient",
        "()Lokhttp3/OkHttpClient;",
        "executed",
        "getExecuted",
        "()Z",
        "setExecuted",
        "(Z)V",
        "getForWebSocket",
        "getOriginalRequest",
        "()Lokhttp3/Request;",
        "transmitter",
        "Lokhttp3/internal/connection/Transmitter;",
        "cancel",
        "",
        "clone",
        "enqueue",
        "responseCallback",
        "Lokhttp3/Callback;",
        "execute",
        "Lokhttp3/Response;",
        "getResponseWithInterceptorChain",
        "isCanceled",
        "isExecuted",
        "redactedUrl",
        "",
        "request",
        "timeout",
        "Lokio/Timeout;",
        "toLoggableString",
        "AsyncCall",
        "Companion",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final a:Le/H$b;


# instance fields
.field private b:Le/a/b/n;

.field private c:Z

.field private final d:Le/F;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Le/I;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Le/H$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/H$b;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/H;->a:Le/H$b;

    return-void
.end method

.method private constructor <init>(Le/F;Le/I;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/H;->d:Le/F;

    iput-object p2, p0, Le/H;->e:Le/I;

    iput-boolean p3, p0, Le/H;->f:Z

    return-void
.end method

.method public synthetic constructor <init>(Le/F;Le/I;ZLkotlin/jvm/b/g;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Le/H;-><init>(Le/F;Le/I;Z)V

    return-void
.end method

.method public static final synthetic a(Le/H;)Le/a/b/n;
    .locals 0

    iget-object p0, p0, Le/H;->b:Le/a/b/n;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "transmitter"

    invoke-static {p0}, Lkotlin/jvm/b/i;->b(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic a(Le/H;Le/a/b/n;)V
    .locals 0

    iput-object p1, p0, Le/H;->b:Le/a/b/n;

    return-void
.end method


# virtual methods
.method public final a()Le/F;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/H;->d:Le/F;

    return-object v0
.end method

.method public a(Le/i;)V
    .locals 2
    .param p1    # Le/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "responseCallback"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Le/H;->c:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Le/H;->c:Z

    sget-object v0, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    iget-object v0, p0, Le/H;->b:Le/a/b/n;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Le/a/b/n;->a()V

    iget-object v0, p0, Le/H;->d:Le/F;

    invoke-virtual {v0}, Le/F;->k()Le/t;

    move-result-object v0

    new-instance v1, Le/H$a;

    invoke-direct {v1, p0, p1}, Le/H$a;-><init>(Le/H;Le/i;)V

    invoke-virtual {v0, v1}, Le/t;->a(Le/H$a;)V

    return-void

    :cond_0
    const-string p1, "transmitter"

    invoke-static {p1}, Lkotlin/jvm/b/i;->b(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1

    :cond_1
    :try_start_1
    const-string p1, "Already Executed"

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Le/H;->f:Z

    return v0
.end method

.method public final c()Le/I;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/H;->e:Le/I;

    return-object v0
.end method

.method public clone()Le/H;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    sget-object v0, Le/H;->a:Le/H$b;

    iget-object v1, p0, Le/H;->d:Le/F;

    iget-object v2, p0, Le/H;->e:Le/I;

    iget-boolean v3, p0, Le/H;->f:Z

    invoke-virtual {v0, v1, v2, v3}, Le/H$b;->a(Le/F;Le/I;Z)Le/H;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Le/H;->clone()Le/H;

    move-result-object v0

    return-object v0
.end method

.method public final d()Le/L;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Le/H;->d:Le/F;

    invoke-virtual {v0}, Le/F;->q()Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/a/h;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    new-instance v0, Le/a/c/k;

    iget-object v2, p0, Le/H;->d:Le/F;

    invoke-direct {v0, v2}, Le/a/c/k;-><init>(Le/F;)V

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    new-instance v0, Le/a/c/a;

    iget-object v2, p0, Le/H;->d:Le/F;

    invoke-virtual {v2}, Le/F;->j()Le/s;

    move-result-object v2

    invoke-direct {v0, v2}, Le/a/c/a;-><init>(Le/s;)V

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    new-instance v0, Le/a/a/a;

    iget-object v2, p0, Le/H;->d:Le/F;

    invoke-virtual {v2}, Le/F;->d()Le/e;

    move-result-object v2

    invoke-direct {v0, v2}, Le/a/a/a;-><init>(Le/e;)V

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    sget-object v0, Le/a/b/a;->a:Le/a/b/a;

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Le/H;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Le/H;->d:Le/F;

    invoke-virtual {v0}, Le/F;->r()Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/a/h;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    :cond_0
    new-instance v0, Le/a/c/b;

    iget-boolean v2, p0, Le/H;->f:Z

    invoke-direct {v0, v2}, Le/a/c/b;-><init>(Z)V

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    new-instance v10, Le/a/c/h;

    iget-object v2, p0, Le/H;->b:Le/a/b/n;

    const-string v11, "transmitter"

    const/4 v12, 0x0

    if-eqz v2, :cond_8

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Le/H;->e:Le/I;

    iget-object v0, p0, Le/H;->d:Le/F;

    invoke-virtual {v0}, Le/F;->g()I

    move-result v7

    iget-object v0, p0, Le/H;->d:Le/F;

    invoke-virtual {v0}, Le/F;->x()I

    move-result v8

    iget-object v0, p0, Le/H;->d:Le/F;

    invoke-virtual {v0}, Le/F;->B()I

    move-result v9

    move-object v0, v10

    move-object v6, p0

    invoke-direct/range {v0 .. v9}, Le/a/c/h;-><init>(Ljava/util/List;Le/a/b/n;Le/a/b/c;ILe/I;Le/h;III)V

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Le/H;->e:Le/I;

    invoke-virtual {v10, v1}, Le/a/c/h;->a(Le/I;)Le/L;

    move-result-object v1

    iget-object v2, p0, Le/H;->b:Le/a/b/n;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Le/a/b/n;->g()Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_2

    iget-object v0, p0, Le/H;->b:Le/a/b/n;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v12}, Le/a/b/n;->a(Ljava/io/IOException;)Ljava/io/IOException;

    return-object v1

    :cond_1
    invoke-static {v11}, Lkotlin/jvm/b/i;->b(Ljava/lang/String;)V

    throw v12

    :cond_2
    :try_start_1
    invoke-static {v1}, Le/a/d;->a(Ljava/io/Closeable;)V

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Canceled"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-static {v11}, Lkotlin/jvm/b/i;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v12

    :catchall_0
    move-exception v1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x1

    :try_start_2
    iget-object v2, p0, Le/H;->b:Le/a/b/n;

    if-eqz v2, :cond_5

    invoke-virtual {v2, v0}, Le/a/b/n;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Lkotlin/o;

    const-string v2, "null cannot be cast to non-null type kotlin.Throwable"

    invoke-direct {v0, v2}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    throw v0

    :cond_5
    invoke-static {v11}, Lkotlin/jvm/b/i;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v12

    :catchall_1
    move-exception v0

    move v13, v1

    move-object v1, v0

    move v0, v13

    :goto_0
    if-nez v0, :cond_7

    iget-object v0, p0, Le/H;->b:Le/a/b/n;

    if-nez v0, :cond_6

    invoke-static {v11}, Lkotlin/jvm/b/i;->b(Ljava/lang/String;)V

    throw v12

    :cond_6
    invoke-virtual {v0, v12}, Le/a/b/n;->a(Ljava/io/IOException;)Ljava/io/IOException;

    :cond_7
    throw v1

    :cond_8
    invoke-static {v11}, Lkotlin/jvm/b/i;->b(Ljava/lang/String;)V

    throw v12
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Le/H;->b:Le/a/b/n;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Le/a/b/n;->g()Z

    move-result v0

    return v0

    :cond_0
    const-string v0, "transmitter"

    invoke-static {v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/H;->e:Le/I;

    invoke-virtual {v0}, Le/I;->h()Le/C;

    move-result-object v0

    invoke-virtual {v0}, Le/C;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Le/H;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "canceled "

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Le/H;->f:Z

    if-eqz v1, :cond_1

    const-string v1, "web socket"

    goto :goto_1

    :cond_1
    const-string v1, "call"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Le/H;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
