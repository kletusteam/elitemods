.class final Le/e$d;
.super Ljava/lang/Object;

# interfaces
.implements Le/a/a/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field private final a:Lf/A;

.field private final b:Lf/A;

.field private c:Z

.field private final d:Le/a/a/e$b;

.field final synthetic e:Le/e;


# direct methods
.method public constructor <init>(Le/e;Le/a/a/e$b;)V
    .locals 1
    .param p1    # Le/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le/a/a/e$b;",
            ")V"
        }
    .end annotation

    const-string v0, "editor"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Le/e$d;->e:Le/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Le/e$d;->d:Le/a/a/e$b;

    iget-object p1, p0, Le/e$d;->d:Le/a/a/e$b;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Le/a/a/e$b;->a(I)Lf/A;

    move-result-object p1

    iput-object p1, p0, Le/e$d;->a:Lf/A;

    new-instance p1, Le/f;

    iget-object p2, p0, Le/e$d;->a:Lf/A;

    invoke-direct {p1, p0, p2}, Le/f;-><init>(Le/e$d;Lf/A;)V

    iput-object p1, p0, Le/e$d;->b:Lf/A;

    return-void
.end method

.method public static final synthetic a(Le/e$d;)Le/a/a/e$b;
    .locals 0

    iget-object p0, p0, Le/e$d;->d:Le/a/a/e$b;

    return-object p0
.end method


# virtual methods
.method public a()Lf/A;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/e$d;->b:Lf/A;

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Le/e$d;->c:Z

    return-void
.end method

.method public abort()V
    .locals 4

    iget-object v0, p0, Le/e$d;->e:Le/e;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Le/e$d;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Le/e$d;->c:Z

    iget-object v2, p0, Le/e$d;->e:Le/e;

    invoke-virtual {v2}, Le/e;->a()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v2, v3}, Le/e;->b(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    iget-object v0, p0, Le/e$d;->a:Lf/A;

    invoke-static {v0}, Le/a/d;->a(Ljava/io/Closeable;)V

    :try_start_2
    iget-object v0, p0, Le/e$d;->d:Le/a/a/e$b;

    invoke-virtual {v0}, Le/a/a/e$b;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Le/e$d;->c:Z

    return v0
.end method
