.class final Le/e$a;
.super Le/N;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final b:Lf/k;

.field private final c:Le/a/a/e$d;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Le/a/a/e$d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Le/a/a/e$d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Le/N;-><init>()V

    iput-object p1, p0, Le/e$a;->c:Le/a/a/e$d;

    iput-object p2, p0, Le/e$a;->d:Ljava/lang/String;

    iput-object p3, p0, Le/e$a;->e:Ljava/lang/String;

    iget-object p1, p0, Le/e$a;->c:Le/a/a/e$d;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Le/a/a/e$d;->b(I)Lf/C;

    move-result-object p1

    new-instance p2, Le/d;

    invoke-direct {p2, p0, p1, p1}, Le/d;-><init>(Le/e$a;Lf/C;Lf/C;)V

    invoke-static {p2}, Lf/s;->a(Lf/C;)Lf/k;

    move-result-object p1

    iput-object p1, p0, Le/e$a;->b:Lf/k;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 3

    iget-object v0, p0, Le/e$a;->e:Ljava/lang/String;

    const-wide/16 v1, -0x1

    if-eqz v0, :cond_0

    invoke-static {v0, v1, v2}, Le/a/d;->a(Ljava/lang/String;J)J

    move-result-wide v1

    :cond_0
    return-wide v1
.end method

.method public b()Le/E;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/e$a;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v1, Le/E;->c:Le/E$a;

    invoke-virtual {v1, v0}, Le/E$a;->b(Ljava/lang/String;)Le/E;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public p()Lf/k;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/e$a;->b:Lf/k;

    return-object v0
.end method

.method public final r()Le/a/a/e$d;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/e$a;->c:Le/a/a/e$d;

    return-object v0
.end method
