.class public Le/F;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;
.implements Le/h$a;
.implements Le/Q;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/F$a;,
        Le/F$b;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00d8\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0013\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\u0008\u0016\u0018\u0000 p2\u00020\u00012\u00020\u00022\u00020\u0003:\u0002opB\u0007\u0008\u0016\u00a2\u0006\u0002\u0010\u0004B\u000f\u0008\u0000\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\r\u0010\u0008\u001a\u00020\tH\u0007\u00a2\u0006\u0002\u0008LJ\u000f\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0007\u00a2\u0006\u0002\u0008MJ\r\u0010\u000e\u001a\u00020\u000fH\u0007\u00a2\u0006\u0002\u0008NJ\r\u0010\u0014\u001a\u00020\u0015H\u0007\u00a2\u0006\u0002\u0008OJ\r\u0010\u0017\u001a\u00020\u000fH\u0007\u00a2\u0006\u0002\u0008PJ\r\u0010\u0018\u001a\u00020\u0019H\u0007\u00a2\u0006\u0002\u0008QJ\u0013\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u001cH\u0007\u00a2\u0006\u0002\u0008RJ\r\u0010\u001f\u001a\u00020 H\u0007\u00a2\u0006\u0002\u0008SJ\r\u0010\"\u001a\u00020#H\u0007\u00a2\u0006\u0002\u0008TJ\r\u0010%\u001a\u00020&H\u0007\u00a2\u0006\u0002\u0008UJ\r\u0010(\u001a\u00020)H\u0007\u00a2\u0006\u0002\u0008VJ\r\u0010+\u001a\u00020,H\u0007\u00a2\u0006\u0002\u0008WJ\r\u0010.\u001a\u00020,H\u0007\u00a2\u0006\u0002\u0008XJ\r\u0010/\u001a\u000200H\u0007\u00a2\u0006\u0002\u0008YJ\u0013\u00102\u001a\u0008\u0012\u0004\u0012\u0002030\u001cH\u0007\u00a2\u0006\u0002\u0008ZJ\u0013\u00104\u001a\u0008\u0012\u0004\u0012\u0002030\u001cH\u0007\u00a2\u0006\u0002\u0008[J\u0008\u0010\\\u001a\u00020\u0006H\u0016J\u0010\u0010]\u001a\u00020^2\u0006\u0010_\u001a\u00020`H\u0016J\u0018\u0010a\u001a\u00020b2\u0006\u0010_\u001a\u00020`2\u0006\u0010c\u001a\u00020dH\u0016J\r\u00105\u001a\u00020\u000fH\u0007\u00a2\u0006\u0002\u0008eJ\u0013\u00106\u001a\u0008\u0012\u0004\u0012\u0002070\u001cH\u0007\u00a2\u0006\u0002\u0008fJ\u000f\u00108\u001a\u0004\u0018\u000109H\u0007\u00a2\u0006\u0002\u0008gJ\r\u0010;\u001a\u00020\tH\u0007\u00a2\u0006\u0002\u0008hJ\r\u0010<\u001a\u00020=H\u0007\u00a2\u0006\u0002\u0008iJ\r\u0010?\u001a\u00020\u000fH\u0007\u00a2\u0006\u0002\u0008jJ\r\u0010@\u001a\u00020,H\u0007\u00a2\u0006\u0002\u0008kJ\r\u0010A\u001a\u00020BH\u0007\u00a2\u0006\u0002\u0008lJ\r\u0010D\u001a\u00020EH\u0007\u00a2\u0006\u0002\u0008mJ\r\u0010H\u001a\u00020\u000fH\u0007\u00a2\u0006\u0002\u0008nR\u0013\u0010\u0008\u001a\u00020\t8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\nR\u0015\u0010\u000b\u001a\u0004\u0018\u00010\u000c8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\rR\u0013\u0010\u000e\u001a\u00020\u000f8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u0010R\u0015\u0010\u0011\u001a\u0004\u0018\u00010\u00128G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0013R\u0013\u0010\u0014\u001a\u00020\u00158G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0016R\u0013\u0010\u0017\u001a\u00020\u000f8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0010R\u0013\u0010\u0018\u001a\u00020\u00198G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u001aR\u0019\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u001c8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001eR\u0013\u0010\u001f\u001a\u00020 8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010!R\u0013\u0010\"\u001a\u00020#8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010$R\u0013\u0010%\u001a\u00020&8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\'R\u0013\u0010(\u001a\u00020)8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010*R\u0013\u0010+\u001a\u00020,8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010-R\u0013\u0010.\u001a\u00020,8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010-R\u0013\u0010/\u001a\u0002008G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u00101R\u0019\u00102\u001a\u0008\u0012\u0004\u0012\u0002030\u001c8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00082\u0010\u001eR\u0019\u00104\u001a\u0008\u0012\u0004\u0012\u0002030\u001c8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00084\u0010\u001eR\u0013\u00105\u001a\u00020\u000f8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00085\u0010\u0010R\u0019\u00106\u001a\u0008\u0012\u0004\u0012\u0002070\u001c8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00086\u0010\u001eR\u0015\u00108\u001a\u0004\u0018\u0001098G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00088\u0010:R\u0013\u0010;\u001a\u00020\t8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008;\u0010\nR\u0013\u0010<\u001a\u00020=8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008<\u0010>R\u0013\u0010?\u001a\u00020\u000f8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008?\u0010\u0010R\u0013\u0010@\u001a\u00020,8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008@\u0010-R\u0013\u0010A\u001a\u00020B8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008A\u0010CR\u0011\u0010D\u001a\u00020E8G\u00a2\u0006\u0006\u001a\u0004\u0008D\u0010FR\u0010\u0010G\u001a\u0004\u0018\u00010EX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010H\u001a\u00020\u000f8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008H\u0010\u0010R\u0015\u0010I\u001a\u0004\u0018\u00010J8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008I\u0010K\u00a8\u0006q"
    }
    d2 = {
        "Lokhttp3/OkHttpClient;",
        "",
        "Lokhttp3/Call$Factory;",
        "Lokhttp3/WebSocket$Factory;",
        "()V",
        "builder",
        "Lokhttp3/OkHttpClient$Builder;",
        "(Lokhttp3/OkHttpClient$Builder;)V",
        "authenticator",
        "Lokhttp3/Authenticator;",
        "()Lokhttp3/Authenticator;",
        "cache",
        "Lokhttp3/Cache;",
        "()Lokhttp3/Cache;",
        "callTimeoutMillis",
        "",
        "()I",
        "certificateChainCleaner",
        "Lokhttp3/internal/tls/CertificateChainCleaner;",
        "()Lokhttp3/internal/tls/CertificateChainCleaner;",
        "certificatePinner",
        "Lokhttp3/CertificatePinner;",
        "()Lokhttp3/CertificatePinner;",
        "connectTimeoutMillis",
        "connectionPool",
        "Lokhttp3/ConnectionPool;",
        "()Lokhttp3/ConnectionPool;",
        "connectionSpecs",
        "",
        "Lokhttp3/ConnectionSpec;",
        "()Ljava/util/List;",
        "cookieJar",
        "Lokhttp3/CookieJar;",
        "()Lokhttp3/CookieJar;",
        "dispatcher",
        "Lokhttp3/Dispatcher;",
        "()Lokhttp3/Dispatcher;",
        "dns",
        "Lokhttp3/Dns;",
        "()Lokhttp3/Dns;",
        "eventListenerFactory",
        "Lokhttp3/EventListener$Factory;",
        "()Lokhttp3/EventListener$Factory;",
        "followRedirects",
        "",
        "()Z",
        "followSslRedirects",
        "hostnameVerifier",
        "Ljavax/net/ssl/HostnameVerifier;",
        "()Ljavax/net/ssl/HostnameVerifier;",
        "interceptors",
        "Lokhttp3/Interceptor;",
        "networkInterceptors",
        "pingIntervalMillis",
        "protocols",
        "Lokhttp3/Protocol;",
        "proxy",
        "Ljava/net/Proxy;",
        "()Ljava/net/Proxy;",
        "proxyAuthenticator",
        "proxySelector",
        "Ljava/net/ProxySelector;",
        "()Ljava/net/ProxySelector;",
        "readTimeoutMillis",
        "retryOnConnectionFailure",
        "socketFactory",
        "Ljavax/net/SocketFactory;",
        "()Ljavax/net/SocketFactory;",
        "sslSocketFactory",
        "Ljavax/net/ssl/SSLSocketFactory;",
        "()Ljavax/net/ssl/SSLSocketFactory;",
        "sslSocketFactoryOrNull",
        "writeTimeoutMillis",
        "x509TrustManager",
        "Ljavax/net/ssl/X509TrustManager;",
        "()Ljavax/net/ssl/X509TrustManager;",
        "-deprecated_authenticator",
        "-deprecated_cache",
        "-deprecated_callTimeoutMillis",
        "-deprecated_certificatePinner",
        "-deprecated_connectTimeoutMillis",
        "-deprecated_connectionPool",
        "-deprecated_connectionSpecs",
        "-deprecated_cookieJar",
        "-deprecated_dispatcher",
        "-deprecated_dns",
        "-deprecated_eventListenerFactory",
        "-deprecated_followRedirects",
        "-deprecated_followSslRedirects",
        "-deprecated_hostnameVerifier",
        "-deprecated_interceptors",
        "-deprecated_networkInterceptors",
        "newBuilder",
        "newCall",
        "Lokhttp3/Call;",
        "request",
        "Lokhttp3/Request;",
        "newWebSocket",
        "Lokhttp3/WebSocket;",
        "listener",
        "Lokhttp3/WebSocketListener;",
        "-deprecated_pingIntervalMillis",
        "-deprecated_protocols",
        "-deprecated_proxy",
        "-deprecated_proxyAuthenticator",
        "-deprecated_proxySelector",
        "-deprecated_readTimeoutMillis",
        "-deprecated_retryOnConnectionFailure",
        "-deprecated_socketFactory",
        "-deprecated_sslSocketFactory",
        "-deprecated_writeTimeoutMillis",
        "Builder",
        "Companion",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le/G;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final c:Le/F$b;


# instance fields
.field private final A:I

.field private final B:I

.field private final C:I

.field private final D:I

.field private final E:I

.field private final d:Le/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Le/o;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le/D;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le/D;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final h:Le/x$b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final i:Z

.field private final j:Le/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final k:Z

.field private final l:Z

.field private final m:Le/s;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final n:Le/e;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final o:Le/v;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final p:Ljava/net/Proxy;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final q:Ljava/net/ProxySelector;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final r:Le/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final s:Ljavax/net/SocketFactory;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final t:Ljavax/net/ssl/SSLSocketFactory;

.field private final u:Ljavax/net/ssl/X509TrustManager;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le/G;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final x:Ljavax/net/ssl/HostnameVerifier;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final y:Le/j;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final z:Le/a/i/c;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Le/F$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/F$b;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/F;->c:Le/F$b;

    const/4 v0, 0x2

    new-array v1, v0, [Le/G;

    sget-object v2, Le/G;->d:Le/G;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Le/G;->b:Le/G;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-static {v1}, Le/a/d;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Le/F;->a:Ljava/util/List;

    new-array v0, v0, [Le/p;

    sget-object v1, Le/p;->d:Le/p;

    aput-object v1, v0, v3

    sget-object v1, Le/p;->f:Le/p;

    aput-object v1, v0, v4

    invoke-static {v0}, Le/a/d;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Le/F;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    new-instance v0, Le/F$a;

    invoke-direct {v0}, Le/F$a;-><init>()V

    invoke-direct {p0, v0}, Le/F;-><init>(Le/F$a;)V

    return-void
.end method

.method public constructor <init>(Le/F$a;)V
    .locals 4
    .param p1    # Le/F$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Le/F$a;->k()Le/t;

    move-result-object v0

    iput-object v0, p0, Le/F;->d:Le/t;

    invoke-virtual {p1}, Le/F$a;->h()Le/o;

    move-result-object v0

    iput-object v0, p0, Le/F;->e:Le/o;

    invoke-virtual {p1}, Le/F$a;->q()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Le/a/d;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Le/F;->f:Ljava/util/List;

    invoke-virtual {p1}, Le/F$a;->r()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Le/a/d;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Le/F;->g:Ljava/util/List;

    invoke-virtual {p1}, Le/F$a;->m()Le/x$b;

    move-result-object v0

    iput-object v0, p0, Le/F;->h:Le/x$b;

    invoke-virtual {p1}, Le/F$a;->y()Z

    move-result v0

    iput-boolean v0, p0, Le/F;->i:Z

    invoke-virtual {p1}, Le/F$a;->b()Le/c;

    move-result-object v0

    iput-object v0, p0, Le/F;->j:Le/c;

    invoke-virtual {p1}, Le/F$a;->n()Z

    move-result v0

    iput-boolean v0, p0, Le/F;->k:Z

    invoke-virtual {p1}, Le/F$a;->o()Z

    move-result v0

    iput-boolean v0, p0, Le/F;->l:Z

    invoke-virtual {p1}, Le/F$a;->j()Le/s;

    move-result-object v0

    iput-object v0, p0, Le/F;->m:Le/s;

    invoke-virtual {p1}, Le/F$a;->c()Le/e;

    move-result-object v0

    iput-object v0, p0, Le/F;->n:Le/e;

    invoke-virtual {p1}, Le/F$a;->l()Le/v;

    move-result-object v0

    iput-object v0, p0, Le/F;->o:Le/v;

    invoke-virtual {p1}, Le/F$a;->u()Ljava/net/Proxy;

    move-result-object v0

    iput-object v0, p0, Le/F;->p:Ljava/net/Proxy;

    invoke-virtual {p1}, Le/F$a;->u()Ljava/net/Proxy;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Le/a/h/a;->a:Le/a/h/a;

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Le/F$a;->w()Ljava/net/ProxySelector;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    sget-object v0, Le/a/h/a;->a:Le/a/h/a;

    :goto_1
    iput-object v0, p0, Le/F;->q:Ljava/net/ProxySelector;

    invoke-virtual {p1}, Le/F$a;->v()Le/c;

    move-result-object v0

    iput-object v0, p0, Le/F;->r:Le/c;

    invoke-virtual {p1}, Le/F$a;->z()Ljavax/net/SocketFactory;

    move-result-object v0

    iput-object v0, p0, Le/F;->s:Ljavax/net/SocketFactory;

    invoke-virtual {p1}, Le/F$a;->i()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Le/F;->v:Ljava/util/List;

    invoke-virtual {p1}, Le/F$a;->t()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Le/F;->w:Ljava/util/List;

    invoke-virtual {p1}, Le/F$a;->p()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v0

    iput-object v0, p0, Le/F;->x:Ljavax/net/ssl/HostnameVerifier;

    invoke-virtual {p1}, Le/F$a;->d()I

    move-result v0

    iput v0, p0, Le/F;->A:I

    invoke-virtual {p1}, Le/F$a;->g()I

    move-result v0

    iput v0, p0, Le/F;->B:I

    invoke-virtual {p1}, Le/F$a;->x()I

    move-result v0

    iput v0, p0, Le/F;->C:I

    invoke-virtual {p1}, Le/F$a;->B()I

    move-result v0

    iput v0, p0, Le/F;->D:I

    invoke-virtual {p1}, Le/F$a;->s()I

    move-result v0

    iput v0, p0, Le/F;->E:I

    invoke-virtual {p1}, Le/F$a;->A()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_9

    iget-object v0, p0, Le/F;->v:Ljava/util/List;

    instance-of v3, v0, Ljava/util/Collection;

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Le/p;

    invoke-virtual {v3}, Le/p;->b()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_6

    goto :goto_3

    :cond_6
    sget-object v0, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v0}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v0

    invoke-virtual {v0}, Le/a/g/g;->d()Ljavax/net/ssl/X509TrustManager;

    move-result-object v0

    iput-object v0, p0, Le/F;->u:Ljavax/net/ssl/X509TrustManager;

    sget-object v0, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v0}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v0

    iget-object v3, p0, Le/F;->u:Ljavax/net/ssl/X509TrustManager;

    invoke-virtual {v0, v3}, Le/a/g/g;->c(Ljavax/net/ssl/X509TrustManager;)V

    sget-object v0, Le/F;->c:Le/F$b;

    iget-object v3, p0, Le/F;->u:Ljavax/net/ssl/X509TrustManager;

    if-eqz v3, :cond_8

    invoke-static {v0, v3}, Le/F$b;->a(Le/F$b;Ljavax/net/ssl/X509TrustManager;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Le/F;->t:Ljavax/net/ssl/SSLSocketFactory;

    sget-object v0, Le/a/i/c;->a:Le/a/i/c$a;

    iget-object v3, p0, Le/F;->u:Ljavax/net/ssl/X509TrustManager;

    if-eqz v3, :cond_7

    invoke-virtual {v0, v3}, Le/a/i/c$a;->a(Ljavax/net/ssl/X509TrustManager;)Le/a/i/c;

    move-result-object v0

    iput-object v0, p0, Le/F;->z:Le/a/i/c;

    goto :goto_4

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_8
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_9
    :goto_3
    invoke-virtual {p1}, Le/F$a;->A()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Le/F;->t:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {p1}, Le/F$a;->e()Le/a/i/c;

    move-result-object v0

    iput-object v0, p0, Le/F;->z:Le/a/i/c;

    invoke-virtual {p1}, Le/F$a;->C()Ljavax/net/ssl/X509TrustManager;

    move-result-object v0

    iput-object v0, p0, Le/F;->u:Ljavax/net/ssl/X509TrustManager;

    :goto_4
    iget-object v0, p0, Le/F;->t:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_a

    sget-object v0, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v0}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v0

    iget-object v3, p0, Le/F;->t:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, v3}, Le/a/g/g;->a(Ljavax/net/ssl/SSLSocketFactory;)V

    :cond_a
    invoke-virtual {p1}, Le/F$a;->f()Le/j;

    move-result-object p1

    iget-object v0, p0, Le/F;->z:Le/a/i/c;

    invoke-virtual {p1, v0}, Le/j;->a(Le/a/i/c;)Le/j;

    move-result-object p1

    iput-object p1, p0, Le/F;->y:Le/j;

    iget-object p1, p0, Le/F;->f:Ljava/util/List;

    const-string v0, "null cannot be cast to non-null type kotlin.collections.List<okhttp3.Interceptor?>"

    if-eqz p1, :cond_e

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_d

    iget-object p1, p0, Le/F;->g:Ljava/util/List;

    if-eqz p1, :cond_c

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_b

    return-void

    :cond_b
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Null network interceptor: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Le/F;->g:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    new-instance p1, Lkotlin/o;

    invoke-direct {p1, v0}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_d
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Null interceptor: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Le/F;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    new-instance p1, Lkotlin/o;

    invoke-direct {p1, v0}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic a()Ljava/util/List;
    .locals 1

    sget-object v0, Le/F;->b:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic b()Ljava/util/List;
    .locals 1

    sget-object v0, Le/F;->a:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final A()Ljavax/net/ssl/SSLSocketFactory;
    .locals 2
    .annotation build Lkotlin/jvm/JvmName;
        name = "sslSocketFactory"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->t:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CLEARTEXT-only client"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final B()I
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "writeTimeoutMillis"
    .end annotation

    iget v0, p0, Le/F;->D:I

    return v0
.end method

.method public a(Le/I;)Le/h;
    .locals 2
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Le/H;->a:Le/H$b;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, p1, v1}, Le/H$b;->a(Le/F;Le/I;Z)Le/H;

    move-result-object p1

    return-object p1
.end method

.method public final c()Le/c;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "authenticator"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->j:Le/c;

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final d()Le/e;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "cache"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/F;->n:Le/e;

    return-object v0
.end method

.method public final e()I
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "callTimeoutMillis"
    .end annotation

    iget v0, p0, Le/F;->A:I

    return v0
.end method

.method public final f()Le/j;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "certificatePinner"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->y:Le/j;

    return-object v0
.end method

.method public final g()I
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "connectTimeoutMillis"
    .end annotation

    iget v0, p0, Le/F;->B:I

    return v0
.end method

.method public final h()Le/o;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "connectionPool"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->e:Le/o;

    return-object v0
.end method

.method public final i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Le/p;",
            ">;"
        }
    .end annotation

    .annotation build Lkotlin/jvm/JvmName;
        name = "connectionSpecs"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->v:Ljava/util/List;

    return-object v0
.end method

.method public final j()Le/s;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "cookieJar"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->m:Le/s;

    return-object v0
.end method

.method public final k()Le/t;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "dispatcher"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->d:Le/t;

    return-object v0
.end method

.method public final l()Le/v;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "dns"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->o:Le/v;

    return-object v0
.end method

.method public final m()Le/x$b;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "eventListenerFactory"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->h:Le/x$b;

    return-object v0
.end method

.method public final n()Z
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "followRedirects"
    .end annotation

    iget-boolean v0, p0, Le/F;->k:Z

    return v0
.end method

.method public final o()Z
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "followSslRedirects"
    .end annotation

    iget-boolean v0, p0, Le/F;->l:Z

    return v0
.end method

.method public final p()Ljavax/net/ssl/HostnameVerifier;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "hostnameVerifier"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->x:Ljavax/net/ssl/HostnameVerifier;

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Le/D;",
            ">;"
        }
    .end annotation

    .annotation build Lkotlin/jvm/JvmName;
        name = "interceptors"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->f:Ljava/util/List;

    return-object v0
.end method

.method public final r()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Le/D;",
            ">;"
        }
    .end annotation

    .annotation build Lkotlin/jvm/JvmName;
        name = "networkInterceptors"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->g:Ljava/util/List;

    return-object v0
.end method

.method public final s()I
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "pingIntervalMillis"
    .end annotation

    iget v0, p0, Le/F;->E:I

    return v0
.end method

.method public final t()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Le/G;",
            ">;"
        }
    .end annotation

    .annotation build Lkotlin/jvm/JvmName;
        name = "protocols"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->w:Ljava/util/List;

    return-object v0
.end method

.method public final u()Ljava/net/Proxy;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "proxy"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/F;->p:Ljava/net/Proxy;

    return-object v0
.end method

.method public final v()Le/c;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "proxyAuthenticator"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->r:Le/c;

    return-object v0
.end method

.method public final w()Ljava/net/ProxySelector;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "proxySelector"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->q:Ljava/net/ProxySelector;

    return-object v0
.end method

.method public final x()I
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "readTimeoutMillis"
    .end annotation

    iget v0, p0, Le/F;->C:I

    return v0
.end method

.method public final y()Z
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "retryOnConnectionFailure"
    .end annotation

    iget-boolean v0, p0, Le/F;->i:Z

    return v0
.end method

.method public final z()Ljavax/net/SocketFactory;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "socketFactory"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F;->s:Ljavax/net/SocketFactory;

    return-object v0
.end method
