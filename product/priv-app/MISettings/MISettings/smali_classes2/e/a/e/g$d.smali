.class public final Le/a/e/g$d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;
.implements Le/a/e/t$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/e/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "d"
.end annotation


# instance fields
.field private final a:Le/a/e/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field final synthetic b:Le/a/e/g;


# direct methods
.method public constructor <init>(Le/a/e/g;Le/a/e/t;)V
    .locals 1
    .param p1    # Le/a/e/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le/a/e/t;",
            ")V"
        }
    .end annotation

    const-string v0, "reader"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Le/a/e/g$d;->a:Le/a/e/t;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(IIIZ)V
    .locals 0

    return-void
.end method

.method public a(IILjava/util/List;)V
    .locals 0
    .param p3    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List<",
            "Le/a/e/c;",
            ">;)V"
        }
    .end annotation

    const-string p1, "requestHeaders"

    invoke-static {p3, p1}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {p1, p2, p3}, Le/a/e/g;->a(ILjava/util/List;)V

    return-void
.end method

.method public a(IJ)V
    .locals 3

    if-nez p1, :cond_1

    iget-object p1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    monitor-enter p1

    :try_start_0
    iget-object v0, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v0}, Le/a/e/g;->w()J

    move-result-wide v1

    add-long/2addr v1, p2

    invoke-static {v0, v1, v2}, Le/a/e/g;->a(Le/a/e/g;J)V

    iget-object p2, p0, Le/a/e/g$d;->b:Le/a/e/g;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->notifyAll()V

    sget-object p2, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p1

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance p2, Lkotlin/o;

    const-string p3, "null cannot be cast to non-null type java.lang.Object"

    invoke-direct {p2, p3}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2

    :cond_1
    iget-object v0, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v0, p1}, Le/a/e/g;->b(I)Le/a/e/u;

    move-result-object p1

    if-eqz p1, :cond_2

    monitor-enter p1

    :try_start_2
    invoke-virtual {p1, p2, p3}, Le/a/e/u;->a(J)V

    sget-object p2, Lkotlin/r;->a:Lkotlin/r;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p1

    goto :goto_0

    :catchall_1
    move-exception p2

    monitor-exit p1

    throw p2

    :cond_2
    :goto_0
    return-void
.end method

.method public a(ILe/a/e/b;)V
    .locals 1
    .param p2    # Le/a/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "errorCode"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v0, p1}, Le/a/e/g;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v0, p1, p2}, Le/a/e/g;->a(ILe/a/e/b;)V

    return-void

    :cond_0
    iget-object v0, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v0, p1}, Le/a/e/g;->d(I)Le/a/e/u;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1, p2}, Le/a/e/u;->b(Le/a/e/b;)V

    :cond_1
    return-void
.end method

.method public a(ILe/a/e/b;Lf/l;)V
    .locals 3
    .param p2    # Le/a/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lf/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "errorCode"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "debugData"

    invoke-static {p3, p2}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lf/l;->k()I

    move-result p2

    iget-object p2, p0, Le/a/e/g$d;->b:Le/a/e/g;

    monitor-enter p2

    :try_start_0
    iget-object p3, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {p3}, Le/a/e/g;->v()Ljava/util/Map;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p3

    const/4 v0, 0x0

    new-array v1, v0, [Le/a/e/u;

    invoke-interface {p3, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p3

    if-eqz p3, :cond_2

    check-cast p3, [Le/a/e/u;

    iget-object v1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Le/a/e/g;->a(Z)V

    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p2

    array-length p2, p3

    :goto_0
    if-ge v0, p2, :cond_1

    aget-object v1, p3, v0

    invoke-virtual {v1}, Le/a/e/u;->f()I

    move-result v2

    if-le v2, p1, :cond_0

    invoke-virtual {v1}, Le/a/e/u;->p()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Le/a/e/b;->e:Le/a/e/b;

    invoke-virtual {v1, v2}, Le/a/e/u;->b(Le/a/e/b;)V

    iget-object v2, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v1}, Le/a/e/u;->f()I

    move-result v1

    invoke-virtual {v2, v1}, Le/a/e/g;->d(I)Le/a/e/u;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    :try_start_1
    new-instance p1, Lkotlin/o;

    const-string p3, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, p3}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p2

    throw p1
.end method

.method public a(ZII)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object p1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    monitor-enter p1

    :try_start_0
    iget-object p2, p0, Le/a/e/g$d;->b:Le/a/e/g;

    const/4 p3, 0x0

    invoke-static {p2, p3}, Le/a/e/g;->a(Le/a/e/g;Z)V

    iget-object p2, p0, Le/a/e/g$d;->b:Le/a/e/g;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->notifyAll()V

    sget-object p2, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p1

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance p2, Lkotlin/o;

    const-string p3, "null cannot be cast to non-null type java.lang.Object"

    invoke-direct {p2, p3}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2

    :cond_1
    iget-object p1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-static {p1}, Le/a/e/g;->c(Le/a/e/g;)Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OkHttp "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v1}, Le/a/e/g;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ping"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_2
    new-instance v1, Le/a/e/k;

    invoke-direct {v1, v0, p0, p2, p3}, Le/a/e/k;-><init>(Ljava/lang/String;Le/a/e/g$d;II)V

    invoke-interface {p1, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    :goto_0
    return-void
.end method

.method public a(ZIILjava/util/List;)V
    .locals 15
    .param p4    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZII",
            "Ljava/util/List<",
            "Le/a/e/c;",
            ">;)V"
        }
    .end annotation

    move-object v9, p0

    move/from16 v0, p1

    move/from16 v7, p2

    move-object/from16 v8, p4

    const-string v1, "headerBlock"

    invoke-static {v8, v1}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v9, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v1, v7}, Le/a/e/g;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v9, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v1, v7, v8, v0}, Le/a/e/g;->a(ILjava/util/List;Z)V

    return-void

    :cond_0
    iget-object v10, v9, Le/a/e/g$d;->b:Le/a/e/g;

    monitor-enter v10

    :try_start_0
    iget-object v1, v9, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v1, v7}, Le/a/e/g;->b(I)Le/a/e/u;

    move-result-object v11

    if-nez v11, :cond_4

    iget-object v1, v9, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v1}, Le/a/e/g;->y()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    monitor-exit v10

    return-void

    :cond_1
    :try_start_1
    iget-object v1, v9, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v1}, Le/a/e/g;->q()I

    move-result v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-gt v7, v1, :cond_2

    monitor-exit v10

    return-void

    :cond_2
    :try_start_2
    rem-int/lit8 v1, v7, 0x2

    iget-object v2, v9, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v2}, Le/a/e/g;->s()I

    move-result v2

    rem-int/lit8 v2, v2, 0x2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v1, v2, :cond_3

    monitor-exit v10

    return-void

    :cond_3
    :try_start_3
    invoke-static/range {p4 .. p4}, Le/a/d;->a(Ljava/util/List;)Le/B;

    move-result-object v6

    new-instance v12, Le/a/e/u;

    iget-object v3, v9, Le/a/e/g$d;->b:Le/a/e/g;

    const/4 v4, 0x0

    move-object v1, v12

    move/from16 v2, p2

    move/from16 v5, p1

    invoke-direct/range {v1 .. v6}, Le/a/e/u;-><init>(ILe/a/e/g;ZZLe/B;)V

    iget-object v1, v9, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v1, v7}, Le/a/e/g;->e(I)V

    iget-object v1, v9, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v1}, Le/a/e/g;->v()Ljava/util/Map;

    move-result-object v1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Le/a/e/g;->a()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v13

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OkHttp "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v9, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v2}, Le/a/e/g;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " stream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v14, Le/a/e/j;

    move-object v1, v14

    move-object v3, v12

    move-object v4, p0

    move-object v5, v11

    move/from16 v6, p2

    move-object/from16 v7, p4

    move/from16 v8, p1

    invoke-direct/range {v1 .. v8}, Le/a/e/j;-><init>(Ljava/lang/String;Le/a/e/u;Le/a/e/g$d;Le/a/e/u;ILjava/util/List;Z)V

    invoke-interface {v13, v14}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v10

    return-void

    :cond_4
    :try_start_4
    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit v10

    invoke-static/range {p4 .. p4}, Le/a/d;->a(Ljava/util/List;)Le/B;

    move-result-object v1

    invoke-virtual {v11, v1, v0}, Le/a/e/u;->a(Le/B;Z)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0
.end method

.method public a(ZILf/k;I)V
    .locals 2
    .param p3    # Lf/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p3, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v0, p2}, Le/a/e/g;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v0, p2, p3, p4, p1}, Le/a/e/g;->a(ILf/k;IZ)V

    return-void

    :cond_0
    iget-object v0, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v0, p2}, Le/a/e/g;->b(I)Le/a/e/u;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object p1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    sget-object v0, Le/a/e/b;->b:Le/a/e/b;

    invoke-virtual {p1, p2, v0}, Le/a/e/g;->c(ILe/a/e/b;)V

    iget-object p1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    int-to-long v0, p4

    invoke-virtual {p1, v0, v1}, Le/a/e/g;->h(J)V

    invoke-interface {p3, v0, v1}, Lf/k;->skip(J)V

    return-void

    :cond_1
    invoke-virtual {v0, p3, p4}, Le/a/e/u;->a(Lf/k;I)V

    if-eqz p1, :cond_2

    sget-object p1, Le/a/d;->b:Le/B;

    const/4 p2, 0x1

    invoke-virtual {v0, p1, p2}, Le/a/e/u;->a(Le/B;Z)V

    :cond_2
    return-void
.end method

.method public a(ZLe/a/e/z;)V
    .locals 3
    .param p2    # Le/a/e/z;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-static {v0}, Le/a/e/g;->c(Le/a/e/g;)Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OkHttp "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v2}, Le/a/e/g;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " ACK Settings"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    new-instance v2, Le/a/e/l;

    invoke-direct {v2, v1, p0, p1, p2}, Le/a/e/l;-><init>(Ljava/lang/String;Le/a/e/g$d;ZLe/a/e/z;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public final b(ZLe/a/e/z;)V
    .locals 6
    .param p2    # Le/a/e/z;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v0}, Le/a/e/g;->x()Le/a/e/v;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    iget-object v2, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v2}, Le/a/e/g;->u()Le/a/e/z;

    move-result-object v2

    invoke-virtual {v2}, Le/a/e/z;->c()I

    move-result v2

    if-eqz p1, :cond_0

    iget-object p1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {p1}, Le/a/e/g;->u()Le/a/e/z;

    move-result-object p1

    invoke-virtual {p1}, Le/a/e/z;->a()V

    :cond_0
    iget-object p1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {p1}, Le/a/e/g;->u()Le/a/e/z;

    move-result-object p1

    invoke-virtual {p1, p2}, Le/a/e/z;->a(Le/a/e/z;)V

    iget-object p1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {p1}, Le/a/e/g;->u()Le/a/e/z;

    move-result-object p1

    invoke-virtual {p1}, Le/a/e/z;->c()I

    move-result p1

    const/4 p2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eq p1, p2, :cond_2

    if-eq p1, v2, :cond_2

    sub-int/2addr p1, v2

    int-to-long p1, p1

    iget-object v2, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v2}, Le/a/e/g;->v()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    iget-object v2, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v2}, Le/a/e/g;->v()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    new-array v5, v3, [Le/a/e/u;

    invoke-interface {v2, v5}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    check-cast v2, [Le/a/e/u;

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/o;

    const-string p2, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, p2}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    const-wide/16 p1, 0x0

    :cond_3
    move-object v2, v4

    :goto_0
    sget-object v5, Lkotlin/r;->a:Lkotlin/r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-object v1, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v1}, Le/a/e/g;->x()Le/a/e/v;

    move-result-object v1

    iget-object v5, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v5}, Le/a/e/g;->u()Le/a/e/z;

    move-result-object v5

    invoke-virtual {v1, v5}, Le/a/e/v;->a(Le/a/e/z;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_4
    iget-object v5, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-static {v5, v1}, Le/a/e/g;->a(Le/a/e/g;Ljava/io/IOException;)V

    :goto_1
    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    monitor-exit v0

    if-eqz v2, :cond_5

    if-eqz v2, :cond_4

    array-length v0, v2

    :goto_2
    if-ge v3, v0, :cond_5

    aget-object v1, v2, v3

    monitor-enter v1

    :try_start_5
    invoke-virtual {v1, p1, p2}, Le/a/e/u;->a(J)V

    sget-object v4, Lkotlin/r;->a:Lkotlin/r;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :catchall_0
    move-exception p1

    monitor-exit v1

    throw p1

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v4

    :cond_5
    invoke-static {}, Le/a/e/g;->a()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "OkHttp "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v0}, Le/a/e/g;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " settings"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    new-instance v0, Le/a/e/i;

    invoke-direct {v0, p2, p0}, Le/a/e/i;-><init>(Ljava/lang/String;Le/a/e/g$d;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    :catchall_1
    move-exception p1

    :try_start_6
    monitor-exit v1

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public run()V
    .locals 5

    sget-object v0, Le/a/e/b;->c:Le/a/e/b;

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Le/a/e/g$d;->a:Le/a/e/t;

    invoke-virtual {v2, p0}, Le/a/e/t;->a(Le/a/e/t$c;)V

    :goto_0
    iget-object v2, p0, Le/a/e/g$d;->a:Le/a/e/t;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, p0}, Le/a/e/t;->a(ZLe/a/e/t$c;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Le/a/e/b;->a:Le/a/e/b;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v0, Le/a/e/b;->f:Le/a/e/b;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v3

    move-object v2, v0

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v2, v0

    :goto_1
    :try_start_2
    sget-object v2, Le/a/e/b;->b:Le/a/e/b;

    sget-object v0, Le/a/e/b;->b:Le/a/e/b;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_2
    iget-object v3, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v3, v2, v0, v1}, Le/a/e/g;->a(Le/a/e/b;Le/a/e/b;Ljava/io/IOException;)V

    iget-object v0, p0, Le/a/e/g$d;->a:Le/a/e/t;

    invoke-static {v0}, Le/a/d;->a(Ljava/io/Closeable;)V

    return-void

    :catchall_1
    move-exception v3

    :goto_3
    iget-object v4, p0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v4, v2, v0, v1}, Le/a/e/g;->a(Le/a/e/b;Le/a/e/b;Ljava/io/IOException;)V

    iget-object v0, p0, Le/a/e/g$d;->a:Le/a/e/t;

    invoke-static {v0}, Le/a/d;->a(Ljava/io/Closeable;)V

    throw v3
.end method
