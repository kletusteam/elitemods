.class public final enum Le/a/e/b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/a/e/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Le/a/e/b;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0010\u0008\u0086\u0001\u0018\u0000 \u00122\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0012B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0013"
    }
    d2 = {
        "Lokhttp3/internal/http2/ErrorCode;",
        "",
        "httpCode",
        "",
        "(Ljava/lang/String;II)V",
        "getHttpCode",
        "()I",
        "NO_ERROR",
        "PROTOCOL_ERROR",
        "INTERNAL_ERROR",
        "FLOW_CONTROL_ERROR",
        "REFUSED_STREAM",
        "CANCEL",
        "COMPRESSION_ERROR",
        "CONNECT_ERROR",
        "ENHANCE_YOUR_CALM",
        "INADEQUATE_SECURITY",
        "HTTP_1_1_REQUIRED",
        "Companion",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final enum a:Le/a/e/b;

.field public static final enum b:Le/a/e/b;

.field public static final enum c:Le/a/e/b;

.field public static final enum d:Le/a/e/b;

.field public static final enum e:Le/a/e/b;

.field public static final enum f:Le/a/e/b;

.field public static final enum g:Le/a/e/b;

.field public static final enum h:Le/a/e/b;

.field public static final enum i:Le/a/e/b;

.field public static final enum j:Le/a/e/b;

.field public static final enum k:Le/a/e/b;

.field private static final synthetic l:[Le/a/e/b;

.field public static final m:Le/a/e/b$a;


# instance fields
.field private final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xb

    new-array v1, v0, [Le/a/e/b;

    new-instance v2, Le/a/e/b;

    const/4 v3, 0x0

    const-string v4, "NO_ERROR"

    invoke-direct {v2, v4, v3, v3}, Le/a/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v2, Le/a/e/b;->a:Le/a/e/b;

    aput-object v2, v1, v3

    new-instance v2, Le/a/e/b;

    const/4 v3, 0x1

    const-string v4, "PROTOCOL_ERROR"

    invoke-direct {v2, v4, v3, v3}, Le/a/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v2, Le/a/e/b;->b:Le/a/e/b;

    aput-object v2, v1, v3

    new-instance v2, Le/a/e/b;

    const/4 v3, 0x2

    const-string v4, "INTERNAL_ERROR"

    invoke-direct {v2, v4, v3, v3}, Le/a/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v2, Le/a/e/b;->c:Le/a/e/b;

    aput-object v2, v1, v3

    new-instance v2, Le/a/e/b;

    const/4 v3, 0x3

    const-string v4, "FLOW_CONTROL_ERROR"

    invoke-direct {v2, v4, v3, v3}, Le/a/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v2, Le/a/e/b;->d:Le/a/e/b;

    aput-object v2, v1, v3

    new-instance v2, Le/a/e/b;

    const/4 v3, 0x4

    const/4 v4, 0x7

    const-string v5, "REFUSED_STREAM"

    invoke-direct {v2, v5, v3, v4}, Le/a/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v2, Le/a/e/b;->e:Le/a/e/b;

    aput-object v2, v1, v3

    new-instance v2, Le/a/e/b;

    const/4 v3, 0x5

    const/16 v5, 0x8

    const-string v6, "CANCEL"

    invoke-direct {v2, v6, v3, v5}, Le/a/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v2, Le/a/e/b;->f:Le/a/e/b;

    aput-object v2, v1, v3

    new-instance v2, Le/a/e/b;

    const/4 v3, 0x6

    const/16 v6, 0x9

    const-string v7, "COMPRESSION_ERROR"

    invoke-direct {v2, v7, v3, v6}, Le/a/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v2, Le/a/e/b;->g:Le/a/e/b;

    aput-object v2, v1, v3

    new-instance v2, Le/a/e/b;

    const/16 v3, 0xa

    const-string v7, "CONNECT_ERROR"

    invoke-direct {v2, v7, v4, v3}, Le/a/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v2, Le/a/e/b;->h:Le/a/e/b;

    aput-object v2, v1, v4

    new-instance v2, Le/a/e/b;

    const-string v4, "ENHANCE_YOUR_CALM"

    invoke-direct {v2, v4, v5, v0}, Le/a/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v2, Le/a/e/b;->i:Le/a/e/b;

    aput-object v2, v1, v5

    new-instance v0, Le/a/e/b;

    const-string v2, "INADEQUATE_SECURITY"

    const/16 v4, 0xc

    invoke-direct {v0, v2, v6, v4}, Le/a/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Le/a/e/b;->j:Le/a/e/b;

    aput-object v0, v1, v6

    new-instance v0, Le/a/e/b;

    const-string v2, "HTTP_1_1_REQUIRED"

    const/16 v4, 0xd

    invoke-direct {v0, v2, v3, v4}, Le/a/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Le/a/e/b;->k:Le/a/e/b;

    aput-object v0, v1, v3

    sput-object v1, Le/a/e/b;->l:[Le/a/e/b;

    new-instance v0, Le/a/e/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/a/e/b$a;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/a/e/b;->m:Le/a/e/b$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Le/a/e/b;->n:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Le/a/e/b;
    .locals 1

    const-class v0, Le/a/e/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Le/a/e/b;

    return-object p0
.end method

.method public static values()[Le/a/e/b;
    .locals 1

    sget-object v0, Le/a/e/b;->l:[Le/a/e/b;

    invoke-virtual {v0}, [Le/a/e/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Le/a/e/b;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Le/a/e/b;->n:I

    return v0
.end method
