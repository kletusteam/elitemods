.class public final Le/a/e/j;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Le/a/e/g$d;->a(ZIILjava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Le/a/e/u;

.field final synthetic c:Le/a/e/g$d;

.field final synthetic d:Le/a/e/u;

.field final synthetic e:I

.field final synthetic f:Ljava/util/List;

.field final synthetic g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Le/a/e/u;Le/a/e/g$d;Le/a/e/u;ILjava/util/List;Z)V
    .locals 0

    iput-object p1, p0, Le/a/e/j;->a:Ljava/lang/String;

    iput-object p2, p0, Le/a/e/j;->b:Le/a/e/u;

    iput-object p3, p0, Le/a/e/j;->c:Le/a/e/g$d;

    iput-object p4, p0, Le/a/e/j;->d:Le/a/e/u;

    iput p5, p0, Le/a/e/j;->e:I

    iput-object p6, p0, Le/a/e/j;->f:Ljava/util/List;

    iput-boolean p7, p0, Le/a/e/j;->g:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    iget-object v0, p0, Le/a/e/j;->a:Ljava/lang/String;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "currentThread"

    invoke-static {v1, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Le/a/e/j;->c:Le/a/e/g$d;

    iget-object v0, v0, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v0}, Le/a/e/g;->r()Le/a/e/g$c;

    move-result-object v0

    iget-object v3, p0, Le/a/e/j;->b:Le/a/e/u;

    invoke-virtual {v0, v3}, Le/a/e/g$c;->a(Le/a/e/u;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v3, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v3}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v3

    const/4 v4, 0x4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Http2Connection.Listener failure for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Le/a/e/j;->c:Le/a/e/g$d;

    iget-object v6, v6, Le/a/e/g$d;->b:Le/a/e/g;

    invoke-virtual {v6}, Le/a/e/g;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v0}, Le/a/g/g;->a(ILjava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v3, p0, Le/a/e/j;->b:Le/a/e/u;

    sget-object v4, Le/a/e/b;->b:Le/a/e/b;

    invoke-virtual {v3, v4, v0}, Le/a/e/u;->a(Le/a/e/b;Ljava/io/IOException;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    return-void

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    throw v0
.end method
