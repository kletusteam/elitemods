.class public final Le/a/e/u$b;
.super Ljava/lang/Object;

# interfaces
.implements Lf/A;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/e/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field private final a:Lf/h;

.field private b:Le/B;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private c:Z

.field private d:Z

.field final synthetic e:Le/a/e/u;


# direct methods
.method public constructor <init>(Le/a/e/u;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    iput-object p1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p2, p0, Le/a/e/u$b;->d:Z

    new-instance p1, Lf/h;

    invoke-direct {p1}, Lf/h;-><init>()V

    iput-object p1, p0, Le/a/e/u$b;->a:Lf/h;

    return-void
.end method

.method private final a(Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v1}, Le/a/e/u;->o()Le/a/e/u$d;

    move-result-object v1

    invoke-virtual {v1}, Lf/d;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :goto_0
    :try_start_1
    iget-object v1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v1}, Le/a/e/u;->n()J

    move-result-wide v1

    iget-object v3, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v3}, Le/a/e/u;->m()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    iget-boolean v1, p0, Le/a/e/u$b;->d:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Le/a/e/u$b;->c:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v1}, Le/a/e/u;->d()Le/a/e/b;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v1}, Le/a/e/u;->t()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v1}, Le/a/e/u;->o()Le/a/e/u$d;

    move-result-object v1

    invoke-virtual {v1}, Le/a/e/u$d;->m()V

    iget-object v1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v1}, Le/a/e/u;->b()V

    iget-object v1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v1}, Le/a/e/u;->m()J

    move-result-wide v1

    iget-object v3, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v3}, Le/a/e/u;->n()J

    move-result-wide v3

    sub-long/2addr v1, v3

    iget-object v3, p0, Le/a/e/u$b;->a:Lf/h;

    invoke-virtual {v3}, Lf/h;->size()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v9

    iget-object v1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v1}, Le/a/e/u;->n()J

    move-result-wide v2

    add-long/2addr v2, v9

    invoke-virtual {v1, v2, v3}, Le/a/e/u;->d(J)V

    if-eqz p1, :cond_1

    iget-object p1, p0, Le/a/e/u$b;->a:Lf/h;

    invoke-virtual {p1}, Lf/h;->size()J

    move-result-wide v1

    cmp-long p1, v9, v1

    if-nez p1, :cond_1

    iget-object p1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {p1}, Le/a/e/u;->d()Le/a/e/b;

    move-result-object p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    move v7, p1

    sget-object p1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    monitor-exit v0

    iget-object p1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {p1}, Le/a/e/u;->o()Le/a/e/u$d;

    move-result-object p1

    invoke-virtual {p1}, Lf/d;->j()V

    :try_start_3
    iget-object p1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {p1}, Le/a/e/u;->c()Le/a/e/g;

    move-result-object v5

    iget-object p1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {p1}, Le/a/e/u;->f()I

    move-result v6

    iget-object v8, p0, Le/a/e/u$b;->a:Lf/h;

    invoke-virtual/range {v5 .. v10}, Le/a/e/g;->a(IZLf/h;J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object p1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {p1}, Le/a/e/u;->o()Le/a/e/u$d;

    move-result-object p1

    invoke-virtual {p1}, Le/a/e/u$d;->m()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->o()Le/a/e/u$d;

    move-result-object v0

    invoke-virtual {v0}, Le/a/e/u$d;->m()V

    throw p1

    :catchall_1
    move-exception p1

    :try_start_4
    iget-object v1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v1}, Le/a/e/u;->o()Le/a/e/u$d;

    move-result-object v1

    invoke-virtual {v1}, Le/a/e/u$d;->m()V

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit v0

    throw p1
.end method


# virtual methods
.method public a(Lf/h;J)V
    .locals 2
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sget-boolean v1, Lkotlin/s;->a:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "Assertion failed"

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Le/a/e/u$b;->a:Lf/h;

    invoke-virtual {v0, p1, p2, p3}, Lf/h;->a(Lf/h;J)V

    :goto_1
    iget-object p1, p0, Le/a/e/u$b;->a:Lf/h;

    invoke-virtual {p1}, Lf/h;->size()J

    move-result-wide p1

    const-wide/16 v0, 0x4000

    cmp-long p1, p1, v0

    if-ltz p1, :cond_2

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Le/a/e/u$b;->a(Z)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Le/a/e/u$b;->c:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Le/a/e/u$b;->d:Z

    return v0
.end method

.method public c()Lf/E;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->o()Le/a/e/u$d;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    sget-boolean v2, Lkotlin/s;->a:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Assertion failed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    monitor-enter v0

    :try_start_0
    iget-boolean v2, p0, Le/a/e/u$b;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_2

    monitor-exit v0

    return-void

    :cond_2
    :try_start_1
    iget-object v2, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v2}, Le/a/e/u;->d()Le/a/e/b;

    move-result-object v2

    const/4 v3, 0x0

    if-nez v2, :cond_3

    move v2, v1

    goto :goto_1

    :cond_3
    move v2, v3

    :goto_1
    sget-object v4, Lkotlin/r;->a:Lkotlin/r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v0

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->k()Le/a/e/u$b;

    move-result-object v0

    iget-boolean v0, v0, Le/a/e/u$b;->d:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Le/a/e/u$b;->a:Lf/h;

    invoke-virtual {v0}, Lf/h;->size()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v3

    :goto_2
    iget-object v4, p0, Le/a/e/u$b;->b:Le/B;

    if-eqz v4, :cond_5

    move v4, v1

    goto :goto_3

    :cond_5
    move v4, v3

    :goto_3
    if-eqz v4, :cond_8

    :goto_4
    iget-object v0, p0, Le/a/e/u$b;->a:Lf/h;

    invoke-virtual {v0}, Lf/h;->size()J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-lez v0, :cond_6

    invoke-direct {p0, v3}, Le/a/e/u$b;->a(Z)V

    goto :goto_4

    :cond_6
    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->c()Le/a/e/g;

    move-result-object v0

    iget-object v3, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v3}, Le/a/e/u;->f()I

    move-result v3

    iget-object v4, p0, Le/a/e/u$b;->b:Le/B;

    if-eqz v4, :cond_7

    invoke-static {v4}, Le/a/d;->a(Le/B;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v3, v2, v4}, Le/a/e/g;->a(IZLjava/util/List;)V

    goto :goto_6

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0

    :cond_8
    if-eqz v0, :cond_9

    :goto_5
    iget-object v0, p0, Le/a/e/u$b;->a:Lf/h;

    invoke-virtual {v0}, Lf/h;->size()J

    move-result-wide v2

    cmp-long v0, v2, v6

    if-lez v0, :cond_a

    invoke-direct {p0, v1}, Le/a/e/u$b;->a(Z)V

    goto :goto_5

    :cond_9
    if-eqz v2, :cond_a

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->c()Le/a/e/g;

    move-result-object v2

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->f()I

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Le/a/e/g;->a(IZLf/h;J)V

    :cond_a
    :goto_6
    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    monitor-enter v0

    :try_start_2
    iput-boolean v1, p0, Le/a/e/u$b;->c:Z

    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v0

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->c()Le/a/e/g;

    move-result-object v0

    invoke-virtual {v0}, Le/a/e/g;->flush()V

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->a()V

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :catchall_1
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public flush()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sget-boolean v1, Lkotlin/s;->a:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Assertion failed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v1}, Le/a/e/u;->b()V

    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    :goto_1
    iget-object v0, p0, Le/a/e/u$b;->a:Lf/h;

    invoke-virtual {v0}, Lf/h;->size()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Le/a/e/u$b;->a(Z)V

    iget-object v0, p0, Le/a/e/u$b;->e:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->c()Le/a/e/g;

    move-result-object v0

    invoke-virtual {v0}, Le/a/e/g;->flush()V

    goto :goto_1

    :cond_2
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
