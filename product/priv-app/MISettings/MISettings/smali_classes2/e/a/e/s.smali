.class public final Le/a/e/s;
.super Ljava/lang/Object;

# interfaces
.implements Le/a/c/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/a/e/s$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 &2\u00020\u0001:\u0001&B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0008\u001a\u00020\u0005H\u0016J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u0012H\u0016J\u0008\u0010\u001a\u001a\u00020\u0012H\u0016J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0012\u0010\u001f\u001a\u0004\u0018\u00010 2\u0006\u0010!\u001a\u00020\u000cH\u0016J\u0010\u0010\"\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0008\u0010#\u001a\u00020$H\u0016J\u0010\u0010%\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0016H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lokhttp3/internal/http2/Http2ExchangeCodec;",
        "Lokhttp3/internal/http/ExchangeCodec;",
        "client",
        "Lokhttp3/OkHttpClient;",
        "realConnection",
        "Lokhttp3/internal/connection/RealConnection;",
        "chain",
        "Lokhttp3/Interceptor$Chain;",
        "connection",
        "Lokhttp3/internal/http2/Http2Connection;",
        "(Lokhttp3/OkHttpClient;Lokhttp3/internal/connection/RealConnection;Lokhttp3/Interceptor$Chain;Lokhttp3/internal/http2/Http2Connection;)V",
        "canceled",
        "",
        "protocol",
        "Lokhttp3/Protocol;",
        "stream",
        "Lokhttp3/internal/http2/Http2Stream;",
        "cancel",
        "",
        "createRequestBody",
        "Lokio/Sink;",
        "request",
        "Lokhttp3/Request;",
        "contentLength",
        "",
        "finishRequest",
        "flushRequest",
        "openResponseBodySource",
        "Lokio/Source;",
        "response",
        "Lokhttp3/Response;",
        "readResponseHeaders",
        "Lokhttp3/Response$Builder;",
        "expectContinue",
        "reportedContentLength",
        "trailers",
        "Lokhttp3/Headers;",
        "writeRequestHeaders",
        "Companion",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Le/a/e/s$a;


# instance fields
.field private volatile d:Le/a/e/u;

.field private final e:Le/G;

.field private volatile f:Z

.field private final g:Le/a/b/e;

.field private final h:Le/D$a;

.field private final i:Le/a/e/g;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    new-instance v0, Le/a/e/s$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/a/e/s$a;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/a/e/s;->c:Le/a/e/s$a;

    const-string v2, "connection"

    const-string v3, "host"

    const-string v4, "keep-alive"

    const-string v5, "proxy-connection"

    const-string v6, "te"

    const-string v7, "transfer-encoding"

    const-string v8, "encoding"

    const-string v9, "upgrade"

    const-string v10, ":method"

    const-string v11, ":path"

    const-string v12, ":scheme"

    const-string v13, ":authority"

    filled-new-array/range {v2 .. v13}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Le/a/d;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Le/a/e/s;->a:Ljava/util/List;

    const-string v1, "connection"

    const-string v2, "host"

    const-string v3, "keep-alive"

    const-string v4, "proxy-connection"

    const-string v5, "te"

    const-string v6, "transfer-encoding"

    const-string v7, "encoding"

    const-string v8, "upgrade"

    filled-new-array/range {v1 .. v8}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Le/a/d;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Le/a/e/s;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Le/F;Le/a/b/e;Le/D$a;Le/a/e/g;)V
    .locals 1
    .param p1    # Le/F;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/a/b/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Le/D$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Le/a/e/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "client"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realConnection"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chain"

    invoke-static {p3, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connection"

    invoke-static {p4, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Le/a/e/s;->g:Le/a/b/e;

    iput-object p3, p0, Le/a/e/s;->h:Le/D$a;

    iput-object p4, p0, Le/a/e/s;->i:Le/a/e/g;

    invoke-virtual {p1}, Le/F;->t()Ljava/util/List;

    move-result-object p1

    sget-object p2, Le/G;->e:Le/G;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Le/G;->e:Le/G;

    goto :goto_0

    :cond_0
    sget-object p1, Le/G;->d:Le/G;

    :goto_0
    iput-object p1, p0, Le/a/e/s;->e:Le/G;

    return-void
.end method

.method public static final synthetic d()Ljava/util/List;
    .locals 1

    sget-object v0, Le/a/e/s;->a:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic e()Ljava/util/List;
    .locals 1

    sget-object v0, Le/a/e/s;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(Z)Le/L$a;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/a/e/s;->d:Le/a/e/u;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Le/a/e/u;->s()Le/B;

    move-result-object v0

    sget-object v2, Le/a/e/s;->c:Le/a/e/s$a;

    iget-object v3, p0, Le/a/e/s;->e:Le/G;

    invoke-virtual {v2, v0, v3}, Le/a/e/s$a;->a(Le/B;Le/G;)Le/L$a;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-virtual {v0}, Le/L$a;->b()I

    move-result p1

    const/16 v2, 0x64

    if-ne p1, v2, :cond_0

    move-object v0, v1

    :cond_0
    return-object v0

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1
.end method

.method public a(Le/I;J)Lf/A;
    .locals 0
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string p2, "request"

    invoke-static {p1, p2}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Le/a/e/s;->d:Le/a/e/u;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Le/a/e/u;->j()Lf/A;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1
.end method

.method public a(Le/L;)Lf/C;
    .locals 1
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Le/a/e/s;->d:Le/a/e/u;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Le/a/e/u;->l()Le/a/e/u$c;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Le/a/e/s;->d:Le/a/e/u;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Le/a/e/u;->j()Lf/A;

    move-result-object v0

    invoke-interface {v0}, Lf/A;->close()V

    return-void

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0
.end method

.method public a(Le/I;)V
    .locals 4
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/e/s;->d:Le/a/e/u;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Le/I;->a()Le/K;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Le/a/e/s;->c:Le/a/e/s$a;

    invoke-virtual {v1, p1}, Le/a/e/s$a;->a(Le/I;)Ljava/util/List;

    move-result-object p1

    iget-object v1, p0, Le/a/e/s;->i:Le/a/e/g;

    invoke-virtual {v1, p1, v0}, Le/a/e/g;->a(Ljava/util/List;Z)Le/a/e/u;

    move-result-object p1

    iput-object p1, p0, Le/a/e/s;->d:Le/a/e/u;

    iget-boolean p1, p0, Le/a/e/s;->f:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    iget-object p1, p0, Le/a/e/s;->d:Le/a/e/u;

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v0

    :cond_2
    sget-object v0, Le/a/e/b;->f:Le/a/e/b;

    invoke-virtual {p1, v0}, Le/a/e/u;->a(Le/a/e/b;)V

    new-instance p1, Ljava/io/IOException;

    const-string v0, "Canceled"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    iget-object p1, p0, Le/a/e/s;->d:Le/a/e/u;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Le/a/e/u;->r()Lf/E;

    move-result-object p1

    iget-object v1, p0, Le/a/e/s;->h:Le/D$a;

    invoke-interface {v1}, Le/D$a;->a()I

    move-result v1

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1, v2, v3}, Lf/E;->a(JLjava/util/concurrent/TimeUnit;)Lf/E;

    iget-object p1, p0, Le/a/e/s;->d:Le/a/e/u;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Le/a/e/u;->u()Lf/E;

    move-result-object p1

    iget-object v0, p0, Le/a/e/s;->h:Le/D$a;

    invoke-interface {v0}, Le/D$a;->b()I

    move-result v0

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Lf/E;->a(JLjava/util/concurrent/TimeUnit;)Lf/E;

    return-void

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v0

    :cond_5
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v0
.end method

.method public b(Le/L;)J
    .locals 2
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Le/a/d;->a(Le/L;)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Le/a/b/e;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/e/s;->g:Le/a/b/e;

    return-object v0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Le/a/e/s;->i:Le/a/e/g;

    invoke-virtual {v0}, Le/a/e/g;->flush()V

    return-void
.end method

.method public cancel()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Le/a/e/s;->f:Z

    iget-object v0, p0, Le/a/e/s;->d:Le/a/e/u;

    if-eqz v0, :cond_0

    sget-object v1, Le/a/e/b;->f:Le/a/e/b;

    invoke-virtual {v0, v1}, Le/a/e/u;->a(Le/a/e/b;)V

    :cond_0
    return-void
.end method
