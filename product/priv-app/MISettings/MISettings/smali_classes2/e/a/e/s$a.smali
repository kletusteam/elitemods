.class public final Le/a/e/s$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/e/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/b/g;)V
    .locals 0

    invoke-direct {p0}, Le/a/e/s$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Le/B;Le/G;)Le/L$a;
    .locals 7
    .param p1    # Le/B;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/G;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "headerBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "protocol"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Le/B$a;

    invoke-direct {v0}, Le/B$a;-><init>()V

    invoke-virtual {p1}, Le/B;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    invoke-virtual {p1, v3}, Le/B;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3}, Le/B;->b(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, ":status"

    invoke-static {v4, v6}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    sget-object v2, Le/a/c/l;->a:Le/a/c/l$a;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HTTP/1.1 "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Le/a/c/l$a;->a(Ljava/lang/String;)Le/a/c/l;

    move-result-object v2

    goto :goto_1

    :cond_0
    invoke-static {}, Le/a/e/s;->e()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v0, v4, v5}, Le/B$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/B$a;

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    new-instance p1, Le/L$a;

    invoke-direct {p1}, Le/L$a;-><init>()V

    invoke-virtual {p1, p2}, Le/L$a;->a(Le/G;)Le/L$a;

    iget p2, v2, Le/a/c/l;->c:I

    invoke-virtual {p1, p2}, Le/L$a;->a(I)Le/L$a;

    iget-object p2, v2, Le/a/c/l;->d:Ljava/lang/String;

    invoke-virtual {p1, p2}, Le/L$a;->a(Ljava/lang/String;)Le/L$a;

    invoke-virtual {v0}, Le/B$a;->a()Le/B;

    move-result-object p2

    invoke-virtual {p1, p2}, Le/L$a;->a(Le/B;)Le/L$a;

    return-object p1

    :cond_3
    new-instance p1, Ljava/net/ProtocolException;

    const-string p2, "Expected \':status\' header not present"

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Le/I;)Ljava/util/List;
    .locals 6
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le/I;",
            ")",
            "Ljava/util/List<",
            "Le/a/e/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Le/I;->d()Le/B;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Le/B;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v2, Le/a/e/c;

    sget-object v3, Le/a/e/c;->c:Lf/l;

    invoke-virtual {p1}, Le/I;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Le/a/e/c;-><init>(Lf/l;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Le/a/e/c;

    sget-object v3, Le/a/e/c;->d:Lf/l;

    sget-object v4, Le/a/c/j;->a:Le/a/c/j;

    invoke-virtual {p1}, Le/I;->h()Le/C;

    move-result-object v5

    invoke-virtual {v4, v5}, Le/a/c/j;->a(Le/C;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Le/a/e/c;-><init>(Lf/l;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v2, "Host"

    invoke-virtual {p1, v2}, Le/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v3, Le/a/e/c;

    sget-object v4, Le/a/e/c;->f:Lf/l;

    invoke-direct {v3, v4, v2}, Le/a/e/c;-><init>(Lf/l;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v2, Le/a/e/c;

    sget-object v3, Le/a/e/c;->e:Lf/l;

    invoke-virtual {p1}, Le/I;->h()Le/C;

    move-result-object p1

    invoke-virtual {p1}, Le/C;->n()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v3, p1}, Le/a/e/c;-><init>(Lf/l;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x0

    invoke-virtual {v0}, Le/B;->size()I

    move-result v2

    :goto_0
    if-ge p1, v2, :cond_4

    invoke-virtual {v0, p1}, Le/B;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Locale.US"

    invoke-static {v4, v5}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v3, :cond_3

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v3, v4}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Le/a/e/s;->d()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "te"

    invoke-static {v3, v4}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0, p1}, Le/B;->b(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "trailers"

    invoke-static {v4, v5}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    new-instance v4, Le/a/e/c;

    invoke-virtual {v0, p1}, Le/B;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Le/a/e/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_3
    new-instance p1, Lkotlin/o;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    return-object v1
.end method
