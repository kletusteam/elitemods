.class public final Le/a/e/n;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Le/a/e/g;->a(ILjava/util/List;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Le/a/e/g;

.field final synthetic c:I

.field final synthetic d:Ljava/util/List;

.field final synthetic e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Le/a/e/g;ILjava/util/List;Z)V
    .locals 0

    iput-object p1, p0, Le/a/e/n;->a:Ljava/lang/String;

    iput-object p2, p0, Le/a/e/n;->b:Le/a/e/g;

    iput p3, p0, Le/a/e/n;->c:I

    iput-object p4, p0, Le/a/e/n;->d:Ljava/util/List;

    iput-boolean p5, p0, Le/a/e/n;->e:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Le/a/e/n;->a:Ljava/lang/String;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "currentThread"

    invoke-static {v1, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Le/a/e/n;->b:Le/a/e/g;

    invoke-static {v0}, Le/a/e/g;->b(Le/a/e/g;)Le/a/e/y;

    move-result-object v0

    iget v3, p0, Le/a/e/n;->c:I

    iget-object v4, p0, Le/a/e/n;->d:Ljava/util/List;

    iget-boolean v5, p0, Le/a/e/n;->e:Z

    invoke-interface {v0, v3, v4, v5}, Le/a/e/y;->a(ILjava/util/List;Z)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v3, p0, Le/a/e/n;->b:Le/a/e/g;

    invoke-virtual {v3}, Le/a/e/g;->x()Le/a/e/v;

    move-result-object v3

    iget v4, p0, Le/a/e/n;->c:I

    sget-object v5, Le/a/e/b;->f:Le/a/e/b;

    invoke-virtual {v3, v4, v5}, Le/a/e/v;->a(ILe/a/e/b;)V

    :cond_0
    if-nez v0, :cond_1

    iget-boolean v0, p0, Le/a/e/n;->e:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Le/a/e/n;->b:Le/a/e/g;

    monitor-enter v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v3, p0, Le/a/e/n;->b:Le/a/e/g;

    invoke-static {v3}, Le/a/e/g;->a(Le/a/e/g;)Ljava/util/Set;

    move-result-object v3

    iget v4, p0, Le/a/e/n;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v0

    throw v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    :cond_2
    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    return-void

    :catchall_1
    move-exception v0

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    throw v0
.end method
