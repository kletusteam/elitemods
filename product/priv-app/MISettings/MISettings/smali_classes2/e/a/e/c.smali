.class public final Le/a/e/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/a/e/c$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0006\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0007B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0006\u0012\u0006\u0010\u0004\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000b\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0006H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\nH\u00d6\u0001J\u0008\u0010\u0012\u001a\u00020\u0003H\u0016R\u0010\u0010\t\u001a\u00020\n8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lokhttp3/internal/http2/Header;",
        "",
        "name",
        "",
        "value",
        "(Ljava/lang/String;Ljava/lang/String;)V",
        "Lokio/ByteString;",
        "(Lokio/ByteString;Ljava/lang/String;)V",
        "(Lokio/ByteString;Lokio/ByteString;)V",
        "hpackSize",
        "",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "Companion",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final a:Lf/l;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final b:Lf/l;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final c:Lf/l;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final d:Lf/l;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final e:Lf/l;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final f:Lf/l;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final g:Le/a/e/c$a;


# instance fields
.field public final h:I
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation
.end field

.field public final i:Lf/l;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public final j:Lf/l;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Le/a/e/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/a/e/c$a;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/a/e/c;->g:Le/a/e/c$a;

    sget-object v0, Lf/l;->b:Lf/l$a;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Lf/l$a;->c(Ljava/lang/String;)Lf/l;

    move-result-object v0

    sput-object v0, Le/a/e/c;->a:Lf/l;

    sget-object v0, Lf/l;->b:Lf/l$a;

    const-string v1, ":status"

    invoke-virtual {v0, v1}, Lf/l$a;->c(Ljava/lang/String;)Lf/l;

    move-result-object v0

    sput-object v0, Le/a/e/c;->b:Lf/l;

    sget-object v0, Lf/l;->b:Lf/l$a;

    const-string v1, ":method"

    invoke-virtual {v0, v1}, Lf/l$a;->c(Ljava/lang/String;)Lf/l;

    move-result-object v0

    sput-object v0, Le/a/e/c;->c:Lf/l;

    sget-object v0, Lf/l;->b:Lf/l$a;

    const-string v1, ":path"

    invoke-virtual {v0, v1}, Lf/l$a;->c(Ljava/lang/String;)Lf/l;

    move-result-object v0

    sput-object v0, Le/a/e/c;->d:Lf/l;

    sget-object v0, Lf/l;->b:Lf/l$a;

    const-string v1, ":scheme"

    invoke-virtual {v0, v1}, Lf/l$a;->c(Ljava/lang/String;)Lf/l;

    move-result-object v0

    sput-object v0, Le/a/e/c;->e:Lf/l;

    sget-object v0, Lf/l;->b:Lf/l$a;

    const-string v1, ":authority"

    invoke-virtual {v0, v1}, Lf/l$a;->c(Ljava/lang/String;)Lf/l;

    move-result-object v0

    sput-object v0, Le/a/e/c;->f:Lf/l;

    return-void
.end method

.method public constructor <init>(Lf/l;Lf/l;)V
    .locals 1
    .param p1    # Lf/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lf/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/a/e/c;->i:Lf/l;

    iput-object p2, p0, Le/a/e/c;->j:Lf/l;

    iget-object p1, p0, Le/a/e/c;->i:Lf/l;

    invoke-virtual {p1}, Lf/l;->k()I

    move-result p1

    add-int/lit8 p1, p1, 0x20

    iget-object p2, p0, Le/a/e/c;->j:Lf/l;

    invoke-virtual {p2}, Lf/l;->k()I

    move-result p2

    add-int/2addr p1, p2

    iput p1, p0, Le/a/e/c;->h:I

    return-void
.end method

.method public constructor <init>(Lf/l;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lf/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lf/l;->b:Lf/l$a;

    invoke-virtual {v0, p2}, Lf/l$a;->c(Ljava/lang/String;)Lf/l;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Le/a/e/c;-><init>(Lf/l;Lf/l;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lf/l;->b:Lf/l$a;

    invoke-virtual {v0, p1}, Lf/l$a;->c(Ljava/lang/String;)Lf/l;

    move-result-object p1

    sget-object v0, Lf/l;->b:Lf/l$a;

    invoke-virtual {v0, p2}, Lf/l$a;->c(Ljava/lang/String;)Lf/l;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Le/a/e/c;-><init>(Lf/l;Lf/l;)V

    return-void
.end method


# virtual methods
.method public final a()Lf/l;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/e/c;->i:Lf/l;

    return-object v0
.end method

.method public final b()Lf/l;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/e/c;->j:Lf/l;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Le/a/e/c;

    if-eqz v0, :cond_0

    check-cast p1, Le/a/e/c;

    iget-object v0, p0, Le/a/e/c;->i:Lf/l;

    iget-object v1, p1, Le/a/e/c;->i:Lf/l;

    invoke-static {v0, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/a/e/c;->j:Lf/l;

    iget-object p1, p1, Le/a/e/c;->j:Lf/l;

    invoke-static {v0, p1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Le/a/e/c;->i:Lf/l;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/l;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Le/a/e/c;->j:Lf/l;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lf/l;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Le/a/e/c;->i:Lf/l;

    invoke-virtual {v1}, Lf/l;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le/a/e/c;->j:Lf/l;

    invoke-virtual {v1}, Lf/l;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
