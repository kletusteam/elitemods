.class public final Le/a/e/u$c;
.super Ljava/lang/Object;

# interfaces
.implements Lf/C;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/e/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "c"
.end annotation


# instance fields
.field private final a:Lf/h;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lf/h;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private c:Le/B;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private d:Z

.field private final e:J

.field private f:Z

.field final synthetic g:Le/a/e/u;


# direct methods
.method public constructor <init>(Le/a/e/u;JZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ)V"
        }
    .end annotation

    iput-object p1, p0, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Le/a/e/u$c;->e:J

    iput-boolean p4, p0, Le/a/e/u$c;->f:Z

    new-instance p1, Lf/h;

    invoke-direct {p1}, Lf/h;-><init>()V

    iput-object p1, p0, Le/a/e/u$c;->a:Lf/h;

    new-instance p1, Lf/h;

    invoke-direct {p1}, Lf/h;-><init>()V

    iput-object p1, p0, Le/a/e/u$c;->b:Lf/h;

    return-void
.end method

.method private final d(J)V
    .locals 2

    iget-object v0, p0, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sget-boolean v1, Lkotlin/s;->a:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "Assertion failed"

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->c()Le/a/e/g;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Le/a/e/g;->h(J)V

    return-void
.end method


# virtual methods
.method public final a(Le/B;)V
    .locals 0
    .param p1    # Le/B;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Le/a/e/u$c;->c:Le/B;

    return-void
.end method

.method public final a(Lf/k;J)V
    .locals 9
    .param p1    # Lf/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    sget-boolean v2, Lkotlin/s;->a:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "Assertion failed"

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v0, p2, v2

    if-lez v0, :cond_a

    iget-object v0, p0, Le/a/e/u$c;->g:Le/a/e/u;

    monitor-enter v0

    :try_start_0
    iget-boolean v4, p0, Le/a/e/u$c;->f:Z

    iget-object v5, p0, Le/a/e/u$c;->b:Lf/h;

    invoke-virtual {v5}, Lf/h;->size()J

    move-result-wide v5

    add-long/2addr v5, p2

    iget-wide v7, p0, Le/a/e/u$c;->e:J

    cmp-long v5, v5, v7

    const/4 v6, 0x0

    if-lez v5, :cond_2

    move v5, v1

    goto :goto_1

    :cond_2
    move v5, v6

    :goto_1
    sget-object v7, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-exit v0

    if-eqz v5, :cond_3

    invoke-interface {p1, p2, p3}, Lf/k;->skip(J)V

    iget-object p1, p0, Le/a/e/u$c;->g:Le/a/e/u;

    sget-object p2, Le/a/e/b;->d:Le/a/e/b;

    invoke-virtual {p1, p2}, Le/a/e/u;->a(Le/a/e/b;)V

    return-void

    :cond_3
    if-eqz v4, :cond_4

    invoke-interface {p1, p2, p3}, Lf/k;->skip(J)V

    return-void

    :cond_4
    iget-object v0, p0, Le/a/e/u$c;->a:Lf/h;

    invoke-interface {p1, v0, p2, p3}, Lf/C;->b(Lf/h;J)J

    move-result-wide v4

    const-wide/16 v7, -0x1

    cmp-long v0, v4, v7

    if-eqz v0, :cond_9

    sub-long/2addr p2, v4

    iget-object v0, p0, Le/a/e/u$c;->g:Le/a/e/u;

    monitor-enter v0

    :try_start_1
    iget-boolean v4, p0, Le/a/e/u$c;->d:Z

    if-eqz v4, :cond_5

    iget-object v4, p0, Le/a/e/u$c;->a:Lf/h;

    invoke-virtual {v4}, Lf/h;->size()J

    move-result-wide v4

    iget-object v6, p0, Le/a/e/u$c;->a:Lf/h;

    invoke-virtual {v6}, Lf/h;->a()V

    goto :goto_3

    :cond_5
    iget-object v4, p0, Le/a/e/u$c;->b:Lf/h;

    invoke-virtual {v4}, Lf/h;->size()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-nez v4, :cond_6

    move v6, v1

    :cond_6
    iget-object v4, p0, Le/a/e/u$c;->b:Lf/h;

    iget-object v5, p0, Le/a/e/u$c;->a:Lf/h;

    invoke-virtual {v4, v5}, Lf/h;->a(Lf/C;)J

    if-eqz v6, :cond_8

    iget-object v4, p0, Le/a/e/u$c;->g:Le/a/e/u;

    if-eqz v4, :cond_7

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    goto :goto_2

    :cond_7
    new-instance p1, Lkotlin/o;

    const-string p2, "null cannot be cast to non-null type java.lang.Object"

    invoke-direct {p1, p2}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    :goto_2
    move-wide v4, v2

    :goto_3
    sget-object v6, Lkotlin/r;->a:Lkotlin/r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    cmp-long v0, v4, v2

    if-lez v0, :cond_1

    invoke-direct {p0, v4, v5}, Le/a/e/u$c;->d(J)V

    goto/16 :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_9
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :catchall_1
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_a
    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Le/a/e/u$c;->f:Z

    return-void
.end method

.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Le/a/e/u$c;->d:Z

    return v0
.end method

.method public b(Lf/h;J)J
    .locals 18
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-wide/from16 v2, p2

    const-string v4, "sink"

    invoke-static {v0, v4}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-ltz v6, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    if-eqz v6, :cond_c

    :goto_1
    iget-object v6, v1, Le/a/e/u$c;->g:Le/a/e/u;

    monitor-enter v6

    :try_start_0
    iget-object v9, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v9}, Le/a/e/u;->i()Le/a/e/u$d;

    move-result-object v9

    invoke-virtual {v9}, Lf/d;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v9, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v9}, Le/a/e/u;->d()Le/a/e/b;

    move-result-object v9

    const/4 v10, 0x0

    if-eqz v9, :cond_3

    iget-object v9, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v9}, Le/a/e/u;->e()Ljava/io/IOException;

    move-result-object v9

    if-eqz v9, :cond_1

    goto :goto_2

    :cond_1
    new-instance v9, Le/a/e/A;

    iget-object v11, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v11}, Le/a/e/u;->d()Le/a/e/b;

    move-result-object v11

    if-eqz v11, :cond_2

    invoke-direct {v9, v11}, Le/a/e/A;-><init>(Le/a/e/b;)V

    goto :goto_2

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v10

    :cond_3
    move-object v9, v10

    :goto_2
    :try_start_2
    iget-boolean v11, v1, Le/a/e/u$c;->d:Z

    if-nez v11, :cond_b

    iget-object v11, v1, Le/a/e/u$c;->b:Lf/h;

    invoke-virtual {v11}, Lf/h;->size()J

    move-result-wide v11

    cmp-long v11, v11, v4

    const-wide/16 v12, -0x1

    if-lez v11, :cond_4

    iget-object v11, v1, Le/a/e/u$c;->b:Lf/h;

    iget-object v14, v1, Le/a/e/u$c;->b:Lf/h;

    invoke-virtual {v14}, Lf/h;->size()J

    move-result-wide v14

    invoke-static {v2, v3, v14, v15}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v14

    invoke-virtual {v11, v0, v14, v15}, Lf/h;->b(Lf/h;J)J

    move-result-wide v14

    iget-object v11, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v11}, Le/a/e/u;->h()J

    move-result-wide v16

    add-long v4, v16, v14

    invoke-virtual {v11, v4, v5}, Le/a/e/u;->c(J)V

    iget-object v4, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v4}, Le/a/e/u;->h()J

    move-result-wide v4

    iget-object v11, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v11}, Le/a/e/u;->g()J

    move-result-wide v16

    sub-long v4, v4, v16

    if-nez v9, :cond_6

    iget-object v11, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v11}, Le/a/e/u;->c()Le/a/e/g;

    move-result-object v11

    invoke-virtual {v11}, Le/a/e/g;->t()Le/a/e/z;

    move-result-object v11

    invoke-virtual {v11}, Le/a/e/z;->c()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-long v7, v11

    cmp-long v7, v4, v7

    if-ltz v7, :cond_6

    iget-object v7, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v7}, Le/a/e/u;->c()Le/a/e/g;

    move-result-object v7

    iget-object v8, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v8}, Le/a/e/u;->f()I

    move-result v8

    invoke-virtual {v7, v8, v4, v5}, Le/a/e/g;->b(IJ)V

    iget-object v4, v1, Le/a/e/u$c;->g:Le/a/e/u;

    iget-object v5, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v5}, Le/a/e/u;->h()J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Le/a/e/u;->b(J)V

    goto :goto_3

    :cond_4
    iget-boolean v4, v1, Le/a/e/u$c;->f:Z

    if-nez v4, :cond_5

    if-nez v9, :cond_5

    iget-object v4, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v4}, Le/a/e/u;->t()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-wide v14, v12

    const/4 v4, 0x1

    goto :goto_4

    :cond_5
    move-wide v14, v12

    :cond_6
    :goto_3
    const/4 v4, 0x0

    :goto_4
    :try_start_3
    iget-object v5, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v5}, Le/a/e/u;->i()Le/a/e/u$d;

    move-result-object v5

    invoke-virtual {v5}, Le/a/e/u$d;->m()V

    sget-object v5, Lkotlin/r;->a:Lkotlin/r;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit v6

    if-eqz v4, :cond_7

    const-wide/16 v4, 0x0

    goto/16 :goto_1

    :cond_7
    cmp-long v0, v14, v12

    if-eqz v0, :cond_8

    invoke-direct {v1, v14, v15}, Le/a/e/u$c;->d(J)V

    return-wide v14

    :cond_8
    if-eqz v9, :cond_a

    if-nez v9, :cond_9

    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v10

    :cond_9
    throw v9

    :cond_a
    return-wide v12

    :cond_b
    :try_start_4
    new-instance v0, Ljava/io/IOException;

    const-string v2, "stream closed"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_5
    iget-object v2, v1, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v2}, Le/a/e/u;->i()Le/a/e/u$d;

    move-result-object v2

    invoke-virtual {v2}, Le/a/e/u$d;->m()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "byteCount < 0: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Le/a/e/u$c;->f:Z

    return v0
.end method

.method public c()Lf/E;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->i()Le/a/e/u$d;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/a/e/u$c;->g:Le/a/e/u;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Le/a/e/u$c;->d:Z

    iget-object v1, p0, Le/a/e/u$c;->b:Lf/h;

    invoke-virtual {v1}, Lf/h;->size()J

    move-result-wide v1

    iget-object v3, p0, Le/a/e/u$c;->b:Lf/h;

    invoke-virtual {v3}, Lf/h;->a()V

    iget-object v3, p0, Le/a/e/u$c;->g:Le/a/e/u;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    sget-object v3, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-lez v0, :cond_0

    invoke-direct {p0, v1, v2}, Le/a/e/u$c;->d(J)V

    :cond_0
    iget-object v0, p0, Le/a/e/u$c;->g:Le/a/e/u;

    invoke-virtual {v0}, Le/a/e/u;->a()V

    return-void

    :cond_1
    :try_start_1
    new-instance v1, Lkotlin/o;

    const-string v2, "null cannot be cast to non-null type java.lang.Object"

    invoke-direct {v1, v2}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
