.class public final Le/a/e/r;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Le/a/e/g;->b(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Le/a/e/g;

.field final synthetic c:I

.field final synthetic d:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Le/a/e/g;IJ)V
    .locals 0

    iput-object p1, p0, Le/a/e/r;->a:Ljava/lang/String;

    iput-object p2, p0, Le/a/e/r;->b:Le/a/e/g;

    iput p3, p0, Le/a/e/r;->c:I

    iput-wide p4, p0, Le/a/e/r;->d:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Le/a/e/r;->a:Ljava/lang/String;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "currentThread"

    invoke-static {v1, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Le/a/e/r;->b:Le/a/e/g;

    invoke-virtual {v0}, Le/a/e/g;->x()Le/a/e/v;

    move-result-object v0

    iget v3, p0, Le/a/e/r;->c:I

    iget-wide v4, p0, Le/a/e/r;->d:J

    invoke-virtual {v0, v3, v4, v5}, Le/a/e/v;->b(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v3, p0, Le/a/e/r;->b:Le/a/e/g;

    invoke-static {v3, v0}, Le/a/e/g;->a(Le/a/e/g;Ljava/io/IOException;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    return-void

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    throw v0
.end method
