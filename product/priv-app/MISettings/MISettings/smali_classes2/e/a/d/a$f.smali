.class final Le/a/d/a$f;
.super Ljava/lang/Object;

# interfaces
.implements Lf/A;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation


# instance fields
.field private final a:Lf/o;

.field private b:Z

.field final synthetic c:Le/a/d/a;


# direct methods
.method public constructor <init>(Le/a/d/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Le/a/d/a$f;->c:Le/a/d/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/o;

    invoke-static {p1}, Le/a/d/a;->c(Le/a/d/a;)Lf/j;

    move-result-object p1

    invoke-interface {p1}, Lf/A;->c()Lf/E;

    move-result-object p1

    invoke-direct {v0, p1}, Lf/o;-><init>(Lf/E;)V

    iput-object v0, p0, Le/a/d/a$f;->a:Lf/o;

    return-void
.end method


# virtual methods
.method public a(Lf/h;J)V
    .locals 7
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Le/a/d/a$f;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lf/h;->size()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    move-wide v5, p2

    invoke-static/range {v1 .. v6}, Le/a/d;->a(JJJ)V

    iget-object v0, p0, Le/a/d/a$f;->c:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->c(Le/a/d/a;)Lf/j;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lf/A;->a(Lf/h;J)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c()Lf/E;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/d/a$f;->a:Lf/o;

    return-object v0
.end method

.method public close()V
    .locals 2

    iget-boolean v0, p0, Le/a/d/a$f;->b:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Le/a/d/a$f;->b:Z

    iget-object v0, p0, Le/a/d/a$f;->c:Le/a/d/a;

    iget-object v1, p0, Le/a/d/a$f;->a:Lf/o;

    invoke-static {v0, v1}, Le/a/d/a;->a(Le/a/d/a;Lf/o;)V

    iget-object v0, p0, Le/a/d/a$f;->c:Le/a/d/a;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Le/a/d/a;->a(Le/a/d/a;I)V

    return-void
.end method

.method public flush()V
    .locals 1

    iget-boolean v0, p0, Le/a/d/a$f;->b:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Le/a/d/a$f;->c:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->c(Le/a/d/a;)Lf/j;

    move-result-object v0

    invoke-interface {v0}, Lf/j;->flush()V

    return-void
.end method
