.class final Le/a/d/a$b;
.super Ljava/lang/Object;

# interfaces
.implements Lf/A;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field private final a:Lf/o;

.field private b:Z

.field final synthetic c:Le/a/d/a;


# direct methods
.method public constructor <init>(Le/a/d/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Le/a/d/a$b;->c:Le/a/d/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/o;

    invoke-static {p1}, Le/a/d/a;->c(Le/a/d/a;)Lf/j;

    move-result-object p1

    invoke-interface {p1}, Lf/A;->c()Lf/E;

    move-result-object p1

    invoke-direct {v0, p1}, Lf/o;-><init>(Lf/E;)V

    iput-object v0, p0, Le/a/d/a$b;->a:Lf/o;

    return-void
.end method


# virtual methods
.method public a(Lf/h;J)V
    .locals 2
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Le/a/d/a$b;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Le/a/d/a$b;->c:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->c(Le/a/d/a;)Lf/j;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lf/j;->d(J)Lf/j;

    iget-object v0, p0, Le/a/d/a$b;->c:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->c(Le/a/d/a;)Lf/j;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    iget-object v0, p0, Le/a/d/a$b;->c:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->c(Le/a/d/a;)Lf/j;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lf/A;->a(Lf/h;J)V

    iget-object p1, p0, Le/a/d/a$b;->c:Le/a/d/a;

    invoke-static {p1}, Le/a/d/a;->c(Le/a/d/a;)Lf/j;

    move-result-object p1

    invoke-interface {p1, v1}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c()Lf/E;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/d/a$b;->a:Lf/o;

    return-object v0
.end method

.method public declared-synchronized close()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Le/a/d/a$b;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Le/a/d/a$b;->b:Z

    iget-object v0, p0, Le/a/d/a$b;->c:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->c(Le/a/d/a;)Lf/j;

    move-result-object v0

    const-string v1, "0\r\n\r\n"

    invoke-interface {v0, v1}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    iget-object v0, p0, Le/a/d/a$b;->c:Le/a/d/a;

    iget-object v1, p0, Le/a/d/a$b;->a:Lf/o;

    invoke-static {v0, v1}, Le/a/d/a;->a(Le/a/d/a;Lf/o;)V

    iget-object v0, p0, Le/a/d/a$b;->c:Le/a/d/a;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Le/a/d/a;->a(Le/a/d/a;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized flush()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Le/a/d/a$b;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Le/a/d/a$b;->c:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->c(Le/a/d/a;)Lf/j;

    move-result-object v0

    invoke-interface {v0}, Lf/j;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
