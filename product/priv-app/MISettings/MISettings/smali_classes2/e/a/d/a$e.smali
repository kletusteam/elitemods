.class final Le/a/d/a$e;
.super Le/a/d/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "e"
.end annotation


# instance fields
.field private d:J

.field final synthetic e:Le/a/d/a;


# direct methods
.method public constructor <init>(Le/a/d/a;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    iput-object p1, p0, Le/a/d/a$e;->e:Le/a/d/a;

    invoke-direct {p0, p1}, Le/a/d/a$a;-><init>(Le/a/d/a;)V

    iput-wide p2, p0, Le/a/d/a$e;->d:J

    iget-wide p1, p0, Le/a/d/a$e;->d:J

    const-wide/16 v0, 0x0

    cmp-long p1, p1, v0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Le/a/d/a$a;->b()V

    :cond_0
    return-void
.end method


# virtual methods
.method public b(Lf/h;J)J
    .locals 7
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ltz v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    :goto_0
    if-eqz v2, :cond_7

    invoke-virtual {p0}, Le/a/d/a$a;->a()Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    move v3, v4

    :goto_1
    if-eqz v3, :cond_6

    iget-wide v2, p0, Le/a/d/a$e;->d:J

    cmp-long v4, v2, v0

    const-wide/16 v5, -0x1

    if-nez v4, :cond_2

    return-wide v5

    :cond_2
    invoke-static {v2, v3, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    invoke-super {p0, p1, p2, p3}, Le/a/d/a$a;->b(Lf/h;J)J

    move-result-wide p1

    cmp-long p3, p1, v5

    if-nez p3, :cond_4

    iget-object p1, p0, Le/a/d/a$e;->e:Le/a/d/a;

    invoke-static {p1}, Le/a/d/a;->b(Le/a/d/a;)Le/a/b/e;

    move-result-object p1

    if-nez p1, :cond_3

    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1

    :cond_3
    invoke-virtual {p1}, Le/a/b/e;->i()V

    new-instance p1, Ljava/net/ProtocolException;

    const-string p2, "unexpected end of stream"

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Le/a/d/a$a;->b()V

    throw p1

    :cond_4
    iget-wide v2, p0, Le/a/d/a$e;->d:J

    sub-long/2addr v2, p1

    iput-wide v2, p0, Le/a/d/a$e;->d:J

    iget-wide v2, p0, Le/a/d/a$e;->d:J

    cmp-long p3, v2, v0

    if-nez p3, :cond_5

    invoke-virtual {p0}, Le/a/d/a$a;->b()V

    :cond_5
    return-wide p1

    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "byteCount < 0: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public close()V
    .locals 4

    invoke-virtual {p0}, Le/a/d/a$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-wide v0, p0, Le/a/d/a$e;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    const/16 v0, 0x64

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p0, v0, v1}, Le/a/d;->a(Lf/C;ILjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Le/a/d/a$e;->e:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->b(Le/a/d/a;)Le/a/b/e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Le/a/b/e;->i()V

    invoke-virtual {p0}, Le/a/d/a$a;->b()V

    goto :goto_0

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0

    :cond_2
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Le/a/d/a$a;->a(Z)V

    return-void
.end method
