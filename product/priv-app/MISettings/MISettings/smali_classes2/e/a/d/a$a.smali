.class abstract Le/a/d/a$a;
.super Ljava/lang/Object;

# interfaces
.implements Lf/C;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "a"
.end annotation


# instance fields
.field private final a:Lf/o;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private b:Z

.field final synthetic c:Le/a/d/a;


# direct methods
.method public constructor <init>(Le/a/d/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Le/a/d/a$a;->c:Le/a/d/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/o;

    invoke-static {p1}, Le/a/d/a;->d(Le/a/d/a;)Lf/k;

    move-result-object p1

    invoke-interface {p1}, Lf/C;->c()Lf/E;

    move-result-object p1

    invoke-direct {v0, p1}, Lf/o;-><init>(Lf/E;)V

    iput-object v0, p0, Le/a/d/a$a;->a:Lf/o;

    return-void
.end method


# virtual methods
.method protected final a(Z)V
    .locals 0

    iput-boolean p1, p0, Le/a/d/a$a;->b:Z

    return-void
.end method

.method protected final a()Z
    .locals 1

    iget-boolean v0, p0, Le/a/d/a$a;->b:Z

    return v0
.end method

.method public b(Lf/h;J)J
    .locals 1
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Le/a/d/a$a;->c:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->d(Le/a/d/a;)Lf/k;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lf/C;->b(Lf/h;J)J

    move-result-wide p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide p1

    :catch_0
    move-exception p1

    iget-object p2, p0, Le/a/d/a$a;->c:Le/a/d/a;

    invoke-static {p2}, Le/a/d/a;->b(Le/a/d/a;)Le/a/b/e;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1

    :cond_0
    invoke-virtual {p2}, Le/a/b/e;->i()V

    invoke-virtual {p0}, Le/a/d/a$a;->b()V

    throw p1
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Le/a/d/a$a;->c:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->e(Le/a/d/a;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Le/a/d/a$a;->c:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->e(Le/a/d/a;)I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Le/a/d/a$a;->c:Le/a/d/a;

    iget-object v2, p0, Le/a/d/a$a;->a:Lf/o;

    invoke-static {v0, v2}, Le/a/d/a;->a(Le/a/d/a;Lf/o;)V

    iget-object v0, p0, Le/a/d/a$a;->c:Le/a/d/a;

    invoke-static {v0, v1}, Le/a/d/a;->a(Le/a/d/a;I)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Le/a/d/a$a;->c:Le/a/d/a;

    invoke-static {v2}, Le/a/d/a;->e(Le/a/d/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()Lf/E;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/d/a$a;->a:Lf/o;

    return-object v0
.end method
