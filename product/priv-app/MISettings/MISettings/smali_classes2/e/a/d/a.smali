.class public final Le/a/d/a;
.super Ljava/lang/Object;

# interfaces
.implements Le/a/c/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/a/d/a$f;,
        Le/a/d/a$b;,
        Le/a/d/a$a;,
        Le/a/d/a$e;,
        Le/a/d/a$c;,
        Le/a/d/a$g;,
        Le/a/d/a$d;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0010\u0018\u0000 =2\u00020\u0001:\u0007:;<=>?@B)\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u0014\u001a\u00020\u0015H\u0016J\n\u0010\u0016\u001a\u0004\u0018\u00010\u0005H\u0016J\u0018\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u000cH\u0016J\u0010\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0008\u0010\u001f\u001a\u00020\u0015H\u0016J\u0008\u0010 \u001a\u00020\u0015H\u0016J\u0008\u0010!\u001a\u00020\u0018H\u0002J\u0010\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0002J\u0010\u0010&\u001a\u00020#2\u0006\u0010\'\u001a\u00020\u000cH\u0002J\u0008\u0010(\u001a\u00020\u0018H\u0002J\u0008\u0010)\u001a\u00020#H\u0002J\u0010\u0010*\u001a\u00020#2\u0006\u0010+\u001a\u00020,H\u0016J\u0008\u0010-\u001a\u00020.H\u0002J\u0008\u0010/\u001a\u00020\u0013H\u0002J\u0012\u00100\u001a\u0004\u0018\u0001012\u0006\u00102\u001a\u00020\u000eH\u0016J\u0010\u00103\u001a\u00020\u000c2\u0006\u0010+\u001a\u00020,H\u0016J\u000e\u00104\u001a\u00020\u00152\u0006\u0010+\u001a\u00020,J\u0008\u0010\u0012\u001a\u00020\u0013H\u0016J\u0016\u00105\u001a\u00020\u00152\u0006\u00106\u001a\u00020\u00132\u0006\u00107\u001a\u00020.J\u0010\u00108\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u000c\u00109\u001a\u00020\u000e*\u00020\u001aH\u0002J\u000c\u00109\u001a\u00020\u000e*\u00020,H\u0002R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000fR\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006A"
    }
    d2 = {
        "Lokhttp3/internal/http1/Http1ExchangeCodec;",
        "Lokhttp3/internal/http/ExchangeCodec;",
        "client",
        "Lokhttp3/OkHttpClient;",
        "realConnection",
        "Lokhttp3/internal/connection/RealConnection;",
        "source",
        "Lokio/BufferedSource;",
        "sink",
        "Lokio/BufferedSink;",
        "(Lokhttp3/OkHttpClient;Lokhttp3/internal/connection/RealConnection;Lokio/BufferedSource;Lokio/BufferedSink;)V",
        "headerLimit",
        "",
        "isClosed",
        "",
        "()Z",
        "state",
        "",
        "trailers",
        "Lokhttp3/Headers;",
        "cancel",
        "",
        "connection",
        "createRequestBody",
        "Lokio/Sink;",
        "request",
        "Lokhttp3/Request;",
        "contentLength",
        "detachTimeout",
        "timeout",
        "Lokio/ForwardingTimeout;",
        "finishRequest",
        "flushRequest",
        "newChunkedSink",
        "newChunkedSource",
        "Lokio/Source;",
        "url",
        "Lokhttp3/HttpUrl;",
        "newFixedLengthSource",
        "length",
        "newKnownLengthSink",
        "newUnknownLengthSource",
        "openResponseBodySource",
        "response",
        "Lokhttp3/Response;",
        "readHeaderLine",
        "",
        "readHeaders",
        "readResponseHeaders",
        "Lokhttp3/Response$Builder;",
        "expectContinue",
        "reportedContentLength",
        "skipConnectBody",
        "writeRequest",
        "headers",
        "requestLine",
        "writeRequestHeaders",
        "isChunked",
        "AbstractSource",
        "ChunkedSink",
        "ChunkedSource",
        "Companion",
        "FixedLengthSource",
        "KnownLengthSink",
        "UnknownLengthSource",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final a:Le/a/d/a$d;


# instance fields
.field private b:I

.field private c:J

.field private d:Le/B;

.field private final e:Le/F;

.field private final f:Le/a/b/e;

.field private final g:Lf/k;

.field private final h:Lf/j;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Le/a/d/a$d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/a/d/a$d;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/a/d/a;->a:Le/a/d/a$d;

    return-void
.end method

.method public constructor <init>(Le/F;Le/a/b/e;Lf/k;Lf/j;)V
    .locals 1
    .param p1    # Le/F;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Le/a/b/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Lf/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lf/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "source"

    invoke-static {p3, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sink"

    invoke-static {p4, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/a/d/a;->e:Le/F;

    iput-object p2, p0, Le/a/d/a;->f:Le/a/b/e;

    iput-object p3, p0, Le/a/d/a;->g:Lf/k;

    iput-object p4, p0, Le/a/d/a;->h:Lf/j;

    const/high16 p1, 0x40000

    int-to-long p1, p1

    iput-wide p1, p0, Le/a/d/a;->c:J

    return-void
.end method

.method public static final synthetic a(Le/a/d/a;)Le/F;
    .locals 0

    iget-object p0, p0, Le/a/d/a;->e:Le/F;

    return-object p0
.end method

.method private final a(J)Lf/C;
    .locals 2

    iget v0, p0, Le/a/d/a;->b:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x5

    iput v0, p0, Le/a/d/a;->b:I

    new-instance v0, Le/a/d/a$e;

    invoke-direct {v0, p0, p1, p2}, Le/a/d/a$e;-><init>(Le/a/d/a;J)V

    return-object v0

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "state: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Le/a/d/a;->b:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private final a(Le/C;)Lf/C;
    .locals 2

    iget v0, p0, Le/a/d/a;->b:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x5

    iput v0, p0, Le/a/d/a;->b:I

    new-instance v0, Le/a/d/a$c;

    invoke-direct {v0, p0, p1}, Le/a/d/a$c;-><init>(Le/a/d/a;Le/C;)V

    return-object v0

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "state: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Le/a/d/a;->b:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static final synthetic a(Le/a/d/a;I)V
    .locals 0

    iput p1, p0, Le/a/d/a;->b:I

    return-void
.end method

.method public static final synthetic a(Le/a/d/a;Le/B;)V
    .locals 0

    iput-object p1, p0, Le/a/d/a;->d:Le/B;

    return-void
.end method

.method public static final synthetic a(Le/a/d/a;Lf/o;)V
    .locals 0

    invoke-direct {p0, p1}, Le/a/d/a;->a(Lf/o;)V

    return-void
.end method

.method private final a(Lf/o;)V
    .locals 2

    invoke-virtual {p1}, Lf/o;->g()Lf/E;

    move-result-object v0

    sget-object v1, Lf/E;->a:Lf/E;

    invoke-virtual {p1, v1}, Lf/o;->a(Lf/E;)Lf/o;

    invoke-virtual {v0}, Lf/E;->a()Lf/E;

    invoke-virtual {v0}, Lf/E;->b()Lf/E;

    return-void
.end method

.method public static final synthetic b(Le/a/d/a;)Le/a/b/e;
    .locals 0

    iget-object p0, p0, Le/a/d/a;->f:Le/a/b/e;

    return-object p0
.end method

.method private final b(Le/I;)Z
    .locals 2
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "Transfer-Encoding"

    invoke-virtual {p1, v0}, Le/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "chunked"

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public static final synthetic c(Le/a/d/a;)Lf/j;
    .locals 0

    iget-object p0, p0, Le/a/d/a;->h:Lf/j;

    return-object p0
.end method

.method private final d()Lf/A;
    .locals 2

    iget v0, p0, Le/a/d/a;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Le/a/d/a;->b:I

    new-instance v0, Le/a/d/a$b;

    invoke-direct {v0, p0}, Le/a/d/a$b;-><init>(Le/a/d/a;)V

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Le/a/d/a;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static final synthetic d(Le/a/d/a;)Lf/k;
    .locals 0

    iget-object p0, p0, Le/a/d/a;->g:Lf/k;

    return-object p0
.end method

.method private final d(Le/L;)Z
    .locals 3
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    const-string v1, "Transfer-Encoding"

    const/4 v2, 0x2

    invoke-static {p1, v1, v0, v2, v0}, Le/L;->a(Le/L;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "chunked"

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public static final synthetic e(Le/a/d/a;)I
    .locals 0

    iget p0, p0, Le/a/d/a;->b:I

    return p0
.end method

.method private final e()Lf/A;
    .locals 2

    iget v0, p0, Le/a/d/a;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Le/a/d/a;->b:I

    new-instance v0, Le/a/d/a$f;

    invoke-direct {v0, p0}, Le/a/d/a$f;-><init>(Le/a/d/a;)V

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Le/a/d/a;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static final synthetic f(Le/a/d/a;)Le/B;
    .locals 0

    iget-object p0, p0, Le/a/d/a;->d:Le/B;

    return-object p0
.end method

.method private final f()Lf/C;
    .locals 2

    iget v0, p0, Le/a/d/a;->b:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x5

    iput v0, p0, Le/a/d/a;->b:I

    iget-object v0, p0, Le/a/d/a;->f:Le/a/b/e;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Le/a/b/e;->i()V

    new-instance v0, Le/a/d/a$g;

    invoke-direct {v0, p0}, Le/a/d/a$g;-><init>(Le/a/d/a;)V

    return-object v0

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Le/a/d/a;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static final synthetic g(Le/a/d/a;)Le/B;
    .locals 0

    invoke-direct {p0}, Le/a/d/a;->h()Le/B;

    move-result-object p0

    return-object p0
.end method

.method private final g()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Le/a/d/a;->g:Lf/k;

    iget-wide v1, p0, Le/a/d/a;->c:J

    invoke-interface {v0, v1, v2}, Lf/k;->c(J)Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Le/a/d/a;->c:J

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    int-to-long v3, v3

    sub-long/2addr v1, v3

    iput-wide v1, p0, Le/a/d/a;->c:J

    return-object v0
.end method

.method private final h()Le/B;
    .locals 3

    new-instance v0, Le/B$a;

    invoke-direct {v0}, Le/B$a;-><init>()V

    invoke-direct {p0}, Le/a/d/a;->g()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Le/B$a;->a(Ljava/lang/String;)Le/B$a;

    invoke-direct {p0}, Le/a/d/a;->g()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Le/B$a;->a()Le/B;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Z)Le/L$a;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget v0, p0, Le/a/d/a;->b:I

    const/4 v1, 0x3

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :cond_1
    :goto_0
    if-eqz v2, :cond_5

    :try_start_0
    sget-object v0, Le/a/c/l;->a:Le/a/c/l$a;

    invoke-direct {p0}, Le/a/d/a;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Le/a/c/l$a;->a(Ljava/lang/String;)Le/a/c/l;

    move-result-object v0

    new-instance v2, Le/L$a;

    invoke-direct {v2}, Le/L$a;-><init>()V

    iget-object v3, v0, Le/a/c/l;->b:Le/G;

    invoke-virtual {v2, v3}, Le/L$a;->a(Le/G;)Le/L$a;

    iget v3, v0, Le/a/c/l;->c:I

    invoke-virtual {v2, v3}, Le/L$a;->a(I)Le/L$a;

    iget-object v3, v0, Le/a/c/l;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Le/L$a;->a(Ljava/lang/String;)Le/L$a;

    invoke-direct {p0}, Le/a/d/a;->h()Le/B;

    move-result-object v3

    invoke-virtual {v2, v3}, Le/L$a;->a(Le/B;)Le/L$a;

    const/16 v3, 0x64

    if-eqz p1, :cond_2

    iget p1, v0, Le/a/c/l;->c:I

    if-ne p1, v3, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    iget p1, v0, Le/a/c/l;->c:I

    if-ne p1, v3, :cond_3

    iput v1, p0, Le/a/d/a;->b:I

    goto :goto_1

    :cond_3
    const/4 p1, 0x4

    iput p1, p0, Le/a/d/a;->b:I
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object v2

    :catch_0
    move-exception p1

    iget-object v0, p0, Le/a/d/a;->f:Le/a/b/e;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Le/a/b/e;->j()Le/O;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Le/O;->a()Le/a;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Le/a;->k()Le/C;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Le/C;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    goto :goto_2

    :cond_4
    const-string v0, "unknown"

    :goto_2
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unexpected end of stream on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_5
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "state: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Le/a/d/a;->b:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Le/I;J)Lf/A;
    .locals 2
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Le/I;->a()Le/K;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Le/I;->a()Le/K;

    move-result-object v0

    invoke-virtual {v0}, Le/K;->c()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/net/ProtocolException;

    const-string p2, "Duplex connections are not supported for HTTP/1"

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Le/a/d/a;->b(Le/I;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-direct {p0}, Le/a/d/a;->d()Lf/A;

    move-result-object p1

    goto :goto_1

    :cond_2
    const-wide/16 v0, -0x1

    cmp-long p1, p2, v0

    if-eqz p1, :cond_3

    invoke-direct {p0}, Le/a/d/a;->e()Lf/A;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot stream a request body without chunked encoding or a known content length!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Le/L;)Lf/C;
    .locals 4
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Le/a/c/f;->a(Le/L;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Le/a/d/a;->a(J)Lf/C;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Le/a/d/a;->d(Le/L;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object p1

    invoke-virtual {p1}, Le/I;->h()Le/C;

    move-result-object p1

    invoke-direct {p0, p1}, Le/a/d/a;->a(Le/C;)Lf/C;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Le/a/d;->a(Le/L;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long p1, v0, v2

    if-eqz p1, :cond_2

    invoke-direct {p0, v0, v1}, Le/a/d/a;->a(J)Lf/C;

    move-result-object p1

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Le/a/d/a;->f()Lf/C;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Le/a/d/a;->h:Lf/j;

    invoke-interface {v0}, Lf/j;->flush()V

    return-void
.end method

.method public final a(Le/B;Ljava/lang/String;)V
    .locals 5
    .param p1    # Le/B;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "headers"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestLine"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Le/a/d/a;->b:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Le/a/d/a;->h:Lf/j;

    invoke-interface {v0, p2}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object p2

    const-string v0, "\r\n"

    invoke-interface {p2, v0}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    invoke-virtual {p1}, Le/B;->size()I

    move-result p2

    :goto_1
    if-ge v1, p2, :cond_1

    iget-object v3, p0, Le/a/d/a;->h:Lf/j;

    invoke-virtual {p1, v1}, Le/B;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v3

    const-string v4, ": "

    invoke-interface {v3, v4}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v3

    invoke-virtual {p1, v1}, Le/B;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v3

    invoke-interface {v3, v0}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object p1, p0, Le/a/d/a;->h:Lf/j;

    invoke-interface {p1, v0}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    iput v2, p0, Le/a/d/a;->b:I

    return-void

    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "state: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Le/a/d/a;->b:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public a(Le/I;)V
    .locals 3
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Le/a/c/j;->a:Le/a/c/j;

    iget-object v1, p0, Le/a/d/a;->f:Le/a/b/e;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Le/a/b/e;->j()Le/O;

    move-result-object v1

    invoke-virtual {v1}, Le/O;->b()Ljava/net/Proxy;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    const-string v2, "realConnection!!.route().proxy.type()"

    invoke-static {v1, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Le/a/c/j;->a(Le/I;Ljava/net/Proxy$Type;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Le/I;->d()Le/B;

    move-result-object p1

    invoke-virtual {p0, p1, v0}, Le/a/d/a;->a(Le/B;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1
.end method

.method public b(Le/L;)J
    .locals 2
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Le/a/c/f;->a(Le/L;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Le/a/d/a;->d(Le/L;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Le/a/d;->a(Le/L;)J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public b()Le/a/b/e;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/a/d/a;->f:Le/a/b/e;

    return-object v0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Le/a/d/a;->h:Lf/j;

    invoke-interface {v0}, Lf/j;->flush()V

    return-void
.end method

.method public final c(Le/L;)V
    .locals 4
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Le/a/d;->a(Le/L;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, v0, v1}, Le/a/d/a;->a(J)Lf/C;

    move-result-object p1

    const v0, 0x7fffffff

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p1, v0, v1}, Le/a/d;->b(Lf/C;ILjava/util/concurrent/TimeUnit;)Z

    invoke-interface {p1}, Lf/C;->close()V

    return-void
.end method

.method public cancel()V
    .locals 1

    iget-object v0, p0, Le/a/d/a;->f:Le/a/b/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Le/a/b/e;->a()V

    :cond_0
    return-void
.end method
