.class final Le/a/d/a$c;
.super Le/a/d/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field private d:J

.field private e:Z

.field private final f:Le/C;

.field final synthetic g:Le/a/d/a;


# direct methods
.method public constructor <init>(Le/a/d/a;Le/C;)V
    .locals 1
    .param p1    # Le/a/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le/C;",
            ")V"
        }
    .end annotation

    const-string v0, "url"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Le/a/d/a$c;->g:Le/a/d/a;

    invoke-direct {p0, p1}, Le/a/d/a$a;-><init>(Le/a/d/a;)V

    iput-object p2, p0, Le/a/d/a$c;->f:Le/C;

    const-wide/16 p1, -0x1

    iput-wide p1, p0, Le/a/d/a$c;->d:J

    const/4 p1, 0x1

    iput-boolean p1, p0, Le/a/d/a$c;->e:Z

    return-void
.end method

.method private final e()V
    .locals 7

    iget-wide v0, p0, Le/a/d/a$c;->d:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/a/d/a$c;->g:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->d(Le/a/d/a;)Lf/k;

    move-result-object v0

    invoke-interface {v0}, Lf/k;->g()Ljava/lang/String;

    :cond_0
    :try_start_0
    iget-object v0, p0, Le/a/d/a$c;->g:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->d(Le/a/d/a;)Lf/k;

    move-result-object v0

    invoke-interface {v0}, Lf/k;->h()J

    move-result-wide v0

    iput-wide v0, p0, Le/a/d/a$c;->d:J

    iget-object v0, p0, Le/a/d/a$c;->g:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->d(Le/a/d/a;)Lf/k;

    move-result-object v0

    invoke-interface {v0}, Lf/k;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-static {v0}, Lkotlin/g/g;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Le/a/d/a$c;->d:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-ltz v1, :cond_6

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x0

    if-lez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    const/4 v5, 0x0

    if-eqz v1, :cond_2

    const-string v1, ";"

    const/4 v6, 0x2

    invoke-static {v0, v1, v2, v6, v5}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_6

    :cond_2
    iget-wide v0, p0, Le/a/d/a$c;->d:J

    cmp-long v0, v0, v3

    if-nez v0, :cond_5

    iput-boolean v2, p0, Le/a/d/a$c;->e:Z

    iget-object v0, p0, Le/a/d/a$c;->g:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->g(Le/a/d/a;)Le/B;

    move-result-object v1

    invoke-static {v0, v1}, Le/a/d/a;->a(Le/a/d/a;Le/B;)V

    iget-object v0, p0, Le/a/d/a$c;->g:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->a(Le/a/d/a;)Le/F;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Le/F;->j()Le/s;

    move-result-object v0

    iget-object v1, p0, Le/a/d/a$c;->f:Le/C;

    iget-object v2, p0, Le/a/d/a$c;->g:Le/a/d/a;

    invoke-static {v2}, Le/a/d/a;->f(Le/a/d/a;)Le/B;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {v0, v1, v2}, Le/a/c/f;->a(Le/s;Le/C;Le/B;)V

    invoke-virtual {p0}, Le/a/d/a$a;->b()V

    goto :goto_1

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_5
    :goto_1
    return-void

    :cond_6
    :try_start_1
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "expected chunk size and optional extensions"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " but was \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Le/a/d/a$c;->d:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    new-instance v0, Lkotlin/o;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/net/ProtocolException;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public b(Lf/h;J)J
    .locals 7
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ltz v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    :goto_0
    if-eqz v2, :cond_8

    invoke-virtual {p0}, Le/a/d/a$a;->a()Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    move v3, v4

    :goto_1
    if-eqz v3, :cond_7

    iget-boolean v2, p0, Le/a/d/a$c;->e:Z

    const-wide/16 v3, -0x1

    if-nez v2, :cond_2

    return-wide v3

    :cond_2
    iget-wide v5, p0, Le/a/d/a$c;->d:J

    cmp-long v0, v5, v0

    if-eqz v0, :cond_3

    cmp-long v0, v5, v3

    if-nez v0, :cond_4

    :cond_3
    invoke-direct {p0}, Le/a/d/a$c;->e()V

    iget-boolean v0, p0, Le/a/d/a$c;->e:Z

    if-nez v0, :cond_4

    return-wide v3

    :cond_4
    iget-wide v0, p0, Le/a/d/a$c;->d:J

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    invoke-super {p0, p1, p2, p3}, Le/a/d/a$a;->b(Lf/h;J)J

    move-result-wide p1

    cmp-long p3, p1, v3

    if-nez p3, :cond_6

    iget-object p1, p0, Le/a/d/a$c;->g:Le/a/d/a;

    invoke-static {p1}, Le/a/d/a;->b(Le/a/d/a;)Le/a/b/e;

    move-result-object p1

    if-nez p1, :cond_5

    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1

    :cond_5
    invoke-virtual {p1}, Le/a/b/e;->i()V

    new-instance p1, Ljava/net/ProtocolException;

    const-string p2, "unexpected end of stream"

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Le/a/d/a$a;->b()V

    throw p1

    :cond_6
    iget-wide v0, p0, Le/a/d/a$c;->d:J

    sub-long/2addr v0, p1

    iput-wide v0, p0, Le/a/d/a$c;->d:J

    return-wide p1

    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "byteCount < 0: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public close()V
    .locals 2

    invoke-virtual {p0}, Le/a/d/a$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Le/a/d/a$c;->e:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x64

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p0, v0, v1}, Le/a/d;->a(Lf/C;ILjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Le/a/d/a$c;->g:Le/a/d/a;

    invoke-static {v0}, Le/a/d/a;->b(Le/a/d/a;)Le/a/b/e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Le/a/b/e;->i()V

    invoke-virtual {p0}, Le/a/d/a$a;->b()V

    goto :goto_0

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0

    :cond_2
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Le/a/d/a$a;->a(Z)V

    return-void
.end method
