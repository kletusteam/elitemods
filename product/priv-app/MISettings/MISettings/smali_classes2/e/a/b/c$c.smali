.class public final Le/a/b/c$c;
.super Lf/n;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/b/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "c"
.end annotation


# instance fields
.field private b:J

.field private c:Z

.field private d:Z

.field private final e:J

.field final synthetic f:Le/a/b/c;


# direct methods
.method public constructor <init>(Le/a/b/c;Lf/C;J)V
    .locals 1
    .param p1    # Le/a/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/C;",
            "J)V"
        }
    .end annotation

    const-string v0, "delegate"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Le/a/b/c$c;->f:Le/a/b/c;

    invoke-direct {p0, p2}, Lf/n;-><init>(Lf/C;)V

    iput-wide p3, p0, Le/a/b/c$c;->e:J

    iget-wide p1, p0, Le/a/b/c$c;->e:J

    const-wide/16 p3, 0x0

    cmp-long p1, p1, p3

    if-nez p1, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Le/a/b/c$c;->a(Ljava/io/IOException;)Ljava/io/IOException;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(TE;)TE;"
        }
    .end annotation

    iget-boolean v0, p0, Le/a/b/c$c;->c:Z

    if-eqz v0, :cond_0

    return-object p1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Le/a/b/c$c;->c:Z

    iget-object v1, p0, Le/a/b/c$c;->f:Le/a/b/c;

    iget-wide v2, p0, Le/a/b/c$c;->b:J

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Le/a/b/c;->a(JZZLjava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    return-object p1
.end method

.method public b(Lf/h;J)J
    .locals 7
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Le/a/b/c$c;->d:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    :try_start_0
    invoke-virtual {p0}, Lf/n;->a()Lf/C;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lf/C;->b(Lf/h;J)J

    move-result-wide p1

    const-wide/16 v0, -0x1

    cmp-long p3, p1, v0

    const/4 v2, 0x0

    if-nez p3, :cond_0

    invoke-virtual {p0, v2}, Le/a/b/c$c;->a(Ljava/io/IOException;)Ljava/io/IOException;

    return-wide v0

    :cond_0
    iget-wide v3, p0, Le/a/b/c$c;->b:J

    add-long/2addr v3, p1

    iget-wide v5, p0, Le/a/b/c$c;->e:J

    cmp-long p3, v5, v0

    if-eqz p3, :cond_2

    iget-wide v0, p0, Le/a/b/c$c;->e:J

    cmp-long p3, v3, v0

    if-gtz p3, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/net/ProtocolException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "expected "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Le/a/b/c$c;->e:J

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p3, " bytes but received "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_0
    iput-wide v3, p0, Le/a/b/c$c;->b:J

    iget-wide v0, p0, Le/a/b/c$c;->e:J

    cmp-long p3, v3, v0

    if-nez p3, :cond_3

    invoke-virtual {p0, v2}, Le/a/b/c$c;->a(Ljava/io/IOException;)Ljava/io/IOException;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    return-wide p1

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1}, Le/a/b/c$c;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Le/a/b/c$c;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Le/a/b/c$c;->d:Z

    :try_start_0
    invoke-super {p0}, Lf/n;->close()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Le/a/b/c$c;->a(Ljava/io/IOException;)Ljava/io/IOException;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Le/a/b/c$c;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method
