.class public final Le/a/b/a;
.super Ljava/lang/Object;

# interfaces
.implements Le/D;


# static fields
.field public static final a:Le/a/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Le/a/b/a;

    invoke-direct {v0}, Le/a/b/a;-><init>()V

    sput-object v0, Le/a/b/a;->a:Le/a/b/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Le/D$a;)Le/L;
    .locals 5
    .param p1    # Le/D$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "chain"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Le/a/c/h;

    invoke-virtual {v0}, Le/a/c/h;->d()Le/I;

    move-result-object v1

    invoke-virtual {v0}, Le/a/c/h;->f()Le/a/b/n;

    move-result-object v2

    invoke-virtual {v1}, Le/I;->f()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GET"

    invoke-static {v3, v4}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, p1, v3}, Le/a/b/n;->a(Le/D$a;Z)Le/a/b/c;

    move-result-object p1

    invoke-virtual {v0, v1, v2, p1}, Le/a/c/h;->a(Le/I;Le/a/b/n;Le/a/b/c;)Le/L;

    move-result-object p1

    return-object p1
.end method
