.class public final Le/a/b/n;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/a/b/n$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u007f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006*\u0001 \u0018\u00002\u00020\u0001:\u0001FB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010#\u001a\u00020$2\u0006\u0010\n\u001a\u00020\u000bJ\u0006\u0010%\u001a\u00020$J\u0006\u0010&\u001a\u00020\tJ\u0006\u0010\'\u001a\u00020$J\u0010\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+H\u0002J\u0006\u0010,\u001a\u00020$J;\u0010-\u001a\u0002H.\"\n\u0008\u0000\u0010.*\u0004\u0018\u00010/2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u00100\u001a\u00020\t2\u0006\u00101\u001a\u00020\t2\u0006\u00102\u001a\u0002H.H\u0000\u00a2\u0006\u0004\u00083\u00104J\u0006\u00105\u001a\u00020\tJ)\u00106\u001a\u0002H.\"\n\u0008\u0000\u0010.*\u0004\u0018\u00010/2\u0006\u00102\u001a\u0002H.2\u0006\u00107\u001a\u00020\tH\u0002\u00a2\u0006\u0002\u00108J\u001d\u00109\u001a\u00020\u00152\u0006\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020\tH\u0000\u00a2\u0006\u0002\u0008=J\u0012\u0010\u001c\u001a\u0004\u0018\u00010/2\u0008\u00102\u001a\u0004\u0018\u00010/J\u000e\u0010>\u001a\u00020$2\u0006\u0010\u001d\u001a\u00020\u001eJ\u0008\u0010?\u001a\u0004\u0018\u00010@J\u0006\u0010\u001f\u001a\u00020AJ\u0006\u0010\"\u001a\u00020$J\u0006\u0010B\u001a\u00020$J!\u0010C\u001a\u0002H.\"\n\u0008\u0000\u0010.*\u0004\u0018\u00010/2\u0006\u0010D\u001a\u0002H.H\u0002\u00a2\u0006\u0002\u0010ER\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0001X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u001a\u001a\u00020\t8F\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u001bR\u000e\u0010\u001c\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010!R\u000e\u0010\"\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006G"
    }
    d2 = {
        "Lokhttp3/internal/connection/Transmitter;",
        "",
        "client",
        "Lokhttp3/OkHttpClient;",
        "call",
        "Lokhttp3/Call;",
        "(Lokhttp3/OkHttpClient;Lokhttp3/Call;)V",
        "callStackTrace",
        "canceled",
        "",
        "connection",
        "Lokhttp3/internal/connection/RealConnection;",
        "getConnection",
        "()Lokhttp3/internal/connection/RealConnection;",
        "setConnection",
        "(Lokhttp3/internal/connection/RealConnection;)V",
        "connectionPool",
        "Lokhttp3/internal/connection/RealConnectionPool;",
        "eventListener",
        "Lokhttp3/EventListener;",
        "exchange",
        "Lokhttp3/internal/connection/Exchange;",
        "exchangeFinder",
        "Lokhttp3/internal/connection/ExchangeFinder;",
        "exchangeRequestDone",
        "exchangeResponseDone",
        "isCanceled",
        "()Z",
        "noMoreExchanges",
        "request",
        "Lokhttp3/Request;",
        "timeout",
        "okhttp3/internal/connection/Transmitter$timeout$1",
        "Lokhttp3/internal/connection/Transmitter$timeout$1;",
        "timeoutEarlyExit",
        "acquireConnectionNoEvents",
        "",
        "callStart",
        "canRetry",
        "cancel",
        "createAddress",
        "Lokhttp3/Address;",
        "url",
        "Lokhttp3/HttpUrl;",
        "exchangeDoneDueToException",
        "exchangeMessageDone",
        "E",
        "Ljava/io/IOException;",
        "requestDone",
        "responseDone",
        "e",
        "exchangeMessageDone$okhttp",
        "(Lokhttp3/internal/connection/Exchange;ZZLjava/io/IOException;)Ljava/io/IOException;",
        "hasExchange",
        "maybeReleaseConnection",
        "force",
        "(Ljava/io/IOException;Z)Ljava/io/IOException;",
        "newExchange",
        "chain",
        "Lokhttp3/Interceptor$Chain;",
        "doExtensiveHealthChecks",
        "newExchange$okhttp",
        "prepareToConnect",
        "releaseConnectionNoEvents",
        "Ljava/net/Socket;",
        "Lokio/Timeout;",
        "timeoutEnter",
        "timeoutExit",
        "cause",
        "(Ljava/io/IOException;)Ljava/io/IOException;",
        "TransmitterReference",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private final a:Le/a/b/i;

.field private final b:Le/x;

.field private final c:Le/a/b/o;

.field private d:Ljava/lang/Object;

.field private e:Le/I;

.field private f:Le/a/b/d;

.field private g:Le/a/b/e;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private h:Le/a/b/c;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private final n:Le/F;

.field private final o:Le/h;


# direct methods
.method public constructor <init>(Le/F;Le/h;)V
    .locals 2
    .param p1    # Le/F;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "client"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "call"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/a/b/n;->n:Le/F;

    iput-object p2, p0, Le/a/b/n;->o:Le/h;

    iget-object p1, p0, Le/a/b/n;->n:Le/F;

    invoke-virtual {p1}, Le/F;->h()Le/o;

    move-result-object p1

    invoke-virtual {p1}, Le/o;->a()Le/a/b/i;

    move-result-object p1

    iput-object p1, p0, Le/a/b/n;->a:Le/a/b/i;

    iget-object p1, p0, Le/a/b/n;->n:Le/F;

    invoke-virtual {p1}, Le/F;->m()Le/x$b;

    move-result-object p1

    iget-object p2, p0, Le/a/b/n;->o:Le/h;

    invoke-interface {p1, p2}, Le/x$b;->a(Le/h;)Le/x;

    move-result-object p1

    iput-object p1, p0, Le/a/b/n;->b:Le/x;

    new-instance p1, Le/a/b/o;

    invoke-direct {p1, p0}, Le/a/b/o;-><init>(Le/a/b/n;)V

    iget-object p2, p0, Le/a/b/n;->n:Le/F;

    invoke-virtual {p2}, Le/F;->e()I

    move-result p2

    int-to-long v0, p2

    sget-object p2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, p2}, Lf/E;->a(JLjava/util/concurrent/TimeUnit;)Lf/E;

    iput-object p1, p0, Le/a/b/n;->c:Le/a/b/o;

    return-void
.end method

.method private final a(Le/C;)Le/a;
    .locals 17

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Le/C;->i()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v0, Le/a/b/n;->n:Le/F;

    invoke-virtual {v1}, Le/F;->A()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    iget-object v1, v0, Le/a/b/n;->n:Le/F;

    invoke-virtual {v1}, Le/F;->p()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v1

    iget-object v3, v0, Le/a/b/n;->n:Le/F;

    invoke-virtual {v3}, Le/F;->f()Le/j;

    move-result-object v3

    move-object v10, v1

    move-object v9, v2

    move-object v11, v3

    goto :goto_0

    :cond_0
    move-object v9, v2

    move-object v10, v9

    move-object v11, v10

    :goto_0
    new-instance v1, Le/a;

    invoke-virtual/range {p1 .. p1}, Le/C;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Le/C;->k()I

    move-result v6

    iget-object v2, v0, Le/a/b/n;->n:Le/F;

    invoke-virtual {v2}, Le/F;->l()Le/v;

    move-result-object v7

    iget-object v2, v0, Le/a/b/n;->n:Le/F;

    invoke-virtual {v2}, Le/F;->z()Ljavax/net/SocketFactory;

    move-result-object v8

    iget-object v2, v0, Le/a/b/n;->n:Le/F;

    invoke-virtual {v2}, Le/F;->v()Le/c;

    move-result-object v12

    iget-object v2, v0, Le/a/b/n;->n:Le/F;

    invoke-virtual {v2}, Le/F;->u()Ljava/net/Proxy;

    move-result-object v13

    iget-object v2, v0, Le/a/b/n;->n:Le/F;

    invoke-virtual {v2}, Le/F;->t()Ljava/util/List;

    move-result-object v14

    iget-object v2, v0, Le/a/b/n;->n:Le/F;

    invoke-virtual {v2}, Le/F;->i()Ljava/util/List;

    move-result-object v15

    iget-object v2, v0, Le/a/b/n;->n:Le/F;

    invoke-virtual {v2}, Le/F;->w()Ljava/net/ProxySelector;

    move-result-object v16

    move-object v4, v1

    invoke-direct/range {v4 .. v16}, Le/a;-><init>(Ljava/lang/String;ILe/v;Ljavax/net/SocketFactory;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Le/j;Le/c;Ljava/net/Proxy;Ljava/util/List;Ljava/util/List;Ljava/net/ProxySelector;)V

    return-object v1
.end method

.method private final a(Ljava/io/IOException;Z)Ljava/io/IOException;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(TE;Z)TE;"
        }
    .end annotation

    new-instance v0, Lkotlin/jvm/b/o;

    invoke-direct {v0}, Lkotlin/jvm/b/o;-><init>()V

    iget-object v1, p0, Le/a/b/n;->a:Le/a/b/i;

    monitor-enter v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz p2, :cond_1

    :try_start_0
    iget-object v4, p0, Le/a/b/n;->h:Le/a/b/c;

    if-nez v4, :cond_0

    goto :goto_0

    :cond_0
    move v4, v2

    goto :goto_1

    :catchall_0
    move-exception p1

    goto/16 :goto_6

    :cond_1
    :goto_0
    move v4, v3

    :goto_1
    if-eqz v4, :cond_d

    iget-object v4, p0, Le/a/b/n;->g:Le/a/b/e;

    iput-object v4, v0, Lkotlin/jvm/b/o;->a:Ljava/lang/Object;

    iget-object v4, p0, Le/a/b/n;->g:Le/a/b/e;

    const/4 v5, 0x0

    if-eqz v4, :cond_3

    iget-object v4, p0, Le/a/b/n;->h:Le/a/b/c;

    if-nez v4, :cond_3

    if-nez p2, :cond_2

    iget-boolean p2, p0, Le/a/b/n;->m:Z

    if-eqz p2, :cond_3

    :cond_2
    invoke-virtual {p0}, Le/a/b/n;->h()Ljava/net/Socket;

    move-result-object p2

    goto :goto_2

    :cond_3
    move-object p2, v5

    :goto_2
    iget-object v4, p0, Le/a/b/n;->g:Le/a/b/e;

    if-eqz v4, :cond_4

    iput-object v5, v0, Lkotlin/jvm/b/o;->a:Ljava/lang/Object;

    :cond_4
    iget-boolean v4, p0, Le/a/b/n;->m:Z

    if-eqz v4, :cond_5

    iget-object v4, p0, Le/a/b/n;->h:Le/a/b/c;

    if-nez v4, :cond_5

    move v4, v3

    goto :goto_3

    :cond_5
    move v4, v2

    :goto_3
    sget-object v6, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    if-eqz p2, :cond_6

    invoke-static {p2}, Le/a/d;->a(Ljava/net/Socket;)V

    :cond_6
    iget-object p2, v0, Lkotlin/jvm/b/o;->a:Ljava/lang/Object;

    move-object v0, p2

    check-cast v0, Le/n;

    if-eqz v0, :cond_8

    iget-object v0, p0, Le/a/b/n;->b:Le/x;

    iget-object v1, p0, Le/a/b/n;->o:Le/h;

    check-cast p2, Le/n;

    if-eqz p2, :cond_7

    invoke-virtual {v0, v1, p2}, Le/x;->b(Le/h;Le/n;)V

    goto :goto_4

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_8
    :goto_4
    if-eqz v4, :cond_c

    if-eqz p1, :cond_9

    move v2, v3

    :cond_9
    invoke-direct {p0, p1}, Le/a/b/n;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    if-eqz v2, :cond_b

    iget-object p2, p0, Le/a/b/n;->b:Le/x;

    iget-object v0, p0, Le/a/b/n;->o:Le/h;

    if-eqz p1, :cond_a

    invoke-virtual {p2, v0, p1}, Le/x;->a(Le/h;Ljava/io/IOException;)V

    goto :goto_5

    :cond_a
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_b
    iget-object p2, p0, Le/a/b/n;->b:Le/x;

    iget-object v0, p0, Le/a/b/n;->o:Le/h;

    invoke-virtual {p2, v0}, Le/x;->a(Le/h;)V

    :cond_c
    :goto_5
    return-object p1

    :cond_d
    :try_start_1
    const-string p1, "cannot release connection while it is in use"

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_6
    monitor-exit v1

    throw p1
.end method

.method private final b(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(TE;)TE;"
        }
    .end annotation

    iget-boolean v0, p0, Le/a/b/n;->l:Z

    if-eqz v0, :cond_0

    return-object p1

    :cond_0
    iget-object v0, p0, Le/a/b/n;->c:Le/a/b/o;

    invoke-virtual {v0}, Lf/d;->k()Z

    move-result v0

    if-nez v0, :cond_1

    return-object p1

    :cond_1
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_2

    invoke-virtual {v0, p1}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    :cond_2
    return-object v0
.end method


# virtual methods
.method public final a(Le/D$a;Z)Le/a/b/c;
    .locals 10
    .param p1    # Le/D$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "chain"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/b/n;->a:Le/a/b/i;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Le/a/b/n;->m:Z

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_4

    iget-object v1, p0, Le/a/b/n;->h:Le/a/b/c;

    const/4 v3, 0x0

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    if-eqz v2, :cond_3

    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-exit v0

    iget-object v0, p0, Le/a/b/n;->f:Le/a/b/d;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v2, p0, Le/a/b/n;->n:Le/F;

    invoke-virtual {v0, v2, p1, p2}, Le/a/b/d;->a(Le/F;Le/D$a;Z)Le/a/c/e;

    move-result-object v9

    new-instance p1, Le/a/b/c;

    iget-object v6, p0, Le/a/b/n;->o:Le/h;

    iget-object v7, p0, Le/a/b/n;->b:Le/x;

    iget-object v8, p0, Le/a/b/n;->f:Le/a/b/d;

    if-eqz v8, :cond_1

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v4 .. v9}, Le/a/b/c;-><init>(Le/a/b/n;Le/h;Le/x;Le/a/b/d;Le/a/c/e;)V

    iget-object p2, p0, Le/a/b/n;->a:Le/a/b/i;

    monitor-enter p2

    :try_start_1
    iput-object p1, p0, Le/a/b/n;->h:Le/a/b/c;

    iput-boolean v3, p0, Le/a/b/n;->i:Z

    iput-boolean v3, p0, Le/a/b/n;->j:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p2

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p2

    throw p1

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_3
    :try_start_2
    const-string p1, "cannot make a new request because the previous response is still open: please call response.close()"

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_4
    const-string p1, "released"

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public final a(Le/a/b/c;ZZLjava/io/IOException;)Ljava/io/IOException;
    .locals 3
    .param p1    # Le/a/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(",
            "Le/a/b/c;",
            "ZZTE;)TE;"
        }
    .end annotation

    const-string v0, "exchange"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/b/n;->a:Le/a/b/i;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Le/a/b/n;->h:Le/a/b/c;

    invoke-static {p1, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    xor-int/2addr p1, v1

    if-eqz p1, :cond_0

    monitor-exit v0

    return-object p4

    :cond_0
    const/4 p1, 0x0

    if-eqz p2, :cond_1

    :try_start_1
    iget-boolean p2, p0, Le/a/b/n;->i:Z

    xor-int/2addr p2, v1

    iput-boolean v1, p0, Le/a/b/n;->i:Z

    goto :goto_0

    :cond_1
    move p2, p1

    :goto_0
    if-eqz p3, :cond_3

    iget-boolean p3, p0, Le/a/b/n;->j:Z

    if-nez p3, :cond_2

    move p2, v1

    :cond_2
    iput-boolean v1, p0, Le/a/b/n;->j:Z

    :cond_3
    iget-boolean p3, p0, Le/a/b/n;->i:Z

    if-eqz p3, :cond_6

    iget-boolean p3, p0, Le/a/b/n;->j:Z

    if-eqz p3, :cond_6

    if-eqz p2, :cond_6

    iget-object p2, p0, Le/a/b/n;->h:Le/a/b/c;

    const/4 p3, 0x0

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Le/a/b/c;->b()Le/a/b/e;

    move-result-object p2

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Le/a/b/e;->e()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p2, v2}, Le/a/b/e;->a(I)V

    iput-object p3, p0, Le/a/b/n;->h:Le/a/b/c;

    goto :goto_1

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p3

    :cond_5
    :try_start_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p3

    :cond_6
    move v1, p1

    :goto_1
    :try_start_3
    sget-object p2, Lkotlin/r;->a:Lkotlin/r;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v0

    if-eqz v1, :cond_7

    invoke-direct {p0, p4, p1}, Le/a/b/n;->a(Ljava/io/IOException;Z)Ljava/io/IOException;

    move-result-object p4

    :cond_7
    return-object p4

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public final a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2
    .param p1    # Ljava/io/IOException;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/a/b/n;->a:Le/a/b/i;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Le/a/b/n;->m:Z

    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Le/a/b/n;->a(Ljava/io/IOException;Z)Ljava/io/IOException;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public final a()V
    .locals 2

    sget-object v0, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v0}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v0

    const-string v1, "response.body().close()"

    invoke-virtual {v0, v1}, Le/a/g/g;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Le/a/b/n;->d:Ljava/lang/Object;

    iget-object v0, p0, Le/a/b/n;->b:Le/x;

    iget-object v1, p0, Le/a/b/n;->o:Le/h;

    invoke-virtual {v0, v1}, Le/x;->b(Le/h;)V

    return-void
.end method

.method public final a(Le/I;)V
    .locals 7
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/b/n;->e:Le/I;

    if-eqz v0, :cond_5

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Le/I;->h()Le/C;

    move-result-object v0

    invoke-virtual {p1}, Le/I;->h()Le/C;

    move-result-object v2

    invoke-static {v0, v2}, Le/a/d;->a(Le/C;Le/C;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Le/a/b/n;->f:Le/a/b/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Le/a/b/d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_1
    iget-object v0, p0, Le/a/b/n;->h:Le/a/b/c;

    const/4 v2, 0x1

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Le/a/b/n;->f:Le/a/b/d;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1, v2}, Le/a/b/n;->a(Ljava/io/IOException;Z)Ljava/io/IOException;

    iput-object v1, p0, Le/a/b/n;->f:Le/a/b/d;

    goto :goto_1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Check failed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_5
    :goto_1
    iput-object p1, p0, Le/a/b/n;->e:Le/I;

    new-instance v0, Le/a/b/d;

    iget-object v3, p0, Le/a/b/n;->a:Le/a/b/i;

    invoke-virtual {p1}, Le/I;->h()Le/C;

    move-result-object p1

    invoke-direct {p0, p1}, Le/a/b/n;->a(Le/C;)Le/a;

    move-result-object v4

    iget-object v5, p0, Le/a/b/n;->o:Le/h;

    iget-object v6, p0, Le/a/b/n;->b:Le/x;

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Le/a/b/d;-><init>(Le/a/b/n;Le/a/b/i;Le/a;Le/h;Le/x;)V

    iput-object v0, p0, Le/a/b/n;->f:Le/a/b/d;

    return-void
.end method

.method public final a(Le/a/b/e;)V
    .locals 2
    .param p1    # Le/a/b/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "connection"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/b/n;->a:Le/a/b/i;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    sget-boolean v1, Lkotlin/s;->a:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Assertion failed"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Le/a/b/n;->g:Le/a/b/e;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    iput-object p1, p0, Le/a/b/n;->g:Le/a/b/e;

    invoke-virtual {p1}, Le/a/b/e;->f()Ljava/util/List;

    move-result-object p1

    new-instance v0, Le/a/b/n$a;

    iget-object v1, p0, Le/a/b/n;->d:Ljava/lang/Object;

    invoke-direct {v0, p0, v1}, Le/a/b/n$a;-><init>(Le/a/b/n;Ljava/lang/Object;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Check failed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final b()Z
    .locals 2

    iget-object v0, p0, Le/a/b/n;->f:Le/a/b/d;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Le/a/b/d;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Le/a/b/n;->f:Le/a/b/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Le/a/b/d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1
.end method

.method public final c()V
    .locals 4

    iget-object v0, p0, Le/a/b/n;->a:Le/a/b/i;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Le/a/b/n;->k:Z

    iget-object v1, p0, Le/a/b/n;->h:Le/a/b/c;

    iget-object v2, p0, Le/a/b/n;->f:Le/a/b/d;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Le/a/b/d;->a()Le/a/b/e;

    move-result-object v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v2, p0, Le/a/b/n;->g:Le/a/b/e;

    :goto_0
    sget-object v3, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Le/a/b/c;->a()V

    goto :goto_1

    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Le/a/b/e;->a()V

    :cond_2
    :goto_1
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final d()V
    .locals 3

    iget-object v0, p0, Le/a/b/n;->a:Le/a/b/i;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Le/a/b/n;->m:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Le/a/b/n;->h:Le/a/b/c;

    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :cond_0
    :try_start_1
    const-string v1, "Check failed."

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final e()Le/a/b/e;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/a/b/n;->g:Le/a/b/e;

    return-object v0
.end method

.method public final f()Z
    .locals 2

    iget-object v0, p0, Le/a/b/n;->a:Le/a/b/i;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Le/a/b/n;->h:Le/a/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final g()Z
    .locals 2

    iget-object v0, p0, Le/a/b/n;->a:Le/a/b/i;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Le/a/b/n;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final h()Ljava/net/Socket;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/a/b/n;->a:Le/a/b/i;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    sget-boolean v1, Lkotlin/s;->a:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Assertion failed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Le/a/b/n;->g:Le/a/b/e;

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Le/a/b/e;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, -0x1

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/Reference;

    invoke-virtual {v4}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Le/a/b/n;

    invoke-static {v4, p0}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move v3, v5

    :goto_2
    if-eq v3, v5, :cond_4

    const/4 v2, 0x1

    :cond_4
    if-eqz v2, :cond_7

    iget-object v0, p0, Le/a/b/n;->g:Le/a/b/e;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Le/a/b/e;->f()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iput-object v1, p0, Le/a/b/n;->g:Le/a/b/e;

    invoke-virtual {v0}, Le/a/b/e;->f()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Le/a/b/e;->a(J)V

    iget-object v2, p0, Le/a/b/n;->a:Le/a/b/i;

    invoke-virtual {v2, v0}, Le/a/b/i;->a(Le/a/b/e;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Le/a/b/e;->k()Ljava/net/Socket;

    move-result-object v0

    return-object v0

    :cond_5
    return-object v1

    :cond_6
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Check failed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1
.end method

.method public final i()V
    .locals 2

    iget-boolean v0, p0, Le/a/b/n;->l:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Le/a/b/n;->l:Z

    iget-object v0, p0, Le/a/b/n;->c:Le/a/b/o;

    invoke-virtual {v0}, Lf/d;->k()Z

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Check failed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final j()V
    .locals 1

    iget-object v0, p0, Le/a/b/n;->c:Le/a/b/o;

    invoke-virtual {v0}, Lf/d;->j()V

    return-void
.end method
