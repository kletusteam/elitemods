.class public final Le/a/b/e;
.super Le/a/e/g$c;

# interfaces
.implements Le/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/a/b/e$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00e4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 p2\u00020\u00012\u00020\u0002:\u0001pB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0006\u00105\u001a\u000206J>\u00107\u001a\u0002062\u0006\u00108\u001a\u00020\t2\u0006\u00109\u001a\u00020\t2\u0006\u0010:\u001a\u00020\t2\u0006\u0010;\u001a\u00020\t2\u0006\u0010<\u001a\u00020\u00172\u0006\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020@J(\u0010A\u001a\u0002062\u0006\u00108\u001a\u00020\t2\u0006\u00109\u001a\u00020\t2\u0006\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020@H\u0002J\u0010\u0010B\u001a\u0002062\u0006\u0010C\u001a\u00020DH\u0002J0\u0010E\u001a\u0002062\u0006\u00108\u001a\u00020\t2\u0006\u00109\u001a\u00020\t2\u0006\u0010:\u001a\u00020\t2\u0006\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020@H\u0002J*\u0010F\u001a\u0004\u0018\u00010G2\u0006\u00109\u001a\u00020\t2\u0006\u0010:\u001a\u00020\t2\u0006\u0010H\u001a\u00020G2\u0006\u0010I\u001a\u00020JH\u0002J\u0008\u0010K\u001a\u00020GH\u0002J(\u0010L\u001a\u0002062\u0006\u0010C\u001a\u00020D2\u0006\u0010;\u001a\u00020\t2\u0006\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020@H\u0002J\n\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016J%\u0010M\u001a\u00020\u00172\u0006\u0010N\u001a\u00020O2\u000e\u0010P\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010QH\u0000\u00a2\u0006\u0002\u0008RJ\u000e\u0010S\u001a\u00020\u00172\u0006\u0010T\u001a\u00020\u0017J\u001d\u0010U\u001a\u00020V2\u0006\u0010W\u001a\u00020X2\u0006\u0010Y\u001a\u00020ZH\u0000\u00a2\u0006\u0002\u0008[J\u0015\u0010\\\u001a\u00020]2\u0006\u0010^\u001a\u00020_H\u0000\u00a2\u0006\u0002\u0008`J\u0006\u0010\u0019\u001a\u000206J\u0010\u0010a\u001a\u0002062\u0006\u0010b\u001a\u00020\u000fH\u0016J\u0010\u0010c\u001a\u0002062\u0006\u0010d\u001a\u00020eH\u0016J\u0008\u0010\u001d\u001a\u00020\u001eH\u0016J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0016\u0010f\u001a\u00020\u00172\u000c\u0010g\u001a\u0008\u0012\u0004\u0012\u00020\u00060QH\u0002J\u0008\u0010)\u001a\u00020 H\u0016J\u0010\u0010h\u001a\u0002062\u0006\u0010;\u001a\u00020\tH\u0002J\u000e\u0010i\u001a\u00020\u00172\u0006\u0010I\u001a\u00020JJ\u0008\u0010j\u001a\u00020kH\u0016J\u0017\u0010l\u001a\u0002062\u0008\u0010m\u001a\u0004\u0018\u00010nH\u0000\u00a2\u0006\u0002\u0008oR\u000e\u0010\u0008\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0010\u001a\u00020\u0011X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\u00178F\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0018R\u001a\u0010\u0019\u001a\u00020\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001a\u0010\u0018\"\u0004\u0008\u001b\u0010\u001cR\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001f\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\"\u001a\u00020\tX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008#\u0010$\"\u0004\u0008%\u0010&R\u0010\u0010\'\u001a\u0004\u0018\u00010(X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010)\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010*\u001a\u0004\u0018\u00010+X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010,\u001a\u00020\tX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008-\u0010$\"\u0004\u0008.\u0010&R\u001d\u0010/\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002020100\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00083\u00104\u00a8\u0006q"
    }
    d2 = {
        "Lokhttp3/internal/connection/RealConnection;",
        "Lokhttp3/internal/http2/Http2Connection$Listener;",
        "Lokhttp3/Connection;",
        "connectionPool",
        "Lokhttp3/internal/connection/RealConnectionPool;",
        "route",
        "Lokhttp3/Route;",
        "(Lokhttp3/internal/connection/RealConnectionPool;Lokhttp3/Route;)V",
        "allocationLimit",
        "",
        "getConnectionPool",
        "()Lokhttp3/internal/connection/RealConnectionPool;",
        "handshake",
        "Lokhttp3/Handshake;",
        "http2Connection",
        "Lokhttp3/internal/http2/Http2Connection;",
        "idleAtNanos",
        "",
        "getIdleAtNanos$okhttp",
        "()J",
        "setIdleAtNanos$okhttp",
        "(J)V",
        "isMultiplexed",
        "",
        "()Z",
        "noNewExchanges",
        "getNoNewExchanges",
        "setNoNewExchanges",
        "(Z)V",
        "protocol",
        "Lokhttp3/Protocol;",
        "rawSocket",
        "Ljava/net/Socket;",
        "refusedStreamCount",
        "routeFailureCount",
        "getRouteFailureCount$okhttp",
        "()I",
        "setRouteFailureCount$okhttp",
        "(I)V",
        "sink",
        "Lokio/BufferedSink;",
        "socket",
        "source",
        "Lokio/BufferedSource;",
        "successCount",
        "getSuccessCount$okhttp",
        "setSuccessCount$okhttp",
        "transmitters",
        "",
        "Ljava/lang/ref/Reference;",
        "Lokhttp3/internal/connection/Transmitter;",
        "getTransmitters",
        "()Ljava/util/List;",
        "cancel",
        "",
        "connect",
        "connectTimeout",
        "readTimeout",
        "writeTimeout",
        "pingIntervalMillis",
        "connectionRetryEnabled",
        "call",
        "Lokhttp3/Call;",
        "eventListener",
        "Lokhttp3/EventListener;",
        "connectSocket",
        "connectTls",
        "connectionSpecSelector",
        "Lokhttp3/internal/connection/ConnectionSpecSelector;",
        "connectTunnel",
        "createTunnel",
        "Lokhttp3/Request;",
        "tunnelRequest",
        "url",
        "Lokhttp3/HttpUrl;",
        "createTunnelRequest",
        "establishProtocol",
        "isEligible",
        "address",
        "Lokhttp3/Address;",
        "routes",
        "",
        "isEligible$okhttp",
        "isHealthy",
        "doExtensiveChecks",
        "newCodec",
        "Lokhttp3/internal/http/ExchangeCodec;",
        "client",
        "Lokhttp3/OkHttpClient;",
        "chain",
        "Lokhttp3/Interceptor$Chain;",
        "newCodec$okhttp",
        "newWebSocketStreams",
        "Lokhttp3/internal/ws/RealWebSocket$Streams;",
        "exchange",
        "Lokhttp3/internal/connection/Exchange;",
        "newWebSocketStreams$okhttp",
        "onSettings",
        "connection",
        "onStream",
        "stream",
        "Lokhttp3/internal/http2/Http2Stream;",
        "routeMatchesAny",
        "candidates",
        "startHttp2",
        "supportsUrl",
        "toString",
        "",
        "trackFailure",
        "e",
        "Ljava/io/IOException;",
        "trackFailure$okhttp",
        "Companion",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final c:Le/a/b/e$a;


# instance fields
.field private d:Ljava/net/Socket;

.field private e:Ljava/net/Socket;

.field private f:Le/A;

.field private g:Le/G;

.field private h:Le/a/e/g;

.field private i:Lf/k;

.field private j:Lf/j;

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/ref/Reference<",
            "Le/a/b/n;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private q:J

.field private final r:Le/a/b/i;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final s:Le/O;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Le/a/b/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/a/b/e$a;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/a/b/e;->c:Le/a/b/e$a;

    return-void
.end method

.method public constructor <init>(Le/a/b/i;Le/O;)V
    .locals 1
    .param p1    # Le/a/b/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/O;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "connectionPool"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "route"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Le/a/e/g$c;-><init>()V

    iput-object p1, p0, Le/a/b/e;->r:Le/a/b/i;

    iput-object p2, p0, Le/a/b/e;->s:Le/O;

    const/4 p1, 0x1

    iput p1, p0, Le/a/b/e;->o:I

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Le/a/b/e;->p:Ljava/util/List;

    const-wide p1, 0x7fffffffffffffffL

    iput-wide p1, p0, Le/a/b/e;->q:J

    return-void
.end method

.method public static final synthetic a(Le/a/b/e;)Le/A;
    .locals 0

    iget-object p0, p0, Le/a/b/e;->f:Le/A;

    return-object p0
.end method

.method private final a(IILe/I;Le/C;)Le/I;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CONNECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x1

    invoke-static {p4, v1}, Le/a/d;->a(Le/C;Z)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p4, " HTTP/1.1"

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    :goto_0
    iget-object v0, p0, Le/a/b/e;->i:Lf/k;

    const/4 v2, 0x0

    if-eqz v0, :cond_7

    iget-object v3, p0, Le/a/b/e;->j:Lf/j;

    if-eqz v3, :cond_6

    new-instance v4, Le/a/d/a;

    invoke-direct {v4, v2, v2, v0, v3}, Le/a/d/a;-><init>(Le/F;Le/a/b/e;Lf/k;Lf/j;)V

    invoke-interface {v0}, Lf/C;->c()Lf/E;

    move-result-object v5

    int-to-long v6, p1

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v6, v7, v8}, Lf/E;->a(JLjava/util/concurrent/TimeUnit;)Lf/E;

    invoke-interface {v3}, Lf/A;->c()Lf/E;

    move-result-object v5

    int-to-long v6, p2

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v6, v7, v8}, Lf/E;->a(JLjava/util/concurrent/TimeUnit;)Lf/E;

    invoke-virtual {p3}, Le/I;->d()Le/B;

    move-result-object v5

    invoke-virtual {v4, v5, p4}, Le/a/d/a;->a(Le/B;Ljava/lang/String;)V

    invoke-virtual {v4}, Le/a/d/a;->a()V

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Le/a/d/a;->a(Z)Le/L$a;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v5, p3}, Le/L$a;->a(Le/I;)Le/L$a;

    invoke-virtual {v5}, Le/L$a;->a()Le/L;

    move-result-object p3

    invoke-virtual {v4, p3}, Le/a/d/a;->c(Le/L;)V

    invoke-virtual {p3}, Le/L;->q()I

    move-result v4

    const/16 v5, 0xc8

    if-eq v4, v5, :cond_3

    const/16 v0, 0x197

    if-ne v4, v0, :cond_2

    iget-object v0, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->a()Le/a;

    move-result-object v0

    invoke-virtual {v0}, Le/a;->g()Le/c;

    move-result-object v0

    iget-object v3, p0, Le/a/b/e;->s:Le/O;

    invoke-interface {v0, v3, p3}, Le/c;->a(Le/O;Le/L;)Le/I;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v3, 0x2

    const-string v4, "Connection"

    invoke-static {p3, v4, v2, v3, v2}, Le/L;->a(Le/L;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    const-string v2, "close"

    invoke-static {v2, p3, v1}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p3

    if-eqz p3, :cond_0

    return-object v0

    :cond_0
    move-object p3, v0

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Failed to authenticate with proxy"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Unexpected response code for CONNECT: "

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Le/L;->q()I

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    invoke-interface {v0}, Lf/k;->getBuffer()Lf/h;

    move-result-object p1

    invoke-virtual {p1}, Lf/h;->d()Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-interface {v3}, Lf/j;->getBuffer()Lf/h;

    move-result-object p1

    invoke-virtual {p1}, Lf/h;->d()Z

    move-result p1

    if-eqz p1, :cond_4

    return-object v2

    :cond_4
    new-instance p1, Ljava/io/IOException;

    const-string p2, "TLS tunnel buffered too many bytes!"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_6
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2
.end method

.method private final a(IIILe/h;Le/x;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Le/a/b/e;->l()Le/I;

    move-result-object v0

    invoke-virtual {v0}, Le/I;->h()Le/C;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x15

    if-ge v2, v3, :cond_1

    invoke-direct {p0, p1, p2, p4, p5}, Le/a/b/e;->a(IILe/h;Le/x;)V

    invoke-direct {p0, p2, p3, v0, v1}, Le/a/b/e;->a(IILe/I;Le/C;)Le/I;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v3, p0, Le/a/b/e;->d:Ljava/net/Socket;

    if-eqz v3, :cond_0

    invoke-static {v3}, Le/a/d;->a(Ljava/net/Socket;)V

    :cond_0
    const/4 v3, 0x0

    iput-object v3, p0, Le/a/b/e;->d:Ljava/net/Socket;

    iput-object v3, p0, Le/a/b/e;->j:Lf/j;

    iput-object v3, p0, Le/a/b/e;->i:Lf/k;

    iget-object v4, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v4}, Le/O;->d()Ljava/net/InetSocketAddress;

    move-result-object v4

    iget-object v5, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v5}, Le/O;->b()Ljava/net/Proxy;

    move-result-object v5

    invoke-virtual {p5, p4, v4, v5, v3}, Le/x;->a(Le/h;Ljava/net/InetSocketAddress;Ljava/net/Proxy;Le/G;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final a(IILe/h;Le/x;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->b()Ljava/net/Proxy;

    move-result-object v0

    iget-object v1, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v1}, Le/O;->a()Le/a;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v3, Le/a/b/f;->a:[I

    invoke-virtual {v2}, Ljava/net/Proxy$Type;->ordinal()I

    move-result v2

    aget v2, v3, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    :goto_0
    new-instance v1, Ljava/net/Socket;

    invoke-direct {v1, v0}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Le/a;->i()Ljavax/net/SocketFactory;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v1

    if-eqz v1, :cond_3

    :goto_1
    iput-object v1, p0, Le/a/b/e;->d:Ljava/net/Socket;

    iget-object v2, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v2}, Le/O;->d()Ljava/net/InetSocketAddress;

    move-result-object v2

    invoke-virtual {p4, p3, v2, v0}, Le/x;->a(Le/h;Ljava/net/InetSocketAddress;Ljava/net/Proxy;)V

    invoke-virtual {v1, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    :try_start_0
    sget-object p2, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {p2}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object p2

    iget-object p3, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {p3}, Le/O;->d()Ljava/net/InetSocketAddress;

    move-result-object p3

    invoke-virtual {p2, v1, p3, p1}, Le/a/g/g;->a(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V
    :try_end_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-static {v1}, Lf/s;->b(Ljava/net/Socket;)Lf/C;

    move-result-object p1

    invoke-static {p1}, Lf/s;->a(Lf/C;)Lf/k;

    move-result-object p1

    iput-object p1, p0, Le/a/b/e;->i:Lf/k;

    invoke-static {v1}, Lf/s;->a(Ljava/net/Socket;)Lf/A;

    move-result-object p1

    invoke-static {p1}, Lf/s;->a(Lf/A;)Lf/j;

    move-result-object p1

    iput-object p1, p0, Le/a/b/e;->j:Lf/j;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object p2

    const-string p3, "throw with null exception"

    invoke-static {p2, p3}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_2

    :goto_2
    return-void

    :cond_2
    new-instance p2, Ljava/io/IOException;

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :catch_1
    move-exception p1

    new-instance p2, Ljava/net/ConnectException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Failed to connect to "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p4, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {p4}, Le/O;->d()Ljava/net/InetSocketAddress;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Ljava/net/ConnectException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw p2

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1
.end method

.method private final a(Le/a/b/b;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->a()Le/a;

    move-result-object v0

    invoke-virtual {v0}, Le/a;->j()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_a

    :try_start_0
    iget-object v3, p0, Le/a/b/e;->d:Ljava/net/Socket;

    invoke-virtual {v0}, Le/a;->k()Le/C;

    move-result-object v4

    invoke-virtual {v4}, Le/C;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Le/a;->k()Le/C;

    move-result-object v5

    invoke-virtual {v5}, Le/C;->k()I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v1, v3, v4, v5, v6}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v1

    if-eqz v1, :cond_9

    check-cast v1, Ljavax/net/ssl/SSLSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p1, v1}, Le/a/b/b;->a(Ljavax/net/ssl/SSLSocket;)Le/p;

    move-result-object p1

    invoke-virtual {p1}, Le/p;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v3}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v3

    invoke-virtual {v0}, Le/a;->k()Le/C;

    move-result-object v4

    invoke-virtual {v4}, Le/C;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Le/a;->e()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3, v1, v4, v5}, Le/a/g/g;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)V

    :cond_0
    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v3

    sget-object v4, Le/A;->b:Le/A$a;

    const-string v5, "sslSocketSession"

    invoke-static {v3, v5}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Le/A$a;->a(Ljavax/net/ssl/SSLSession;)Le/A;

    move-result-object v4

    invoke-virtual {v0}, Le/a;->d()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-virtual {v0}, Le/a;->k()Le/C;

    move-result-object v7

    invoke-virtual {v7}, Le/C;->h()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7, v3}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v4}, Le/A;->c()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v6

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_1

    new-instance p1, Lkotlin/o;

    const-string v0, "null cannot be cast to non-null type java.security.cert.X509Certificate"

    invoke-direct {p1, v0}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    check-cast p1, Ljava/security/cert/X509Certificate;

    new-instance v3, Ljavax/net/ssl/SSLPeerUnverifiedException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\n              |Hostname "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Le/a;->k()Le/C;

    move-result-object v0

    invoke-virtual {v0}, Le/C;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " not verified:\n              |    certificate: "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Le/j;->b:Le/j$b;

    invoke-virtual {v0, p1}, Le/j$b;->a(Ljava/security/cert/Certificate;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n              |    DN: "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object v0

    const-string v5, "cert.subjectDN"

    invoke-static {v0, v5}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/security/Principal;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n              |    subjectAltNames: "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Le/a/i/d;->a:Le/a/i/d;

    invoke-virtual {v0, p1}, Le/a/i/d;->a(Ljava/security/cert/X509Certificate;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "\n              "

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v2, v6, v2}, Lkotlin/g/g;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, p1}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    new-instance p1, Ljavax/net/ssl/SSLPeerUnverifiedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Hostname "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Le/a;->k()Le/C;

    move-result-object v0

    invoke-virtual {v0}, Le/C;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " not verified (no certificates)"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    invoke-virtual {v0}, Le/a;->a()Le/j;

    move-result-object v3

    if-eqz v3, :cond_7

    new-instance v5, Le/A;

    invoke-virtual {v4}, Le/A;->d()Le/P;

    move-result-object v6

    invoke-virtual {v4}, Le/A;->a()Le/m;

    move-result-object v7

    invoke-virtual {v4}, Le/A;->b()Ljava/util/List;

    move-result-object v8

    new-instance v9, Le/a/b/g;

    invoke-direct {v9, v3, v4, v0}, Le/a/b/g;-><init>(Le/j;Le/A;Le/a;)V

    invoke-direct {v5, v6, v7, v8, v9}, Le/A;-><init>(Le/P;Le/m;Ljava/util/List;Lkotlin/jvm/a/a;)V

    iput-object v5, p0, Le/a/b/e;->f:Le/A;

    invoke-virtual {v0}, Le/a;->k()Le/C;

    move-result-object v0

    invoke-virtual {v0}, Le/C;->h()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Le/a/b/h;

    invoke-direct {v4, p0}, Le/a/b/h;-><init>(Le/a/b/e;)V

    invoke-virtual {v3, v0, v4}, Le/j;->a(Ljava/lang/String;Lkotlin/jvm/a/a;)V

    invoke-virtual {p1}, Le/p;->c()Z

    move-result p1

    if-eqz p1, :cond_4

    sget-object p1, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {p1}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object p1

    invoke-virtual {p1, v1}, Le/a/g/g;->b(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;

    move-result-object v2

    :cond_4
    iput-object v1, p0, Le/a/b/e;->e:Ljava/net/Socket;

    invoke-static {v1}, Lf/s;->b(Ljava/net/Socket;)Lf/C;

    move-result-object p1

    invoke-static {p1}, Lf/s;->a(Lf/C;)Lf/k;

    move-result-object p1

    iput-object p1, p0, Le/a/b/e;->i:Lf/k;

    invoke-static {v1}, Lf/s;->a(Ljava/net/Socket;)Lf/A;

    move-result-object p1

    invoke-static {p1}, Lf/s;->a(Lf/A;)Lf/j;

    move-result-object p1

    iput-object p1, p0, Le/a/b/e;->j:Lf/j;

    if-eqz v2, :cond_5

    sget-object p1, Le/G;->h:Le/G$a;

    invoke-virtual {p1, v2}, Le/G$a;->a(Ljava/lang/String;)Le/G;

    move-result-object p1

    goto :goto_0

    :cond_5
    sget-object p1, Le/G;->b:Le/G;

    :goto_0
    iput-object p1, p0, Le/a/b/e;->g:Le/G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_6

    sget-object p1, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {p1}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object p1

    invoke-virtual {p1, v1}, Le/a/g/g;->a(Ljavax/net/ssl/SSLSocket;)V

    :cond_6
    return-void

    :cond_7
    :try_start_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_8
    :try_start_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    :catchall_0
    move-exception p1

    goto :goto_1

    :cond_9
    :try_start_4
    new-instance p1, Lkotlin/o;

    const-string v0, "null cannot be cast to non-null type javax.net.ssl.SSLSocket"

    invoke-direct {p1, v0}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_1
    move-exception p1

    move-object v1, v2

    goto :goto_1

    :cond_a
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    :goto_1
    if-eqz v1, :cond_b

    sget-object v0, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v0}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v0

    invoke-virtual {v0, v1}, Le/a/g/g;->a(Ljavax/net/ssl/SSLSocket;)V

    :cond_b
    if-eqz v1, :cond_c

    invoke-static {v1}, Le/a/d;->a(Ljava/net/Socket;)V

    :cond_c
    throw p1
.end method

.method private final a(Le/a/b/b;ILe/h;Le/x;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->a()Le/a;

    move-result-object v0

    invoke-virtual {v0}, Le/a;->j()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object p1, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {p1}, Le/O;->a()Le/a;

    move-result-object p1

    invoke-virtual {p1}, Le/a;->e()Ljava/util/List;

    move-result-object p1

    sget-object p3, Le/G;->e:Le/G;

    invoke-interface {p1, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Le/a/b/e;->d:Ljava/net/Socket;

    iput-object p1, p0, Le/a/b/e;->e:Ljava/net/Socket;

    sget-object p1, Le/G;->e:Le/G;

    iput-object p1, p0, Le/a/b/e;->g:Le/G;

    invoke-direct {p0, p2}, Le/a/b/e;->b(I)V

    return-void

    :cond_0
    iget-object p1, p0, Le/a/b/e;->d:Ljava/net/Socket;

    iput-object p1, p0, Le/a/b/e;->e:Ljava/net/Socket;

    sget-object p1, Le/G;->b:Le/G;

    iput-object p1, p0, Le/a/b/e;->g:Le/G;

    return-void

    :cond_1
    invoke-virtual {p4, p3}, Le/x;->g(Le/h;)V

    invoke-direct {p0, p1}, Le/a/b/e;->a(Le/a/b/b;)V

    iget-object p1, p0, Le/a/b/e;->f:Le/A;

    invoke-virtual {p4, p3, p1}, Le/x;->a(Le/h;Le/A;)V

    iget-object p1, p0, Le/a/b/e;->g:Le/G;

    sget-object p3, Le/G;->d:Le/G;

    if-ne p1, p3, :cond_2

    invoke-direct {p0, p2}, Le/a/b/e;->b(I)V

    :cond_2
    return-void
.end method

.method private final a(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Le/O;",
            ">;)Z"
        }
    .end annotation

    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le/O;

    invoke-virtual {v0}, Le/O;->b()Ljava/net/Proxy;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v3

    sget-object v4, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v3}, Le/O;->b()Ljava/net/Proxy;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v3

    sget-object v4, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v3}, Le/O;->d()Ljava/net/InetSocketAddress;

    move-result-object v3

    invoke-virtual {v0}, Le/O;->d()Ljava/net/InetSocketAddress;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    move v2, v1

    :cond_3
    :goto_1
    return v2
.end method

.method private final b(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/a/b/e;->e:Ljava/net/Socket;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v2, p0, Le/a/b/e;->i:Lf/k;

    if-eqz v2, :cond_1

    iget-object v3, p0, Le/a/b/e;->j:Lf/j;

    if-eqz v3, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    new-instance v5, Le/a/e/g$a;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Le/a/e/g$a;-><init>(Z)V

    iget-object v7, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v7}, Le/O;->a()Le/a;

    move-result-object v7

    invoke-virtual {v7}, Le/a;->k()Le/C;

    move-result-object v7

    invoke-virtual {v7}, Le/C;->h()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7, v2, v3}, Le/a/e/g$a;->a(Ljava/net/Socket;Ljava/lang/String;Lf/k;Lf/j;)Le/a/e/g$a;

    invoke-virtual {v5, p0}, Le/a/e/g$a;->a(Le/a/e/g$c;)Le/a/e/g$a;

    invoke-virtual {v5, p1}, Le/a/e/g$a;->a(I)Le/a/e/g$a;

    invoke-virtual {v5}, Le/a/e/g$a;->a()Le/a/e/g;

    move-result-object p1

    iput-object p1, p0, Le/a/b/e;->h:Le/a/e/g;

    invoke-static {p1, v4, v6, v1}, Le/a/e/g;->a(Le/a/e/g;ZILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1
.end method

.method private final l()Le/I;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Le/I$a;

    invoke-direct {v0}, Le/I$a;-><init>()V

    iget-object v1, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v1}, Le/O;->a()Le/a;

    move-result-object v1

    invoke-virtual {v1}, Le/a;->k()Le/C;

    move-result-object v1

    invoke-virtual {v0, v1}, Le/I$a;->a(Le/C;)Le/I$a;

    const-string v1, "CONNECT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Le/I$a;->a(Ljava/lang/String;Le/K;)Le/I$a;

    iget-object v1, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v1}, Le/O;->a()Le/a;

    move-result-object v1

    invoke-virtual {v1}, Le/a;->k()Le/C;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Le/a/d;->a(Le/C;Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Host"

    invoke-virtual {v0, v2, v1}, Le/I$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    const-string v1, "Proxy-Connection"

    const-string v2, "Keep-Alive"

    invoke-virtual {v0, v1, v2}, Le/I$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    const-string v1, "User-Agent"

    const-string v2, "okhttp/4.2.2"

    invoke-virtual {v0, v1, v2}, Le/I$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    invoke-virtual {v0}, Le/I$a;->a()Le/I;

    move-result-object v0

    new-instance v1, Le/L$a;

    invoke-direct {v1}, Le/L$a;-><init>()V

    invoke-virtual {v1, v0}, Le/L$a;->a(Le/I;)Le/L$a;

    sget-object v2, Le/G;->b:Le/G;

    invoke-virtual {v1, v2}, Le/L$a;->a(Le/G;)Le/L$a;

    const/16 v2, 0x197

    invoke-virtual {v1, v2}, Le/L$a;->a(I)Le/L$a;

    const-string v2, "Preemptive Authenticate"

    invoke-virtual {v1, v2}, Le/L$a;->a(Ljava/lang/String;)Le/L$a;

    sget-object v2, Le/a/d;->c:Le/N;

    invoke-virtual {v1, v2}, Le/L$a;->a(Le/N;)Le/L$a;

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3}, Le/L$a;->b(J)Le/L$a;

    invoke-virtual {v1, v2, v3}, Le/L$a;->a(J)Le/L$a;

    const-string v2, "Proxy-Authenticate"

    const-string v3, "OkHttp-Preemptive"

    invoke-virtual {v1, v2, v3}, Le/L$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/L$a;

    invoke-virtual {v1}, Le/L$a;->a()Le/L;

    move-result-object v1

    iget-object v2, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v2}, Le/O;->a()Le/a;

    move-result-object v2

    invoke-virtual {v2}, Le/a;->g()Le/c;

    move-result-object v2

    iget-object v3, p0, Le/a/b/e;->s:Le/O;

    invoke-interface {v2, v3, v1}, Le/c;->a(Le/O;Le/L;)Le/I;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v0, v1

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Le/F;Le/D$a;)Le/a/c/e;
    .locals 6
    .param p1    # Le/F;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/D$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "client"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chain"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/b/e;->e:Ljava/net/Socket;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v2, p0, Le/a/b/e;->i:Lf/k;

    if-eqz v2, :cond_2

    iget-object v3, p0, Le/a/b/e;->j:Lf/j;

    if-eqz v3, :cond_1

    iget-object v1, p0, Le/a/b/e;->h:Le/a/e/g;

    if-eqz v1, :cond_0

    new-instance v0, Le/a/e/s;

    invoke-direct {v0, p1, p0, p2, v1}, Le/a/e/s;-><init>(Le/F;Le/a/b/e;Le/D$a;Le/a/e/g;)V

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Le/D$a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    invoke-interface {v2}, Lf/C;->c()Lf/E;

    move-result-object v0

    invoke-interface {p2}, Le/D$a;->a()I

    move-result v1

    int-to-long v4, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v1}, Lf/E;->a(JLjava/util/concurrent/TimeUnit;)Lf/E;

    invoke-interface {v3}, Lf/A;->c()Lf/E;

    move-result-object v0

    invoke-interface {p2}, Le/D$a;->b()I

    move-result p2

    int-to-long v4, p2

    sget-object p2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, p2}, Lf/E;->a(JLjava/util/concurrent/TimeUnit;)Lf/E;

    new-instance v0, Le/a/d/a;

    invoke-direct {v0, p1, p0, v2, v3}, Le/a/d/a;-><init>(Le/F;Le/a/b/e;Lf/k;Lf/j;)V

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Le/a/b/e;->d:Ljava/net/Socket;

    if-eqz v0, :cond_0

    invoke-static {v0}, Le/a/d;->a(Ljava/net/Socket;)V

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 0

    iput p1, p0, Le/a/b/e;->m:I

    return-void
.end method

.method public final a(IIIIZLe/h;Le/x;)V
    .locals 16
    .param p6    # Le/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Le/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v7, p0

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    const-string v0, "call"

    invoke-static {v8, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventListener"

    invoke-static {v9, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v7, Le/a/b/e;->g:Le/G;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_e

    iget-object v0, v7, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->a()Le/a;

    move-result-object v0

    invoke-virtual {v0}, Le/a;->b()Ljava/util/List;

    move-result-object v0

    new-instance v10, Le/a/b/b;

    invoke-direct {v10, v0}, Le/a/b/b;-><init>(Ljava/util/List;)V

    iget-object v1, v7, Le/a/b/e;->s:Le/O;

    invoke-virtual {v1}, Le/O;->a()Le/a;

    move-result-object v1

    invoke-virtual {v1}, Le/a;->j()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Le/p;->f:Le/p;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v7, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->a()Le/a;

    move-result-object v0

    invoke-virtual {v0}, Le/a;->k()Le/C;

    move-result-object v0

    invoke-virtual {v0}, Le/C;->h()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v1}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v1

    invoke-virtual {v1, v0}, Le/a/g/g;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    new-instance v1, Le/a/b/l;

    new-instance v2, Ljava/net/UnknownServiceException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CLEARTEXT communication to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " not permitted by network security policy"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Le/a/b/l;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_2
    new-instance v0, Le/a/b/l;

    new-instance v1, Ljava/net/UnknownServiceException;

    const-string v2, "CLEARTEXT communication not enabled for client"

    invoke-direct {v1, v2}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Le/a/b/l;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_3
    iget-object v0, v7, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->a()Le/a;

    move-result-object v0

    invoke-virtual {v0}, Le/a;->e()Ljava/util/List;

    move-result-object v0

    sget-object v1, Le/G;->e:Le/G;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    :goto_1
    const/4 v11, 0x0

    move-object v12, v11

    :goto_2
    :try_start_0
    iget-object v0, v7, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    invoke-direct/range {v1 .. v6}, Le/a/b/e;->a(IIILe/h;Le/x;)V

    iget-object v0, v7, Le/a/b/e;->d:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_4

    goto :goto_4

    :cond_4
    move/from16 v13, p1

    move/from16 v14, p2

    goto :goto_3

    :cond_5
    move/from16 v13, p1

    move/from16 v14, p2

    :try_start_1
    invoke-direct {v7, v13, v14, v8, v9}, Le/a/b/e;->a(IILe/h;Le/x;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    move/from16 v15, p4

    :try_start_2
    invoke-direct {v7, v10, v15, v8, v9}, Le/a/b/e;->a(Le/a/b/b;ILe/h;Le/x;)V

    iget-object v0, v7, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->d()Ljava/net/InetSocketAddress;

    move-result-object v0

    iget-object v1, v7, Le/a/b/e;->s:Le/O;

    invoke-virtual {v1}, Le/O;->b()Ljava/net/Proxy;

    move-result-object v1

    iget-object v2, v7, Le/a/b/e;->g:Le/G;

    invoke-virtual {v9, v8, v0, v1, v2}, Le/x;->a(Le/h;Ljava/net/InetSocketAddress;Ljava/net/Proxy;Le/G;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_4
    iget-object v0, v7, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v7, Le/a/b/e;->d:Ljava/net/Socket;

    if-eqz v0, :cond_6

    goto :goto_5

    :cond_6
    new-instance v0, Le/a/b/l;

    new-instance v1, Ljava/net/ProtocolException;

    const-string v2, "Too many tunnel connections attempted: 21"

    invoke-direct {v1, v2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Le/a/b/l;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_7
    :goto_5
    iget-object v0, v7, Le/a/b/e;->h:Le/a/e/g;

    if-eqz v0, :cond_8

    iget-object v1, v7, Le/a/b/e;->r:Le/a/b/i;

    monitor-enter v1

    :try_start_3
    invoke-virtual {v0}, Le/a/e/g;->z()I

    move-result v0

    iput v0, v7, Le/a/b/e;->o:I

    sget-object v0, Lkotlin/r;->a:Lkotlin/r;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v1

    goto :goto_6

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_8
    :goto_6
    return-void

    :catch_0
    move-exception v0

    goto :goto_8

    :catch_1
    move-exception v0

    goto :goto_7

    :catch_2
    move-exception v0

    move/from16 v13, p1

    move/from16 v14, p2

    :goto_7
    move/from16 v15, p4

    :goto_8
    iget-object v1, v7, Le/a/b/e;->e:Ljava/net/Socket;

    if-eqz v1, :cond_9

    invoke-static {v1}, Le/a/d;->a(Ljava/net/Socket;)V

    :cond_9
    iget-object v1, v7, Le/a/b/e;->d:Ljava/net/Socket;

    if-eqz v1, :cond_a

    invoke-static {v1}, Le/a/d;->a(Ljava/net/Socket;)V

    :cond_a
    iput-object v11, v7, Le/a/b/e;->e:Ljava/net/Socket;

    iput-object v11, v7, Le/a/b/e;->d:Ljava/net/Socket;

    iput-object v11, v7, Le/a/b/e;->i:Lf/k;

    iput-object v11, v7, Le/a/b/e;->j:Lf/j;

    iput-object v11, v7, Le/a/b/e;->f:Le/A;

    iput-object v11, v7, Le/a/b/e;->g:Le/G;

    iput-object v11, v7, Le/a/b/e;->h:Le/a/e/g;

    iget-object v1, v7, Le/a/b/e;->s:Le/O;

    invoke-virtual {v1}, Le/O;->d()Ljava/net/InetSocketAddress;

    move-result-object v3

    iget-object v1, v7, Le/a/b/e;->s:Le/O;

    invoke-virtual {v1}, Le/O;->b()Ljava/net/Proxy;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v1, p7

    move-object/from16 v2, p6

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, Le/x;->a(Le/h;Ljava/net/InetSocketAddress;Ljava/net/Proxy;Le/G;Ljava/io/IOException;)V

    if-nez v12, :cond_b

    new-instance v1, Le/a/b/l;

    invoke-direct {v1, v0}, Le/a/b/l;-><init>(Ljava/io/IOException;)V

    move-object v12, v1

    goto :goto_9

    :cond_b
    invoke-virtual {v12, v0}, Le/a/b/l;->a(Ljava/io/IOException;)V

    :goto_9
    if-eqz p5, :cond_c

    invoke-virtual {v10, v0}, Le/a/b/b;->a(Ljava/io/IOException;)Z

    move-result v0

    if-eqz v0, :cond_c

    goto/16 :goto_2

    :cond_c
    throw v12

    :cond_d
    new-instance v0, Le/a/b/l;

    new-instance v1, Ljava/net/UnknownServiceException;

    const-string v2, "H2_PRIOR_KNOWLEDGE cannot be used with HTTPS"

    invoke-direct {v1, v2}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Le/a/b/l;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_e
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(J)V
    .locals 0

    iput-wide p1, p0, Le/a/b/e;->q:J

    return-void
.end method

.method public a(Le/a/e/g;)V
    .locals 1
    .param p1    # Le/a/e/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "connection"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/b/e;->r:Le/a/b/i;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p1}, Le/a/e/g;->z()I

    move-result p1

    iput p1, p0, Le/a/b/e;->o:I

    sget-object p1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public a(Le/a/e/u;)V
    .locals 2
    .param p1    # Le/a/e/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "stream"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Le/a/e/b;->e:Le/a/e/b;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Le/a/e/u;->a(Le/a/e/b;Ljava/io/IOException;)V

    return-void
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 4
    .param p1    # Ljava/io/IOException;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Le/a/b/e;->r:Le/a/b/i;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    sget-boolean v2, Lkotlin/s;->a:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Assertion failed"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Le/a/b/e;->r:Le/a/b/i;

    monitor-enter v0

    :try_start_0
    instance-of v2, p1, Le/a/e/A;

    if-eqz v2, :cond_3

    check-cast p1, Le/a/e/A;

    iget-object p1, p1, Le/a/e/A;->a:Le/a/e/b;

    sget-object v2, Le/a/b/f;->b:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v2, p1

    if-eq p1, v1, :cond_2

    const/4 v2, 0x2

    if-eq p1, v2, :cond_6

    iput-boolean v1, p0, Le/a/b/e;->k:Z

    iget p1, p0, Le/a/b/e;->l:I

    add-int/2addr p1, v1

    iput p1, p0, Le/a/b/e;->l:I

    goto :goto_1

    :cond_2
    iget p1, p0, Le/a/b/e;->n:I

    add-int/2addr p1, v1

    iput p1, p0, Le/a/b/e;->n:I

    iget p1, p0, Le/a/b/e;->n:I

    if-le p1, v1, :cond_6

    iput-boolean v1, p0, Le/a/b/e;->k:Z

    iget p1, p0, Le/a/b/e;->l:I

    add-int/2addr p1, v1

    iput p1, p0, Le/a/b/e;->l:I

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Le/a/b/e;->h()Z

    move-result v2

    if-eqz v2, :cond_4

    instance-of v2, p1, Le/a/e/a;

    if-eqz v2, :cond_6

    :cond_4
    iput-boolean v1, p0, Le/a/b/e;->k:Z

    iget v2, p0, Le/a/b/e;->m:I

    if-nez v2, :cond_6

    if-eqz p1, :cond_5

    iget-object v2, p0, Le/a/b/e;->r:Le/a/b/i;

    iget-object v3, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v2, v3, p1}, Le/a/b/i;->a(Le/O;Ljava/io/IOException;)V

    :cond_5
    iget p1, p0, Le/a/b/e;->l:I

    add-int/2addr p1, v1

    iput p1, p0, Le/a/b/e;->l:I

    :cond_6
    :goto_1
    sget-object p1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public final a(Le/C;)Z
    .locals 4
    .param p1    # Le/C;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->a()Le/a;

    move-result-object v0

    invoke-virtual {v0}, Le/a;->k()Le/C;

    move-result-object v0

    invoke-virtual {p1}, Le/C;->k()I

    move-result v1

    invoke-virtual {v0}, Le/C;->k()I

    move-result v2

    const/4 v3, 0x0

    if-eq v1, v2, :cond_0

    return v3

    :cond_0
    invoke-virtual {p1}, Le/C;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Le/C;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Le/a/b/e;->f:Le/A;

    if-eqz v0, :cond_4

    sget-object v0, Le/a/i/d;->a:Le/a/i/d;

    invoke-virtual {p1}, Le/C;->h()Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Le/a/b/e;->f:Le/A;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Le/A;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    check-cast v2, Ljava/security/cert/X509Certificate;

    invoke-virtual {v0, p1, v2}, Le/a/i/d;->verify(Ljava/lang/String;Ljava/security/cert/X509Certificate;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_0

    :cond_2
    new-instance p1, Lkotlin/o;

    const-string v0, "null cannot be cast to non-null type java.security.cert.X509Certificate"

    invoke-direct {p1, v0}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1

    :cond_4
    move v1, v3

    :goto_0
    return v1
.end method

.method public final a(Le/a;Ljava/util/List;)Z
    .locals 4
    .param p1    # Le/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le/a;",
            "Ljava/util/List<",
            "Le/O;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/b/e;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Le/a/b/e;->o:I

    const/4 v2, 0x0

    if-ge v0, v1, :cond_9

    iget-boolean v0, p0, Le/a/b/e;->k:Z

    if-eqz v0, :cond_0

    goto/16 :goto_0

    :cond_0
    iget-object v0, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v0}, Le/O;->a()Le/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Le/a;->a(Le/a;)Z

    move-result v0

    if-nez v0, :cond_1

    return v2

    :cond_1
    invoke-virtual {p1}, Le/a;->k()Le/C;

    move-result-object v0

    invoke-virtual {v0}, Le/C;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Le/a/b/e;->j()Le/O;

    move-result-object v1

    invoke-virtual {v1}, Le/O;->a()Le/a;

    move-result-object v1

    invoke-virtual {v1}, Le/a;->k()Le/C;

    move-result-object v1

    invoke-virtual {v1}, Le/C;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    return v1

    :cond_2
    iget-object v0, p0, Le/a/b/e;->h:Le/a/e/g;

    if-nez v0, :cond_3

    return v2

    :cond_3
    if-eqz p2, :cond_9

    invoke-direct {p0, p2}, Le/a/b/e;->a(Ljava/util/List;)Z

    move-result p2

    if-nez p2, :cond_4

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Le/a;->d()Ljavax/net/ssl/HostnameVerifier;

    move-result-object p2

    sget-object v0, Le/a/i/d;->a:Le/a/i/d;

    if-eq p2, v0, :cond_5

    return v2

    :cond_5
    invoke-virtual {p1}, Le/a;->k()Le/C;

    move-result-object p2

    invoke-virtual {p0, p2}, Le/a/b/e;->a(Le/C;)Z

    move-result p2

    if-nez p2, :cond_6

    return v2

    :cond_6
    :try_start_0
    invoke-virtual {p1}, Le/a;->a()Le/j;

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_8

    invoke-virtual {p1}, Le/a;->k()Le/C;

    move-result-object p1

    invoke-virtual {p1}, Le/C;->h()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Le/a/b/e;->g()Le/A;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Le/A;->c()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Le/j;->a(Ljava/lang/String;Ljava/util/List;)V

    return v1

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    throw v0

    :cond_8
    :try_start_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_1
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_1 .. :try_end_1} :catch_0

    throw v0

    :catch_0
    :cond_9
    :goto_0
    return v2
.end method

.method public final a(Z)Z
    .locals 5

    iget-object v0, p0, Le/a/b/e;->e:Ljava/net/Socket;

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    iget-object v2, p0, Le/a/b/e;->i:Lf/k;

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v1

    const/4 v3, 0x0

    if-nez v1, :cond_4

    invoke-virtual {v0}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Le/a/b/e;->h:Le/a/e/g;

    const/4 v4, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Le/a/e/g;->y()Z

    move-result p1

    xor-int/2addr p1, v4

    return p1

    :cond_1
    if-eqz p1, :cond_3

    :try_start_0
    invoke-virtual {v0}, Ljava/net/Socket;->getSoTimeout()I

    move-result p1
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v0, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    invoke-interface {v2}, Lf/k;->d()Z

    move-result v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_2

    move v1, v4

    goto :goto_0

    :cond_2
    move v1, v3

    :goto_0
    :try_start_2
    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    return v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v1
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    return v3

    :catch_1
    :cond_3
    return v4

    :cond_4
    :goto_1
    return v3

    :cond_5
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_6
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1
.end method

.method public final b()J
    .locals 2

    iget-wide v0, p0, Le/a/b/e;->q:J

    return-wide v0
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Le/a/b/e;->k:Z

    return-void
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Le/a/b/e;->k:Z

    return v0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Le/a/b/e;->l:I

    return v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Le/a/b/e;->m:I

    return v0
.end method

.method public final f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/ref/Reference<",
            "Le/a/b/n;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/b/e;->p:Ljava/util/List;

    return-object v0
.end method

.method public g()Le/A;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/a/b/e;->f:Le/A;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Le/a/b/e;->h:Le/a/e/g;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final i()V
    .locals 3

    iget-object v0, p0, Le/a/b/e;->r:Le/a/b/i;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    sget-boolean v2, Lkotlin/s;->a:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Assertion failed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Le/a/b/e;->r:Le/a/b/i;

    monitor-enter v0

    :try_start_0
    iput-boolean v1, p0, Le/a/b/e;->k:Z

    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public j()Le/O;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/b/e;->s:Le/O;

    return-object v0
.end method

.method public k()Ljava/net/Socket;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/b/e;->e:Ljava/net/Socket;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Connection{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v1}, Le/O;->a()Le/a;

    move-result-object v1

    invoke-virtual {v1}, Le/a;->k()Le/C;

    move-result-object v1

    invoke-virtual {v1}, Le/C;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v1}, Le/O;->a()Le/a;

    move-result-object v1

    invoke-virtual {v1}, Le/a;->k()Le/C;

    move-result-object v1

    invoke-virtual {v1}, Le/C;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, " proxy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v1}, Le/O;->b()Ljava/net/Proxy;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " hostAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le/a/b/e;->s:Le/O;

    invoke-virtual {v1}, Le/O;->d()Ljava/net/InetSocketAddress;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " cipherSuite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le/a/b/e;->f:Le/A;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Le/A;->a()Le/m;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "none"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " protocol="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le/a/b/e;->g:Le/G;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
