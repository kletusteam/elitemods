.class public final Le/a/b/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/a/b/c$b;,
        Le/a/b/c$c;,
        Le/a/b/c$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 ?2\u00020\u0001:\u0003?@AB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ7\u0010\u0017\u001a\u0002H\u0018\"\n\u0008\u0000\u0010\u0018*\u0004\u0018\u00010\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u001e\u001a\u0002H\u0018\u00a2\u0006\u0002\u0010\u001fJ\u0006\u0010 \u001a\u00020!J\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0016\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020\u0012J\u0006\u0010)\u001a\u00020!J\u0006\u0010*\u001a\u00020!J\u0006\u0010+\u001a\u00020!J\u0006\u0010,\u001a\u00020-J\u0006\u0010.\u001a\u00020!J\u0006\u0010/\u001a\u00020!J\u000e\u00100\u001a\u0002012\u0006\u00102\u001a\u000203J\u0010\u00104\u001a\u0004\u0018\u0001052\u0006\u00106\u001a\u00020\u0012J\u000e\u00107\u001a\u00020!2\u0006\u00102\u001a\u000203J\u0006\u00108\u001a\u00020!J\u0006\u00109\u001a\u00020!J\u0010\u0010:\u001a\u00020!2\u0006\u0010\u001e\u001a\u00020\u0019H\u0002J\u0006\u0010;\u001a\u00020<J\u0006\u0010=\u001a\u00020!J\u000e\u0010>\u001a\u00020!2\u0006\u0010&\u001a\u00020\'R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u0012@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006B"
    }
    d2 = {
        "Lokhttp3/internal/connection/Exchange;",
        "",
        "transmitter",
        "Lokhttp3/internal/connection/Transmitter;",
        "call",
        "Lokhttp3/Call;",
        "eventListener",
        "Lokhttp3/EventListener;",
        "finder",
        "Lokhttp3/internal/connection/ExchangeFinder;",
        "codec",
        "Lokhttp3/internal/http/ExchangeCodec;",
        "(Lokhttp3/internal/connection/Transmitter;Lokhttp3/Call;Lokhttp3/EventListener;Lokhttp3/internal/connection/ExchangeFinder;Lokhttp3/internal/http/ExchangeCodec;)V",
        "getCall$okhttp",
        "()Lokhttp3/Call;",
        "getEventListener$okhttp",
        "()Lokhttp3/EventListener;",
        "<set-?>",
        "",
        "isDuplex",
        "()Z",
        "getTransmitter$okhttp",
        "()Lokhttp3/internal/connection/Transmitter;",
        "bodyComplete",
        "E",
        "Ljava/io/IOException;",
        "bytesRead",
        "",
        "responseDone",
        "requestDone",
        "e",
        "(JZZLjava/io/IOException;)Ljava/io/IOException;",
        "cancel",
        "",
        "connection",
        "Lokhttp3/internal/connection/RealConnection;",
        "createRequestBody",
        "Lokio/Sink;",
        "request",
        "Lokhttp3/Request;",
        "duplex",
        "detachWithViolence",
        "finishRequest",
        "flushRequest",
        "newWebSocketStreams",
        "Lokhttp3/internal/ws/RealWebSocket$Streams;",
        "noNewExchangesOnConnection",
        "noRequestBody",
        "openResponseBody",
        "Lokhttp3/ResponseBody;",
        "response",
        "Lokhttp3/Response;",
        "readResponseHeaders",
        "Lokhttp3/Response$Builder;",
        "expectContinue",
        "responseHeadersEnd",
        "responseHeadersStart",
        "timeoutEarlyExit",
        "trackFailure",
        "trailers",
        "Lokhttp3/Headers;",
        "webSocketUpgradeFailed",
        "writeRequestHeaders",
        "Companion",
        "RequestBodySink",
        "ResponseBodySource",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final a:Le/a/b/c$a;


# instance fields
.field private b:Z

.field private final c:Le/a/b/n;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Le/h;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Le/x;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Le/a/b/d;

.field private final g:Le/a/c/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Le/a/b/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/a/b/c$a;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/a/b/c;->a:Le/a/b/c$a;

    return-void
.end method

.method public constructor <init>(Le/a/b/n;Le/h;Le/x;Le/a/b/d;Le/a/c/e;)V
    .locals 1
    .param p1    # Le/a/b/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Le/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Le/a/b/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Le/a/c/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transmitter"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "call"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventListener"

    invoke-static {p3, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "finder"

    invoke-static {p4, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "codec"

    invoke-static {p5, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/a/b/c;->c:Le/a/b/n;

    iput-object p2, p0, Le/a/b/c;->d:Le/h;

    iput-object p3, p0, Le/a/b/c;->e:Le/x;

    iput-object p4, p0, Le/a/b/c;->f:Le/a/b/d;

    iput-object p5, p0, Le/a/b/c;->g:Le/a/c/e;

    return-void
.end method

.method private final a(Ljava/io/IOException;)V
    .locals 1

    iget-object v0, p0, Le/a/b/c;->f:Le/a/b/d;

    invoke-virtual {v0}, Le/a/b/d;->d()V

    iget-object v0, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {v0}, Le/a/c/e;->b()Le/a/b/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Le/a/b/e;->a(Ljava/io/IOException;)V

    return-void

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1
.end method


# virtual methods
.method public final a(Z)Le/L$a;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    :try_start_0
    iget-object v0, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {v0, p1}, Le/a/c/e;->a(Z)Le/L$a;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Le/L$a;->a(Le/a/b/c;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object p1

    :catch_0
    move-exception p1

    iget-object v0, p0, Le/a/b/c;->e:Le/x;

    iget-object v1, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v0, v1, p1}, Le/x;->c(Le/h;Ljava/io/IOException;)V

    invoke-direct {p0, p1}, Le/a/b/c;->a(Ljava/io/IOException;)V

    throw p1
.end method

.method public final a(Le/L;)Le/N;
    .locals 4
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Le/a/b/c;->e:Le/x;

    iget-object v1, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v0, v1}, Le/x;->e(Le/h;)V

    const-string v0, "Content-Type"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Le/L;->a(Le/L;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {v1, p1}, Le/a/c/e;->b(Le/L;)J

    move-result-wide v1

    iget-object v3, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {v3, p1}, Le/a/c/e;->a(Le/L;)Lf/C;

    move-result-object p1

    new-instance v3, Le/a/b/c$c;

    invoke-direct {v3, p0, p1, v1, v2}, Le/a/b/c$c;-><init>(Le/a/b/c;Lf/C;J)V

    new-instance p1, Le/a/c/i;

    invoke-static {v3}, Lf/s;->a(Lf/C;)Lf/k;

    move-result-object v3

    invoke-direct {p1, v0, v1, v2, v3}, Le/a/c/i;-><init>(Ljava/lang/String;JLf/k;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    iget-object v0, p0, Le/a/b/c;->e:Le/x;

    iget-object v1, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v0, v1, p1}, Le/x;->c(Le/h;Ljava/io/IOException;)V

    invoke-direct {p0, p1}, Le/a/b/c;->a(Ljava/io/IOException;)V

    throw p1
.end method

.method public final a(Le/I;Z)Lf/A;
    .locals 3
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-boolean p2, p0, Le/a/b/c;->b:Z

    invoke-virtual {p1}, Le/I;->a()Le/K;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Le/K;->a()J

    move-result-wide v0

    iget-object p2, p0, Le/a/b/c;->e:Le/x;

    iget-object v2, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {p2, v2}, Le/x;->c(Le/h;)V

    iget-object p2, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {p2, p1, v0, v1}, Le/a/c/e;->a(Le/I;J)Lf/A;

    move-result-object p1

    new-instance p2, Le/a/b/c$b;

    invoke-direct {p2, p0, p1, v0, v1}, Le/a/b/c$b;-><init>(Le/a/b/c;Lf/A;J)V

    return-object p2

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1
.end method

.method public final a(JZZLjava/io/IOException;)Ljava/io/IOException;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(JZZTE;)TE;"
        }
    .end annotation

    if-eqz p5, :cond_0

    invoke-direct {p0, p5}, Le/a/b/c;->a(Ljava/io/IOException;)V

    :cond_0
    if-eqz p4, :cond_2

    if-eqz p5, :cond_1

    iget-object v0, p0, Le/a/b/c;->e:Le/x;

    iget-object v1, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v0, v1, p5}, Le/x;->b(Le/h;Ljava/io/IOException;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Le/a/b/c;->e:Le/x;

    iget-object v1, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v0, v1, p1, p2}, Le/x;->a(Le/h;J)V

    :cond_2
    :goto_0
    if-eqz p3, :cond_4

    if-eqz p5, :cond_3

    iget-object p1, p0, Le/a/b/c;->e:Le/x;

    iget-object p2, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {p1, p2, p5}, Le/x;->c(Le/h;Ljava/io/IOException;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Le/a/b/c;->e:Le/x;

    iget-object v1, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v0, v1, p1, p2}, Le/x;->b(Le/h;J)V

    :cond_4
    :goto_1
    iget-object p1, p0, Le/a/b/c;->c:Le/a/b/n;

    invoke-virtual {p1, p0, p4, p3, p5}, Le/a/b/n;->a(Le/a/b/c;ZZLjava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    return-object p1
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {v0}, Le/a/c/e;->cancel()V

    return-void
.end method

.method public final a(Le/I;)V
    .locals 2
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Le/a/b/c;->e:Le/x;

    iget-object v1, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v0, v1}, Le/x;->d(Le/h;)V

    iget-object v0, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {v0, p1}, Le/a/c/e;->a(Le/I;)V

    iget-object v0, p0, Le/a/b/c;->e:Le/x;

    iget-object v1, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v0, v1, p1}, Le/x;->a(Le/h;Le/I;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    iget-object v0, p0, Le/a/b/c;->e:Le/x;

    iget-object v1, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v0, v1, p1}, Le/x;->b(Le/h;Ljava/io/IOException;)V

    invoke-direct {p0, p1}, Le/a/b/c;->a(Ljava/io/IOException;)V

    throw p1
.end method

.method public final b()Le/a/b/e;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {v0}, Le/a/c/e;->b()Le/a/b/e;

    move-result-object v0

    return-object v0
.end method

.method public final b(Le/L;)V
    .locals 2
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/b/c;->e:Le/x;

    iget-object v1, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v0, v1, p1}, Le/x;->a(Le/h;Le/L;)V

    return-void
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {v0}, Le/a/c/e;->cancel()V

    iget-object v0, p0, Le/a/b/c;->c:Le/a/b/n;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v1, v2}, Le/a/b/n;->a(Le/a/b/c;ZZLjava/io/IOException;)Ljava/io/IOException;

    return-void
.end method

.method public final d()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {v0}, Le/a/c/e;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Le/a/b/c;->e:Le/x;

    iget-object v2, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v1, v2, v0}, Le/x;->b(Le/h;Ljava/io/IOException;)V

    invoke-direct {p0, v0}, Le/a/b/c;->a(Ljava/io/IOException;)V

    throw v0
.end method

.method public final e()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {v0}, Le/a/c/e;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Le/a/b/c;->e:Le/x;

    iget-object v2, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v1, v2, v0}, Le/x;->b(Le/h;Ljava/io/IOException;)V

    invoke-direct {p0, v0}, Le/a/b/c;->a(Ljava/io/IOException;)V

    throw v0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Le/a/b/c;->b:Z

    return v0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Le/a/b/c;->g:Le/a/c/e;

    invoke-interface {v0}, Le/a/c/e;->b()Le/a/b/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Le/a/b/e;->i()V

    return-void

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0
.end method

.method public final h()V
    .locals 4

    iget-object v0, p0, Le/a/b/c;->c:Le/a/b/n;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v1, v2, v3}, Le/a/b/n;->a(Le/a/b/c;ZZLjava/io/IOException;)Ljava/io/IOException;

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Le/a/b/c;->e:Le/x;

    iget-object v1, p0, Le/a/b/c;->d:Le/h;

    invoke-virtual {v0, v1}, Le/x;->f(Le/h;)V

    return-void
.end method
