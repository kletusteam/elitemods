.class public final Le/a/b/d;
.super Ljava/lang/Object;


# instance fields
.field private a:Le/a/b/m$b;

.field private final b:Le/a/b/m;

.field private c:Le/a/b/e;

.field private d:Z

.field private e:Le/O;

.field private final f:Le/a/b/n;

.field private final g:Le/a/b/i;

.field private final h:Le/a;

.field private final i:Le/h;

.field private final j:Le/x;


# direct methods
.method public constructor <init>(Le/a/b/n;Le/a/b/i;Le/a;Le/h;Le/x;)V
    .locals 1
    .param p1    # Le/a/b/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/a/b/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Le/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Le/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Le/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transmitter"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectionPool"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p3, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "call"

    invoke-static {p4, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventListener"

    invoke-static {p5, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/a/b/d;->f:Le/a/b/n;

    iput-object p2, p0, Le/a/b/d;->g:Le/a/b/i;

    iput-object p3, p0, Le/a/b/d;->h:Le/a;

    iput-object p4, p0, Le/a/b/d;->i:Le/h;

    iput-object p5, p0, Le/a/b/d;->j:Le/x;

    new-instance p1, Le/a/b/m;

    iget-object p2, p0, Le/a/b/d;->h:Le/a;

    iget-object p3, p0, Le/a/b/d;->g:Le/a/b/i;

    invoke-virtual {p3}, Le/a/b/i;->b()Le/a/b/k;

    move-result-object p3

    iget-object p4, p0, Le/a/b/d;->i:Le/h;

    iget-object p5, p0, Le/a/b/d;->j:Le/x;

    invoke-direct {p1, p2, p3, p4, p5}, Le/a/b/m;-><init>(Le/a;Le/a/b/k;Le/h;Le/x;)V

    iput-object p1, p0, Le/a/b/d;->b:Le/a/b/m;

    return-void
.end method

.method private final a(IIIIZ)Le/a/b/e;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    new-instance v0, Lkotlin/jvm/b/o;

    invoke-direct {v0}, Lkotlin/jvm/b/o;-><init>()V

    iget-object v2, v1, Le/a/b/d;->g:Le/a/b/i;

    monitor-enter v2

    :try_start_0
    iget-object v3, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v3}, Le/a/b/n;->g()Z

    move-result v3

    if-nez v3, :cond_26

    const/4 v3, 0x0

    iput-boolean v3, v1, Le/a/b/d;->d:Z

    iget-object v4, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v4}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v4

    iput-object v4, v0, Lkotlin/jvm/b/o;->a:Ljava/lang/Object;

    iget-object v4, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v4}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v4

    const/4 v5, 0x0

    if-eqz v4, :cond_1

    iget-object v4, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v4}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Le/a/b/e;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v4}, Le/a/b/n;->h()Ljava/net/Socket;

    move-result-object v4

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    throw v5

    :cond_1
    move-object v4, v5

    :goto_0
    :try_start_1
    iget-object v6, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v6}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v6}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v6

    iput-object v5, v0, Lkotlin/jvm/b/o;->a:Ljava/lang/Object;

    goto :goto_1

    :cond_2
    move-object v6, v5

    :goto_1
    const/4 v7, 0x1

    if-nez v6, :cond_6

    iget-object v8, v1, Le/a/b/d;->g:Le/a/b/i;

    iget-object v9, v1, Le/a/b/d;->h:Le/a;

    iget-object v10, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v8, v9, v10, v5, v3}, Le/a/b/i;->a(Le/a;Le/a/b/n;Ljava/util/List;Z)Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v6, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v6}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v6

    move-object v9, v5

    move v8, v7

    goto :goto_3

    :cond_3
    iget-object v8, v1, Le/a/b/d;->e:Le/O;

    if-eqz v8, :cond_4

    iget-object v8, v1, Le/a/b/d;->e:Le/O;

    iput-object v5, v1, Le/a/b/d;->e:Le/O;

    :goto_2
    move-object v9, v8

    move v8, v3

    goto :goto_3

    :cond_4
    invoke-direct/range {p0 .. p0}, Le/a/b/d;->e()Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v8}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {v8}, Le/a/b/e;->j()Le/O;

    move-result-object v8

    goto :goto_2

    :cond_5
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    throw v5

    :cond_6
    move v8, v3

    move-object v9, v5

    :goto_3
    :try_start_2
    sget-object v10, Lkotlin/r;->a:Lkotlin/r;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    monitor-exit v2

    if-eqz v4, :cond_7

    invoke-static {v4}, Le/a/d;->a(Ljava/net/Socket;)V

    :cond_7
    iget-object v0, v0, Lkotlin/jvm/b/o;->a:Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Le/a/b/e;

    if-eqz v2, :cond_9

    iget-object v2, v1, Le/a/b/d;->j:Le/x;

    iget-object v4, v1, Le/a/b/d;->i:Le/h;

    check-cast v0, Le/a/b/e;

    if-eqz v0, :cond_8

    invoke-virtual {v2, v4, v0}, Le/x;->b(Le/h;Le/n;)V

    goto :goto_4

    :cond_8
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_9
    :goto_4
    if-eqz v8, :cond_b

    iget-object v0, v1, Le/a/b/d;->j:Le/x;

    iget-object v2, v1, Le/a/b/d;->i:Le/h;

    if-eqz v6, :cond_a

    invoke-virtual {v0, v2, v6}, Le/x;->a(Le/h;Le/n;)V

    goto :goto_5

    :cond_a
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_b
    :goto_5
    if-eqz v6, :cond_d

    if-eqz v6, :cond_c

    return-object v6

    :cond_c
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_d
    if-nez v9, :cond_10

    iget-object v0, v1, Le/a/b/d;->a:Le/a/b/m$b;

    if-eqz v0, :cond_f

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Le/a/b/m$b;->b()Z

    move-result v0

    if-nez v0, :cond_10

    goto :goto_6

    :cond_e
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_f
    :goto_6
    iget-object v0, v1, Le/a/b/d;->b:Le/a/b/m;

    invoke-virtual {v0}, Le/a/b/m;->b()Le/a/b/m$b;

    move-result-object v0

    iput-object v0, v1, Le/a/b/d;->a:Le/a/b/m$b;

    move v0, v7

    goto :goto_7

    :cond_10
    move v0, v3

    :goto_7
    iget-object v2, v1, Le/a/b/d;->g:Le/a/b/i;

    monitor-enter v2

    :try_start_3
    iget-object v4, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v4}, Le/a/b/n;->g()Z

    move-result v4

    if-nez v4, :cond_25

    if-eqz v0, :cond_12

    iget-object v0, v1, Le/a/b/d;->a:Le/a/b/m$b;

    if-eqz v0, :cond_11

    invoke-virtual {v0}, Le/a/b/m$b;->a()Ljava/util/List;

    move-result-object v0

    iget-object v4, v1, Le/a/b/d;->g:Le/a/b/i;

    iget-object v10, v1, Le/a/b/d;->h:Le/a;

    iget-object v11, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v4, v10, v11, v0, v3}, Le/a/b/i;->a(Le/a;Le/a/b/n;Ljava/util/List;Z)Z

    move-result v3

    if-eqz v3, :cond_13

    iget-object v3, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v3}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v6

    move v8, v7

    goto :goto_8

    :cond_11
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v5

    :cond_12
    move-object v0, v5

    :cond_13
    :goto_8
    if-nez v8, :cond_17

    if-nez v9, :cond_15

    :try_start_4
    iget-object v3, v1, Le/a/b/d;->a:Le/a/b/m$b;

    if-eqz v3, :cond_14

    invoke-virtual {v3}, Le/a/b/m$b;->c()Le/O;

    move-result-object v9

    goto :goto_9

    :cond_14
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v5

    :cond_15
    :goto_9
    :try_start_5
    new-instance v6, Le/a/b/e;

    iget-object v3, v1, Le/a/b/d;->g:Le/a/b/i;

    if-eqz v9, :cond_16

    invoke-direct {v6, v3, v9}, Le/a/b/e;-><init>(Le/a/b/i;Le/O;)V

    iput-object v6, v1, Le/a/b/d;->c:Le/a/b/e;

    goto :goto_a

    :cond_16
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v5

    :cond_17
    :goto_a
    :try_start_6
    sget-object v3, Lkotlin/r;->a:Lkotlin/r;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    monitor-exit v2

    if-eqz v8, :cond_1a

    iget-object v0, v1, Le/a/b/d;->j:Le/x;

    iget-object v2, v1, Le/a/b/d;->i:Le/h;

    if-eqz v6, :cond_19

    invoke-virtual {v0, v2, v6}, Le/x;->a(Le/h;Le/n;)V

    if-eqz v6, :cond_18

    return-object v6

    :cond_18
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_19
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_1a
    if-eqz v6, :cond_24

    iget-object v2, v1, Le/a/b/d;->i:Le/h;

    iget-object v3, v1, Le/a/b/d;->j:Le/x;

    move-object v10, v6

    move/from16 v11, p1

    move/from16 v12, p2

    move/from16 v13, p3

    move/from16 v14, p4

    move/from16 v15, p5

    move-object/from16 v16, v2

    move-object/from16 v17, v3

    invoke-virtual/range {v10 .. v17}, Le/a/b/e;->a(IIIIZLe/h;Le/x;)V

    iget-object v2, v1, Le/a/b/d;->g:Le/a/b/i;

    invoke-virtual {v2}, Le/a/b/i;->b()Le/a/b/k;

    move-result-object v2

    if-eqz v6, :cond_23

    invoke-virtual {v6}, Le/a/b/e;->j()Le/O;

    move-result-object v3

    invoke-virtual {v2, v3}, Le/a/b/k;->a(Le/O;)V

    iget-object v2, v1, Le/a/b/d;->g:Le/a/b/i;

    monitor-enter v2

    :try_start_7
    iput-object v5, v1, Le/a/b/d;->c:Le/a/b/e;

    iget-object v3, v1, Le/a/b/d;->g:Le/a/b/i;

    iget-object v4, v1, Le/a/b/d;->h:Le/a;

    iget-object v8, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v3, v4, v8, v0, v7}, Le/a/b/i;->a(Le/a;Le/a/b/n;Ljava/util/List;Z)Z

    move-result v0

    if-eqz v0, :cond_1d

    if-eqz v6, :cond_1c

    invoke-virtual {v6, v7}, Le/a/b/e;->b(Z)V

    if-eqz v6, :cond_1b

    invoke-virtual {v6}, Le/a/b/e;->k()Ljava/net/Socket;

    move-result-object v0

    iget-object v3, v1, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v3}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v6

    iput-object v9, v1, Le/a/b/d;->e:Le/O;

    goto :goto_b

    :cond_1b
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    throw v5

    :cond_1c
    :try_start_8
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v5

    :cond_1d
    :try_start_9
    iget-object v0, v1, Le/a/b/d;->g:Le/a/b/i;

    if-eqz v6, :cond_22

    invoke-virtual {v0, v6}, Le/a/b/i;->b(Le/a/b/e;)V

    iget-object v0, v1, Le/a/b/d;->f:Le/a/b/n;

    if-eqz v6, :cond_21

    invoke-virtual {v0, v6}, Le/a/b/n;->a(Le/a/b/e;)V

    move-object v0, v5

    :goto_b
    sget-object v3, Lkotlin/r;->a:Lkotlin/r;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    monitor-exit v2

    if-eqz v0, :cond_1e

    invoke-static {v0}, Le/a/d;->a(Ljava/net/Socket;)V

    :cond_1e
    iget-object v0, v1, Le/a/b/d;->j:Le/x;

    iget-object v2, v1, Le/a/b/d;->i:Le/h;

    if-eqz v6, :cond_20

    invoke-virtual {v0, v2, v6}, Le/x;->a(Le/h;Le/n;)V

    if-eqz v6, :cond_1f

    return-object v6

    :cond_1f
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_20
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_21
    :try_start_a
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    throw v5

    :cond_22
    :try_start_b
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    throw v5

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_23
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_24
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_25
    :try_start_c
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Canceled"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_26
    :try_start_d
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Canceled"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private final a(IIIIZZ)Le/a/b/e;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    invoke-direct/range {p0 .. p5}, Le/a/b/d;->a(IIIIZ)Le/a/b/e;

    move-result-object v0

    iget-object v1, p0, Le/a/b/d;->g:Le/a/b/i;

    monitor-enter v1

    :try_start_0
    invoke-virtual {v0}, Le/a/b/e;->e()I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    sget-object v2, Lkotlin/r;->a:Lkotlin/r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    invoke-virtual {v0, p6}, Le/a/b/e;->a(Z)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Le/a/b/e;->i()V

    goto :goto_0

    :cond_1
    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit v1

    throw p1
.end method

.method private final e()Z
    .locals 2

    iget-object v0, p0, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v0}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v0}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Le/a/b/e;->d()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v0}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Le/a/b/e;->j()Le/O;

    move-result-object v0

    invoke-virtual {v0}, Le/O;->a()Le/a;

    move-result-object v0

    invoke-virtual {v0}, Le/a;->k()Le/C;

    move-result-object v0

    iget-object v1, p0, Le/a/b/d;->h:Le/a;

    invoke-virtual {v1}, Le/a;->k()Le/C;

    move-result-object v1

    invoke-static {v0, v1}, Le/a/d;->a(Le/C;Le/C;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public final a()Le/a/b/e;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/a/b/d;->g:Le/a/b/i;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    sget-boolean v1, Lkotlin/s;->a:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Assertion failed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Le/a/b/d;->c:Le/a/b/e;

    return-object v0
.end method

.method public final a(Le/F;Le/D$a;Z)Le/a/c/e;
    .locals 8
    .param p1    # Le/F;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/D$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "client"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chain"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2}, Le/D$a;->c()I

    move-result v2

    invoke-interface {p2}, Le/D$a;->a()I

    move-result v3

    invoke-interface {p2}, Le/D$a;->b()I

    move-result v4

    invoke-virtual {p1}, Le/F;->s()I

    move-result v5

    invoke-virtual {p1}, Le/F;->y()Z

    move-result v6

    move-object v1, p0

    move v7, p3

    :try_start_0
    invoke-direct/range {v1 .. v7}, Le/a/b/d;->a(IIIIZZ)Le/a/b/e;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Le/a/b/e;->a(Le/F;Le/D$a;)Le/a/c/e;

    move-result-object p1
    :try_end_0
    .catch Le/a/b/l; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-virtual {p0}, Le/a/b/d;->d()V

    new-instance p2, Le/a/b/l;

    invoke-direct {p2, p1}, Le/a/b/l;-><init>(Ljava/io/IOException;)V

    throw p2

    :catch_1
    move-exception p1

    invoke-virtual {p0}, Le/a/b/d;->d()V

    throw p1
.end method

.method public final b()Z
    .locals 4

    iget-object v0, p0, Le/a/b/d;->g:Le/a/b/i;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Le/a/b/d;->e:Le/O;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    monitor-exit v0

    return v2

    :cond_0
    :try_start_1
    invoke-direct {p0}, Le/a/b/d;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Le/a/b/d;->f:Le/a/b/n;

    invoke-virtual {v1}, Le/a/b/n;->e()Le/a/b/e;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Le/a/b/e;->j()Le/O;

    move-result-object v1

    iput-object v1, p0, Le/a/b/d;->e:Le/O;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return v2

    :cond_1
    :try_start_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x0

    throw v0

    :cond_2
    :try_start_3
    iget-object v1, p0, Le/a/b/d;->a:Le/a/b/m$b;

    const/4 v3, 0x0

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Le/a/b/m$b;->b()Z

    move-result v1

    goto :goto_0

    :cond_3
    move v1, v3

    :goto_0
    if-nez v1, :cond_5

    iget-object v1, p0, Le/a/b/d;->b:Le/a/b/m;

    invoke-virtual {v1}, Le/a/b/m;->a()Z

    move-result v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_4

    goto :goto_1

    :cond_4
    move v2, v3

    :cond_5
    :goto_1
    monitor-exit v0

    return v2

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final c()Z
    .locals 2

    iget-object v0, p0, Le/a/b/d;->g:Le/a/b/i;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Le/a/b/d;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final d()V
    .locals 3

    iget-object v0, p0, Le/a/b/d;->g:Le/a/b/i;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    sget-boolean v2, Lkotlin/s;->a:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Assertion failed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Le/a/b/d;->g:Le/a/b/i;

    monitor-enter v0

    :try_start_0
    iput-boolean v1, p0, Le/a/b/d;->d:Z

    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
