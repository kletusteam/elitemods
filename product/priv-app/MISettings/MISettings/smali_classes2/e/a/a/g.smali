.class final Le/a/a/g;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Le/a/a/e;-><init>(Le/a/f/b;Ljava/io/File;IIJLjava/util/concurrent/Executor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Le/a/a/e;


# direct methods
.method constructor <init>(Le/a/a/e;)V
    .locals 0

    iput-object p1, p0, Le/a/a/g;->a:Le/a/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Le/a/a/g;->a:Le/a/a/e;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Le/a/a/g;->a:Le/a/a/e;

    invoke-static {v1}, Le/a/a/e;->a(Le/a/a/e;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Le/a/a/g;->a:Le/a/a/e;

    invoke-virtual {v1}, Le/a/a/e;->b()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    goto :goto_2

    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iget-object v2, p0, Le/a/a/g;->a:Le/a/a/e;

    invoke-virtual {v2}, Le/a/a/e;->u()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    iget-object v2, p0, Le/a/a/g;->a:Le/a/a/e;

    invoke-static {v2, v1}, Le/a/a/e;->c(Le/a/a/e;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    :try_start_3
    iget-object v2, p0, Le/a/a/g;->a:Le/a/a/e;

    invoke-static {v2}, Le/a/a/e;->b(Le/a/a/e;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Le/a/a/g;->a:Le/a/a/e;

    invoke-virtual {v2}, Le/a/a/e;->t()V

    iget-object v2, p0, Le/a/a/g;->a:Le/a/a/e;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Le/a/a/e;->a(Le/a/a/e;I)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_1
    :try_start_4
    iget-object v2, p0, Le/a/a/g;->a:Le/a/a/e;

    invoke-static {v2, v1}, Le/a/a/e;->b(Le/a/a/e;Z)V

    iget-object v1, p0, Le/a/a/g;->a:Le/a/a/e;

    invoke-static {}, Lf/s;->a()Lf/A;

    move-result-object v2

    invoke-static {v2}, Lf/s;->a(Lf/A;)Lf/j;

    move-result-object v2

    invoke-static {v1, v2}, Le/a/a/e;->a(Le/a/a/e;Lf/j;)V

    :cond_1
    :goto_1
    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit v0

    return-void

    :cond_2
    :goto_2
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
