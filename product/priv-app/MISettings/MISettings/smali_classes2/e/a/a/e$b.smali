.class public final Le/a/a/e$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field private final a:[Z
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private b:Z

.field private final c:Le/a/a/e$c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field final synthetic d:Le/a/a/e;


# direct methods
.method public constructor <init>(Le/a/a/e;Le/a/a/e$c;)V
    .locals 1
    .param p1    # Le/a/a/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le/a/a/e$c;",
            ")V"
        }
    .end annotation

    const-string v0, "entry"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Le/a/a/e$b;->d:Le/a/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Le/a/a/e$b;->c:Le/a/a/e$c;

    iget-object p2, p0, Le/a/a/e$b;->c:Le/a/a/e$c;

    invoke-virtual {p2}, Le/a/a/e$c;->f()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Le/a/a/e;->r()I

    move-result p1

    new-array p1, p1, [Z

    :goto_0
    iput-object p1, p0, Le/a/a/e$b;->a:[Z

    return-void
.end method


# virtual methods
.method public final a(I)Lf/A;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/a/e$b;->d:Le/a/a/e;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Le/a/a/e$b;->b:Z

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_3

    iget-object v1, p0, Le/a/a/e$b;->c:Le/a/a/e$c;

    invoke-virtual {v1}, Le/a/a/e$c;->b()Le/a/a/e$b;

    move-result-object v1

    invoke-static {v1, p0}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_0

    invoke-static {}, Lf/s;->a()Lf/A;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p1

    :cond_0
    :try_start_1
    iget-object v1, p0, Le/a/a/e$b;->c:Le/a/a/e$c;

    invoke-virtual {v1}, Le/a/a/e$c;->f()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Le/a/a/e$b;->a:[Z

    if-eqz v1, :cond_1

    aput-boolean v2, v1, p1

    goto :goto_0

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 p1, 0x0

    throw p1

    :cond_2
    :goto_0
    :try_start_2
    iget-object v1, p0, Le/a/a/e$b;->c:Le/a/a/e$c;

    invoke-virtual {v1}, Le/a/a/e$c;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v2, p0, Le/a/a/e$b;->d:Le/a/a/e;

    invoke-virtual {v2}, Le/a/a/e;->q()Le/a/f/b;

    move-result-object v2

    invoke-interface {v2, v1}, Le/a/f/b;->b(Ljava/io/File;)Lf/A;

    move-result-object v1
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    new-instance v2, Le/a/a/i;

    new-instance v3, Le/a/a/f;

    invoke-direct {v3, p0, p1}, Le/a/a/f;-><init>(Le/a/a/e$b;I)V

    invoke-direct {v2, v1, v3}, Le/a/a/i;-><init>(Lf/A;Lkotlin/jvm/a/b;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit v0

    return-object v2

    :catch_0
    :try_start_5
    invoke-static {}, Lf/s;->a()Lf/A;

    move-result-object p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit v0

    return-object p1

    :cond_3
    :try_start_6
    const-string p1, "Check failed."

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public final a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/a/a/e$b;->d:Le/a/a/e;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Le/a/a/e$b;->b:Z

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_1

    iget-object v1, p0, Le/a/a/e$b;->c:Le/a/a/e$c;

    invoke-virtual {v1}, Le/a/a/e$c;->b()Le/a/a/e$b;

    move-result-object v1

    invoke-static {v1, p0}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Le/a/a/e$b;->d:Le/a/a/e;

    const/4 v3, 0x0

    invoke-virtual {v1, p0, v3}, Le/a/a/e;->a(Le/a/a/e$b;Z)V

    :cond_0
    iput-boolean v2, p0, Le/a/a/e$b;->b:Z

    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :cond_1
    :try_start_1
    const-string v1, "Check failed."

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final b()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/a/a/e$b;->d:Le/a/a/e;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Le/a/a/e$b;->b:Z

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_1

    iget-object v1, p0, Le/a/a/e$b;->c:Le/a/a/e$c;

    invoke-virtual {v1}, Le/a/a/e$c;->b()Le/a/a/e$b;

    move-result-object v1

    invoke-static {v1, p0}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Le/a/a/e$b;->d:Le/a/a/e;

    invoke-virtual {v1, p0, v2}, Le/a/a/e;->a(Le/a/a/e$b;Z)V

    :cond_0
    iput-boolean v2, p0, Le/a/a/e$b;->b:Z

    sget-object v1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :cond_1
    :try_start_1
    const-string v1, "Check failed."

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final c()V
    .locals 4

    iget-object v0, p0, Le/a/a/e$b;->c:Le/a/a/e$c;

    invoke-virtual {v0}, Le/a/a/e$c;->b()Le/a/a/e$b;

    move-result-object v0

    invoke-static {v0, p0}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, Le/a/a/e$b;->d:Le/a/a/e;

    invoke-virtual {v1}, Le/a/a/e;->r()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    :try_start_0
    iget-object v2, p0, Le/a/a/e$b;->d:Le/a/a/e;

    invoke-virtual {v2}, Le/a/a/e;->q()Le/a/f/b;

    move-result-object v2

    iget-object v3, p0, Le/a/a/e$b;->c:Le/a/a/e$c;

    invoke-virtual {v3}, Le/a/a/e$c;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-interface {v2, v3}, Le/a/f/b;->e(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Le/a/a/e$b;->c:Le/a/a/e$c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Le/a/a/e$c;->a(Le/a/a/e$b;)V

    :cond_1
    return-void
.end method

.method public final d()Le/a/a/e$c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/a/e$b;->c:Le/a/a/e$c;

    return-object v0
.end method

.method public final e()[Z
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/a/a/e$b;->a:[Z

    return-object v0
.end method
