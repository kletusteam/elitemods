.class public final Le/a/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/io/Flushable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/a/a/e$d;,
        Le/a/a/e$b;,
        Le/a/a/e$c;,
        Le/a/a/e$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010)\n\u0002\u0008\u0007\u0018\u0000 V2\u00020\u00012\u00020\u0002:\u0004VWXYB7\u0008\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0008\u00104\u001a\u000205H\u0002J\u0008\u00106\u001a\u000205H\u0016J!\u00107\u001a\u0002052\n\u00108\u001a\u000609R\u00020\u00002\u0006\u0010:\u001a\u00020\u0012H\u0000\u00a2\u0006\u0002\u0008;J\u0006\u0010<\u001a\u000205J \u0010=\u001a\u0008\u0018\u000109R\u00020\u00002\u0006\u0010>\u001a\u00020$2\u0008\u0008\u0002\u0010?\u001a\u00020\u000bH\u0007J\u0006\u0010@\u001a\u000205J\u0008\u0010A\u001a\u000205H\u0016J\u0017\u0010B\u001a\u0008\u0018\u00010CR\u00020\u00002\u0006\u0010>\u001a\u00020$H\u0086\u0002J\u0006\u0010D\u001a\u000205J\u0006\u0010E\u001a\u00020\u0012J\u0008\u0010F\u001a\u00020\u0012H\u0002J\u0008\u0010G\u001a\u00020!H\u0002J\u0008\u0010H\u001a\u000205H\u0002J\u0008\u0010I\u001a\u000205H\u0002J\u0010\u0010J\u001a\u0002052\u0006\u0010K\u001a\u00020$H\u0002J\r\u0010L\u001a\u000205H\u0000\u00a2\u0006\u0002\u0008MJ\u000e\u0010N\u001a\u00020\u00122\u0006\u0010>\u001a\u00020$J\u0019\u0010O\u001a\u00020\u00122\n\u0010P\u001a\u00060%R\u00020\u0000H\u0000\u00a2\u0006\u0002\u0008QJ\u0006\u00101\u001a\u00020\u000bJ\u0010\u0010R\u001a\u000c\u0012\u0008\u0012\u00060CR\u00020\u00000SJ\u0006\u0010T\u001a\u000205J\u0010\u0010U\u001a\u0002052\u0006\u0010>\u001a\u00020$H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u00020\u0012X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u00020\u0004X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010 \u001a\u0004\u0018\u00010!X\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\"\u001a\u0012\u0012\u0004\u0012\u00020$\u0012\u0008\u0012\u00060%R\u00020\u00000#X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\'R&\u0010\n\u001a\u00020\u000b2\u0006\u0010(\u001a\u00020\u000b8F@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008)\u0010*\"\u0004\u0008+\u0010,R\u000e\u0010-\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00101\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\u0008X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00082\u00103\u00a8\u0006Z"
    }
    d2 = {
        "Lokhttp3/internal/cache/DiskLruCache;",
        "Ljava/io/Closeable;",
        "Ljava/io/Flushable;",
        "fileSystem",
        "Lokhttp3/internal/io/FileSystem;",
        "directory",
        "Ljava/io/File;",
        "appVersion",
        "",
        "valueCount",
        "maxSize",
        "",
        "executor",
        "Ljava/util/concurrent/Executor;",
        "(Lokhttp3/internal/io/FileSystem;Ljava/io/File;IIJLjava/util/concurrent/Executor;)V",
        "cleanupRunnable",
        "Ljava/lang/Runnable;",
        "closed",
        "",
        "getClosed$okhttp",
        "()Z",
        "setClosed$okhttp",
        "(Z)V",
        "getDirectory",
        "()Ljava/io/File;",
        "getFileSystem$okhttp",
        "()Lokhttp3/internal/io/FileSystem;",
        "hasJournalErrors",
        "initialized",
        "journalFile",
        "journalFileBackup",
        "journalFileTmp",
        "journalWriter",
        "Lokio/BufferedSink;",
        "lruEntries",
        "Ljava/util/LinkedHashMap;",
        "",
        "Lokhttp3/internal/cache/DiskLruCache$Entry;",
        "getLruEntries$okhttp",
        "()Ljava/util/LinkedHashMap;",
        "value",
        "getMaxSize",
        "()J",
        "setMaxSize",
        "(J)V",
        "mostRecentRebuildFailed",
        "mostRecentTrimFailed",
        "nextSequenceNumber",
        "redundantOpCount",
        "size",
        "getValueCount$okhttp",
        "()I",
        "checkNotClosed",
        "",
        "close",
        "completeEdit",
        "editor",
        "Lokhttp3/internal/cache/DiskLruCache$Editor;",
        "success",
        "completeEdit$okhttp",
        "delete",
        "edit",
        "key",
        "expectedSequenceNumber",
        "evictAll",
        "flush",
        "get",
        "Lokhttp3/internal/cache/DiskLruCache$Snapshot;",
        "initialize",
        "isClosed",
        "journalRebuildRequired",
        "newJournalWriter",
        "processJournal",
        "readJournal",
        "readJournalLine",
        "line",
        "rebuildJournal",
        "rebuildJournal$okhttp",
        "remove",
        "removeEntry",
        "entry",
        "removeEntry$okhttp",
        "snapshots",
        "",
        "trimToSize",
        "validateKey",
        "Companion",
        "Editor",
        "Entry",
        "Snapshot",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
# The value of this static final field might be set in the static constructor
.field public static final a:Ljava/lang/String; = "journal"
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

# The value of this static final field might be set in the static constructor
.field public static final b:Ljava/lang/String; = "journal.tmp"
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

# The value of this static final field might be set in the static constructor
.field public static final c:Ljava/lang/String; = "journal.bkp"
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

# The value of this static final field might be set in the static constructor
.field public static final d:Ljava/lang/String; = "libcore.io.DiskLruCache"
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

# The value of this static final field might be set in the static constructor
.field public static final e:Ljava/lang/String; = "1"
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

# The value of this static final field might be set in the static constructor
.field public static final f:J = -0x1L
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation
.end field

.field public static final g:Lkotlin/g/f;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

# The value of this static final field might be set in the static constructor
.field public static final h:Ljava/lang/String; = "CLEAN"
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

# The value of this static final field might be set in the static constructor
.field public static final i:Ljava/lang/String; = "DIRTY"
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

# The value of this static final field might be set in the static constructor
.field public static final j:Ljava/lang/String; = "REMOVE"
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

# The value of this static final field might be set in the static constructor
.field public static final k:Ljava/lang/String; = "READ"
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final l:Le/a/a/e$a;


# instance fields
.field private final A:Ljava/lang/Runnable;

.field private final B:Le/a/f/b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final C:Ljava/io/File;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final D:I

.field private final E:I

.field private final F:Ljava/util/concurrent/Executor;

.field private m:J

.field private final n:Ljava/io/File;

.field private final o:Ljava/io/File;

.field private final p:Ljava/io/File;

.field private q:J

.field private r:Lf/j;

.field private final s:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Le/a/a/e$c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private t:I

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Le/a/a/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/a/a/e$a;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/a/a/e;->l:Le/a/a/e$a;

    const-string v0, "journal"

    sput-object v0, Le/a/a/e;->a:Ljava/lang/String;

    const-string v0, "journal.tmp"

    sput-object v0, Le/a/a/e;->b:Ljava/lang/String;

    const-string v0, "journal.bkp"

    sput-object v0, Le/a/a/e;->c:Ljava/lang/String;

    const-string v0, "libcore.io.DiskLruCache"

    sput-object v0, Le/a/a/e;->d:Ljava/lang/String;

    const-string v0, "1"

    sput-object v0, Le/a/a/e;->e:Ljava/lang/String;

    const-wide/16 v0, -0x1

    sput-wide v0, Le/a/a/e;->f:J

    new-instance v0, Lkotlin/g/f;

    const-string v1, "[a-z0-9_-]{1,120}"

    invoke-direct {v0, v1}, Lkotlin/g/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Le/a/a/e;->g:Lkotlin/g/f;

    const-string v0, "CLEAN"

    sput-object v0, Le/a/a/e;->h:Ljava/lang/String;

    const-string v0, "DIRTY"

    sput-object v0, Le/a/a/e;->i:Ljava/lang/String;

    const-string v0, "REMOVE"

    sput-object v0, Le/a/a/e;->j:Ljava/lang/String;

    const-string v0, "READ"

    sput-object v0, Le/a/a/e;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Le/a/f/b;Ljava/io/File;IIJLjava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Le/a/f/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/io/File;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "fileSystem"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "directory"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "executor"

    invoke-static {p7, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/a/a/e;->B:Le/a/f/b;

    iput-object p2, p0, Le/a/a/e;->C:Ljava/io/File;

    iput p3, p0, Le/a/a/e;->D:I

    iput p4, p0, Le/a/a/e;->E:I

    iput-object p7, p0, Le/a/a/e;->F:Ljava/util/concurrent/Executor;

    iput-wide p5, p0, Le/a/a/e;->m:J

    new-instance p1, Ljava/util/LinkedHashMap;

    const/4 p2, 0x0

    const/high16 p3, 0x3f400000    # 0.75f

    const/4 p4, 0x1

    invoke-direct {p1, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object p1, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    new-instance p1, Le/a/a/g;

    invoke-direct {p1, p0}, Le/a/a/g;-><init>(Le/a/a/e;)V

    iput-object p1, p0, Le/a/a/e;->A:Ljava/lang/Runnable;

    new-instance p1, Ljava/io/File;

    iget-object p2, p0, Le/a/a/e;->C:Ljava/io/File;

    sget-object p3, Le/a/a/e;->a:Ljava/lang/String;

    invoke-direct {p1, p2, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object p1, p0, Le/a/a/e;->n:Ljava/io/File;

    new-instance p1, Ljava/io/File;

    iget-object p2, p0, Le/a/a/e;->C:Ljava/io/File;

    sget-object p3, Le/a/a/e;->b:Ljava/lang/String;

    invoke-direct {p1, p2, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object p1, p0, Le/a/a/e;->o:Ljava/io/File;

    new-instance p1, Ljava/io/File;

    iget-object p2, p0, Le/a/a/e;->C:Ljava/io/File;

    sget-object p3, Le/a/a/e;->c:Ljava/lang/String;

    invoke-direct {p1, p2, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object p1, p0, Le/a/a/e;->p:Ljava/io/File;

    return-void
.end method

.method public static synthetic a(Le/a/a/e;Ljava/lang/String;JILjava/lang/Object;)Le/a/a/e$b;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    sget-wide p2, Le/a/a/e;->f:J

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Le/a/a/e;->a(Ljava/lang/String;J)Le/a/a/e$b;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Le/a/a/e;I)V
    .locals 0

    iput p1, p0, Le/a/a/e;->t:I

    return-void
.end method

.method public static final synthetic a(Le/a/a/e;Lf/j;)V
    .locals 0

    iput-object p1, p0, Le/a/a/e;->r:Lf/j;

    return-void
.end method

.method public static final synthetic a(Le/a/a/e;Z)V
    .locals 0

    iput-boolean p1, p0, Le/a/a/e;->u:Z

    return-void
.end method

.method public static final synthetic a(Le/a/a/e;)Z
    .locals 0

    iget-boolean p0, p0, Le/a/a/e;->v:Z

    return p0
.end method

.method public static final synthetic b(Le/a/a/e;Z)V
    .locals 0

    iput-boolean p1, p0, Le/a/a/e;->y:Z

    return-void
.end method

.method public static final synthetic b(Le/a/a/e;)Z
    .locals 0

    invoke-direct {p0}, Le/a/a/e;->w()Z

    move-result p0

    return p0
.end method

.method public static final synthetic c(Le/a/a/e;Z)V
    .locals 0

    iput-boolean p1, p0, Le/a/a/e;->x:Z

    return-void
.end method

.method private final g(Ljava/lang/String;)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    const/16 v2, 0x20

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v6}, Lkotlin/g/g;->a(Ljava/lang/CharSequence;CIZILjava/lang/Object;)I

    move-result v8

    const-string v9, "unexpected journal line: "

    const/4 v10, -0x1

    if-eq v8, v10, :cond_9

    add-int/lit8 v11, v8, 0x1

    const/16 v2, 0x20

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object/from16 v1, p1

    move v3, v11

    invoke-static/range {v1 .. v6}, Lkotlin/g/g;->a(Ljava/lang/CharSequence;CIZILjava/lang/Object;)I

    move-result v1

    const-string v2, "(this as java.lang.String).substring(startIndex)"

    const-string v3, "null cannot be cast to non-null type java.lang.String"

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x0

    if-ne v1, v10, :cond_1

    if-eqz v7, :cond_0

    invoke-virtual {v7, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v12, Le/a/a/e;->j:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-ne v8, v12, :cond_2

    sget-object v12, Le/a/a/e;->j:Ljava/lang/String;

    invoke-static {v7, v12, v6, v4, v5}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    iget-object v1, v0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v11}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    new-instance v1, Lkotlin/o;

    invoke-direct {v1, v3}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    if-eqz v7, :cond_8

    invoke-virtual {v7, v11, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const-string v12, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v11, v12}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_2
    iget-object v12, v0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v12, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Le/a/a/e$c;

    if-nez v12, :cond_3

    new-instance v12, Le/a/a/e$c;

    invoke-direct {v12, v0, v11}, Le/a/a/e$c;-><init>(Le/a/a/e;Ljava/lang/String;)V

    iget-object v13, v0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-interface {v13, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    if-eq v1, v10, :cond_5

    sget-object v11, Le/a/a/e;->h:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    if-ne v8, v11, :cond_5

    sget-object v11, Le/a/a/e;->h:Ljava/lang/String;

    invoke-static {v7, v11, v6, v4, v5}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    const/4 v4, 0x1

    add-int/2addr v1, v4

    if-eqz v7, :cond_4

    invoke-virtual {v7, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v14, v4, [C

    const/16 v1, 0x20

    aput-char v1, v14, v6

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x6

    const/16 v18, 0x0

    invoke-static/range {v13 .. v18}, Lkotlin/g/g;->a(Ljava/lang/CharSequence;[CZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v12, v4}, Le/a/a/e$c;->a(Z)V

    invoke-virtual {v12, v5}, Le/a/a/e$c;->a(Le/a/a/e$b;)V

    invoke-virtual {v12, v1}, Le/a/a/e$c;->a(Ljava/util/List;)V

    goto :goto_0

    :cond_4
    new-instance v1, Lkotlin/o;

    invoke-direct {v1, v3}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    if-ne v1, v10, :cond_6

    sget-object v2, Le/a/a/e;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v8, v2, :cond_6

    sget-object v2, Le/a/a/e;->i:Ljava/lang/String;

    invoke-static {v7, v2, v6, v4, v5}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v1, Le/a/a/e$b;

    invoke-direct {v1, v0, v12}, Le/a/a/e$b;-><init>(Le/a/a/e;Le/a/a/e$c;)V

    invoke-virtual {v12, v1}, Le/a/a/e$c;->a(Le/a/a/e$b;)V

    goto :goto_0

    :cond_6
    if-ne v1, v10, :cond_7

    sget-object v1, Le/a/a/e;->k:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v8, v1, :cond_7

    sget-object v1, Le/a/a/e;->k:Ljava/lang/String;

    invoke-static {v7, v1, v6, v4, v5}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :goto_0
    return-void

    :cond_7
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    new-instance v1, Lkotlin/o;

    invoke-direct {v1, v3}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private final h(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Le/a/a/e;->g:Lkotlin/g/f;

    invoke-virtual {v0, p1}, Lkotlin/g/f;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "keys must match regex [a-z0-9_-]{1,120}: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x22

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final declared-synchronized v()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Le/a/a/e;->w:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "cache is closed"

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private final w()Z
    .locals 2

    iget v0, p0, Le/a/a/e;->t:I

    const/16 v1, 0x7d0

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final x()Lf/j;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->n:Ljava/io/File;

    invoke-interface {v0, v1}, Le/a/f/b;->f(Ljava/io/File;)Lf/A;

    move-result-object v0

    new-instance v1, Le/a/a/i;

    new-instance v2, Le/a/a/h;

    invoke-direct {v2, p0}, Le/a/a/h;-><init>(Le/a/a/e;)V

    invoke-direct {v1, v0, v2}, Le/a/a/i;-><init>(Lf/A;Lkotlin/jvm/a/b;)V

    invoke-static {v1}, Lf/s;->a(Lf/A;)Lf/j;

    move-result-object v0

    return-object v0
.end method

.method private final y()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->o:Ljava/io/File;

    invoke-interface {v0, v1}, Le/a/f/b;->e(Ljava/io/File;)V

    iget-object v0, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "i.next()"

    invoke-static {v1, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Le/a/a/e$c;

    invoke-virtual {v1}, Le/a/a/e$c;->b()Le/a/a/e$b;

    move-result-object v2

    const/4 v3, 0x0

    if-nez v2, :cond_1

    iget v2, p0, Le/a/a/e;->E:I

    :goto_1
    if-ge v3, v2, :cond_0

    iget-wide v4, p0, Le/a/a/e;->q:J

    invoke-virtual {v1}, Le/a/a/e$c;->e()[J

    move-result-object v6

    aget-wide v7, v6, v3

    add-long/2addr v4, v7

    iput-wide v4, p0, Le/a/a/e;->q:J

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Le/a/a/e$c;->a(Le/a/a/e$b;)V

    iget v2, p0, Le/a/a/e;->E:I

    :goto_2
    if-ge v3, v2, :cond_2

    iget-object v4, p0, Le/a/a/e;->B:Le/a/f/b;

    invoke-virtual {v1}, Le/a/a/e$c;->a()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-interface {v4, v5}, Le/a/f/b;->e(Ljava/io/File;)V

    iget-object v4, p0, Le/a/a/e;->B:Le/a/f/b;

    invoke-virtual {v1}, Le/a/a/e$c;->c()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-interface {v4, v5}, Le/a/f/b;->e(Ljava/io/File;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private final z()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, ", "

    iget-object v1, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v2, p0, Le/a/a/e;->n:Ljava/io/File;

    invoke-interface {v1, v2}, Le/a/f/b;->a(Ljava/io/File;)Lf/C;

    move-result-object v1

    invoke-static {v1}, Lf/s;->a(Lf/C;)Lf/k;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v1}, Lf/k;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, Lf/k;->g()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, Lf/k;->g()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1}, Lf/k;->g()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1}, Lf/k;->g()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Le/a/a/e;->d:Ljava/lang/String;

    invoke-static {v8, v3}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    const/4 v9, 0x1

    xor-int/2addr v8, v9

    if-nez v8, :cond_2

    sget-object v8, Le/a/a/e;->e:Ljava/lang/String;

    invoke-static {v8, v4}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    xor-int/2addr v8, v9

    if-nez v8, :cond_2

    iget v8, p0, Le/a/a/e;->D:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v5}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    xor-int/2addr v5, v9

    if-nez v5, :cond_2

    iget v5, p0, Le/a/a/e;->E:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v6}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    xor-int/2addr v5, v9

    if-nez v5, :cond_2

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v5
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v8, 0x0

    if-lez v5, :cond_0

    goto :goto_0

    :cond_0
    move v9, v8

    :goto_0
    if-nez v9, :cond_2

    :goto_1
    :try_start_1
    invoke-interface {v1}, Lf/k;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Le/a/a/e;->g(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :catch_0
    :try_start_2
    iget-object v0, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    sub-int/2addr v8, v0

    iput v8, p0, Le/a/a/e;->t:I

    invoke-interface {v1}, Lf/k;->d()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Le/a/a/e;->t()V

    goto :goto_2

    :cond_1
    invoke-direct {p0}, Le/a/a/e;->x()Lf/j;

    move-result-object v0

    iput-object v0, p0, Le/a/a/e;->r:Lf/j;

    :goto_2
    sget-object v0, Lkotlin/r;->a:Lkotlin/r;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v1, v2}, Lkotlin/c/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    return-void

    :cond_2
    :try_start_3
    new-instance v5, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "unexpected journal header: ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x5d

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v2, v0

    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_3
    invoke-static {v1, v2}, Lkotlin/c/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;J)Le/a/a/e$b;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lkotlin/jvm/JvmOverloads;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Le/a/a/e;->s()V

    invoke-direct {p0}, Le/a/a/e;->v()V

    invoke-direct {p0, p1}, Le/a/a/e;->h(Ljava/lang/String;)V

    iget-object v0, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le/a/a/e$c;

    sget-wide v1, Le/a/a/e;->f:J

    cmp-long v1, p2, v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Le/a/a/e$c;->g()J

    move-result-wide v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long p2, v3, p2

    if-eqz p2, :cond_1

    :cond_0
    monitor-exit p0

    return-object v2

    :cond_1
    if-eqz v0, :cond_2

    :try_start_1
    invoke-virtual {v0}, Le/a/a/e$c;->b()Le/a/a/e$b;

    move-result-object p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_2
    move-object p2, v2

    :goto_0
    if-eqz p2, :cond_3

    monitor-exit p0

    return-object v2

    :cond_3
    :try_start_2
    iget-boolean p2, p0, Le/a/a/e;->x:Z

    if-nez p2, :cond_8

    iget-boolean p2, p0, Le/a/a/e;->y:Z

    if-eqz p2, :cond_4

    goto :goto_1

    :cond_4
    iget-object p2, p0, Le/a/a/e;->r:Lf/j;

    if-eqz p2, :cond_7

    sget-object p3, Le/a/a/e;->i:Ljava/lang/String;

    invoke-interface {p2, p3}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object p3

    const/16 v1, 0x20

    invoke-interface {p3, v1}, Lf/j;->writeByte(I)Lf/j;

    move-result-object p3

    invoke-interface {p3, p1}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object p3

    const/16 v1, 0xa

    invoke-interface {p3, v1}, Lf/j;->writeByte(I)Lf/j;

    invoke-interface {p2}, Lf/j;->flush()V

    iget-boolean p2, p0, Le/a/a/e;->u:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz p2, :cond_5

    monitor-exit p0

    return-object v2

    :cond_5
    if-nez v0, :cond_6

    :try_start_3
    new-instance v0, Le/a/a/e$c;

    invoke-direct {v0, p0, p1}, Le/a/a/e$c;-><init>(Le/a/a/e;Ljava/lang/String;)V

    iget-object p2, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    new-instance p1, Le/a/a/e$b;

    invoke-direct {p1, p0, v0}, Le/a/a/e$b;-><init>(Le/a/a/e;Le/a/a/e$c;)V

    invoke-virtual {v0, p1}, Le/a/a/e$c;->a(Le/a/a/e$b;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object p1

    :cond_7
    :try_start_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    :cond_8
    :goto_1
    :try_start_5
    iget-object p1, p0, Le/a/a/e;->F:Ljava/util/concurrent/Executor;

    iget-object p2, p0, Le/a/a/e;->A:Ljava/lang/Runnable;

    invoke-interface {p1, p2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return-object v2

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Le/a/a/e;->close()V

    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->C:Ljava/io/File;

    invoke-interface {v0, v1}, Le/a/f/b;->c(Ljava/io/File;)V

    return-void
.end method

.method public final declared-synchronized a(Le/a/a/e$b;Z)V
    .locals 9
    .param p1    # Le/a/a/e$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "editor"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Le/a/a/e$b;->d()Le/a/a/e$c;

    move-result-object v0

    invoke-virtual {v0}, Le/a/a/e$c;->b()Le/a/a/e$b;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p2, :cond_3

    invoke-virtual {v0}, Le/a/a/e$c;->f()Z

    move-result v3

    if-nez v3, :cond_3

    iget v3, p0, Le/a/a/e;->E:I

    move v4, v1

    :goto_0
    if-ge v4, v3, :cond_3

    invoke-virtual {p1}, Le/a/a/e$b;->e()[Z

    move-result-object v5

    if-eqz v5, :cond_2

    aget-boolean v5, v5, v4

    if-eqz v5, :cond_1

    iget-object v5, p0, Le/a/a/e;->B:Le/a/f/b;

    invoke-virtual {v0}, Le/a/a/e$c;->c()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    invoke-interface {v5, v6}, Le/a/f/b;->d(Ljava/io/File;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1}, Le/a/a/e$b;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Le/a/a/e$b;->a()V

    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Newly created entry didn\'t create value for index "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_3
    :try_start_2
    iget p1, p0, Le/a/a/e;->E:I

    :goto_1
    if-ge v1, p1, :cond_6

    invoke-virtual {v0}, Le/a/a/e$c;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    if-eqz p2, :cond_4

    iget-object v4, p0, Le/a/a/e;->B:Le/a/f/b;

    invoke-interface {v4, v3}, Le/a/f/b;->d(Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Le/a/a/e$c;->a()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/io/File;

    iget-object v5, p0, Le/a/a/e;->B:Le/a/f/b;

    invoke-interface {v5, v3, v4}, Le/a/f/b;->a(Ljava/io/File;Ljava/io/File;)V

    invoke-virtual {v0}, Le/a/a/e$c;->e()[J

    move-result-object v3

    aget-wide v5, v3, v1

    iget-object v3, p0, Le/a/a/e;->B:Le/a/f/b;

    invoke-interface {v3, v4}, Le/a/f/b;->g(Ljava/io/File;)J

    move-result-wide v3

    invoke-virtual {v0}, Le/a/a/e$c;->e()[J

    move-result-object v7

    aput-wide v3, v7, v1

    iget-wide v7, p0, Le/a/a/e;->q:J

    sub-long/2addr v7, v5

    add-long/2addr v7, v3

    iput-wide v7, p0, Le/a/a/e;->q:J

    goto :goto_2

    :cond_4
    iget-object v4, p0, Le/a/a/e;->B:Le/a/f/b;

    invoke-interface {v4, v3}, Le/a/f/b;->e(Ljava/io/File;)V

    :cond_5
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    iget p1, p0, Le/a/a/e;->t:I

    const/4 v1, 0x1

    add-int/2addr p1, v1

    iput p1, p0, Le/a/a/e;->t:I

    invoke-virtual {v0, v2}, Le/a/a/e$c;->a(Le/a/a/e$b;)V

    iget-object p1, p0, Le/a/a/e;->r:Lf/j;

    if-eqz p1, :cond_c

    invoke-virtual {v0}, Le/a/a/e$c;->f()Z

    move-result v2

    const/16 v3, 0xa

    const/16 v4, 0x20

    if-nez v2, :cond_8

    if-eqz p2, :cond_7

    goto :goto_3

    :cond_7
    iget-object p2, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Le/a/a/e$c;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p2, Le/a/a/e;->j:Ljava/lang/String;

    invoke-interface {p1, p2}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object p2

    invoke-interface {p2, v4}, Lf/j;->writeByte(I)Lf/j;

    invoke-virtual {v0}, Le/a/a/e$c;->d()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    invoke-interface {p1, v3}, Lf/j;->writeByte(I)Lf/j;

    goto :goto_4

    :cond_8
    :goto_3
    invoke-virtual {v0, v1}, Le/a/a/e$c;->a(Z)V

    sget-object v1, Le/a/a/e;->h:Ljava/lang/String;

    invoke-interface {p1, v1}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v1

    invoke-interface {v1, v4}, Lf/j;->writeByte(I)Lf/j;

    invoke-virtual {v0}, Le/a/a/e$c;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    invoke-virtual {v0, p1}, Le/a/a/e$c;->a(Lf/j;)V

    invoke-interface {p1, v3}, Lf/j;->writeByte(I)Lf/j;

    if-eqz p2, :cond_9

    iget-wide v1, p0, Le/a/a/e;->z:J

    const-wide/16 v3, 0x1

    add-long/2addr v3, v1

    iput-wide v3, p0, Le/a/a/e;->z:J

    invoke-virtual {v0, v1, v2}, Le/a/a/e$c;->a(J)V

    :cond_9
    :goto_4
    invoke-interface {p1}, Lf/j;->flush()V

    iget-wide p1, p0, Le/a/a/e;->q:J

    iget-wide v0, p0, Le/a/a/e;->m:J

    cmp-long p1, p1, v0

    if-gtz p1, :cond_a

    invoke-direct {p0}, Le/a/a/e;->w()Z

    move-result p1

    if-eqz p1, :cond_b

    :cond_a
    iget-object p1, p0, Le/a/a/e;->F:Ljava/util/concurrent/Executor;

    iget-object p2, p0, Le/a/a/e;->A:Ljava/lang/Runnable;

    invoke-interface {p1, p2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_b
    monitor-exit p0

    return-void

    :cond_c
    :try_start_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    :cond_d
    :try_start_4
    const-string p1, "Check failed."

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Le/a/a/e$c;)Z
    .locals 7
    .param p1    # Le/a/a/e$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "entry"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Le/a/a/e$c;->b()Le/a/a/e$b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Le/a/a/e$b;->c()V

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Le/a/a/e;->E:I

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Le/a/a/e;->B:Le/a/f/b;

    invoke-virtual {p1}, Le/a/a/e$c;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-interface {v2, v3}, Le/a/f/b;->e(Ljava/io/File;)V

    iget-wide v2, p0, Le/a/a/e;->q:J

    invoke-virtual {p1}, Le/a/a/e$c;->e()[J

    move-result-object v4

    aget-wide v5, v4, v0

    sub-long/2addr v2, v5

    iput-wide v2, p0, Le/a/a/e;->q:J

    invoke-virtual {p1}, Le/a/a/e$c;->e()[J

    move-result-object v2

    const-wide/16 v3, 0x0

    aput-wide v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Le/a/a/e;->t:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Le/a/a/e;->t:I

    iget-object v0, p0, Le/a/a/e;->r:Lf/j;

    if-eqz v0, :cond_3

    sget-object v2, Le/a/a/e;->j:Ljava/lang/String;

    invoke-interface {v0, v2}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v0

    const/16 v2, 0x20

    invoke-interface {v0, v2}, Lf/j;->writeByte(I)Lf/j;

    move-result-object v0

    invoke-virtual {p1}, Le/a/a/e$c;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v0

    const/16 v2, 0xa

    invoke-interface {v0, v2}, Lf/j;->writeByte(I)Lf/j;

    iget-object v0, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Le/a/a/e$c;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Le/a/a/e;->w()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Le/a/a/e;->F:Ljava/util/concurrent/Executor;

    iget-object v0, p0, Le/a/a/e;->A:Ljava/lang/Runnable;

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_2
    return v1

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Le/a/a/e;->w:Z

    return v0
.end method

.method public declared-synchronized close()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Le/a/a/e;->v:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Le/a/a/e;->w:Z

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    iget-object v0, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    const-string v2, "lruEntries.values"

    invoke-static {v0, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    new-array v3, v2, [Le/a/a/e$c;

    invoke-interface {v0, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, [Le/a/a/e$c;

    array-length v3, v0

    :goto_0
    const/4 v4, 0x0

    if-ge v2, v3, :cond_3

    aget-object v5, v0, v2

    invoke-virtual {v5}, Le/a/a/e$c;->b()Le/a/a/e$b;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Le/a/a/e$c;->b()Le/a/a/e$b;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Le/a/a/e$b;->a()V

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-virtual {p0}, Le/a/a/e;->u()V

    iget-object v0, p0, Le/a/a/e;->r:Lf/j;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lf/A;->close()V

    iput-object v4, p0, Le/a/a/e;->r:Lf/j;

    iput-boolean v1, p0, Le/a/a/e;->w:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_4
    :try_start_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    :cond_5
    :try_start_3
    new-instance v0, Lkotlin/o;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    :goto_2
    iput-boolean v1, p0, Le/a/a/e;->w:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(Ljava/lang/String;)Le/a/a/e$d;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Le/a/a/e;->s()V

    invoke-direct {p0}, Le/a/a/e;->v()V

    invoke-direct {p0, p1}, Le/a/a/e;->h(Ljava/lang/String;)V

    iget-object v0, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le/a/a/e$c;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    const-string v2, "lruEntries[key] ?: return null"

    invoke-static {v0, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Le/a/a/e$c;->f()Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Le/a/a/e$c;->h()Le/a/a/e$d;

    move-result-object v0

    if-eqz v0, :cond_3

    iget v2, p0, Le/a/a/e;->t:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Le/a/a/e;->t:I

    iget-object v2, p0, Le/a/a/e;->r:Lf/j;

    if-eqz v2, :cond_2

    sget-object v1, Le/a/a/e;->k:Ljava/lang/String;

    invoke-interface {v2, v1}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v1

    const/16 v2, 0x20

    invoke-interface {v1, v2}, Lf/j;->writeByte(I)Lf/j;

    move-result-object v1

    invoke-interface {v1, p1}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object p1

    const/16 v1, 0xa

    invoke-interface {p1, v1}, Lf/j;->writeByte(I)Lf/j;

    invoke-direct {p0}, Le/a/a/e;->w()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Le/a/a/e;->F:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Le/a/a/e;->A:Ljava/lang/Runnable;

    invoke-interface {p1, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_3
    monitor-exit p0

    return-object v1

    :cond_4
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized f(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Le/a/a/e;->s()V

    invoke-direct {p0}, Le/a/a/e;->v()V

    invoke-direct {p0, p1}, Le/a/a/e;->h(Ljava/lang/String;)V

    iget-object v0, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Le/a/a/e$c;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    const-string v1, "lruEntries[key] ?: return false"

    invoke-static {p1, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Le/a/a/e;->a(Le/a/a/e$c;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-wide v1, p0, Le/a/a/e;->q:J

    iget-wide v3, p0, Le/a/a/e;->m:J

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    iput-boolean v0, p0, Le/a/a/e;->x:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return p1

    :cond_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Le/a/a/e;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, Le/a/a/e;->v()V

    invoke-virtual {p0}, Le/a/a/e;->u()V

    iget-object v0, p0, Le/a/a/e;->r:Lf/j;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lf/j;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x0

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final p()Ljava/io/File;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/a/e;->C:Ljava/io/File;

    return-object v0
.end method

.method public final q()Le/a/f/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    return-object v0
.end method

.method public final r()I
    .locals 1

    iget v0, p0, Le/a/a/e;->E:I

    return v0
.end method

.method public final declared-synchronized s()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    sget-boolean v1, Lkotlin/s;->a:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "Assertion failed"

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_1
    :goto_0
    iget-boolean v0, p0, Le/a/a/e;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_2

    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->p:Ljava/io/File;

    invoke-interface {v0, v1}, Le/a/f/b;->d(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->n:Ljava/io/File;

    invoke-interface {v0, v1}, Le/a/f/b;->d(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->p:Ljava/io/File;

    invoke-interface {v0, v1}, Le/a/f/b;->e(Ljava/io/File;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->p:Ljava/io/File;

    iget-object v2, p0, Le/a/a/e;->n:Ljava/io/File;

    invoke-interface {v0, v1, v2}, Le/a/f/b;->a(Ljava/io/File;Ljava/io/File;)V

    :cond_4
    :goto_1
    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->n:Ljava/io/File;

    invoke-interface {v0, v1}, Le/a/f/b;->d(Ljava/io/File;)Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v1, 0x1

    if-eqz v0, :cond_5

    :try_start_2
    invoke-direct {p0}, Le/a/a/e;->z()V

    invoke-direct {p0}, Le/a/a/e;->y()V

    iput-boolean v1, p0, Le/a/a/e;->v:Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    sget-object v2, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v2}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v2

    const/4 v3, 0x5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DiskLruCache "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Le/a/a/e;->C:Ljava/io/File;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, " is corrupt: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ", removing"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, Le/a/g/g;->a(ILjava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v0, 0x0

    :try_start_4
    invoke-virtual {p0}, Le/a/a/e;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iput-boolean v0, p0, Le/a/a/e;->w:Z

    goto :goto_2

    :catchall_0
    move-exception v1

    iput-boolean v0, p0, Le/a/a/e;->w:Z

    throw v1

    :cond_5
    :goto_2
    invoke-virtual {p0}, Le/a/a/e;->t()V

    iput-boolean v1, p0, Le/a/a/e;->v:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    monitor-exit p0

    return-void

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized t()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Le/a/a/e;->r:Lf/j;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lf/A;->close()V

    :cond_0
    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->o:Ljava/io/File;

    invoke-interface {v0, v1}, Le/a/f/b;->b(Ljava/io/File;)Lf/A;

    move-result-object v0

    invoke-static {v0}, Lf/s;->a(Lf/A;)Lf/j;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    :try_start_1
    sget-object v2, Le/a/a/e;->d:Ljava/lang/String;

    invoke-interface {v0, v2}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v2

    const/16 v3, 0xa

    invoke-interface {v2, v3}, Lf/j;->writeByte(I)Lf/j;

    sget-object v2, Le/a/a/e;->e:Ljava/lang/String;

    invoke-interface {v0, v2}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v2

    invoke-interface {v2, v3}, Lf/j;->writeByte(I)Lf/j;

    iget v2, p0, Le/a/a/e;->D:I

    int-to-long v4, v2

    invoke-interface {v0, v4, v5}, Lf/j;->g(J)Lf/j;

    move-result-object v2

    invoke-interface {v2, v3}, Lf/j;->writeByte(I)Lf/j;

    iget v2, p0, Le/a/a/e;->E:I

    int-to-long v4, v2

    invoke-interface {v0, v4, v5}, Lf/j;->g(J)Lf/j;

    move-result-object v2

    invoke-interface {v2, v3}, Lf/j;->writeByte(I)Lf/j;

    invoke-interface {v0, v3}, Lf/j;->writeByte(I)Lf/j;

    iget-object v2, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Le/a/a/e$c;

    invoke-virtual {v4}, Le/a/a/e$c;->b()Le/a/a/e$b;

    move-result-object v5

    const/16 v6, 0x20

    if-eqz v5, :cond_1

    sget-object v5, Le/a/a/e;->i:Ljava/lang/String;

    invoke-interface {v0, v5}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v5

    invoke-interface {v5, v6}, Lf/j;->writeByte(I)Lf/j;

    invoke-virtual {v4}, Le/a/a/e$c;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    invoke-interface {v0, v3}, Lf/j;->writeByte(I)Lf/j;

    goto :goto_0

    :cond_1
    sget-object v5, Le/a/a/e;->h:Ljava/lang/String;

    invoke-interface {v0, v5}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v5

    invoke-interface {v5, v6}, Lf/j;->writeByte(I)Lf/j;

    invoke-virtual {v4}, Le/a/a/e$c;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    invoke-virtual {v4, v0}, Le/a/a/e$c;->a(Lf/j;)V

    invoke-interface {v0, v3}, Lf/j;->writeByte(I)Lf/j;

    goto :goto_0

    :cond_2
    sget-object v2, Lkotlin/r;->a:Lkotlin/r;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v0, v1}, Lkotlin/c/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->n:Ljava/io/File;

    invoke-interface {v0, v1}, Le/a/f/b;->d(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->n:Ljava/io/File;

    iget-object v2, p0, Le/a/a/e;->p:Ljava/io/File;

    invoke-interface {v0, v1, v2}, Le/a/f/b;->a(Ljava/io/File;Ljava/io/File;)V

    :cond_3
    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->o:Ljava/io/File;

    iget-object v2, p0, Le/a/a/e;->n:Ljava/io/File;

    invoke-interface {v0, v1, v2}, Le/a/f/b;->a(Ljava/io/File;Ljava/io/File;)V

    iget-object v0, p0, Le/a/a/e;->B:Le/a/f/b;

    iget-object v1, p0, Le/a/a/e;->p:Ljava/io/File;

    invoke-interface {v0, v1}, Le/a/f/b;->e(Ljava/io/File;)V

    invoke-direct {p0}, Le/a/a/e;->x()Lf/j;

    move-result-object v0

    iput-object v0, p0, Le/a/a/e;->r:Lf/j;

    const/4 v0, 0x0

    iput-boolean v0, p0, Le/a/a/e;->u:Z

    iput-boolean v0, p0, Le/a/a/e;->y:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    :try_start_4
    invoke-static {v0, v1}, Lkotlin/c/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final u()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    iget-wide v0, p0, Le/a/a/e;->q:J

    iget-wide v2, p0, Le/a/a/e;->m:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Le/a/a/e;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "lruEntries.values.iterator().next()"

    invoke-static {v0, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Le/a/a/e$c;

    invoke-virtual {p0, v0}, Le/a/a/e;->a(Le/a/a/e$c;)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Le/a/a/e;->x:Z

    return-void
.end method
