.class public final Le/a/a/d$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private a:Ljava/util/Date;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/Date;

.field private d:Ljava/lang/String;

.field private e:Ljava/util/Date;

.field private f:J

.field private g:J

.field private h:Ljava/lang/String;

.field private i:I

.field private final j:J

.field private final k:Le/I;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final l:Le/L;


# direct methods
.method public constructor <init>(JLe/I;Le/L;)V
    .locals 4
    .param p3    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Le/L;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "request"

    invoke-static {p3, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Le/a/a/d$b;->j:J

    iput-object p3, p0, Le/a/a/d$b;->k:Le/I;

    iput-object p4, p0, Le/a/a/d$b;->l:Le/L;

    const/4 p1, -0x1

    iput p1, p0, Le/a/a/d$b;->i:I

    iget-object p2, p0, Le/a/a/d$b;->l:Le/L;

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Le/L;->B()J

    move-result-wide p2

    iput-wide p2, p0, Le/a/a/d$b;->f:J

    iget-object p2, p0, Le/a/a/d$b;->l:Le/L;

    invoke-virtual {p2}, Le/L;->z()J

    move-result-wide p2

    iput-wide p2, p0, Le/a/a/d$b;->g:J

    iget-object p2, p0, Le/a/a/d$b;->l:Le/L;

    invoke-virtual {p2}, Le/L;->t()Le/B;

    move-result-object p2

    const/4 p3, 0x0

    invoke-virtual {p2}, Le/B;->size()I

    move-result p4

    :goto_0
    if-ge p3, p4, :cond_5

    invoke-virtual {p2, p3}, Le/B;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p3}, Le/B;->b(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const-string v3, "Date"

    invoke-static {v0, v3, v2}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, Le/a/c/d;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Le/a/a/d$b;->a:Ljava/util/Date;

    iput-object v1, p0, Le/a/a/d$b;->b:Ljava/lang/String;

    goto :goto_1

    :cond_0
    const-string v3, "Expires"

    invoke-static {v0, v3, v2}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v1}, Le/a/c/d;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Le/a/a/d$b;->e:Ljava/util/Date;

    goto :goto_1

    :cond_1
    const-string v3, "Last-Modified"

    invoke-static {v0, v3, v2}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v1}, Le/a/c/d;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Le/a/a/d$b;->c:Ljava/util/Date;

    iput-object v1, p0, Le/a/a/d$b;->d:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string v3, "ETag"

    invoke-static {v0, v3, v2}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    iput-object v1, p0, Le/a/a/d$b;->h:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string v3, "Age"

    invoke-static {v0, v3, v2}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {v1, p1}, Le/a/d;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Le/a/a/d$b;->i:I

    :cond_4
    :goto_1
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method

.method private final a(Le/I;)Z
    .locals 1

    const-string v0, "If-Modified-Since"

    invoke-virtual {p1, v0}, Le/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "If-None-Match"

    invoke-virtual {p1, v0}, Le/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private final b()J
    .locals 9

    iget-object v0, p0, Le/a/a/d$b;->a:Ljava/util/Date;

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    iget-wide v3, p0, Le/a/a/d$b;->g:J

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    :cond_0
    iget v0, p0, Le/a/a/d$b;->i:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    :cond_1
    iget-wide v3, p0, Le/a/a/d$b;->g:J

    iget-wide v5, p0, Le/a/a/d$b;->f:J

    sub-long v5, v3, v5

    iget-wide v7, p0, Le/a/a/d$b;->j:J

    sub-long/2addr v7, v3

    add-long/2addr v1, v5

    add-long/2addr v1, v7

    return-wide v1
.end method

.method private final c()Le/a/a/d;
    .locals 13

    iget-object v0, p0, Le/a/a/d$b;->l:Le/L;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    new-instance v0, Le/a/a/d;

    iget-object v2, p0, Le/a/a/d$b;->k:Le/I;

    invoke-direct {v0, v2, v1}, Le/a/a/d;-><init>(Le/I;Le/L;)V

    return-object v0

    :cond_0
    iget-object v0, p0, Le/a/a/d$b;->k:Le/I;

    invoke-virtual {v0}, Le/I;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Le/a/a/d$b;->l:Le/L;

    invoke-virtual {v0}, Le/L;->s()Le/A;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Le/a/a/d;

    iget-object v2, p0, Le/a/a/d$b;->k:Le/I;

    invoke-direct {v0, v2, v1}, Le/a/a/d;-><init>(Le/I;Le/L;)V

    return-object v0

    :cond_1
    sget-object v0, Le/a/a/d;->a:Le/a/a/d$a;

    iget-object v2, p0, Le/a/a/d$b;->l:Le/L;

    iget-object v3, p0, Le/a/a/d$b;->k:Le/I;

    invoke-virtual {v0, v2, v3}, Le/a/a/d$a;->a(Le/L;Le/I;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Le/a/a/d;

    iget-object v2, p0, Le/a/a/d$b;->k:Le/I;

    invoke-direct {v0, v2, v1}, Le/a/a/d;-><init>(Le/I;Le/L;)V

    return-object v0

    :cond_2
    iget-object v0, p0, Le/a/a/d$b;->k:Le/I;

    invoke-virtual {v0}, Le/I;->b()Le/g;

    move-result-object v0

    invoke-virtual {v0}, Le/g;->g()Z

    move-result v2

    if-nez v2, :cond_e

    iget-object v2, p0, Le/a/a/d$b;->k:Le/I;

    invoke-direct {p0, v2}, Le/a/a/d$b;->a(Le/I;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_3
    iget-object v2, p0, Le/a/a/d$b;->l:Le/L;

    invoke-virtual {v2}, Le/L;->b()Le/g;

    move-result-object v2

    invoke-direct {p0}, Le/a/a/d$b;->b()J

    move-result-wide v3

    invoke-direct {p0}, Le/a/a/d$b;->d()J

    move-result-wide v5

    invoke-virtual {v0}, Le/g;->c()I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_4

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Le/g;->c()I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v7, v9, v10}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v9

    invoke-static {v5, v6, v9, v10}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    :cond_4
    invoke-virtual {v0}, Le/g;->e()I

    move-result v7

    const-wide/16 v9, 0x0

    if-eq v7, v8, :cond_5

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Le/g;->e()I

    move-result v11

    int-to-long v11, v11

    invoke-virtual {v7, v11, v12}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v11

    goto :goto_0

    :cond_5
    move-wide v11, v9

    :goto_0
    invoke-virtual {v2}, Le/g;->f()Z

    move-result v7

    if-nez v7, :cond_6

    invoke-virtual {v0}, Le/g;->d()I

    move-result v7

    if-eq v7, v8, :cond_6

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Le/g;->d()I

    move-result v0

    int-to-long v8, v0

    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v9

    :cond_6
    invoke-virtual {v2}, Le/g;->g()Z

    move-result v0

    if-nez v0, :cond_9

    add-long/2addr v11, v3

    add-long/2addr v9, v5

    cmp-long v0, v11, v9

    if-gez v0, :cond_9

    iget-object v0, p0, Le/a/a/d$b;->l:Le/L;

    invoke-virtual {v0}, Le/L;->w()Le/L$a;

    move-result-object v0

    cmp-long v2, v11, v5

    const-string v5, "Warning"

    if-ltz v2, :cond_7

    const-string v2, "110 HttpURLConnection \"Response is stale\""

    invoke-virtual {v0, v5, v2}, Le/L$a;->a(Ljava/lang/String;Ljava/lang/String;)Le/L$a;

    :cond_7
    const-wide/32 v6, 0x5265c00

    cmp-long v2, v3, v6

    if-lez v2, :cond_8

    invoke-direct {p0}, Le/a/a/d$b;->e()Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "113 HttpURLConnection \"Heuristic expiration\""

    invoke-virtual {v0, v5, v2}, Le/L$a;->a(Ljava/lang/String;Ljava/lang/String;)Le/L$a;

    :cond_8
    new-instance v2, Le/a/a/d;

    invoke-virtual {v0}, Le/L$a;->a()Le/L;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Le/a/a/d;-><init>(Le/I;Le/L;)V

    return-object v2

    :cond_9
    iget-object v0, p0, Le/a/a/d$b;->h:Ljava/lang/String;

    const-string v2, "If-Modified-Since"

    if-eqz v0, :cond_a

    const-string v2, "If-None-Match"

    goto :goto_1

    :cond_a
    iget-object v0, p0, Le/a/a/d$b;->c:Ljava/util/Date;

    if-eqz v0, :cond_b

    iget-object v0, p0, Le/a/a/d$b;->d:Ljava/lang/String;

    goto :goto_1

    :cond_b
    iget-object v0, p0, Le/a/a/d$b;->a:Ljava/util/Date;

    if-eqz v0, :cond_d

    iget-object v0, p0, Le/a/a/d$b;->b:Ljava/lang/String;

    :goto_1
    iget-object v3, p0, Le/a/a/d$b;->k:Le/I;

    invoke-virtual {v3}, Le/I;->d()Le/B;

    move-result-object v3

    invoke-virtual {v3}, Le/B;->a()Le/B$a;

    move-result-object v3

    if-eqz v0, :cond_c

    invoke-virtual {v3, v2, v0}, Le/B$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/B$a;

    iget-object v0, p0, Le/a/a/d$b;->k:Le/I;

    invoke-virtual {v0}, Le/I;->g()Le/I$a;

    move-result-object v0

    invoke-virtual {v3}, Le/B$a;->a()Le/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Le/I$a;->a(Le/B;)Le/I$a;

    invoke-virtual {v0}, Le/I$a;->a()Le/I;

    move-result-object v0

    new-instance v1, Le/a/a/d;

    iget-object v2, p0, Le/a/a/d$b;->l:Le/L;

    invoke-direct {v1, v0, v2}, Le/a/a/d;-><init>(Le/I;Le/L;)V

    return-object v1

    :cond_c
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_d
    new-instance v0, Le/a/a/d;

    iget-object v2, p0, Le/a/a/d$b;->k:Le/I;

    invoke-direct {v0, v2, v1}, Le/a/a/d;-><init>(Le/I;Le/L;)V

    return-object v0

    :cond_e
    :goto_2
    new-instance v0, Le/a/a/d;

    iget-object v2, p0, Le/a/a/d$b;->k:Le/I;

    invoke-direct {v0, v2, v1}, Le/a/a/d;-><init>(Le/I;Le/L;)V

    return-object v0
.end method

.method private final d()J
    .locals 6

    iget-object v0, p0, Le/a/a/d$b;->l:Le/L;

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Le/L;->b()Le/g;

    move-result-object v0

    invoke-virtual {v0}, Le/g;->c()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Le/g;->c()I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0

    :cond_0
    iget-object v0, p0, Le/a/a/d$b;->e:Ljava/util/Date;

    const-wide/16 v2, 0x0

    if-eqz v0, :cond_3

    iget-object v1, p0, Le/a/a/d$b;->a:Ljava/util/Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    goto :goto_0

    :cond_1
    iget-wide v4, p0, Le/a/a/d$b;->g:J

    :goto_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    sub-long/2addr v0, v4

    cmp-long v4, v0, v2

    if-lez v4, :cond_2

    goto :goto_1

    :cond_2
    move-wide v0, v2

    :goto_1
    return-wide v0

    :cond_3
    iget-object v0, p0, Le/a/a/d$b;->c:Ljava/util/Date;

    if-eqz v0, :cond_7

    iget-object v0, p0, Le/a/a/d$b;->l:Le/L;

    invoke-virtual {v0}, Le/L;->A()Le/I;

    move-result-object v0

    invoke-virtual {v0}, Le/I;->h()Le/C;

    move-result-object v0

    invoke-virtual {v0}, Le/C;->l()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    iget-object v0, p0, Le/a/a/d$b;->a:Ljava/util/Date;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    goto :goto_2

    :cond_4
    iget-wide v4, p0, Le/a/a/d$b;->f:J

    :goto_2
    iget-object v0, p0, Le/a/a/d$b;->c:Ljava/util/Date;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    sub-long/2addr v4, v0

    cmp-long v0, v4, v2

    if-lez v0, :cond_5

    const/16 v0, 0xa

    int-to-long v0, v0

    div-long v2, v4, v0

    :cond_5
    return-wide v2

    :cond_6
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_7
    return-wide v2

    :cond_8
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1
.end method

.method private final e()Z
    .locals 2

    iget-object v0, p0, Le/a/a/d$b;->l:Le/L;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Le/L;->b()Le/g;

    move-result-object v0

    invoke-virtual {v0}, Le/g;->c()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Le/a/a/d$b;->e:Ljava/util/Date;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0
.end method


# virtual methods
.method public final a()Le/a/a/d;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    invoke-direct {p0}, Le/a/a/d$b;->c()Le/a/a/d;

    move-result-object v0

    invoke-virtual {v0}, Le/a/a/d;->b()Le/I;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Le/a/a/d$b;->k:Le/I;

    invoke-virtual {v1}, Le/I;->b()Le/g;

    move-result-object v1

    invoke-virtual {v1}, Le/g;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Le/a/a/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Le/a/a/d;-><init>(Le/I;Le/L;)V

    :cond_0
    return-object v0
.end method
