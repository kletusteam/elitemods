.class public final Le/a/a/e$d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "d"
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/C;",
            ">;"
        }
    .end annotation
.end field

.field private final d:[J

.field final synthetic e:Le/a/a/e;


# direct methods
.method public constructor <init>(Le/a/a/e;Ljava/lang/String;JLjava/util/List;[J)V
    .locals 1
    .param p1    # Le/a/a/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # J
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List<",
            "+",
            "Lf/C;",
            ">;[J)V"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sources"

    invoke-static {p5, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lengths"

    invoke-static {p6, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Le/a/a/e$d;->e:Le/a/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Le/a/a/e$d;->a:Ljava/lang/String;

    iput-wide p3, p0, Le/a/a/e$d;->b:J

    iput-object p5, p0, Le/a/a/e$d;->c:Ljava/util/List;

    iput-object p6, p0, Le/a/a/e$d;->d:[J

    return-void
.end method


# virtual methods
.method public final a()Le/a/a/e$b;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/a/a/e$d;->e:Le/a/a/e;

    iget-object v1, p0, Le/a/a/e$d;->a:Ljava/lang/String;

    iget-wide v2, p0, Le/a/a/e$d;->b:J

    invoke-virtual {v0, v1, v2, v3}, Le/a/a/e;->a(Ljava/lang/String;J)Le/a/a/e$b;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lf/C;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/a/e$d;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/C;

    return-object p1
.end method

.method public close()V
    .locals 2

    iget-object v0, p0, Le/a/a/e$d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/C;

    invoke-static {v1}, Le/a/d;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_0
    return-void
.end method
