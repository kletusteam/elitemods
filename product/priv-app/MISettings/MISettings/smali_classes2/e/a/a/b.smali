.class public final Le/a/a/b;
.super Ljava/lang/Object;

# interfaces
.implements Lf/C;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Le/a/a/a;->a(Le/a/a/c;Le/L;)Le/L;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field private a:Z

.field final synthetic b:Lf/k;

.field final synthetic c:Le/a/a/c;

.field final synthetic d:Lf/j;


# direct methods
.method constructor <init>(Lf/k;Le/a/a/c;Lf/j;)V
    .locals 0

    iput-object p1, p0, Le/a/a/b;->b:Lf/k;

    iput-object p2, p0, Le/a/a/b;->c:Le/a/a/c;

    iput-object p3, p0, Le/a/a/b;->d:Lf/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Lf/h;J)J
    .locals 8
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Le/a/a/b;->b:Lf/k;

    invoke-interface {v1, p1, p2, p3}, Lf/C;->b(Lf/h;J)J

    move-result-wide p2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v1, -0x1

    cmp-long v3, p2, v1

    if-nez v3, :cond_1

    iget-boolean p1, p0, Le/a/a/b;->a:Z

    if-nez p1, :cond_0

    iput-boolean v0, p0, Le/a/a/b;->a:Z

    iget-object p1, p0, Le/a/a/b;->d:Lf/j;

    invoke-interface {p1}, Lf/A;->close()V

    :cond_0
    return-wide v1

    :cond_1
    iget-object v0, p0, Le/a/a/b;->d:Lf/j;

    invoke-interface {v0}, Lf/j;->getBuffer()Lf/h;

    move-result-object v3

    invoke-virtual {p1}, Lf/h;->size()J

    move-result-wide v0

    sub-long v4, v0, p2

    move-object v2, p1

    move-wide v6, p2

    invoke-virtual/range {v2 .. v7}, Lf/h;->a(Lf/h;JJ)Lf/h;

    iget-object p1, p0, Le/a/a/b;->d:Lf/j;

    invoke-interface {p1}, Lf/j;->e()Lf/j;

    return-wide p2

    :catch_0
    move-exception p1

    iget-boolean p2, p0, Le/a/a/b;->a:Z

    if-nez p2, :cond_2

    iput-boolean v0, p0, Le/a/a/b;->a:Z

    iget-object p2, p0, Le/a/a/b;->c:Le/a/a/c;

    invoke-interface {p2}, Le/a/a/c;->abort()V

    :cond_2
    throw p1
.end method

.method public c()Lf/E;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/a/b;->b:Lf/k;

    invoke-interface {v0}, Lf/C;->c()Lf/E;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Le/a/a/b;->a:Z

    if-nez v0, :cond_0

    const/16 v0, 0x64

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p0, v0, v1}, Le/a/d;->a(Lf/C;ILjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Le/a/a/b;->a:Z

    iget-object v0, p0, Le/a/a/b;->c:Le/a/a/c;

    invoke-interface {v0}, Le/a/a/c;->abort()V

    :cond_0
    iget-object v0, p0, Le/a/a/b;->b:Lf/k;

    invoke-interface {v0}, Lf/C;->close()V

    return-void
.end method
