.class final Le/a/a/h;
.super Lkotlin/jvm/b/j;

# interfaces
.implements Lkotlin/jvm/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Le/a/a/e;->x()Lf/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/b/j;",
        "Lkotlin/jvm/a/b<",
        "Ljava/io/IOException;",
        "Lkotlin/r;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic b:Le/a/a/e;


# direct methods
.method constructor <init>(Le/a/a/e;)V
    .locals 0

    iput-object p1, p0, Le/a/a/h;->b:Le/a/a/e;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/b/j;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/io/IOException;

    invoke-virtual {p0, p1}, Le/a/a/h;->a(Ljava/io/IOException;)V

    sget-object p1, Lkotlin/r;->a:Lkotlin/r;

    return-object p1
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 1
    .param p1    # Ljava/io/IOException;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Le/a/a/h;->b:Le/a/a/e;

    invoke-static {p1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result p1

    sget-boolean v0, Lkotlin/s;->a:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Assertion failed"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    iget-object p1, p0, Le/a/a/h;->b:Le/a/a/e;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Le/a/a/e;->a(Le/a/a/e;Z)V

    return-void
.end method
