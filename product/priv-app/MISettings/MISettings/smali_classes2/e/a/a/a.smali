.class public final Le/a/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Le/D;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/a/a/a$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\u0008H\u0002J\u0010\u0010\u000c\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0010"
    }
    d2 = {
        "Lokhttp3/internal/cache/CacheInterceptor;",
        "Lokhttp3/Interceptor;",
        "cache",
        "Lokhttp3/Cache;",
        "(Lokhttp3/Cache;)V",
        "getCache$okhttp",
        "()Lokhttp3/Cache;",
        "cacheWritingResponse",
        "Lokhttp3/Response;",
        "cacheRequest",
        "Lokhttp3/internal/cache/CacheRequest;",
        "response",
        "intercept",
        "chain",
        "Lokhttp3/Interceptor$Chain;",
        "Companion",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final a:Le/a/a/a$a;


# instance fields
.field private final b:Le/e;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Le/a/a/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/a/a/a$a;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/a/a/a;->a:Le/a/a/a$a;

    return-void
.end method

.method public constructor <init>(Le/e;)V
    .locals 0
    .param p1    # Le/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/a/a/a;->b:Le/e;

    return-void
.end method

.method private final a(Le/a/a/c;Le/L;)Le/L;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    return-object p2

    :cond_0
    invoke-interface {p1}, Le/a/a/c;->a()Lf/A;

    move-result-object v0

    invoke-virtual {p2}, Le/L;->a()Le/N;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Le/N;->p()Lf/k;

    move-result-object v1

    invoke-static {v0}, Lf/s;->a(Lf/A;)Lf/j;

    move-result-object v0

    new-instance v3, Le/a/a/b;

    invoke-direct {v3, v1, p1, v0}, Le/a/a/b;-><init>(Lf/k;Le/a/a/c;Lf/j;)V

    const/4 p1, 0x2

    const-string v0, "Content-Type"

    invoke-static {p2, v0, v2, p1, v2}, Le/L;->a(Le/L;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Le/L;->a()Le/N;

    move-result-object v0

    invoke-virtual {v0}, Le/N;->a()J

    move-result-wide v0

    invoke-virtual {p2}, Le/L;->w()Le/L$a;

    move-result-object p2

    new-instance v2, Le/a/c/i;

    invoke-static {v3}, Lf/s;->a(Lf/C;)Lf/k;

    move-result-object v3

    invoke-direct {v2, p1, v0, v1, v3}, Le/a/c/i;-><init>(Ljava/lang/String;JLf/k;)V

    invoke-virtual {p2, v2}, Le/L$a;->a(Le/N;)Le/L$a;

    invoke-virtual {p2}, Le/L$a;->a()Le/L;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2
.end method


# virtual methods
.method public a(Le/D$a;)Le/L;
    .locals 6
    .param p1    # Le/D$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "chain"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/a/a;->b:Le/e;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Le/D$a;->d()Le/I;

    move-result-object v2

    invoke-virtual {v0, v2}, Le/e;->a(Le/I;)Le/L;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v4, Le/a/a/d$b;

    invoke-interface {p1}, Le/D$a;->d()Le/I;

    move-result-object v5

    invoke-direct {v4, v2, v3, v5, v0}, Le/a/a/d$b;-><init>(JLe/I;Le/L;)V

    invoke-virtual {v4}, Le/a/a/d$b;->a()Le/a/a/d;

    move-result-object v2

    invoke-virtual {v2}, Le/a/a/d;->b()Le/I;

    move-result-object v3

    invoke-virtual {v2}, Le/a/a/d;->a()Le/L;

    move-result-object v4

    iget-object v5, p0, Le/a/a/a;->b:Le/e;

    if-eqz v5, :cond_1

    invoke-virtual {v5, v2}, Le/e;->a(Le/a/a/d;)V

    :cond_1
    if-eqz v0, :cond_2

    if-nez v4, :cond_2

    invoke-virtual {v0}, Le/L;->a()Le/N;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v2}, Le/a/d;->a(Ljava/io/Closeable;)V

    :cond_2
    if-nez v3, :cond_3

    if-nez v4, :cond_3

    new-instance v0, Le/L$a;

    invoke-direct {v0}, Le/L$a;-><init>()V

    invoke-interface {p1}, Le/D$a;->d()Le/I;

    move-result-object p1

    invoke-virtual {v0, p1}, Le/L$a;->a(Le/I;)Le/L$a;

    sget-object p1, Le/G;->b:Le/G;

    invoke-virtual {v0, p1}, Le/L$a;->a(Le/G;)Le/L$a;

    const/16 p1, 0x1f8

    invoke-virtual {v0, p1}, Le/L$a;->a(I)Le/L$a;

    const-string p1, "Unsatisfiable Request (only-if-cached)"

    invoke-virtual {v0, p1}, Le/L$a;->a(Ljava/lang/String;)Le/L$a;

    sget-object p1, Le/a/d;->c:Le/N;

    invoke-virtual {v0, p1}, Le/L$a;->a(Le/N;)Le/L$a;

    const-wide/16 v1, -0x1

    invoke-virtual {v0, v1, v2}, Le/L$a;->b(J)Le/L$a;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Le/L$a;->a(J)Le/L$a;

    invoke-virtual {v0}, Le/L$a;->a()Le/L;

    move-result-object p1

    return-object p1

    :cond_3
    if-nez v3, :cond_5

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Le/L;->w()Le/L$a;

    move-result-object p1

    sget-object v0, Le/a/a/a;->a:Le/a/a/a$a;

    invoke-static {v0, v4}, Le/a/a/a$a;->a(Le/a/a/a$a;Le/L;)Le/L;

    move-result-object v0

    invoke-virtual {p1, v0}, Le/L$a;->a(Le/L;)Le/L$a;

    invoke-virtual {p1}, Le/L$a;->a()Le/L;

    move-result-object p1

    return-object p1

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_5
    :try_start_0
    invoke-interface {p1, v3}, Le/D$a;->a(Le/I;)Le/L;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_6

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Le/L;->a()Le/N;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, Le/a/d;->a(Ljava/io/Closeable;)V

    :cond_6
    if-eqz v4, :cond_a

    if-eqz p1, :cond_9

    invoke-virtual {p1}, Le/L;->q()I

    move-result v0

    const/16 v2, 0x130

    if-ne v0, v2, :cond_9

    invoke-virtual {v4}, Le/L;->w()Le/L$a;

    move-result-object v0

    sget-object v2, Le/a/a/a;->a:Le/a/a/a$a;

    invoke-virtual {v4}, Le/L;->t()Le/B;

    move-result-object v3

    invoke-virtual {p1}, Le/L;->t()Le/B;

    move-result-object v5

    invoke-static {v2, v3, v5}, Le/a/a/a$a;->a(Le/a/a/a$a;Le/B;Le/B;)Le/B;

    move-result-object v2

    invoke-virtual {v0, v2}, Le/L$a;->a(Le/B;)Le/L$a;

    invoke-virtual {p1}, Le/L;->B()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Le/L$a;->b(J)Le/L$a;

    invoke-virtual {p1}, Le/L;->z()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Le/L$a;->a(J)Le/L$a;

    sget-object v2, Le/a/a/a;->a:Le/a/a/a$a;

    invoke-static {v2, v4}, Le/a/a/a$a;->a(Le/a/a/a$a;Le/L;)Le/L;

    move-result-object v2

    invoke-virtual {v0, v2}, Le/L$a;->a(Le/L;)Le/L$a;

    sget-object v2, Le/a/a/a;->a:Le/a/a/a$a;

    invoke-static {v2, p1}, Le/a/a/a$a;->a(Le/a/a/a$a;Le/L;)Le/L;

    move-result-object v2

    invoke-virtual {v0, v2}, Le/L$a;->b(Le/L;)Le/L$a;

    invoke-virtual {v0}, Le/L$a;->a()Le/L;

    move-result-object v0

    invoke-virtual {p1}, Le/L;->a()Le/N;

    move-result-object p1

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Le/N;->close()V

    iget-object p1, p0, Le/a/a/a;->b:Le/e;

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Le/e;->p()V

    iget-object p1, p0, Le/a/a/a;->b:Le/e;

    invoke-virtual {p1, v4, v0}, Le/e;->a(Le/L;Le/L;)V

    return-object v0

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_8
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_9
    invoke-virtual {v4}, Le/L;->a()Le/N;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-static {v0}, Le/a/d;->a(Ljava/io/Closeable;)V

    :cond_a
    if-eqz p1, :cond_d

    invoke-virtual {p1}, Le/L;->w()Le/L$a;

    move-result-object v0

    sget-object v1, Le/a/a/a;->a:Le/a/a/a$a;

    invoke-static {v1, v4}, Le/a/a/a$a;->a(Le/a/a/a$a;Le/L;)Le/L;

    move-result-object v1

    invoke-virtual {v0, v1}, Le/L$a;->a(Le/L;)Le/L$a;

    sget-object v1, Le/a/a/a;->a:Le/a/a/a$a;

    invoke-static {v1, p1}, Le/a/a/a$a;->a(Le/a/a/a$a;Le/L;)Le/L;

    move-result-object p1

    invoke-virtual {v0, p1}, Le/L$a;->b(Le/L;)Le/L$a;

    invoke-virtual {v0}, Le/L$a;->a()Le/L;

    move-result-object p1

    iget-object v0, p0, Le/a/a/a;->b:Le/e;

    if-eqz v0, :cond_c

    invoke-static {p1}, Le/a/c/f;->a(Le/L;)Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Le/a/a/d;->a:Le/a/a/d$a;

    invoke-virtual {v0, p1, v3}, Le/a/a/d$a;->a(Le/L;Le/I;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Le/a/a/a;->b:Le/e;

    invoke-virtual {v0, p1}, Le/e;->a(Le/L;)Le/a/a/c;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Le/a/a/a;->a(Le/a/a/c;Le/L;)Le/L;

    move-result-object p1

    return-object p1

    :cond_b
    sget-object v0, Le/a/c/g;->a:Le/a/c/g;

    invoke-virtual {v3}, Le/I;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Le/a/c/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    :try_start_1
    iget-object v0, p0, Le/a/a/a;->b:Le/e;

    invoke-virtual {v0, v3}, Le/e;->b(Le/I;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    :cond_c
    return-object p1

    :cond_d
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :catchall_0
    move-exception p1

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Le/L;->a()Le/N;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-static {v0}, Le/a/d;->a(Ljava/io/Closeable;)V

    :cond_e
    throw p1
.end method
