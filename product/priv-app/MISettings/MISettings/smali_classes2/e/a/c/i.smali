.class public final Le/a/c/i;
.super Le/N;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:Lf/k;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLf/k;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lf/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "source"

    invoke-static {p4, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Le/N;-><init>()V

    iput-object p1, p0, Le/a/c/i;->b:Ljava/lang/String;

    iput-wide p2, p0, Le/a/c/i;->c:J

    iput-object p4, p0, Le/a/c/i;->d:Lf/k;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Le/a/c/i;->c:J

    return-wide v0
.end method

.method public b()Le/E;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/a/c/i;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v1, Le/E;->c:Le/E$a;

    invoke-virtual {v1, v0}, Le/E$a;->b(Ljava/lang/String;)Le/E;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public p()Lf/k;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/c/i;->d:Lf/k;

    return-object v0
.end method
