.class public final Le/a/c/a;
.super Ljava/lang/Object;

# interfaces
.implements Le/D;


# instance fields
.field private final a:Le/s;


# direct methods
.method public constructor <init>(Le/s;)V
    .locals 1
    .param p1    # Le/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cookieJar"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/a/c/a;->a:Le/s;

    return-void
.end method

.method private final a(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Le/q;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-ltz v1, :cond_1

    check-cast v2, Le/q;

    if-lez v1, :cond_0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v2}, Le/q;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Le/q;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v3

    goto :goto_0

    :cond_1
    invoke-static {}, Lkotlin/a/h;->b()V

    const/4 p1, 0x0

    throw p1

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "StringBuilder().apply(builderAction).toString()"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public a(Le/D$a;)Le/L;
    .locals 12
    .param p1    # Le/D$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "chain"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Le/D$a;->d()Le/I;

    move-result-object v0

    invoke-virtual {v0}, Le/I;->g()Le/I$a;

    move-result-object v1

    invoke-virtual {v0}, Le/I;->a()Le/K;

    move-result-object v2

    const-string v3, "Content-Type"

    const-wide/16 v4, -0x1

    const-string v6, "Content-Length"

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Le/K;->b()Le/E;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Le/E;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v3, v7}, Le/I$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    :cond_0
    invoke-virtual {v2}, Le/K;->a()J

    move-result-wide v7

    cmp-long v2, v7, v4

    const-string v9, "Transfer-Encoding"

    if-eqz v2, :cond_1

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Le/I$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    invoke-virtual {v1, v9}, Le/I$a;->a(Ljava/lang/String;)Le/I$a;

    goto :goto_0

    :cond_1
    const-string v2, "chunked"

    invoke-virtual {v1, v9, v2}, Le/I$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    invoke-virtual {v1, v6}, Le/I$a;->a(Ljava/lang/String;)Le/I$a;

    :cond_2
    :goto_0
    const-string v2, "Host"

    invoke-virtual {v0, v2}, Le/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    if-nez v7, :cond_3

    invoke-virtual {v0}, Le/I;->h()Le/C;

    move-result-object v7

    invoke-static {v7, v8, v9, v10}, Le/a/d;->a(Le/C;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v2, v7}, Le/I$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    :cond_3
    const-string v2, "Connection"

    invoke-virtual {v0, v2}, Le/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_4

    const-string v7, "Keep-Alive"

    invoke-virtual {v1, v2, v7}, Le/I$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    :cond_4
    const-string v2, "Accept-Encoding"

    invoke-virtual {v0, v2}, Le/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v11, "gzip"

    if-nez v7, :cond_5

    const-string v7, "Range"

    invoke-virtual {v0, v7}, Le/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_5

    invoke-virtual {v1, v2, v11}, Le/I$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    move v8, v9

    :cond_5
    iget-object v2, p0, Le/a/c/a;->a:Le/s;

    invoke-virtual {v0}, Le/I;->h()Le/C;

    move-result-object v7

    invoke-interface {v2, v7}, Le/s;->a(Le/C;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    xor-int/2addr v7, v9

    if-eqz v7, :cond_6

    invoke-direct {p0, v2}, Le/a/c/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "Cookie"

    invoke-virtual {v1, v7, v2}, Le/I$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    :cond_6
    const-string v2, "User-Agent"

    invoke-virtual {v0, v2}, Le/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_7

    const-string v7, "okhttp/4.2.2"

    invoke-virtual {v1, v2, v7}, Le/I$a;->b(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    :cond_7
    invoke-virtual {v1}, Le/I$a;->a()Le/I;

    move-result-object v1

    invoke-interface {p1, v1}, Le/D$a;->a(Le/I;)Le/L;

    move-result-object p1

    iget-object v1, p0, Le/a/c/a;->a:Le/s;

    invoke-virtual {v0}, Le/I;->h()Le/C;

    move-result-object v2

    invoke-virtual {p1}, Le/L;->t()Le/B;

    move-result-object v7

    invoke-static {v1, v2, v7}, Le/a/c/f;->a(Le/s;Le/C;Le/B;)V

    invoke-virtual {p1}, Le/L;->w()Le/L$a;

    move-result-object v1

    invoke-virtual {v1, v0}, Le/L$a;->a(Le/I;)Le/L$a;

    if-eqz v8, :cond_8

    const/4 v0, 0x2

    const-string v2, "Content-Encoding"

    invoke-static {p1, v2, v10, v0, v10}, Le/L;->a(Le/L;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v11, v7, v9}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-static {p1}, Le/a/c/f;->a(Le/L;)Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {p1}, Le/L;->a()Le/N;

    move-result-object v7

    if-eqz v7, :cond_8

    new-instance v8, Lf/p;

    invoke-virtual {v7}, Le/N;->p()Lf/k;

    move-result-object v7

    invoke-direct {v8, v7}, Lf/p;-><init>(Lf/C;)V

    invoke-virtual {p1}, Le/L;->t()Le/B;

    move-result-object v7

    invoke-virtual {v7}, Le/B;->a()Le/B$a;

    move-result-object v7

    invoke-virtual {v7, v2}, Le/B$a;->c(Ljava/lang/String;)Le/B$a;

    invoke-virtual {v7, v6}, Le/B$a;->c(Ljava/lang/String;)Le/B$a;

    invoke-virtual {v7}, Le/B$a;->a()Le/B;

    move-result-object v2

    invoke-virtual {v1, v2}, Le/L$a;->a(Le/B;)Le/L$a;

    invoke-static {p1, v3, v10, v0, v10}, Le/L;->a(Le/L;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Le/a/c/i;

    invoke-static {v8}, Lf/s;->a(Lf/C;)Lf/k;

    move-result-object v2

    invoke-direct {v0, p1, v4, v5, v2}, Le/a/c/i;-><init>(Ljava/lang/String;JLf/k;)V

    invoke-virtual {v1, v0}, Le/L$a;->a(Le/N;)Le/L$a;

    :cond_8
    invoke-virtual {v1}, Le/L$a;->a()Le/L;

    move-result-object p1

    return-object p1
.end method
