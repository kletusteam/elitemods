.class public final Le/a/c/k;
.super Ljava/lang/Object;

# interfaces
.implements Le/D;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/a/c/k$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002J\u001c\u0010\u000b\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0002J\u0010\u0010\u000e\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0012H\u0002J(\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u0006H\u0002J\u0018\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u0006H\u0002J\u0018\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\u001cH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lokhttp3/internal/http/RetryAndFollowUpInterceptor;",
        "Lokhttp3/Interceptor;",
        "client",
        "Lokhttp3/OkHttpClient;",
        "(Lokhttp3/OkHttpClient;)V",
        "buildRedirectRequest",
        "Lokhttp3/Request;",
        "userResponse",
        "Lokhttp3/Response;",
        "method",
        "",
        "followUpRequest",
        "route",
        "Lokhttp3/Route;",
        "intercept",
        "chain",
        "Lokhttp3/Interceptor$Chain;",
        "isRecoverable",
        "",
        "e",
        "Ljava/io/IOException;",
        "requestSendStarted",
        "recover",
        "transmitter",
        "Lokhttp3/internal/connection/Transmitter;",
        "userRequest",
        "requestIsOneShot",
        "retryAfter",
        "",
        "defaultDelay",
        "Companion",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final a:Le/a/c/k$a;


# instance fields
.field private final b:Le/F;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Le/a/c/k$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/a/c/k$a;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/a/c/k;->a:Le/a/c/k$a;

    return-void
.end method

.method public constructor <init>(Le/F;)V
    .locals 1
    .param p1    # Le/F;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "client"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/a/c/k;->b:Le/F;

    return-void
.end method

.method private final a(Le/L;I)I
    .locals 3

    const/4 v0, 0x0

    const-string v1, "Retry-After"

    const/4 v2, 0x2

    invoke-static {p1, v1, v0, v2, v0}, Le/L;->a(Le/L;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    new-instance p2, Lkotlin/g/f;

    const-string v0, "\\d+"

    invoke-direct {p2, v0}, Lkotlin/g/f;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Lkotlin/g/f;->a(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    const-string p2, "Integer.valueOf(header)"

    invoke-static {p1, p2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_0
    const p1, 0x7fffffff

    return p1

    :cond_1
    return p2
.end method

.method private final a(Le/L;Le/O;)Le/I;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Le/L;->q()I

    move-result v0

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v1

    invoke-virtual {v1}, Le/I;->f()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x133

    const/4 v3, 0x0

    if-eq v0, v2, :cond_c

    const/16 v2, 0x134

    if-eq v0, v2, :cond_c

    const/16 v2, 0x191

    if-eq v0, v2, :cond_b

    const/16 v2, 0x1f7

    if-eq v0, v2, :cond_8

    const/16 v2, 0x197

    if-eq v0, v2, :cond_5

    const/16 p2, 0x198

    if-eq v0, p2, :cond_0

    packed-switch v0, :pswitch_data_0

    return-object v3

    :pswitch_0
    invoke-direct {p0, p1, v1}, Le/a/c/k;->a(Le/L;Ljava/lang/String;)Le/I;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object v0, p0, Le/a/c/k;->b:Le/F;

    invoke-virtual {v0}, Le/F;->y()Z

    move-result v0

    if-nez v0, :cond_1

    return-object v3

    :cond_1
    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v0

    invoke-virtual {v0}, Le/I;->a()Le/K;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Le/K;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    return-object v3

    :cond_2
    invoke-virtual {p1}, Le/L;->x()Le/L;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Le/L;->q()I

    move-result v0

    if-ne v0, p2, :cond_3

    return-object v3

    :cond_3
    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, Le/a/c/k;->a(Le/L;I)I

    move-result p2

    if-lez p2, :cond_4

    return-object v3

    :cond_4
    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object p1

    return-object p1

    :cond_5
    if-eqz p2, :cond_7

    invoke-virtual {p2}, Le/O;->b()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Le/a/c/k;->b:Le/F;

    invoke-virtual {v0}, Le/F;->v()Le/c;

    move-result-object v0

    invoke-interface {v0, p2, p1}, Le/c;->a(Le/O;Le/L;)Le/I;

    move-result-object p1

    return-object p1

    :cond_6
    new-instance p1, Ljava/net/ProtocolException;

    const-string p2, "Received HTTP_PROXY_AUTH (407) code while not using proxy"

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v3

    :cond_8
    invoke-virtual {p1}, Le/L;->x()Le/L;

    move-result-object p2

    if-eqz p2, :cond_9

    invoke-virtual {p2}, Le/L;->q()I

    move-result p2

    if-ne p2, v2, :cond_9

    return-object v3

    :cond_9
    const p2, 0x7fffffff

    invoke-direct {p0, p1, p2}, Le/a/c/k;->a(Le/L;I)I

    move-result p2

    if-nez p2, :cond_a

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object p1

    return-object p1

    :cond_a
    return-object v3

    :cond_b
    iget-object v0, p0, Le/a/c/k;->b:Le/F;

    invoke-virtual {v0}, Le/F;->c()Le/c;

    move-result-object v0

    invoke-interface {v0, p2, p1}, Le/c;->a(Le/O;Le/L;)Le/I;

    move-result-object p1

    return-object p1

    :cond_c
    const-string p2, "GET"

    invoke-static {v1, p2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_d

    const-string p2, "HEAD"

    invoke-static {v1, p2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_d

    return-object v3

    :cond_d
    invoke-direct {p0, p1, v1}, Le/a/c/k;->a(Le/L;Ljava/lang/String;)Le/I;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private final a(Le/L;Ljava/lang/String;)Le/I;
    .locals 5

    iget-object v0, p0, Le/a/c/k;->b:Le/F;

    invoke-virtual {v0}, Le/F;->n()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x2

    const-string v2, "Location"

    invoke-static {p1, v2, v1, v0, v1}, Le/L;->a(Le/L;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v2

    invoke-virtual {v2}, Le/I;->h()Le/C;

    move-result-object v2

    invoke-virtual {v2, v0}, Le/C;->b(Ljava/lang/String;)Le/C;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Le/C;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v3

    invoke-virtual {v3}, Le/I;->h()Le/C;

    move-result-object v3

    invoke-virtual {v3}, Le/C;->n()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Le/a/c/k;->b:Le/F;

    invoke-virtual {v2}, Le/F;->o()Z

    move-result v2

    if-nez v2, :cond_1

    return-object v1

    :cond_1
    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v2

    invoke-virtual {v2}, Le/I;->g()Le/I$a;

    move-result-object v2

    invoke-static {p2}, Le/a/c/g;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Le/a/c/g;->a:Le/a/c/g;

    invoke-virtual {v3, p2}, Le/a/c/g;->d(Ljava/lang/String;)Z

    move-result v3

    sget-object v4, Le/a/c/g;->a:Le/a/c/g;

    invoke-virtual {v4, p2}, Le/a/c/g;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string p2, "GET"

    invoke-virtual {v2, p2, v1}, Le/I$a;->a(Ljava/lang/String;Le/K;)Le/I$a;

    goto :goto_0

    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v1

    invoke-virtual {v1}, Le/I;->a()Le/K;

    move-result-object v1

    :cond_3
    invoke-virtual {v2, p2, v1}, Le/I$a;->a(Ljava/lang/String;Le/K;)Le/I$a;

    :goto_0
    if-nez v3, :cond_4

    const-string p2, "Transfer-Encoding"

    invoke-virtual {v2, p2}, Le/I$a;->a(Ljava/lang/String;)Le/I$a;

    const-string p2, "Content-Length"

    invoke-virtual {v2, p2}, Le/I$a;->a(Ljava/lang/String;)Le/I$a;

    const-string p2, "Content-Type"

    invoke-virtual {v2, p2}, Le/I$a;->a(Ljava/lang/String;)Le/I$a;

    :cond_4
    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object p1

    invoke-virtual {p1}, Le/I;->h()Le/C;

    move-result-object p1

    invoke-static {p1, v0}, Le/a/d;->a(Le/C;Le/C;)Z

    move-result p1

    if-nez p1, :cond_5

    const-string p1, "Authorization"

    invoke-virtual {v2, p1}, Le/I$a;->a(Ljava/lang/String;)Le/I$a;

    :cond_5
    invoke-virtual {v2, v0}, Le/I$a;->a(Le/C;)Le/I$a;

    invoke-virtual {v2}, Le/I$a;->a()Le/I;

    move-result-object p1

    return-object p1

    :cond_6
    return-object v1
.end method

.method private final a(Ljava/io/IOException;Le/I;)Z
    .locals 0

    invoke-virtual {p2}, Le/I;->a()Le/K;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Le/K;->d()Z

    move-result p2

    if-nez p2, :cond_1

    :cond_0
    instance-of p1, p1, Ljava/io/FileNotFoundException;

    if-eqz p1, :cond_2

    :cond_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final a(Ljava/io/IOException;Le/a/b/n;ZLe/I;)Z
    .locals 2

    iget-object v0, p0, Le/a/c/k;->b:Le/F;

    invoke-virtual {v0}, Le/F;->y()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    if-eqz p3, :cond_1

    invoke-direct {p0, p1, p4}, Le/a/c/k;->a(Ljava/io/IOException;Le/I;)Z

    move-result p4

    if-eqz p4, :cond_1

    return v1

    :cond_1
    invoke-direct {p0, p1, p3}, Le/a/c/k;->a(Ljava/io/IOException;Z)Z

    move-result p1

    if-nez p1, :cond_2

    return v1

    :cond_2
    invoke-virtual {p2}, Le/a/b/n;->b()Z

    move-result p1

    if-nez p1, :cond_3

    return v1

    :cond_3
    const/4 p1, 0x1

    return p1
.end method

.method private final a(Ljava/io/IOException;Z)Z
    .locals 3

    instance-of v0, p1, Ljava/net/ProtocolException;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    instance-of v0, p1, Ljava/io/InterruptedIOException;

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    instance-of p1, p1, Ljava/net/SocketTimeoutException;

    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    move v1, v2

    :cond_1
    return v1

    :cond_2
    instance-of p2, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz p2, :cond_3

    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    instance-of p2, p2, Ljava/security/cert/CertificateException;

    if-eqz p2, :cond_3

    return v1

    :cond_3
    instance-of p1, p1, Ljavax/net/ssl/SSLPeerUnverifiedException;

    if-eqz p1, :cond_4

    return v1

    :cond_4
    return v2
.end method


# virtual methods
.method public a(Le/D$a;)Le/L;
    .locals 8
    .param p1    # Le/D$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "chain"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Le/D$a;->d()Le/I;

    move-result-object v0

    check-cast p1, Le/a/c/h;

    invoke-virtual {p1}, Le/a/c/h;->f()Le/a/b/n;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    move v5, v2

    move-object v4, v3

    :goto_0
    invoke-virtual {v1, v0}, Le/a/b/n;->a(Le/I;)V

    invoke-virtual {v1}, Le/a/b/n;->g()Z

    move-result v6

    if-nez v6, :cond_b

    :try_start_0
    invoke-virtual {p1, v0, v1, v3}, Le/a/c/h;->a(Le/I;Le/a/b/n;Le/a/b/c;)Le/L;

    move-result-object v0
    :try_end_0
    .catch Le/a/b/l; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Le/L;->w()Le/L$a;

    move-result-object v0

    invoke-virtual {v4}, Le/L;->w()Le/L$a;

    move-result-object v4

    invoke-virtual {v4, v3}, Le/L$a;->a(Le/N;)Le/L$a;

    invoke-virtual {v4}, Le/L$a;->a()Le/L;

    move-result-object v4

    invoke-virtual {v0, v4}, Le/L$a;->c(Le/L;)Le/L$a;

    invoke-virtual {v0}, Le/L$a;->a()Le/L;

    move-result-object v0

    :cond_0
    move-object v4, v0

    invoke-virtual {v4}, Le/L;->r()Le/a/b/c;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Le/a/b/c;->b()Le/a/b/e;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Le/a/b/e;->j()Le/O;

    move-result-object v6

    goto :goto_1

    :cond_1
    move-object v6, v3

    :goto_1
    invoke-direct {p0, v4, v6}, Le/a/c/k;->a(Le/L;Le/O;)Le/I;

    move-result-object v6

    if-nez v6, :cond_3

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Le/a/b/c;->f()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {v1}, Le/a/b/n;->i()V

    :cond_2
    return-object v4

    :cond_3
    invoke-virtual {v6}, Le/I;->a()Le/K;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {v7}, Le/K;->d()Z

    move-result v7

    if-eqz v7, :cond_4

    return-object v4

    :cond_4
    invoke-virtual {v4}, Le/L;->a()Le/N;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-static {v7}, Le/a/d;->a(Ljava/io/Closeable;)V

    :cond_5
    invoke-virtual {v1}, Le/a/b/n;->f()Z

    move-result v7

    if-eqz v7, :cond_6

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Le/a/b/c;->c()V

    :cond_6
    add-int/lit8 v5, v5, 0x1

    const/16 v0, 0x14

    if-gt v5, v0, :cond_7

    move-object v0, v6

    goto :goto_0

    :cond_7
    new-instance p1, Ljava/net/ProtocolException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Too many follow-up requests: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_0
    move-exception v6

    :try_start_1
    instance-of v7, v6, Le/a/e/a;

    if-nez v7, :cond_8

    const/4 v7, 0x1

    goto :goto_2

    :cond_8
    move v7, v2

    :goto_2
    invoke-direct {p0, v6, v1, v7, v0}, Le/a/c/k;->a(Ljava/io/IOException;Le/a/b/n;ZLe/I;)Z

    move-result v7

    if-eqz v7, :cond_9

    goto :goto_3

    :cond_9
    throw v6

    :catch_1
    move-exception v6

    invoke-virtual {v6}, Le/a/b/l;->b()Ljava/io/IOException;

    move-result-object v7

    invoke-direct {p0, v7, v1, v2, v0}, Le/a/c/k;->a(Ljava/io/IOException;Le/a/b/n;ZLe/I;)Z

    move-result v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v7, :cond_a

    :goto_3
    invoke-virtual {v1}, Le/a/b/n;->d()V

    goto/16 :goto_0

    :cond_a
    :try_start_2
    invoke-virtual {v6}, Le/a/b/l;->a()Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_4
    invoke-virtual {v1}, Le/a/b/n;->d()V

    throw p1

    :cond_b
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Canceled"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
