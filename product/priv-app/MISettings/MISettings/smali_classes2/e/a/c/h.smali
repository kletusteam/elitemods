.class public final Le/a/c/h;
.super Ljava/lang/Object;

# interfaces
.implements Le/D$a;


# instance fields
.field private a:I

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le/D;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Le/a/b/n;

.field private final d:Le/a/b/c;

.field private final e:I

.field private final f:Le/I;

.field private final g:Le/h;

.field private final h:I

.field private final i:I

.field private final j:I


# direct methods
.method public constructor <init>(Ljava/util/List;Le/a/b/n;Le/a/b/c;ILe/I;Le/h;III)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/a/b/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Le/a/b/c;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Le/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Le/D;",
            ">;",
            "Le/a/b/n;",
            "Le/a/b/c;",
            "I",
            "Le/I;",
            "Le/h;",
            "III)V"
        }
    .end annotation

    const-string v0, "interceptors"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transmitter"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "request"

    invoke-static {p5, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "call"

    invoke-static {p6, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/a/c/h;->b:Ljava/util/List;

    iput-object p2, p0, Le/a/c/h;->c:Le/a/b/n;

    iput-object p3, p0, Le/a/c/h;->d:Le/a/b/c;

    iput p4, p0, Le/a/c/h;->e:I

    iput-object p5, p0, Le/a/c/h;->f:Le/I;

    iput-object p6, p0, Le/a/c/h;->g:Le/h;

    iput p7, p0, Le/a/c/h;->h:I

    iput p8, p0, Le/a/c/h;->i:I

    iput p9, p0, Le/a/c/h;->j:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Le/a/c/h;->i:I

    return v0
.end method

.method public a(Le/I;)Le/L;
    .locals 2
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/a/c/h;->c:Le/a/b/n;

    iget-object v1, p0, Le/a/c/h;->d:Le/a/b/c;

    invoke-virtual {p0, p1, v0, v1}, Le/a/c/h;->a(Le/I;Le/a/b/n;Le/a/b/c;)Le/L;

    move-result-object p1

    return-object p1
.end method

.method public final a(Le/I;Le/a/b/n;Le/a/b/c;)Le/L;
    .locals 16
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/a/b/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Le/a/b/c;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "request"

    move-object/from16 v7, p1

    invoke-static {v7, v1}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "transmitter"

    move-object/from16 v4, p2

    invoke-static {v4, v1}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget v1, v0, Le/a/c/h;->e:I

    iget-object v2, v0, Le/a/c/h;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_d

    iget v1, v0, Le/a/c/h;->a:I

    const/4 v12, 0x1

    add-int/2addr v1, v12

    iput v1, v0, Le/a/c/h;->a:I

    iget-object v1, v0, Le/a/c/h;->d:Le/a/b/c;

    const/4 v13, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Le/a/b/c;->b()Le/a/b/e;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual/range {p1 .. p1}, Le/I;->h()Le/C;

    move-result-object v2

    invoke-virtual {v1, v2}, Le/a/b/e;->a(Le/C;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v13

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v1, 0x0

    throw v1

    :cond_2
    :goto_0
    move v1, v12

    :goto_1
    const-string v14, "network interceptor "

    if-eqz v1, :cond_c

    iget-object v1, v0, Le/a/c/h;->d:Le/a/b/c;

    if-eqz v1, :cond_4

    iget v1, v0, Le/a/c/h;->a:I

    if-gt v1, v12, :cond_3

    goto :goto_2

    :cond_3
    move v1, v13

    goto :goto_3

    :cond_4
    :goto_2
    move v1, v12

    :goto_3
    const-string v15, " must call proceed() exactly once"

    if-eqz v1, :cond_b

    new-instance v1, Le/a/c/h;

    iget-object v3, v0, Le/a/c/h;->b:Ljava/util/List;

    iget v2, v0, Le/a/c/h;->e:I

    add-int/lit8 v6, v2, 0x1

    iget-object v8, v0, Le/a/c/h;->g:Le/h;

    iget v9, v0, Le/a/c/h;->h:I

    iget v10, v0, Le/a/c/h;->i:I

    iget v11, v0, Le/a/c/h;->j:I

    move-object v2, v1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v7, p1

    invoke-direct/range {v2 .. v11}, Le/a/c/h;-><init>(Ljava/util/List;Le/a/b/n;Le/a/b/c;ILe/I;Le/h;III)V

    iget-object v2, v0, Le/a/c/h;->b:Ljava/util/List;

    iget v3, v0, Le/a/c/h;->e:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Le/D;

    invoke-interface {v2, v1}, Le/D;->a(Le/D$a;)Le/L;

    move-result-object v3

    const-string v4, "interceptor "

    if-eqz v3, :cond_a

    if-eqz p3, :cond_6

    iget v5, v0, Le/a/c/h;->e:I

    add-int/2addr v5, v12

    iget-object v6, v0, Le/a/c/h;->b:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_6

    iget v1, v1, Le/a/c/h;->a:I

    if-ne v1, v12, :cond_5

    goto :goto_4

    :cond_5
    move v1, v13

    goto :goto_5

    :cond_6
    :goto_4
    move v1, v12

    :goto_5
    if-eqz v1, :cond_9

    invoke-virtual {v3}, Le/L;->a()Le/N;

    move-result-object v1

    if-eqz v1, :cond_7

    goto :goto_6

    :cond_7
    move v12, v13

    :goto_6
    if-eqz v12, :cond_8

    return-object v3

    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " returned a response with no body"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_a
    new-instance v1, Ljava/lang/NullPointerException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " returned null"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Le/a/c/h;->b:Ljava/util/List;

    iget v3, v0, Le/a/c/h;->e:I

    sub-int/2addr v3, v12

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Le/D;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Le/a/c/h;->b:Ljava/util/List;

    iget v3, v0, Le/a/c/h;->e:I

    sub-int/2addr v3, v12

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Le/D;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " must retain the same host and port"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_d
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method public b()I
    .locals 1

    iget v0, p0, Le/a/c/h;->j:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Le/a/c/h;->h:I

    return v0
.end method

.method public d()Le/I;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/c/h;->f:Le/I;

    return-object v0
.end method

.method public final e()Le/a/b/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/c/h;->d:Le/a/b/c;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0
.end method

.method public final f()Le/a/b/n;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/a/c/h;->c:Le/a/b/n;

    return-object v0
.end method
