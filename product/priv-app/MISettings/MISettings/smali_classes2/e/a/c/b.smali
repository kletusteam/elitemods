.class public final Le/a/c/b;
.super Ljava/lang/Object;

# interfaces
.implements Le/D;


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Le/a/c/b;->a:Z

    return-void
.end method


# virtual methods
.method public a(Le/D$a;)Le/L;
    .locals 10
    .param p1    # Le/D$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "chain"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Le/a/c/h;

    invoke-virtual {p1}, Le/a/c/h;->e()Le/a/b/c;

    move-result-object v0

    invoke-virtual {p1}, Le/a/c/h;->d()Le/I;

    move-result-object p1

    invoke-virtual {p1}, Le/I;->a()Le/K;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, p1}, Le/a/b/c;->a(Le/I;)V

    invoke-virtual {p1}, Le/I;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Le/a/c/g;->b(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz v4, :cond_4

    if-eqz v1, :cond_4

    const-string v4, "Expect"

    invoke-virtual {p1, v4}, Le/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v8, "100-continue"

    invoke-static {v8, v4, v6}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Le/a/b/c;->e()V

    invoke-virtual {v0}, Le/a/b/c;->i()V

    invoke-virtual {v0, v6}, Le/a/b/c;->a(Z)Le/L$a;

    move-result-object v4

    move v8, v6

    goto :goto_0

    :cond_0
    move v8, v5

    move-object v4, v7

    :goto_0
    if-nez v4, :cond_2

    invoke-virtual {v1}, Le/K;->c()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v0}, Le/a/b/c;->e()V

    invoke-virtual {v0, p1, v6}, Le/a/b/c;->a(Le/I;Z)Lf/A;

    move-result-object v9

    invoke-static {v9}, Lf/s;->a(Lf/A;)Lf/j;

    move-result-object v9

    invoke-virtual {v1, v9}, Le/K;->a(Lf/j;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v0, p1, v5}, Le/a/b/c;->a(Le/I;Z)Lf/A;

    move-result-object v9

    invoke-static {v9}, Lf/s;->a(Lf/A;)Lf/j;

    move-result-object v9

    invoke-virtual {v1, v9}, Le/K;->a(Lf/j;)V

    invoke-interface {v9}, Lf/A;->close()V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Le/a/b/c;->h()V

    invoke-virtual {v0}, Le/a/b/c;->b()Le/a/b/e;

    move-result-object v9

    if-eqz v9, :cond_3

    invoke-virtual {v9}, Le/a/b/e;->h()Z

    move-result v9

    if-nez v9, :cond_5

    invoke-virtual {v0}, Le/a/b/c;->g()V

    goto :goto_1

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v7

    :cond_4
    invoke-virtual {v0}, Le/a/b/c;->h()V

    move v8, v5

    move-object v4, v7

    :cond_5
    :goto_1
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Le/K;->c()Z

    move-result v1

    if-nez v1, :cond_7

    :cond_6
    invoke-virtual {v0}, Le/a/b/c;->d()V

    :cond_7
    if-nez v8, :cond_8

    invoke-virtual {v0}, Le/a/b/c;->i()V

    :cond_8
    if-nez v4, :cond_a

    invoke-virtual {v0, v5}, Le/a/b/c;->a(Z)Le/L$a;

    move-result-object v4

    if-eqz v4, :cond_9

    goto :goto_2

    :cond_9
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v7

    :cond_a
    :goto_2
    invoke-virtual {v4, p1}, Le/L$a;->a(Le/I;)Le/L$a;

    invoke-virtual {v0}, Le/a/b/c;->b()Le/a/b/e;

    move-result-object v1

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Le/a/b/e;->g()Le/A;

    move-result-object v1

    invoke-virtual {v4, v1}, Le/L$a;->a(Le/A;)Le/L$a;

    invoke-virtual {v4, v2, v3}, Le/L$a;->b(J)Le/L$a;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Le/L$a;->a(J)Le/L$a;

    invoke-virtual {v4}, Le/L$a;->a()Le/L;

    move-result-object v1

    invoke-virtual {v1}, Le/L;->q()I

    move-result v4

    const/16 v8, 0x64

    if-ne v4, v8, :cond_d

    invoke-virtual {v0, v5}, Le/a/b/c;->a(Z)Le/L$a;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {v1, p1}, Le/L$a;->a(Le/I;)Le/L$a;

    invoke-virtual {v0}, Le/a/b/c;->b()Le/a/b/e;

    move-result-object p1

    if-eqz p1, :cond_b

    invoke-virtual {p1}, Le/a/b/e;->g()Le/A;

    move-result-object p1

    invoke-virtual {v1, p1}, Le/L$a;->a(Le/A;)Le/L$a;

    invoke-virtual {v1, v2, v3}, Le/L$a;->b(J)Le/L$a;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Le/L$a;->a(J)Le/L$a;

    invoke-virtual {v1}, Le/L$a;->a()Le/L;

    move-result-object v1

    invoke-virtual {v1}, Le/L;->q()I

    move-result v4

    goto :goto_3

    :cond_b
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v7

    :cond_c
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v7

    :cond_d
    :goto_3
    invoke-virtual {v0, v1}, Le/a/b/c;->b(Le/L;)V

    iget-boolean p1, p0, Le/a/c/b;->a:Z

    if-eqz p1, :cond_e

    const/16 p1, 0x65

    if-ne v4, p1, :cond_e

    invoke-virtual {v1}, Le/L;->w()Le/L$a;

    move-result-object p1

    sget-object v1, Le/a/d;->c:Le/N;

    invoke-virtual {p1, v1}, Le/L$a;->a(Le/N;)Le/L$a;

    invoke-virtual {p1}, Le/L$a;->a()Le/L;

    move-result-object p1

    goto :goto_4

    :cond_e
    invoke-virtual {v1}, Le/L;->w()Le/L$a;

    move-result-object p1

    invoke-virtual {v0, v1}, Le/a/b/c;->a(Le/L;)Le/N;

    move-result-object v1

    invoke-virtual {p1, v1}, Le/L$a;->a(Le/N;)Le/L$a;

    invoke-virtual {p1}, Le/L$a;->a()Le/L;

    move-result-object p1

    :goto_4
    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v1

    const-string v2, "Connection"

    invoke-virtual {v1, v2}, Le/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "close"

    invoke-static {v3, v1, v6}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_f

    const/4 v1, 0x2

    invoke-static {p1, v2, v7, v1, v7}, Le/L;->a(Le/L;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1, v6}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_10

    :cond_f
    invoke-virtual {v0}, Le/a/b/c;->g()V

    :cond_10
    const/16 v0, 0xcc

    if-eq v4, v0, :cond_11

    const/16 v0, 0xcd

    if-ne v4, v0, :cond_14

    :cond_11
    invoke-virtual {p1}, Le/L;->a()Le/N;

    move-result-object v0

    if-eqz v0, :cond_12

    invoke-virtual {v0}, Le/N;->a()J

    move-result-wide v0

    goto :goto_5

    :cond_12
    const-wide/16 v0, -0x1

    :goto_5
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_14

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HTTP "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " had non-zero Content-Length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Le/L;->a()Le/N;

    move-result-object p1

    if-eqz p1, :cond_13

    invoke-virtual {p1}, Le/N;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    :cond_13
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    return-object p1

    :cond_15
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v7
.end method
