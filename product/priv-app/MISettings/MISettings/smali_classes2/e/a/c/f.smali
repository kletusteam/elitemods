.class public final Le/a/c/f;
.super Ljava/lang/Object;


# annotations
.annotation build Lkotlin/jvm/JvmName;
    name = "HttpHeaders"
.end annotation


# static fields
.field private static final a:Lf/l;

.field private static final b:Lf/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lf/l;->b:Lf/l$a;

    const-string v1, "\"\\"

    invoke-virtual {v0, v1}, Lf/l$a;->c(Ljava/lang/String;)Lf/l;

    move-result-object v0

    sput-object v0, Le/a/c/f;->a:Lf/l;

    sget-object v0, Lf/l;->b:Lf/l$a;

    const-string v1, "\t ,="

    invoke-virtual {v0, v1}, Lf/l$a;->c(Ljava/lang/String;)Lf/l;

    move-result-object v0

    sput-object v0, Le/a/c/f;->b:Lf/l;

    return-void
.end method

.method public static final a(Le/s;Le/C;Le/B;)V
    .locals 1
    .param p0    # Le/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Le/C;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/B;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$this$receiveHeaders"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "headers"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Le/s;->a:Le/s;

    if-ne p0, v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Le/q;->e:Le/q$a;

    invoke-virtual {v0, p1, p2}, Le/q$a;->a(Le/C;Le/B;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    invoke-interface {p0, p1, p2}, Le/s;->a(Le/C;Ljava/util/List;)V

    return-void
.end method

.method public static final a(Le/L;)Z
    .locals 8
    .param p0    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$this$promisesBody"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Le/L;->A()Le/I;

    move-result-object v0

    invoke-virtual {v0}, Le/I;->f()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HEAD"

    invoke-static {v0, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Le/L;->q()I

    move-result v0

    const/16 v2, 0x64

    const/4 v3, 0x1

    if-lt v0, v2, :cond_1

    const/16 v2, 0xc8

    if-lt v0, v2, :cond_2

    :cond_1
    const/16 v2, 0xcc

    if-eq v0, v2, :cond_2

    const/16 v2, 0x130

    if-eq v0, v2, :cond_2

    return v3

    :cond_2
    invoke-static {p0}, Le/a/d;->a(Le/L;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_4

    const/4 v0, 0x2

    const/4 v2, 0x0

    const-string v4, "Transfer-Encoding"

    invoke-static {p0, v4, v2, v0, v2}, Le/L;->a(Le/L;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "chunked"

    invoke-static {v0, p0, v3}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p0

    if-eqz p0, :cond_3

    goto :goto_0

    :cond_3
    return v1

    :cond_4
    :goto_0
    return v3
.end method
