.class public final Le/p;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/p$a;,
        Le/p$b;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\t\u0018\u0000 $2\u00020\u0001:\u0002#$B7\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u000e\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006\u0012\u000e\u0010\u0008\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\tJ\u001d\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0003H\u0000\u00a2\u0006\u0002\u0008\u0017J\u0015\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u000c\u0018\u00010\u000bH\u0007\u00a2\u0006\u0002\u0008\u0018J\u0013\u0010\u0019\u001a\u00020\u00032\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\u0008\u0010\u001b\u001a\u00020\u001cH\u0016J\u000e\u0010\u001d\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u0015J\u0018\u0010\u001f\u001a\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0003H\u0002J\r\u0010\u0004\u001a\u00020\u0003H\u0007\u00a2\u0006\u0002\u0008 J\u0015\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u000bH\u0007\u00a2\u0006\u0002\u0008!J\u0008\u0010\"\u001a\u00020\u0007H\u0016R\u0019\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u000c\u0018\u00010\u000b8G\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\rR\u0018\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000eR\u0013\u0010\u0002\u001a\u00020\u00038\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u000fR\u0013\u0010\u0004\u001a\u00020\u00038\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u000fR\u0019\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u000b8G\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\rR\u0018\u0010\u0008\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000e\u00a8\u0006%"
    }
    d2 = {
        "Lokhttp3/ConnectionSpec;",
        "",
        "isTls",
        "",
        "supportsTlsExtensions",
        "cipherSuitesAsString",
        "",
        "",
        "tlsVersionsAsString",
        "(ZZ[Ljava/lang/String;[Ljava/lang/String;)V",
        "cipherSuites",
        "",
        "Lokhttp3/CipherSuite;",
        "()Ljava/util/List;",
        "[Ljava/lang/String;",
        "()Z",
        "tlsVersions",
        "Lokhttp3/TlsVersion;",
        "apply",
        "",
        "sslSocket",
        "Ljavax/net/ssl/SSLSocket;",
        "isFallback",
        "apply$okhttp",
        "-deprecated_cipherSuites",
        "equals",
        "other",
        "hashCode",
        "",
        "isCompatible",
        "socket",
        "supportedSpec",
        "-deprecated_supportsTlsExtensions",
        "-deprecated_tlsVersions",
        "toString",
        "Builder",
        "Companion",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field private static final a:[Le/m;

.field private static final b:[Le/m;

.field public static final c:Le/p;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final d:Le/p;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final e:Le/p;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final f:Le/p;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final g:Le/p$b;


# instance fields
.field private final h:Z

.field private final i:Z

.field private final j:[Ljava/lang/String;

.field private final k:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    new-instance v0, Le/p$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/p$b;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/p;->g:Le/p$b;

    const/16 v0, 0x9

    new-array v1, v0, [Le/m;

    sget-object v2, Le/m;->lb:Le/m;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Le/m;->mb:Le/m;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Le/m;->nb:Le/m;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    sget-object v2, Le/m;->Ya:Le/m;

    const/4 v6, 0x3

    aput-object v2, v1, v6

    sget-object v2, Le/m;->bb:Le/m;

    const/4 v7, 0x4

    aput-object v2, v1, v7

    sget-object v2, Le/m;->Za:Le/m;

    const/4 v8, 0x5

    aput-object v2, v1, v8

    sget-object v2, Le/m;->cb:Le/m;

    const/4 v9, 0x6

    aput-object v2, v1, v9

    sget-object v2, Le/m;->ib:Le/m;

    const/4 v10, 0x7

    aput-object v2, v1, v10

    sget-object v2, Le/m;->hb:Le/m;

    const/16 v11, 0x8

    aput-object v2, v1, v11

    sput-object v1, Le/p;->a:[Le/m;

    const/16 v1, 0x10

    new-array v1, v1, [Le/m;

    sget-object v2, Le/m;->lb:Le/m;

    aput-object v2, v1, v3

    sget-object v2, Le/m;->mb:Le/m;

    aput-object v2, v1, v4

    sget-object v2, Le/m;->nb:Le/m;

    aput-object v2, v1, v5

    sget-object v2, Le/m;->Ya:Le/m;

    aput-object v2, v1, v6

    sget-object v2, Le/m;->bb:Le/m;

    aput-object v2, v1, v7

    sget-object v2, Le/m;->Za:Le/m;

    aput-object v2, v1, v8

    sget-object v2, Le/m;->cb:Le/m;

    aput-object v2, v1, v9

    sget-object v2, Le/m;->ib:Le/m;

    aput-object v2, v1, v10

    sget-object v2, Le/m;->hb:Le/m;

    aput-object v2, v1, v11

    sget-object v2, Le/m;->Ja:Le/m;

    aput-object v2, v1, v0

    sget-object v0, Le/m;->Ka:Le/m;

    const/16 v2, 0xa

    aput-object v0, v1, v2

    sget-object v0, Le/m;->ha:Le/m;

    const/16 v2, 0xb

    aput-object v0, v1, v2

    sget-object v0, Le/m;->ia:Le/m;

    const/16 v2, 0xc

    aput-object v0, v1, v2

    sget-object v0, Le/m;->F:Le/m;

    const/16 v2, 0xd

    aput-object v0, v1, v2

    sget-object v0, Le/m;->J:Le/m;

    const/16 v2, 0xe

    aput-object v0, v1, v2

    sget-object v0, Le/m;->j:Le/m;

    const/16 v2, 0xf

    aput-object v0, v1, v2

    sput-object v1, Le/p;->b:[Le/m;

    new-instance v0, Le/p$a;

    invoke-direct {v0, v4}, Le/p$a;-><init>(Z)V

    sget-object v1, Le/p;->a:[Le/m;

    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Le/m;

    invoke-virtual {v0, v1}, Le/p$a;->a([Le/m;)Le/p$a;

    new-array v1, v5, [Le/P;

    sget-object v2, Le/P;->a:Le/P;

    aput-object v2, v1, v3

    sget-object v2, Le/P;->b:Le/P;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Le/p$a;->a([Le/P;)Le/p$a;

    invoke-virtual {v0, v4}, Le/p$a;->a(Z)Le/p$a;

    invoke-virtual {v0}, Le/p$a;->a()Le/p;

    move-result-object v0

    sput-object v0, Le/p;->c:Le/p;

    new-instance v0, Le/p$a;

    invoke-direct {v0, v4}, Le/p$a;-><init>(Z)V

    sget-object v1, Le/p;->b:[Le/m;

    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Le/m;

    invoke-virtual {v0, v1}, Le/p$a;->a([Le/m;)Le/p$a;

    new-array v1, v5, [Le/P;

    sget-object v2, Le/P;->a:Le/P;

    aput-object v2, v1, v3

    sget-object v2, Le/P;->b:Le/P;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Le/p$a;->a([Le/P;)Le/p$a;

    invoke-virtual {v0, v4}, Le/p$a;->a(Z)Le/p$a;

    invoke-virtual {v0}, Le/p$a;->a()Le/p;

    move-result-object v0

    sput-object v0, Le/p;->d:Le/p;

    new-instance v0, Le/p$a;

    invoke-direct {v0, v4}, Le/p$a;-><init>(Z)V

    sget-object v1, Le/p;->b:[Le/m;

    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Le/m;

    invoke-virtual {v0, v1}, Le/p$a;->a([Le/m;)Le/p$a;

    new-array v1, v7, [Le/P;

    sget-object v2, Le/P;->a:Le/P;

    aput-object v2, v1, v3

    sget-object v2, Le/P;->b:Le/P;

    aput-object v2, v1, v4

    sget-object v2, Le/P;->c:Le/P;

    aput-object v2, v1, v5

    sget-object v2, Le/P;->d:Le/P;

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Le/p$a;->a([Le/P;)Le/p$a;

    invoke-virtual {v0, v4}, Le/p$a;->a(Z)Le/p$a;

    invoke-virtual {v0}, Le/p$a;->a()Le/p;

    move-result-object v0

    sput-object v0, Le/p;->e:Le/p;

    new-instance v0, Le/p$a;

    invoke-direct {v0, v3}, Le/p$a;-><init>(Z)V

    invoke-virtual {v0}, Le/p$a;->a()Le/p;

    move-result-object v0

    sput-object v0, Le/p;->f:Le/p;

    return-void
.end method

.method public constructor <init>(ZZ[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p3    # [Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # [Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Le/p;->h:Z

    iput-boolean p2, p0, Le/p;->i:Z

    iput-object p3, p0, Le/p;->j:[Ljava/lang/String;

    iput-object p4, p0, Le/p;->k:[Ljava/lang/String;

    return-void
.end method

.method public static final synthetic a(Le/p;)[Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Le/p;->j:[Ljava/lang/String;

    return-object p0
.end method

.method private final b(Ljavax/net/ssl/SSLSocket;Z)Le/p;
    .locals 4

    iget-object v0, p0, Le/p;->j:[Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledCipherSuites()[Ljava/lang/String;

    move-result-object v0

    const-string v1, "sslSocket.enabledCipherSuites"

    invoke-static {v0, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Le/p;->j:[Ljava/lang/String;

    sget-object v2, Le/m;->qb:Le/m$a;

    invoke-virtual {v2}, Le/m$a;->a()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v0, v1, v2}, Le/a/d;->b([Ljava/lang/String;[Ljava/lang/String;Ljava/util/Comparator;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledCipherSuites()[Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Le/p;->k:[Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledProtocols()[Ljava/lang/String;

    move-result-object v1

    const-string v2, "sslSocket.enabledProtocols"

    invoke-static {v1, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Le/p;->k:[Ljava/lang/String;

    invoke-static {}, Lkotlin/b/a;->a()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v1, v2, v3}, Le/a/d;->b([Ljava/lang/String;[Ljava/lang/String;Ljava/util/Comparator;)[Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledProtocols()[Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getSupportedCipherSuites()[Ljava/lang/String;

    move-result-object p1

    const-string v2, "supportedCipherSuites"

    invoke-static {p1, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Le/m;->qb:Le/m$a;

    invoke-virtual {v2}, Le/m$a;->a()Ljava/util/Comparator;

    move-result-object v2

    const-string v3, "TLS_FALLBACK_SCSV"

    invoke-static {p1, v3, v2}, Le/a/d;->a([Ljava/lang/String;Ljava/lang/String;Ljava/util/Comparator;)I

    move-result v2

    const-string v3, "cipherSuitesIntersection"

    if-eqz p2, :cond_2

    const/4 p2, -0x1

    if-eq v2, p2, :cond_2

    invoke-static {v0, v3}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    aget-object p1, p1, v2

    const-string p2, "supportedCipherSuites[indexOfFallbackScsv]"

    invoke-static {p1, p2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Le/a/d;->a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    :cond_2
    new-instance p1, Le/p$a;

    invoke-direct {p1, p0}, Le/p$a;-><init>(Le/p;)V

    invoke-static {v0, v3}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    array-length p2, v0

    invoke-static {v0, p2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Ljava/lang/String;

    invoke-virtual {p1, p2}, Le/p$a;->a([Ljava/lang/String;)Le/p$a;

    const-string p2, "tlsVersionsIntersection"

    invoke-static {v1, p2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    array-length p2, v1

    invoke-static {v1, p2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Ljava/lang/String;

    invoke-virtual {p1, p2}, Le/p$a;->b([Ljava/lang/String;)Le/p$a;

    invoke-virtual {p1}, Le/p$a;->a()Le/p;

    move-result-object p1

    return-object p1
.end method

.method public static final synthetic b(Le/p;)[Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Le/p;->k:[Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Le/m;",
            ">;"
        }
    .end annotation

    .annotation build Lkotlin/jvm/JvmName;
        name = "cipherSuites"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/p;->j:[Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v0, v3

    sget-object v5, Le/m;->qb:Le/m$a;

    invoke-virtual {v5, v4}, Le/m$a;->a(Ljava/lang/String;)Le/m;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lkotlin/a/h;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return-object v0
.end method

.method public final a(Ljavax/net/ssl/SSLSocket;Z)V
    .locals 1
    .param p1    # Ljavax/net/ssl/SSLSocket;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "sslSocket"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Le/p;->b(Ljavax/net/ssl/SSLSocket;Z)Le/p;

    move-result-object p2

    invoke-virtual {p2}, Le/p;->d()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Le/p;->k:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljavax/net/ssl/SSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2}, Le/p;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object p2, p2, Le/p;->j:[Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljavax/net/ssl/SSLSocket;->setEnabledCipherSuites([Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final a(Ljavax/net/ssl/SSLSocket;)Z
    .locals 4
    .param p1    # Ljavax/net/ssl/SSLSocket;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "socket"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Le/p;->h:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Le/p;->k:[Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledProtocols()[Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lkotlin/b/a;->a()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v0, v2, v3}, Le/a/d;->a([Ljava/lang/String;[Ljava/lang/String;Ljava/util/Comparator;)Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Le/p;->j:[Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledCipherSuites()[Ljava/lang/String;

    move-result-object p1

    sget-object v2, Le/m;->qb:Le/m$a;

    invoke-virtual {v2}, Le/m$a;->a()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v0, p1, v2}, Le/a/d;->a([Ljava/lang/String;[Ljava/lang/String;Ljava/util/Comparator;)Z

    move-result p1

    if-nez p1, :cond_2

    return v1

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public final b()Z
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "isTls"
    .end annotation

    iget-boolean v0, p0, Le/p;->h:Z

    return v0
.end method

.method public final c()Z
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "supportsTlsExtensions"
    .end annotation

    iget-boolean v0, p0, Le/p;->i:Z

    return v0
.end method

.method public final d()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Le/P;",
            ">;"
        }
    .end annotation

    .annotation build Lkotlin/jvm/JvmName;
        name = "tlsVersions"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/p;->k:[Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v0, v3

    sget-object v5, Le/P;->g:Le/P$a;

    invoke-virtual {v5, v4}, Le/P$a;->a(Ljava/lang/String;)Le/P;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lkotlin/a/h;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    instance-of v0, p1, Le/p;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x1

    if-ne p1, p0, :cond_1

    return v0

    :cond_1
    iget-boolean v2, p0, Le/p;->h:Z

    check-cast p1, Le/p;

    iget-boolean v3, p1, Le/p;->h:Z

    if-eq v2, v3, :cond_2

    return v1

    :cond_2
    if-eqz v2, :cond_5

    iget-object v2, p0, Le/p;->j:[Ljava/lang/String;

    iget-object v3, p1, Le/p;->j:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    :cond_3
    iget-object v2, p0, Le/p;->k:[Ljava/lang/String;

    iget-object v3, p1, Le/p;->k:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    return v1

    :cond_4
    iget-boolean v2, p0, Le/p;->i:Z

    iget-boolean p1, p1, Le/p;->i:Z

    if-eq v2, p1, :cond_5

    return v1

    :cond_5
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Le/p;->h:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x20f

    iget-object v1, p0, Le/p;->j:[Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Le/p;->k:[Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Le/p;->i:Z

    xor-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    const/16 v0, 0x11

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-boolean v0, p0, Le/p;->h:Z

    if-nez v0, :cond_0

    const-string v0, "ConnectionSpec()"

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConnectionSpec("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "cipherSuites="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Le/p;->a()Ljava/util/List;

    move-result-object v1

    const-string v2, "[all enabled]"

    invoke-static {v1, v2}, Ljava/util/Objects;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "tlsVersions="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Le/p;->d()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v2}, Ljava/util/Objects;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "supportsTlsExtensions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Le/p;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
