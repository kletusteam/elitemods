.class public final Le/f;
.super Lf/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Le/e$d;-><init>(Le/e;Le/a/a/e$b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic b:Le/e$d;


# direct methods
.method constructor <init>(Le/e$d;Lf/A;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/A;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Le/f;->b:Le/e$d;

    invoke-direct {p0, p2}, Lf/m;-><init>(Lf/A;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/f;->b:Le/e$d;

    iget-object v0, v0, Le/e$d;->e:Le/e;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Le/f;->b:Le/e$d;

    invoke-virtual {v1}, Le/e$d;->b()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Le/f;->b:Le/e$d;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Le/e$d;->a(Z)V

    iget-object v1, p0, Le/f;->b:Le/e$d;

    iget-object v1, v1, Le/e$d;->e:Le/e;

    invoke-virtual {v1}, Le/e;->b()I

    move-result v3

    add-int/2addr v3, v2

    invoke-virtual {v1, v3}, Le/e;->c(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    invoke-super {p0}, Lf/m;->close()V

    iget-object v0, p0, Le/f;->b:Le/e$d;

    invoke-static {v0}, Le/e$d;->a(Le/e$d;)Le/a/a/e$b;

    move-result-object v0

    invoke-virtual {v0}, Le/a/a/e$b;->b()V

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
