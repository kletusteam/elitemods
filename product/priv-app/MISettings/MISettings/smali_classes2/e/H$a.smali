.class public final Le/H$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/H;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field private volatile a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final b:Le/i;

.field final synthetic c:Le/H;


# direct methods
.method public constructor <init>(Le/H;Le/i;)V
    .locals 1
    .param p1    # Le/H;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le/i;",
            ")V"
        }
    .end annotation

    const-string v0, "responseCallback"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Le/H$a;->c:Le/H;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Le/H$a;->b:Le/i;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object p1, p0, Le/H$a;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/H$a;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method public final a(Le/H$a;)V
    .locals 1
    .param p1    # Le/H$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Le/H$a;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, Le/H$a;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public final a(Ljava/util/concurrent/ExecutorService;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "executorService"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/H$a;->c:Le/H;

    invoke-virtual {v0}, Le/H;->a()Le/F;

    move-result-object v0

    invoke-virtual {v0}, Le/F;->k()Le/t;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sget-boolean v1, Lkotlin/s;->a:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Assertion failed"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {p1, p0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    :try_start_1
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "executor rejected"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    iget-object p1, p0, Le/H$a;->c:Le/H;

    invoke-static {p1}, Le/H;->a(Le/H;)Le/a/b/n;

    move-result-object p1

    invoke-virtual {p1, v0}, Le/a/b/n;->a(Ljava/io/IOException;)Ljava/io/IOException;

    iget-object p1, p0, Le/H$a;->b:Le/i;

    iget-object v1, p0, Le/H$a;->c:Le/H;

    invoke-interface {p1, v1, v0}, Le/i;->a(Le/h;Ljava/io/IOException;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object p1, p0, Le/H$a;->c:Le/H;

    invoke-virtual {p1}, Le/H;->a()Le/F;

    move-result-object p1

    invoke-virtual {p1}, Le/F;->k()Le/t;

    move-result-object p1

    invoke-virtual {p1, p0}, Le/t;->b(Le/H$a;)V

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Le/H$a;->c:Le/H;

    invoke-virtual {v0}, Le/H;->a()Le/F;

    move-result-object v0

    invoke-virtual {v0}, Le/F;->k()Le/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Le/t;->b(Le/H$a;)V

    throw p1
.end method

.method public final b()Le/H;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/H$a;->c:Le/H;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/H$a;->c:Le/H;

    invoke-virtual {v0}, Le/H;->c()Le/I;

    move-result-object v0

    invoke-virtual {v0}, Le/I;->h()Le/C;

    move-result-object v0

    invoke-virtual {v0}, Le/C;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OkHttp "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le/H$a;->c:Le/H;

    invoke-virtual {v1}, Le/H;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "currentThread"

    invoke-static {v1, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    const/4 v0, 0x0

    :try_start_0
    iget-object v3, p0, Le/H$a;->c:Le/H;

    invoke-static {v3}, Le/H;->a(Le/H;)Le/a/b/n;

    move-result-object v3

    invoke-virtual {v3}, Le/a/b/n;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, p0, Le/H$a;->c:Le/H;

    invoke-virtual {v3}, Le/H;->d()Le/L;

    move-result-object v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v3, 0x1

    :try_start_2
    iget-object v4, p0, Le/H$a;->b:Le/i;

    iget-object v5, p0, Le/H$a;->c:Le/H;

    invoke-interface {v4, v5, v0}, Le/i;->a(Le/h;Le/L;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v0, p0, Le/H$a;->c:Le/H;

    invoke-virtual {v0}, Le/H;->a()Le/F;

    move-result-object v0

    invoke-virtual {v0}, Le/F;->k()Le/t;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, p0}, Le/t;->b(Le/H$a;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v3

    move-object v7, v3

    move v3, v0

    move-object v0, v7

    :goto_1
    if-eqz v3, :cond_0

    :try_start_4
    sget-object v3, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v3}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v3

    const/4 v4, 0x4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Callback failure for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Le/H$a;->c:Le/H;

    invoke-virtual {v6}, Le/H;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v0}, Le/a/g/g;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_0
    iget-object v3, p0, Le/H$a;->b:Le/i;

    iget-object v4, p0, Le/H$a;->c:Le/H;

    invoke-interface {v3, v4, v0}, Le/i;->a(Le/h;Ljava/io/IOException;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_2
    :try_start_5
    iget-object v0, p0, Le/H$a;->c:Le/H;

    invoke-virtual {v0}, Le/H;->a()Le/F;

    move-result-object v0

    invoke-virtual {v0}, Le/F;->k()Le/t;

    move-result-object v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    :goto_3
    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    return-void

    :goto_4
    :try_start_6
    iget-object v3, p0, Le/H$a;->c:Le/H;

    invoke-virtual {v3}, Le/H;->a()Le/F;

    move-result-object v3

    invoke-virtual {v3}, Le/F;->k()Le/t;

    move-result-object v3

    invoke-virtual {v3, p0}, Le/t;->b(Le/H$a;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v0

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    throw v0
.end method
