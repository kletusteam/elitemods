.class public final Le/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/io/Flushable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/e$d;,
        Le/e$c;,
        Le/e$a;,
        Le/e$b;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000c\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010)\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u0000 C2\u00020\u00012\u00020\u0002:\u0004BCDEB\u0017\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\u0008\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\u001f\u001a\u00020 2\u000c\u0010!\u001a\u0008\u0018\u00010\"R\u00020\u000cH\u0002J\u0008\u0010#\u001a\u00020 H\u0016J\u0006\u0010$\u001a\u00020 J\r\u0010\u0003\u001a\u00020\u0004H\u0007\u00a2\u0006\u0002\u0008%J\u0006\u0010&\u001a\u00020 J\u0008\u0010\'\u001a\u00020 H\u0016J\u0017\u0010(\u001a\u0004\u0018\u00010)2\u0006\u0010*\u001a\u00020+H\u0000\u00a2\u0006\u0002\u0008,J\u0006\u0010\u0010\u001a\u00020\u0011J\u0006\u0010-\u001a\u00020 J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0015\u001a\u00020\u0011J\u0017\u0010.\u001a\u0004\u0018\u00010/2\u0006\u00100\u001a\u00020)H\u0000\u00a2\u0006\u0002\u00081J\u0015\u00102\u001a\u00020 2\u0006\u0010*\u001a\u00020+H\u0000\u00a2\u0006\u0002\u00083J\u0006\u0010\u0016\u001a\u00020\u0011J\u0006\u00104\u001a\u00020\u0006J\r\u00105\u001a\u00020 H\u0000\u00a2\u0006\u0002\u00086J\u0015\u00107\u001a\u00020 2\u0006\u00108\u001a\u000209H\u0000\u00a2\u0006\u0002\u0008:J\u001d\u0010;\u001a\u00020 2\u0006\u0010<\u001a\u00020)2\u0006\u0010=\u001a\u00020)H\u0000\u00a2\u0006\u0002\u0008>J\u000c\u0010?\u001a\u0008\u0012\u0004\u0012\u00020A0@J\u0006\u0010\u0017\u001a\u00020\u0011J\u0006\u0010\u001c\u001a\u00020\u0011R\u0014\u0010\u000b\u001a\u00020\u000cX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0003\u001a\u00020\u00048G\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0012\u001a\u00020\u00138F\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0014R\u000e\u0010\u0015\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u00020\u0011X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR\u001a\u0010\u001c\u001a\u00020\u0011X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001d\u0010\u0019\"\u0004\u0008\u001e\u0010\u001b\u00a8\u0006F"
    }
    d2 = {
        "Lokhttp3/Cache;",
        "Ljava/io/Closeable;",
        "Ljava/io/Flushable;",
        "directory",
        "Ljava/io/File;",
        "maxSize",
        "",
        "(Ljava/io/File;J)V",
        "fileSystem",
        "Lokhttp3/internal/io/FileSystem;",
        "(Ljava/io/File;JLokhttp3/internal/io/FileSystem;)V",
        "cache",
        "Lokhttp3/internal/cache/DiskLruCache;",
        "getCache$okhttp",
        "()Lokhttp3/internal/cache/DiskLruCache;",
        "()Ljava/io/File;",
        "hitCount",
        "",
        "isClosed",
        "",
        "()Z",
        "networkCount",
        "requestCount",
        "writeAbortCount",
        "getWriteAbortCount$okhttp",
        "()I",
        "setWriteAbortCount$okhttp",
        "(I)V",
        "writeSuccessCount",
        "getWriteSuccessCount$okhttp",
        "setWriteSuccessCount$okhttp",
        "abortQuietly",
        "",
        "editor",
        "Lokhttp3/internal/cache/DiskLruCache$Editor;",
        "close",
        "delete",
        "-deprecated_directory",
        "evictAll",
        "flush",
        "get",
        "Lokhttp3/Response;",
        "request",
        "Lokhttp3/Request;",
        "get$okhttp",
        "initialize",
        "put",
        "Lokhttp3/internal/cache/CacheRequest;",
        "response",
        "put$okhttp",
        "remove",
        "remove$okhttp",
        "size",
        "trackConditionalCacheHit",
        "trackConditionalCacheHit$okhttp",
        "trackResponse",
        "cacheStrategy",
        "Lokhttp3/internal/cache/CacheStrategy;",
        "trackResponse$okhttp",
        "update",
        "cached",
        "network",
        "update$okhttp",
        "urls",
        "",
        "",
        "CacheResponseBody",
        "Companion",
        "Entry",
        "RealCacheRequest",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final a:Le/e$b;


# instance fields
.field private final b:Le/a/a/e;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Le/e$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/e$b;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/e;->a:Le/e$b;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;J)V
    .locals 1
    .param p1    # Ljava/io/File;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "directory"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Le/a/f/b;->a:Le/a/f/b;

    invoke-direct {p0, p1, p2, p3, v0}, Le/e;-><init>(Ljava/io/File;JLe/a/f/b;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;JLe/a/f/b;)V
    .locals 8
    .param p1    # Ljava/io/File;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Le/a/f/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "directory"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileSystem"

    invoke-static {p4, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Le/a/a/e;->l:Le/a/a/e$a;

    const v4, 0x31191

    const/4 v5, 0x2

    move-object v2, p4

    move-object v3, p1

    move-wide v6, p2

    invoke-virtual/range {v1 .. v7}, Le/a/a/e$a;->a(Le/a/f/b;Ljava/io/File;IIJ)Le/a/a/e;

    move-result-object p1

    iput-object p1, p0, Le/e;->b:Le/a/a/e;

    return-void
.end method

.method private final a(Le/a/a/e$b;)V
    .locals 0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Le/a/a/e$b;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Le/e;->d:I

    return v0
.end method

.method public final a(Le/I;)Le/L;
    .locals 4
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Le/e;->a:Le/e$b;

    invoke-virtual {p1}, Le/I;->h()Le/C;

    move-result-object v1

    invoke-virtual {v0, v1}, Le/e$b;->a(Le/C;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Le/e;->b:Le/a/a/e;

    invoke-virtual {v2, v0}, Le/a/a/e;->e(Ljava/lang/String;)Le/a/a/e$d;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_2

    :try_start_1
    new-instance v2, Le/e$c;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Le/a/a/e$d;->b(I)Lf/C;

    move-result-object v3

    invoke-direct {v2, v3}, Le/e$c;-><init>(Lf/C;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    invoke-virtual {v2, v0}, Le/e$c;->a(Le/a/a/e$d;)Le/L;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Le/e$c;->a(Le/I;Le/L;)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {v0}, Le/L;->a()Le/N;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p1}, Le/a/d;->a(Ljava/io/Closeable;)V

    :cond_0
    return-object v1

    :cond_1
    return-object v0

    :catch_0
    invoke-static {v0}, Le/a/d;->a(Ljava/io/Closeable;)V

    :catch_1
    :cond_2
    return-object v1
.end method

.method public final a(Le/L;)Le/a/a/c;
    .locals 9
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v0

    invoke-virtual {v0}, Le/I;->f()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Le/a/c/g;->a:Le/a/c/g;

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v2

    invoke-virtual {v2}, Le/I;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Le/a/c/g;->a(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object p1

    invoke-virtual {p0, p1}, Le/e;->b(Le/I;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object v2

    :cond_0
    const-string v1, "GET"

    invoke-static {v0, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    return-object v2

    :cond_1
    sget-object v0, Le/e;->a:Le/e$b;

    invoke-virtual {v0, p1}, Le/e$b;->a(Le/L;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-object v2

    :cond_2
    new-instance v0, Le/e$c;

    invoke-direct {v0, p1}, Le/e$c;-><init>(Le/L;)V

    :try_start_1
    iget-object v3, p0, Le/e;->b:Le/a/a/e;

    sget-object v1, Le/e;->a:Le/e$b;

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object p1

    invoke-virtual {p1}, Le/I;->h()Le/C;

    move-result-object p1

    invoke-virtual {v1, p1}, Le/e$b;->a(Le/C;)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Le/a/a/e;->a(Le/a/a/e;Ljava/lang/String;JILjava/lang/Object;)Le/a/a/e$b;

    move-result-object p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz p1, :cond_3

    :try_start_2
    invoke-virtual {v0, p1}, Le/e$c;->a(Le/a/a/e$b;)V

    new-instance v0, Le/e$d;

    invoke-direct {v0, p0, p1}, Le/e$d;-><init>(Le/e;Le/a/a/e$b;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    return-object v0

    :cond_3
    return-object v2

    :catch_1
    move-object p1, v2

    :catch_2
    invoke-direct {p0, p1}, Le/e;->a(Le/a/a/e$b;)V

    return-object v2
.end method

.method public final a(Le/L;Le/L;)V
    .locals 1
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cached"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "network"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Le/e$c;

    invoke-direct {v0, p2}, Le/e$c;-><init>(Le/L;)V

    invoke-virtual {p1}, Le/L;->a()Le/N;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Le/e$a;

    invoke-virtual {p1}, Le/e$a;->r()Le/a/a/e$d;

    move-result-object p1

    const/4 p2, 0x0

    :try_start_0
    invoke-virtual {p1}, Le/a/a/e$d;->a()Le/a/a/e$b;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Le/e$c;->a(Le/a/a/e$b;)V

    invoke-virtual {p2}, Le/a/a/e$b;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-direct {p0, p2}, Le/e;->a(Le/a/a/e$b;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance p1, Lkotlin/o;

    const-string p2, "null cannot be cast to non-null type okhttp3.Cache.CacheResponseBody"

    invoke-direct {p1, p2}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final declared-synchronized a(Le/a/a/d;)V
    .locals 1
    .param p1    # Le/a/a/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    monitor-enter p0

    :try_start_0
    const-string v0, "cacheStrategy"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Le/e;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Le/e;->g:I

    invoke-virtual {p1}, Le/a/a/d;->b()Le/I;

    move-result-object v0

    if-eqz v0, :cond_0

    iget p1, p0, Le/e;->e:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Le/e;->e:I

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Le/a/a/d;->a()Le/L;

    move-result-object p1

    if-eqz p1, :cond_1

    iget p1, p0, Le/e;->f:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Le/e;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Le/e;->c:I

    return v0
.end method

.method public final b(I)V
    .locals 0

    iput p1, p0, Le/e;->d:I

    return-void
.end method

.method public final b(Le/I;)V
    .locals 2
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/e;->b:Le/a/a/e;

    sget-object v1, Le/e;->a:Le/e$b;

    invoke-virtual {p1}, Le/I;->h()Le/C;

    move-result-object p1

    invoke-virtual {v1, p1}, Le/e$b;->a(Le/C;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Le/a/a/e;->f(Ljava/lang/String;)Z

    return-void
.end method

.method public final c(I)V
    .locals 0

    iput p1, p0, Le/e;->c:I

    return-void
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/e;->b:Le/a/a/e;

    invoke-virtual {v0}, Le/a/a/e;->close()V

    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Le/e;->b:Le/a/a/e;

    invoke-virtual {v0}, Le/a/a/e;->flush()V

    return-void
.end method

.method public final declared-synchronized p()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Le/e;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Le/e;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
