.class final Le/e$c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/e$c$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u0000 .2\u00020\u0001:\u0001.B\u000f\u0008\u0010\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u000f\u0008\u0010\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0016\u0010\u001b\u001a\u00020\r2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0005\u001a\u00020\u0006J\u0016\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020 0\u001f2\u0006\u0010!\u001a\u00020\"H\u0002J\u0012\u0010\u0005\u001a\u00020\u00062\n\u0010#\u001a\u00060$R\u00020%J\u001e\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)2\u000c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020 0\u001fH\u0002J\u0012\u0010+\u001a\u00020\'2\n\u0010,\u001a\u00060-R\u00020%R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u00020\r8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lokhttp3/Cache$Entry;",
        "",
        "rawSource",
        "Lokio/Source;",
        "(Lokio/Source;)V",
        "response",
        "Lokhttp3/Response;",
        "(Lokhttp3/Response;)V",
        "code",
        "",
        "handshake",
        "Lokhttp3/Handshake;",
        "isHttps",
        "",
        "()Z",
        "message",
        "",
        "protocol",
        "Lokhttp3/Protocol;",
        "receivedResponseMillis",
        "",
        "requestMethod",
        "responseHeaders",
        "Lokhttp3/Headers;",
        "sentRequestMillis",
        "url",
        "varyHeaders",
        "matches",
        "request",
        "Lokhttp3/Request;",
        "readCertificateList",
        "",
        "Ljava/security/cert/Certificate;",
        "source",
        "Lokio/BufferedSource;",
        "snapshot",
        "Lokhttp3/internal/cache/DiskLruCache$Snapshot;",
        "Lokhttp3/internal/cache/DiskLruCache;",
        "writeCertList",
        "",
        "sink",
        "Lokio/BufferedSink;",
        "certificates",
        "writeTo",
        "editor",
        "Lokhttp3/internal/cache/DiskLruCache$Editor;",
        "Companion",
        "okhttp"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field public static final c:Le/e$c$a;


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Le/B;

.field private final f:Ljava/lang/String;

.field private final g:Le/G;

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:Le/B;

.field private final k:Le/A;

.field private final l:J

.field private final m:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Le/e$c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le/e$c$a;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Le/e$c;->c:Le/e$c$a;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v1}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v1

    invoke-virtual {v1}, Le/a/g/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-Sent-Millis"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Le/e$c;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Le/a/g/g;->c:Le/a/g/g$a;

    invoke-virtual {v1}, Le/a/g/g$a;->a()Le/a/g/g;

    move-result-object v1

    invoke-virtual {v1}, Le/a/g/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-Received-Millis"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Le/e$c;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Le/L;)V
    .locals 2
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v0

    invoke-virtual {v0}, Le/I;->h()Le/C;

    move-result-object v0

    invoke-virtual {v0}, Le/C;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/e$c;->d:Ljava/lang/String;

    sget-object v0, Le/e;->a:Le/e$b;

    invoke-virtual {v0, p1}, Le/e$b;->b(Le/L;)Le/B;

    move-result-object v0

    iput-object v0, p0, Le/e$c;->e:Le/B;

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v0

    invoke-virtual {v0}, Le/I;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/e$c;->f:Ljava/lang/String;

    invoke-virtual {p1}, Le/L;->y()Le/G;

    move-result-object v0

    iput-object v0, p0, Le/e$c;->g:Le/G;

    invoke-virtual {p1}, Le/L;->q()I

    move-result v0

    iput v0, p0, Le/e$c;->h:I

    invoke-virtual {p1}, Le/L;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/e$c;->i:Ljava/lang/String;

    invoke-virtual {p1}, Le/L;->t()Le/B;

    move-result-object v0

    iput-object v0, p0, Le/e$c;->j:Le/B;

    invoke-virtual {p1}, Le/L;->s()Le/A;

    move-result-object v0

    iput-object v0, p0, Le/e$c;->k:Le/A;

    invoke-virtual {p1}, Le/L;->B()J

    move-result-wide v0

    iput-wide v0, p0, Le/e$c;->l:J

    invoke-virtual {p1}, Le/L;->z()J

    move-result-wide v0

    iput-wide v0, p0, Le/e$c;->m:J

    return-void
.end method

.method public constructor <init>(Lf/C;)V
    .locals 9
    .param p1    # Lf/C;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "rawSource"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    invoke-static {p1}, Lf/s;->a(Lf/C;)Lf/k;

    move-result-object v0

    invoke-interface {v0}, Lf/k;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Le/e$c;->d:Ljava/lang/String;

    invoke-interface {v0}, Lf/k;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Le/e$c;->f:Ljava/lang/String;

    new-instance v1, Le/B$a;

    invoke-direct {v1}, Le/B$a;-><init>()V

    sget-object v2, Le/e;->a:Le/e$b;

    invoke-virtual {v2, v0}, Le/e$b;->a(Lf/k;)I

    move-result v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v2, :cond_0

    invoke-interface {v0}, Lf/k;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Le/B$a;->a(Ljava/lang/String;)Le/B$a;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Le/B$a;->a()Le/B;

    move-result-object v1

    iput-object v1, p0, Le/e$c;->e:Le/B;

    sget-object v1, Le/a/c/l;->a:Le/a/c/l$a;

    invoke-interface {v0}, Lf/k;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Le/a/c/l$a;->a(Ljava/lang/String;)Le/a/c/l;

    move-result-object v1

    iget-object v2, v1, Le/a/c/l;->b:Le/G;

    iput-object v2, p0, Le/e$c;->g:Le/G;

    iget v2, v1, Le/a/c/l;->c:I

    iput v2, p0, Le/e$c;->h:I

    iget-object v1, v1, Le/a/c/l;->d:Ljava/lang/String;

    iput-object v1, p0, Le/e$c;->i:Ljava/lang/String;

    new-instance v1, Le/B$a;

    invoke-direct {v1}, Le/B$a;-><init>()V

    sget-object v2, Le/e;->a:Le/e$b;

    invoke-virtual {v2, v0}, Le/e$b;->a(Lf/k;)I

    move-result v2

    move v4, v3

    :goto_1
    if-ge v4, v2, :cond_1

    invoke-interface {v0}, Lf/k;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Le/B$a;->a(Ljava/lang/String;)Le/B$a;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    sget-object v2, Le/e$c;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Le/B$a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Le/e$c;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Le/B$a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Le/e$c;->a:Ljava/lang/String;

    invoke-virtual {v1, v5}, Le/B$a;->c(Ljava/lang/String;)Le/B$a;

    sget-object v5, Le/e$c;->b:Ljava/lang/String;

    invoke-virtual {v1, v5}, Le/B$a;->c(Ljava/lang/String;)Le/B$a;

    const-wide/16 v5, 0x0

    if-eqz v2, :cond_2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    goto :goto_2

    :cond_2
    move-wide v7, v5

    :goto_2
    iput-wide v7, p0, Le/e$c;->l:J

    if-eqz v4, :cond_3

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    :cond_3
    iput-wide v5, p0, Le/e$c;->m:J

    invoke-virtual {v1}, Le/B$a;->a()Le/B;

    move-result-object v1

    iput-object v1, p0, Le/e$c;->j:Le/B;

    invoke-direct {p0}, Le/e$c;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Lf/k;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_4

    const/4 v3, 0x1

    :cond_4
    if-nez v3, :cond_6

    invoke-interface {v0}, Lf/k;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Le/m;->qb:Le/m$a;

    invoke-virtual {v2, v1}, Le/m$a;->a(Ljava/lang/String;)Le/m;

    move-result-object v1

    invoke-direct {p0, v0}, Le/e$c;->a(Lf/k;)Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v0}, Le/e$c;->a(Lf/k;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0}, Lf/k;->d()Z

    move-result v4

    if-nez v4, :cond_5

    sget-object v4, Le/P;->g:Le/P$a;

    invoke-interface {v0}, Lf/k;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Le/P$a;->a(Ljava/lang/String;)Le/P;

    move-result-object v0

    goto :goto_3

    :cond_5
    sget-object v0, Le/P;->e:Le/P;

    :goto_3
    sget-object v4, Le/A;->b:Le/A$a;

    invoke-virtual {v4, v0, v1, v2, v3}, Le/A$a;->a(Le/P;Le/m;Ljava/util/List;Ljava/util/List;)Le/A;

    move-result-object v0

    iput-object v0, p0, Le/e$c;->k:Le/A;

    goto :goto_4

    :cond_6
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "expected \"\" but was \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x22

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, Le/e$c;->k:Le/A;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_4
    invoke-interface {p1}, Lf/C;->close()V

    return-void

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Lf/C;->close()V

    throw v0
.end method

.method private final a(Lf/k;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/k;",
            ")",
            "Ljava/util/List<",
            "Ljava/security/cert/Certificate;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Le/e;->a:Le/e$b;

    invoke-virtual {v0, p1}, Le/e$b;->a(Lf/k;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_0
    :try_start_0
    const-string v1, "X.509"

    invoke-static {v1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_2

    invoke-interface {p1}, Lf/k;->g()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lf/h;

    invoke-direct {v5}, Lf/h;-><init>()V

    sget-object v6, Lf/l;->b:Lf/l$a;

    invoke-virtual {v6, v4}, Lf/l$a;->a(Ljava/lang/String;)Lf/l;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v5, v4}, Lf/h;->c(Lf/l;)Lf/h;

    invoke-virtual {v5}, Lf/h;->i()Ljava/io/InputStream;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x0

    throw p1

    :cond_2
    return-object v2

    :catch_0
    move-exception p1

    new-instance v0, Ljava/io/IOException;

    invoke-virtual {p1}, Ljava/security/cert/CertificateException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final a(Lf/j;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/j;",
            "Ljava/util/List<",
            "+",
            "Ljava/security/cert/Certificate;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    invoke-interface {p1, v0, v1}, Lf/j;->g(J)Lf/j;

    move-result-object v0

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lf/j;->writeByte(I)Lf/j;

    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/security/cert/Certificate;

    invoke-virtual {v3}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v5

    sget-object v4, Lf/l;->b:Lf/l$a;

    const-string v3, "bytes"

    invoke-static {v5, v3}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x3

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lf/l$a;->a(Lf/l$a;[BIIILjava/lang/Object;)Lf/l;

    move-result-object v3

    invoke-virtual {v3}, Lf/l;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v3

    invoke-interface {v3, v1}, Lf/j;->writeByte(I)Lf/j;
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    :catch_0
    move-exception p1

    new-instance p2, Ljava/io/IOException;

    invoke-virtual {p1}, Ljava/security/cert/CertificateEncodingException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private final a()Z
    .locals 5

    iget-object v0, p0, Le/e$c;->d:Ljava/lang/String;

    const-string v1, "https://"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lkotlin/g/g;->b(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Le/a/a/e$d;)Le/L;
    .locals 5
    .param p1    # Le/a/a/e$d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/e$c;->j:Le/B;

    const-string v1, "Content-Type"

    invoke-virtual {v0, v1}, Le/B;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Le/e$c;->j:Le/B;

    const-string v2, "Content-Length"

    invoke-virtual {v1, v2}, Le/B;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Le/I$a;

    invoke-direct {v2}, Le/I$a;-><init>()V

    iget-object v3, p0, Le/e$c;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Le/I$a;->b(Ljava/lang/String;)Le/I$a;

    iget-object v3, p0, Le/e$c;->f:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Le/I$a;->a(Ljava/lang/String;Le/K;)Le/I$a;

    iget-object v3, p0, Le/e$c;->e:Le/B;

    invoke-virtual {v2, v3}, Le/I$a;->a(Le/B;)Le/I$a;

    invoke-virtual {v2}, Le/I$a;->a()Le/I;

    move-result-object v2

    new-instance v3, Le/L$a;

    invoke-direct {v3}, Le/L$a;-><init>()V

    invoke-virtual {v3, v2}, Le/L$a;->a(Le/I;)Le/L$a;

    iget-object v2, p0, Le/e$c;->g:Le/G;

    invoke-virtual {v3, v2}, Le/L$a;->a(Le/G;)Le/L$a;

    iget v2, p0, Le/e$c;->h:I

    invoke-virtual {v3, v2}, Le/L$a;->a(I)Le/L$a;

    iget-object v2, p0, Le/e$c;->i:Ljava/lang/String;

    invoke-virtual {v3, v2}, Le/L$a;->a(Ljava/lang/String;)Le/L$a;

    iget-object v2, p0, Le/e$c;->j:Le/B;

    invoke-virtual {v3, v2}, Le/L$a;->a(Le/B;)Le/L$a;

    new-instance v2, Le/e$a;

    invoke-direct {v2, p1, v0, v1}, Le/e$a;-><init>(Le/a/a/e$d;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Le/L$a;->a(Le/N;)Le/L$a;

    iget-object p1, p0, Le/e$c;->k:Le/A;

    invoke-virtual {v3, p1}, Le/L$a;->a(Le/A;)Le/L$a;

    iget-wide v0, p0, Le/e$c;->l:J

    invoke-virtual {v3, v0, v1}, Le/L$a;->b(J)Le/L$a;

    iget-wide v0, p0, Le/e$c;->m:J

    invoke-virtual {v3, v0, v1}, Le/L$a;->a(J)Le/L$a;

    invoke-virtual {v3}, Le/L$a;->a()Le/L;

    move-result-object p1

    return-object p1
.end method

.method public final a(Le/a/a/e$b;)V
    .locals 7
    .param p1    # Le/a/a/e$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "editor"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Le/a/a/e$b;->a(I)Lf/A;

    move-result-object p1

    invoke-static {p1}, Lf/s;->a(Lf/A;)Lf/j;

    move-result-object p1

    iget-object v1, p0, Le/e$c;->d:Ljava/lang/String;

    invoke-interface {p1, v1}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v1

    const/16 v2, 0xa

    invoke-interface {v1, v2}, Lf/j;->writeByte(I)Lf/j;

    iget-object v1, p0, Le/e$c;->f:Ljava/lang/String;

    invoke-interface {p1, v1}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v1

    invoke-interface {v1, v2}, Lf/j;->writeByte(I)Lf/j;

    iget-object v1, p0, Le/e$c;->e:Le/B;

    invoke-virtual {v1}, Le/B;->size()I

    move-result v1

    int-to-long v3, v1

    invoke-interface {p1, v3, v4}, Lf/j;->g(J)Lf/j;

    move-result-object v1

    invoke-interface {v1, v2}, Lf/j;->writeByte(I)Lf/j;

    iget-object v1, p0, Le/e$c;->e:Le/B;

    invoke-virtual {v1}, Le/B;->size()I

    move-result v1

    move v3, v0

    :goto_0
    const-string v4, ": "

    if-ge v3, v1, :cond_0

    iget-object v5, p0, Le/e$c;->e:Le/B;

    invoke-virtual {v5, v3}, Le/B;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v5}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v5

    invoke-interface {v5, v4}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v4

    iget-object v5, p0, Le/e$c;->e:Le/B;

    invoke-virtual {v5, v3}, Le/B;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v4

    invoke-interface {v4, v2}, Lf/j;->writeByte(I)Lf/j;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Le/a/c/l;

    iget-object v3, p0, Le/e$c;->g:Le/G;

    iget v5, p0, Le/e$c;->h:I

    iget-object v6, p0, Le/e$c;->i:Ljava/lang/String;

    invoke-direct {v1, v3, v5, v6}, Le/a/c/l;-><init>(Le/G;ILjava/lang/String;)V

    invoke-virtual {v1}, Le/a/c/l;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v1

    invoke-interface {v1, v2}, Lf/j;->writeByte(I)Lf/j;

    iget-object v1, p0, Le/e$c;->j:Le/B;

    invoke-virtual {v1}, Le/B;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    int-to-long v5, v1

    invoke-interface {p1, v5, v6}, Lf/j;->g(J)Lf/j;

    move-result-object v1

    invoke-interface {v1, v2}, Lf/j;->writeByte(I)Lf/j;

    iget-object v1, p0, Le/e$c;->j:Le/B;

    invoke-virtual {v1}, Le/B;->size()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_1

    iget-object v3, p0, Le/e$c;->j:Le/B;

    invoke-virtual {v3, v0}, Le/B;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v3

    invoke-interface {v3, v4}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v3

    iget-object v5, p0, Le/e$c;->j:Le/B;

    invoke-virtual {v5, v0}, Le/B;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v3

    invoke-interface {v3, v2}, Lf/j;->writeByte(I)Lf/j;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    sget-object v0, Le/e$c;->a:Ljava/lang/String;

    invoke-interface {p1, v0}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v0

    invoke-interface {v0, v4}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v0

    iget-wide v5, p0, Le/e$c;->l:J

    invoke-interface {v0, v5, v6}, Lf/j;->g(J)Lf/j;

    move-result-object v0

    invoke-interface {v0, v2}, Lf/j;->writeByte(I)Lf/j;

    sget-object v0, Le/e$c;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v0

    invoke-interface {v0, v4}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v0

    iget-wide v3, p0, Le/e$c;->m:J

    invoke-interface {v0, v3, v4}, Lf/j;->g(J)Lf/j;

    move-result-object v0

    invoke-interface {v0, v2}, Lf/j;->writeByte(I)Lf/j;

    invoke-direct {p0}, Le/e$c;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1, v2}, Lf/j;->writeByte(I)Lf/j;

    iget-object v0, p0, Le/e$c;->k:Le/A;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Le/A;->a()Le/m;

    move-result-object v0

    invoke-virtual {v0}, Le/m;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v0

    invoke-interface {v0, v2}, Lf/j;->writeByte(I)Lf/j;

    iget-object v0, p0, Le/e$c;->k:Le/A;

    invoke-virtual {v0}, Le/A;->c()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Le/e$c;->a(Lf/j;Ljava/util/List;)V

    iget-object v0, p0, Le/e$c;->k:Le/A;

    invoke-virtual {v0}, Le/A;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Le/e$c;->a(Lf/j;Ljava/util/List;)V

    iget-object v0, p0, Le/e$c;->k:Le/A;

    invoke-virtual {v0}, Le/A;->d()Le/P;

    move-result-object v0

    invoke-virtual {v0}, Le/P;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    move-result-object v0

    invoke-interface {v0, v2}, Lf/j;->writeByte(I)Lf/j;

    goto :goto_2

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1

    :cond_3
    :goto_2
    invoke-interface {p1}, Lf/A;->close()V

    return-void
.end method

.method public final a(Le/I;Le/L;)Z
    .locals 2
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/e$c;->d:Ljava/lang/String;

    invoke-virtual {p1}, Le/I;->h()Le/C;

    move-result-object v1

    invoke-virtual {v1}, Le/C;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/e$c;->f:Ljava/lang/String;

    invoke-virtual {p1}, Le/I;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Le/e;->a:Le/e$b;

    iget-object v1, p0, Le/e$c;->e:Le/B;

    invoke-virtual {v0, p2, v1, p1}, Le/e$b;->a(Le/L;Le/B;Le/I;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
