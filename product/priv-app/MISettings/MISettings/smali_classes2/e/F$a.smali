.class public final Le/F$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/F;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private A:I

.field private B:I

.field private a:Le/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private b:Le/o;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le/D;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le/D;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private e:Le/x$b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private f:Z

.field private g:Le/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field private j:Le/s;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private k:Le/e;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private l:Le/v;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private m:Ljava/net/Proxy;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private n:Ljava/net/ProxySelector;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private o:Le/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private p:Ljavax/net/SocketFactory;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private q:Ljavax/net/ssl/SSLSocketFactory;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private r:Ljavax/net/ssl/X509TrustManager;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Le/G;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private u:Ljavax/net/ssl/HostnameVerifier;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private v:Le/j;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private w:Le/a/i/c;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Le/t;

    invoke-direct {v0}, Le/t;-><init>()V

    iput-object v0, p0, Le/F$a;->a:Le/t;

    new-instance v0, Le/o;

    invoke-direct {v0}, Le/o;-><init>()V

    iput-object v0, p0, Le/F$a;->b:Le/o;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Le/F$a;->c:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Le/F$a;->d:Ljava/util/List;

    sget-object v0, Le/x;->a:Le/x;

    invoke-static {v0}, Le/a/d;->a(Le/x;)Le/x$b;

    move-result-object v0

    iput-object v0, p0, Le/F$a;->e:Le/x$b;

    const/4 v0, 0x1

    iput-boolean v0, p0, Le/F$a;->f:Z

    sget-object v1, Le/c;->a:Le/c;

    iput-object v1, p0, Le/F$a;->g:Le/c;

    iput-boolean v0, p0, Le/F$a;->h:Z

    iput-boolean v0, p0, Le/F$a;->i:Z

    sget-object v0, Le/s;->a:Le/s;

    iput-object v0, p0, Le/F$a;->j:Le/s;

    sget-object v0, Le/v;->a:Le/v;

    iput-object v0, p0, Le/F$a;->l:Le/v;

    sget-object v0, Le/c;->a:Le/c;

    iput-object v0, p0, Le/F$a;->o:Le/c;

    invoke-static {}, Ljavax/net/SocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    const-string v1, "SocketFactory.getDefault()"

    invoke-static {v0, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Le/F$a;->p:Ljavax/net/SocketFactory;

    sget-object v0, Le/F;->c:Le/F$b;

    invoke-virtual {v0}, Le/F$b;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Le/F$a;->s:Ljava/util/List;

    sget-object v0, Le/F;->c:Le/F$b;

    invoke-virtual {v0}, Le/F$b;->b()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Le/F$a;->t:Ljava/util/List;

    sget-object v0, Le/a/i/d;->a:Le/a/i/d;

    iput-object v0, p0, Le/F$a;->u:Ljavax/net/ssl/HostnameVerifier;

    sget-object v0, Le/j;->a:Le/j;

    iput-object v0, p0, Le/F$a;->v:Le/j;

    const/16 v0, 0x2710

    iput v0, p0, Le/F$a;->y:I

    iput v0, p0, Le/F$a;->z:I

    iput v0, p0, Le/F$a;->A:I

    return-void
.end method


# virtual methods
.method public final A()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/F$a;->q:Ljavax/net/ssl/SSLSocketFactory;

    return-object v0
.end method

.method public final B()I
    .locals 1

    iget v0, p0, Le/F$a;->A:I

    return v0
.end method

.method public final C()Ljavax/net/ssl/X509TrustManager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/F$a;->r:Ljavax/net/ssl/X509TrustManager;

    return-object v0
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Le/F$a;
    .locals 1
    .param p3    # Ljava/util/concurrent/TimeUnit;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "unit"

    invoke-static {p3, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeout"

    invoke-static {v0, p1, p2, p3}, Le/a/d;->a(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result p1

    iput p1, p0, Le/F$a;->y:I

    return-object p0
.end method

.method public final a(Le/e;)Le/F$a;
    .locals 0
    .param p1    # Le/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iput-object p1, p0, Le/F$a;->k:Le/e;

    return-object p0
.end method

.method public final a()Le/F;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Le/F;

    invoke-direct {v0, p0}, Le/F;-><init>(Le/F$a;)V

    return-object v0
.end method

.method public final b(JLjava/util/concurrent/TimeUnit;)Le/F$a;
    .locals 1
    .param p3    # Ljava/util/concurrent/TimeUnit;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "unit"

    invoke-static {p3, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeout"

    invoke-static {v0, p1, p2, p3}, Le/a/d;->a(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result p1

    iput p1, p0, Le/F$a;->z:I

    return-object p0
.end method

.method public final b()Le/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->g:Le/c;

    return-object v0
.end method

.method public final c(JLjava/util/concurrent/TimeUnit;)Le/F$a;
    .locals 1
    .param p3    # Ljava/util/concurrent/TimeUnit;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "unit"

    invoke-static {p3, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeout"

    invoke-static {v0, p1, p2, p3}, Le/a/d;->a(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result p1

    iput p1, p0, Le/F$a;->A:I

    return-object p0
.end method

.method public final c()Le/e;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/F$a;->k:Le/e;

    return-object v0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Le/F$a;->x:I

    return v0
.end method

.method public final e()Le/a/i/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/F$a;->w:Le/a/i/c;

    return-object v0
.end method

.method public final f()Le/j;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->v:Le/j;

    return-object v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Le/F$a;->y:I

    return v0
.end method

.method public final h()Le/o;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->b:Le/o;

    return-object v0
.end method

.method public final i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Le/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->s:Ljava/util/List;

    return-object v0
.end method

.method public final j()Le/s;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->j:Le/s;

    return-object v0
.end method

.method public final k()Le/t;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->a:Le/t;

    return-object v0
.end method

.method public final l()Le/v;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->l:Le/v;

    return-object v0
.end method

.method public final m()Le/x$b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->e:Le/x$b;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    iget-boolean v0, p0, Le/F$a;->h:Z

    return v0
.end method

.method public final o()Z
    .locals 1

    iget-boolean v0, p0, Le/F$a;->i:Z

    return v0
.end method

.method public final p()Ljavax/net/ssl/HostnameVerifier;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->u:Ljavax/net/ssl/HostnameVerifier;

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Le/D;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->c:Ljava/util/List;

    return-object v0
.end method

.method public final r()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Le/D;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->d:Ljava/util/List;

    return-object v0
.end method

.method public final s()I
    .locals 1

    iget v0, p0, Le/F$a;->B:I

    return v0
.end method

.method public final t()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Le/G;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->t:Ljava/util/List;

    return-object v0
.end method

.method public final u()Ljava/net/Proxy;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/F$a;->m:Ljava/net/Proxy;

    return-object v0
.end method

.method public final v()Le/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->o:Le/c;

    return-object v0
.end method

.method public final w()Ljava/net/ProxySelector;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Le/F$a;->n:Ljava/net/ProxySelector;

    return-object v0
.end method

.method public final x()I
    .locals 1

    iget v0, p0, Le/F$a;->z:I

    return v0
.end method

.method public final y()Z
    .locals 1

    iget-boolean v0, p0, Le/F$a;->f:Z

    return v0
.end method

.method public final z()Ljavax/net/SocketFactory;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Le/F$a;->p:Ljavax/net/SocketFactory;

    return-object v0
.end method
