.class public Le/L$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/L;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Le/I;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private b:Le/G;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private c:I

.field private d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private e:Le/A;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private f:Le/B$a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private g:Le/N;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private h:Le/L;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private i:Le/L;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private j:Le/L;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private k:J

.field private l:J

.field private m:Le/a/b/c;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Le/L$a;->c:I

    new-instance v0, Le/B$a;

    invoke-direct {v0}, Le/B$a;-><init>()V

    iput-object v0, p0, Le/L$a;->f:Le/B$a;

    return-void
.end method

.method public constructor <init>(Le/L;)V
    .locals 2
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Le/L$a;->c:I

    invoke-virtual {p1}, Le/L;->A()Le/I;

    move-result-object v0

    iput-object v0, p0, Le/L$a;->a:Le/I;

    invoke-virtual {p1}, Le/L;->y()Le/G;

    move-result-object v0

    iput-object v0, p0, Le/L$a;->b:Le/G;

    invoke-virtual {p1}, Le/L;->q()I

    move-result v0

    iput v0, p0, Le/L$a;->c:I

    invoke-virtual {p1}, Le/L;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/L$a;->d:Ljava/lang/String;

    invoke-virtual {p1}, Le/L;->s()Le/A;

    move-result-object v0

    iput-object v0, p0, Le/L$a;->e:Le/A;

    invoke-virtual {p1}, Le/L;->t()Le/B;

    move-result-object v0

    invoke-virtual {v0}, Le/B;->a()Le/B$a;

    move-result-object v0

    iput-object v0, p0, Le/L$a;->f:Le/B$a;

    invoke-virtual {p1}, Le/L;->a()Le/N;

    move-result-object v0

    iput-object v0, p0, Le/L$a;->g:Le/N;

    invoke-virtual {p1}, Le/L;->v()Le/L;

    move-result-object v0

    iput-object v0, p0, Le/L$a;->h:Le/L;

    invoke-virtual {p1}, Le/L;->p()Le/L;

    move-result-object v0

    iput-object v0, p0, Le/L$a;->i:Le/L;

    invoke-virtual {p1}, Le/L;->x()Le/L;

    move-result-object v0

    iput-object v0, p0, Le/L$a;->j:Le/L;

    invoke-virtual {p1}, Le/L;->B()J

    move-result-wide v0

    iput-wide v0, p0, Le/L$a;->k:J

    invoke-virtual {p1}, Le/L;->z()J

    move-result-wide v0

    iput-wide v0, p0, Le/L$a;->l:J

    invoke-virtual {p1}, Le/L;->r()Le/a/b/c;

    move-result-object p1

    iput-object p1, p0, Le/L$a;->m:Le/a/b/c;

    return-void
.end method

.method private final a(Ljava/lang/String;Le/L;)V
    .locals 3

    if-eqz p2, :cond_8

    invoke-virtual {p2}, Le/L;->a()Le/N;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_7

    invoke-virtual {p2}, Le/L;->v()Le/L;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v2

    :goto_1
    if-eqz v0, :cond_6

    invoke-virtual {p2}, Le/L;->p()Le/L;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_2

    :cond_2
    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    invoke-virtual {p2}, Le/L;->x()Le/L;

    move-result-object p2

    if-nez p2, :cond_3

    goto :goto_3

    :cond_3
    move v1, v2

    :goto_3
    if-eqz v1, :cond_4

    goto :goto_4

    :cond_4
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".priorResponse != null"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_5
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".cacheResponse != null"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_6
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".networkResponse != null"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_7
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".body != null"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_8
    :goto_4
    return-void
.end method

.method private final d(Le/L;)V
    .locals 1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Le/L;->a()Le/N;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "priorResponse.body != null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_1
    return-void
.end method


# virtual methods
.method public a(I)Le/L$a;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iput p1, p0, Le/L$a;->c:I

    return-object p0
.end method

.method public a(J)Le/L$a;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iput-wide p1, p0, Le/L$a;->l:J

    return-object p0
.end method

.method public a(Le/A;)Le/L$a;
    .locals 0
    .param p1    # Le/A;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iput-object p1, p0, Le/L$a;->e:Le/A;

    return-object p0
.end method

.method public a(Le/B;)Le/L$a;
    .locals 1
    .param p1    # Le/B;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "headers"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Le/B;->a()Le/B$a;

    move-result-object p1

    iput-object p1, p0, Le/L$a;->f:Le/B$a;

    return-object p0
.end method

.method public a(Le/G;)Le/L$a;
    .locals 1
    .param p1    # Le/G;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "protocol"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Le/L$a;->b:Le/G;

    return-object p0
.end method

.method public a(Le/I;)Le/L$a;
    .locals 1
    .param p1    # Le/I;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Le/L$a;->a:Le/I;

    return-object p0
.end method

.method public a(Le/L;)Le/L$a;
    .locals 1
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cacheResponse"

    invoke-direct {p0, v0, p1}, Le/L$a;->a(Ljava/lang/String;Le/L;)V

    iput-object p1, p0, Le/L$a;->i:Le/L;

    return-object p0
.end method

.method public a(Le/N;)Le/L$a;
    .locals 0
    .param p1    # Le/N;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iput-object p1, p0, Le/L$a;->g:Le/N;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Le/L$a;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Le/L$a;->d:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Le/L$a;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/L$a;->f:Le/B$a;

    invoke-virtual {v0, p1, p2}, Le/B$a;->a(Ljava/lang/String;Ljava/lang/String;)Le/B$a;

    return-object p0
.end method

.method public a()Le/L;
    .locals 19
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    iget v1, v0, Le/L$a;->c:I

    if-ltz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_4

    iget-object v3, v0, Le/L$a;->a:Le/I;

    if-eqz v3, :cond_3

    iget-object v4, v0, Le/L$a;->b:Le/G;

    if-eqz v4, :cond_2

    iget-object v5, v0, Le/L$a;->d:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget v6, v0, Le/L$a;->c:I

    iget-object v7, v0, Le/L$a;->e:Le/A;

    iget-object v1, v0, Le/L$a;->f:Le/B$a;

    invoke-virtual {v1}, Le/B$a;->a()Le/B;

    move-result-object v8

    iget-object v9, v0, Le/L$a;->g:Le/N;

    iget-object v10, v0, Le/L$a;->h:Le/L;

    iget-object v11, v0, Le/L$a;->i:Le/L;

    iget-object v12, v0, Le/L$a;->j:Le/L;

    iget-wide v13, v0, Le/L$a;->k:J

    iget-wide v1, v0, Le/L$a;->l:J

    iget-object v15, v0, Le/L$a;->m:Le/a/b/c;

    new-instance v18, Le/L;

    move-wide/from16 v16, v1

    move-object/from16 v2, v18

    move-object v1, v15

    move-wide/from16 v15, v16

    move-object/from16 v17, v1

    invoke-direct/range {v2 .. v17}, Le/L;-><init>(Le/I;Le/G;Ljava/lang/String;ILe/A;Le/B;Le/N;Le/L;Le/L;Le/L;JJLe/a/b/c;)V

    return-object v18

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "message == null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "protocol == null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "request == null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "code < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, v0, Le/L$a;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final a(Le/a/b/c;)V
    .locals 1
    .param p1    # Le/a/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "deferredTrailers"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Le/L$a;->m:Le/a/b/c;

    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Le/L$a;->c:I

    return v0
.end method

.method public b(J)Le/L$a;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iput-wide p1, p0, Le/L$a;->k:J

    return-object p0
.end method

.method public b(Le/L;)Le/L$a;
    .locals 1
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "networkResponse"

    invoke-direct {p0, v0, p1}, Le/L$a;->a(Ljava/lang/String;Le/L;)V

    iput-object p1, p0, Le/L$a;->h:Le/L;

    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Le/L$a;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Le/L$a;->f:Le/B$a;

    invoke-virtual {v0, p1, p2}, Le/B$a;->c(Ljava/lang/String;Ljava/lang/String;)Le/B$a;

    return-object p0
.end method

.method public c(Le/L;)Le/L$a;
    .locals 0
    .param p1    # Le/L;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    invoke-direct {p0, p1}, Le/L$a;->d(Le/L;)V

    iput-object p1, p0, Le/L$a;->j:Le/L;

    return-object p0
.end method
