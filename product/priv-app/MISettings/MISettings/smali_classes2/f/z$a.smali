.class public final Lf/z$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/z;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/b/g;)V
    .locals 0

    invoke-direct {p0}, Lf/z$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lf/h;I)Lf/l;
    .locals 7
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "buffer"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lf/h;->size()J

    move-result-wide v1

    int-to-long v5, p2

    const-wide/16 v3, 0x0

    invoke-static/range {v1 .. v6}, Lf/c;->a(JJJ)V

    iget-object v0, p1, Lf/h;->c:Lf/x;

    const/4 v1, 0x0

    move-object v3, v0

    move v0, v1

    move v2, v0

    :goto_0
    const/4 v4, 0x0

    if-ge v0, p2, :cond_2

    if-eqz v3, :cond_1

    iget v4, v3, Lf/x;->d:I

    iget v5, v3, Lf/x;->c:I

    if-eq v4, v5, :cond_0

    sub-int/2addr v4, v5

    add-int/2addr v0, v4

    add-int/lit8 v2, v2, 0x1

    iget-object v3, v3, Lf/x;->g:Lf/x;

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "s.limit == s.pos"

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v4

    :cond_2
    new-array v0, v2, [[B

    mul-int/lit8 v2, v2, 0x2

    new-array v2, v2, [I

    iget-object p1, p1, Lf/h;->c:Lf/x;

    move-object v3, p1

    move p1, v1

    :goto_1
    if-ge v1, p2, :cond_4

    if-eqz v3, :cond_3

    iget-object v5, v3, Lf/x;->b:[B

    aput-object v5, v0, p1

    iget v5, v3, Lf/x;->d:I

    iget v6, v3, Lf/x;->c:I

    sub-int/2addr v5, v6

    add-int/2addr v1, v5

    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v5

    aput v5, v2, p1

    array-length v5, v0

    add-int/2addr v5, p1

    iget v6, v3, Lf/x;->c:I

    aput v6, v2, v5

    const/4 v5, 0x1

    iput-boolean v5, v3, Lf/x;->e:Z

    add-int/2addr p1, v5

    iget-object v3, v3, Lf/x;->g:Lf/x;

    goto :goto_1

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v4

    :cond_4
    new-instance p1, Lf/z;

    invoke-direct {p1, v0, v2, v4}, Lf/z;-><init>([[B[ILkotlin/jvm/b/g;)V

    return-object p1
.end method
