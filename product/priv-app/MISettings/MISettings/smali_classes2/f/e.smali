.class public final Lf/e;
.super Ljava/lang/Object;

# interfaces
.implements Lf/A;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/d;->a(Lf/A;)Lf/A;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lf/d;

.field final synthetic b:Lf/A;


# direct methods
.method constructor <init>(Lf/d;Lf/A;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/A;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lf/e;->a:Lf/d;

    iput-object p2, p0, Lf/e;->b:Lf/A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/h;J)V
    .locals 7
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lf/h;->size()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    move-wide v5, p2

    invoke-static/range {v1 .. v6}, Lf/c;->a(JJJ)V

    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-lez v2, :cond_4

    iget-object v2, p1, Lf/h;->c:Lf/x;

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    :goto_1
    const/high16 v4, 0x10000

    int-to-long v4, v4

    cmp-long v4, v0, v4

    if-gez v4, :cond_2

    iget v4, v2, Lf/x;->d:I

    iget v5, v2, Lf/x;->c:I

    sub-int/2addr v4, v5

    int-to-long v4, v4

    add-long/2addr v0, v4

    cmp-long v4, v0, p2

    if-ltz v4, :cond_0

    move-wide v0, p2

    goto :goto_2

    :cond_0
    iget-object v2, v2, Lf/x;->g:Lf/x;

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v3

    :cond_2
    :goto_2
    const/4 v2, 0x0

    iget-object v3, p0, Lf/e;->a:Lf/d;

    invoke-virtual {v3}, Lf/d;->j()V

    :try_start_0
    iget-object v3, p0, Lf/e;->b:Lf/A;

    invoke-interface {v3, p1, v0, v1}, Lf/A;->a(Lf/h;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr p2, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lf/e;->a:Lf/d;

    invoke-virtual {v1, v0}, Lf/d;->a(Z)V

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_0
    move-exception p1

    :try_start_1
    iget-object p2, p0, Lf/e;->a:Lf/d;

    invoke-virtual {p2, p1}, Lf/d;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_3
    iget-object p2, p0, Lf/e;->a:Lf/d;

    invoke-virtual {p2, v2}, Lf/d;->a(Z)V

    throw p1

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v3

    :cond_4
    return-void
.end method

.method public bridge synthetic c()Lf/E;
    .locals 1

    invoke-virtual {p0}, Lf/e;->c()Lf/d;

    move-result-object v0

    return-object v0
.end method

.method public c()Lf/d;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lf/e;->a:Lf/d;

    return-object v0
.end method

.method public close()V
    .locals 3

    iget-object v0, p0, Lf/e;->a:Lf/d;

    invoke-virtual {v0}, Lf/d;->j()V

    :try_start_0
    iget-object v0, p0, Lf/e;->b:Lf/A;

    invoke-interface {v0}, Lf/A;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v1, p0, Lf/e;->a:Lf/d;

    invoke-virtual {v1, v0}, Lf/d;->a(Z)V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v1, p0, Lf/e;->a:Lf/d;

    invoke-virtual {v1, v0}, Lf/d;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    iget-object v1, p0, Lf/e;->a:Lf/d;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lf/d;->a(Z)V

    throw v0
.end method

.method public flush()V
    .locals 3

    iget-object v0, p0, Lf/e;->a:Lf/d;

    invoke-virtual {v0}, Lf/d;->j()V

    :try_start_0
    iget-object v0, p0, Lf/e;->b:Lf/A;

    invoke-interface {v0}, Lf/A;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v1, p0, Lf/e;->a:Lf/d;

    invoke-virtual {v1, v0}, Lf/d;->a(Z)V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v1, p0, Lf/e;->a:Lf/d;

    invoke-virtual {v1, v0}, Lf/d;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    iget-object v1, p0, Lf/e;->a:Lf/d;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lf/d;->a(Z)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncTimeout.sink("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/e;->b:Lf/A;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
