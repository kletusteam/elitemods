.class public final Lf/y;
.super Ljava/lang/Object;


# static fields
.field public static a:Lf/x;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field public static b:J
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation
.end field

.field public static final c:Lf/y;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/y;

    invoke-direct {v0}, Lf/y;-><init>()V

    sput-object v0, Lf/y;->c:Lf/y;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a()Lf/x;
    .locals 6
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    sget-object v0, Lf/y;->c:Lf/y;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/y;->a:Lf/x;

    if-eqz v1, :cond_0

    iget-object v2, v1, Lf/x;->g:Lf/x;

    sput-object v2, Lf/y;->a:Lf/x;

    const/4 v2, 0x0

    iput-object v2, v1, Lf/x;->g:Lf/x;

    sget-wide v2, Lf/y;->b:J

    const/16 v4, 0x2000

    int-to-long v4, v4

    sub-long/2addr v2, v4

    sput-wide v2, Lf/y;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :cond_0
    monitor-exit v0

    new-instance v0, Lf/x;

    invoke-direct {v0}, Lf/x;-><init>()V

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static final a(Lf/x;)V
    .locals 8
    .param p0    # Lf/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "segment"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/x;->g:Lf/x;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/x;->h:Lf/x;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lf/x;->e:Z

    if-eqz v0, :cond_1

    return-void

    :cond_1
    sget-object v0, Lf/y;->c:Lf/y;

    monitor-enter v0

    :try_start_0
    sget-wide v2, Lf/y;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v4, 0x2000

    int-to-long v4, v4

    add-long/2addr v2, v4

    const-wide/32 v6, 0x10000

    cmp-long v2, v2, v6

    if-lez v2, :cond_2

    monitor-exit v0

    return-void

    :cond_2
    :try_start_1
    sget-wide v2, Lf/y;->b:J

    add-long/2addr v2, v4

    sput-wide v2, Lf/y;->b:J

    sget-object v2, Lf/y;->a:Lf/x;

    iput-object v2, p0, Lf/x;->g:Lf/x;

    iput v1, p0, Lf/x;->d:I

    iget v1, p0, Lf/x;->d:I

    iput v1, p0, Lf/x;->c:I

    sput-object p0, Lf/y;->a:Lf/x;

    sget-object p0, Lkotlin/r;->a:Lkotlin/r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0

    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
