.class public final Lf/q;
.super Ljava/lang/Object;

# interfaces
.implements Lf/C;


# instance fields
.field private a:I

.field private b:Z

.field private final c:Lf/k;

.field private final d:Ljava/util/zip/Inflater;


# direct methods
.method public constructor <init>(Lf/k;Ljava/util/zip/Inflater;)V
    .locals 1
    .param p1    # Lf/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/zip/Inflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inflater"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/q;->c:Lf/k;

    iput-object p2, p0, Lf/q;->d:Ljava/util/zip/Inflater;

    return-void
.end method

.method private final b()V
    .locals 4

    iget v0, p0, Lf/q;->a:I

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lf/q;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v1}, Ljava/util/zip/Inflater;->getRemaining()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lf/q;->a:I

    sub-int/2addr v1, v0

    iput v1, p0, Lf/q;->a:I

    iget-object v1, p0, Lf/q;->c:Lf/k;

    int-to-long v2, v0

    invoke-interface {v1, v2, v3}, Lf/k;->skip(J)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/q;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->needsInput()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-direct {p0}, Lf/q;->b()V

    iget-object v0, p0, Lf/q;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->getRemaining()I

    move-result v0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    iget-object v0, p0, Lf/q;->c:Lf/k;

    invoke-interface {v0}, Lf/k;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    return v2

    :cond_2
    iget-object v0, p0, Lf/q;->c:Lf/k;

    invoke-interface {v0}, Lf/k;->getBuffer()Lf/h;

    move-result-object v0

    iget-object v0, v0, Lf/h;->c:Lf/x;

    if-eqz v0, :cond_3

    iget v2, v0, Lf/x;->d:I

    iget v3, v0, Lf/x;->c:I

    sub-int/2addr v2, v3

    iput v2, p0, Lf/q;->a:I

    iget-object v2, p0, Lf/q;->d:Ljava/util/zip/Inflater;

    iget-object v0, v0, Lf/x;->b:[B

    iget v4, p0, Lf/q;->a:I

    invoke-virtual {v2, v0, v3, v4}, Ljava/util/zip/Inflater;->setInput([BII)V

    return v1

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Lf/h;J)J
    .locals 7
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    const/4 v3, 0x1

    if-ltz v2, :cond_0

    move v4, v3

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_8

    iget-boolean v4, p0, Lf/q;->b:Z

    xor-int/2addr v4, v3

    if-eqz v4, :cond_7

    if-nez v2, :cond_1

    return-wide v0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lf/q;->a()Z

    move-result v0

    :try_start_0
    invoke-virtual {p1, v3}, Lf/h;->b(I)Lf/x;

    move-result-object v1

    iget v2, v1, Lf/x;->d:I

    rsub-int v2, v2, 0x2000

    int-to-long v4, v2

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v2, v4

    iget-object v4, p0, Lf/q;->d:Ljava/util/zip/Inflater;

    iget-object v5, v1, Lf/x;->b:[B

    iget v6, v1, Lf/x;->d:I

    invoke-virtual {v4, v5, v6, v2}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result v2

    if-lez v2, :cond_2

    iget p2, v1, Lf/x;->d:I

    add-int/2addr p2, v2

    iput p2, v1, Lf/x;->d:I

    invoke-virtual {p1}, Lf/h;->size()J

    move-result-wide p2

    int-to-long v0, v2

    add-long/2addr p2, v0

    invoke-virtual {p1, p2, p3}, Lf/h;->k(J)V

    return-wide v0

    :cond_2
    iget-object v2, p0, Lf/q;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v2}, Ljava/util/zip/Inflater;->finished()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lf/q;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v2}, Ljava/util/zip/Inflater;->needsDictionary()Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_3
    if-nez v0, :cond_4

    goto :goto_1

    :cond_4
    new-instance p1, Ljava/io/EOFException;

    const-string p2, "source exhausted prematurely"

    invoke-direct {p1, p2}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    :goto_2
    invoke-direct {p0}, Lf/q;->b()V

    iget p2, v1, Lf/x;->c:I

    iget p3, v1, Lf/x;->d:I

    if-ne p2, p3, :cond_6

    invoke-virtual {v1}, Lf/x;->b()Lf/x;

    move-result-object p2

    iput-object p2, p1, Lf/h;->c:Lf/x;

    invoke-static {v1}, Lf/y;->a(Lf/x;)V
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    const-wide/16 p1, -0x1

    return-wide p1

    :catch_0
    move-exception p1

    new-instance p2, Ljava/io/IOException;

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "byteCount < 0: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public c()Lf/E;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lf/q;->c:Lf/k;

    invoke-interface {v0}, Lf/C;->c()Lf/E;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lf/q;->b:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/q;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->end()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/q;->b:Z

    iget-object v0, p0, Lf/q;->c:Lf/k;

    invoke-interface {v0}, Lf/C;->close()V

    return-void
.end method
