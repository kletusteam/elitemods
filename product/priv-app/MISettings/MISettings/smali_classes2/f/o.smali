.class public Lf/o;
.super Lf/E;


# instance fields
.field private f:Lf/E;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/E;)V
    .locals 1
    .param p1    # Lf/E;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lf/E;-><init>()V

    iput-object p1, p0, Lf/o;->f:Lf/E;

    return-void
.end method


# virtual methods
.method public a()Lf/E;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lf/o;->f:Lf/E;

    invoke-virtual {v0}, Lf/E;->a()Lf/E;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Lf/E;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lf/o;->f:Lf/E;

    invoke-virtual {v0, p1, p2}, Lf/E;->a(J)Lf/E;

    move-result-object p1

    return-object p1
.end method

.method public a(JLjava/util/concurrent/TimeUnit;)Lf/E;
    .locals 1
    .param p3    # Ljava/util/concurrent/TimeUnit;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "unit"

    invoke-static {p3, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/o;->f:Lf/E;

    invoke-virtual {v0, p1, p2, p3}, Lf/E;->a(JLjava/util/concurrent/TimeUnit;)Lf/E;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lf/E;)Lf/o;
    .locals 1
    .param p1    # Lf/E;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lf/o;->f:Lf/E;

    return-object p0
.end method

.method public b()Lf/E;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lf/o;->f:Lf/E;

    invoke-virtual {v0}, Lf/E;->b()Lf/E;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 2

    iget-object v0, p0, Lf/o;->f:Lf/E;

    invoke-virtual {v0}, Lf/E;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lf/o;->f:Lf/E;

    invoke-virtual {v0}, Lf/E;->d()Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/o;->f:Lf/E;

    invoke-virtual {v0}, Lf/E;->e()V

    return-void
.end method

.method public final g()Lf/E;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "delegate"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lf/o;->f:Lf/E;

    return-object v0
.end method
