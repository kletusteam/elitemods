.class public final Lf/f;
.super Ljava/lang/Object;

# interfaces
.implements Lf/C;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/d;->a(Lf/C;)Lf/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lf/d;

.field final synthetic b:Lf/C;


# direct methods
.method constructor <init>(Lf/d;Lf/C;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/C;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lf/f;->a:Lf/d;

    iput-object p2, p0, Lf/f;->b:Lf/C;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Lf/h;J)J
    .locals 1
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/f;->a:Lf/d;

    invoke-virtual {v0}, Lf/d;->j()V

    :try_start_0
    iget-object v0, p0, Lf/f;->b:Lf/C;

    invoke-interface {v0, p1, p2, p3}, Lf/C;->b(Lf/h;J)J

    move-result-wide p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p3, 0x1

    iget-object v0, p0, Lf/f;->a:Lf/d;

    invoke-virtual {v0, p3}, Lf/d;->a(Z)V

    return-wide p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    iget-object p2, p0, Lf/f;->a:Lf/d;

    invoke-virtual {p2, p1}, Lf/d;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    iget-object p2, p0, Lf/f;->a:Lf/d;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Lf/d;->a(Z)V

    throw p1
.end method

.method public bridge synthetic c()Lf/E;
    .locals 1

    invoke-virtual {p0}, Lf/f;->c()Lf/d;

    move-result-object v0

    return-object v0
.end method

.method public c()Lf/d;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lf/f;->a:Lf/d;

    return-object v0
.end method

.method public close()V
    .locals 3

    iget-object v0, p0, Lf/f;->a:Lf/d;

    invoke-virtual {v0}, Lf/d;->j()V

    :try_start_0
    iget-object v0, p0, Lf/f;->b:Lf/C;

    invoke-interface {v0}, Lf/C;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v1, p0, Lf/f;->a:Lf/d;

    invoke-virtual {v1, v0}, Lf/d;->a(Z)V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v1, p0, Lf/f;->a:Lf/d;

    invoke-virtual {v1, v0}, Lf/d;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    iget-object v1, p0, Lf/f;->a:Lf/d;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lf/d;->a(Z)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncTimeout.source("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/f;->b:Lf/C;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
