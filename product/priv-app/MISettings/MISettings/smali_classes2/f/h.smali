.class public final Lf/h;
.super Ljava/lang/Object;

# interfaces
.implements Lf/k;
.implements Lf/j;
.implements Ljava/lang/Cloneable;
.implements Ljava/nio/channels/ByteChannel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u00b6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u001a\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0005\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0012\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\n\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0018\u0018\u0000 \u009c\u00012\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0004\u009c\u0001\u009d\u0001B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0000H\u0016J\u0006\u0010\u0011\u001a\u00020\u0012J\u0008\u0010\u0013\u001a\u00020\u0000H\u0016J\u0008\u0010\u0014\u001a\u00020\u0012H\u0016J\u0006\u0010\u0015\u001a\u00020\u000cJ$\u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u00182\u0008\u0008\u0002\u0010\u0019\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\u001a\u001a\u00020\u000cH\u0007J\"\u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0019\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\u001a\u001a\u00020\u000cJ\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0008\u0010\u001f\u001a\u00020\u0000H\u0016J\u0008\u0010 \u001a\u00020\u0000H\u0016J\u0013\u0010!\u001a\u00020\"2\u0008\u0010#\u001a\u0004\u0018\u00010$H\u0096\u0002J\u0008\u0010%\u001a\u00020\"H\u0016J\u0008\u0010&\u001a\u00020\u0012H\u0016J\u0016\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020\u000cH\u0087\u0002\u00a2\u0006\u0002\u0008*J\u0015\u0010*\u001a\u00020(2\u0006\u0010+\u001a\u00020\u000cH\u0007\u00a2\u0006\u0002\u0008,J\u0008\u0010-\u001a\u00020.H\u0016J\u0018\u0010/\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u00100\u001a\u00020\u001cH\u0002J\u000e\u00101\u001a\u00020\u001c2\u0006\u00100\u001a\u00020\u001cJ\u000e\u00102\u001a\u00020\u001c2\u0006\u00100\u001a\u00020\u001cJ\u000e\u00103\u001a\u00020\u001c2\u0006\u00100\u001a\u00020\u001cJ\u0010\u00104\u001a\u00020\u000c2\u0006\u00105\u001a\u00020(H\u0016J\u0018\u00104\u001a\u00020\u000c2\u0006\u00105\u001a\u00020(2\u0006\u00106\u001a\u00020\u000cH\u0016J \u00104\u001a\u00020\u000c2\u0006\u00105\u001a\u00020(2\u0006\u00106\u001a\u00020\u000c2\u0006\u00107\u001a\u00020\u000cH\u0016J\u0010\u00104\u001a\u00020\u000c2\u0006\u00108\u001a\u00020\u001cH\u0016J\u0018\u00104\u001a\u00020\u000c2\u0006\u00108\u001a\u00020\u001c2\u0006\u00106\u001a\u00020\u000cH\u0016J\u0010\u00109\u001a\u00020\u000c2\u0006\u0010:\u001a\u00020\u001cH\u0016J\u0018\u00109\u001a\u00020\u000c2\u0006\u0010:\u001a\u00020\u001c2\u0006\u00106\u001a\u00020\u000cH\u0016J\u0008\u0010;\u001a\u00020<H\u0016J\u0008\u0010=\u001a\u00020\"H\u0016J\u0006\u0010>\u001a\u00020\u001cJ\u0008\u0010?\u001a\u00020\u0018H\u0016J\u0008\u0010@\u001a\u00020\u0001H\u0016J\u0018\u0010A\u001a\u00020\"2\u0006\u0010\u0019\u001a\u00020\u000c2\u0006\u00108\u001a\u00020\u001cH\u0016J(\u0010A\u001a\u00020\"2\u0006\u0010\u0019\u001a\u00020\u000c2\u0006\u00108\u001a\u00020\u001c2\u0006\u0010B\u001a\u00020.2\u0006\u0010\u001a\u001a\u00020.H\u0016J0\u0010A\u001a\u00020\"2\u0006\u0010C\u001a\u00020\n2\u0006\u0010D\u001a\u00020.2\u0006\u00108\u001a\u00020E2\u0006\u0010B\u001a\u00020.2\u0006\u0010F\u001a\u00020.H\u0002J\u0010\u0010G\u001a\u00020.2\u0006\u0010H\u001a\u00020IH\u0016J\u0010\u0010G\u001a\u00020.2\u0006\u0010H\u001a\u00020EH\u0016J \u0010G\u001a\u00020.2\u0006\u0010H\u001a\u00020E2\u0006\u0010\u0019\u001a\u00020.2\u0006\u0010\u001a\u001a\u00020.H\u0016J\u0018\u0010G\u001a\u00020\u000c2\u0006\u0010H\u001a\u00020\u00002\u0006\u0010\u001a\u001a\u00020\u000cH\u0016J\u0010\u0010J\u001a\u00020\u000c2\u0006\u0010H\u001a\u00020KH\u0016J\u0012\u0010L\u001a\u00020M2\u0008\u0008\u0002\u0010N\u001a\u00020MH\u0007J\u0008\u0010O\u001a\u00020(H\u0016J\u0008\u0010P\u001a\u00020EH\u0016J\u0010\u0010P\u001a\u00020E2\u0006\u0010\u001a\u001a\u00020\u000cH\u0016J\u0008\u0010Q\u001a\u00020\u001cH\u0016J\u0010\u0010Q\u001a\u00020\u001c2\u0006\u0010\u001a\u001a\u00020\u000cH\u0016J\u0008\u0010R\u001a\u00020\u000cH\u0016J\u000e\u0010S\u001a\u00020\u00002\u0006\u0010T\u001a\u00020<J\u0016\u0010S\u001a\u00020\u00002\u0006\u0010T\u001a\u00020<2\u0006\u0010\u001a\u001a\u00020\u000cJ \u0010S\u001a\u00020\u00122\u0006\u0010T\u001a\u00020<2\u0006\u0010\u001a\u001a\u00020\u000c2\u0006\u0010U\u001a\u00020\"H\u0002J\u0010\u0010V\u001a\u00020\u00122\u0006\u0010H\u001a\u00020EH\u0016J\u0018\u0010V\u001a\u00020\u00122\u0006\u0010H\u001a\u00020\u00002\u0006\u0010\u001a\u001a\u00020\u000cH\u0016J\u0008\u0010W\u001a\u00020\u000cH\u0016J\u0008\u0010X\u001a\u00020.H\u0016J\u0008\u0010Y\u001a\u00020.H\u0016J\u0008\u0010Z\u001a\u00020\u000cH\u0016J\u0008\u0010[\u001a\u00020\u000cH\u0016J\u0008\u0010\\\u001a\u00020]H\u0016J\u0008\u0010^\u001a\u00020]H\u0016J\u0010\u0010_\u001a\u00020\u001e2\u0006\u0010`\u001a\u00020aH\u0016J\u0018\u0010_\u001a\u00020\u001e2\u0006\u0010\u001a\u001a\u00020\u000c2\u0006\u0010`\u001a\u00020aH\u0016J\u0012\u0010b\u001a\u00020M2\u0008\u0008\u0002\u0010N\u001a\u00020MH\u0007J\u0008\u0010c\u001a\u00020\u001eH\u0016J\u0010\u0010c\u001a\u00020\u001e2\u0006\u0010\u001a\u001a\u00020\u000cH\u0016J\u0008\u0010d\u001a\u00020.H\u0016J\n\u0010e\u001a\u0004\u0018\u00010\u001eH\u0016J\u0015\u0010e\u001a\u00020\u001e2\u0006\u0010f\u001a\u00020\u000cH\u0000\u00a2\u0006\u0002\u0008gJ\u0008\u0010h\u001a\u00020\u001eH\u0016J\u0010\u0010h\u001a\u00020\u001e2\u0006\u0010i\u001a\u00020\u000cH\u0016J\u0010\u0010j\u001a\u00020\"2\u0006\u0010\u001a\u001a\u00020\u000cH\u0016J\u0010\u0010k\u001a\u00020\u00122\u0006\u0010\u001a\u001a\u00020\u000cH\u0016J8\u0010l\u001a\u0002Hm\"\u0004\u0008\u0000\u0010m2\u0006\u00106\u001a\u00020\u000c2\u001a\u0010n\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\n\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u0002Hm0oH\u0082\u0008\u00a2\u0006\u0002\u0010pJ\u0010\u0010q\u001a\u00020.2\u0006\u0010r\u001a\u00020sH\u0016J\u001f\u0010t\u001a\u00020.2\u0006\u0010r\u001a\u00020s2\u0008\u0008\u0002\u0010u\u001a\u00020\"H\u0000\u00a2\u0006\u0002\u0008vJ\u0006\u0010w\u001a\u00020\u001cJ\u0006\u0010x\u001a\u00020\u001cJ\u0006\u0010y\u001a\u00020\u001cJ\r\u0010\r\u001a\u00020\u000cH\u0007\u00a2\u0006\u0002\u0008zJ\u0010\u0010{\u001a\u00020\u00122\u0006\u0010\u001a\u001a\u00020\u000cH\u0016J\u0006\u0010|\u001a\u00020\u001cJ\u000e\u0010|\u001a\u00020\u001c2\u0006\u0010\u001a\u001a\u00020.J\u0008\u0010}\u001a\u00020~H\u0016J\u0008\u0010\u007f\u001a\u00020\u001eH\u0016J\u0018\u0010\u0080\u0001\u001a\u00020\n2\u0007\u0010\u0081\u0001\u001a\u00020.H\u0000\u00a2\u0006\u0003\u0008\u0082\u0001J\u0012\u0010\u0083\u0001\u001a\u00020.2\u0007\u0010\u0084\u0001\u001a\u00020IH\u0016J\u0012\u0010\u0083\u0001\u001a\u00020\u00002\u0007\u0010\u0084\u0001\u001a\u00020EH\u0016J\"\u0010\u0083\u0001\u001a\u00020\u00002\u0007\u0010\u0084\u0001\u001a\u00020E2\u0006\u0010\u0019\u001a\u00020.2\u0006\u0010\u001a\u001a\u00020.H\u0016J\u001a\u0010\u0083\u0001\u001a\u00020\u00122\u0007\u0010\u0084\u0001\u001a\u00020\u00002\u0006\u0010\u001a\u001a\u00020\u000cH\u0016J\u0012\u0010\u0083\u0001\u001a\u00020\u00002\u0007\u0010\u0085\u0001\u001a\u00020\u001cH\u0016J\u001b\u0010\u0083\u0001\u001a\u00020\u00022\u0008\u0010\u0084\u0001\u001a\u00030\u0086\u00012\u0006\u0010\u001a\u001a\u00020\u000cH\u0016J\u0013\u0010\u0087\u0001\u001a\u00020\u000c2\u0008\u0010\u0084\u0001\u001a\u00030\u0086\u0001H\u0016J\u0011\u0010\u0088\u0001\u001a\u00020\u00002\u0006\u00105\u001a\u00020.H\u0016J\u0012\u0010\u0089\u0001\u001a\u00020\u00002\u0007\u0010\u008a\u0001\u001a\u00020\u000cH\u0016J\u0012\u0010\u008b\u0001\u001a\u00020\u00002\u0007\u0010\u008a\u0001\u001a\u00020\u000cH\u0016J\u0012\u0010\u008c\u0001\u001a\u00020\u00002\u0007\u0010\u008d\u0001\u001a\u00020.H\u0016J\u0012\u0010\u008e\u0001\u001a\u00020\u00002\u0007\u0010\u008d\u0001\u001a\u00020.H\u0016J\u0012\u0010\u008f\u0001\u001a\u00020\u00002\u0007\u0010\u008a\u0001\u001a\u00020\u000cH\u0016J\u0012\u0010\u0090\u0001\u001a\u00020\u00002\u0007\u0010\u008a\u0001\u001a\u00020\u000cH\u0016J\u0012\u0010\u0091\u0001\u001a\u00020\u00002\u0007\u0010\u0092\u0001\u001a\u00020.H\u0016J\u0012\u0010\u0093\u0001\u001a\u00020\u00002\u0007\u0010\u0092\u0001\u001a\u00020.H\u0016J\u001a\u0010\u0094\u0001\u001a\u00020\u00002\u0007\u0010\u0095\u0001\u001a\u00020\u001e2\u0006\u0010`\u001a\u00020aH\u0016J,\u0010\u0094\u0001\u001a\u00020\u00002\u0007\u0010\u0095\u0001\u001a\u00020\u001e2\u0007\u0010\u0096\u0001\u001a\u00020.2\u0007\u0010\u0097\u0001\u001a\u00020.2\u0006\u0010`\u001a\u00020aH\u0016J\u001b\u0010\u0098\u0001\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u00182\u0008\u0008\u0002\u0010\u001a\u001a\u00020\u000cH\u0007J\u0012\u0010\u0099\u0001\u001a\u00020\u00002\u0007\u0010\u0095\u0001\u001a\u00020\u001eH\u0016J$\u0010\u0099\u0001\u001a\u00020\u00002\u0007\u0010\u0095\u0001\u001a\u00020\u001e2\u0007\u0010\u0096\u0001\u001a\u00020.2\u0007\u0010\u0097\u0001\u001a\u00020.H\u0016J\u0012\u0010\u009a\u0001\u001a\u00020\u00002\u0007\u0010\u009b\u0001\u001a\u00020.H\u0016R\u0014\u0010\u0006\u001a\u00020\u00008VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0002\n\u0000R&\u0010\r\u001a\u00020\u000c2\u0006\u0010\u000b\u001a\u00020\u000c8\u0007@@X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u009e\u0001"
    }
    d2 = {
        "Lokio/Buffer;",
        "Lokio/BufferedSource;",
        "Lokio/BufferedSink;",
        "",
        "Ljava/nio/channels/ByteChannel;",
        "()V",
        "buffer",
        "getBuffer",
        "()Lokio/Buffer;",
        "head",
        "Lokio/Segment;",
        "<set-?>",
        "",
        "size",
        "()J",
        "setSize$jvm",
        "(J)V",
        "clear",
        "",
        "clone",
        "close",
        "completeSegmentByteCount",
        "copyTo",
        "out",
        "Ljava/io/OutputStream;",
        "offset",
        "byteCount",
        "digest",
        "Lokio/ByteString;",
        "algorithm",
        "",
        "emit",
        "emitCompleteSegments",
        "equals",
        "",
        "other",
        "",
        "exhausted",
        "flush",
        "get",
        "",
        "pos",
        "getByte",
        "index",
        "-deprecated_getByte",
        "hashCode",
        "",
        "hmac",
        "key",
        "hmacSha1",
        "hmacSha256",
        "hmacSha512",
        "indexOf",
        "b",
        "fromIndex",
        "toIndex",
        "bytes",
        "indexOfElement",
        "targetBytes",
        "inputStream",
        "Ljava/io/InputStream;",
        "isOpen",
        "md5",
        "outputStream",
        "peek",
        "rangeEquals",
        "bytesOffset",
        "segment",
        "segmentPos",
        "",
        "bytesLimit",
        "read",
        "sink",
        "Ljava/nio/ByteBuffer;",
        "readAll",
        "Lokio/Sink;",
        "readAndWriteUnsafe",
        "Lokio/Buffer$UnsafeCursor;",
        "unsafeCursor",
        "readByte",
        "readByteArray",
        "readByteString",
        "readDecimalLong",
        "readFrom",
        "input",
        "forever",
        "readFully",
        "readHexadecimalUnsignedLong",
        "readInt",
        "readIntLe",
        "readLong",
        "readLongLe",
        "readShort",
        "",
        "readShortLe",
        "readString",
        "charset",
        "Ljava/nio/charset/Charset;",
        "readUnsafe",
        "readUtf8",
        "readUtf8CodePoint",
        "readUtf8Line",
        "newline",
        "readUtf8Line$jvm",
        "readUtf8LineStrict",
        "limit",
        "request",
        "require",
        "seek",
        "T",
        "lambda",
        "Lkotlin/Function2;",
        "(JLkotlin/jvm/functions/Function2;)Ljava/lang/Object;",
        "select",
        "options",
        "Lokio/Options;",
        "selectPrefix",
        "selectTruncated",
        "selectPrefix$jvm",
        "sha1",
        "sha256",
        "sha512",
        "-deprecated_size",
        "skip",
        "snapshot",
        "timeout",
        "Lokio/Timeout;",
        "toString",
        "writableSegment",
        "minimumCapacity",
        "writableSegment$jvm",
        "write",
        "source",
        "byteString",
        "Lokio/Source;",
        "writeAll",
        "writeByte",
        "writeDecimalLong",
        "v",
        "writeHexadecimalUnsignedLong",
        "writeInt",
        "i",
        "writeIntLe",
        "writeLong",
        "writeLongLe",
        "writeShort",
        "s",
        "writeShortLe",
        "writeString",
        "string",
        "beginIndex",
        "endIndex",
        "writeTo",
        "writeUtf8",
        "writeUtf8CodePoint",
        "codePoint",
        "Companion",
        "UnsafeCursor",
        "jvm"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xb
    }
.end annotation


# static fields
.field private static final a:[B

.field public static final b:Lf/h$a;


# instance fields
.field public c:Lf/x;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/h$a;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Lf/h;->b:Lf/h$a;

    sget-object v0, Lkotlin/g/c;->a:Ljava/nio/charset/Charset;

    const-string v1, "0123456789abcdef"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    const-string v1, "(this as java.lang.String).getBytes(charset)"

    invoke-static {v0, v1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lf/h;->a:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bridge synthetic a(Lf/h;Lf/t;ZILjava/lang/Object;)I
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lf/h;->a(Lf/t;Z)I

    move-result p0

    return p0
.end method

.method private final a(Lf/x;I[BII)Z
    .locals 5

    iget v0, p1, Lf/x;->d:I

    iget-object v1, p1, Lf/x;->b:[B

    :goto_0
    if-ge p4, p5, :cond_3

    if-ne p2, v0, :cond_1

    iget-object p1, p1, Lf/x;->g:Lf/x;

    if-eqz p1, :cond_0

    iget-object p2, p1, Lf/x;->b:[B

    iget v0, p1, Lf/x;->c:I

    iget v1, p1, Lf/x;->d:I

    move v4, v1

    move-object v1, p2

    move p2, v0

    move v0, v4

    goto :goto_1

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1

    :cond_1
    :goto_1
    aget-byte v2, v1, p2

    aget-byte v3, p3, p4

    if-eq v2, v3, :cond_2

    const/4 p1, 0x0

    return p1

    :cond_2
    add-int/lit8 p2, p2, 0x1

    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    return p1
.end method


# virtual methods
.method public a(Lf/t;)I
    .locals 3
    .param p1    # Lf/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "options"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lf/h;->a(Lf/h;Lf/t;ZILjava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Lf/t;->a()[Lf/l;

    move-result-object p1

    aget-object p1, p1, v0

    invoke-virtual {p1}, Lf/l;->k()I

    move-result p1

    int-to-long v1, p1

    invoke-virtual {p0, v1, v2}, Lf/h;->skip(J)V

    return v0
.end method

.method public final a(Lf/t;Z)I
    .locals 18
    .param p1    # Lf/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "options"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lf/h;->c:Lf/x;

    const/4 v3, -0x2

    const/4 v4, -0x1

    if-eqz v2, :cond_12

    iget-object v5, v2, Lf/x;->b:[B

    iget v6, v2, Lf/x;->c:I

    iget v7, v2, Lf/x;->d:I

    invoke-virtual/range {p1 .. p1}, Lf/t;->c()[I

    move-result-object v1

    const/4 v8, 0x0

    move v10, v4

    move v9, v6

    move v11, v7

    move-object v6, v2

    move-object v7, v5

    move v5, v8

    :goto_0
    add-int/lit8 v12, v5, 0x1

    aget v5, v1, v5

    add-int/lit8 v13, v12, 0x1

    aget v12, v1, v12

    if-eq v12, v4, :cond_0

    move v10, v12

    :cond_0
    if-nez v6, :cond_1

    goto :goto_3

    :cond_1
    const/4 v12, 0x0

    if-gez v5, :cond_b

    mul-int/lit8 v5, v5, -0x1

    add-int v14, v13, v5

    :goto_1
    add-int/lit8 v5, v9, 0x1

    aget-byte v9, v7, v9

    and-int/lit16 v9, v9, 0xff

    add-int/lit8 v15, v13, 0x1

    aget v13, v1, v13

    if-eq v9, v13, :cond_2

    return v10

    :cond_2
    if-ne v15, v14, :cond_3

    const/4 v9, 0x1

    goto :goto_2

    :cond_3
    move v9, v8

    :goto_2
    if-ne v5, v11, :cond_9

    if-eqz v6, :cond_8

    iget-object v5, v6, Lf/x;->g:Lf/x;

    if-eqz v5, :cond_7

    iget v6, v5, Lf/x;->c:I

    iget-object v7, v5, Lf/x;->b:[B

    iget v11, v5, Lf/x;->d:I

    if-ne v5, v2, :cond_6

    if-nez v9, :cond_5

    :goto_3
    if-eqz p2, :cond_4

    return v3

    :cond_4
    return v10

    :cond_5
    move v5, v6

    move-object v6, v12

    goto :goto_4

    :cond_6
    move/from16 v17, v6

    move-object v6, v5

    move/from16 v5, v17

    goto :goto_4

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v12

    :cond_8
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v12

    :cond_9
    :goto_4
    if-eqz v9, :cond_a

    aget v9, v1, v15

    goto :goto_6

    :cond_a
    move v9, v5

    move v13, v15

    goto :goto_1

    :cond_b
    add-int/lit8 v14, v9, 0x1

    aget-byte v9, v7, v9

    and-int/lit16 v9, v9, 0xff

    add-int v15, v13, v5

    :goto_5
    if-ne v13, v15, :cond_c

    return v10

    :cond_c
    aget v3, v1, v13

    if-ne v9, v3, :cond_11

    add-int/2addr v13, v5

    aget v9, v1, v13

    if-ne v14, v11, :cond_f

    iget-object v3, v6, Lf/x;->g:Lf/x;

    if-eqz v3, :cond_e

    iget v5, v3, Lf/x;->c:I

    iget-object v6, v3, Lf/x;->b:[B

    iget v7, v3, Lf/x;->d:I

    if-ne v3, v2, :cond_d

    move v11, v7

    move-object v7, v6

    move-object v6, v12

    goto :goto_6

    :cond_d
    move v11, v7

    move-object v7, v6

    move-object v6, v3

    goto :goto_6

    :cond_e
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v12

    :cond_f
    move v5, v14

    :goto_6
    if-ltz v9, :cond_10

    return v9

    :cond_10
    neg-int v3, v9

    move v9, v5

    move v5, v3

    const/4 v3, -0x2

    goto/16 :goto_0

    :cond_11
    add-int/lit8 v13, v13, 0x1

    const/4 v3, -0x2

    goto :goto_5

    :cond_12
    if-eqz p2, :cond_13

    const/16 v16, -0x2

    goto :goto_7

    :cond_13
    move/from16 v16, v4

    :goto_7
    return v16
.end method

.method public a([BII)I
    .locals 7
    .param p1    # [B
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v0, p1

    int-to-long v1, v0

    int-to-long v3, p2

    int-to-long v5, p3

    invoke-static/range {v1 .. v6}, Lf/c;->a(JJJ)V

    iget-object v0, p0, Lf/h;->c:Lf/x;

    if-eqz v0, :cond_1

    iget v1, v0, Lf/x;->d:I

    iget v2, v0, Lf/x;->c:I

    sub-int/2addr v1, v2

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result p3

    iget-object v1, v0, Lf/x;->b:[B

    iget v2, v0, Lf/x;->c:I

    invoke-static {v1, v2, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, v0, Lf/x;->c:I

    add-int/2addr p1, p3

    iput p1, v0, Lf/x;->c:I

    iget-wide p1, p0, Lf/h;->d:J

    int-to-long v1, p3

    sub-long/2addr p1, v1

    iput-wide p1, p0, Lf/h;->d:J

    iget p1, v0, Lf/x;->c:I

    iget p2, v0, Lf/x;->d:I

    if-ne p1, p2, :cond_0

    invoke-virtual {v0}, Lf/x;->b()Lf/x;

    move-result-object p1

    iput-object p1, p0, Lf/h;->c:Lf/x;

    invoke-static {v0}, Lf/y;->a(Lf/x;)V

    :cond_0
    return p3

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public a(BJJ)J
    .locals 16

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-wide/from16 v2, p2

    move-wide/from16 v4, p4

    const-wide/16 v6, 0x0

    cmp-long v8, v6, v2

    if-lez v8, :cond_0

    goto :goto_0

    :cond_0
    cmp-long v8, v4, v2

    if-ltz v8, :cond_1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v8, 0x0

    :goto_1
    if-eqz v8, :cond_14

    iget-wide v8, v0, Lf/h;->d:J

    cmp-long v10, v4, v8

    if-lez v10, :cond_2

    move-wide v4, v8

    :cond_2
    cmp-long v8, v2, v4

    const-wide/16 v9, -0x1

    if-nez v8, :cond_3

    return-wide v9

    :cond_3
    iget-object v8, v0, Lf/h;->c:Lf/x;

    if-eqz v8, :cond_13

    invoke-virtual/range {p0 .. p0}, Lf/h;->size()J

    move-result-wide v11

    sub-long/2addr v11, v2

    cmp-long v11, v11, v2

    const/4 v12, 0x0

    if-gez v11, :cond_b

    invoke-virtual/range {p0 .. p0}, Lf/h;->size()J

    move-result-wide v6

    :goto_2
    cmp-long v11, v6, v2

    if-lez v11, :cond_5

    iget-object v8, v8, Lf/x;->h:Lf/x;

    if-eqz v8, :cond_4

    iget v11, v8, Lf/x;->d:I

    iget v13, v8, Lf/x;->c:I

    sub-int/2addr v11, v13

    int-to-long v13, v11

    sub-long/2addr v6, v13

    goto :goto_2

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v12

    :cond_5
    if-eqz v8, :cond_a

    :goto_3
    cmp-long v11, v6, v4

    if-gez v11, :cond_9

    iget-object v11, v8, Lf/x;->b:[B

    iget v13, v8, Lf/x;->d:I

    int-to-long v13, v13

    iget v15, v8, Lf/x;->c:I

    int-to-long v9, v15

    add-long/2addr v9, v4

    sub-long/2addr v9, v6

    invoke-static {v13, v14, v9, v10}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v9

    long-to-int v9, v9

    iget v10, v8, Lf/x;->c:I

    int-to-long v13, v10

    add-long/2addr v13, v2

    sub-long/2addr v13, v6

    long-to-int v2, v13

    :goto_4
    if-ge v2, v9, :cond_7

    aget-byte v3, v11, v2

    if-ne v3, v1, :cond_6

    :goto_5
    iget v1, v8, Lf/x;->c:I

    sub-int/2addr v2, v1

    int-to-long v1, v2

    add-long/2addr v1, v6

    return-wide v1

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    iget v2, v8, Lf/x;->d:I

    iget v3, v8, Lf/x;->c:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v2, v6

    iget-object v8, v8, Lf/x;->g:Lf/x;

    if-eqz v8, :cond_8

    move-wide v6, v2

    const-wide/16 v9, -0x1

    goto :goto_3

    :cond_8
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v12

    :cond_9
    move-wide v1, v9

    return-wide v1

    :cond_a
    move-wide v1, v9

    return-wide v1

    :cond_b
    :goto_6
    iget v9, v8, Lf/x;->d:I

    iget v10, v8, Lf/x;->c:I

    sub-int/2addr v9, v10

    int-to-long v9, v9

    add-long/2addr v9, v6

    cmp-long v11, v9, v2

    if-lez v11, :cond_11

    if-eqz v8, :cond_10

    :goto_7
    cmp-long v9, v6, v4

    if-gez v9, :cond_f

    iget-object v9, v8, Lf/x;->b:[B

    iget v10, v8, Lf/x;->d:I

    int-to-long v10, v10

    iget v13, v8, Lf/x;->c:I

    int-to-long v13, v13

    add-long/2addr v13, v4

    sub-long/2addr v13, v6

    invoke-static {v10, v11, v13, v14}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    long-to-int v10, v10

    iget v11, v8, Lf/x;->c:I

    int-to-long v13, v11

    add-long/2addr v13, v2

    sub-long/2addr v13, v6

    long-to-int v2, v13

    :goto_8
    if-ge v2, v10, :cond_d

    aget-byte v3, v9, v2

    if-ne v3, v1, :cond_c

    goto :goto_5

    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_d
    iget v2, v8, Lf/x;->d:I

    iget v3, v8, Lf/x;->c:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v2, v6

    iget-object v8, v8, Lf/x;->g:Lf/x;

    if-eqz v8, :cond_e

    move-wide v6, v2

    goto :goto_7

    :cond_e
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v12

    :cond_f
    const-wide/16 v6, -0x1

    return-wide v6

    :cond_10
    const-wide/16 v6, -0x1

    return-wide v6

    :cond_11
    const-wide/16 v6, -0x1

    iget-object v8, v8, Lf/x;->g:Lf/x;

    if-eqz v8, :cond_12

    move-wide v6, v9

    goto :goto_6

    :cond_12
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v12

    :cond_13
    move-wide v6, v9

    return-wide v6

    :cond_14
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "size="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v6, v0, Lf/h;->d:J

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, " fromIndex="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " toIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public a(Lf/C;)J
    .locals 6
    .param p1    # Lf/C;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    :goto_0
    const/16 v2, 0x2000

    int-to-long v2, v2

    invoke-interface {p1, p0, v2, v3}, Lf/C;->b(Lf/h;J)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    return-wide v0

    :cond_0
    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public a(Lf/l;)J
    .locals 2
    .param p1    # Lf/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "bytes"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lf/h;->a(Lf/l;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lf/l;J)J
    .locals 18
    .param p1    # Lf/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v6, p0

    move-wide/from16 v0, p2

    const-string v2, "bytes"

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lf/l;->k()I

    move-result v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-lez v2, :cond_0

    move v2, v5

    goto :goto_0

    :cond_0
    move v2, v4

    :goto_0
    if-eqz v2, :cond_15

    const-wide/16 v7, 0x0

    cmp-long v2, v0, v7

    if-ltz v2, :cond_1

    goto :goto_1

    :cond_1
    move v5, v4

    :goto_1
    if-eqz v5, :cond_14

    iget-object v2, v6, Lf/h;->c:Lf/x;

    if-eqz v2, :cond_13

    invoke-virtual/range {p0 .. p0}, Lf/h;->size()J

    move-result-wide v11

    sub-long/2addr v11, v0

    cmp-long v5, v11, v0

    const-wide/16 v11, 0x1

    const/4 v13, 0x0

    if-gez v5, :cond_a

    invoke-virtual/range {p0 .. p0}, Lf/h;->size()J

    move-result-wide v7

    :goto_2
    cmp-long v5, v7, v0

    if-lez v5, :cond_3

    iget-object v2, v2, Lf/x;->h:Lf/x;

    if-eqz v2, :cond_2

    iget v5, v2, Lf/x;->d:I

    iget v14, v2, Lf/x;->c:I

    sub-int/2addr v5, v14

    int-to-long v14, v5

    sub-long/2addr v7, v14

    goto :goto_2

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v13

    :cond_3
    if-eqz v2, :cond_9

    invoke-virtual/range {p1 .. p1}, Lf/l;->g()[B

    move-result-object v14

    aget-byte v15, v14, v4

    invoke-virtual/range {p1 .. p1}, Lf/l;->k()I

    move-result v5

    iget-wide v3, v6, Lf/h;->d:J

    int-to-long v9, v5

    sub-long/2addr v3, v9

    add-long v9, v3, v11

    move-object v11, v2

    :goto_3
    cmp-long v2, v7, v9

    if-gez v2, :cond_8

    iget-object v12, v11, Lf/x;->b:[B

    iget v2, v11, Lf/x;->d:I

    iget v3, v11, Lf/x;->c:I

    int-to-long v3, v3

    add-long/2addr v3, v9

    sub-long/2addr v3, v7

    move-object/from16 v16, v14

    int-to-long v13, v2

    invoke-static {v13, v14, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v13, v2

    iget v2, v11, Lf/x;->c:I

    int-to-long v2, v2

    add-long/2addr v2, v0

    sub-long/2addr v2, v7

    long-to-int v0, v2

    move v14, v0

    :goto_4
    if-ge v14, v13, :cond_6

    aget-byte v0, v12, v14

    if-ne v0, v15, :cond_4

    add-int/lit8 v2, v14, 0x1

    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object v1, v11

    move-object/from16 v3, v16

    move/from16 v17, v5

    invoke-direct/range {v0 .. v5}, Lf/h;->a(Lf/x;I[BII)Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, v11, Lf/x;->c:I

    sub-int/2addr v14, v0

    int-to-long v0, v14

    :goto_5
    add-long/2addr v0, v7

    return-wide v0

    :cond_4
    move/from16 v17, v5

    :cond_5
    add-int/lit8 v14, v14, 0x1

    move/from16 v5, v17

    goto :goto_4

    :cond_6
    move/from16 v17, v5

    iget v0, v11, Lf/x;->d:I

    iget v1, v11, Lf/x;->c:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    add-long/2addr v0, v7

    iget-object v11, v11, Lf/x;->g:Lf/x;

    if-eqz v11, :cond_7

    move-wide v7, v0

    move-object/from16 v14, v16

    move/from16 v5, v17

    const/4 v13, 0x0

    goto :goto_3

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0

    :cond_8
    const-wide/16 v0, -0x1

    return-wide v0

    :cond_9
    const-wide/16 v0, -0x1

    return-wide v0

    :cond_a
    :goto_6
    iget v5, v2, Lf/x;->d:I

    iget v9, v2, Lf/x;->c:I

    sub-int/2addr v5, v9

    int-to-long v9, v5

    add-long/2addr v9, v7

    cmp-long v5, v9, v0

    if-lez v5, :cond_11

    if-eqz v2, :cond_10

    invoke-virtual/range {p1 .. p1}, Lf/l;->g()[B

    move-result-object v9

    aget-byte v10, v9, v4

    invoke-virtual/range {p1 .. p1}, Lf/l;->k()I

    move-result v13

    iget-wide v3, v6, Lf/h;->d:J

    int-to-long v14, v13

    sub-long/2addr v3, v14

    add-long/2addr v11, v3

    move-object v14, v2

    :goto_7
    cmp-long v2, v7, v11

    if-gez v2, :cond_f

    iget-object v15, v14, Lf/x;->b:[B

    iget v2, v14, Lf/x;->d:I

    iget v3, v14, Lf/x;->c:I

    int-to-long v3, v3

    add-long/2addr v3, v11

    sub-long/2addr v3, v7

    int-to-long v5, v2

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v6, v2

    iget v2, v14, Lf/x;->c:I

    int-to-long v2, v2

    add-long/2addr v2, v0

    sub-long/2addr v2, v7

    long-to-int v0, v2

    move v5, v0

    :goto_8
    if-ge v5, v6, :cond_d

    aget-byte v0, v15, v5

    if-ne v0, v10, :cond_b

    add-int/lit8 v2, v5, 0x1

    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object v1, v14

    move-object v3, v9

    move/from16 v16, v5

    move v5, v13

    invoke-direct/range {v0 .. v5}, Lf/h;->a(Lf/x;I[BII)Z

    move-result v0

    if-eqz v0, :cond_c

    iget v0, v14, Lf/x;->c:I

    sub-int v5, v16, v0

    int-to-long v0, v5

    goto :goto_5

    :cond_b
    move/from16 v16, v5

    :cond_c
    add-int/lit8 v5, v16, 0x1

    goto :goto_8

    :cond_d
    iget v0, v14, Lf/x;->d:I

    iget v1, v14, Lf/x;->c:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    add-long/2addr v0, v7

    iget-object v14, v14, Lf/x;->g:Lf/x;

    if-eqz v14, :cond_e

    move-object/from16 v6, p0

    move-wide v7, v0

    goto :goto_7

    :cond_e
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v5, 0x0

    throw v5

    :cond_f
    const-wide/16 v6, -0x1

    return-wide v6

    :cond_10
    const-wide/16 v6, -0x1

    return-wide v6

    :cond_11
    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    iget-object v2, v2, Lf/x;->g:Lf/x;

    if-eqz v2, :cond_12

    move-object/from16 v6, p0

    move-wide v7, v9

    goto :goto_6

    :cond_12
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_13
    const-wide/16 v6, -0x1

    return-wide v6

    :cond_14
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fromIndex < 0: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_15
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bytes is empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lf/h;JJ)Lf/h;
    .locals 8
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "out"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v1, p0, Lf/h;->d:J

    move-wide v3, p2

    move-wide v5, p4

    invoke-static/range {v1 .. v6}, Lf/c;->a(JJJ)V

    const-wide/16 v0, 0x0

    cmp-long v2, p4, v0

    if-nez v2, :cond_0

    return-object p0

    :cond_0
    iget-wide v2, p1, Lf/h;->d:J

    add-long/2addr v2, p4

    iput-wide v2, p1, Lf/h;->d:J

    iget-object v2, p0, Lf/h;->c:Lf/x;

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_7

    iget v4, v2, Lf/x;->d:I

    iget v5, v2, Lf/x;->c:I

    sub-int v6, v4, v5

    int-to-long v6, v6

    cmp-long v6, p2, v6

    if-ltz v6, :cond_1

    sub-int/2addr v4, v5

    int-to-long v3, v4

    sub-long/2addr p2, v3

    iget-object v2, v2, Lf/x;->g:Lf/x;

    goto :goto_0

    :cond_1
    :goto_1
    cmp-long v4, p4, v0

    if-lez v4, :cond_6

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lf/x;->c()Lf/x;

    move-result-object v4

    iget v5, v4, Lf/x;->c:I

    long-to-int p2, p2

    add-int/2addr v5, p2

    iput v5, v4, Lf/x;->c:I

    iget p2, v4, Lf/x;->c:I

    long-to-int p3, p4

    add-int/2addr p2, p3

    iget p3, v4, Lf/x;->d:I

    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result p2

    iput p2, v4, Lf/x;->d:I

    iget-object p2, p1, Lf/h;->c:Lf/x;

    if-nez p2, :cond_2

    iput-object v4, v4, Lf/x;->h:Lf/x;

    iget-object p2, v4, Lf/x;->h:Lf/x;

    iput-object p2, v4, Lf/x;->g:Lf/x;

    iget-object p2, v4, Lf/x;->g:Lf/x;

    iput-object p2, p1, Lf/h;->c:Lf/x;

    goto :goto_2

    :cond_2
    if-eqz p2, :cond_4

    iget-object p2, p2, Lf/x;->h:Lf/x;

    if-eqz p2, :cond_3

    invoke-virtual {p2, v4}, Lf/x;->a(Lf/x;)Lf/x;

    :goto_2
    iget p2, v4, Lf/x;->d:I

    iget p3, v4, Lf/x;->c:I

    sub-int/2addr p2, p3

    int-to-long p2, p2

    sub-long/2addr p4, p2

    iget-object v2, v2, Lf/x;->g:Lf/x;

    move-wide p2, v0

    goto :goto_1

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v3

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v3

    :cond_5
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v3

    :cond_6
    return-object p0

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v3
.end method

.method public a(Ljava/lang/String;)Lf/h;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "string"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lf/h;->a(Ljava/lang/String;II)Lf/h;

    return-object p0
.end method

.method public a(Ljava/lang/String;II)Lf/h;
    .locals 11
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "string"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ltz p2, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_f

    if-lt p3, p2, :cond_1

    move v2, v1

    goto :goto_1

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_e

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-gt p3, v2, :cond_2

    move v2, v1

    goto :goto_2

    :cond_2
    move v2, v0

    :goto_2
    if-eqz v2, :cond_d

    :goto_3
    if-ge p2, p3, :cond_c

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x80

    if-ge v2, v3, :cond_5

    invoke-virtual {p0, v1}, Lf/h;->b(I)Lf/x;

    move-result-object v4

    iget-object v5, v4, Lf/x;->b:[B

    iget v6, v4, Lf/x;->d:I

    sub-int/2addr v6, p2

    rsub-int v7, v6, 0x2000

    invoke-static {p3, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    add-int/lit8 v8, p2, 0x1

    add-int/2addr p2, v6

    int-to-byte v2, v2

    aput-byte v2, v5, p2

    :goto_4
    if-ge v8, v7, :cond_4

    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result p2

    if-lt p2, v3, :cond_3

    goto :goto_5

    :cond_3
    add-int/lit8 v2, v8, 0x1

    add-int/2addr v8, v6

    int-to-byte p2, p2

    aput-byte p2, v5, v8

    move v8, v2

    goto :goto_4

    :cond_4
    :goto_5
    add-int/2addr v6, v8

    iget p2, v4, Lf/x;->d:I

    sub-int/2addr v6, p2

    add-int/2addr p2, v6

    iput p2, v4, Lf/x;->d:I

    iget-wide v2, p0, Lf/h;->d:J

    int-to-long v4, v6

    add-long/2addr v2, v4

    iput-wide v2, p0, Lf/h;->d:J

    move p2, v8

    goto :goto_3

    :cond_5
    const/16 v4, 0x800

    if-ge v2, v4, :cond_6

    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lf/h;->b(I)Lf/x;

    move-result-object v5

    iget-object v6, v5, Lf/x;->b:[B

    iget v7, v5, Lf/x;->d:I

    shr-int/lit8 v8, v2, 0x6

    or-int/lit16 v8, v8, 0xc0

    int-to-byte v8, v8

    aput-byte v8, v6, v7

    add-int/lit8 v8, v7, 0x1

    and-int/lit8 v2, v2, 0x3f

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v6, v8

    add-int/2addr v7, v4

    iput v7, v5, Lf/x;->d:I

    iget-wide v2, p0, Lf/h;->d:J

    const-wide/16 v4, 0x2

    add-long/2addr v2, v4

    iput-wide v2, p0, Lf/h;->d:J

    :goto_6
    add-int/lit8 p2, p2, 0x1

    goto :goto_3

    :cond_6
    const v4, 0xd800

    const/16 v5, 0x3f

    if-lt v2, v4, :cond_b

    const v4, 0xdfff

    if-le v2, v4, :cond_7

    goto :goto_9

    :cond_7
    add-int/lit8 v6, p2, 0x1

    if-ge v6, p3, :cond_8

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    goto :goto_7

    :cond_8
    move v7, v0

    :goto_7
    const v8, 0xdbff

    if-gt v2, v8, :cond_a

    const v8, 0xdc00

    if-gt v8, v7, :cond_a

    if-ge v4, v7, :cond_9

    goto :goto_8

    :cond_9
    const/high16 v4, 0x10000

    and-int/lit16 v2, v2, 0x3ff

    shl-int/lit8 v2, v2, 0xa

    and-int/lit16 v6, v7, 0x3ff

    or-int/2addr v2, v6

    add-int/2addr v2, v4

    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lf/h;->b(I)Lf/x;

    move-result-object v6

    iget-object v7, v6, Lf/x;->b:[B

    iget v8, v6, Lf/x;->d:I

    shr-int/lit8 v9, v2, 0x12

    or-int/lit16 v9, v9, 0xf0

    int-to-byte v9, v9

    aput-byte v9, v7, v8

    add-int/lit8 v9, v8, 0x1

    shr-int/lit8 v10, v2, 0xc

    and-int/2addr v10, v5

    or-int/2addr v10, v3

    int-to-byte v10, v10

    aput-byte v10, v7, v9

    add-int/lit8 v9, v8, 0x2

    shr-int/lit8 v10, v2, 0x6

    and-int/2addr v10, v5

    or-int/2addr v10, v3

    int-to-byte v10, v10

    aput-byte v10, v7, v9

    add-int/lit8 v9, v8, 0x3

    and-int/2addr v2, v5

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v7, v9

    add-int/2addr v8, v4

    iput v8, v6, Lf/x;->d:I

    iget-wide v2, p0, Lf/h;->d:J

    const-wide/16 v4, 0x4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lf/h;->d:J

    add-int/lit8 p2, p2, 0x2

    goto/16 :goto_3

    :cond_a
    :goto_8
    invoke-virtual {p0, v5}, Lf/h;->writeByte(I)Lf/h;

    move p2, v6

    goto/16 :goto_3

    :cond_b
    :goto_9
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lf/h;->b(I)Lf/x;

    move-result-object v6

    iget-object v7, v6, Lf/x;->b:[B

    iget v8, v6, Lf/x;->d:I

    shr-int/lit8 v9, v2, 0xc

    or-int/lit16 v9, v9, 0xe0

    int-to-byte v9, v9

    aput-byte v9, v7, v8

    add-int/lit8 v9, v8, 0x1

    shr-int/lit8 v10, v2, 0x6

    and-int/2addr v5, v10

    or-int/2addr v5, v3

    int-to-byte v5, v5

    aput-byte v5, v7, v9

    add-int/lit8 v5, v8, 0x2

    and-int/lit8 v2, v2, 0x3f

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v7, v5

    add-int/2addr v8, v4

    iput v8, v6, Lf/x;->d:I

    iget-wide v2, p0, Lf/h;->d:J

    const-wide/16 v4, 0x3

    add-long/2addr v2, v4

    iput-wide v2, p0, Lf/h;->d:J

    goto/16 :goto_6

    :cond_c
    return-object p0

    :cond_d
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "endIndex > string.length: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " > "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_e
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "endIndex < beginIndex: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " < "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_f
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "beginIndex < 0: "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public a(Ljava/lang/String;IILjava/nio/charset/Charset;)Lf/h;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/nio/charset/Charset;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "string"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "charset"

    invoke-static {p4, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ltz p2, :cond_0

    move v2, v0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_7

    if-lt p3, p2, :cond_1

    move v2, v0

    goto :goto_1

    :cond_1
    move v2, v1

    :goto_1
    if-eqz v2, :cond_6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-gt p3, v2, :cond_2

    goto :goto_2

    :cond_2
    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    sget-object v0, Lkotlin/g/c;->a:Ljava/nio/charset/Charset;

    invoke-static {p4, v0}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p1, p2, p3}, Lf/h;->a(Ljava/lang/String;II)Lf/h;

    return-object p0

    :cond_3
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    const-string p2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {p1, p2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_4

    invoke-virtual {p1, p4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    const-string p2, "(this as java.lang.String).getBytes(charset)"

    invoke-static {p1, p2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    array-length p2, p1

    invoke-virtual {p0, p1, v1, p2}, Lf/h;->write([BII)Lf/h;

    return-object p0

    :cond_4
    new-instance p1, Lkotlin/o;

    const-string p2, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, p2}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "endIndex > string.length: "

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " > "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_6
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "endIndex < beginIndex: "

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " < "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "beginIndex < 0: "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lf/j;
    .locals 0

    invoke-virtual {p0, p1}, Lf/h;->a(Ljava/lang/String;)Lf/h;

    return-object p0
.end method

.method public bridge synthetic a(Ljava/lang/String;II)Lf/j;
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Lf/h;->a(Ljava/lang/String;II)Lf/h;

    return-object p0
.end method

.method public final a(I)Lf/l;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    if-nez p1, :cond_0

    sget-object p1, Lf/l;->a:Lf/l;

    goto :goto_0

    :cond_0
    sget-object v0, Lf/z;->f:Lf/z$a;

    invoke-virtual {v0, p0, p1}, Lf/z$a;->a(Lf/h;I)Lf/l;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public a(JLjava/nio/charset/Charset;)Ljava/lang/String;
    .locals 6
    .param p3    # Ljava/nio/charset/Charset;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "charset"

    invoke-static {p3, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const v1, 0x7fffffff

    int-to-long v1, v1

    cmp-long v1, p1, v1

    if-gtz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_6

    iget-wide v1, p0, Lf/h;->d:J

    cmp-long v1, v1, p1

    if-ltz v1, :cond_5

    if-nez v0, :cond_1

    const-string p1, ""

    return-object p1

    :cond_1
    iget-object v0, p0, Lf/h;->c:Lf/x;

    if-eqz v0, :cond_4

    iget v1, v0, Lf/x;->c:I

    int-to-long v2, v1

    add-long/2addr v2, p1

    iget v4, v0, Lf/x;->d:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    invoke-virtual {p0, p1, p2}, Lf/h;->e(J)[B

    move-result-object p1

    new-instance p2, Ljava/lang/String;

    invoke-direct {p2, p1, p3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object p2

    :cond_2
    iget-object v2, v0, Lf/x;->b:[B

    long-to-int v3, p1

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2, v1, v3, p3}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iget p3, v0, Lf/x;->c:I

    add-int/2addr p3, v3

    iput p3, v0, Lf/x;->c:I

    iget-wide v1, p0, Lf/h;->d:J

    sub-long/2addr v1, p1

    iput-wide v1, p0, Lf/h;->d:J

    iget p1, v0, Lf/x;->c:I

    iget p2, v0, Lf/x;->d:I

    if-ne p1, p2, :cond_3

    invoke-virtual {v0}, Lf/x;->b()Lf/x;

    move-result-object p1

    iput-object p1, p0, Lf/h;->c:Lf/x;

    invoke-static {v0}, Lf/y;->a(Lf/x;)V

    :cond_3
    return-object v4

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1

    :cond_5
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :cond_6
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "byteCount: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public a(Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/nio/charset/Charset;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "charset"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v0, p0, Lf/h;->d:J

    invoke-virtual {p0, v0, v1, p1}, Lf/h;->a(JLjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final a()V
    .locals 2

    iget-wide v0, p0, Lf/h;->d:J

    invoke-virtual {p0, v0, v1}, Lf/h;->skip(J)V

    return-void
.end method

.method public a(Lf/h;J)V
    .locals 8
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eq p1, p0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    if-eqz v1, :cond_f

    iget-wide v2, p1, Lf/h;->d:J

    const-wide/16 v4, 0x0

    move-wide v6, p2

    invoke-static/range {v2 .. v7}, Lf/c;->a(JJJ)V

    :goto_1
    const-wide/16 v1, 0x0

    cmp-long v1, p2, v1

    if-lez v1, :cond_e

    iget-object v1, p1, Lf/h;->c:Lf/x;

    const/4 v2, 0x0

    if-eqz v1, :cond_d

    iget v3, v1, Lf/x;->d:I

    if-eqz v1, :cond_c

    iget v1, v1, Lf/x;->c:I

    sub-int/2addr v3, v1

    int-to-long v3, v3

    cmp-long v1, p2, v3

    if-gez v1, :cond_7

    iget-object v1, p0, Lf/h;->c:Lf/x;

    if-eqz v1, :cond_2

    if-eqz v1, :cond_1

    iget-object v1, v1, Lf/x;->h:Lf/x;

    goto :goto_2

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_2
    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_5

    iget-boolean v3, v1, Lf/x;->f:Z

    if-eqz v3, :cond_5

    iget v3, v1, Lf/x;->d:I

    int-to-long v3, v3

    add-long/2addr v3, p2

    iget-boolean v5, v1, Lf/x;->e:Z

    if-eqz v5, :cond_3

    move v5, v0

    goto :goto_3

    :cond_3
    iget v5, v1, Lf/x;->c:I

    :goto_3
    int-to-long v5, v5

    sub-long/2addr v3, v5

    const/16 v5, 0x2000

    int-to-long v5, v5

    cmp-long v3, v3, v5

    if-gtz v3, :cond_5

    iget-object v0, p1, Lf/h;->c:Lf/x;

    if-eqz v0, :cond_4

    long-to-int v2, p2

    invoke-virtual {v0, v1, v2}, Lf/x;->a(Lf/x;I)V

    iget-wide v0, p1, Lf/h;->d:J

    sub-long/2addr v0, p2

    iput-wide v0, p1, Lf/h;->d:J

    iget-wide v0, p0, Lf/h;->d:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lf/h;->d:J

    return-void

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_5
    iget-object v1, p1, Lf/h;->c:Lf/x;

    if-eqz v1, :cond_6

    long-to-int v3, p2

    invoke-virtual {v1, v3}, Lf/x;->a(I)Lf/x;

    move-result-object v1

    iput-object v1, p1, Lf/h;->c:Lf/x;

    goto :goto_4

    :cond_6
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_7
    :goto_4
    iget-object v1, p1, Lf/h;->c:Lf/x;

    if-eqz v1, :cond_b

    iget v3, v1, Lf/x;->d:I

    iget v4, v1, Lf/x;->c:I

    sub-int/2addr v3, v4

    int-to-long v3, v3

    invoke-virtual {v1}, Lf/x;->b()Lf/x;

    move-result-object v5

    iput-object v5, p1, Lf/h;->c:Lf/x;

    iget-object v5, p0, Lf/h;->c:Lf/x;

    if-nez v5, :cond_8

    iput-object v1, p0, Lf/h;->c:Lf/x;

    iput-object v1, v1, Lf/x;->h:Lf/x;

    iget-object v2, v1, Lf/x;->h:Lf/x;

    iput-object v2, v1, Lf/x;->g:Lf/x;

    goto :goto_5

    :cond_8
    if-eqz v5, :cond_a

    iget-object v5, v5, Lf/x;->h:Lf/x;

    if-eqz v5, :cond_9

    invoke-virtual {v5, v1}, Lf/x;->a(Lf/x;)Lf/x;

    invoke-virtual {v1}, Lf/x;->a()V

    :goto_5
    iget-wide v1, p1, Lf/h;->d:J

    sub-long/2addr v1, v3

    iput-wide v1, p1, Lf/h;->d:J

    iget-wide v1, p0, Lf/h;->d:J

    add-long/2addr v1, v3

    iput-wide v1, p0, Lf/h;->d:J

    sub-long/2addr p2, v3

    goto/16 :goto_1

    :cond_9
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_a
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_b
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_c
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_d
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_e
    return-void

    :cond_f
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "source == this"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a([B)V
    .locals 3
    .param p1    # [B
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    array-length v1, p1

    sub-int/2addr v1, v0

    invoke-virtual {p0, p1, v0, v1}, Lf/h;->a([BII)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    add-int/2addr v0, v1

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :cond_1
    return-void
.end method

.method public a(J)Z
    .locals 2

    iget-wide v0, p0, Lf/h;->d:J

    cmp-long p1, v0, p1

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final b()J
    .locals 5

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-wide v2

    :cond_0
    iget-object v2, p0, Lf/h;->c:Lf/x;

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    iget-object v2, v2, Lf/x;->h:Lf/x;

    if-eqz v2, :cond_2

    iget v3, v2, Lf/x;->d:I

    const/16 v4, 0x2000

    if-ge v3, v4, :cond_1

    iget-boolean v4, v2, Lf/x;->f:Z

    if-eqz v4, :cond_1

    iget v2, v2, Lf/x;->c:I

    sub-int/2addr v3, v2

    int-to-long v2, v3

    sub-long/2addr v0, v2

    :cond_1
    return-wide v0

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v3

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v3
.end method

.method public b(Lf/h;J)J
    .locals 4
    .param p1    # Lf/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-ltz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_3

    iget-wide v2, p0, Lf/h;->d:J

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    const-wide/16 p1, -0x1

    return-wide p1

    :cond_1
    cmp-long v0, p2, v2

    if-lez v0, :cond_2

    move-wide p2, v2

    :cond_2
    invoke-virtual {p1, p0, p2, p3}, Lf/h;->a(Lf/h;J)V

    return-wide p2

    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "byteCount < 0: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public b(Lf/l;)J
    .locals 2
    .param p1    # Lf/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "targetBytes"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lf/h;->b(Lf/l;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(Lf/l;J)J
    .locals 12
    .param p1    # Lf/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "targetBytes"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ltz v2, :cond_0

    move v2, v4

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    if-eqz v2, :cond_1a

    iget-object v2, p0, Lf/h;->c:Lf/x;

    const-wide/16 v5, -0x1

    if-eqz v2, :cond_19

    invoke-virtual {p0}, Lf/h;->size()J

    move-result-wide v7

    sub-long/2addr v7, p2

    cmp-long v7, v7, p2

    const/4 v8, 0x2

    const/4 v9, 0x0

    if-gez v7, :cond_d

    invoke-virtual {p0}, Lf/h;->size()J

    move-result-wide v0

    :goto_1
    cmp-long v7, v0, p2

    if-lez v7, :cond_2

    iget-object v2, v2, Lf/x;->h:Lf/x;

    if-eqz v2, :cond_1

    iget v7, v2, Lf/x;->d:I

    iget v10, v2, Lf/x;->c:I

    sub-int/2addr v7, v10

    int-to-long v10, v7

    sub-long/2addr v0, v10

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v9

    :cond_2
    if-eqz v2, :cond_c

    invoke-virtual {p1}, Lf/l;->k()I

    move-result v7

    if-ne v7, v8, :cond_7

    invoke-virtual {p1, v3}, Lf/l;->a(I)B

    move-result v3

    invoke-virtual {p1, v4}, Lf/l;->a(I)B

    move-result p1

    :goto_2
    iget-wide v7, p0, Lf/h;->d:J

    cmp-long v4, v0, v7

    if-gez v4, :cond_c

    iget-object v4, v2, Lf/x;->b:[B

    iget v7, v2, Lf/x;->c:I

    int-to-long v7, v7

    add-long/2addr v7, p2

    sub-long/2addr v7, v0

    long-to-int p2, v7

    iget p3, v2, Lf/x;->d:I

    :goto_3
    if-ge p2, p3, :cond_5

    aget-byte v7, v4, p2

    if-eq v7, v3, :cond_4

    if-ne v7, p1, :cond_3

    goto :goto_4

    :cond_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_3

    :cond_4
    :goto_4
    iget p1, v2, Lf/x;->c:I

    :goto_5
    sub-int/2addr p2, p1

    int-to-long p1, p2

    add-long/2addr p1, v0

    return-wide p1

    :cond_5
    iget p2, v2, Lf/x;->d:I

    iget p3, v2, Lf/x;->c:I

    sub-int/2addr p2, p3

    int-to-long p2, p2

    add-long/2addr p2, v0

    iget-object v2, v2, Lf/x;->g:Lf/x;

    if-eqz v2, :cond_6

    move-wide v0, p2

    goto :goto_2

    :cond_6
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v9

    :cond_7
    invoke-virtual {p1}, Lf/l;->g()[B

    move-result-object p1

    :goto_6
    iget-wide v7, p0, Lf/h;->d:J

    cmp-long v4, v0, v7

    if-gez v4, :cond_c

    iget-object v4, v2, Lf/x;->b:[B

    iget v7, v2, Lf/x;->c:I

    int-to-long v7, v7

    add-long/2addr v7, p2

    sub-long/2addr v7, v0

    long-to-int p2, v7

    iget p3, v2, Lf/x;->d:I

    :goto_7
    if-ge p2, p3, :cond_a

    aget-byte v7, v4, p2

    array-length v8, p1

    move v10, v3

    :goto_8
    if-ge v10, v8, :cond_9

    aget-byte v11, p1, v10

    if-ne v7, v11, :cond_8

    :goto_9
    iget p1, v2, Lf/x;->c:I

    goto :goto_5

    :cond_8
    add-int/lit8 v10, v10, 0x1

    goto :goto_8

    :cond_9
    add-int/lit8 p2, p2, 0x1

    goto :goto_7

    :cond_a
    iget p2, v2, Lf/x;->d:I

    iget p3, v2, Lf/x;->c:I

    sub-int/2addr p2, p3

    int-to-long p2, p2

    add-long/2addr p2, v0

    iget-object v2, v2, Lf/x;->g:Lf/x;

    if-eqz v2, :cond_b

    move-wide v0, p2

    goto :goto_6

    :cond_b
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v9

    :cond_c
    return-wide v5

    :cond_d
    :goto_a
    iget v7, v2, Lf/x;->d:I

    iget v10, v2, Lf/x;->c:I

    sub-int/2addr v7, v10

    int-to-long v10, v7

    add-long/2addr v10, v0

    cmp-long v7, v10, p2

    if-lez v7, :cond_17

    if-eqz v2, :cond_16

    invoke-virtual {p1}, Lf/l;->k()I

    move-result v7

    if-ne v7, v8, :cond_11

    invoke-virtual {p1, v3}, Lf/l;->a(I)B

    move-result v3

    invoke-virtual {p1, v4}, Lf/l;->a(I)B

    move-result p1

    :goto_b
    iget-wide v7, p0, Lf/h;->d:J

    cmp-long v4, v0, v7

    if-gez v4, :cond_16

    iget-object v4, v2, Lf/x;->b:[B

    iget v7, v2, Lf/x;->c:I

    int-to-long v7, v7

    add-long/2addr v7, p2

    sub-long/2addr v7, v0

    long-to-int p2, v7

    iget p3, v2, Lf/x;->d:I

    :goto_c
    if-ge p2, p3, :cond_f

    aget-byte v7, v4, p2

    if-eq v7, v3, :cond_4

    if-ne v7, p1, :cond_e

    goto/16 :goto_4

    :cond_e
    add-int/lit8 p2, p2, 0x1

    goto :goto_c

    :cond_f
    iget p2, v2, Lf/x;->d:I

    iget p3, v2, Lf/x;->c:I

    sub-int/2addr p2, p3

    int-to-long p2, p2

    add-long/2addr p2, v0

    iget-object v2, v2, Lf/x;->g:Lf/x;

    if-eqz v2, :cond_10

    move-wide v0, p2

    goto :goto_b

    :cond_10
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v9

    :cond_11
    invoke-virtual {p1}, Lf/l;->g()[B

    move-result-object p1

    :goto_d
    iget-wide v7, p0, Lf/h;->d:J

    cmp-long v4, v0, v7

    if-gez v4, :cond_16

    iget-object v4, v2, Lf/x;->b:[B

    iget v7, v2, Lf/x;->c:I

    int-to-long v7, v7

    add-long/2addr v7, p2

    sub-long/2addr v7, v0

    long-to-int p2, v7

    iget p3, v2, Lf/x;->d:I

    :goto_e
    if-ge p2, p3, :cond_14

    aget-byte v7, v4, p2

    array-length v8, p1

    move v10, v3

    :goto_f
    if-ge v10, v8, :cond_13

    aget-byte v11, p1, v10

    if-ne v7, v11, :cond_12

    goto/16 :goto_9

    :cond_12
    add-int/lit8 v10, v10, 0x1

    goto :goto_f

    :cond_13
    add-int/lit8 p2, p2, 0x1

    goto :goto_e

    :cond_14
    iget p2, v2, Lf/x;->d:I

    iget p3, v2, Lf/x;->c:I

    sub-int/2addr p2, p3

    int-to-long p2, p2

    add-long/2addr p2, v0

    iget-object v2, v2, Lf/x;->g:Lf/x;

    if-eqz v2, :cond_15

    move-wide v0, p2

    goto :goto_d

    :cond_15
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v9

    :cond_16
    return-wide v5

    :cond_17
    iget-object v2, v2, Lf/x;->g:Lf/x;

    if-eqz v2, :cond_18

    move-wide v0, v10

    goto/16 :goto_a

    :cond_18
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v9

    :cond_19
    return-wide v5

    :cond_1a
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "fromIndex < 0: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public b(J)Lf/l;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lf/l;

    invoke-virtual {p0, p1, p2}, Lf/h;->e(J)[B

    move-result-object p1

    invoke-direct {v0, p1}, Lf/l;-><init>([B)V

    return-object v0
.end method

.method public final b(I)Lf/x;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/16 v0, 0x2000

    const/4 v1, 0x1

    if-lt p1, v1, :cond_0

    if-gt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_6

    iget-object v1, p0, Lf/h;->c:Lf/x;

    if-nez v1, :cond_1

    invoke-static {}, Lf/y;->a()Lf/x;

    move-result-object p1

    iput-object p1, p0, Lf/h;->c:Lf/x;

    iput-object p1, p1, Lf/x;->h:Lf/x;

    iput-object p1, p1, Lf/x;->g:Lf/x;

    return-object p1

    :cond_1
    const/4 v2, 0x0

    if-eqz v1, :cond_5

    iget-object v1, v1, Lf/x;->h:Lf/x;

    if-eqz v1, :cond_4

    iget v2, v1, Lf/x;->d:I

    add-int/2addr v2, p1

    if-gt v2, v0, :cond_3

    iget-boolean p1, v1, Lf/x;->f:Z

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    move-object p1, v1

    goto :goto_2

    :cond_3
    :goto_1
    invoke-static {}, Lf/y;->a()Lf/x;

    move-result-object p1

    invoke-virtual {v1, p1}, Lf/x;->a(Lf/x;)Lf/x;

    :goto_2
    return-object p1

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_5
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "unexpected capacity"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c()Lf/E;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    sget-object v0, Lf/E;->a:Lf/E;

    return-object v0
.end method

.method public c(I)Lf/h;
    .locals 8
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/16 v0, 0x80

    if-ge p1, v0, :cond_0

    invoke-virtual {p0, p1}, Lf/h;->writeByte(I)Lf/h;

    goto/16 :goto_1

    :cond_0
    const/16 v1, 0x800

    const/16 v2, 0x3f

    if-ge p1, v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lf/h;->b(I)Lf/x;

    move-result-object v3

    iget-object v4, v3, Lf/x;->b:[B

    iget v5, v3, Lf/x;->d:I

    shr-int/lit8 v6, p1, 0x6

    or-int/lit16 v6, v6, 0xc0

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    add-int/lit8 v6, v5, 0x1

    and-int/2addr p1, v2

    or-int/2addr p1, v0

    int-to-byte p1, p1

    aput-byte p1, v4, v6

    add-int/2addr v5, v1

    iput v5, v3, Lf/x;->d:I

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lf/h;->d:J

    goto/16 :goto_1

    :cond_1
    const v1, 0xdfff

    const v3, 0xd800

    if-le v3, p1, :cond_2

    goto :goto_0

    :cond_2
    if-lt v1, p1, :cond_3

    invoke-virtual {p0, v2}, Lf/h;->writeByte(I)Lf/h;

    goto :goto_1

    :cond_3
    :goto_0
    const/high16 v1, 0x10000

    if-ge p1, v1, :cond_4

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lf/h;->b(I)Lf/x;

    move-result-object v3

    iget-object v4, v3, Lf/x;->b:[B

    iget v5, v3, Lf/x;->d:I

    shr-int/lit8 v6, p1, 0xc

    or-int/lit16 v6, v6, 0xe0

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    add-int/lit8 v6, v5, 0x1

    shr-int/lit8 v7, p1, 0x6

    and-int/2addr v7, v2

    or-int/2addr v7, v0

    int-to-byte v7, v7

    aput-byte v7, v4, v6

    add-int/lit8 v6, v5, 0x2

    and-int/2addr p1, v2

    or-int/2addr p1, v0

    int-to-byte p1, p1

    aput-byte p1, v4, v6

    add-int/2addr v5, v1

    iput v5, v3, Lf/x;->d:I

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lf/h;->d:J

    goto :goto_1

    :cond_4
    const v1, 0x10ffff

    if-gt p1, v1, :cond_5

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lf/h;->b(I)Lf/x;

    move-result-object v3

    iget-object v4, v3, Lf/x;->b:[B

    iget v5, v3, Lf/x;->d:I

    shr-int/lit8 v6, p1, 0x12

    or-int/lit16 v6, v6, 0xf0

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    add-int/lit8 v6, v5, 0x1

    shr-int/lit8 v7, p1, 0xc

    and-int/2addr v7, v2

    or-int/2addr v7, v0

    int-to-byte v7, v7

    aput-byte v7, v4, v6

    add-int/lit8 v6, v5, 0x2

    shr-int/lit8 v7, p1, 0x6

    and-int/2addr v7, v2

    or-int/2addr v7, v0

    int-to-byte v7, v7

    aput-byte v7, v4, v6

    add-int/lit8 v6, v5, 0x3

    and-int/2addr p1, v2

    or-int/2addr p1, v0

    int-to-byte p1, p1

    aput-byte p1, v4, v6

    add-int/2addr v5, v1

    iput v5, v3, Lf/x;->d:I

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lf/h;->d:J

    :goto_1
    return-object p0

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected code point: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c(Lf/l;)Lf/h;
    .locals 1
    .param p1    # Lf/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "byteString"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p0}, Lf/l;->a(Lf/h;)V

    return-object p0
.end method

.method public bridge synthetic c(Lf/l;)Lf/j;
    .locals 0

    invoke-virtual {p0, p1}, Lf/h;->c(Lf/l;)Lf/h;

    return-object p0
.end method

.method public c(J)Ljava/lang/String;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_4

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v2, p1, v0

    const-wide/16 v3, 0x1

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    add-long v0, p1, v3

    :goto_1
    const/16 v2, 0xa

    int-to-byte v2, v2

    const-wide/16 v7, 0x0

    move-object v5, p0

    move v6, v2

    move-wide v9, v0

    invoke-virtual/range {v5 .. v10}, Lf/h;->a(BJJ)J

    move-result-wide v5

    const-wide/16 v7, -0x1

    cmp-long v7, v5, v7

    if-eqz v7, :cond_2

    invoke-virtual {p0, v5, v6}, Lf/h;->j(J)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    iget-wide v5, p0, Lf/h;->d:J

    cmp-long v5, v0, v5

    if-gez v5, :cond_3

    sub-long v3, v0, v3

    invoke-virtual {p0, v3, v4}, Lf/h;->h(J)B

    move-result v3

    const/16 v4, 0xd

    int-to-byte v4, v4

    if-ne v3, v4, :cond_3

    invoke-virtual {p0, v0, v1}, Lf/h;->h(J)B

    move-result v3

    if-ne v3, v2, :cond_3

    invoke-virtual {p0, v0, v1}, Lf/h;->j(J)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    new-instance v6, Lf/h;

    invoke-direct {v6}, Lf/h;-><init>()V

    const-wide/16 v2, 0x0

    const/16 v0, 0x20

    iget-wide v4, p0, Lf/h;->d:J

    int-to-long v0, v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    move-object v0, p0

    move-object v1, v6

    invoke-virtual/range {v0 .. v5}, Lf/h;->a(Lf/h;JJ)Lf/h;

    new-instance v0, Ljava/io/EOFException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\\n not found: limit="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lf/h;->d:J

    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " content="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lf/h;->k()Lf/l;

    move-result-object p1

    invoke-virtual {p1}, Lf/l;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x2026

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "limit < 0: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public clone()Lf/h;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lf/h;

    invoke-direct {v0}, Lf/h;-><init>()V

    iget-wide v1, p0, Lf/h;->d:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    iget-object v1, p0, Lf/h;->c:Lf/x;

    const/4 v2, 0x0

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lf/x;->c()Lf/x;

    move-result-object v1

    iput-object v1, v0, Lf/h;->c:Lf/x;

    iget-object v1, v0, Lf/h;->c:Lf/x;

    if-eqz v1, :cond_8

    iput-object v1, v1, Lf/x;->h:Lf/x;

    if-eqz v1, :cond_7

    if-eqz v1, :cond_6

    iget-object v3, v1, Lf/x;->h:Lf/x;

    iput-object v3, v1, Lf/x;->g:Lf/x;

    iget-object v1, p0, Lf/h;->c:Lf/x;

    if-eqz v1, :cond_5

    iget-object v1, v1, Lf/x;->g:Lf/x;

    :goto_0
    iget-object v3, p0, Lf/h;->c:Lf/x;

    if-eq v1, v3, :cond_4

    iget-object v3, v0, Lf/h;->c:Lf/x;

    if-eqz v3, :cond_3

    iget-object v3, v3, Lf/x;->h:Lf/x;

    if-eqz v3, :cond_2

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lf/x;->c()Lf/x;

    move-result-object v4

    invoke-virtual {v3, v4}, Lf/x;->a(Lf/x;)Lf/x;

    iget-object v1, v1, Lf/x;->g:Lf/x;

    goto :goto_0

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_4
    iget-wide v1, p0, Lf/h;->d:J

    iput-wide v1, v0, Lf/h;->d:J

    return-object v0

    :cond_5
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_6
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_8
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_9
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lf/h;->clone()Lf/h;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public d(J)Lf/h;
    .locals 9
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/16 p1, 0x30

    invoke-virtual {p0, p1}, Lf/h;->writeByte(I)Lf/h;

    return-object p0

    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->highestOneBit(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v0

    const/4 v1, 0x4

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lf/h;->b(I)Lf/x;

    move-result-object v2

    iget-object v3, v2, Lf/x;->b:[B

    iget v4, v2, Lf/x;->d:I

    add-int v5, v4, v0

    add-int/lit8 v5, v5, -0x1

    :goto_0
    if-lt v5, v4, :cond_1

    sget-object v6, Lf/h;->a:[B

    const-wide/16 v7, 0xf

    and-long/2addr v7, p1

    long-to-int v7, v7

    aget-byte v6, v6, v7

    aput-byte v6, v3, v5

    ushr-long/2addr p1, v1

    add-int/lit8 v5, v5, -0x1

    goto :goto_0

    :cond_1
    iget p1, v2, Lf/x;->d:I

    add-int/2addr p1, v0

    iput p1, v2, Lf/x;->d:I

    iget-wide p1, p0, Lf/h;->d:J

    int-to-long v0, v0

    add-long/2addr p1, v0

    iput-wide p1, p0, Lf/h;->d:J

    return-object p0
.end method

.method public bridge synthetic d(J)Lf/j;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lf/h;->d(J)Lf/h;

    return-object p0
.end method

.method public d()Z
    .locals 4

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public e()Lf/h;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    return-object p0
.end method

.method public bridge synthetic e()Lf/j;
    .locals 0

    invoke-virtual {p0}, Lf/h;->e()Lf/h;

    return-object p0
.end method

.method public e(J)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const v0, 0x7fffffff

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    iget-wide v0, p0, Lf/h;->d:J

    cmp-long v0, v0, p1

    if-ltz v0, :cond_1

    long-to-int p1, p1

    new-array p1, p1, [B

    invoke-virtual {p0, p1}, Lf/h;->a([B)V

    return-object p1

    :cond_1
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "byteCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 18
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    return v2

    :cond_0
    instance-of v3, v1, Lf/h;

    const/4 v4, 0x0

    if-nez v3, :cond_1

    return v4

    :cond_1
    iget-wide v5, v0, Lf/h;->d:J

    check-cast v1, Lf/h;

    iget-wide v7, v1, Lf/h;->d:J

    cmp-long v3, v5, v7

    if-eqz v3, :cond_2

    return v4

    :cond_2
    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-nez v3, :cond_3

    return v2

    :cond_3
    iget-object v3, v0, Lf/h;->c:Lf/x;

    const/4 v5, 0x0

    if-eqz v3, :cond_c

    iget-object v1, v1, Lf/h;->c:Lf/x;

    if-eqz v1, :cond_b

    iget v6, v3, Lf/x;->c:I

    iget v9, v1, Lf/x;->c:I

    move v11, v9

    move-wide v9, v7

    :goto_0
    iget-wide v12, v0, Lf/h;->d:J

    cmp-long v12, v9, v12

    if-gez v12, :cond_a

    iget v12, v3, Lf/x;->d:I

    sub-int/2addr v12, v6

    iget v13, v1, Lf/x;->d:I

    sub-int/2addr v13, v11

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v12

    int-to-long v12, v12

    move-wide v14, v7

    :goto_1
    cmp-long v16, v14, v12

    if-gez v16, :cond_5

    iget-object v7, v3, Lf/x;->b:[B

    add-int/lit8 v8, v6, 0x1

    aget-byte v6, v7, v6

    iget-object v7, v1, Lf/x;->b:[B

    add-int/lit8 v17, v11, 0x1

    aget-byte v7, v7, v11

    if-eq v6, v7, :cond_4

    return v4

    :cond_4
    const-wide/16 v6, 0x1

    add-long/2addr v14, v6

    move v6, v8

    move/from16 v11, v17

    const-wide/16 v7, 0x0

    goto :goto_1

    :cond_5
    iget v7, v3, Lf/x;->d:I

    if-ne v6, v7, :cond_7

    iget-object v3, v3, Lf/x;->g:Lf/x;

    if-eqz v3, :cond_6

    iget v6, v3, Lf/x;->c:I

    goto :goto_2

    :cond_6
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_7
    :goto_2
    iget v7, v1, Lf/x;->d:I

    if-ne v11, v7, :cond_9

    iget-object v1, v1, Lf/x;->g:Lf/x;

    if-eqz v1, :cond_8

    iget v7, v1, Lf/x;->c:I

    move v11, v7

    goto :goto_3

    :cond_8
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_9
    :goto_3
    add-long/2addr v9, v12

    const-wide/16 v7, 0x0

    goto :goto_0

    :cond_a
    return v2

    :cond_b
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5

    :cond_c
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v5
.end method

.method public f()J
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-wide v1, v0, Lf/h;->d:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_c

    const-wide/16 v5, -0x7

    const/4 v7, 0x0

    move-wide v8, v5

    move v5, v7

    move v6, v5

    :cond_0
    iget-object v10, v0, Lf/h;->c:Lf/x;

    if-eqz v10, :cond_b

    iget-object v11, v10, Lf/x;->b:[B

    iget v12, v10, Lf/x;->c:I

    iget v13, v10, Lf/x;->d:I

    :goto_0
    if-ge v12, v13, :cond_7

    aget-byte v15, v11, v12

    const/16 v14, 0x30

    int-to-byte v14, v14

    if-lt v15, v14, :cond_4

    const/16 v1, 0x39

    int-to-byte v1, v1

    if-gt v15, v1, :cond_4

    sub-int/2addr v14, v15

    const-wide v1, -0xcccccccccccccccL

    cmp-long v16, v3, v1

    if-ltz v16, :cond_2

    if-nez v16, :cond_1

    int-to-long v1, v14

    cmp-long v1, v1, v8

    if-gez v1, :cond_1

    goto :goto_1

    :cond_1
    const-wide/16 v1, 0xa

    mul-long/2addr v3, v1

    int-to-long v1, v14

    add-long/2addr v3, v1

    goto :goto_2

    :cond_2
    :goto_1
    new-instance v1, Lf/h;

    invoke-direct {v1}, Lf/h;-><init>()V

    invoke-virtual {v1, v3, v4}, Lf/h;->g(J)Lf/h;

    invoke-virtual {v1, v15}, Lf/h;->writeByte(I)Lf/h;

    if-nez v5, :cond_3

    invoke-virtual {v1}, Lf/h;->readByte()B

    :cond_3
    new-instance v2, Ljava/lang/NumberFormatException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Number too large: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lf/h;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    const/16 v1, 0x2d

    int-to-byte v1, v1

    if-ne v15, v1, :cond_5

    if-nez v7, :cond_5

    const-wide/16 v1, 0x1

    sub-long/2addr v8, v1

    const/4 v5, 0x1

    :goto_2
    add-int/lit8 v12, v12, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_5
    if-eqz v7, :cond_6

    const/4 v6, 0x1

    goto :goto_3

    :cond_6
    new-instance v1, Ljava/lang/NumberFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected leading [0-9] or \'-\' character but was 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v15}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    :goto_3
    if-ne v12, v13, :cond_8

    invoke-virtual {v10}, Lf/x;->b()Lf/x;

    move-result-object v1

    iput-object v1, v0, Lf/h;->c:Lf/x;

    invoke-static {v10}, Lf/y;->a(Lf/x;)V

    goto :goto_4

    :cond_8
    iput v12, v10, Lf/x;->c:I

    :goto_4
    if-nez v6, :cond_9

    iget-object v1, v0, Lf/h;->c:Lf/x;

    if-nez v1, :cond_0

    :cond_9
    iget-wide v1, v0, Lf/h;->d:J

    int-to-long v6, v7

    sub-long/2addr v1, v6

    iput-wide v1, v0, Lf/h;->d:J

    if-eqz v5, :cond_a

    goto :goto_5

    :cond_a
    neg-long v3, v3

    :goto_5
    return-wide v3

    :cond_b
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v1, 0x0

    throw v1

    :cond_c
    new-instance v1, Ljava/io/EOFException;

    invoke-direct {v1}, Ljava/io/EOFException;-><init>()V

    throw v1
.end method

.method public f(J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    iget-wide v0, p0, Lf/h;->d:J

    cmp-long p1, v0, p1

    if-ltz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1
.end method

.method public flush()V
    .locals 0

    return-void
.end method

.method public g(J)Lf/h;
    .locals 12
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    const/16 p1, 0x30

    invoke-virtual {p0, p1}, Lf/h;->writeByte(I)Lf/h;

    return-object p0

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x1

    if-gez v2, :cond_2

    neg-long p1, p1

    cmp-long v2, p1, v0

    if-gez v2, :cond_1

    const-string p1, "-9223372036854775808"

    invoke-virtual {p0, p1}, Lf/h;->a(Ljava/lang/String;)Lf/h;

    return-object p0

    :cond_1
    move v3, v4

    :cond_2
    const-wide/32 v5, 0x5f5e100

    cmp-long v2, p1, v5

    const/16 v5, 0xa

    if-gez v2, :cond_a

    const-wide/16 v6, 0x2710

    cmp-long v2, p1, v6

    if-gez v2, :cond_6

    const-wide/16 v6, 0x64

    cmp-long v2, p1, v6

    if-gez v2, :cond_4

    const-wide/16 v6, 0xa

    cmp-long v2, p1, v6

    if-gez v2, :cond_3

    goto/16 :goto_1

    :cond_3
    const/4 v4, 0x2

    goto/16 :goto_1

    :cond_4
    const-wide/16 v6, 0x3e8

    cmp-long v2, p1, v6

    if-gez v2, :cond_5

    const/4 v2, 0x3

    goto :goto_0

    :cond_5
    const/4 v2, 0x4

    :goto_0
    move v4, v2

    goto/16 :goto_1

    :cond_6
    const-wide/32 v6, 0xf4240

    cmp-long v2, p1, v6

    if-gez v2, :cond_8

    const-wide/32 v6, 0x186a0

    cmp-long v2, p1, v6

    if-gez v2, :cond_7

    const/4 v2, 0x5

    goto :goto_0

    :cond_7
    const/4 v2, 0x6

    goto :goto_0

    :cond_8
    const-wide/32 v6, 0x989680

    cmp-long v2, p1, v6

    if-gez v2, :cond_9

    const/4 v2, 0x7

    goto :goto_0

    :cond_9
    const/16 v2, 0x8

    goto :goto_0

    :cond_a
    const-wide v6, 0xe8d4a51000L

    cmp-long v2, p1, v6

    if-gez v2, :cond_e

    const-wide v6, 0x2540be400L

    cmp-long v2, p1, v6

    if-gez v2, :cond_c

    const-wide/32 v6, 0x3b9aca00

    cmp-long v2, p1, v6

    if-gez v2, :cond_b

    const/16 v4, 0x9

    goto :goto_1

    :cond_b
    move v4, v5

    goto :goto_1

    :cond_c
    const-wide v6, 0x174876e800L

    cmp-long v2, p1, v6

    if-gez v2, :cond_d

    const/16 v2, 0xb

    goto :goto_0

    :cond_d
    const/16 v2, 0xc

    goto :goto_0

    :cond_e
    const-wide v6, 0x38d7ea4c68000L

    cmp-long v2, p1, v6

    if-gez v2, :cond_11

    const-wide v6, 0x9184e72a000L

    cmp-long v2, p1, v6

    if-gez v2, :cond_f

    const/16 v4, 0xd

    goto :goto_1

    :cond_f
    const-wide v6, 0x5af3107a4000L

    cmp-long v2, p1, v6

    if-gez v2, :cond_10

    const/16 v2, 0xe

    goto :goto_0

    :cond_10
    const/16 v2, 0xf

    goto :goto_0

    :cond_11
    const-wide v6, 0x16345785d8a0000L

    cmp-long v2, p1, v6

    if-gez v2, :cond_13

    const-wide v6, 0x2386f26fc10000L

    cmp-long v2, p1, v6

    if-gez v2, :cond_12

    const/16 v4, 0x10

    goto :goto_1

    :cond_12
    const/16 v4, 0x11

    goto :goto_1

    :cond_13
    const-wide v6, 0xde0b6b3a7640000L

    cmp-long v2, p1, v6

    if-gez v2, :cond_14

    const/16 v4, 0x12

    goto :goto_1

    :cond_14
    const/16 v4, 0x13

    :goto_1
    if-eqz v3, :cond_15

    add-int/lit8 v4, v4, 0x1

    :cond_15
    invoke-virtual {p0, v4}, Lf/h;->b(I)Lf/x;

    move-result-object v2

    iget-object v6, v2, Lf/x;->b:[B

    iget v7, v2, Lf/x;->d:I

    add-int/2addr v7, v4

    :goto_2
    cmp-long v8, p1, v0

    if-eqz v8, :cond_16

    int-to-long v8, v5

    rem-long v10, p1, v8

    long-to-int v10, v10

    add-int/lit8 v7, v7, -0x1

    sget-object v11, Lf/h;->a:[B

    aget-byte v10, v11, v10

    aput-byte v10, v6, v7

    div-long/2addr p1, v8

    goto :goto_2

    :cond_16
    if-eqz v3, :cond_17

    add-int/lit8 v7, v7, -0x1

    const/16 p1, 0x2d

    int-to-byte p1, p1

    aput-byte p1, v6, v7

    :cond_17
    iget p1, v2, Lf/x;->d:I

    add-int/2addr p1, v4

    iput p1, v2, Lf/x;->d:I

    iget-wide p1, p0, Lf/h;->d:J

    int-to-long v0, v4

    add-long/2addr p1, v0

    iput-wide p1, p0, Lf/h;->d:J

    return-object p0
.end method

.method public bridge synthetic g(J)Lf/j;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lf/h;->g(J)Lf/h;

    return-object p0
.end method

.method public g()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1}, Lf/h;->c(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBuffer()Lf/h;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    return-object p0
.end method

.method public final h(J)B
    .locals 8
    .annotation build Lkotlin/jvm/JvmName;
        name = "getByte"
    .end annotation

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v4, 0x1

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Lf/c;->a(JJJ)V

    iget-object v0, p0, Lf/h;->c:Lf/x;

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lf/h;->size()J

    move-result-wide v2

    sub-long/2addr v2, p1

    cmp-long v2, v2, p1

    if-gez v2, :cond_3

    invoke-virtual {p0}, Lf/h;->size()J

    move-result-wide v2

    :goto_0
    cmp-long v4, v2, p1

    if-lez v4, :cond_1

    iget-object v0, v0, Lf/x;->h:Lf/x;

    if-eqz v0, :cond_0

    iget v4, v0, Lf/x;->d:I

    iget v5, v0, Lf/x;->c:I

    sub-int/2addr v4, v5

    int-to-long v4, v4

    sub-long/2addr v2, v4

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, v0, Lf/x;->b:[B

    iget v0, v0, Lf/x;->c:I

    int-to-long v4, v0

    add-long/2addr v4, p1

    sub-long/2addr v4, v2

    long-to-int p1, v4

    aget-byte p1, v1, p1

    return p1

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_3
    const-wide/16 v2, 0x0

    :goto_1
    iget v4, v0, Lf/x;->d:I

    iget v5, v0, Lf/x;->c:I

    sub-int/2addr v4, v5

    int-to-long v6, v4

    add-long/2addr v6, v2

    cmp-long v4, v6, p1

    if-lez v4, :cond_5

    if-eqz v0, :cond_4

    iget-object v0, v0, Lf/x;->b:[B

    int-to-long v4, v5

    add-long/2addr v4, p1

    sub-long/2addr v4, v2

    long-to-int p1, v4

    aget-byte p1, v0, p1

    return p1

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_5
    iget-object v0, v0, Lf/x;->g:Lf/x;

    if-eqz v0, :cond_6

    move-wide v2, v6

    goto :goto_1

    :cond_6
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1
.end method

.method public h()J
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    move v1, v0

    move-wide v4, v2

    :cond_0
    iget-object v6, p0, Lf/h;->c:Lf/x;

    if-eqz v6, :cond_9

    iget-object v7, v6, Lf/x;->b:[B

    iget v8, v6, Lf/x;->c:I

    iget v9, v6, Lf/x;->d:I

    :goto_0
    if-ge v8, v9, :cond_6

    aget-byte v10, v7, v8

    const/16 v11, 0x30

    int-to-byte v11, v11

    if-lt v10, v11, :cond_1

    const/16 v12, 0x39

    int-to-byte v12, v12

    if-gt v10, v12, :cond_1

    sub-int v11, v10, v11

    goto :goto_2

    :cond_1
    const/16 v11, 0x61

    int-to-byte v11, v11

    if-lt v10, v11, :cond_2

    const/16 v12, 0x66

    int-to-byte v12, v12

    if-gt v10, v12, :cond_2

    :goto_1
    sub-int v11, v10, v11

    add-int/lit8 v11, v11, 0xa

    goto :goto_2

    :cond_2
    const/16 v11, 0x41

    int-to-byte v11, v11

    if-lt v10, v11, :cond_4

    const/16 v12, 0x46

    int-to-byte v12, v12

    if-gt v10, v12, :cond_4

    goto :goto_1

    :goto_2
    const-wide/high16 v12, -0x1000000000000000L    # -3.105036184601418E231

    and-long/2addr v12, v4

    cmp-long v12, v12, v2

    if-nez v12, :cond_3

    const/4 v10, 0x4

    shl-long/2addr v4, v10

    int-to-long v10, v11

    or-long/2addr v4, v10

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    new-instance v0, Lf/h;

    invoke-direct {v0}, Lf/h;-><init>()V

    invoke-virtual {v0, v4, v5}, Lf/h;->d(J)Lf/h;

    invoke-virtual {v0, v10}, Lf/h;->writeByte(I)Lf/h;

    new-instance v1, Ljava/lang/NumberFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Number too large: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lf/h;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_3

    :cond_5
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected leading [0-9a-fA-F] character but was 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    :goto_3
    if-ne v8, v9, :cond_7

    invoke-virtual {v6}, Lf/x;->b()Lf/x;

    move-result-object v7

    iput-object v7, p0, Lf/h;->c:Lf/x;

    invoke-static {v6}, Lf/y;->a(Lf/x;)V

    goto :goto_4

    :cond_7
    iput v8, v6, Lf/x;->c:I

    :goto_4
    if-nez v0, :cond_8

    iget-object v6, p0, Lf/h;->c:Lf/x;

    if-nez v6, :cond_0

    :cond_8
    iget-wide v2, p0, Lf/h;->d:J

    int-to-long v0, v1

    sub-long/2addr v2, v0

    iput-wide v2, p0, Lf/h;->d:J

    return-wide v4

    :cond_9
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0

    :cond_a
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0
.end method

.method public hashCode()I
    .locals 5

    iget-object v0, p0, Lf/h;->c:Lf/x;

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :cond_0
    iget v2, v0, Lf/x;->c:I

    iget v3, v0, Lf/x;->d:I

    :goto_0
    if-ge v2, v3, :cond_1

    mul-int/lit8 v1, v1, 0x1f

    iget-object v4, v0, Lf/x;->b:[B

    aget-byte v4, v4, v2

    add-int/2addr v1, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lf/x;->g:Lf/x;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lf/h;->c:Lf/x;

    if-ne v0, v2, :cond_0

    return v1

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0

    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method public i()Ljava/io/InputStream;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lf/i;

    invoke-direct {v0, p0}, Lf/i;-><init>(Lf/h;)V

    return-object v0
.end method

.method public i(J)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    sget-object v0, Lkotlin/g/c;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p0, p1, p2, v0}, Lf/h;->a(JLjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isOpen()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final j(J)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    const-wide/16 v1, 0x1

    if-lez v0, :cond_0

    sub-long v3, p1, v1

    invoke-virtual {p0, v3, v4}, Lf/h;->h(J)B

    move-result v0

    const/16 v5, 0xd

    int-to-byte v5, v5

    if-ne v0, v5, :cond_0

    invoke-virtual {p0, v3, v4}, Lf/h;->i(J)Ljava/lang/String;

    move-result-object p1

    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lf/h;->skip(J)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lf/h;->i(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v1, v2}, Lf/h;->skip(J)V

    :goto_0
    return-object p1
.end method

.method public j()[B
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-wide v0, p0, Lf/h;->d:J

    invoke-virtual {p0, v0, v1}, Lf/h;->e(J)[B

    move-result-object v0

    return-object v0
.end method

.method public k()Lf/l;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lf/l;

    invoke-virtual {p0}, Lf/h;->j()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lf/l;-><init>([B)V

    return-object v0
.end method

.method public final k(J)V
    .locals 0

    iput-wide p1, p0, Lf/h;->d:J

    return-void
.end method

.method public l()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h;->readInt()I

    move-result v0

    invoke-static {v0}, Lf/c;->a(I)I

    move-result v0

    return v0
.end method

.method public m()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h;->readShort()S

    move-result v0

    invoke-static {v0}, Lf/c;->a(S)S

    move-result v0

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-wide v0, p0, Lf/h;->d:J

    sget-object v2, Lkotlin/g/c;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0, v1, v2}, Lf/h;->a(JLjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lf/l;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-wide v0, p0, Lf/h;->d:J

    const v2, 0x7fffffff

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-wide v0, p0, Lf/h;->d:J

    long-to-int v0, v0

    invoke-virtual {p0, v0}, Lf/h;->a(I)Lf/l;

    move-result-object v0

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "size > Integer.MAX_VALUE: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lf/h;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public read(Ljava/nio/ByteBuffer;)I
    .locals 6
    .param p1    # Ljava/nio/ByteBuffer;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/h;->c:Lf/x;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    iget v2, v0, Lf/x;->d:I

    iget v3, v0, Lf/x;->c:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, v0, Lf/x;->b:[B

    iget v3, v0, Lf/x;->c:I

    invoke-virtual {p1, v2, v3, v1}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    iget p1, v0, Lf/x;->c:I

    add-int/2addr p1, v1

    iput p1, v0, Lf/x;->c:I

    iget-wide v2, p0, Lf/h;->d:J

    int-to-long v4, v1

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lf/h;->d:J

    iget p1, v0, Lf/x;->c:I

    iget v2, v0, Lf/x;->d:I

    if-ne p1, v2, :cond_0

    invoke-virtual {v0}, Lf/x;->b()Lf/x;

    move-result-object p1

    iput-object p1, p0, Lf/h;->c:Lf/x;

    invoke-static {v0}, Lf/y;->a(Lf/x;)V

    :cond_0
    return v1

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public readByte()B
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lf/h;->c:Lf/x;

    if-eqz v2, :cond_1

    iget v3, v2, Lf/x;->c:I

    iget v4, v2, Lf/x;->d:I

    iget-object v5, v2, Lf/x;->b:[B

    add-int/lit8 v6, v3, 0x1

    aget-byte v3, v5, v3

    const-wide/16 v7, 0x1

    sub-long/2addr v0, v7

    iput-wide v0, p0, Lf/h;->d:J

    if-ne v6, v4, :cond_0

    invoke-virtual {v2}, Lf/x;->b()Lf/x;

    move-result-object v0

    iput-object v0, p0, Lf/h;->c:Lf/x;

    invoke-static {v2}, Lf/y;->a(Lf/x;)V

    goto :goto_0

    :cond_0
    iput v6, v2, Lf/x;->c:I

    :goto_0
    return v3

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0

    :cond_2
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0
.end method

.method public readInt()I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x4

    cmp-long v4, v0, v2

    if-ltz v4, :cond_3

    iget-object v4, p0, Lf/h;->c:Lf/x;

    if-eqz v4, :cond_2

    iget v5, v4, Lf/x;->c:I

    iget v6, v4, Lf/x;->d:I

    sub-int v7, v6, v5

    int-to-long v7, v7

    cmp-long v7, v7, v2

    if-gez v7, :cond_0

    invoke-virtual {p0}, Lf/h;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    invoke-virtual {p0}, Lf/h;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    invoke-virtual {p0}, Lf/h;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    invoke-virtual {p0}, Lf/h;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v7, v4, Lf/x;->b:[B

    add-int/lit8 v8, v5, 0x1

    aget-byte v5, v7, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x18

    add-int/lit8 v9, v8, 0x1

    aget-byte v8, v7, v8

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x10

    or-int/2addr v5, v8

    add-int/lit8 v8, v9, 0x1

    aget-byte v9, v7, v9

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x8

    or-int/2addr v5, v9

    add-int/lit8 v9, v8, 0x1

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v5, v7

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lf/h;->d:J

    if-ne v9, v6, :cond_1

    invoke-virtual {v4}, Lf/x;->b()Lf/x;

    move-result-object v0

    iput-object v0, p0, Lf/h;->c:Lf/x;

    invoke-static {v4}, Lf/y;->a(Lf/x;)V

    goto :goto_0

    :cond_1
    iput v9, v4, Lf/x;->c:I

    :goto_0
    return v5

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0

    :cond_3
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0
.end method

.method public readShort()S
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x2

    cmp-long v4, v0, v2

    if-ltz v4, :cond_3

    iget-object v4, p0, Lf/h;->c:Lf/x;

    if-eqz v4, :cond_2

    iget v5, v4, Lf/x;->c:I

    iget v6, v4, Lf/x;->d:I

    sub-int v7, v6, v5

    const/4 v8, 0x2

    if-ge v7, v8, :cond_0

    invoke-virtual {p0}, Lf/h;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    invoke-virtual {p0}, Lf/h;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0

    :cond_0
    iget-object v7, v4, Lf/x;->b:[B

    add-int/lit8 v8, v5, 0x1

    aget-byte v5, v7, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    add-int/lit8 v9, v8, 0x1

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v5, v7

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lf/h;->d:J

    if-ne v9, v6, :cond_1

    invoke-virtual {v4}, Lf/x;->b()Lf/x;

    move-result-object v0

    iput-object v0, p0, Lf/h;->c:Lf/x;

    invoke-static {v4}, Lf/y;->a(Lf/x;)V

    goto :goto_0

    :cond_1
    iput v9, v4, Lf/x;->c:I

    :goto_0
    int-to-short v0, v5

    return v0

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 v0, 0x0

    throw v0

    :cond_3
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0
.end method

.method public final size()J
    .locals 2
    .annotation build Lkotlin/jvm/JvmName;
        name = "size"
    .end annotation

    iget-wide v0, p0, Lf/h;->d:J

    return-wide v0
.end method

.method public skip(J)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    :cond_0
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lf/h;->c:Lf/x;

    if-eqz v0, :cond_1

    iget v1, v0, Lf/x;->d:I

    iget v2, v0, Lf/x;->c:I

    sub-int/2addr v1, v2

    int-to-long v1, v1

    invoke-static {p1, p2, v1, v2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    long-to-int v1, v1

    iget-wide v2, p0, Lf/h;->d:J

    int-to-long v4, v1

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lf/h;->d:J

    sub-long/2addr p1, v4

    iget v2, v0, Lf/x;->c:I

    add-int/2addr v2, v1

    iput v2, v0, Lf/x;->c:I

    iget v1, v0, Lf/x;->c:I

    iget v2, v0, Lf/x;->d:I

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lf/x;->b()Lf/x;

    move-result-object v1

    iput-object v1, p0, Lf/h;->c:Lf/x;

    invoke-static {v0}, Lf/y;->a(Lf/x;)V

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :cond_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    invoke-virtual {p0}, Lf/h;->o()Lf/l;

    move-result-object v0

    invoke-virtual {v0}, Lf/l;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Ljava/nio/ByteBuffer;)I
    .locals 6
    .param p1    # Ljava/nio/ByteBuffer;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    move v1, v0

    :goto_0
    if-lez v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lf/h;->b(I)Lf/x;

    move-result-object v2

    iget v3, v2, Lf/x;->d:I

    rsub-int v3, v3, 0x2000

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-object v4, v2, Lf/x;->b:[B

    iget v5, v2, Lf/x;->d:I

    invoke-virtual {p1, v4, v5, v3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    sub-int/2addr v1, v3

    iget v4, v2, Lf/x;->d:I

    add-int/2addr v4, v3

    iput v4, v2, Lf/x;->d:I

    goto :goto_0

    :cond_0
    iget-wide v1, p0, Lf/h;->d:J

    int-to-long v3, v0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lf/h;->d:J

    return v0
.end method

.method public write([B)Lf/h;
    .locals 2
    .param p1    # [B
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lf/h;->write([BII)Lf/h;

    return-object p0
.end method

.method public write([BII)Lf/h;
    .locals 9
    .param p1    # [B
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v0, p1

    int-to-long v1, v0

    int-to-long v3, p2

    int-to-long v7, p3

    move-wide v5, v7

    invoke-static/range {v1 .. v6}, Lf/c;->a(JJJ)V

    add-int/2addr p3, p2

    :goto_0
    if-ge p2, p3, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lf/h;->b(I)Lf/x;

    move-result-object v0

    sub-int v1, p3, p2

    iget v2, v0, Lf/x;->d:I

    rsub-int v2, v2, 0x2000

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, v0, Lf/x;->b:[B

    iget v3, v0, Lf/x;->d:I

    invoke-static {p1, p2, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v1

    iget v2, v0, Lf/x;->d:I

    add-int/2addr v2, v1

    iput v2, v0, Lf/x;->d:I

    goto :goto_0

    :cond_0
    iget-wide p1, p0, Lf/h;->d:J

    add-long/2addr p1, v7

    iput-wide p1, p0, Lf/h;->d:J

    return-object p0
.end method

.method public bridge synthetic write([B)Lf/j;
    .locals 0

    invoke-virtual {p0, p1}, Lf/h;->write([B)Lf/h;

    return-object p0
.end method

.method public bridge synthetic write([BII)Lf/j;
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Lf/h;->write([BII)Lf/h;

    return-object p0
.end method

.method public writeByte(I)Lf/h;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lf/h;->b(I)Lf/x;

    move-result-object v0

    iget-object v1, v0, Lf/x;->b:[B

    iget v2, v0, Lf/x;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v0, Lf/x;->d:I

    int-to-byte p1, p1

    aput-byte p1, v1, v2

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lf/h;->d:J

    return-object p0
.end method

.method public bridge synthetic writeByte(I)Lf/j;
    .locals 0

    invoke-virtual {p0, p1}, Lf/h;->writeByte(I)Lf/h;

    return-object p0
.end method

.method public writeInt(I)Lf/h;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lf/h;->b(I)Lf/x;

    move-result-object v0

    iget-object v1, v0, Lf/x;->b:[B

    iget v2, v0, Lf/x;->d:I

    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x18

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    add-int/lit8 v2, v3, 0x1

    ushr-int/lit8 v4, p1, 0x10

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    add-int/lit8 v2, v3, 0x1

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    aput-byte p1, v1, v3

    iput v2, v0, Lf/x;->d:I

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lf/h;->d:J

    return-object p0
.end method

.method public bridge synthetic writeInt(I)Lf/j;
    .locals 0

    invoke-virtual {p0, p1}, Lf/h;->writeInt(I)Lf/h;

    return-object p0
.end method

.method public writeShort(I)Lf/h;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lf/h;->b(I)Lf/x;

    move-result-object v0

    iget-object v1, v0, Lf/x;->b:[B

    iget v2, v0, Lf/x;->d:I

    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    add-int/lit8 v2, v3, 0x1

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    aput-byte p1, v1, v3

    iput v2, v0, Lf/x;->d:I

    iget-wide v0, p0, Lf/h;->d:J

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lf/h;->d:J

    return-object p0
.end method

.method public bridge synthetic writeShort(I)Lf/j;
    .locals 0

    invoke-virtual {p0, p1}, Lf/h;->writeShort(I)Lf/h;

    return-object p0
.end method
