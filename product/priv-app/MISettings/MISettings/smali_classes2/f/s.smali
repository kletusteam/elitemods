.class public final Lf/s;
.super Ljava/lang/Object;


# annotations
.annotation build Lkotlin/jvm/JvmName;
    name = "Okio"
.end annotation


# direct methods
.method public static final a()Lf/A;
    .locals 1
    .annotation build Lkotlin/jvm/JvmName;
        name = "blackhole"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lf/g;

    invoke-direct {v0}, Lf/g;-><init>()V

    return-object v0
.end method

.method public static final a(Ljava/io/File;)Lf/A;
    .locals 2
    .param p0    # Ljava/io/File;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/io/FileOutputStream;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-static {v0}, Lf/s;->a(Ljava/io/OutputStream;)Lf/A;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Ljava/io/File;Z)Lf/A;
    .locals 1
    .param p0    # Ljava/io/File;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .annotation build Lkotlin/jvm/JvmOverloads;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-static {v0}, Lf/s;->a(Ljava/io/OutputStream;)Lf/A;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic a(Ljava/io/File;ZILjava/lang/Object;)Lf/A;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .annotation build Lkotlin/jvm/JvmOverloads;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-static {p0, p1}, Lf/s;->a(Ljava/io/File;Z)Lf/A;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Ljava/io/OutputStream;)Lf/A;
    .locals 2
    .param p0    # Ljava/io/OutputStream;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/u;

    new-instance v1, Lf/E;

    invoke-direct {v1}, Lf/E;-><init>()V

    invoke-direct {v0, p0, v1}, Lf/u;-><init>(Ljava/io/OutputStream;Lf/E;)V

    return-object v0
.end method

.method public static final a(Ljava/net/Socket;)Lf/A;
    .locals 3
    .param p0    # Ljava/net/Socket;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/B;

    invoke-direct {v0, p0}, Lf/B;-><init>(Ljava/net/Socket;)V

    new-instance v1, Lf/u;

    invoke-virtual {p0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object p0

    const-string v2, "getOutputStream()"

    invoke-static {p0, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p0, v0}, Lf/u;-><init>(Ljava/io/OutputStream;Lf/E;)V

    invoke-virtual {v0, v1}, Lf/d;->a(Lf/A;)Lf/A;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Ljava/io/InputStream;)Lf/C;
    .locals 2
    .param p0    # Ljava/io/InputStream;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/r;

    new-instance v1, Lf/E;

    invoke-direct {v1}, Lf/E;-><init>()V

    invoke-direct {v0, p0, v1}, Lf/r;-><init>(Ljava/io/InputStream;Lf/E;)V

    return-object v0
.end method

.method public static final a(Lf/A;)Lf/j;
    .locals 1
    .param p0    # Lf/A;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/v;

    invoke-direct {v0, p0}, Lf/v;-><init>(Lf/A;)V

    return-object v0
.end method

.method public static final a(Lf/C;)Lf/k;
    .locals 1
    .param p0    # Lf/C;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/w;

    invoke-direct {v0, p0}, Lf/w;-><init>(Lf/C;)V

    return-object v0
.end method

.method public static final a(Ljava/lang/AssertionError;)Z
    .locals 4
    .param p0    # Ljava/lang/AssertionError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/AssertionError;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/AssertionError;->getMessage()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 v0, 0x2

    const/4 v2, 0x0

    const-string v3, "getsockname failed"

    invoke-static {p0, v3, v1, v0, v2}, Lkotlin/g/g;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result p0

    goto :goto_0

    :cond_0
    move p0, v1

    :goto_0
    if-eqz p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public static final b(Ljava/io/File;)Lf/C;
    .locals 1
    .param p0    # Ljava/io/File;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v0}, Lf/s;->a(Ljava/io/InputStream;)Lf/C;

    move-result-object p0

    return-object p0
.end method

.method public static final b(Ljava/net/Socket;)Lf/C;
    .locals 3
    .param p0    # Ljava/net/Socket;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/B;

    invoke-direct {v0, p0}, Lf/B;-><init>(Ljava/net/Socket;)V

    new-instance v1, Lf/r;

    invoke-virtual {p0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object p0

    const-string v2, "getInputStream()"

    invoke-static {p0, v2}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p0, v0}, Lf/r;-><init>(Ljava/io/InputStream;Lf/E;)V

    invoke-virtual {v0, v1}, Lf/d;->a(Lf/C;)Lf/C;

    move-result-object p0

    return-object p0
.end method
