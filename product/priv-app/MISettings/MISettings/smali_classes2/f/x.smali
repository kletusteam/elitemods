.class public final Lf/x;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/x$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u000b\u0008\u0000\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u0007\u0008\u0016\u00a2\u0006\u0002\u0010\u0002B/\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000bJ\u0006\u0010\u000e\u001a\u00020\u000fJ\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0000J\u000e\u0010\u0011\u001a\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u0000J\u0006\u0010\u0013\u001a\u00020\u0000J\u000e\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u0015\u001a\u00020\u0006J\u0006\u0010\u0016\u001a\u00020\u0000J\u0016\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u00002\u0006\u0010\u0015\u001a\u00020\u0006R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u00020\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0004\u0018\u00010\u00008\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u00020\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u00020\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00008\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0008\u001a\u00020\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lokio/Segment;",
        "",
        "()V",
        "data",
        "",
        "pos",
        "",
        "limit",
        "shared",
        "",
        "owner",
        "([BIIZZ)V",
        "next",
        "prev",
        "compact",
        "",
        "pop",
        "push",
        "segment",
        "sharedCopy",
        "split",
        "byteCount",
        "unsharedCopy",
        "writeTo",
        "sink",
        "Companion",
        "jvm"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xb
    }
.end annotation


# static fields
.field public static final a:Lf/x$a;


# instance fields
.field public final b:[B
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public c:I
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation
.end field

.field public d:I
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation
.end field

.field public e:Z
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation
.end field

.field public f:Z
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation
.end field

.field public g:Lf/x;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field public h:Lf/x;
    .annotation build Lkotlin/jvm/JvmField;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/x$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/x$a;-><init>(Lkotlin/jvm/b/g;)V

    sput-object v0, Lf/x;->a:Lf/x$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Lf/x;->b:[B

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/x;->f:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/x;->e:Z

    return-void
.end method

.method public constructor <init>([BIIZZ)V
    .locals 1
    .param p1    # [B
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/x;->b:[B

    iput p2, p0, Lf/x;->c:I

    iput p3, p0, Lf/x;->d:I

    iput-boolean p4, p0, Lf/x;->e:Z

    iput-boolean p5, p0, Lf/x;->f:Z

    return-void
.end method


# virtual methods
.method public final a(I)Lf/x;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x0

    if-lez p1, :cond_0

    iget v1, p0, Lf/x;->d:I

    iget v2, p0, Lf/x;->c:I

    sub-int/2addr v1, v2

    if-gt p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    if-eqz v1, :cond_3

    const/16 v1, 0x400

    if-lt p1, v1, :cond_1

    invoke-virtual {p0}, Lf/x;->c()Lf/x;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-static {}, Lf/y;->a()Lf/x;

    move-result-object v1

    iget-object v2, p0, Lf/x;->b:[B

    iget v3, p0, Lf/x;->c:I

    iget-object v4, v1, Lf/x;->b:[B

    invoke-static {v2, v3, v4, v0, p1}, Lf/b;->a([BI[BII)V

    move-object v0, v1

    :goto_1
    iget v1, v0, Lf/x;->c:I

    add-int/2addr v1, p1

    iput v1, v0, Lf/x;->d:I

    iget v1, p0, Lf/x;->c:I

    add-int/2addr v1, p1

    iput v1, p0, Lf/x;->c:I

    iget-object p1, p0, Lf/x;->h:Lf/x;

    if-eqz p1, :cond_2

    invoke-virtual {p1, v0}, Lf/x;->a(Lf/x;)Lf/x;

    return-object v0

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "byteCount out of range"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Lf/x;)Lf/x;
    .locals 1
    .param p1    # Lf/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "segment"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p0, p1, Lf/x;->h:Lf/x;

    iget-object v0, p0, Lf/x;->g:Lf/x;

    iput-object v0, p1, Lf/x;->g:Lf/x;

    iget-object v0, p0, Lf/x;->g:Lf/x;

    if-eqz v0, :cond_0

    iput-object p1, v0, Lf/x;->h:Lf/x;

    iput-object p1, p0, Lf/x;->g:Lf/x;

    return-object p1

    :cond_0
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    const/4 p1, 0x0

    throw p1
.end method

.method public final a()V
    .locals 6

    iget-object v0, p0, Lf/x;->h:Lf/x;

    const/4 v1, 0x0

    if-eq v0, p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_9

    iget-object v0, p0, Lf/x;->h:Lf/x;

    const/4 v2, 0x0

    if-eqz v0, :cond_8

    iget-boolean v3, v0, Lf/x;->f:Z

    if-nez v3, :cond_1

    return-void

    :cond_1
    iget v3, p0, Lf/x;->d:I

    iget v4, p0, Lf/x;->c:I

    sub-int/2addr v3, v4

    if-eqz v0, :cond_7

    iget v4, v0, Lf/x;->d:I

    rsub-int v4, v4, 0x2000

    if-eqz v0, :cond_6

    iget-boolean v5, v0, Lf/x;->e:Z

    if-eqz v5, :cond_2

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_5

    iget v1, v0, Lf/x;->c:I

    :goto_1
    add-int/2addr v4, v1

    if-le v3, v4, :cond_3

    return-void

    :cond_3
    iget-object v0, p0, Lf/x;->h:Lf/x;

    if-eqz v0, :cond_4

    invoke-virtual {p0, v0, v3}, Lf/x;->a(Lf/x;I)V

    invoke-virtual {p0}, Lf/x;->b()Lf/x;

    invoke-static {p0}, Lf/y;->a(Lf/x;)V

    return-void

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_5
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_6
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_7
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_8
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v2

    :cond_9
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cannot compact"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lf/x;I)V
    .locals 4
    .param p1    # Lf/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p1, Lf/x;->f:Z

    if-eqz v0, :cond_3

    iget v0, p1, Lf/x;->d:I

    add-int v1, v0, p2

    const/16 v2, 0x2000

    if-le v1, v2, :cond_2

    iget-boolean v1, p1, Lf/x;->e:Z

    if-nez v1, :cond_1

    add-int v1, v0, p2

    iget v3, p1, Lf/x;->c:I

    sub-int/2addr v1, v3

    if-gt v1, v2, :cond_0

    iget-object v1, p1, Lf/x;->b:[B

    sub-int/2addr v0, v3

    const/4 v2, 0x0

    invoke-static {v1, v3, v1, v2, v0}, Lf/b;->a([BI[BII)V

    iget v0, p1, Lf/x;->d:I

    iget v1, p1, Lf/x;->c:I

    sub-int/2addr v0, v1

    iput v0, p1, Lf/x;->d:I

    iput v2, p1, Lf/x;->c:I

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :cond_2
    :goto_0
    iget-object v0, p0, Lf/x;->b:[B

    iget v1, p0, Lf/x;->c:I

    iget-object v2, p1, Lf/x;->b:[B

    iget v3, p1, Lf/x;->d:I

    invoke-static {v0, v1, v2, v3, p2}, Lf/b;->a([BI[BII)V

    iget v0, p1, Lf/x;->d:I

    add-int/2addr v0, p2

    iput v0, p1, Lf/x;->d:I

    iget p1, p0, Lf/x;->c:I

    add-int/2addr p1, p2

    iput p1, p0, Lf/x;->c:I

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "only owner can write"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final b()Lf/x;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Lf/x;->g:Lf/x;

    const/4 v1, 0x0

    if-eq v0, p0, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iget-object v2, p0, Lf/x;->h:Lf/x;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lf/x;->g:Lf/x;

    iput-object v3, v2, Lf/x;->g:Lf/x;

    iget-object v3, p0, Lf/x;->g:Lf/x;

    if-eqz v3, :cond_1

    iput-object v2, v3, Lf/x;->h:Lf/x;

    iput-object v1, p0, Lf/x;->g:Lf/x;

    iput-object v1, p0, Lf/x;->h:Lf/x;

    return-object v0

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_2
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1
.end method

.method public final c()Lf/x;
    .locals 7
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/x;->e:Z

    new-instance v0, Lf/x;

    iget-object v2, p0, Lf/x;->b:[B

    iget v3, p0, Lf/x;->c:I

    iget v4, p0, Lf/x;->d:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lf/x;-><init>([BIIZZ)V

    return-object v0
.end method
