.class public final Lf/d$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/b/g;)V
    .locals 0

    invoke-direct {p0}, Lf/d$a;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lf/d$a;Lf/d;JZ)V
    .locals 0
    .param p1    # Lf/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3, p4}, Lf/d$a;->a(Lf/d;JZ)V

    return-void
.end method

.method private final a(Lf/d;JZ)V
    .locals 6

    const-class v0, Lf/d;

    monitor-enter v0

    :try_start_0
    invoke-static {}, Lf/d;->g()Lf/d;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lf/d;

    invoke-direct {v1}, Lf/d;-><init>()V

    invoke-static {v1}, Lf/d;->b(Lf/d;)V

    new-instance v1, Lf/d$b;

    invoke-direct {v1}, Lf/d$b;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, p2, v3

    if-eqz v3, :cond_1

    if-eqz p4, :cond_1

    invoke-virtual {p1}, Lf/E;->c()J

    move-result-wide v3

    sub-long/2addr v3, v1

    invoke-static {p2, p3, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    add-long/2addr p2, v1

    invoke-static {p1, p2, p3}, Lf/d;->b(Lf/d;J)V

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    add-long/2addr p2, v1

    invoke-static {p1, p2, p3}, Lf/d;->b(Lf/d;J)V

    goto :goto_0

    :cond_2
    if-eqz p4, :cond_9

    invoke-virtual {p1}, Lf/E;->c()J

    move-result-wide p2

    invoke-static {p1, p2, p3}, Lf/d;->b(Lf/d;J)V

    :goto_0
    invoke-static {p1, v1, v2}, Lf/d;->a(Lf/d;J)J

    move-result-wide p2

    invoke-static {}, Lf/d;->g()Lf/d;

    move-result-object p4

    const/4 v3, 0x0

    if-eqz p4, :cond_8

    :goto_1
    invoke-static {p4}, Lf/d;->a(Lf/d;)Lf/d;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-static {p4}, Lf/d;->a(Lf/d;)Lf/d;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-static {v4, v1, v2}, Lf/d;->a(Lf/d;J)J

    move-result-wide v4

    cmp-long v4, p2, v4

    if-gez v4, :cond_3

    goto :goto_2

    :cond_3
    invoke-static {p4}, Lf/d;->a(Lf/d;)Lf/d;

    move-result-object p4

    if-eqz p4, :cond_4

    goto :goto_1

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_5
    :try_start_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :cond_6
    :goto_2
    :try_start_2
    invoke-static {p4}, Lf/d;->a(Lf/d;)Lf/d;

    move-result-object p2

    invoke-static {p1, p2}, Lf/d;->a(Lf/d;Lf/d;)V

    invoke-static {p4, p1}, Lf/d;->a(Lf/d;Lf/d;)V

    invoke-static {}, Lf/d;->g()Lf/d;

    move-result-object p1

    if-ne p4, p1, :cond_7

    const-class p1, Lf/d;

    invoke-virtual {p1}, Ljava/lang/Object;->notify()V

    :cond_7
    sget-object p1, Lkotlin/r;->a:Lkotlin/r;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v0

    return-void

    :cond_8
    :try_start_3
    invoke-static {}, Lkotlin/jvm/b/i;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    :cond_9
    :try_start_4
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public static final synthetic a(Lf/d$a;Lf/d;)Z
    .locals 0
    .param p1    # Lf/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lf/d$a;->a(Lf/d;)Z

    move-result p0

    return p0
.end method

.method private final a(Lf/d;)Z
    .locals 3

    const-class v0, Lf/d;

    monitor-enter v0

    :try_start_0
    invoke-static {}, Lf/d;->g()Lf/d;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    invoke-static {v1}, Lf/d;->a(Lf/d;)Lf/d;

    move-result-object v2

    if-ne v2, p1, :cond_0

    invoke-static {p1}, Lf/d;->a(Lf/d;)Lf/d;

    move-result-object v2

    invoke-static {v1, v2}, Lf/d;->a(Lf/d;Lf/d;)V

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lf/d;->a(Lf/d;Lf/d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x0

    monitor-exit v0

    return p1

    :cond_0
    :try_start_1
    invoke-static {v1}, Lf/d;->a(Lf/d;)Lf/d;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method


# virtual methods
.method public final a()Lf/d;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    invoke-static {}, Lf/d;->g()Lf/d;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    invoke-static {v0}, Lf/d;->a(Lf/d;)Lf/d;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    const-class v0, Lf/d;

    invoke-static {}, Lf/d;->h()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/Object;->wait(J)V

    invoke-static {}, Lf/d;->g()Lf/d;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lf/d;->a(Lf/d;)Lf/d;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v4, v2

    invoke-static {}, Lf/d;->i()J

    move-result-wide v2

    cmp-long v0, v4, v2

    if-ltz v0, :cond_0

    invoke-static {}, Lf/d;->g()Lf/d;

    move-result-object v1

    :cond_0
    return-object v1

    :cond_1
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lf/d;->a(Lf/d;J)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_3

    const-wide/32 v4, 0xf4240

    div-long v6, v2, v4

    mul-long/2addr v4, v6

    sub-long/2addr v2, v4

    const-class v0, Lf/d;

    long-to-int v2, v2

    invoke-virtual {v0, v6, v7, v2}, Ljava/lang/Object;->wait(JI)V

    return-object v1

    :cond_3
    invoke-static {}, Lf/d;->g()Lf/d;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-static {v0}, Lf/d;->a(Lf/d;)Lf/d;

    move-result-object v3

    invoke-static {v2, v3}, Lf/d;->a(Lf/d;Lf/d;)V

    invoke-static {v0, v1}, Lf/d;->a(Lf/d;Lf/d;)V

    return-object v0

    :cond_4
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1

    :cond_5
    invoke-static {}, Lkotlin/jvm/b/i;->a()V

    throw v1
.end method
