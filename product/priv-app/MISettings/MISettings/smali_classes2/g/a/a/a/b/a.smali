.class public Lg/a/a/a/b/a;
.super Ljava/lang/Object;


# direct methods
.method public static a()Ljava/security/MessageDigest;
    .locals 1

    const-string v0, "SHA-1"

    invoke-static {v0}, Lg/a/a/a/b/a;->a(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/security/MessageDigest;
    .locals 1

    :try_start_0
    invoke-static {p0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object p0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static a([B)[B
    .locals 1

    invoke-static {}, Lg/a/a/a/b/a;->a()Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/lang/String;)[B
    .locals 0

    invoke-static {p0}, Lg/a/a/a/a/b;->a(Ljava/lang/String;)[B

    move-result-object p0

    invoke-static {p0}, Lg/a/a/a/b/a;->a([B)[B

    move-result-object p0

    return-object p0
.end method
