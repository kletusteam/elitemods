.class public final Ld/w/a;
.super Ljava/lang/Object;


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f060000

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f060001

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f060002

.field public static final abc_btn_colored_text_material:I = 0x7f060003

.field public static final abc_color_highlight_material:I = 0x7f060004

.field public static final abc_decor_view_status_guard:I = 0x7f060005

.field public static final abc_decor_view_status_guard_light:I = 0x7f060006

.field public static final abc_hint_foreground_material_dark:I = 0x7f060007

.field public static final abc_hint_foreground_material_light:I = 0x7f060008

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f060009

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f06000a

.field public static final abc_primary_text_material_dark:I = 0x7f06000b

.field public static final abc_primary_text_material_light:I = 0x7f06000c

.field public static final abc_search_url_text:I = 0x7f06000d

.field public static final abc_search_url_text_normal:I = 0x7f06000e

.field public static final abc_search_url_text_pressed:I = 0x7f06000f

.field public static final abc_search_url_text_selected:I = 0x7f060010

.field public static final abc_secondary_text_material_dark:I = 0x7f060011

.field public static final abc_secondary_text_material_light:I = 0x7f060012

.field public static final abc_tint_btn_checkable:I = 0x7f060013

.field public static final abc_tint_default:I = 0x7f060014

.field public static final abc_tint_edittext:I = 0x7f060015

.field public static final abc_tint_seek_thumb:I = 0x7f060016

.field public static final abc_tint_spinner:I = 0x7f060017

.field public static final abc_tint_switch_track:I = 0x7f060018

.field public static final accent_material_dark:I = 0x7f060019

.field public static final accent_material_light:I = 0x7f06001a

.field public static final androidx_core_ripple_material_light:I = 0x7f06001f

.field public static final androidx_core_secondary_text_default_material_light:I = 0x7f060020

.field public static final background_floating_material_dark:I = 0x7f060032

.field public static final background_floating_material_light:I = 0x7f060033

.field public static final background_material_dark:I = 0x7f060034

.field public static final background_material_light:I = 0x7f060035

.field public static final bright_foreground_disabled_material_dark:I = 0x7f060047

.field public static final bright_foreground_disabled_material_light:I = 0x7f060048

.field public static final bright_foreground_inverse_material_dark:I = 0x7f060049

.field public static final bright_foreground_inverse_material_light:I = 0x7f06004a

.field public static final bright_foreground_material_dark:I = 0x7f06004b

.field public static final bright_foreground_material_light:I = 0x7f06004c

.field public static final button_material_dark:I = 0x7f06004f

.field public static final button_material_light:I = 0x7f060050

.field public static final dim_foreground_disabled_material_dark:I = 0x7f060068

.field public static final dim_foreground_disabled_material_light:I = 0x7f060069

.field public static final dim_foreground_material_dark:I = 0x7f06006a

.field public static final dim_foreground_material_light:I = 0x7f06006b

.field public static final error_color_material_dark:I = 0x7f06006c

.field public static final error_color_material_light:I = 0x7f06006d

.field public static final foreground_material_dark:I = 0x7f06007e

.field public static final foreground_material_light:I = 0x7f06007f

.field public static final highlighted_text_material_dark:I = 0x7f06008d

.field public static final highlighted_text_material_light:I = 0x7f06008e

.field public static final material_blue_grey_800:I = 0x7f060098

.field public static final material_blue_grey_900:I = 0x7f060099

.field public static final material_blue_grey_950:I = 0x7f06009a

.field public static final material_deep_teal_200:I = 0x7f06009b

.field public static final material_deep_teal_500:I = 0x7f06009c

.field public static final material_grey_100:I = 0x7f06009d

.field public static final material_grey_300:I = 0x7f06009e

.field public static final material_grey_50:I = 0x7f06009f

.field public static final material_grey_600:I = 0x7f0600a0

.field public static final material_grey_800:I = 0x7f0600a1

.field public static final material_grey_850:I = 0x7f0600a2

.field public static final material_grey_900:I = 0x7f0600a3

.field public static final miuix_folme_color_blink_tint:I = 0x7f0602a5

.field public static final miuix_folme_color_touch_tint:I = 0x7f0602a6

.field public static final miuix_folme_color_touch_tint_dark:I = 0x7f0602a7

.field public static final miuix_folme_color_touch_tint_dark_p1:I = 0x7f0602a8

.field public static final miuix_folme_color_touch_tint_dark_p2:I = 0x7f0602a9

.field public static final miuix_folme_color_touch_tint_dark_p3:I = 0x7f0602aa

.field public static final miuix_folme_color_touch_tint_light:I = 0x7f0602ab

.field public static final miuix_folme_color_touch_tint_light_p1:I = 0x7f0602ac

.field public static final miuix_folme_color_touch_tint_light_p2:I = 0x7f0602ad

.field public static final miuix_folme_color_touch_tint_light_p3:I = 0x7f0602ae

.field public static final notification_action_color_filter:I = 0x7f060305

.field public static final notification_icon_bg_color:I = 0x7f060306

.field public static final primary_dark_material_dark:I = 0x7f06030f

.field public static final primary_dark_material_light:I = 0x7f060310

.field public static final primary_material_dark:I = 0x7f060311

.field public static final primary_material_light:I = 0x7f060312

.field public static final primary_text_default_material_dark:I = 0x7f060313

.field public static final primary_text_default_material_light:I = 0x7f060314

.field public static final primary_text_disabled_material_dark:I = 0x7f060315

.field public static final primary_text_disabled_material_light:I = 0x7f060316

.field public static final ripple_material_dark:I = 0x7f06031c

.field public static final ripple_material_light:I = 0x7f06031d

.field public static final secondary_text_default_material_dark:I = 0x7f06031e

.field public static final secondary_text_default_material_light:I = 0x7f06031f

.field public static final secondary_text_disabled_material_dark:I = 0x7f060320

.field public static final secondary_text_disabled_material_light:I = 0x7f060321

.field public static final switch_thumb_disabled_material_dark:I = 0x7f060327

.field public static final switch_thumb_disabled_material_light:I = 0x7f060328

.field public static final switch_thumb_material_dark:I = 0x7f060329

.field public static final switch_thumb_material_light:I = 0x7f06032a

.field public static final switch_thumb_normal_material_dark:I = 0x7f06032b

.field public static final switch_thumb_normal_material_light:I = 0x7f06032c

.field public static final tooltip_background_dark:I = 0x7f060334

.field public static final tooltip_background_light:I = 0x7f060335

.field public static final visual_check_borderlayout_bg_stroke_color:I = 0x7f0603a4

.field public static final visual_check_text_color:I = 0x7f0603a5

.field public static final visual_check_text_color_red:I = 0x7f0603a6

.field public static final visual_check_textview_checked_text_color:I = 0x7f0603a7

.field public static final visual_check_textview_checked_text_color_red:I = 0x7f0603a8

.field public static final visual_check_textview_unchecked_text_color:I = 0x7f0603a9
