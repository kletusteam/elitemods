.class public final enum Lb/c/b/c/a$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb/c/b/c/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lb/c/b/c/a$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lb/c/b/c/a$a;

.field public static final enum b:Lb/c/b/c/a$a;

.field private static final synthetic c:[Lb/c/b/c/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lb/c/b/c/a$a;

    const/4 v1, 0x0

    const-string v2, "GET"

    invoke-direct {v0, v2, v1}, Lb/c/b/c/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/c/b/c/a$a;->a:Lb/c/b/c/a$a;

    new-instance v0, Lb/c/b/c/a$a;

    const/4 v2, 0x1

    const-string v3, "POST"

    invoke-direct {v0, v3, v2}, Lb/c/b/c/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/c/b/c/a$a;->b:Lb/c/b/c/a$a;

    const/4 v0, 0x2

    new-array v0, v0, [Lb/c/b/c/a$a;

    sget-object v3, Lb/c/b/c/a$a;->a:Lb/c/b/c/a$a;

    aput-object v3, v0, v1

    sget-object v1, Lb/c/b/c/a$a;->b:Lb/c/b/c/a$a;

    aput-object v1, v0, v2

    sput-object v0, Lb/c/b/c/a$a;->c:[Lb/c/b/c/a$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lb/c/b/c/a$a;
    .locals 1

    const-class v0, Lb/c/b/c/a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lb/c/b/c/a$a;

    return-object p0
.end method

.method public static values()[Lb/c/b/c/a$a;
    .locals 1

    sget-object v0, Lb/c/b/c/a$a;->c:[Lb/c/b/c/a$a;

    invoke-virtual {v0}, [Lb/c/b/c/a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lb/c/b/c/a$a;

    return-object v0
.end method
