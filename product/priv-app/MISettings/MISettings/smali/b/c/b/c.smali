.class Lb/c/b/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lb/c/b/d;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Exception;

.field final synthetic b:Lb/c/b/d;


# direct methods
.method constructor <init>(Lb/c/b/d;Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lb/c/b/c;->b:Lb/c/b/d;

    iput-object p2, p0, Lb/c/b/c;->a:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lb/c/b/c;->a:Ljava/lang/Exception;

    const-string v1, "DownloadBitmapUtils"

    const-string v2, "run: exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lb/c/b/c;->b:Lb/c/b/d;

    iget-object v1, v0, Lb/c/b/d;->b:Landroid/widget/ImageView;

    iget v0, v0, Lb/c/b/d;->c:I

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method
