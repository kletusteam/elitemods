.class public Lb/c/b/b/d;
.super Ljava/lang/Object;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static d:Lb/c/b/b/d;


# instance fields
.field private e:Ljava/util/concurrent/ThreadPoolExecutor;

.field private f:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/concurrent/DelayQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/DelayQueue<",
            "Lb/c/b/c/g;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/Runnable;

.field private i:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, Lb/c/b/b/d;->a:I

    sget v0, Lb/c/b/b/d;->a:I

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sput v0, Lb/c/b/b/d;->b:I

    sget v0, Lb/c/b/b/d;->a:I

    mul-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    sput v0, Lb/c/b/b/d;->c:I

    return-void
.end method

.method private constructor <init>()V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lb/c/b/b/a;

    invoke-direct {v0, p0}, Lb/c/b/b/a;-><init>(Lb/c/b/b/d;)V

    iput-object v0, p0, Lb/c/b/b/d;->h:Ljava/lang/Runnable;

    new-instance v0, Lb/c/b/b/b;

    invoke-direct {v0, p0}, Lb/c/b/b/b;-><init>(Lb/c/b/b/d;)V

    iput-object v0, p0, Lb/c/b/b/d;->i:Ljava/lang/Runnable;

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lb/c/b/b/d;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v0, Ljava/util/concurrent/DelayQueue;

    invoke-direct {v0}, Ljava/util/concurrent/DelayQueue;-><init>()V

    iput-object v0, p0, Lb/c/b/b/d;->g:Ljava/util/concurrent/DelayQueue;

    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sget v2, Lb/c/b/b/d;->b:I

    sget v3, Lb/c/b/b/d;->c:I

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/ArrayBlockingQueue;

    const/4 v1, 0x4

    invoke-direct {v7, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    new-instance v8, Lb/c/b/b/c;

    invoke-direct {v8, p0}, Lb/c/b/b/c;-><init>(Lb/c/b/b/d;)V

    const-wide/16 v4, 0x1e

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V

    iput-object v0, p0, Lb/c/b/b/d;->e:Ljava/util/concurrent/ThreadPoolExecutor;

    iget-object v0, p0, Lb/c/b/b/d;->e:Ljava/util/concurrent/ThreadPoolExecutor;

    iget-object v1, p0, Lb/c/b/b/d;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lb/c/b/b/d;->e:Ljava/util/concurrent/ThreadPoolExecutor;

    iget-object v1, p0, Lb/c/b/b/d;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a()Lb/c/b/b/d;
    .locals 2

    sget-object v0, Lb/c/b/b/d;->d:Lb/c/b/b/d;

    if-nez v0, :cond_1

    const-class v0, Lb/c/b/b/d;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lb/c/b/b/d;->d:Lb/c/b/b/d;

    if-nez v1, :cond_0

    new-instance v1, Lb/c/b/b/d;

    invoke-direct {v1}, Lb/c/b/b/d;-><init>()V

    sput-object v1, Lb/c/b/b/d;->d:Lb/c/b/b/d;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lb/c/b/b/d;->d:Lb/c/b/b/d;

    return-object v0
.end method

.method static synthetic a(Lb/c/b/b/d;)Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 0

    iget-object p0, p0, Lb/c/b/b/d;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object p0
.end method

.method static synthetic b(Lb/c/b/b/d;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 0

    iget-object p0, p0, Lb/c/b/b/d;->e:Ljava/util/concurrent/ThreadPoolExecutor;

    return-object p0
.end method

.method static synthetic c(Lb/c/b/b/d;)Ljava/util/concurrent/DelayQueue;
    .locals 0

    iget-object p0, p0, Lb/c/b/b/d;->g:Ljava/util/concurrent/DelayQueue;

    return-object p0
.end method


# virtual methods
.method public a(Lb/c/b/c/g;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lb/c/b/b/d;->g:Ljava/util/concurrent/DelayQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/DelayQueue;->put(Ljava/util/concurrent/Delayed;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lb/c/b/b/d;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
