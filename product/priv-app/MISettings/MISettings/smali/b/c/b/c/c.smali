.class public Lb/c/b/c/c;
.super Ljava/lang/Object;

# interfaces
.implements Lb/c/b/c/i;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lb/c/b/c/a$a;

.field private d:Lb/c/b/c/j;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lb/c/b/c/a$a;->a:Lb/c/b/c/a$a;

    iput-object v0, p0, Lb/c/b/c/c;->c:Lb/c/b/c/a$a;

    return-void
.end method


# virtual methods
.method public a(Lb/c/b/c/a$a;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lb/c/b/c/c;->c:Lb/c/b/c/a$a;

    :cond_0
    return-void
.end method

.method public a(Lb/c/b/c/j;)V
    .locals 0

    iput-object p1, p0, Lb/c/b/c/c;->d:Lb/c/b/c/j;

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lb/c/b/c/c;->b:Ljava/util/Map;

    return-void
.end method

.method public execute()V
    .locals 4

    iget-object v0, p0, Lb/c/b/c/c;->d:Lb/c/b/c/j;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    sget-object v1, Lb/c/b/c/b;->a:[I

    iget-object v2, p0, Lb/c/b/c/c;->c:Lb/c/b/c/a$a;

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    new-instance v0, Lb/c/b/c/a;

    invoke-direct {v0}, Lb/c/b/c/a;-><init>()V

    invoke-static {}, Lb/c/b/f;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lb/c/b/c/c;->a:Ljava/lang/String;

    iget-object v3, p0, Lb/c/b/c/c;->b:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3}, Lb/c/b/c/a;->b(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Lb/c/b/c/a;

    invoke-direct {v0}, Lb/c/b/c/a;-><init>()V

    invoke-static {}, Lb/c/b/f;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lb/c/b/c/c;->a:Ljava/lang/String;

    iget-object v3, p0, Lb/c/b/c/c;->b:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3}, Lb/c/b/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "execute: result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Niel-TestNet"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lb/c/b/c/c;->d:Lb/c/b/c/j;

    invoke-interface {v0}, Lb/c/b/c/j;->a()V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lb/c/b/c/c;->d:Lb/c/b/c/j;

    invoke-interface {v1, v0}, Lb/c/b/c/j;->a(Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lb/c/b/c/c;->a:Ljava/lang/String;

    return-void
.end method
