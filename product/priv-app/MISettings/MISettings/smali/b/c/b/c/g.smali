.class public Lb/c/b/c/g;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;
.implements Ljava/util/concurrent/Delayed;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lb/c/b/a/a;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;",
        "Ljava/util/concurrent/Delayed;"
    }
.end annotation


# instance fields
.field private a:Lb/c/b/c/i;

.field private b:I

.field private c:I

.field private d:J

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;Lb/c/b/c/a$a;Lb/c/b/c/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lb/c/b/c/a$a;",
            "Lb/c/b/c/h<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lb/c/b/c/g;->b:I

    const/4 v0, 0x2

    iput v0, p0, Lb/c/b/c/g;->c:I

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lb/c/b/c/g;->e:Landroid/os/Handler;

    new-instance v0, Lb/c/b/c/c;

    invoke-direct {v0}, Lb/c/b/c/c;-><init>()V

    iput-object v0, p0, Lb/c/b/c/g;->a:Lb/c/b/c/i;

    iget-object v0, p0, Lb/c/b/c/g;->a:Lb/c/b/c/i;

    invoke-interface {v0, p1}, Lb/c/b/c/i;->setUrl(Ljava/lang/String;)V

    iget-object p1, p0, Lb/c/b/c/g;->a:Lb/c/b/c/i;

    invoke-interface {p1, p2}, Lb/c/b/c/i;->a(Ljava/util/Map;)V

    iget-object p1, p0, Lb/c/b/c/g;->a:Lb/c/b/c/i;

    invoke-interface {p1, p3}, Lb/c/b/c/i;->a(Lb/c/b/c/a$a;)V

    iget-object p1, p0, Lb/c/b/c/g;->a:Lb/c/b/c/i;

    new-instance p2, Lb/c/b/c/f;

    invoke-direct {p2, p0, p4}, Lb/c/b/c/f;-><init>(Lb/c/b/c/g;Lb/c/b/c/h;)V

    invoke-interface {p1, p2}, Lb/c/b/c/i;->a(Lb/c/b/c/j;)V

    return-void
.end method

.method static synthetic a(Lb/c/b/c/g;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lb/c/b/c/g;->e:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic b(Lb/c/b/c/g;)I
    .locals 0

    iget p0, p0, Lb/c/b/c/g;->b:I

    return p0
.end method

.method private b()V
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0xbb8

    add-long/2addr v0, v2

    iput-wide v0, p0, Lb/c/b/c/g;->d:J

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lb/c/b/b/d;->a(Lb/c/b/c/g;)V

    iget v0, p0, Lb/c/b/c/g;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/c/b/c/g;->b:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onException: retry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lb/c/b/c/g;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Niel-TestNet"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic c(Lb/c/b/c/g;)I
    .locals 0

    iget p0, p0, Lb/c/b/c/g;->c:I

    return p0
.end method

.method static synthetic d(Lb/c/b/c/g;)V
    .locals 0

    invoke-direct {p0}, Lb/c/b/c/g;->b()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/concurrent/Delayed;)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public a()V
    .locals 1

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lb/c/b/c/g;->c:I

    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Ljava/util/concurrent/Delayed;

    invoke-virtual {p0, p1}, Lb/c/b/c/g;->a(Ljava/util/concurrent/Delayed;)I

    move-result p1

    return p1
.end method

.method public getDelay(Ljava/util/concurrent/TimeUnit;)J
    .locals 4

    iget-wide v0, p0, Lb/c/b/c/g;->d:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public run()V
    .locals 1

    iget-object v0, p0, Lb/c/b/c/g;->a:Lb/c/b/c/i;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lb/c/b/c/i;->execute()V

    :cond_0
    return-void
.end method
