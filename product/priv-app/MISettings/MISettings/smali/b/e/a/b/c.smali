.class public Lb/e/a/b/c;
.super Ljava/lang/Object;


# static fields
.field private static volatile a:Lb/a/b/q;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()Lb/a/b/q;
    .locals 5

    sget-object v0, Lb/e/a/b/c;->a:Lb/a/b/q;

    if-nez v0, :cond_1

    const-class v0, Lb/e/a/b/c;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lb/e/a/b/c;->a:Lb/a/b/q;

    if-nez v1, :cond_0

    new-instance v1, Lb/a/b/r;

    invoke-direct {v1}, Lb/a/b/r;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0x80

    aput v4, v2, v3

    invoke-virtual {v1, v2}, Lb/a/b/r;->a([I)Lb/a/b/r;

    invoke-virtual {v1}, Lb/a/b/r;->a()Lb/a/b/q;

    move-result-object v1

    sput-object v1, Lb/e/a/b/c;->a:Lb/a/b/q;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lb/e/a/b/c;->a:Lb/a/b/q;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    :try_start_0
    invoke-static {}, Lb/e/a/b/c;->a()Lb/a/b/q;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lb/a/b/q;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    const-string p1, "GsonUtils"

    const-string v0, "fromJson: "

    invoke-static {p1, v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-object v1
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-static {}, Lb/e/a/b/c;->a()Lb/a/b/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lb/a/b/q;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    const-string v0, "GsonUtils"

    const-string v1, "toJson: "

    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string p0, ""

    return-object p0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-static {}, Lb/e/a/b/c;->a()Lb/a/b/q;

    move-result-object v1

    new-instance v2, Lb/e/a/b/a;

    invoke-direct {v2, p1}, Lb/e/a/b/a;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v1, p0, v2}, Lb/a/b/q;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string p1, "GsonUtils"

    const-string v1, "fromJson: "

    invoke-static {p1, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object p0, v0

    :goto_0
    return-object p0
.end method
