.class final Lb/e/a/a/d;
.super Ljava/lang/Object;

# interfaces
.implements Lb/e/a/a/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lb/e/a/a/e;->a(Lb/e/a/a/h;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lb/e/a/a/h;


# direct methods
.method constructor <init>(Lb/e/a/a/h;)V
    .locals 0

    iput-object p1, p0, Lb/e/a/a/d;->a:Lb/e/a/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/accounts/AccountManagerFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    const-string v0, ""

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    const-string v2, "authtoken"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/Bundle;

    const-string v1, "intent"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/content/Intent;

    if-eqz p1, :cond_0

    sget-boolean v1, Lb/e/a/a/e;->a:Z

    if-eqz v1, :cond_0

    const/high16 v1, 0x10000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {}, Lb/e/a/a;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p1

    invoke-virtual {p1}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    :cond_1
    :goto_0
    sget-object p1, Lb/e/a/a/e;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "extTokenStr:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    return-void

    :cond_2
    invoke-static {v0}, Lc/a/a/b/i;->a(Ljava/lang/Object;)Lc/a/a/b/i;

    move-result-object p1

    invoke-static {}, Lc/a/a/i/b;->b()Lc/a/a/b/h;

    move-result-object v0

    invoke-virtual {p1, v0}, Lc/a/a/b/i;->a(Lc/a/a/b/h;)Lc/a/a/b/i;

    move-result-object p1

    new-instance v0, Lb/e/a/a/b;

    invoke-direct {v0, p0}, Lb/e/a/a/b;-><init>(Lb/e/a/a/d;)V

    new-instance v1, Lb/e/a/a/c;

    invoke-direct {v1, p0}, Lb/e/a/a/c;-><init>(Lb/e/a/a/d;)V

    invoke-virtual {p1, v0, v1}, Lc/a/a/b/i;->a(Lc/a/a/e/d;Lc/a/a/e/d;)Lc/a/a/c/b;

    return-void
.end method
