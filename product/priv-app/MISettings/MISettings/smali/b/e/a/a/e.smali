.class public Lb/e/a/a/e;
.super Ljava/lang/Object;


# static fields
.field public static a:Z

.field public static final b:Ljava/lang/String;

.field private static c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lb/e/a/a/e;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_log"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/e/a/a/e;->b:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lb/e/a/a/e;->c:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .locals 3

    invoke-static {}, Lb/e/a/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    const-string v1, ""

    const-string v2, "account_auth_token"

    invoke-virtual {v0, v2, v1}, Lb/e/a/b/h;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lb/e/a/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    const-string v2, "account_service_token"

    invoke-virtual {v0, v2, v1}, Lb/e/a/b/h;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lb/e/a/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    const-string v2, "account_user_id"

    invoke-virtual {v0, v2, v1}, Lb/e/a/b/h;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lb/e/a/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    const-string v2, "account_security"

    invoke-virtual {v0, v2, v1}, Lb/e/a/b/h;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Lb/e/a/a/g;)V
    .locals 8

    sget-object v0, Lb/e/a/a/e;->b:Ljava/lang/String;

    const-string v1, "getAuthToken"

    invoke-static {v0, v1}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lb/e/a/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lb/e/a/a/e;->c()Landroid/accounts/Account;

    move-result-object v2

    sget-object v1, Lb/e/a/a/e;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAuthToken xiaomiSystemAccount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lb/e/a/a/a;

    invoke-direct {v6, p0}, Lb/e/a/a/a;-><init>(Lb/e/a/a/g;)V

    const/4 v7, 0x0

    const-string v3, "miuikite"

    invoke-virtual/range {v1 .. v7}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    :cond_0
    return-void
.end method

.method public static a(Lb/e/a/a/h;)V
    .locals 2

    sget-object v0, Lb/e/a/a/e;->b:Ljava/lang/String;

    const-string v1, "getToken witch Callback"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lb/e/a/a/d;

    invoke-direct {v0, p0}, Lb/e/a/a/d;-><init>(Lb/e/a/a/h;)V

    invoke-static {v0}, Lb/e/a/a/e;->a(Lb/e/a/a/g;)V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lb/e/a/a/e;->b(Ljava/lang/String;)V

    return-void
.end method

.method public static b()Landroid/accounts/AccountManager;
    .locals 1

    invoke-static {}, Lb/e/a/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)V
    .locals 5

    invoke-static {}, Lb/e/a/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v1

    const-string v2, "account_auth_token"

    invoke-virtual {v1, v2, p0}, Lb/e/a/b/h;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lb/e/a/a/f;->a(Ljava/lang/String;)Lb/e/a/a/f;

    move-result-object p0

    invoke-static {}, Lb/e/a/a/e;->c()Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lb/e/a/a/e;->b()Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "encrypted_user_id"

    invoke-virtual {v2, v1, v3}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lb/e/a/a/e;->a:Z

    if-eqz v2, :cond_1

    sget-object v2, Lb/e/a/a/e;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "extToken:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/misettings/common/utils/h;->a()Lb/a/b/q;

    move-result-object v4

    invoke-virtual {v4, p0}, Lb/a/b/q;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lb/e/a/a/e;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "authToken:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lb/e/a/a/f;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lb/e/a/a/e;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "userId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lb/e/a/a/e;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "security:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lb/e/a/a/f;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v2

    iget-object v3, p0, Lb/e/a/a/f;->a:Ljava/lang/String;

    const-string v4, "account_service_token"

    invoke-virtual {v2, v4, v3}, Lb/e/a/b/h;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v2

    const-string v3, "account_user_id"

    invoke-virtual {v2, v3, v1}, Lb/e/a/b/h;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    iget-object p0, p0, Lb/e/a/a/f;->b:Ljava/lang/String;

    const-string v1, "account_security"

    invoke-virtual {v0, v1, p0}, Lb/e/a/b/h;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static c()Landroid/accounts/Account;
    .locals 2

    invoke-static {}, Lb/e/a/a/e;->b()Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.xiaomi"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public static d()V
    .locals 4

    invoke-static {}, Lb/e/a/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    const-string v1, "account_auth_token"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lb/e/a/b/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lb/e/a/a/e;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "old serviceToken: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lb/e/a/a;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.xiaomi"

    invoke-virtual {v1, v2, v0}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static e()Z
    .locals 3

    invoke-static {}, Lb/e/a/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    const-string v1, "account_auth_token"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lb/e/a/b/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
