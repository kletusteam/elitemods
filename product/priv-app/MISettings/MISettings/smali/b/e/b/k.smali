.class final Lb/e/b/k;
.super Ljava/lang/Object;

# interfaces
.implements Lc/a/a/e/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lc/a/a/e/d<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lb/e/b/d/e;

.field final synthetic b:Lb/e/b/b/a;


# direct methods
.method constructor <init>(Lb/e/b/d/e;Lb/e/b/b/a;)V
    .locals 0

    iput-object p1, p0, Lb/e/b/k;->a:Lb/e/b/d/e;

    iput-object p2, p0, Lb/e/b/k;->b:Lb/e/b/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lb/e/b/k;->a:Lb/e/b/d/e;

    invoke-interface {v1}, Lb/e/b/d/e;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb/e/b/k;->a:Lb/e/b/d/e;

    invoke-interface {v1}, Lb/e/b/d/e;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lb/e/b/k;->b:Lb/e/b/b/a;

    invoke-interface {v0, p1}, Lb/e/b/b/a;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lb/e/b/k;->a(Ljava/lang/Throwable;)V

    return-void
.end method
