.class public Lb/e/b/e/b;
.super Lb/e/b/e/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Lb/e/b/e/a<",
        "TR;>;"
    }
.end annotation


# static fields
.field private static final a:Lb/a/b/q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lb/a/b/r;

    invoke-direct {v0}, Lb/a/b/r;-><init>()V

    invoke-virtual {v0}, Lb/a/b/r;->a()Lb/a/b/q;

    move-result-object v0

    sput-object v0, Lb/e/b/e/b;->a:Lb/a/b/q;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lb/e/b/e/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TR;>;)TR;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lb/e/b/c/c;
        }
    .end annotation

    :try_start_0
    sget-object v0, Lb/e/b/e/b;->a:Lb/a/b/q;

    invoke-virtual {v0, p1, p2}, Lb/a/b/q;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Lb/a/b/E; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->printStackTrace()V

    new-instance p2, Lb/e/b/c/c;

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lb/e/b/c/c;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method
