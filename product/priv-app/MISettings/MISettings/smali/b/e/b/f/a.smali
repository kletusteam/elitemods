.class public abstract Lb/e/b/f/a;
.super Ljava/lang/Object;


# instance fields
.field protected final a:Le/I$a;


# direct methods
.method public constructor <init>(Lb/e/b/d/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Le/I$a;

    invoke-direct {v0}, Le/I$a;-><init>()V

    iput-object v0, p0, Lb/e/b/f/a;->a:Le/I$a;

    invoke-virtual {p0, p1}, Lb/e/b/f/a;->c(Lb/e/b/d/e;)V

    invoke-virtual {p0, p1}, Lb/e/b/f/a;->b(Lb/e/b/d/e;)V

    invoke-virtual {p0, p1}, Lb/e/b/f/a;->a(Lb/e/b/d/e;)V

    invoke-direct {p0}, Lb/e/b/f/a;->b()V

    invoke-direct {p0, p1}, Lb/e/b/f/a;->d(Lb/e/b/d/e;)V

    return-void
.end method

.method private b()V
    .locals 6

    invoke-static {}, Lb/e/b/m;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    const-string v1, ""

    const-string v2, "account_service_token"

    invoke-virtual {v0, v2, v1}, Lb/e/a/b/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lb/e/b/m;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v2

    const-string v3, "account_user_id"

    invoke-virtual {v2, v3, v1}, Lb/e/a/b/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lb/e/b/f/a;->a:Le/I$a;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "serviceToken="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "cookie"

    invoke-virtual {v2, v4, v3}, Le/I$a;->a(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    iget-object v2, p0, Lb/e/b/f/a;->a:Le/I$a;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cUserId="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Le/I$a;->a(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "commonCookies:   serviceToken="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Net_log =>"

    invoke-static {v2, v0}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "commonCookies:   cUserId="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private d(Lb/e/b/d/e;)V
    .locals 6

    invoke-interface {p1}, Lb/e/b/d/e;->getCookies()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Lb/e/b/d/e;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " manualCookies:   "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "Net_log =>"

    invoke-static {v5, v3}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lb/e/b/f/a;->a:Le/I$a;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cookie"

    invoke-virtual {v3, v2, v1}, Le/I$a;->a(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method


# virtual methods
.method public a()Le/I;
    .locals 1

    iget-object v0, p0, Lb/e/b/f/a;->a:Le/I$a;

    invoke-virtual {v0}, Le/I$a;->a()Le/I;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lb/e/b/d/e;)V
    .locals 5

    invoke-interface {p1}, Lb/e/b/d/e;->getHeader()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Lb/e/b/d/e;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " additionalHeader:   "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Net_log =>"

    invoke-static {v4, v3}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lb/e/b/f/a;->a:Le/I$a;

    invoke-virtual {v3, v2, v1}, Le/I$a;->a(Ljava/lang/String;Ljava/lang/String;)Le/I$a;

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method protected b(Lb/e/b/d/e;)V
    .locals 0

    return-void
.end method

.method protected c(Lb/e/b/d/e;)V
    .locals 2

    iget-object v0, p0, Lb/e/b/f/a;->a:Le/I$a;

    invoke-interface {p1}, Lb/e/b/d/e;->isAddParams()Z

    move-result v1

    invoke-static {p1, v1}, Lb/e/b/e;->a(Lb/e/b/d/e;Z)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Le/I$a;->b(Ljava/lang/String;)Le/I$a;

    return-void
.end method
