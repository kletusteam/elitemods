.class public abstract Lb/e/b/d/b;
.super Ljava/lang/Object;

# interfaces
.implements Lb/e/b/d/e;


# instance fields
.field private transient retriedTime:I

.field private transient tag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lb/e/b/d/b;->retriedTime:I

    return-void
.end method


# virtual methods
.method public addRetriedTime()V
    .locals 1

    iget v0, p0, Lb/e/b/d/b;->retriedTime:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/e/b/d/b;->retriedTime:I

    return-void
.end method

.method public canRetry()Z
    .locals 2

    iget v0, p0, Lb/e/b/d/b;->retriedTime:I

    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getBody()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCookies()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeader()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    const-string v0, "https://api.kite.miui.com"

    return-object v0
.end method

.method public getParser()Lb/e/b/e/a;
    .locals 1

    new-instance v0, Lb/e/b/e/b;

    invoke-direct {v0}, Lb/e/b/e/b;-><init>()V

    return-object v0
.end method

.method public getRetryCode()I
    .locals 1

    const/16 v0, 0x191

    return v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lb/e/b/d/b;->tag:Ljava/lang/String;

    return-object v0
.end method

.method public isAddParams()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setTag(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lb/e/b/d/b;->tag:Ljava/lang/String;

    return-void
.end method
