.class public Lb/e/b/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/e/b/c$a;
    }
.end annotation


# static fields
.field private static a:I = 0xa00000

.field private static b:I = 0xa

.field private static c:I = 0xa

.field private static d:I = 0xa

.field private static volatile e:Lb/e/b/c;


# instance fields
.field private f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lb/e/b/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lb/e/b/c$a;

.field private h:Le/F;

.field private i:Landroid/content/Context;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lb/e/b/m;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/e/b/c;->a(Landroid/content/Context;)V

    return-void
.end method

.method public static a()Lb/e/b/c;
    .locals 2

    sget-object v0, Lb/e/b/c;->e:Lb/e/b/c;

    if-nez v0, :cond_1

    const-class v0, Lb/e/b/c;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lb/e/b/c;->e:Lb/e/b/c;

    if-nez v1, :cond_0

    new-instance v1, Lb/e/b/c;

    invoke-direct {v1}, Lb/e/b/c;-><init>()V

    sput-object v1, Lb/e/b/c;->e:Lb/e/b/c;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lb/e/b/c;->e:Lb/e/b/c;

    return-object v0
.end method

.method static synthetic a(Lb/e/b/c;Lb/e/b/d/e;Le/L;Lb/e/b/b/b;Lb/e/b/b/a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lb/e/b/c;->a(Lb/e/b/d/e;Le/L;Lb/e/b/b/b;Lb/e/b/b/a;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private a(Lb/e/b/d/e;Le/L;Lb/e/b/b/b;Lb/e/b/b/a;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lb/e/b/d/e;",
            "Le/L;",
            "Lb/e/b/b/b<",
            "TR;>;",
            "Lb/e/b/b/a;",
            ")TR;"
        }
    .end annotation

    const/4 v0, 0x0

    const-string v1, "Net_log =>"

    if-eqz p1, :cond_b

    if-eqz p2, :cond_b

    invoke-virtual {p2}, Le/L;->a()Le/N;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {p2}, Le/L;->a()Le/N;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    goto/16 :goto_4

    :cond_0
    :try_start_0
    invoke-virtual {p2}, Le/L;->a()Le/N;

    move-result-object v2

    invoke-virtual {v2}, Le/N;->q()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "code"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Le/L;->q()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Lb/e/b/d/e;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lb/e/b/d/e;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\" => response = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "null"

    goto :goto_0

    :cond_1
    move-object v4, v2

    :goto_0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v3

    goto :goto_1

    :catch_1
    move-exception v3

    move-object v2, v0

    :goto_1
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    :goto_2
    invoke-virtual {p2}, Le/L;->q()I

    move-result v3

    const/16 v4, 0x191

    const-string v5, "{}"

    if-ne v3, v4, :cond_2

    move-object v2, v5

    :cond_2
    invoke-virtual {p2}, Le/L;->q()I

    move-result v3

    const/16 v4, 0x3ed

    if-ne v3, v4, :cond_3

    invoke-direct {p0}, Lb/e/b/c;->b()V

    move-object v2, v5

    :cond_3
    invoke-interface {p1}, Lb/e/b/d/e;->getParser()Lb/e/b/e/a;

    move-result-object v3

    invoke-interface {p1}, Lb/e/b/d/e;->getResultClass()Ljava/lang/Class;

    move-result-object v5

    :try_start_2
    invoke-virtual {v3, v2, v5}, Lb/e/b/e/a;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    if-eqz p3, :cond_9

    if-eqz v0, :cond_8

    invoke-virtual {p2}, Le/L;->q()I

    move-result v3

    const/16 v5, 0xc8

    if-lt v3, v5, :cond_4

    const/16 v5, 0x12b

    if-le v3, v5, :cond_7

    :cond_4
    instance-of v5, v0, Lb/e/b/g/a;

    if-eqz v5, :cond_7

    new-instance v5, Lb/e/b/c/b;

    move-object v6, v0

    check-cast v6, Lb/e/b/g/a;

    iget v6, v6, Lb/e/b/g/a;->code:I

    invoke-direct {v5, v2, v3, v6}, Lb/e/b/c/b;-><init>(Ljava/lang/String;II)V

    invoke-interface {p3, v5}, Lb/e/b/b/b;->a(Lb/e/b/c/a;)V

    invoke-virtual {p2}, Le/L;->q()I

    move-result v2

    invoke-interface {p1}, Lb/e/b/d/e;->getRetryCode()I

    move-result v3

    if-eq v2, v3, :cond_5

    invoke-virtual {p2}, Le/L;->q()I

    move-result p2

    if-ne p2, v4, :cond_6

    :cond_5
    invoke-interface {p1}, Lb/e/b/d/e;->canRetry()Z

    move-result p2

    if-eqz p2, :cond_6

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Lb/e/b/d/e;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " => retry "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v1, p2}, Lb/e/a/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object p2, v0

    check-cast p2, Lb/e/b/g/a;

    iget p2, p2, Lb/e/b/g/a;->code:I

    invoke-static {p1, p4, p2}, Lb/e/b/e;->a(Lb/e/b/d/e;Lb/e/b/b/a;I)V

    :cond_6
    return-object v0

    :cond_7
    invoke-interface {p3, v0}, Lb/e/b/b/b;->a(Ljava/lang/Object;)V

    goto :goto_3

    :cond_8
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Lb/e/b/d/e;->getTag()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p4, " response failed: result is null\uff01\uff01"

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v1, p2}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance p2, Lb/e/b/c/b;

    const-string p4, "result is null\uff01\uff01"

    invoke-direct {p2, p4}, Lb/e/b/c/b;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, p2}, Lb/e/b/b/b;->a(Lb/e/b/c/a;)V

    iget-object p2, p0, Lb/e/b/c;->g:Lb/e/b/c$a;

    if-eqz p2, :cond_9

    iget-object p2, p0, Lb/e/b/c;->g:Lb/e/b/c$a;

    invoke-interface {p2}, Lb/e/b/c$a;->a()V
    :try_end_2
    .catch Lb/e/b/c/c; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_9
    :goto_3
    return-object v0

    :catch_2
    move-exception p2

    :try_start_3
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Lb/e/b/d/e;->getTag()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " response failed: "

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p3, :cond_a

    invoke-interface {p3, p2}, Lb/e/b/b/b;->a(Lb/e/b/c/a;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    :cond_a
    return-object v0

    :cond_b
    :goto_4
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Lb/e/b/d/e;->getTag()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " null == param || null == response || null == response.body() || null == response.body().toString()"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lb/e/a/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic a(Lb/e/b/c;Lb/e/b/d/e;Ljava/lang/String;Lb/e/b/b/b;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lb/e/b/c;->a(Lb/e/b/d/e;Ljava/lang/String;Lb/e/b/b/b;)V

    return-void
.end method

.method private a(Lb/e/b/d/e;Ljava/lang/String;Lb/e/b/b/b;)V
    .locals 1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p2, "failed, no message!!!!"

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Lb/e/b/d/e;->getTag()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " response failed: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Net_log =>"

    invoke-static {v0, p1}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance p1, Lb/e/b/c/b;

    invoke-direct {p1, p2}, Lb/e/b/c/b;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, p1}, Lb/e/b/b/b;->a(Lb/e/b/c/a;)V

    return-void
.end method

.method private b()V
    .locals 3

    :try_start_0
    invoke-static {}, Lb/e/b/m;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lb/e/b/m;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "green_kid_device_id"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "HttpUtils"

    const-string v2, "clearDeviceId error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method private c()Le/F;
    .locals 5

    iget-object v0, p0, Lb/e/b/c;->h:Le/F;

    if-nez v0, :cond_0

    new-instance v0, Le/F$a;

    invoke-direct {v0}, Le/F$a;-><init>()V

    sget v1, Lb/e/b/c;->b:I

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Le/F$a;->a(JLjava/util/concurrent/TimeUnit;)Le/F$a;

    sget v1, Lb/e/b/c;->c:I

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Le/F$a;->c(JLjava/util/concurrent/TimeUnit;)Le/F$a;

    sget v1, Lb/e/b/c;->d:I

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Le/F$a;->b(JLjava/util/concurrent/TimeUnit;)Le/F$a;

    new-instance v1, Le/e;

    iget-object v2, p0, Lb/e/b/c;->i:Landroid/content/Context;

    const-string v3, "greenguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    sget v3, Lb/e/b/c;->a:I

    int-to-long v3, v3

    invoke-direct {v1, v2, v3, v4}, Le/e;-><init>(Ljava/io/File;J)V

    invoke-virtual {v0, v1}, Le/F$a;->a(Le/e;)Le/F$a;

    invoke-virtual {v0}, Le/F$a;->a()Le/F;

    move-result-object v0

    iput-object v0, p0, Lb/e/b/c;->h:Le/F;

    :cond_0
    iget-object v0, p0, Lb/e/b/c;->h:Le/F;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lb/e/b/c;->i:Landroid/content/Context;

    invoke-direct {p0}, Lb/e/b/c;->c()Le/F;

    move-result-object p1

    iput-object p1, p0, Lb/e/b/c;->h:Le/F;

    return-void
.end method

.method public a(Lb/e/b/d/e;Lb/e/b/b/b;Lb/e/b/b/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/e/b/d/e;",
            "Lb/e/b/b/b;",
            "Lb/e/b/b/a<",
            "*>;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lb/e/b/c;->f:Ljava/lang/ref/WeakReference;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    rem-int/lit16 v2, v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "%04d"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lb/e/b/d/e;->setTag(Ljava/lang/String;)V

    invoke-static {p1}, Lb/e/b/e;->c(Lb/e/b/d/e;)Le/I;

    move-result-object v0

    iget-object v1, p0, Lb/e/b/c;->h:Le/F;

    invoke-virtual {v1, v0}, Le/F;->a(Le/I;)Le/h;

    move-result-object v0

    new-instance v1, Lb/e/b/b;

    invoke-direct {v1, p0, p1, p2, p3}, Lb/e/b/b;-><init>(Lb/e/b/c;Lb/e/b/d/e;Lb/e/b/b/b;Lb/e/b/b/a;)V

    invoke-interface {v0, v1}, Le/h;->a(Le/i;)V

    return-void
.end method
