.class public abstract enum Lb/a/b/H;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lb/a/b/H;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lb/a/b/H;

.field public static final enum b:Lb/a/b/H;

.field private static final synthetic c:[Lb/a/b/H;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lb/a/b/F;

    const/4 v1, 0x0

    const-string v2, "DEFAULT"

    invoke-direct {v0, v2, v1}, Lb/a/b/F;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/H;->a:Lb/a/b/H;

    new-instance v0, Lb/a/b/G;

    const/4 v2, 0x1

    const-string v3, "STRING"

    invoke-direct {v0, v3, v2}, Lb/a/b/G;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/H;->b:Lb/a/b/H;

    const/4 v0, 0x2

    new-array v0, v0, [Lb/a/b/H;

    sget-object v3, Lb/a/b/H;->a:Lb/a/b/H;

    aput-object v3, v0, v1

    sget-object v1, Lb/a/b/H;->b:Lb/a/b/H;

    aput-object v1, v0, v2

    sput-object v0, Lb/a/b/H;->c:[Lb/a/b/H;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILb/a/b/F;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lb/a/b/H;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lb/a/b/H;
    .locals 1

    const-class v0, Lb/a/b/H;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lb/a/b/H;

    return-object p0
.end method

.method public static values()[Lb/a/b/H;
    .locals 1

    sget-object v0, Lb/a/b/H;->c:[Lb/a/b/H;

    invoke-virtual {v0}, [Lb/a/b/H;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lb/a/b/H;

    return-object v0
.end method
