.class Lb/a/b/a/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Lb/a/b/K;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb/a/b/a/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lb/a/b/q;Lb/a/b/b/a;)Lb/a/b/J;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lb/a/b/q;",
            "Lb/a/b/b/a<",
            "TT;>;)",
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p2}, Lb/a/b/b/a;->b()Ljava/lang/reflect/Type;

    move-result-object p2

    instance-of v0, p2, Ljava/lang/reflect/GenericArrayType;

    if-nez v0, :cond_1

    instance-of v0, p2, Ljava/lang/Class;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    return-object p1

    :cond_1
    invoke-static {p2}, Lb/a/b/a/b;->d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object p2

    invoke-static {p2}, Lb/a/b/b/a;->a(Ljava/lang/reflect/Type;)Lb/a/b/b/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lb/a/b/q;->a(Lb/a/b/b/a;)Lb/a/b/J;

    move-result-object v0

    new-instance v1, Lb/a/b/a/a/b;

    invoke-static {p2}, Lb/a/b/a/b;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object p2

    invoke-direct {v1, p1, v0, p2}, Lb/a/b/a/a/b;-><init>(Lb/a/b/q;Lb/a/b/J;Ljava/lang/Class;)V

    return-object v1
.end method
