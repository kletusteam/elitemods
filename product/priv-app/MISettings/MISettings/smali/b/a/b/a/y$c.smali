.class abstract Lb/a/b/a/y$c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb/a/b/a/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "TT;>;"
    }
.end annotation


# instance fields
.field a:Lb/a/b/a/y$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/a/y$d<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field b:Lb/a/b/a/y$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/a/y$d<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field c:I

.field final synthetic d:Lb/a/b/a/y;


# direct methods
.method constructor <init>(Lb/a/b/a/y;)V
    .locals 1

    iput-object p1, p0, Lb/a/b/a/y$c;->d:Lb/a/b/a/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object p1, p0, Lb/a/b/a/y$c;->d:Lb/a/b/a/y;

    iget-object v0, p1, Lb/a/b/a/y;->f:Lb/a/b/a/y$d;

    iget-object v0, v0, Lb/a/b/a/y$d;->d:Lb/a/b/a/y$d;

    iput-object v0, p0, Lb/a/b/a/y$c;->a:Lb/a/b/a/y$d;

    const/4 v0, 0x0

    iput-object v0, p0, Lb/a/b/a/y$c;->b:Lb/a/b/a/y$d;

    iget p1, p1, Lb/a/b/a/y;->e:I

    iput p1, p0, Lb/a/b/a/y$c;->c:I

    return-void
.end method


# virtual methods
.method final a()Lb/a/b/a/y$d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lb/a/b/a/y$d<",
            "TK;TV;>;"
        }
    .end annotation

    goto/32 :goto_6

    nop

    :goto_0
    new-instance v0, Ljava/util/ConcurrentModificationException;

    goto/32 :goto_e

    nop

    :goto_1
    new-instance v0, Ljava/util/NoSuchElementException;

    goto/32 :goto_12

    nop

    :goto_2
    iget-object v2, v1, Lb/a/b/a/y;->f:Lb/a/b/a/y$d;

    goto/32 :goto_c

    nop

    :goto_3
    iget v2, p0, Lb/a/b/a/y$c;->c:I

    goto/32 :goto_11

    nop

    :goto_4
    iget-object v1, v0, Lb/a/b/a/y$d;->d:Lb/a/b/a/y$d;

    goto/32 :goto_5

    nop

    :goto_5
    iput-object v1, p0, Lb/a/b/a/y$c;->a:Lb/a/b/a/y$d;

    goto/32 :goto_10

    nop

    :goto_6
    iget-object v0, p0, Lb/a/b/a/y$c;->a:Lb/a/b/a/y$d;

    goto/32 :goto_9

    nop

    :goto_7
    throw v0

    :goto_8
    goto/32 :goto_1

    nop

    :goto_9
    iget-object v1, p0, Lb/a/b/a/y$c;->d:Lb/a/b/a/y;

    goto/32 :goto_2

    nop

    :goto_a
    return-object v0

    :goto_b
    goto/32 :goto_0

    nop

    :goto_c
    if-ne v0, v2, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_d

    nop

    :goto_d
    iget v1, v1, Lb/a/b/a/y;->e:I

    goto/32 :goto_3

    nop

    :goto_e
    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    goto/32 :goto_7

    nop

    :goto_f
    throw v0

    :goto_10
    iput-object v0, p0, Lb/a/b/a/y$c;->b:Lb/a/b/a/y$d;

    goto/32 :goto_a

    nop

    :goto_11
    if-eq v1, v2, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_4

    nop

    :goto_12
    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    goto/32 :goto_f

    nop
.end method

.method public final hasNext()Z
    .locals 2

    iget-object v0, p0, Lb/a/b/a/y$c;->a:Lb/a/b/a/y$d;

    iget-object v1, p0, Lb/a/b/a/y$c;->d:Lb/a/b/a/y;

    iget-object v1, v1, Lb/a/b/a/y;->f:Lb/a/b/a/y$d;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final remove()V
    .locals 3

    iget-object v0, p0, Lb/a/b/a/y$c;->b:Lb/a/b/a/y$d;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lb/a/b/a/y$c;->d:Lb/a/b/a/y;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lb/a/b/a/y;->a(Lb/a/b/a/y$d;Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lb/a/b/a/y$c;->b:Lb/a/b/a/y$d;

    iget-object v0, p0, Lb/a/b/a/y$c;->d:Lb/a/b/a/y;

    iget v0, v0, Lb/a/b/a/y;->e:I

    iput v0, p0, Lb/a/b/a/y$c;->c:I

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method
