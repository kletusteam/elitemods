.class public final Lb/a/b/a/a/ja;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/a/b/a/a/ja$a;
    }
.end annotation


# static fields
.field public static final A:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final B:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field public static final C:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final D:Lb/a/b/K;

.field public static final E:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public static final F:Lb/a/b/K;

.field public static final G:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/StringBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public static final H:Lb/a/b/K;

.field public static final I:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/net/URL;",
            ">;"
        }
    .end annotation
.end field

.field public static final J:Lb/a/b/K;

.field public static final K:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field public static final L:Lb/a/b/K;

.field public static final M:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final N:Lb/a/b/K;

.field public static final O:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field public static final P:Lb/a/b/K;

.field public static final Q:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/util/Currency;",
            ">;"
        }
    .end annotation
.end field

.field public static final R:Lb/a/b/K;

.field public static final S:Lb/a/b/K;

.field public static final T:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field public static final U:Lb/a/b/K;

.field public static final V:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final W:Lb/a/b/K;

.field public static final X:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Lb/a/b/w;",
            ">;"
        }
    .end annotation
.end field

.field public static final Y:Lb/a/b/K;

.field public static final Z:Lb/a/b/K;

.field public static final a:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lb/a/b/K;

.field public static final c:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/util/BitSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lb/a/b/K;

.field public static final e:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lb/a/b/K;

.field public static final h:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lb/a/b/K;

.field public static final j:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Lb/a/b/K;

.field public static final l:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lb/a/b/K;

.field public static final n:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:Lb/a/b/K;

.field public static final p:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Lb/a/b/K;

.field public static final r:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/util/concurrent/atomic/AtomicIntegerArray;",
            ">;"
        }
    .end annotation
.end field

.field public static final s:Lb/a/b/K;

.field public static final t:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final u:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final v:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final w:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final x:Lb/a/b/K;

.field public static final y:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static final z:Lb/a/b/K;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lb/a/b/a/a/H;

    invoke-direct {v0}, Lb/a/b/a/a/H;-><init>()V

    invoke-virtual {v0}, Lb/a/b/J;->a()Lb/a/b/J;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->a:Lb/a/b/J;

    const-class v0, Ljava/lang/Class;

    sget-object v1, Lb/a/b/a/a/ja;->a:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->b:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/U;

    invoke-direct {v0}, Lb/a/b/a/a/U;-><init>()V

    invoke-virtual {v0}, Lb/a/b/J;->a()Lb/a/b/J;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->c:Lb/a/b/J;

    const-class v0, Ljava/util/BitSet;

    sget-object v1, Lb/a/b/a/a/ja;->c:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->d:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/ca;

    invoke-direct {v0}, Lb/a/b/a/a/ca;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->e:Lb/a/b/J;

    new-instance v0, Lb/a/b/a/a/da;

    invoke-direct {v0}, Lb/a/b/a/a/da;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->f:Lb/a/b/J;

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Boolean;

    sget-object v2, Lb/a/b/a/a/ja;->e:Lb/a/b/J;

    invoke-static {v0, v1, v2}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->g:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/ea;

    invoke-direct {v0}, Lb/a/b/a/a/ea;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->h:Lb/a/b/J;

    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Byte;

    sget-object v2, Lb/a/b/a/a/ja;->h:Lb/a/b/J;

    invoke-static {v0, v1, v2}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->i:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/fa;

    invoke-direct {v0}, Lb/a/b/a/a/fa;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->j:Lb/a/b/J;

    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Short;

    sget-object v2, Lb/a/b/a/a/ja;->j:Lb/a/b/J;

    invoke-static {v0, v1, v2}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->k:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/ga;

    invoke-direct {v0}, Lb/a/b/a/a/ga;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->l:Lb/a/b/J;

    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Integer;

    sget-object v2, Lb/a/b/a/a/ja;->l:Lb/a/b/J;

    invoke-static {v0, v1, v2}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->m:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/ha;

    invoke-direct {v0}, Lb/a/b/a/a/ha;-><init>()V

    invoke-virtual {v0}, Lb/a/b/J;->a()Lb/a/b/J;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->n:Lb/a/b/J;

    const-class v0, Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v1, Lb/a/b/a/a/ja;->n:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->o:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/ia;

    invoke-direct {v0}, Lb/a/b/a/a/ia;-><init>()V

    invoke-virtual {v0}, Lb/a/b/J;->a()Lb/a/b/J;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->p:Lb/a/b/J;

    const-class v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    sget-object v1, Lb/a/b/a/a/ja;->p:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->q:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/x;

    invoke-direct {v0}, Lb/a/b/a/a/x;-><init>()V

    invoke-virtual {v0}, Lb/a/b/J;->a()Lb/a/b/J;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->r:Lb/a/b/J;

    const-class v0, Ljava/util/concurrent/atomic/AtomicIntegerArray;

    sget-object v1, Lb/a/b/a/a/ja;->r:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->s:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/y;

    invoke-direct {v0}, Lb/a/b/a/a/y;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->t:Lb/a/b/J;

    new-instance v0, Lb/a/b/a/a/z;

    invoke-direct {v0}, Lb/a/b/a/a/z;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->u:Lb/a/b/J;

    new-instance v0, Lb/a/b/a/a/A;

    invoke-direct {v0}, Lb/a/b/a/a/A;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->v:Lb/a/b/J;

    new-instance v0, Lb/a/b/a/a/B;

    invoke-direct {v0}, Lb/a/b/a/a/B;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->w:Lb/a/b/J;

    const-class v0, Ljava/lang/Number;

    sget-object v1, Lb/a/b/a/a/ja;->w:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->x:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/C;

    invoke-direct {v0}, Lb/a/b/a/a/C;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->y:Lb/a/b/J;

    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Character;

    sget-object v2, Lb/a/b/a/a/ja;->y:Lb/a/b/J;

    invoke-static {v0, v1, v2}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->z:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/D;

    invoke-direct {v0}, Lb/a/b/a/a/D;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->A:Lb/a/b/J;

    new-instance v0, Lb/a/b/a/a/E;

    invoke-direct {v0}, Lb/a/b/a/a/E;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->B:Lb/a/b/J;

    new-instance v0, Lb/a/b/a/a/F;

    invoke-direct {v0}, Lb/a/b/a/a/F;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->C:Lb/a/b/J;

    const-class v0, Ljava/lang/String;

    sget-object v1, Lb/a/b/a/a/ja;->A:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->D:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/G;

    invoke-direct {v0}, Lb/a/b/a/a/G;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->E:Lb/a/b/J;

    const-class v0, Ljava/lang/StringBuilder;

    sget-object v1, Lb/a/b/a/a/ja;->E:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->F:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/I;

    invoke-direct {v0}, Lb/a/b/a/a/I;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->G:Lb/a/b/J;

    const-class v0, Ljava/lang/StringBuffer;

    sget-object v1, Lb/a/b/a/a/ja;->G:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->H:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/J;

    invoke-direct {v0}, Lb/a/b/a/a/J;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->I:Lb/a/b/J;

    const-class v0, Ljava/net/URL;

    sget-object v1, Lb/a/b/a/a/ja;->I:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->J:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/K;

    invoke-direct {v0}, Lb/a/b/a/a/K;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->K:Lb/a/b/J;

    const-class v0, Ljava/net/URI;

    sget-object v1, Lb/a/b/a/a/ja;->K:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->L:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/L;

    invoke-direct {v0}, Lb/a/b/a/a/L;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->M:Lb/a/b/J;

    const-class v0, Ljava/net/InetAddress;

    sget-object v1, Lb/a/b/a/a/ja;->M:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->b(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->N:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/M;

    invoke-direct {v0}, Lb/a/b/a/a/M;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->O:Lb/a/b/J;

    const-class v0, Ljava/util/UUID;

    sget-object v1, Lb/a/b/a/a/ja;->O:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->P:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/N;

    invoke-direct {v0}, Lb/a/b/a/a/N;-><init>()V

    invoke-virtual {v0}, Lb/a/b/J;->a()Lb/a/b/J;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->Q:Lb/a/b/J;

    const-class v0, Ljava/util/Currency;

    sget-object v1, Lb/a/b/a/a/ja;->Q:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->R:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/P;

    invoke-direct {v0}, Lb/a/b/a/a/P;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->S:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/Q;

    invoke-direct {v0}, Lb/a/b/a/a/Q;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->T:Lb/a/b/J;

    const-class v0, Ljava/util/Calendar;

    const-class v1, Ljava/util/GregorianCalendar;

    sget-object v2, Lb/a/b/a/a/ja;->T:Lb/a/b/J;

    invoke-static {v0, v1, v2}, Lb/a/b/a/a/ja;->b(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->U:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/S;

    invoke-direct {v0}, Lb/a/b/a/a/S;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->V:Lb/a/b/J;

    const-class v0, Ljava/util/Locale;

    sget-object v1, Lb/a/b/a/a/ja;->V:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->W:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/T;

    invoke-direct {v0}, Lb/a/b/a/a/T;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->X:Lb/a/b/J;

    const-class v0, Lb/a/b/w;

    sget-object v1, Lb/a/b/a/a/ja;->X:Lb/a/b/J;

    invoke-static {v0, v1}, Lb/a/b/a/a/ja;->b(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v0

    sput-object v0, Lb/a/b/a/a/ja;->Y:Lb/a/b/K;

    new-instance v0, Lb/a/b/a/a/V;

    invoke-direct {v0}, Lb/a/b/a/a/V;-><init>()V

    sput-object v0, Lb/a/b/a/a/ja;->Z:Lb/a/b/K;

    return-void
.end method

.method public static a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Lb/a/b/J<",
            "TTT;>;)",
            "Lb/a/b/K;"
        }
    .end annotation

    new-instance v0, Lb/a/b/a/a/W;

    invoke-direct {v0, p0, p1}, Lb/a/b/a/a/W;-><init>(Ljava/lang/Class;Lb/a/b/J;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Lb/a/b/J<",
            "-TTT;>;)",
            "Lb/a/b/K;"
        }
    .end annotation

    new-instance v0, Lb/a/b/a/a/X;

    invoke-direct {v0, p0, p1, p2}, Lb/a/b/a/a/X;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT1;>;",
            "Lb/a/b/J<",
            "TT1;>;)",
            "Lb/a/b/K;"
        }
    .end annotation

    new-instance v0, Lb/a/b/a/a/aa;

    invoke-direct {v0, p0, p1}, Lb/a/b/a/a/aa;-><init>(Ljava/lang/Class;Lb/a/b/J;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Ljava/lang/Class<",
            "+TTT;>;",
            "Lb/a/b/J<",
            "-TTT;>;)",
            "Lb/a/b/K;"
        }
    .end annotation

    new-instance v0, Lb/a/b/a/a/Y;

    invoke-direct {v0, p0, p1, p2}, Lb/a/b/a/a/Y;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)V

    return-object v0
.end method
