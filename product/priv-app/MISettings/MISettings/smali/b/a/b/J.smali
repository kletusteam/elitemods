.class public abstract Lb/a/b/J;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lb/a/b/J;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lb/a/b/I;

    invoke-direct {v0, p0}, Lb/a/b/I;-><init>(Lb/a/b/J;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Lb/a/b/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lb/a/b/w;"
        }
    .end annotation

    :try_start_0
    new-instance v0, Lb/a/b/a/a/j;

    invoke-direct {v0}, Lb/a/b/a/a/j;-><init>()V

    invoke-virtual {p0, v0, p1}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    invoke-virtual {v0}, Lb/a/b/a/a/j;->v()Lb/a/b/w;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Lb/a/b/x;

    invoke-direct {v0, p1}, Lb/a/b/x;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public abstract a(Lb/a/b/c/b;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/b;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract a(Lb/a/b/c/d;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/d;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
