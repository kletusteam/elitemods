.class public final Lb/a/b/a/a/b;
.super Lb/a/b/J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lb/a/b/J<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lb/a/b/K;


# instance fields
.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TE;>;"
        }
    .end annotation
.end field

.field private final c:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "TE;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lb/a/b/a/a/a;

    invoke-direct {v0}, Lb/a/b/a/a/a;-><init>()V

    sput-object v0, Lb/a/b/a/a/b;->a:Lb/a/b/K;

    return-void
.end method

.method public constructor <init>(Lb/a/b/q;Lb/a/b/J;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/q;",
            "Lb/a/b/J<",
            "TE;>;",
            "Ljava/lang/Class<",
            "TE;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lb/a/b/J;-><init>()V

    new-instance v0, Lb/a/b/a/a/w;

    invoke-direct {v0, p1, p2, p3}, Lb/a/b/a/a/w;-><init>(Lb/a/b/q;Lb/a/b/J;Ljava/lang/reflect/Type;)V

    iput-object v0, p0, Lb/a/b/a/a/b;->c:Lb/a/b/J;

    iput-object p3, p0, Lb/a/b/a/a/b;->b:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public a(Lb/a/b/c/b;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lb/a/b/c/b;->C()Lb/a/b/c/c;

    move-result-object v0

    sget-object v1, Lb/a/b/c/c;->i:Lb/a/b/c/c;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lb/a/b/c/b;->A()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lb/a/b/c/b;->a()V

    :goto_0
    invoke-virtual {p1}, Lb/a/b/c/b;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lb/a/b/a/a/b;->c:Lb/a/b/J;

    invoke-virtual {v1, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lb/a/b/c/b;->q()V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    iget-object v1, p0, Lb/a/b/a/a/b;->b:Ljava/lang/Class;

    invoke-static {v1, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v1, v2, v3}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method public a(Lb/a/b/c/d;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lb/a/b/c/d;->u()Lb/a/b/c/d;

    return-void

    :cond_0
    invoke-virtual {p1}, Lb/a/b/c/d;->a()Lb/a/b/c/d;

    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-static {p2, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lb/a/b/a/a/b;->c:Lb/a/b/J;

    invoke-virtual {v3, p1, v2}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lb/a/b/c/d;->p()Lb/a/b/c/d;

    return-void
.end method
