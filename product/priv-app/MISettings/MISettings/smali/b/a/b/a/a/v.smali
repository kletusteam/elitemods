.class public final Lb/a/b/a/a/v;
.super Lb/a/b/J;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/a/b/a/a/v$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lb/a/b/J<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lb/a/b/D;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/D<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Lb/a/b/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/v<",
            "TT;>;"
        }
    .end annotation
.end field

.field final c:Lb/a/b/q;

.field private final d:Lb/a/b/b/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/b/a<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final e:Lb/a/b/K;

.field private final f:Lb/a/b/a/a/v$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/a/a/v<",
            "TT;>.a;"
        }
    .end annotation
.end field

.field private g:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lb/a/b/D;Lb/a/b/v;Lb/a/b/q;Lb/a/b/b/a;Lb/a/b/K;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/D<",
            "TT;>;",
            "Lb/a/b/v<",
            "TT;>;",
            "Lb/a/b/q;",
            "Lb/a/b/b/a<",
            "TT;>;",
            "Lb/a/b/K;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lb/a/b/J;-><init>()V

    new-instance v0, Lb/a/b/a/a/v$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lb/a/b/a/a/v$a;-><init>(Lb/a/b/a/a/v;Lb/a/b/a/a/u;)V

    iput-object v0, p0, Lb/a/b/a/a/v;->f:Lb/a/b/a/a/v$a;

    iput-object p1, p0, Lb/a/b/a/a/v;->a:Lb/a/b/D;

    iput-object p2, p0, Lb/a/b/a/a/v;->b:Lb/a/b/v;

    iput-object p3, p0, Lb/a/b/a/a/v;->c:Lb/a/b/q;

    iput-object p4, p0, Lb/a/b/a/a/v;->d:Lb/a/b/b/a;

    iput-object p5, p0, Lb/a/b/a/a/v;->e:Lb/a/b/K;

    return-void
.end method

.method private b()Lb/a/b/J;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/a/a/v;->g:Lb/a/b/J;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lb/a/b/a/a/v;->c:Lb/a/b/q;

    iget-object v1, p0, Lb/a/b/a/a/v;->e:Lb/a/b/K;

    iget-object v2, p0, Lb/a/b/a/a/v;->d:Lb/a/b/b/a;

    invoke-virtual {v0, v1, v2}, Lb/a/b/q;->a(Lb/a/b/K;Lb/a/b/b/a;)Lb/a/b/J;

    move-result-object v0

    iput-object v0, p0, Lb/a/b/a/a/v;->g:Lb/a/b/J;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public a(Lb/a/b/c/b;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/b;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/a/a/v;->b:Lb/a/b/v;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lb/a/b/a/a/v;->b()Lb/a/b/J;

    move-result-object v0

    invoke-virtual {v0, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {p1}, Lb/a/b/a/D;->a(Lb/a/b/c/b;)Lb/a/b/w;

    move-result-object p1

    invoke-virtual {p1}, Lb/a/b/w;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    iget-object v0, p0, Lb/a/b/a/a/v;->b:Lb/a/b/v;

    iget-object v1, p0, Lb/a/b/a/a/v;->d:Lb/a/b/b/a;

    invoke-virtual {v1}, Lb/a/b/b/a;->b()Ljava/lang/reflect/Type;

    move-result-object v1

    iget-object v2, p0, Lb/a/b/a/a/v;->f:Lb/a/b/a/a/v$a;

    invoke-interface {v0, p1, v1, v2}, Lb/a/b/v;->a(Lb/a/b/w;Ljava/lang/reflect/Type;Lb/a/b/u;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public a(Lb/a/b/c/d;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/d;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/a/a/v;->a:Lb/a/b/D;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lb/a/b/a/a/v;->b()Lb/a/b/J;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    return-void

    :cond_0
    if-nez p2, :cond_1

    invoke-virtual {p1}, Lb/a/b/c/d;->u()Lb/a/b/c/d;

    return-void

    :cond_1
    iget-object v1, p0, Lb/a/b/a/a/v;->d:Lb/a/b/b/a;

    invoke-virtual {v1}, Lb/a/b/b/a;->b()Ljava/lang/reflect/Type;

    move-result-object v1

    iget-object v2, p0, Lb/a/b/a/a/v;->f:Lb/a/b/a/a/v$a;

    invoke-interface {v0, p2, v1, v2}, Lb/a/b/D;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lb/a/b/C;)Lb/a/b/w;

    move-result-object p2

    invoke-static {p2, p1}, Lb/a/b/a/D;->a(Lb/a/b/w;Lb/a/b/c/d;)V

    return-void
.end method
