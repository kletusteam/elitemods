.class Lb/a/b/a/r;
.super Lb/a/b/J;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lb/a/b/a/s;->a(Lb/a/b/q;Lb/a/b/b/a;)Lb/a/b/J;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lb/a/b/J<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private a:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation
.end field

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Lb/a/b/q;

.field final synthetic e:Lb/a/b/b/a;

.field final synthetic f:Lb/a/b/a/s;


# direct methods
.method constructor <init>(Lb/a/b/a/s;ZZLb/a/b/q;Lb/a/b/b/a;)V
    .locals 0

    iput-object p1, p0, Lb/a/b/a/r;->f:Lb/a/b/a/s;

    iput-boolean p2, p0, Lb/a/b/a/r;->b:Z

    iput-boolean p3, p0, Lb/a/b/a/r;->c:Z

    iput-object p4, p0, Lb/a/b/a/r;->d:Lb/a/b/q;

    iput-object p5, p0, Lb/a/b/a/r;->e:Lb/a/b/b/a;

    invoke-direct {p0}, Lb/a/b/J;-><init>()V

    return-void
.end method

.method private b()Lb/a/b/J;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/a/r;->a:Lb/a/b/J;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lb/a/b/a/r;->d:Lb/a/b/q;

    iget-object v1, p0, Lb/a/b/a/r;->f:Lb/a/b/a/s;

    iget-object v2, p0, Lb/a/b/a/r;->e:Lb/a/b/b/a;

    invoke-virtual {v0, v1, v2}, Lb/a/b/q;->a(Lb/a/b/K;Lb/a/b/b/a;)Lb/a/b/J;

    move-result-object v0

    iput-object v0, p0, Lb/a/b/a/r;->a:Lb/a/b/J;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public a(Lb/a/b/c/b;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/b;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lb/a/b/a/r;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lb/a/b/c/b;->D()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-direct {p0}, Lb/a/b/a/r;->b()Lb/a/b/J;

    move-result-object v0

    invoke-virtual {v0, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public a(Lb/a/b/c/d;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/d;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lb/a/b/a/r;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lb/a/b/c/d;->u()Lb/a/b/c/d;

    return-void

    :cond_0
    invoke-direct {p0}, Lb/a/b/a/r;->b()Lb/a/b/J;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    return-void
.end method
