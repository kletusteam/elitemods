.class final Lb/a/b/a/a/w;
.super Lb/a/b/J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lb/a/b/J<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lb/a/b/q;

.field private final b:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/reflect/Type;


# direct methods
.method constructor <init>(Lb/a/b/q;Lb/a/b/J;Ljava/lang/reflect/Type;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/q;",
            "Lb/a/b/J<",
            "TT;>;",
            "Ljava/lang/reflect/Type;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lb/a/b/J;-><init>()V

    iput-object p1, p0, Lb/a/b/a/a/w;->a:Lb/a/b/q;

    iput-object p2, p0, Lb/a/b/a/a/w;->b:Lb/a/b/J;

    iput-object p3, p0, Lb/a/b/a/a/w;->c:Ljava/lang/reflect/Type;

    return-void
.end method

.method private a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/reflect/Type;
    .locals 1

    if-eqz p2, :cond_1

    const-class v0, Ljava/lang/Object;

    if-eq p1, v0, :cond_0

    instance-of v0, p1, Ljava/lang/reflect/TypeVariable;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Class;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    :cond_1
    return-object p1
.end method


# virtual methods
.method public a(Lb/a/b/c/b;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/b;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/a/a/w;->b:Lb/a/b/J;

    invoke-virtual {v0, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public a(Lb/a/b/c/d;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/d;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/a/a/w;->b:Lb/a/b/J;

    iget-object v1, p0, Lb/a/b/a/a/w;->c:Ljava/lang/reflect/Type;

    invoke-direct {p0, v1, p2}, Lb/a/b/a/a/w;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/reflect/Type;

    move-result-object v1

    iget-object v2, p0, Lb/a/b/a/a/w;->c:Ljava/lang/reflect/Type;

    if-eq v1, v2, :cond_1

    iget-object v0, p0, Lb/a/b/a/a/w;->a:Lb/a/b/q;

    invoke-static {v1}, Lb/a/b/b/a;->a(Ljava/lang/reflect/Type;)Lb/a/b/b/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lb/a/b/q;->a(Lb/a/b/b/a;)Lb/a/b/J;

    move-result-object v0

    instance-of v1, v0, Lb/a/b/a/a/p$a;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lb/a/b/a/a/w;->b:Lb/a/b/J;

    instance-of v2, v1, Lb/a/b/a/a/p$a;

    if-nez v2, :cond_1

    move-object v0, v1

    :cond_1
    :goto_0
    invoke-virtual {v0, p1, p2}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    return-void
.end method
