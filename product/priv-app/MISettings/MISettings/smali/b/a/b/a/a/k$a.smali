.class final Lb/a/b/a/a/k$a;
.super Lb/a/b/J;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb/a/b/a/a/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lb/a/b/J<",
        "Ljava/util/Map<",
        "TK;TV;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "TK;>;"
        }
    .end annotation
.end field

.field private final b:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "TV;>;"
        }
    .end annotation
.end field

.field private final c:Lb/a/b/a/A;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/a/A<",
            "+",
            "Ljava/util/Map<",
            "TK;TV;>;>;"
        }
    .end annotation
.end field

.field final synthetic d:Lb/a/b/a/a/k;


# direct methods
.method public constructor <init>(Lb/a/b/a/a/k;Lb/a/b/q;Ljava/lang/reflect/Type;Lb/a/b/J;Ljava/lang/reflect/Type;Lb/a/b/J;Lb/a/b/a/A;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/q;",
            "Ljava/lang/reflect/Type;",
            "Lb/a/b/J<",
            "TK;>;",
            "Ljava/lang/reflect/Type;",
            "Lb/a/b/J<",
            "TV;>;",
            "Lb/a/b/a/A<",
            "+",
            "Ljava/util/Map<",
            "TK;TV;>;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lb/a/b/a/a/k$a;->d:Lb/a/b/a/a/k;

    invoke-direct {p0}, Lb/a/b/J;-><init>()V

    new-instance p1, Lb/a/b/a/a/w;

    invoke-direct {p1, p2, p4, p3}, Lb/a/b/a/a/w;-><init>(Lb/a/b/q;Lb/a/b/J;Ljava/lang/reflect/Type;)V

    iput-object p1, p0, Lb/a/b/a/a/k$a;->a:Lb/a/b/J;

    new-instance p1, Lb/a/b/a/a/w;

    invoke-direct {p1, p2, p6, p5}, Lb/a/b/a/a/w;-><init>(Lb/a/b/q;Lb/a/b/J;Ljava/lang/reflect/Type;)V

    iput-object p1, p0, Lb/a/b/a/a/k$a;->b:Lb/a/b/J;

    iput-object p7, p0, Lb/a/b/a/a/k$a;->c:Lb/a/b/a/A;

    return-void
.end method

.method private a(Lb/a/b/w;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Lb/a/b/w;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lb/a/b/w;->c()Lb/a/b/B;

    move-result-object p1

    invoke-virtual {p1}, Lb/a/b/B;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lb/a/b/B;->l()Ljava/lang/Number;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lb/a/b/B;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lb/a/b/B;->h()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-virtual {p1}, Lb/a/b/B;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lb/a/b/B;->m()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_3
    invoke-virtual {p1}, Lb/a/b/w;->e()Z

    move-result p1

    if-eqz p1, :cond_4

    const-string p1, "null"

    return-object p1

    :cond_4
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method


# virtual methods
.method public bridge synthetic a(Lb/a/b/c/b;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lb/a/b/a/a/k$a;->a(Lb/a/b/c/b;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public a(Lb/a/b/c/b;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/b;",
            ")",
            "Ljava/util/Map<",
            "TK;TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lb/a/b/c/b;->C()Lb/a/b/c/c;

    move-result-object v0

    sget-object v1, Lb/a/b/c/c;->i:Lb/a/b/c/c;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lb/a/b/c/b;->A()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v1, p0, Lb/a/b/a/a/k$a;->c:Lb/a/b/a/A;

    invoke-interface {v1}, Lb/a/b/a/A;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    sget-object v2, Lb/a/b/c/c;->a:Lb/a/b/c/c;

    const-string v3, "duplicate key: "

    if-ne v0, v2, :cond_3

    invoke-virtual {p1}, Lb/a/b/c/b;->a()V

    :goto_0
    invoke-virtual {p1}, Lb/a/b/c/b;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lb/a/b/c/b;->a()V

    iget-object v0, p0, Lb/a/b/a/a/k$a;->a:Lb/a/b/J;

    invoke-virtual {v0, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lb/a/b/a/a/k$a;->b:Lb/a/b/J;

    invoke-virtual {v2, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lb/a/b/c/b;->q()V

    goto :goto_0

    :cond_1
    new-instance p1, Lb/a/b/E;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lb/a/b/E;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    invoke-virtual {p1}, Lb/a/b/c/b;->q()V

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lb/a/b/c/b;->b()V

    :goto_1
    invoke-virtual {p1}, Lb/a/b/c/b;->s()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lb/a/b/a/u;->a:Lb/a/b/a/u;

    invoke-virtual {v0, p1}, Lb/a/b/a/u;->a(Lb/a/b/c/b;)V

    iget-object v0, p0, Lb/a/b/a/a/k$a;->a:Lb/a/b/J;

    invoke-virtual {v0, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lb/a/b/a/a/k$a;->b:Lb/a/b/J;

    invoke-virtual {v2, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_4

    goto :goto_1

    :cond_4
    new-instance p1, Lb/a/b/E;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lb/a/b/E;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    invoke-virtual {p1}, Lb/a/b/c/b;->r()V

    :goto_2
    return-object v1
.end method

.method public bridge synthetic a(Lb/a/b/c/d;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p2, Ljava/util/Map;

    invoke-virtual {p0, p1, p2}, Lb/a/b/a/a/k$a;->a(Lb/a/b/c/d;Ljava/util/Map;)V

    return-void
.end method

.method public a(Lb/a/b/c/d;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/d;",
            "Ljava/util/Map<",
            "TK;TV;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lb/a/b/c/d;->u()Lb/a/b/c/d;

    return-void

    :cond_0
    iget-object v0, p0, Lb/a/b/a/a/k$a;->d:Lb/a/b/a/a/k;

    iget-boolean v0, v0, Lb/a/b/a/a/k;->b:Z

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lb/a/b/c/d;->b()Lb/a/b/c/d;

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lb/a/b/c/d;->e(Ljava/lang/String;)Lb/a/b/c/d;

    iget-object v1, p0, Lb/a/b/a/a/k$a;->b:Lb/a/b/J;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lb/a/b/c/d;->q()Lb/a/b/c/d;

    return-void

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    iget-object v5, p0, Lb/a/b/a/a/k$a;->a:Lb/a/b/J;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Lb/a/b/J;->a(Ljava/lang/Object;)Lb/a/b/w;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Lb/a/b/w;->d()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v5}, Lb/a/b/w;->f()Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_2

    :cond_3
    move v4, v2

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v4, 0x1

    :goto_3
    or-int/2addr v3, v4

    goto :goto_1

    :cond_5
    if-eqz v3, :cond_7

    invoke-virtual {p1}, Lb/a/b/c/d;->a()Lb/a/b/c/d;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    :goto_4
    if-ge v2, p2, :cond_6

    invoke-virtual {p1}, Lb/a/b/c/d;->a()Lb/a/b/c/d;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lb/a/b/w;

    invoke-static {v3, p1}, Lb/a/b/a/D;->a(Lb/a/b/w;Lb/a/b/c/d;)V

    iget-object v3, p0, Lb/a/b/a/a/k$a;->b:Lb/a/b/J;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lb/a/b/c/d;->p()Lb/a/b/c/d;

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    invoke-virtual {p1}, Lb/a/b/c/d;->p()Lb/a/b/c/d;

    goto :goto_6

    :cond_7
    invoke-virtual {p1}, Lb/a/b/c/d;->b()Lb/a/b/c/d;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    :goto_5
    if-ge v2, p2, :cond_8

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lb/a/b/w;

    invoke-direct {p0, v3}, Lb/a/b/a/a/k$a;->a(Lb/a/b/w;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lb/a/b/c/d;->e(Ljava/lang/String;)Lb/a/b/c/d;

    iget-object v3, p0, Lb/a/b/a/a/k$a;->b:Lb/a/b/J;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_8
    invoke-virtual {p1}, Lb/a/b/c/d;->q()Lb/a/b/c/d;

    :goto_6
    return-void
.end method
