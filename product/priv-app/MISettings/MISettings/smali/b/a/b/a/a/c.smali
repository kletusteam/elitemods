.class public final Lb/a/b/a/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Lb/a/b/K;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/a/b/a/a/c$a;
    }
.end annotation


# instance fields
.field private final a:Lb/a/b/a/q;


# direct methods
.method public constructor <init>(Lb/a/b/a/q;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb/a/b/a/a/c;->a:Lb/a/b/a/q;

    return-void
.end method


# virtual methods
.method public a(Lb/a/b/q;Lb/a/b/b/a;)Lb/a/b/J;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lb/a/b/q;",
            "Lb/a/b/b/a<",
            "TT;>;)",
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p2}, Lb/a/b/b/a;->b()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p2}, Lb/a/b/b/a;->a()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Ljava/util/Collection;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {v0, v1}, Lb/a/b/a/b;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lb/a/b/b/a;->a(Ljava/lang/reflect/Type;)Lb/a/b/b/a;

    move-result-object v1

    invoke-virtual {p1, v1}, Lb/a/b/q;->a(Lb/a/b/b/a;)Lb/a/b/J;

    move-result-object v1

    iget-object v2, p0, Lb/a/b/a/a/c;->a:Lb/a/b/a/q;

    invoke-virtual {v2, p2}, Lb/a/b/a/q;->a(Lb/a/b/b/a;)Lb/a/b/a/A;

    move-result-object p2

    new-instance v2, Lb/a/b/a/a/c$a;

    invoke-direct {v2, p1, v0, v1, p2}, Lb/a/b/a/a/c$a;-><init>(Lb/a/b/q;Ljava/lang/reflect/Type;Lb/a/b/J;Lb/a/b/a/A;)V

    return-object v2
.end method
