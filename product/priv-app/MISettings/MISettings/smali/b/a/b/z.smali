.class public final Lb/a/b/z;
.super Lb/a/b/w;


# instance fields
.field private final a:Lb/a/b/a/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/a/y<",
            "Ljava/lang/String;",
            "Lb/a/b/w;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lb/a/b/w;-><init>()V

    new-instance v0, Lb/a/b/a/y;

    invoke-direct {v0}, Lb/a/b/a/y;-><init>()V

    iput-object v0, p0, Lb/a/b/z;->a:Lb/a/b/a/y;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lb/a/b/w;)V
    .locals 1

    iget-object v0, p0, Lb/a/b/z;->a:Lb/a/b/a/y;

    if-nez p2, :cond_0

    sget-object p2, Lb/a/b/y;->a:Lb/a/b/y;

    :cond_0
    invoke-virtual {v0, p1, p2}, Lb/a/b/a/y;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p1, p0, :cond_1

    instance-of v0, p1, Lb/a/b/z;

    if-eqz v0, :cond_0

    check-cast p1, Lb/a/b/z;

    iget-object p1, p1, Lb/a/b/z;->a:Lb/a/b/a/y;

    iget-object v0, p0, Lb/a/b/z;->a:Lb/a/b/a/y;

    invoke-virtual {p1, v0}, Ljava/util/AbstractMap;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "Lb/a/b/w;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/z;->a:Lb/a/b/a/y;

    invoke-virtual {v0}, Lb/a/b/a/y;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lb/a/b/z;->a:Lb/a/b/a/y;

    invoke-virtual {v0}, Ljava/util/AbstractMap;->hashCode()I

    move-result v0

    return v0
.end method
