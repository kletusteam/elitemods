.class Lb/a/b/a/a/T;
.super Lb/a/b/J;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb/a/b/a/a/ja;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lb/a/b/J<",
        "Lb/a/b/w;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lb/a/b/J;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lb/a/b/c/b;)Lb/a/b/w;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lb/a/b/a/a/ba;->a:[I

    invoke-virtual {p1}, Lb/a/b/c/b;->C()Lb/a/b/c/c;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :pswitch_0
    new-instance v0, Lb/a/b/z;

    invoke-direct {v0}, Lb/a/b/z;-><init>()V

    invoke-virtual {p1}, Lb/a/b/c/b;->b()V

    :goto_0
    invoke-virtual {p1}, Lb/a/b/c/b;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lb/a/b/c/b;->z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lb/a/b/a/a/T;->a(Lb/a/b/c/b;)Lb/a/b/w;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lb/a/b/z;->a(Ljava/lang/String;Lb/a/b/w;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lb/a/b/c/b;->r()V

    return-object v0

    :pswitch_1
    new-instance v0, Lb/a/b/t;

    invoke-direct {v0}, Lb/a/b/t;-><init>()V

    invoke-virtual {p1}, Lb/a/b/c/b;->a()V

    :goto_1
    invoke-virtual {p1}, Lb/a/b/c/b;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1}, Lb/a/b/a/a/T;->a(Lb/a/b/c/b;)Lb/a/b/w;

    move-result-object v1

    invoke-virtual {v0, v1}, Lb/a/b/t;->a(Lb/a/b/w;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lb/a/b/c/b;->q()V

    return-object v0

    :pswitch_2
    invoke-virtual {p1}, Lb/a/b/c/b;->A()V

    sget-object p1, Lb/a/b/y;->a:Lb/a/b/y;

    return-object p1

    :pswitch_3
    new-instance v0, Lb/a/b/B;

    invoke-virtual {p1}, Lb/a/b/c/b;->B()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lb/a/b/B;-><init>(Ljava/lang/String;)V

    return-object v0

    :pswitch_4
    new-instance v0, Lb/a/b/B;

    invoke-virtual {p1}, Lb/a/b/c/b;->v()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {v0, p1}, Lb/a/b/B;-><init>(Ljava/lang/Boolean;)V

    return-object v0

    :pswitch_5
    invoke-virtual {p1}, Lb/a/b/c/b;->B()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lb/a/b/B;

    new-instance v1, Lb/a/b/a/v;

    invoke-direct {v1, p1}, Lb/a/b/a/v;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lb/a/b/B;-><init>(Ljava/lang/Number;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Lb/a/b/c/b;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lb/a/b/a/a/T;->a(Lb/a/b/c/b;)Lb/a/b/w;

    move-result-object p1

    return-object p1
.end method

.method public a(Lb/a/b/c/d;Lb/a/b/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p2, :cond_8

    invoke-virtual {p2}, Lb/a/b/w;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p2}, Lb/a/b/w;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lb/a/b/w;->c()Lb/a/b/B;

    move-result-object p2

    invoke-virtual {p2}, Lb/a/b/B;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lb/a/b/B;->l()Ljava/lang/Number;

    move-result-object p2

    invoke-virtual {p1, p2}, Lb/a/b/c/d;->a(Ljava/lang/Number;)Lb/a/b/c/d;

    goto/16 :goto_3

    :cond_1
    invoke-virtual {p2}, Lb/a/b/B;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lb/a/b/B;->h()Z

    move-result p2

    invoke-virtual {p1, p2}, Lb/a/b/c/d;->d(Z)Lb/a/b/c/d;

    goto/16 :goto_3

    :cond_2
    invoke-virtual {p2}, Lb/a/b/B;->m()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lb/a/b/c/d;->g(Ljava/lang/String;)Lb/a/b/c/d;

    goto/16 :goto_3

    :cond_3
    invoke-virtual {p2}, Lb/a/b/w;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lb/a/b/c/d;->a()Lb/a/b/c/d;

    invoke-virtual {p2}, Lb/a/b/w;->a()Lb/a/b/t;

    move-result-object p2

    invoke-virtual {p2}, Lb/a/b/t;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/a/b/w;

    invoke-virtual {p0, p1, v0}, Lb/a/b/a/a/T;->a(Lb/a/b/c/d;Lb/a/b/w;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lb/a/b/c/d;->p()Lb/a/b/c/d;

    goto :goto_3

    :cond_5
    invoke-virtual {p2}, Lb/a/b/w;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lb/a/b/c/d;->b()Lb/a/b/c/d;

    invoke-virtual {p2}, Lb/a/b/w;->b()Lb/a/b/z;

    move-result-object p2

    invoke-virtual {p2}, Lb/a/b/z;->h()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lb/a/b/c/d;->e(Ljava/lang/String;)Lb/a/b/c/d;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/a/b/w;

    invoke-virtual {p0, p1, v0}, Lb/a/b/a/a/T;->a(Lb/a/b/c/d;Lb/a/b/w;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lb/a/b/c/d;->q()Lb/a/b/c/d;

    goto :goto_3

    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Couldn\'t write "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    :goto_2
    invoke-virtual {p1}, Lb/a/b/c/d;->u()Lb/a/b/c/d;

    :goto_3
    return-void
.end method

.method public bridge synthetic a(Lb/a/b/c/d;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p2, Lb/a/b/w;

    invoke-virtual {p0, p1, p2}, Lb/a/b/a/a/T;->a(Lb/a/b/c/d;Lb/a/b/w;)V

    return-void
.end method
