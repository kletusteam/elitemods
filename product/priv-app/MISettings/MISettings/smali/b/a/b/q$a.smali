.class Lb/a/b/q$a;
.super Lb/a/b/J;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb/a/b/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lb/a/b/J<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private a:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lb/a/b/J;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lb/a/b/c/b;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/b;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/q$a;->a:Lb/a/b/J;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public a(Lb/a/b/J;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/J<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/q$a;->a:Lb/a/b/J;

    if-nez v0, :cond_0

    iput-object p1, p0, Lb/a/b/q$a;->a:Lb/a/b/J;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public a(Lb/a/b/c/d;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/d;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/q$a;->a:Lb/a/b/J;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method
