.class Lb/a/b/a/a/o;
.super Lb/a/b/a/a/p$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lb/a/b/a/a/p;->a(Lb/a/b/q;Ljava/lang/reflect/Field;Ljava/lang/String;Lb/a/b/b/a;ZZ)Lb/a/b/a/a/p$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic d:Ljava/lang/reflect/Field;

.field final synthetic e:Z

.field final synthetic f:Lb/a/b/J;

.field final synthetic g:Lb/a/b/q;

.field final synthetic h:Lb/a/b/b/a;

.field final synthetic i:Z

.field final synthetic j:Lb/a/b/a/a/p;


# direct methods
.method constructor <init>(Lb/a/b/a/a/p;Ljava/lang/String;ZZLjava/lang/reflect/Field;ZLb/a/b/J;Lb/a/b/q;Lb/a/b/b/a;Z)V
    .locals 0

    iput-object p1, p0, Lb/a/b/a/a/o;->j:Lb/a/b/a/a/p;

    iput-object p5, p0, Lb/a/b/a/a/o;->d:Ljava/lang/reflect/Field;

    iput-boolean p6, p0, Lb/a/b/a/a/o;->e:Z

    iput-object p7, p0, Lb/a/b/a/a/o;->f:Lb/a/b/J;

    iput-object p8, p0, Lb/a/b/a/a/o;->g:Lb/a/b/q;

    iput-object p9, p0, Lb/a/b/a/a/o;->h:Lb/a/b/b/a;

    iput-boolean p10, p0, Lb/a/b/a/a/o;->i:Z

    invoke-direct {p0, p2, p3, p4}, Lb/a/b/a/a/p$b;-><init>(Ljava/lang/String;ZZ)V

    return-void
.end method


# virtual methods
.method a(Lb/a/b/c/b;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, p2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lb/a/b/a/a/o;->f:Lb/a/b/J;

    goto/32 :goto_6

    nop

    :goto_3
    return-void

    :goto_4
    if-eqz p1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_5

    nop

    :goto_5
    iget-boolean v0, p0, Lb/a/b/a/a/o;->i:Z

    goto/32 :goto_7

    nop

    :goto_6
    invoke-virtual {v0, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_4

    nop

    :goto_7
    if-eqz v0, :cond_1

    goto/32 :goto_1

    :cond_1
    :goto_8
    goto/32 :goto_9

    nop

    :goto_9
    iget-object v0, p0, Lb/a/b/a/a/o;->d:Ljava/lang/reflect/Field;

    goto/32 :goto_0

    nop
.end method

.method a(Lb/a/b/c/d;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    goto/32 :goto_e

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v3}, Lb/a/b/b/a;->b()Ljava/lang/reflect/Type;

    move-result-object v3

    goto/32 :goto_6

    nop

    :goto_2
    iget-object v0, p0, Lb/a/b/a/a/o;->f:Lb/a/b/J;

    goto/32 :goto_4

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_2

    nop

    :goto_4
    goto :goto_7

    :goto_5
    goto/32 :goto_c

    nop

    :goto_6
    invoke-direct {v0, v1, v2, v3}, Lb/a/b/a/a/w;-><init>(Lb/a/b/q;Lb/a/b/J;Ljava/lang/reflect/Type;)V

    :goto_7
    goto/32 :goto_d

    nop

    :goto_8
    iget-boolean v0, p0, Lb/a/b/a/a/o;->e:Z

    goto/32 :goto_3

    nop

    :goto_9
    iget-object v2, p0, Lb/a/b/a/a/o;->f:Lb/a/b/J;

    goto/32 :goto_b

    nop

    :goto_a
    iget-object v1, p0, Lb/a/b/a/a/o;->g:Lb/a/b/q;

    goto/32 :goto_9

    nop

    :goto_b
    iget-object v3, p0, Lb/a/b/a/a/o;->h:Lb/a/b/b/a;

    goto/32 :goto_1

    nop

    :goto_c
    new-instance v0, Lb/a/b/a/a/w;

    goto/32 :goto_a

    nop

    :goto_d
    invoke-virtual {v0, p1, p2}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    goto/32 :goto_0

    nop

    :goto_e
    iget-object v0, p0, Lb/a/b/a/a/o;->d:Ljava/lang/reflect/Field;

    goto/32 :goto_f

    nop

    :goto_f
    invoke-virtual {v0, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_8

    nop
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    iget-boolean v0, p0, Lb/a/b/a/a/p$b;->b:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lb/a/b/a/a/o;->d:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method
