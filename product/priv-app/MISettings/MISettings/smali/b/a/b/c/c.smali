.class public final enum Lb/a/b/c/c;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lb/a/b/c/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lb/a/b/c/c;

.field public static final enum b:Lb/a/b/c/c;

.field public static final enum c:Lb/a/b/c/c;

.field public static final enum d:Lb/a/b/c/c;

.field public static final enum e:Lb/a/b/c/c;

.field public static final enum f:Lb/a/b/c/c;

.field public static final enum g:Lb/a/b/c/c;

.field public static final enum h:Lb/a/b/c/c;

.field public static final enum i:Lb/a/b/c/c;

.field public static final enum j:Lb/a/b/c/c;

.field private static final synthetic k:[Lb/a/b/c/c;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    new-instance v0, Lb/a/b/c/c;

    const/4 v1, 0x0

    const-string v2, "BEGIN_ARRAY"

    invoke-direct {v0, v2, v1}, Lb/a/b/c/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/c/c;->a:Lb/a/b/c/c;

    new-instance v0, Lb/a/b/c/c;

    const/4 v2, 0x1

    const-string v3, "END_ARRAY"

    invoke-direct {v0, v3, v2}, Lb/a/b/c/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/c/c;->b:Lb/a/b/c/c;

    new-instance v0, Lb/a/b/c/c;

    const/4 v3, 0x2

    const-string v4, "BEGIN_OBJECT"

    invoke-direct {v0, v4, v3}, Lb/a/b/c/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/c/c;->c:Lb/a/b/c/c;

    new-instance v0, Lb/a/b/c/c;

    const/4 v4, 0x3

    const-string v5, "END_OBJECT"

    invoke-direct {v0, v5, v4}, Lb/a/b/c/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/c/c;->d:Lb/a/b/c/c;

    new-instance v0, Lb/a/b/c/c;

    const/4 v5, 0x4

    const-string v6, "NAME"

    invoke-direct {v0, v6, v5}, Lb/a/b/c/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/c/c;->e:Lb/a/b/c/c;

    new-instance v0, Lb/a/b/c/c;

    const/4 v6, 0x5

    const-string v7, "STRING"

    invoke-direct {v0, v7, v6}, Lb/a/b/c/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/c/c;->f:Lb/a/b/c/c;

    new-instance v0, Lb/a/b/c/c;

    const/4 v7, 0x6

    const-string v8, "NUMBER"

    invoke-direct {v0, v8, v7}, Lb/a/b/c/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/c/c;->g:Lb/a/b/c/c;

    new-instance v0, Lb/a/b/c/c;

    const/4 v8, 0x7

    const-string v9, "BOOLEAN"

    invoke-direct {v0, v9, v8}, Lb/a/b/c/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/c/c;->h:Lb/a/b/c/c;

    new-instance v0, Lb/a/b/c/c;

    const/16 v9, 0x8

    const-string v10, "NULL"

    invoke-direct {v0, v10, v9}, Lb/a/b/c/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/c/c;->i:Lb/a/b/c/c;

    new-instance v0, Lb/a/b/c/c;

    const/16 v10, 0x9

    const-string v11, "END_DOCUMENT"

    invoke-direct {v0, v11, v10}, Lb/a/b/c/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/a/b/c/c;->j:Lb/a/b/c/c;

    const/16 v0, 0xa

    new-array v0, v0, [Lb/a/b/c/c;

    sget-object v11, Lb/a/b/c/c;->a:Lb/a/b/c/c;

    aput-object v11, v0, v1

    sget-object v1, Lb/a/b/c/c;->b:Lb/a/b/c/c;

    aput-object v1, v0, v2

    sget-object v1, Lb/a/b/c/c;->c:Lb/a/b/c/c;

    aput-object v1, v0, v3

    sget-object v1, Lb/a/b/c/c;->d:Lb/a/b/c/c;

    aput-object v1, v0, v4

    sget-object v1, Lb/a/b/c/c;->e:Lb/a/b/c/c;

    aput-object v1, v0, v5

    sget-object v1, Lb/a/b/c/c;->f:Lb/a/b/c/c;

    aput-object v1, v0, v6

    sget-object v1, Lb/a/b/c/c;->g:Lb/a/b/c/c;

    aput-object v1, v0, v7

    sget-object v1, Lb/a/b/c/c;->h:Lb/a/b/c/c;

    aput-object v1, v0, v8

    sget-object v1, Lb/a/b/c/c;->i:Lb/a/b/c/c;

    aput-object v1, v0, v9

    sget-object v1, Lb/a/b/c/c;->j:Lb/a/b/c/c;

    aput-object v1, v0, v10

    sput-object v0, Lb/a/b/c/c;->k:[Lb/a/b/c/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lb/a/b/c/c;
    .locals 1

    const-class v0, Lb/a/b/c/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lb/a/b/c/c;

    return-object p0
.end method

.method public static values()[Lb/a/b/c/c;
    .locals 1

    sget-object v0, Lb/a/b/c/c;->k:[Lb/a/b/c/c;

    invoke-virtual {v0}, [Lb/a/b/c/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lb/a/b/c/c;

    return-object v0
.end method
