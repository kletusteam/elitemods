.class final Lb/a/b/a/a/c$a;
.super Lb/a/b/J;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb/a/b/a/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lb/a/b/J<",
        "Ljava/util/Collection<",
        "TE;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lb/a/b/J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/J<",
            "TE;>;"
        }
    .end annotation
.end field

.field private final b:Lb/a/b/a/A;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/a/A<",
            "+",
            "Ljava/util/Collection<",
            "TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lb/a/b/q;Ljava/lang/reflect/Type;Lb/a/b/J;Lb/a/b/a/A;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/q;",
            "Ljava/lang/reflect/Type;",
            "Lb/a/b/J<",
            "TE;>;",
            "Lb/a/b/a/A<",
            "+",
            "Ljava/util/Collection<",
            "TE;>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lb/a/b/J;-><init>()V

    new-instance v0, Lb/a/b/a/a/w;

    invoke-direct {v0, p1, p3, p2}, Lb/a/b/a/a/w;-><init>(Lb/a/b/q;Lb/a/b/J;Ljava/lang/reflect/Type;)V

    iput-object v0, p0, Lb/a/b/a/a/c$a;->a:Lb/a/b/J;

    iput-object p4, p0, Lb/a/b/a/a/c$a;->b:Lb/a/b/a/A;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lb/a/b/c/b;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lb/a/b/a/a/c$a;->a(Lb/a/b/c/b;)Ljava/util/Collection;

    move-result-object p1

    return-object p1
.end method

.method public a(Lb/a/b/c/b;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/b;",
            ")",
            "Ljava/util/Collection<",
            "TE;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lb/a/b/c/b;->C()Lb/a/b/c/c;

    move-result-object v0

    sget-object v1, Lb/a/b/c/c;->i:Lb/a/b/c/c;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lb/a/b/c/b;->A()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v0, p0, Lb/a/b/a/a/c$a;->b:Lb/a/b/a/A;

    invoke-interface {v0}, Lb/a/b/a/A;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p1}, Lb/a/b/c/b;->a()V

    :goto_0
    invoke-virtual {p1}, Lb/a/b/c/b;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lb/a/b/a/a/c$a;->a:Lb/a/b/J;

    invoke-virtual {v1, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lb/a/b/c/b;->q()V

    return-object v0
.end method

.method public bridge synthetic a(Lb/a/b/c/d;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p2, Ljava/util/Collection;

    invoke-virtual {p0, p1, p2}, Lb/a/b/a/a/c$a;->a(Lb/a/b/c/d;Ljava/util/Collection;)V

    return-void
.end method

.method public a(Lb/a/b/c/d;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/c/d;",
            "Ljava/util/Collection<",
            "TE;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lb/a/b/c/d;->u()Lb/a/b/c/d;

    return-void

    :cond_0
    invoke-virtual {p1}, Lb/a/b/c/d;->a()Lb/a/b/c/d;

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lb/a/b/a/a/c$a;->a:Lb/a/b/J;

    invoke-virtual {v1, p1, v0}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lb/a/b/c/d;->p()Lb/a/b/c/d;

    return-void
.end method
