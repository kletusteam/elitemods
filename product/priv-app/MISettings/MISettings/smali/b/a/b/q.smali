.class public final Lb/a/b/q;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/a/b/q$a;
    }
.end annotation


# static fields
.field private static final a:Lb/a/b/b/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/b/b/a<",
            "*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/util/Map<",
            "Lb/a/b/b/a<",
            "*>;",
            "Lb/a/b/q$a<",
            "*>;>;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lb/a/b/b/a<",
            "*>;",
            "Lb/a/b/J<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final d:Lb/a/b/a/q;

.field private final e:Lb/a/b/a/a/f;

.field final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb/a/b/K;",
            ">;"
        }
    .end annotation
.end field

.field final g:Lb/a/b/a/s;

.field final h:Lb/a/b/k;

.field final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/reflect/Type;",
            "Lb/a/b/s<",
            "*>;>;"
        }
    .end annotation
.end field

.field final j:Z

.field final k:Z

.field final l:Z

.field final m:Z

.field final n:Z

.field final o:Z

.field final p:Z

.field final q:Ljava/lang/String;

.field final r:I

.field final s:I

.field final t:Lb/a/b/H;

.field final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb/a/b/K;",
            ">;"
        }
    .end annotation
.end field

.field final v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb/a/b/K;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lb/a/b/b/a;->a(Ljava/lang/Class;)Lb/a/b/b/a;

    move-result-object v0

    sput-object v0, Lb/a/b/q;->a:Lb/a/b/b/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 18

    move-object/from16 v0, p0

    sget-object v1, Lb/a/b/a/s;->a:Lb/a/b/a/s;

    sget-object v2, Lb/a/b/j;->a:Lb/a/b/j;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v3

    sget-object v11, Lb/a/b/H;->a:Lb/a/b/H;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v15

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v16

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v17

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x2

    const/4 v14, 0x2

    invoke-direct/range {v0 .. v17}, Lb/a/b/q;-><init>(Lb/a/b/a/s;Lb/a/b/k;Ljava/util/Map;ZZZZZZZLb/a/b/H;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method constructor <init>(Lb/a/b/a/s;Lb/a/b/k;Ljava/util/Map;ZZZZZZZLb/a/b/H;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/a/s;",
            "Lb/a/b/k;",
            "Ljava/util/Map<",
            "Ljava/lang/reflect/Type;",
            "Lb/a/b/s<",
            "*>;>;ZZZZZZZ",
            "Lb/a/b/H;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List<",
            "Lb/a/b/K;",
            ">;",
            "Ljava/util/List<",
            "Lb/a/b/K;",
            ">;",
            "Ljava/util/List<",
            "Lb/a/b/K;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p5

    move/from16 v5, p10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v6, Ljava/lang/ThreadLocal;

    invoke-direct {v6}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v6, v0, Lb/a/b/q;->b:Ljava/lang/ThreadLocal;

    new-instance v6, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v6}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v6, v0, Lb/a/b/q;->c:Ljava/util/Map;

    iput-object v1, v0, Lb/a/b/q;->g:Lb/a/b/a/s;

    iput-object v2, v0, Lb/a/b/q;->h:Lb/a/b/k;

    iput-object v3, v0, Lb/a/b/q;->i:Ljava/util/Map;

    new-instance v6, Lb/a/b/a/q;

    invoke-direct {v6, p3}, Lb/a/b/a/q;-><init>(Ljava/util/Map;)V

    iput-object v6, v0, Lb/a/b/q;->d:Lb/a/b/a/q;

    move v3, p4

    iput-boolean v3, v0, Lb/a/b/q;->j:Z

    iput-boolean v4, v0, Lb/a/b/q;->k:Z

    move/from16 v3, p6

    iput-boolean v3, v0, Lb/a/b/q;->l:Z

    move/from16 v3, p7

    iput-boolean v3, v0, Lb/a/b/q;->m:Z

    move/from16 v3, p8

    iput-boolean v3, v0, Lb/a/b/q;->n:Z

    move/from16 v3, p9

    iput-boolean v3, v0, Lb/a/b/q;->o:Z

    iput-boolean v5, v0, Lb/a/b/q;->p:Z

    move-object/from16 v3, p11

    iput-object v3, v0, Lb/a/b/q;->t:Lb/a/b/H;

    move-object/from16 v6, p12

    iput-object v6, v0, Lb/a/b/q;->q:Ljava/lang/String;

    move/from16 v6, p13

    iput v6, v0, Lb/a/b/q;->r:I

    move/from16 v6, p14

    iput v6, v0, Lb/a/b/q;->s:I

    move-object/from16 v6, p15

    iput-object v6, v0, Lb/a/b/q;->u:Ljava/util/List;

    move-object/from16 v6, p16

    iput-object v6, v0, Lb/a/b/q;->v:Ljava/util/List;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    sget-object v7, Lb/a/b/a/a/ja;->Y:Lb/a/b/K;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v7, Lb/a/b/a/a/n;->a:Lb/a/b/K;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v6, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v7, p17

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    sget-object v7, Lb/a/b/a/a/ja;->D:Lb/a/b/K;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v7, Lb/a/b/a/a/ja;->m:Lb/a/b/K;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v7, Lb/a/b/a/a/ja;->g:Lb/a/b/K;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v7, Lb/a/b/a/a/ja;->i:Lb/a/b/K;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v7, Lb/a/b/a/a/ja;->k:Lb/a/b/K;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static/range {p11 .. p11}, Lb/a/b/q;->a(Lb/a/b/H;)Lb/a/b/J;

    move-result-object v3

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const-class v8, Ljava/lang/Long;

    invoke-static {v7, v8, v3}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v7, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const-class v8, Ljava/lang/Double;

    invoke-direct {p0, v5}, Lb/a/b/q;->a(Z)Lb/a/b/J;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v7, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v8, Ljava/lang/Float;

    invoke-direct {p0, v5}, Lb/a/b/q;->b(Z)Lb/a/b/J;

    move-result-object v5

    invoke-static {v7, v8, v5}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v5, Lb/a/b/a/a/ja;->x:Lb/a/b/K;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v5, Lb/a/b/a/a/ja;->o:Lb/a/b/K;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v5, Lb/a/b/a/a/ja;->q:Lb/a/b/K;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class v5, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {v3}, Lb/a/b/q;->a(Lb/a/b/J;)Lb/a/b/J;

    move-result-object v7

    invoke-static {v5, v7}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class v5, Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-static {v3}, Lb/a/b/q;->b(Lb/a/b/J;)Lb/a/b/J;

    move-result-object v3

    invoke-static {v5, v3}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->s:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->z:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->F:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->H:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class v3, Ljava/math/BigDecimal;

    sget-object v5, Lb/a/b/a/a/ja;->B:Lb/a/b/J;

    invoke-static {v3, v5}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class v3, Ljava/math/BigInteger;

    sget-object v5, Lb/a/b/a/a/ja;->C:Lb/a/b/J;

    invoke-static {v3, v5}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->J:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->L:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->P:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->R:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->W:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->N:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->d:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/e;->a:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->U:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/t;->a:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/r;->a:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->S:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/b;->a:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->b:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lb/a/b/a/a/c;

    iget-object v5, v0, Lb/a/b/q;->d:Lb/a/b/a/q;

    invoke-direct {v3, v5}, Lb/a/b/a/a/c;-><init>(Lb/a/b/a/q;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lb/a/b/a/a/k;

    iget-object v5, v0, Lb/a/b/q;->d:Lb/a/b/a/q;

    invoke-direct {v3, v5, p5}, Lb/a/b/a/a/k;-><init>(Lb/a/b/a/q;Z)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lb/a/b/a/a/f;

    iget-object v4, v0, Lb/a/b/q;->d:Lb/a/b/a/q;

    invoke-direct {v3, v4}, Lb/a/b/a/a/f;-><init>(Lb/a/b/a/q;)V

    iput-object v3, v0, Lb/a/b/q;->e:Lb/a/b/a/a/f;

    iget-object v3, v0, Lb/a/b/q;->e:Lb/a/b/a/a/f;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lb/a/b/a/a/ja;->Z:Lb/a/b/K;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lb/a/b/a/a/p;

    iget-object v4, v0, Lb/a/b/q;->d:Lb/a/b/a/q;

    iget-object v5, v0, Lb/a/b/q;->e:Lb/a/b/a/a/f;

    invoke-direct {v3, v4, p2, p1, v5}, Lb/a/b/a/a/p;-><init>(Lb/a/b/a/q;Lb/a/b/k;Lb/a/b/a/s;Lb/a/b/a/a/f;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lb/a/b/q;->f:Ljava/util/List;

    return-void
.end method

.method private static a(Lb/a/b/H;)Lb/a/b/J;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/H;",
            ")",
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    sget-object v0, Lb/a/b/H;->a:Lb/a/b/H;

    if-ne p0, v0, :cond_0

    sget-object p0, Lb/a/b/a/a/ja;->t:Lb/a/b/J;

    return-object p0

    :cond_0
    new-instance p0, Lb/a/b/n;

    invoke-direct {p0}, Lb/a/b/n;-><init>()V

    return-object p0
.end method

.method private static a(Lb/a/b/J;)Lb/a/b/J;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;)",
            "Lb/a/b/J<",
            "Ljava/util/concurrent/atomic/AtomicLong;",
            ">;"
        }
    .end annotation

    new-instance v0, Lb/a/b/o;

    invoke-direct {v0, p0}, Lb/a/b/o;-><init>(Lb/a/b/J;)V

    invoke-virtual {v0}, Lb/a/b/J;->a()Lb/a/b/J;

    move-result-object p0

    return-object p0
.end method

.method private a(Z)Lb/a/b/J;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    sget-object p1, Lb/a/b/a/a/ja;->v:Lb/a/b/J;

    return-object p1

    :cond_0
    new-instance p1, Lb/a/b/l;

    invoke-direct {p1, p0}, Lb/a/b/l;-><init>(Lb/a/b/q;)V

    return-object p1
.end method

.method static a(D)V
    .locals 2

    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p0, " is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method."

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/lang/Object;Lb/a/b/c/b;)V
    .locals 0

    if-eqz p0, :cond_1

    :try_start_0
    invoke-virtual {p1}, Lb/a/b/c/b;->C()Lb/a/b/c/c;

    move-result-object p0

    sget-object p1, Lb/a/b/c/c;->j:Lb/a/b/c/c;

    if-ne p0, p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p0, Lb/a/b/x;

    const-string p1, "JSON document was not fully consumed."

    invoke-direct {p0, p1}, Lb/a/b/x;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Lb/a/b/c/e; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    new-instance p1, Lb/a/b/x;

    invoke-direct {p1, p0}, Lb/a/b/x;-><init>(Ljava/lang/Throwable;)V

    throw p1

    :catch_1
    move-exception p0

    new-instance p1, Lb/a/b/E;

    invoke-direct {p1, p0}, Lb/a/b/E;-><init>(Ljava/lang/Throwable;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method private static b(Lb/a/b/J;)Lb/a/b/J;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;)",
            "Lb/a/b/J<",
            "Ljava/util/concurrent/atomic/AtomicLongArray;",
            ">;"
        }
    .end annotation

    new-instance v0, Lb/a/b/p;

    invoke-direct {v0, p0}, Lb/a/b/p;-><init>(Lb/a/b/J;)V

    invoke-virtual {v0}, Lb/a/b/J;->a()Lb/a/b/J;

    move-result-object p0

    return-object p0
.end method

.method private b(Z)Lb/a/b/J;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lb/a/b/J<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    sget-object p1, Lb/a/b/a/a/ja;->u:Lb/a/b/J;

    return-object p1

    :cond_0
    new-instance p1, Lb/a/b/m;

    invoke-direct {p1, p0}, Lb/a/b/m;-><init>(Lb/a/b/q;)V

    return-object p1
.end method


# virtual methods
.method public a(Lb/a/b/K;Lb/a/b/b/a;)Lb/a/b/J;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lb/a/b/K;",
            "Lb/a/b/b/a<",
            "TT;>;)",
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/q;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p1, p0, Lb/a/b/q;->e:Lb/a/b/a/a/f;

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lb/a/b/q;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lb/a/b/K;

    if-nez v0, :cond_2

    if-ne v2, p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v2, p0, p2}, Lb/a/b/K;->a(Lb/a/b/q;Lb/a/b/b/a;)Lb/a/b/J;

    move-result-object v2

    if-eqz v2, :cond_1

    return-object v2

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GSON cannot serialize "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Lb/a/b/b/a;)Lb/a/b/J;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lb/a/b/b/a<",
            "TT;>;)",
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/q;->c:Ljava/util/Map;

    if-nez p1, :cond_0

    sget-object v1, Lb/a/b/q;->a:Lb/a/b/b/a;

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/a/b/J;

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    iget-object v0, p0, Lb/a/b/q;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const/4 v1, 0x0

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lb/a/b/q;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    const/4 v1, 0x1

    :cond_2
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lb/a/b/q$a;

    if-eqz v2, :cond_3

    return-object v2

    :cond_3
    :try_start_0
    new-instance v2, Lb/a/b/q$a;

    invoke-direct {v2}, Lb/a/b/q$a;-><init>()V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lb/a/b/q;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lb/a/b/K;

    invoke-interface {v4, p0, p1}, Lb/a/b/K;->a(Lb/a/b/q;Lb/a/b/b/a;)Lb/a/b/J;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v2, v4}, Lb/a/b/q$a;->a(Lb/a/b/J;)V

    iget-object v2, p0, Lb/a/b/q;->c:Ljava/util/Map;

    invoke-interface {v2, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_5

    iget-object p1, p0, Lb/a/b/q;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {p1}, Ljava/lang/ThreadLocal;->remove()V

    :cond_5
    return-object v4

    :cond_6
    :try_start_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GSON (2.8.7) cannot handle "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v2

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_7

    iget-object p1, p0, Lb/a/b/q;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {p1}, Ljava/lang/ThreadLocal;->remove()V

    :cond_7
    throw v2
.end method

.method public a(Ljava/lang/Class;)Lb/a/b/J;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation

    invoke-static {p1}, Lb/a/b/b/a;->a(Ljava/lang/Class;)Lb/a/b/b/a;

    move-result-object p1

    invoke-virtual {p0, p1}, Lb/a/b/q;->a(Lb/a/b/b/a;)Lb/a/b/J;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/io/Reader;)Lb/a/b/c/b;
    .locals 1

    new-instance v0, Lb/a/b/c/b;

    invoke-direct {v0, p1}, Lb/a/b/c/b;-><init>(Ljava/io/Reader;)V

    iget-boolean p1, p0, Lb/a/b/q;->o:Z

    invoke-virtual {v0, p1}, Lb/a/b/c/b;->a(Z)V

    return-object v0
.end method

.method public a(Ljava/io/Writer;)Lb/a/b/c/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lb/a/b/q;->l:Z

    if-eqz v0, :cond_0

    const-string v0, ")]}\'\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lb/a/b/c/d;

    invoke-direct {v0, p1}, Lb/a/b/c/d;-><init>(Ljava/io/Writer;)V

    iget-boolean p1, p0, Lb/a/b/q;->n:Z

    if-eqz p1, :cond_1

    const-string p1, "  "

    invoke-virtual {v0, p1}, Lb/a/b/c/d;->f(Ljava/lang/String;)V

    :cond_1
    iget-boolean p1, p0, Lb/a/b/q;->j:Z

    invoke-virtual {v0, p1}, Lb/a/b/c/d;->c(Z)V

    return-object v0
.end method

.method public a(Lb/a/b/c/b;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lb/a/b/c/b;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lb/a/b/x;,
            Lb/a/b/E;
        }
    .end annotation

    invoke-virtual {p1}, Lb/a/b/c/b;->t()Z

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lb/a/b/c/b;->a(Z)V

    :try_start_0
    invoke-virtual {p1}, Lb/a/b/c/b;->C()Lb/a/b/c/c;

    const/4 v1, 0x0

    invoke-static {p2}, Lb/a/b/b/a;->a(Ljava/lang/reflect/Type;)Lb/a/b/b/a;

    move-result-object p2

    invoke-virtual {p0, p2}, Lb/a/b/q;->a(Lb/a/b/b/a;)Lb/a/b/J;

    move-result-object p2

    invoke-virtual {p2, p1}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object p2
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1, v0}, Lb/a/b/c/b;->a(Z)V

    return-object p2

    :catchall_0
    move-exception p2

    goto :goto_0

    :catch_0
    move-exception p2

    :try_start_1
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AssertionError (GSON 2.8.7): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/AssertionError;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p2}, Ljava/lang/AssertionError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1

    :catch_1
    move-exception p2

    new-instance v1, Lb/a/b/E;

    invoke-direct {v1, p2}, Lb/a/b/E;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception p2

    new-instance v1, Lb/a/b/E;

    invoke-direct {v1, p2}, Lb/a/b/E;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_3
    move-exception p2

    if-eqz v1, :cond_0

    const/4 p2, 0x0

    invoke-virtual {p1, v0}, Lb/a/b/c/b;->a(Z)V

    return-object p2

    :cond_0
    :try_start_2
    new-instance v1, Lb/a/b/E;

    invoke-direct {v1, p2}, Lb/a/b/E;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    invoke-virtual {p1, v0}, Lb/a/b/c/b;->a(Z)V

    throw p2
.end method

.method public a(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/Reader;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lb/a/b/x;,
            Lb/a/b/E;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lb/a/b/q;->a(Ljava/io/Reader;)Lb/a/b/c/b;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lb/a/b/q;->a(Lb/a/b/c/b;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p2

    invoke-static {p2, p1}, Lb/a/b/q;->a(Ljava/lang/Object;Lb/a/b/c/b;)V

    return-object p2
.end method

.method public a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lb/a/b/E;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lb/a/b/q;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p2}, Lb/a/b/a/C;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lb/a/b/E;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lb/a/b/q;->a(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public a(Lb/a/b/w;)Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    invoke-virtual {p0, p1, v0}, Lb/a/b/q;->a(Lb/a/b/w;Ljava/lang/Appendable;)V

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    sget-object p1, Lb/a/b/y;->a:Lb/a/b/y;

    invoke-virtual {p0, p1}, Lb/a/b/q;->a(Lb/a/b/w;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lb/a/b/q;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Lb/a/b/q;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Lb/a/b/w;Lb/a/b/c/d;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lb/a/b/x;
        }
    .end annotation

    invoke-virtual {p2}, Lb/a/b/c/d;->t()Z

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Lb/a/b/c/d;->b(Z)V

    invoke-virtual {p2}, Lb/a/b/c/d;->s()Z

    move-result v1

    iget-boolean v2, p0, Lb/a/b/q;->m:Z

    invoke-virtual {p2, v2}, Lb/a/b/c/d;->a(Z)V

    invoke-virtual {p2}, Lb/a/b/c/d;->r()Z

    move-result v2

    iget-boolean v3, p0, Lb/a/b/q;->j:Z

    invoke-virtual {p2, v3}, Lb/a/b/c/d;->c(Z)V

    :try_start_0
    invoke-static {p1, p2}, Lb/a/b/a/D;->a(Lb/a/b/w;Lb/a/b/c/d;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p2, v0}, Lb/a/b/c/d;->b(Z)V

    invoke-virtual {p2, v1}, Lb/a/b/c/d;->a(Z)V

    invoke-virtual {p2, v2}, Lb/a/b/c/d;->c(Z)V

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AssertionError (GSON 2.8.7): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/AssertionError;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, p1}, Ljava/lang/AssertionError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v3

    :catch_1
    move-exception p1

    new-instance v3, Lb/a/b/x;

    invoke-direct {v3, p1}, Lb/a/b/x;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-virtual {p2, v0}, Lb/a/b/c/d;->b(Z)V

    invoke-virtual {p2, v1}, Lb/a/b/c/d;->a(Z)V

    invoke-virtual {p2, v2}, Lb/a/b/c/d;->c(Z)V

    throw p1
.end method

.method public a(Lb/a/b/w;Ljava/lang/Appendable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lb/a/b/x;
        }
    .end annotation

    :try_start_0
    invoke-static {p2}, Lb/a/b/a/D;->a(Ljava/lang/Appendable;)Ljava/io/Writer;

    move-result-object p2

    invoke-virtual {p0, p2}, Lb/a/b/q;->a(Ljava/io/Writer;)Lb/a/b/c/d;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lb/a/b/q;->a(Lb/a/b/w;Lb/a/b/c/d;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lb/a/b/x;

    invoke-direct {p2, p1}, Lb/a/b/x;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lb/a/b/c/d;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lb/a/b/x;
        }
    .end annotation

    invoke-static {p2}, Lb/a/b/b/a;->a(Ljava/lang/reflect/Type;)Lb/a/b/b/a;

    move-result-object p2

    invoke-virtual {p0, p2}, Lb/a/b/q;->a(Lb/a/b/b/a;)Lb/a/b/J;

    move-result-object p2

    invoke-virtual {p3}, Lb/a/b/c/d;->t()Z

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p3, v1}, Lb/a/b/c/d;->b(Z)V

    invoke-virtual {p3}, Lb/a/b/c/d;->s()Z

    move-result v1

    iget-boolean v2, p0, Lb/a/b/q;->m:Z

    invoke-virtual {p3, v2}, Lb/a/b/c/d;->a(Z)V

    invoke-virtual {p3}, Lb/a/b/c/d;->r()Z

    move-result v2

    iget-boolean v3, p0, Lb/a/b/q;->j:Z

    invoke-virtual {p3, v3}, Lb/a/b/c/d;->c(Z)V

    :try_start_0
    invoke-virtual {p2, p3, p1}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p3, v0}, Lb/a/b/c/d;->b(Z)V

    invoke-virtual {p3, v1}, Lb/a/b/c/d;->a(Z)V

    invoke-virtual {p3, v2}, Lb/a/b/c/d;->c(Z)V

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    new-instance p2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AssertionError (GSON 2.8.7): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/AssertionError;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p2, p1}, Ljava/lang/AssertionError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw p2

    :catch_1
    move-exception p1

    new-instance p2, Lb/a/b/x;

    invoke-direct {p2, p1}, Lb/a/b/x;-><init>(Ljava/lang/Throwable;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-virtual {p3, v0}, Lb/a/b/c/d;->b(Z)V

    invoke-virtual {p3, v1}, Lb/a/b/c/d;->a(Z)V

    invoke-virtual {p3, v2}, Lb/a/b/c/d;->c(Z)V

    throw p1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lb/a/b/x;
        }
    .end annotation

    :try_start_0
    invoke-static {p3}, Lb/a/b/a/D;->a(Ljava/lang/Appendable;)Ljava/io/Writer;

    move-result-object p3

    invoke-virtual {p0, p3}, Lb/a/b/q;->a(Ljava/io/Writer;)Lb/a/b/c/d;

    move-result-object p3

    invoke-virtual {p0, p1, p2, p3}, Lb/a/b/q;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lb/a/b/c/d;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lb/a/b/x;

    invoke-direct {p2, p1}, Lb/a/b/x;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{serializeNulls:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lb/a/b/q;->j:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ",factories:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb/a/b/q;->f:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",instanceCreators:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb/a/b/q;->d:Lb/a/b/a/q;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
