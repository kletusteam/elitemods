.class public final Lb/a/b/a/a/f;
.super Ljava/lang/Object;

# interfaces
.implements Lb/a/b/K;


# instance fields
.field private final a:Lb/a/b/a/q;


# direct methods
.method public constructor <init>(Lb/a/b/a/q;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb/a/b/a/a/f;->a:Lb/a/b/a/q;

    return-void
.end method


# virtual methods
.method a(Lb/a/b/a/q;Lb/a/b/q;Lb/a/b/b/a;Lcom/google/gson/annotations/JsonAdapter;)Lb/a/b/J;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/b/a/q;",
            "Lb/a/b/q;",
            "Lb/a/b/b/a<",
            "*>;",
            "Lcom/google/gson/annotations/JsonAdapter;",
            ")",
            "Lb/a/b/J<",
            "*>;"
        }
    .end annotation

    goto/32 :goto_42

    nop

    :goto_0
    goto :goto_3

    :goto_1
    goto/32 :goto_3f

    nop

    :goto_2
    invoke-direct/range {v2 .. v7}, Lb/a/b/a/a/v;-><init>(Lb/a/b/D;Lb/a/b/v;Lb/a/b/q;Lb/a/b/b/a;Lb/a/b/K;)V

    :goto_3
    goto/32 :goto_20

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_3a

    :cond_0
    goto/32 :goto_39

    nop

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_1b

    nop

    :goto_6
    const-string p1, " as a @JsonAdapter for "

    goto/32 :goto_21

    nop

    :goto_7
    move-object v4, v1

    goto/32 :goto_32

    nop

    :goto_8
    goto :goto_d

    :goto_9
    goto/32 :goto_c

    nop

    :goto_a
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_24

    nop

    :goto_b
    check-cast p1, Lb/a/b/K;

    goto/32 :goto_36

    nop

    :goto_c
    move-object v3, v1

    :goto_d
    goto/32 :goto_15

    nop

    :goto_e
    move-object v1, p1

    goto/32 :goto_40

    nop

    :goto_f
    return-object p1

    :goto_10
    check-cast v0, Lb/a/b/D;

    goto/32 :goto_11

    nop

    :goto_11
    move-object v3, v0

    goto/32 :goto_8

    nop

    :goto_12
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1c

    nop

    :goto_13
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    goto/32 :goto_12

    nop

    :goto_14
    move-object v2, p1

    goto/32 :goto_18

    nop

    :goto_15
    instance-of v0, p1, Lb/a/b/v;

    goto/32 :goto_3c

    nop

    :goto_16
    instance-of v1, p1, Lb/a/b/v;

    goto/32 :goto_4

    nop

    :goto_17
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_23

    nop

    :goto_18
    move-object v5, p2

    goto/32 :goto_27

    nop

    :goto_19
    throw p2

    :goto_1a
    goto/32 :goto_26

    nop

    :goto_1b
    move-object v0, p1

    goto/32 :goto_10

    nop

    :goto_1c
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_1d
    if-nez v0, :cond_2

    goto/32 :goto_1f

    :cond_2
    goto/32 :goto_37

    nop

    :goto_1e
    goto/16 :goto_3

    :goto_1f
    goto/32 :goto_33

    nop

    :goto_20
    if-nez p1, :cond_3

    goto/32 :goto_2a

    :cond_3
    goto/32 :goto_3e

    nop

    :goto_21
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2d

    nop

    :goto_22
    invoke-static {v0}, Lb/a/b/b/a;->a(Ljava/lang/Class;)Lb/a/b/b/a;

    move-result-object v0

    goto/32 :goto_2e

    nop

    :goto_23
    const-string v0, "Invalid attempt to bind an instance of "

    goto/32 :goto_28

    nop

    :goto_24
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_3d

    nop

    :goto_25
    invoke-interface {p1}, Lb/a/b/a/A;->a()Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_2b

    nop

    :goto_26
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_27
    move-object v6, p3

    goto/32 :goto_2

    nop

    :goto_28
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_29
    invoke-virtual {p1}, Lb/a/b/J;->a()Lb/a/b/J;

    move-result-object p1

    :goto_2a
    goto/32 :goto_f

    nop

    :goto_2b
    instance-of v0, p1, Lb/a/b/J;

    goto/32 :goto_1d

    nop

    :goto_2c
    new-instance p4, Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_2d
    invoke-virtual {p3}, Lb/a/b/b/a;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_30

    nop

    :goto_2e
    invoke-virtual {p1, v0}, Lb/a/b/a/q;->a(Lb/a/b/b/a;)Lb/a/b/a/A;

    move-result-object p1

    goto/32 :goto_25

    nop

    :goto_2f
    if-nez v0, :cond_4

    goto/32 :goto_1

    :cond_4
    goto/32 :goto_b

    nop

    :goto_30
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_34

    nop

    :goto_31
    if-eqz v0, :cond_5

    goto/32 :goto_1a

    :cond_5
    goto/32 :goto_16

    nop

    :goto_32
    new-instance p1, Lb/a/b/a/a/v;

    goto/32 :goto_38

    nop

    :goto_33
    instance-of v0, p1, Lb/a/b/K;

    goto/32 :goto_2f

    nop

    :goto_34
    const-string p1, ". @JsonAdapter value must be a TypeAdapter, TypeAdapterFactory, JsonSerializer or JsonDeserializer."

    goto/32 :goto_a

    nop

    :goto_35
    new-instance p2, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_2c

    nop

    :goto_36
    invoke-interface {p1, p2, p3}, Lb/a/b/K;->a(Lb/a/b/q;Lb/a/b/b/a;)Lb/a/b/J;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_37
    check-cast p1, Lb/a/b/J;

    goto/32 :goto_1e

    nop

    :goto_38
    const/4 v7, 0x0

    goto/32 :goto_14

    nop

    :goto_39
    goto/16 :goto_1a

    :goto_3a
    goto/32 :goto_35

    nop

    :goto_3b
    if-nez p2, :cond_6

    goto/32 :goto_2a

    :cond_6
    goto/32 :goto_29

    nop

    :goto_3c
    if-nez v0, :cond_7

    goto/32 :goto_41

    :cond_7
    goto/32 :goto_e

    nop

    :goto_3d
    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_19

    nop

    :goto_3e
    invoke-interface {p4}, Lcom/google/gson/annotations/JsonAdapter;->nullSafe()Z

    move-result p2

    goto/32 :goto_3b

    nop

    :goto_3f
    instance-of v0, p1, Lb/a/b/D;

    goto/32 :goto_31

    nop

    :goto_40
    check-cast v1, Lb/a/b/v;

    :goto_41
    goto/32 :goto_7

    nop

    :goto_42
    invoke-interface {p4}, Lcom/google/gson/annotations/JsonAdapter;->value()Ljava/lang/Class;

    move-result-object v0

    goto/32 :goto_22

    nop
.end method

.method public a(Lb/a/b/q;Lb/a/b/b/a;)Lb/a/b/J;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lb/a/b/q;",
            "Lb/a/b/b/a<",
            "TT;>;)",
            "Lb/a/b/J<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p2}, Lb/a/b/b/a;->a()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/gson/annotations/JsonAdapter;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/gson/annotations/JsonAdapter;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v1, p0, Lb/a/b/a/a/f;->a:Lb/a/b/a/q;

    invoke-virtual {p0, v1, p1, p2, v0}, Lb/a/b/a/a/f;->a(Lb/a/b/a/q;Lb/a/b/q;Lb/a/b/b/a;Lcom/google/gson/annotations/JsonAdapter;)Lb/a/b/J;

    move-result-object p1

    return-object p1
.end method
