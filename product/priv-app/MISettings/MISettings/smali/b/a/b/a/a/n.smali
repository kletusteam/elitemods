.class public final Lb/a/b/a/a/n;
.super Lb/a/b/J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lb/a/b/J<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lb/a/b/K;


# instance fields
.field private final b:Lb/a/b/q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lb/a/b/a/a/l;

    invoke-direct {v0}, Lb/a/b/a/a/l;-><init>()V

    sput-object v0, Lb/a/b/a/a/n;->a:Lb/a/b/K;

    return-void
.end method

.method constructor <init>(Lb/a/b/q;)V
    .locals 0

    invoke-direct {p0}, Lb/a/b/J;-><init>()V

    iput-object p1, p0, Lb/a/b/a/a/n;->b:Lb/a/b/q;

    return-void
.end method


# virtual methods
.method public a(Lb/a/b/c/b;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lb/a/b/c/b;->C()Lb/a/b/c/c;

    move-result-object v0

    sget-object v1, Lb/a/b/a/a/m;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :pswitch_0
    invoke-virtual {p1}, Lb/a/b/c/b;->A()V

    const/4 p1, 0x0

    return-object p1

    :pswitch_1
    invoke-virtual {p1}, Lb/a/b/c/b;->v()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :pswitch_2
    invoke-virtual {p1}, Lb/a/b/c/b;->w()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    return-object p1

    :pswitch_3
    invoke-virtual {p1}, Lb/a/b/c/b;->B()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_4
    new-instance v0, Lb/a/b/a/y;

    invoke-direct {v0}, Lb/a/b/a/y;-><init>()V

    invoke-virtual {p1}, Lb/a/b/c/b;->b()V

    :goto_0
    invoke-virtual {p1}, Lb/a/b/c/b;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lb/a/b/c/b;->z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lb/a/b/a/a/n;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lb/a/b/c/b;->r()V

    return-object v0

    :pswitch_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lb/a/b/c/b;->a()V

    :goto_1
    invoke-virtual {p1}, Lb/a/b/c/b;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1}, Lb/a/b/a/a/n;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lb/a/b/c/b;->q()V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lb/a/b/c/d;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lb/a/b/c/d;->u()Lb/a/b/c/d;

    return-void

    :cond_0
    iget-object v0, p0, Lb/a/b/a/a/n;->b:Lb/a/b/q;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lb/a/b/q;->a(Ljava/lang/Class;)Lb/a/b/J;

    move-result-object v0

    instance-of v1, v0, Lb/a/b/a/a/n;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lb/a/b/c/d;->b()Lb/a/b/c/d;

    invoke-virtual {p1}, Lb/a/b/c/d;->q()Lb/a/b/c/d;

    return-void

    :cond_1
    invoke-virtual {v0, p1, p2}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    return-void
.end method
