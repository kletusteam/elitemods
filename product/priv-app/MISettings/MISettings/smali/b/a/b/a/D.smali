.class public final Lb/a/b/a/D;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/a/b/a/D$a;
    }
.end annotation


# direct methods
.method public static a(Lb/a/b/c/b;)Lb/a/b/w;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lb/a/b/A;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lb/a/b/c/b;->C()Lb/a/b/c/c;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lb/a/b/c/e; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v0, 0x0

    :try_start_1
    sget-object v1, Lb/a/b/a/a/ja;->X:Lb/a/b/J;

    invoke-virtual {v1, p0}, Lb/a/b/J;->a(Lb/a/b/c/b;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lb/a/b/w;
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lb/a/b/c/e; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    return-object p0

    :catch_0
    move-exception p0

    goto :goto_0

    :catch_1
    move-exception p0

    new-instance v0, Lb/a/b/E;

    invoke-direct {v0, p0}, Lb/a/b/E;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_2
    move-exception p0

    new-instance v0, Lb/a/b/x;

    invoke-direct {v0, p0}, Lb/a/b/x;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_3
    move-exception p0

    new-instance v0, Lb/a/b/E;

    invoke-direct {v0, p0}, Lb/a/b/E;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_4
    move-exception p0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    sget-object p0, Lb/a/b/y;->a:Lb/a/b/y;

    return-object p0

    :cond_0
    new-instance v0, Lb/a/b/E;

    invoke-direct {v0, p0}, Lb/a/b/E;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static a(Ljava/lang/Appendable;)Ljava/io/Writer;
    .locals 1

    instance-of v0, p0, Ljava/io/Writer;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/io/Writer;

    goto :goto_0

    :cond_0
    new-instance v0, Lb/a/b/a/D$a;

    invoke-direct {v0, p0}, Lb/a/b/a/D$a;-><init>(Ljava/lang/Appendable;)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method

.method public static a(Lb/a/b/w;Lb/a/b/c/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lb/a/b/a/a/ja;->X:Lb/a/b/J;

    invoke-virtual {v0, p1, p0}, Lb/a/b/J;->a(Lb/a/b/c/d;Ljava/lang/Object;)V

    return-void
.end method
