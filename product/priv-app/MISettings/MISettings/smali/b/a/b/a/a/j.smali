.class public final Lb/a/b/a/a/j;
.super Lb/a/b/c/d;


# static fields
.field private static final l:Ljava/io/Writer;

.field private static final m:Lb/a/b/B;


# instance fields
.field private final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb/a/b/w;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/String;

.field private p:Lb/a/b/w;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lb/a/b/a/a/i;

    invoke-direct {v0}, Lb/a/b/a/a/i;-><init>()V

    sput-object v0, Lb/a/b/a/a/j;->l:Ljava/io/Writer;

    new-instance v0, Lb/a/b/B;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Lb/a/b/B;-><init>(Ljava/lang/String;)V

    sput-object v0, Lb/a/b/a/a/j;->m:Lb/a/b/B;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lb/a/b/a/a/j;->l:Ljava/io/Writer;

    invoke-direct {p0, v0}, Lb/a/b/c/d;-><init>(Ljava/io/Writer;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    sget-object v0, Lb/a/b/y;->a:Lb/a/b/y;

    iput-object v0, p0, Lb/a/b/a/a/j;->p:Lb/a/b/w;

    return-void
.end method

.method private a(Lb/a/b/w;)V
    .locals 2

    iget-object v0, p0, Lb/a/b/a/a/j;->o:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lb/a/b/w;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/a/b/c/d;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lb/a/b/a/a/j;->w()Lb/a/b/w;

    move-result-object v0

    check-cast v0, Lb/a/b/z;

    iget-object v1, p0, Lb/a/b/a/a/j;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lb/a/b/z;->a(Ljava/lang/String;Lb/a/b/w;)V

    :cond_1
    const/4 p1, 0x0

    iput-object p1, p0, Lb/a/b/a/a/j;->o:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object p1, p0, Lb/a/b/a/a/j;->p:Lb/a/b/w;

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lb/a/b/a/a/j;->w()Lb/a/b/w;

    move-result-object v0

    instance-of v1, v0, Lb/a/b/t;

    if-eqz v1, :cond_4

    check-cast v0, Lb/a/b/t;

    invoke-virtual {v0, p1}, Lb/a/b/t;->a(Lb/a/b/w;)V

    :goto_0
    return-void

    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method private w()Lb/a/b/w;
    .locals 2

    iget-object v0, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/a/b/w;

    return-object v0
.end method


# virtual methods
.method public a()Lb/a/b/c/d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lb/a/b/t;

    invoke-direct {v0}, Lb/a/b/t;-><init>()V

    invoke-direct {p0, v0}, Lb/a/b/a/a/j;->a(Lb/a/b/w;)V

    iget-object v1, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Ljava/lang/Boolean;)Lb/a/b/c/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lb/a/b/a/a/j;->u()Lb/a/b/c/d;

    return-object p0

    :cond_0
    new-instance v0, Lb/a/b/B;

    invoke-direct {v0, p1}, Lb/a/b/B;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {p0, v0}, Lb/a/b/a/a/j;->a(Lb/a/b/w;)V

    return-object p0
.end method

.method public a(Ljava/lang/Number;)Lb/a/b/c/d;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lb/a/b/a/a/j;->u()Lb/a/b/c/d;

    return-object p0

    :cond_0
    invoke-virtual {p0}, Lb/a/b/c/d;->t()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSON forbids NaN and infinities: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    new-instance v0, Lb/a/b/B;

    invoke-direct {v0, p1}, Lb/a/b/B;-><init>(Ljava/lang/Number;)V

    invoke-direct {p0, v0}, Lb/a/b/a/a/j;->a(Lb/a/b/w;)V

    return-object p0
.end method

.method public b()Lb/a/b/c/d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lb/a/b/z;

    invoke-direct {v0}, Lb/a/b/z;-><init>()V

    invoke-direct {p0, v0}, Lb/a/b/a/a/j;->a(Lb/a/b/w;)V

    iget-object v1, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    sget-object v1, Lb/a/b/a/a/j;->m:Lb/a/b/B;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incomplete document"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d(Z)Lb/a/b/c/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lb/a/b/B;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {v0, p1}, Lb/a/b/B;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {p0, v0}, Lb/a/b/a/a/j;->a(Lb/a/b/w;)V

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lb/a/b/c/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_2

    iget-object v0, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/a/b/a/a/j;->o:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lb/a/b/a/a/j;->w()Lb/a/b/w;

    move-result-object v0

    instance-of v0, v0, Lb/a/b/z;

    if-eqz v0, :cond_0

    iput-object p1, p0, Lb/a/b/a/a/j;->o:Ljava/lang/String;

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "name == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public flush()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public g(Ljava/lang/String;)Lb/a/b/c/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lb/a/b/a/a/j;->u()Lb/a/b/c/d;

    return-object p0

    :cond_0
    new-instance v0, Lb/a/b/B;

    invoke-direct {v0, p1}, Lb/a/b/B;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lb/a/b/a/a/j;->a(Lb/a/b/w;)V

    return-object p0
.end method

.method public h(J)Lb/a/b/c/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lb/a/b/B;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-direct {v0, p1}, Lb/a/b/B;-><init>(Ljava/lang/Number;)V

    invoke-direct {p0, v0}, Lb/a/b/a/a/j;->a(Lb/a/b/w;)V

    return-object p0
.end method

.method public p()Lb/a/b/c/d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/a/b/a/a/j;->o:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lb/a/b/a/a/j;->w()Lb/a/b/w;

    move-result-object v0

    instance-of v0, v0, Lb/a/b/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public q()Lb/a/b/c/d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/a/b/a/a/j;->o:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lb/a/b/a/a/j;->w()Lb/a/b/w;

    move-result-object v0

    instance-of v0, v0, Lb/a/b/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public u()Lb/a/b/c/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lb/a/b/y;->a:Lb/a/b/y;

    invoke-direct {p0, v0}, Lb/a/b/a/a/j;->a(Lb/a/b/w;)V

    return-object p0
.end method

.method public v()Lb/a/b/w;
    .locals 3

    iget-object v0, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/a/b/a/a/j;->p:Lb/a/b/w;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected one JSON element but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lb/a/b/a/a/j;->n:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
