.class final Lb/a/b/a/y$b;
.super Ljava/util/AbstractSet;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb/a/b/a/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet<",
        "TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lb/a/b/a/y;


# direct methods
.method constructor <init>(Lb/a/b/a/y;)V
    .locals 0

    iput-object p1, p0, Lb/a/b/a/y$b;->a:Lb/a/b/a/y;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lb/a/b/a/y$b;->a:Lb/a/b/a/y;

    invoke-virtual {v0}, Lb/a/b/a/y;->clear()V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lb/a/b/a/y$b;->a:Lb/a/b/a/y;

    invoke-virtual {v0, p1}, Lb/a/b/a/y;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TK;>;"
        }
    .end annotation

    new-instance v0, Lb/a/b/a/z;

    invoke-direct {v0, p0}, Lb/a/b/a/z;-><init>(Lb/a/b/a/y$b;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lb/a/b/a/y$b;->a:Lb/a/b/a/y;

    invoke-virtual {v0, p1}, Lb/a/b/a/y;->b(Ljava/lang/Object;)Lb/a/b/a/y$d;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lb/a/b/a/y$b;->a:Lb/a/b/a/y;

    iget v0, v0, Lb/a/b/a/y;->d:I

    return v0
.end method
