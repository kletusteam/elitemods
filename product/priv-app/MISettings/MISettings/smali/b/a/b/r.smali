.class public final Lb/a/b/r;
.super Ljava/lang/Object;


# instance fields
.field private a:Lb/a/b/a/s;

.field private b:Lb/a/b/H;

.field private c:Lb/a/b/k;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/reflect/Type;",
            "Lb/a/b/s<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb/a/b/K;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb/a/b/K;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lb/a/b/a/s;->a:Lb/a/b/a/s;

    iput-object v0, p0, Lb/a/b/r;->a:Lb/a/b/a/s;

    sget-object v0, Lb/a/b/H;->a:Lb/a/b/H;

    iput-object v0, p0, Lb/a/b/r;->b:Lb/a/b/H;

    sget-object v0, Lb/a/b/j;->a:Lb/a/b/j;

    iput-object v0, p0, Lb/a/b/r;->c:Lb/a/b/k;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lb/a/b/r;->d:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/a/b/r;->e:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/a/b/r;->f:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/a/b/r;->g:Z

    const/4 v1, 0x2

    iput v1, p0, Lb/a/b/r;->i:I

    iput v1, p0, Lb/a/b/r;->j:I

    iput-boolean v0, p0, Lb/a/b/r;->k:Z

    iput-boolean v0, p0, Lb/a/b/r;->l:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lb/a/b/r;->m:Z

    iput-boolean v0, p0, Lb/a/b/r;->n:Z

    iput-boolean v0, p0, Lb/a/b/r;->o:Z

    iput-boolean v0, p0, Lb/a/b/r;->p:Z

    return-void
.end method

.method private a(Ljava/lang/String;IILjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List<",
            "Lb/a/b/K;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance p2, Lb/a/b/a;

    const-class p3, Ljava/util/Date;

    invoke-direct {p2, p3, p1}, Lb/a/b/a;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    new-instance p3, Lb/a/b/a;

    const-class v0, Ljava/sql/Timestamp;

    invoke-direct {p3, v0, p1}, Lb/a/b/a;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    new-instance v0, Lb/a/b/a;

    const-class v1, Ljava/sql/Date;

    invoke-direct {v0, v1, p1}, Lb/a/b/a;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    move-object p1, p2

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    if-eq p2, p1, :cond_1

    if-eq p3, p1, :cond_1

    new-instance p1, Lb/a/b/a;

    const-class v0, Ljava/util/Date;

    invoke-direct {p1, v0, p2, p3}, Lb/a/b/a;-><init>(Ljava/lang/Class;II)V

    new-instance v0, Lb/a/b/a;

    const-class v1, Ljava/sql/Timestamp;

    invoke-direct {v0, v1, p2, p3}, Lb/a/b/a;-><init>(Ljava/lang/Class;II)V

    new-instance v1, Lb/a/b/a;

    const-class v2, Ljava/sql/Date;

    invoke-direct {v1, v2, p2, p3}, Lb/a/b/a;-><init>(Ljava/lang/Class;II)V

    move-object p3, v0

    move-object v0, v1

    :goto_0
    const-class p2, Ljava/util/Date;

    invoke-static {p2, p1}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object p1

    invoke-interface {p4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class p1, Ljava/sql/Timestamp;

    invoke-static {p1, p3}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object p1

    invoke-interface {p4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class p1, Ljava/sql/Date;

    invoke-static {p1, v0}, Lb/a/b/a/a/ja;->a(Ljava/lang/Class;Lb/a/b/J;)Lb/a/b/K;

    move-result-object p1

    invoke-interface {p4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method


# virtual methods
.method public a()Lb/a/b/q;
    .locals 21

    move-object/from16 v0, p0

    new-instance v1, Ljava/util/ArrayList;

    move-object/from16 v18, v1

    iget-object v2, v0, Lb/a/b/r;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, v0, Lb/a/b/r;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v2, v0, Lb/a/b/r;->e:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, v0, Lb/a/b/r;->f:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v2, v0, Lb/a/b/r;->h:Ljava/lang/String;

    iget v3, v0, Lb/a/b/r;->i:I

    iget v4, v0, Lb/a/b/r;->j:I

    invoke-direct {v0, v2, v3, v4, v1}, Lb/a/b/r;->a(Ljava/lang/String;IILjava/util/List;)V

    new-instance v19, Lb/a/b/q;

    move-object/from16 v1, v19

    iget-object v2, v0, Lb/a/b/r;->a:Lb/a/b/a/s;

    iget-object v3, v0, Lb/a/b/r;->c:Lb/a/b/k;

    iget-object v4, v0, Lb/a/b/r;->d:Ljava/util/Map;

    iget-boolean v5, v0, Lb/a/b/r;->g:Z

    iget-boolean v6, v0, Lb/a/b/r;->k:Z

    iget-boolean v7, v0, Lb/a/b/r;->o:Z

    iget-boolean v8, v0, Lb/a/b/r;->m:Z

    iget-boolean v9, v0, Lb/a/b/r;->n:Z

    iget-boolean v10, v0, Lb/a/b/r;->p:Z

    iget-boolean v11, v0, Lb/a/b/r;->l:Z

    iget-object v12, v0, Lb/a/b/r;->b:Lb/a/b/H;

    iget-object v13, v0, Lb/a/b/r;->h:Ljava/lang/String;

    iget v14, v0, Lb/a/b/r;->i:I

    iget v15, v0, Lb/a/b/r;->j:I

    move-object/from16 v20, v1

    iget-object v1, v0, Lb/a/b/r;->e:Ljava/util/List;

    move-object/from16 v16, v1

    iget-object v1, v0, Lb/a/b/r;->f:Ljava/util/List;

    move-object/from16 v17, v1

    move-object/from16 v1, v20

    invoke-direct/range {v1 .. v18}, Lb/a/b/q;-><init>(Lb/a/b/a/s;Lb/a/b/k;Ljava/util/Map;ZZZZZZZLb/a/b/H;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v19
.end method

.method public varargs a([I)Lb/a/b/r;
    .locals 1

    iget-object v0, p0, Lb/a/b/r;->a:Lb/a/b/a/s;

    invoke-virtual {v0, p1}, Lb/a/b/a/s;->a([I)Lb/a/b/a/s;

    move-result-object p1

    iput-object p1, p0, Lb/a/b/r;->a:Lb/a/b/a/s;

    return-object p0
.end method
