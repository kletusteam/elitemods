.class final Ld/k/a/a/a/t$a;
.super Ljava/lang/Object;

# interfaces
.implements Ld/k/a/a/a/w;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/k/a/a/a/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private a:F

.field private b:F

.field private final c:Ld/k/a/a/a/s$a;

.field private d:D

.field private final e:F


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, -0x3f79999a    # -4.2f

    iput v0, p0, Ld/k/a/a/a/t$a;->a:F

    new-instance v0, Ld/k/a/a/a/s$a;

    invoke-direct {v0}, Ld/k/a/a/a/s$a;-><init>()V

    iput-object v0, p0, Ld/k/a/a/a/t$a;->c:Ld/k/a/a/a/s$a;

    const/high16 v0, 0x447a0000    # 1000.0f

    iput v0, p0, Ld/k/a/a/a/t$a;->e:F

    return-void
.end method

.method static synthetic a(Ld/k/a/a/a/t$a;)F
    .locals 0

    iget p0, p0, Ld/k/a/a/a/t$a;->a:F

    return p0
.end method

.method static synthetic b(Ld/k/a/a/a/t$a;)F
    .locals 0

    iget p0, p0, Ld/k/a/a/a/t$a;->b:F

    return p0
.end method


# virtual methods
.method a(FFJ)Ld/k/a/a/a/s$a;
    .locals 4

    goto/32 :goto_14

    nop

    :goto_0
    const/4 p2, 0x0

    goto/32 :goto_11

    nop

    :goto_1
    float-to-double v0, p3

    goto/32 :goto_9

    nop

    :goto_2
    iget-object p4, p0, Ld/k/a/a/a/t$a;->c:Ld/k/a/a/a/s$a;

    goto/32 :goto_4

    nop

    :goto_3
    const/high16 p4, 0x447a0000    # 1000.0f

    goto/32 :goto_10

    nop

    :goto_4
    float-to-double v2, p2

    goto/32 :goto_b

    nop

    :goto_5
    iput p2, p4, Ld/k/a/a/a/s$a;->b:F

    goto/32 :goto_7

    nop

    :goto_6
    double-to-float p2, v2

    goto/32 :goto_5

    nop

    :goto_7
    iget p2, p4, Ld/k/a/a/a/s$a;->b:F

    goto/32 :goto_18

    nop

    :goto_8
    iget-object p1, p0, Ld/k/a/a/a/t$a;->c:Ld/k/a/a/a/s$a;

    goto/32 :goto_0

    nop

    :goto_9
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    goto/32 :goto_2

    nop

    :goto_a
    add-float/2addr p1, p3

    goto/32 :goto_13

    nop

    :goto_b
    mul-double/2addr v2, v0

    goto/32 :goto_6

    nop

    :goto_c
    return-object p1

    :goto_d
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_f

    nop

    :goto_e
    long-to-float p3, p3

    goto/32 :goto_3

    nop

    :goto_f
    sub-double/2addr v2, v0

    goto/32 :goto_e

    nop

    :goto_10
    div-float/2addr p3, p4

    goto/32 :goto_1

    nop

    :goto_11
    iput p2, p1, Ld/k/a/a/a/s$a;->b:F

    :goto_12
    goto/32 :goto_15

    nop

    :goto_13
    iput p1, p4, Ld/k/a/a/a/s$a;->a:F

    goto/32 :goto_17

    nop

    :goto_14
    const-wide/16 v0, 0x10

    goto/32 :goto_16

    nop

    :goto_15
    iget-object p1, p0, Ld/k/a/a/a/t$a;->c:Ld/k/a/a/a/s$a;

    goto/32 :goto_c

    nop

    :goto_16
    invoke-static {p3, p4, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p3

    goto/32 :goto_19

    nop

    :goto_17
    iget p1, p4, Ld/k/a/a/a/s$a;->a:F

    goto/32 :goto_1b

    nop

    :goto_18
    mul-float/2addr p3, p2

    goto/32 :goto_a

    nop

    :goto_19
    iget-wide v0, p0, Ld/k/a/a/a/t$a;->d:D

    goto/32 :goto_d

    nop

    :goto_1a
    if-nez p1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_8

    nop

    :goto_1b
    invoke-virtual {p0, p1, p2}, Ld/k/a/a/a/t$a;->a(FF)Z

    move-result p1

    goto/32 :goto_1a

    nop
.end method

.method a(F)V
    .locals 4

    goto/32 :goto_a

    nop

    :goto_0
    float-to-double v0, p1

    goto/32 :goto_7

    nop

    :goto_1
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    goto/32 :goto_4

    nop

    :goto_2
    iput-wide v2, p0, Ld/k/a/a/a/t$a;->d:D

    goto/32 :goto_6

    nop

    :goto_3
    iput p1, p0, Ld/k/a/a/a/t$a;->a:F

    goto/32 :goto_9

    nop

    :goto_4
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_5

    nop

    :goto_5
    sub-double/2addr v2, v0

    goto/32 :goto_2

    nop

    :goto_6
    return-void

    :goto_7
    const-wide v2, 0x4005bf0a8b145769L    # Math.E

    goto/32 :goto_1

    nop

    :goto_8
    mul-float/2addr p1, v0

    goto/32 :goto_3

    nop

    :goto_9
    iget p1, p0, Ld/k/a/a/a/t$a;->a:F

    goto/32 :goto_0

    nop

    :goto_a
    const v0, -0x3f79999a    # -4.2f

    goto/32 :goto_8

    nop
.end method

.method public a(FF)Z
    .locals 0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p1

    iget p2, p0, Ld/k/a/a/a/t$a;->b:F

    cmpg-float p1, p1, p2

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method b(F)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    mul-float/2addr p1, v0

    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    const/high16 v0, 0x427a0000    # 62.5f

    goto/32 :goto_0

    nop

    :goto_3
    iput p1, p0, Ld/k/a/a/a/t$a;->b:F

    goto/32 :goto_1

    nop
.end method
