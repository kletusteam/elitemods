.class public abstract Ld/k/a/a/a/s;
.super Ljava/lang/Object;

# interfaces
.implements Ld/k/a/a/a/a$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld/k/a/a/a/s$c;,
        Ld/k/a/a/a/s$b;,
        Ld/k/a/a/a/s$a;,
        Ld/k/a/a/a/s$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ld/k/a/a/a/s<",
        "TT;>;>",
        "Ljava/lang/Object;",
        "Ld/k/a/a/a/a$b;"
    }
.end annotation


# static fields
.field public static final a:Ld/k/a/a/a/s$d;

.field public static final b:Ld/k/a/a/a/s$d;

.field public static final c:Ld/k/a/a/a/s$d;

.field public static final d:Ld/k/a/a/a/s$d;

.field public static final e:Ld/k/a/a/a/s$d;

.field public static final f:Ld/k/a/a/a/s$d;

.field public static final g:Ld/k/a/a/a/s$d;

.field public static final h:Ld/k/a/a/a/s$d;

.field public static final i:Ld/k/a/a/a/s$d;

.field public static final j:Ld/k/a/a/a/s$d;

.field public static final k:Ld/k/a/a/a/s$d;

.field public static final l:Ld/k/a/a/a/s$d;

.field public static final m:Ld/k/a/a/a/s$d;

.field public static final n:Ld/k/a/a/a/s$d;


# instance fields
.field private A:Z

.field o:F

.field p:F

.field q:Z

.field final r:Ljava/lang/Object;

.field final s:Ld/k/a/a/a/u;

.field t:Z

.field u:F

.field v:F

.field private w:J

.field private x:F

.field private final y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ld/k/a/a/a/s$b;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ld/k/a/a/a/s$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ld/k/a/a/a/j;

    const-string v1, "translationX"

    invoke-direct {v0, v1}, Ld/k/a/a/a/j;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->a:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/k;

    const-string v1, "translationY"

    invoke-direct {v0, v1}, Ld/k/a/a/a/k;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->b:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/l;

    const-string v1, "translationZ"

    invoke-direct {v0, v1}, Ld/k/a/a/a/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->c:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/m;

    const-string v1, "scaleX"

    invoke-direct {v0, v1}, Ld/k/a/a/a/m;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->d:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/n;

    const-string v1, "scaleY"

    invoke-direct {v0, v1}, Ld/k/a/a/a/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->e:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/o;

    const-string v1, "rotation"

    invoke-direct {v0, v1}, Ld/k/a/a/a/o;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->f:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/p;

    const-string v1, "rotationX"

    invoke-direct {v0, v1}, Ld/k/a/a/a/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->g:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/q;

    const-string v1, "rotationY"

    invoke-direct {v0, v1}, Ld/k/a/a/a/q;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->h:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/r;

    const-string v1, "x"

    invoke-direct {v0, v1}, Ld/k/a/a/a/r;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->i:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/d;

    const-string v1, "y"

    invoke-direct {v0, v1}, Ld/k/a/a/a/d;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->j:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/e;

    const-string v1, "z"

    invoke-direct {v0, v1}, Ld/k/a/a/a/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->k:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/f;

    const-string v1, "alpha"

    invoke-direct {v0, v1}, Ld/k/a/a/a/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->l:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/g;

    const-string v1, "scrollX"

    invoke-direct {v0, v1}, Ld/k/a/a/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->m:Ld/k/a/a/a/s$d;

    new-instance v0, Ld/k/a/a/a/h;

    const-string v1, "scrollY"

    invoke-direct {v0, v1}, Ld/k/a/a/a/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld/k/a/a/a/s;->n:Ld/k/a/a/a/s$d;

    return-void
.end method

.method constructor <init>(Ld/k/a/a/a/v;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ld/k/a/a/a/s;->o:F

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Ld/k/a/a/a/s;->p:F

    const/4 v1, 0x0

    iput-boolean v1, p0, Ld/k/a/a/a/s;->q:Z

    iput-boolean v1, p0, Ld/k/a/a/a/s;->t:Z

    iput v0, p0, Ld/k/a/a/a/s;->u:F

    iget v0, p0, Ld/k/a/a/a/s;->u:F

    neg-float v0, v0

    iput v0, p0, Ld/k/a/a/a/s;->v:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ld/k/a/a/a/s;->w:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ld/k/a/a/a/s;->y:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ld/k/a/a/a/s;->z:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Ld/k/a/a/a/s;->r:Ljava/lang/Object;

    new-instance v0, Ld/k/a/a/a/i;

    const-string v1, "FloatValueHolder"

    invoke-direct {v0, p0, v1, p1}, Ld/k/a/a/a/i;-><init>(Ld/k/a/a/a/s;Ljava/lang/String;Ld/k/a/a/a/v;)V

    iput-object v0, p0, Ld/k/a/a/a/s;->s:Ld/k/a/a/a/u;

    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Ld/k/a/a/a/s;->x:F

    return-void
.end method

.method private static a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList<",
            "TT;>;)V"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static a(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList<",
            "TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Ld/k/a/a/a/s;->t:Z

    iget-boolean v1, p0, Ld/k/a/a/a/s;->A:Z

    if-nez v1, :cond_0

    invoke-static {}, Ld/k/a/a/a/a;->a()Ld/k/a/a/a/a;

    move-result-object v1

    invoke-virtual {v1, p0}, Ld/k/a/a/a/a;->a(Ld/k/a/a/a/a$b;)V

    :cond_0
    iput-boolean v0, p0, Ld/k/a/a/a/s;->A:Z

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Ld/k/a/a/a/s;->w:J

    iput-boolean v0, p0, Ld/k/a/a/a/s;->q:Z

    :goto_0
    iget-object v1, p0, Ld/k/a/a/a/s;->y:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ld/k/a/a/a/s;->y:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Ld/k/a/a/a/s;->y:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ld/k/a/a/a/s$b;

    iget v2, p0, Ld/k/a/a/a/s;->p:F

    iget v3, p0, Ld/k/a/a/a/s;->o:F

    invoke-interface {v1, p0, p1, v2, v3}, Ld/k/a/a/a/s$b;->a(Ld/k/a/a/a/s;ZFF)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Ld/k/a/a/a/s;->y:Ljava/util/ArrayList;

    invoke-static {p1}, Ld/k/a/a/a/s;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method private c(Z)V
    .locals 2

    iget-boolean v0, p0, Ld/k/a/a/a/s;->t:Z

    if-nez v0, :cond_2

    iput-boolean p1, p0, Ld/k/a/a/a/s;->A:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld/k/a/a/a/s;->t:Z

    iget-boolean v0, p0, Ld/k/a/a/a/s;->q:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Ld/k/a/a/a/s;->d()F

    move-result v0

    iput v0, p0, Ld/k/a/a/a/s;->p:F

    :cond_0
    iget v0, p0, Ld/k/a/a/a/s;->p:F

    iget v1, p0, Ld/k/a/a/a/s;->u:F

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_1

    iget v1, p0, Ld/k/a/a/a/s;->v:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    if-nez p1, :cond_2

    invoke-static {}, Ld/k/a/a/a/a;->a()Ld/k/a/a/a/a;

    move-result-object p1

    const-wide/16 v0, 0x0

    invoke-virtual {p1, p0, v0, v1}, Ld/k/a/a/a/a;->a(Ld/k/a/a/a/a$b;J)V

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Starting value("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Ld/k/a/a/a/s;->p:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ") need to be in between min value("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Ld/k/a/a/a/s;->v:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ") and max value("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Ld/k/a/a/a/s;->u:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_0
    return-void
.end method

.method private d()F
    .locals 2

    iget-object v0, p0, Ld/k/a/a/a/s;->s:Ld/k/a/a/a/u;

    iget-object v1, p0, Ld/k/a/a/a/s;->r:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ld/k/a/a/a/u;->a(Ljava/lang/Object;)F

    move-result v0

    return v0
.end method


# virtual methods
.method public a(F)Ld/k/a/a/a/s;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Ld/k/a/a/a/s;->u:F

    return-object p0
.end method

.method public a(Ld/k/a/a/a/s$c;)Ld/k/a/a/a/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld/k/a/a/a/s$c;",
            ")TT;"
        }
    .end annotation

    invoke-virtual {p0}, Ld/k/a/a/a/s;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ld/k/a/a/a/s;->z:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ld/k/a/a/a/s;->z:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0

    :cond_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Error: Update listeners must be added beforethe animation."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Ld/k/a/a/a/s;->t:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ld/k/a/a/a/s;->b(Z)V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be canceled on the main thread"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Z)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Ld/k/a/a/a/s;->t:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Ld/k/a/a/a/s;->c(Z)V

    :cond_0
    return-void

    :cond_1
    new-instance p1, Landroid/util/AndroidRuntimeException;

    const-string v0, "Animations may only be started on the main thread"

    invoke-direct {p1, v0}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(J)Z
    .locals 4

    iget-wide v0, p0, Ld/k/a/a/a/s;->w:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    iput-wide p1, p0, Ld/k/a/a/a/s;->w:J

    iget p1, p0, Ld/k/a/a/a/s;->p:F

    invoke-virtual {p0, p1}, Ld/k/a/a/a/s;->d(F)V

    return v3

    :cond_0
    sub-long v0, p1, v0

    iput-wide p1, p0, Ld/k/a/a/a/s;->w:J

    invoke-virtual {p0, v0, v1}, Ld/k/a/a/a/s;->b(J)Z

    move-result p1

    iget p2, p0, Ld/k/a/a/a/s;->p:F

    iget v0, p0, Ld/k/a/a/a/s;->u:F

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result p2

    iput p2, p0, Ld/k/a/a/a/s;->p:F

    iget p2, p0, Ld/k/a/a/a/s;->p:F

    iget v0, p0, Ld/k/a/a/a/s;->v:F

    invoke-static {p2, v0}, Ljava/lang/Math;->max(FF)F

    move-result p2

    iput p2, p0, Ld/k/a/a/a/s;->p:F

    iget p2, p0, Ld/k/a/a/a/s;->p:F

    invoke-virtual {p0, p2}, Ld/k/a/a/a/s;->d(F)V

    if-eqz p1, :cond_1

    invoke-direct {p0, v3}, Ld/k/a/a/a/s;->b(Z)V

    :cond_1
    return p1
.end method

.method b()F
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    mul-float/2addr v0, v1

    goto/32 :goto_1

    nop

    :goto_1
    return v0

    :goto_2
    const/high16 v1, 0x3f400000    # 0.75f

    goto/32 :goto_0

    nop

    :goto_3
    iget v0, p0, Ld/k/a/a/a/s;->x:F

    goto/32 :goto_2

    nop
.end method

.method public b(F)Ld/k/a/a/a/s;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Ld/k/a/a/a/s;->v:F

    return-object p0
.end method

.method public b(Ld/k/a/a/a/s$c;)V
    .locals 1

    iget-object v0, p0, Ld/k/a/a/a/s;->z:Ljava/util/ArrayList;

    invoke-static {v0, p1}, Ld/k/a/a/a/s;->a(Ljava/util/ArrayList;Ljava/lang/Object;)V

    return-void
.end method

.method abstract b(J)Z
.end method

.method public c(F)Ld/k/a/a/a/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    iput p1, p0, Ld/k/a/a/a/s;->x:F

    const/high16 v0, 0x3f400000    # 0.75f

    mul-float/2addr p1, v0

    invoke-virtual {p0, p1}, Ld/k/a/a/a/s;->g(F)V

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Minimum visible change must be positive."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Ld/k/a/a/a/s;->t:Z

    return v0
.end method

.method d(F)V
    .locals 3

    goto/32 :goto_10

    nop

    :goto_0
    if-lt p1, v0, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_17

    nop

    :goto_1
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_12

    nop

    :goto_2
    iget-object v0, p0, Ld/k/a/a/a/s;->z:Ljava/util/ArrayList;

    goto/32 :goto_9

    nop

    :goto_3
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_2

    nop

    :goto_4
    invoke-interface {v0, p0, v1, v2}, Ld/k/a/a/a/s$c;->a(Ld/k/a/a/a/s;FF)V

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    check-cast v0, Ld/k/a/a/a/s$c;

    goto/32 :goto_14

    nop

    :goto_7
    iget-object v1, p0, Ld/k/a/a/a/s;->r:Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_8
    invoke-static {p1}, Ld/k/a/a/a/s;->a(Ljava/util/ArrayList;)V

    goto/32 :goto_f

    nop

    :goto_9
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_a
    iget-object p1, p0, Ld/k/a/a/a/s;->z:Ljava/util/ArrayList;

    goto/32 :goto_8

    nop

    :goto_b
    invoke-virtual {v0, v1, p1}, Ld/k/a/a/a/u;->a(Ljava/lang/Object;F)V

    goto/32 :goto_15

    nop

    :goto_c
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_e
    iget v2, p0, Ld/k/a/a/a/s;->o:F

    goto/32 :goto_4

    nop

    :goto_f
    return-void

    :goto_10
    iget-object v0, p0, Ld/k/a/a/a/s;->s:Ld/k/a/a/a/u;

    goto/32 :goto_7

    nop

    :goto_11
    iget-object v0, p0, Ld/k/a/a/a/s;->z:Ljava/util/ArrayList;

    goto/32 :goto_c

    nop

    :goto_12
    goto :goto_16

    :goto_13
    goto/32 :goto_a

    nop

    :goto_14
    iget v1, p0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_e

    nop

    :goto_15
    const/4 p1, 0x0

    :goto_16
    goto/32 :goto_11

    nop

    :goto_17
    iget-object v0, p0, Ld/k/a/a/a/s;->z:Ljava/util/ArrayList;

    goto/32 :goto_d

    nop
.end method

.method public e(F)Ld/k/a/a/a/s;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Ld/k/a/a/a/s;->p:F

    const/4 p1, 0x1

    iput-boolean p1, p0, Ld/k/a/a/a/s;->q:Z

    return-object p0
.end method

.method public f(F)Ld/k/a/a/a/s;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Ld/k/a/a/a/s;->o:F

    return-object p0
.end method

.method abstract g(F)V
.end method
