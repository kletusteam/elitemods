.class Ld/k/a/a/a/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld/k/a/a/a/a$c;,
        Ld/k/a/a/a/a$d;,
        Ld/k/a/a/a/a$e;,
        Ld/k/a/a/a/a$a;,
        Ld/k/a/a/a/a$b;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ld/k/a/a/a/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ld/k/a/a/a/a$b;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ld/k/a/a/a/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ld/k/a/a/a/a$a;

.field private e:Ld/k/a/a/a/a$c;

.field f:J

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Ld/k/a/a/a/a;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Ld/k/a/a/a/a;->b:Landroid/util/ArrayMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ld/k/a/a/a/a;->c:Ljava/util/ArrayList;

    new-instance v0, Ld/k/a/a/a/a$a;

    invoke-direct {v0, p0}, Ld/k/a/a/a/a$a;-><init>(Ld/k/a/a/a/a;)V

    iput-object v0, p0, Ld/k/a/a/a/a;->d:Ld/k/a/a/a/a$a;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ld/k/a/a/a/a;->f:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Ld/k/a/a/a/a;->g:Z

    return-void
.end method

.method public static a()Ld/k/a/a/a/a;
    .locals 2

    sget-object v0, Ld/k/a/a/a/a;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Ld/k/a/a/a/a;->a:Ljava/lang/ThreadLocal;

    new-instance v1, Ld/k/a/a/a/a;

    invoke-direct {v1}, Ld/k/a/a/a/a;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    sget-object v0, Ld/k/a/a/a/a;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld/k/a/a/a/a;

    return-object v0
.end method

.method private b(Ld/k/a/a/a/a$b;J)Z
    .locals 4

    iget-object v0, p0, Ld/k/a/a/a/a;->b:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long p2, v2, p2

    if-gez p2, :cond_1

    iget-object p2, p0, Ld/k/a/a/a/a;->b:Landroid/util/ArrayMap;

    invoke-virtual {p2, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private c()V
    .locals 2

    iget-boolean v0, p0, Ld/k/a/a/a/a;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Ld/k/a/a/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Ld/k/a/a/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ld/k/a/a/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Ld/k/a/a/a/a;->g:Z

    :cond_2
    return-void
.end method


# virtual methods
.method a(J)V
    .locals 5

    goto/32 :goto_11

    nop

    :goto_0
    goto :goto_7

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    invoke-interface {v3, p1, p2}, Ld/k/a/a/a/a$b;->a(J)Z

    :goto_3
    goto/32 :goto_a

    nop

    :goto_4
    if-lt v2, v3, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_8

    nop

    :goto_5
    check-cast v3, Ld/k/a/a/a/a$b;

    goto/32 :goto_c

    nop

    :goto_6
    const/4 v2, 0x0

    :goto_7
    goto/32 :goto_13

    nop

    :goto_8
    iget-object v3, p0, Ld/k/a/a/a/a;->c:Ljava/util/ArrayList;

    goto/32 :goto_12

    nop

    :goto_9
    invoke-direct {p0}, Ld/k/a/a/a/a;->c()V

    goto/32 :goto_b

    nop

    :goto_a
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_0

    nop

    :goto_b
    return-void

    :goto_c
    if-eqz v3, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_e

    nop

    :goto_d
    if-nez v4, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_2

    nop

    :goto_e
    goto :goto_3

    :goto_f
    goto/32 :goto_14

    nop

    :goto_10
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto/32 :goto_4

    nop

    :goto_11
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_12
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_5

    nop

    :goto_13
    iget-object v3, p0, Ld/k/a/a/a/a;->c:Ljava/util/ArrayList;

    goto/32 :goto_10

    nop

    :goto_14
    invoke-direct {p0, v3, v0, v1}, Ld/k/a/a/a/a;->b(Ld/k/a/a/a/a$b;J)Z

    move-result v4

    goto/32 :goto_d

    nop
.end method

.method public a(Ld/k/a/a/a/a$b;)V
    .locals 2

    iget-object v0, p0, Ld/k/a/a/a/a;->b:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ld/k/a/a/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    iget-object v0, p0, Ld/k/a/a/a/a;->c:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    iput-boolean p1, p0, Ld/k/a/a/a/a;->g:Z

    :cond_0
    return-void
.end method

.method public a(Ld/k/a/a/a/a$b;J)V
    .locals 3

    iget-object v0, p0, Ld/k/a/a/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ld/k/a/a/a/a;->b()Ld/k/a/a/a/a$c;

    move-result-object v0

    invoke-virtual {v0}, Ld/k/a/a/a/a$c;->a()V

    :cond_0
    iget-object v0, p0, Ld/k/a/a/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ld/k/a/a/a/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_2

    iget-object v0, p0, Ld/k/a/a/a/a;->b:Landroid/util/ArrayMap;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    add-long/2addr v1, p2

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-void
.end method

.method b()Ld/k/a/a/a/a$c;
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    new-instance v0, Ld/k/a/a/a/a$e;

    goto/32 :goto_4

    nop

    :goto_1
    invoke-direct {v0, v1}, Ld/k/a/a/a/a$d;-><init>(Ld/k/a/a/a/a$a;)V

    goto/32 :goto_5

    nop

    :goto_2
    iget-object v0, p0, Ld/k/a/a/a/a;->e:Ld/k/a/a/a/a$c;

    goto/32 :goto_d

    nop

    :goto_3
    const/16 v1, 0x10

    goto/32 :goto_e

    nop

    :goto_4
    iget-object v1, p0, Ld/k/a/a/a/a;->d:Ld/k/a/a/a/a$a;

    goto/32 :goto_f

    nop

    :goto_5
    iput-object v0, p0, Ld/k/a/a/a/a;->e:Ld/k/a/a/a/a$c;

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    iput-object v0, p0, Ld/k/a/a/a/a;->e:Ld/k/a/a/a/a$c;

    goto/32 :goto_b

    nop

    :goto_8
    iget-object v0, p0, Ld/k/a/a/a/a;->e:Ld/k/a/a/a/a$c;

    goto/32 :goto_10

    nop

    :goto_9
    new-instance v0, Ld/k/a/a/a/a$d;

    goto/32 :goto_11

    nop

    :goto_a
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    goto/32 :goto_3

    nop

    :goto_b
    goto :goto_6

    :goto_c
    goto/32 :goto_9

    nop

    :goto_d
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_a

    nop

    :goto_e
    if-ge v0, v1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_0

    nop

    :goto_f
    invoke-direct {v0, v1}, Ld/k/a/a/a/a$e;-><init>(Ld/k/a/a/a/a$a;)V

    goto/32 :goto_7

    nop

    :goto_10
    return-object v0

    :goto_11
    iget-object v1, p0, Ld/k/a/a/a/a;->d:Ld/k/a/a/a/a$a;

    goto/32 :goto_1

    nop
.end method
