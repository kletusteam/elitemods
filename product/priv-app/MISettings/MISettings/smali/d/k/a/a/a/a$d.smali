.class Ld/k/a/a/a/a$d;
.super Ld/k/a/a/a/a$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/k/a/a/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field private final b:Ljava/lang/Runnable;

.field private final c:Landroid/os/Handler;

.field d:J


# direct methods
.method constructor <init>(Ld/k/a/a/a/a$a;)V
    .locals 2

    invoke-direct {p0, p1}, Ld/k/a/a/a/a$c;-><init>(Ld/k/a/a/a/a$a;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ld/k/a/a/a/a$d;->d:J

    new-instance p1, Ld/k/a/a/a/b;

    invoke-direct {p1, p0}, Ld/k/a/a/a/b;-><init>(Ld/k/a/a/a/a$d;)V

    iput-object p1, p0, Ld/k/a/a/a/a$d;->b:Ljava/lang/Runnable;

    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Ld/k/a/a/a/a$d;->c:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method a()V
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    goto/32 :goto_a

    nop

    :goto_2
    return-void

    :goto_3
    const-wide/16 v2, 0xa

    goto/32 :goto_6

    nop

    :goto_4
    const-wide/16 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_5
    iget-object v3, p0, Ld/k/a/a/a/a$d;->b:Ljava/lang/Runnable;

    goto/32 :goto_0

    nop

    :goto_6
    sub-long/2addr v2, v0

    goto/32 :goto_4

    nop

    :goto_7
    iget-object v2, p0, Ld/k/a/a/a/a$d;->c:Landroid/os/Handler;

    goto/32 :goto_5

    nop

    :goto_8
    sub-long/2addr v0, v2

    goto/32 :goto_3

    nop

    :goto_9
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto/32 :goto_7

    nop

    :goto_a
    iget-wide v2, p0, Ld/k/a/a/a/a$d;->d:J

    goto/32 :goto_8

    nop
.end method
