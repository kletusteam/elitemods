.class public final Ld/k/a/a/a/t;
.super Ld/k/a/a/a/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld/k/a/a/a/t$a;,
        Ld/k/a/a/a/t$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ld/k/a/a/a/s<",
        "Ld/k/a/a/a/t;",
        ">;"
    }
.end annotation


# instance fields
.field private final B:Ld/k/a/a/a/t$a;

.field private C:Ld/k/a/a/a/t$b;


# direct methods
.method public constructor <init>(Ld/k/a/a/a/v;Ld/k/a/a/a/t$b;)V
    .locals 1

    invoke-direct {p0, p1}, Ld/k/a/a/a/s;-><init>(Ld/k/a/a/a/v;)V

    new-instance p1, Ld/k/a/a/a/t$a;

    invoke-direct {p1}, Ld/k/a/a/a/t$a;-><init>()V

    iput-object p1, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    iget-object p1, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    invoke-virtual {p0}, Ld/k/a/a/a/s;->b()F

    move-result v0

    invoke-virtual {p1, v0}, Ld/k/a/a/a/t$a;->b(F)V

    iput-object p2, p0, Ld/k/a/a/a/t;->C:Ld/k/a/a/a/t$b;

    return-void
.end method

.method private j(F)F
    .locals 4

    iget v0, p0, Ld/k/a/a/a/s;->o:F

    div-float/2addr p1, v0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    iget-object p1, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    invoke-static {p1}, Ld/k/a/a/a/t$a;->a(Ld/k/a/a/a/t$a;)F

    move-result p1

    float-to-double v2, p1

    div-double/2addr v0, v2

    double-to-float p1, v0

    return p1
.end method


# virtual methods
.method public bridge synthetic a(F)Ld/k/a/a/a/s;
    .locals 0

    invoke-virtual {p0, p1}, Ld/k/a/a/a/t;->a(F)Ld/k/a/a/a/t;

    return-object p0
.end method

.method public a(F)Ld/k/a/a/a/t;
    .locals 0

    invoke-super {p0, p1}, Ld/k/a/a/a/s;->a(F)Ld/k/a/a/a/s;

    return-object p0
.end method

.method a(FF)Z
    .locals 1

    goto/32 :goto_9

    nop

    :goto_0
    goto :goto_6

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    return p1

    :goto_3
    iget-object v0, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    goto/32 :goto_7

    nop

    :goto_4
    const/4 p1, 0x0

    goto/32 :goto_0

    nop

    :goto_5
    const/4 p1, 0x1

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    invoke-virtual {v0, p1, p2}, Ld/k/a/a/a/t$a;->a(FF)Z

    move-result p1

    goto/32 :goto_c

    nop

    :goto_8
    iget v0, p0, Ld/k/a/a/a/s;->v:F

    goto/32 :goto_d

    nop

    :goto_9
    iget v0, p0, Ld/k/a/a/a/s;->u:F

    goto/32 :goto_a

    nop

    :goto_a
    cmpl-float v0, p1, v0

    goto/32 :goto_e

    nop

    :goto_b
    if-gtz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_3

    nop

    :goto_c
    if-nez p1, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_f

    nop

    :goto_d
    cmpg-float v0, p1, v0

    goto/32 :goto_b

    nop

    :goto_e
    if-ltz v0, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_8

    nop

    :goto_f
    goto :goto_1

    :goto_10
    goto/32 :goto_4

    nop
.end method

.method public bridge synthetic b(F)Ld/k/a/a/a/s;
    .locals 0

    invoke-virtual {p0, p1}, Ld/k/a/a/a/t;->b(F)Ld/k/a/a/a/t;

    return-object p0
.end method

.method public b(F)Ld/k/a/a/a/t;
    .locals 0

    invoke-super {p0, p1}, Ld/k/a/a/a/s;->b(F)Ld/k/a/a/a/s;

    return-object p0
.end method

.method b(J)Z
    .locals 3

    goto/32 :goto_12

    nop

    :goto_0
    iget p2, p0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_2

    nop

    :goto_1
    iget p2, p0, Ld/k/a/a/a/s;->v:F

    goto/32 :goto_4

    nop

    :goto_2
    float-to-int p2, p2

    goto/32 :goto_f

    nop

    :goto_3
    iput p2, p0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_b

    nop

    :goto_4
    cmpg-float v0, p1, p2

    goto/32 :goto_9

    nop

    :goto_5
    return v1

    :goto_6
    goto/32 :goto_1e

    nop

    :goto_7
    iget-object p1, p0, Ld/k/a/a/a/t;->C:Ld/k/a/a/a/t$b;

    goto/32 :goto_0

    nop

    :goto_8
    iget p1, p1, Ld/k/a/a/a/s$a;->b:F

    goto/32 :goto_20

    nop

    :goto_9
    const/4 v1, 0x1

    goto/32 :goto_1f

    nop

    :goto_a
    return p1

    :goto_b
    return v1

    :goto_c
    goto/32 :goto_13

    nop

    :goto_d
    iget p2, p1, Ld/k/a/a/a/s$a;->a:F

    goto/32 :goto_1a

    nop

    :goto_e
    if-nez p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_7

    nop

    :goto_f
    invoke-interface {p1, p2}, Ld/k/a/a/a/t$b;->a(I)V

    goto/32 :goto_5

    nop

    :goto_10
    iput p2, p0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_14

    nop

    :goto_11
    iget p1, p0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_1

    nop

    :goto_12
    iget-object v0, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    goto/32 :goto_16

    nop

    :goto_13
    iget p2, p0, Ld/k/a/a/a/s;->o:F

    goto/32 :goto_1b

    nop

    :goto_14
    return v1

    :goto_15
    goto/32 :goto_1d

    nop

    :goto_16
    iget v1, p0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_17

    nop

    :goto_17
    iget v2, p0, Ld/k/a/a/a/s;->o:F

    goto/32 :goto_1c

    nop

    :goto_18
    cmpl-float v0, p1, p2

    goto/32 :goto_19

    nop

    :goto_19
    if-gtz v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_3

    nop

    :goto_1a
    iput p2, p0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_8

    nop

    :goto_1b
    invoke-virtual {p0, p1, p2}, Ld/k/a/a/a/t;->a(FF)Z

    move-result p1

    goto/32 :goto_e

    nop

    :goto_1c
    invoke-virtual {v0, v1, v2, p1, p2}, Ld/k/a/a/a/t$a;->a(FFJ)Ld/k/a/a/a/s$a;

    move-result-object p1

    goto/32 :goto_d

    nop

    :goto_1d
    iget p2, p0, Ld/k/a/a/a/s;->u:F

    goto/32 :goto_18

    nop

    :goto_1e
    const/4 p1, 0x0

    goto/32 :goto_a

    nop

    :goto_1f
    if-ltz v0, :cond_2

    goto/32 :goto_15

    :cond_2
    goto/32 :goto_10

    nop

    :goto_20
    iput p1, p0, Ld/k/a/a/a/s;->o:F

    goto/32 :goto_11

    nop
.end method

.method public d()F
    .locals 2

    iget v0, p0, Ld/k/a/a/a/s;->o:F

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iget-object v1, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    invoke-static {v1}, Ld/k/a/a/a/t$a;->b(Ld/k/a/a/a/t$a;)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-direct {p0, v0}, Ld/k/a/a/a/t;->j(F)F

    move-result v0

    return v0
.end method

.method public e()F
    .locals 4

    iget v0, p0, Ld/k/a/a/a/s;->o:F

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iget v1, p0, Ld/k/a/a/a/s;->p:F

    iget v2, p0, Ld/k/a/a/a/s;->o:F

    iget-object v3, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    invoke-static {v3}, Ld/k/a/a/a/t$a;->a(Ld/k/a/a/a/t$a;)F

    move-result v3

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget-object v2, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    invoke-static {v2}, Ld/k/a/a/a/t$a;->b(Ld/k/a/a/a/t$a;)F

    move-result v2

    mul-float/2addr v0, v2

    iget-object v2, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    invoke-static {v2}, Ld/k/a/a/a/t$a;->a(Ld/k/a/a/a/t$a;)F

    move-result v2

    div-float/2addr v0, v2

    add-float/2addr v1, v0

    return v1
.end method

.method public bridge synthetic f(F)Ld/k/a/a/a/s;
    .locals 0

    invoke-virtual {p0, p1}, Ld/k/a/a/a/t;->f(F)Ld/k/a/a/a/t;

    return-object p0
.end method

.method public f(F)Ld/k/a/a/a/t;
    .locals 0

    invoke-super {p0, p1}, Ld/k/a/a/a/s;->f(F)Ld/k/a/a/a/s;

    return-object p0
.end method

.method g(F)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0, p1}, Ld/k/a/a/a/t$a;->b(F)V

    goto/32 :goto_0

    nop
.end method

.method public h(F)F
    .locals 2

    iget v0, p0, Ld/k/a/a/a/s;->p:F

    sub-float/2addr p1, v0

    iget v0, p0, Ld/k/a/a/a/s;->o:F

    iget-object v1, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    invoke-static {v1}, Ld/k/a/a/a/t$a;->a(Ld/k/a/a/a/t$a;)F

    move-result v1

    div-float/2addr v0, v1

    add-float/2addr p1, v0

    iget-object v0, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    invoke-static {v0}, Ld/k/a/a/a/t$a;->a(Ld/k/a/a/a/t$a;)F

    move-result v0

    mul-float/2addr p1, v0

    invoke-direct {p0, p1}, Ld/k/a/a/a/t;->j(F)F

    move-result p1

    return p1
.end method

.method public i(F)Ld/k/a/a/a/t;
    .locals 1

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Ld/k/a/a/a/t;->B:Ld/k/a/a/a/t$a;

    invoke-virtual {v0, p1}, Ld/k/a/a/a/t$a;->a(F)V

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Friction must be positive"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
