.class public final Ld/k/a/a/a/x;
.super Ld/k/a/a/a/s;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ld/k/a/a/a/s<",
        "Ld/k/a/a/a/x;",
        ">;"
    }
.end annotation


# instance fields
.field private B:Ld/k/a/a/a/y;

.field private C:F

.field private D:Z


# direct methods
.method public constructor <init>(Ld/k/a/a/a/v;)V
    .locals 0

    invoke-direct {p0, p1}, Ld/k/a/a/a/s;-><init>(Ld/k/a/a/a/v;)V

    const/4 p1, 0x0

    iput-object p1, p0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    iput p1, p0, Ld/k/a/a/a/x;->C:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Ld/k/a/a/a/x;->D:Z

    return-void
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ld/k/a/a/a/y;->a()F

    move-result v0

    float-to-double v0, v0

    iget v2, p0, Ld/k/a/a/a/s;->u:F

    float-to-double v2, v2

    cmpl-double v2, v0, v2

    if-gtz v2, :cond_1

    iget v2, p0, Ld/k/a/a/a/s;->v:F

    float-to-double v2, v2

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be less than the min value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be greater than the max value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Incomplete SpringAnimation: Either final position or a spring force needs to be set."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Ld/k/a/a/a/y;)Ld/k/a/a/a/x;
    .locals 0

    iput-object p1, p0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    return-object p0
.end method

.method public a(Z)V
    .locals 3

    invoke-direct {p0}, Ld/k/a/a/a/x;->e()V

    iget-object v0, p0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    invoke-virtual {p0}, Ld/k/a/a/a/s;->b()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Ld/k/a/a/a/y;->b(D)V

    invoke-super {p0, p1}, Ld/k/a/a/a/s;->a(Z)V

    return-void
.end method

.method a(FF)Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1, p2}, Ld/k/a/a/a/y;->a(FF)Z

    move-result p1

    goto/32 :goto_2

    nop

    :goto_2
    return p1
.end method

.method b(J)Z
    .locals 20

    goto/32 :goto_29

    nop

    :goto_0
    iget-object v1, v0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    goto/32 :goto_35

    nop

    :goto_1
    iget-object v6, v0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    goto/32 :goto_f

    nop

    :goto_2
    iget v1, v0, Ld/k/a/a/a/s;->o:F

    goto/32 :goto_37

    nop

    :goto_3
    invoke-virtual {v6, v7}, Ld/k/a/a/a/y;->b(F)Ld/k/a/a/a/y;

    goto/32 :goto_33

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_2f

    nop

    :goto_5
    move-wide/from16 v11, v18

    goto/32 :goto_b

    nop

    :goto_6
    const-wide/16 v11, 0x2

    goto/32 :goto_48

    nop

    :goto_7
    iget-object v6, v0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    goto/32 :goto_4f

    nop

    :goto_8
    return v2

    :goto_9
    goto/32 :goto_25

    nop

    :goto_a
    cmpl-float v1, v1, v5

    goto/32 :goto_45

    nop

    :goto_b
    invoke-virtual/range {v6 .. v12}, Ld/k/a/a/a/y;->a(DDJ)Ld/k/a/a/a/s$a;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_c
    if-nez v6, :cond_1

    goto/32 :goto_3c

    :cond_1
    goto/32 :goto_3a

    nop

    :goto_d
    move-wide/from16 v18, p1

    goto/32 :goto_1b

    nop

    :goto_e
    iput v4, v0, Ld/k/a/a/a/s;->o:F

    goto/32 :goto_4b

    nop

    :goto_f
    iget v1, v0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_11

    nop

    :goto_10
    iput v4, v0, Ld/k/a/a/a/s;->o:F

    goto/32 :goto_2b

    nop

    :goto_11
    float-to-double v7, v1

    goto/32 :goto_4a

    nop

    :goto_12
    return v3

    :goto_13
    iput v1, v0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_38

    nop

    :goto_14
    iput v1, v0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_e

    nop

    :goto_15
    iget v1, v1, Ld/k/a/a/a/s$a;->b:F

    goto/32 :goto_3d

    nop

    :goto_16
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    goto/32 :goto_4

    nop

    :goto_17
    iget-boolean v1, v0, Ld/k/a/a/a/x;->D:Z

    goto/32 :goto_52

    nop

    :goto_18
    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto/32 :goto_13

    nop

    :goto_19
    iget v5, v1, Ld/k/a/a/a/s$a;->a:F

    goto/32 :goto_27

    nop

    :goto_1a
    cmpl-float v6, v1, v5

    goto/32 :goto_c

    nop

    :goto_1b
    invoke-virtual/range {v13 .. v19}, Ld/k/a/a/a/y;->a(DDJ)Ld/k/a/a/a/s$a;

    move-result-object v1

    goto/32 :goto_49

    nop

    :goto_1c
    invoke-virtual {v6, v1}, Ld/k/a/a/a/y;->b(F)Ld/k/a/a/a/y;

    goto/32 :goto_3b

    nop

    :goto_1d
    move-wide/from16 v16, v5

    goto/32 :goto_30

    nop

    :goto_1e
    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto/32 :goto_4c

    nop

    :goto_1f
    iget v1, v0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_36

    nop

    :goto_20
    float-to-double v5, v1

    goto/32 :goto_1d

    nop

    :goto_21
    const/4 v3, 0x0

    goto/32 :goto_4d

    nop

    :goto_22
    iget v1, v1, Ld/k/a/a/a/s$a;->b:F

    goto/32 :goto_20

    nop

    :goto_23
    iget v5, v0, Ld/k/a/a/a/s;->v:F

    goto/32 :goto_1e

    nop

    :goto_24
    move-wide/from16 v16, v5

    goto/32 :goto_d

    nop

    :goto_25
    iget v1, v0, Ld/k/a/a/a/x;->C:F

    goto/32 :goto_a

    nop

    :goto_26
    invoke-virtual {v1}, Ld/k/a/a/a/y;->a()F

    move-result v1

    goto/32 :goto_14

    nop

    :goto_27
    float-to-double v14, v5

    goto/32 :goto_22

    nop

    :goto_28
    if-nez v1, :cond_2

    goto/32 :goto_2c

    :cond_2
    goto/32 :goto_0

    nop

    :goto_29
    move-object/from16 v0, p0

    goto/32 :goto_17

    nop

    :goto_2a
    iget-object v13, v0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    goto/32 :goto_19

    nop

    :goto_2b
    return v2

    :goto_2c
    goto/32 :goto_12

    nop

    :goto_2d
    iput v1, v0, Ld/k/a/a/a/s;->o:F

    goto/32 :goto_43

    nop

    :goto_2e
    float-to-double v9, v1

    goto/32 :goto_6

    nop

    :goto_2f
    iget v1, v0, Ld/k/a/a/a/x;->C:F

    goto/32 :goto_1a

    nop

    :goto_30
    invoke-virtual/range {v13 .. v19}, Ld/k/a/a/a/y;->a(DDJ)Ld/k/a/a/a/s$a;

    move-result-object v1

    goto/32 :goto_40

    nop

    :goto_31
    iget v1, v0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_23

    nop

    :goto_32
    iget v1, v0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_42

    nop

    :goto_33
    iput v5, v0, Ld/k/a/a/a/x;->C:F

    goto/32 :goto_2a

    nop

    :goto_34
    iget-object v13, v0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    goto/32 :goto_32

    nop

    :goto_35
    invoke-virtual {v1}, Ld/k/a/a/a/y;->a()F

    move-result v1

    goto/32 :goto_41

    nop

    :goto_36
    iget v5, v0, Ld/k/a/a/a/s;->u:F

    goto/32 :goto_18

    nop

    :goto_37
    float-to-double v5, v1

    goto/32 :goto_24

    nop

    :goto_38
    iget v1, v0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_51

    nop

    :goto_39
    iget-object v1, v0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    goto/32 :goto_3f

    nop

    :goto_3a
    iget-object v6, v0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    goto/32 :goto_1c

    nop

    :goto_3b
    iput v5, v0, Ld/k/a/a/a/x;->C:F

    :goto_3c
    goto/32 :goto_50

    nop

    :goto_3d
    iput v1, v0, Ld/k/a/a/a/s;->o:F

    :goto_3e
    goto/32 :goto_31

    nop

    :goto_3f
    invoke-virtual {v1}, Ld/k/a/a/a/y;->a()F

    goto/32 :goto_1

    nop

    :goto_40
    iget v5, v1, Ld/k/a/a/a/s$a;->a:F

    goto/32 :goto_4e

    nop

    :goto_41
    iput v1, v0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_10

    nop

    :goto_42
    float-to-double v14, v1

    goto/32 :goto_2

    nop

    :goto_43
    goto :goto_3e

    :goto_44
    goto/32 :goto_34

    nop

    :goto_45
    if-nez v1, :cond_3

    goto/32 :goto_44

    :cond_3
    goto/32 :goto_39

    nop

    :goto_46
    iget v1, v1, Ld/k/a/a/a/s$a;->b:F

    goto/32 :goto_2d

    nop

    :goto_47
    invoke-virtual {v0, v1, v5}, Ld/k/a/a/a/x;->a(FF)Z

    move-result v1

    goto/32 :goto_28

    nop

    :goto_48
    div-long v18, p1, v11

    goto/32 :goto_5

    nop

    :goto_49
    iget v5, v1, Ld/k/a/a/a/s$a;->a:F

    goto/32 :goto_53

    nop

    :goto_4a
    iget v1, v0, Ld/k/a/a/a/s;->o:F

    goto/32 :goto_2e

    nop

    :goto_4b
    iput-boolean v3, v0, Ld/k/a/a/a/x;->D:Z

    goto/32 :goto_8

    nop

    :goto_4c
    iput v1, v0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_1f

    nop

    :goto_4d
    const/4 v4, 0x0

    goto/32 :goto_16

    nop

    :goto_4e
    iput v5, v0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_46

    nop

    :goto_4f
    iget v7, v0, Ld/k/a/a/a/x;->C:F

    goto/32 :goto_3

    nop

    :goto_50
    iget-object v1, v0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    goto/32 :goto_26

    nop

    :goto_51
    iget v5, v0, Ld/k/a/a/a/s;->o:F

    goto/32 :goto_47

    nop

    :goto_52
    const/4 v2, 0x1

    goto/32 :goto_21

    nop

    :goto_53
    iput v5, v0, Ld/k/a/a/a/s;->p:F

    goto/32 :goto_15

    nop
.end method

.method public d()Ld/k/a/a/a/y;
    .locals 1

    iget-object v0, p0, Ld/k/a/a/a/x;->B:Ld/k/a/a/a/y;

    return-object v0
.end method

.method g(F)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method
