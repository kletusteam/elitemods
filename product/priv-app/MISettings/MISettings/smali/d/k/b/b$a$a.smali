.class Ld/k/b/b$a$a;
.super Ljava/lang/Object;

# interfaces
.implements Ld/k/a/a/a/s$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/k/b/b$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Ld/k/b/b$a;


# direct methods
.method private constructor <init>(Ld/k/b/b$a;)V
    .locals 0

    iput-object p1, p0, Ld/k/b/b$a$a;->a:Ld/k/b/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ld/k/b/b$a;Ld/k/b/a;)V
    .locals 0

    invoke-direct {p0, p1}, Ld/k/b/b$a$a;-><init>(Ld/k/b/b$a;)V

    return-void
.end method


# virtual methods
.method public a(Ld/k/a/a/a/s;FF)V
    .locals 3

    iget-object v0, p0, Ld/k/b/b$a$a;->a:Ld/k/b/b$a;

    iput p3, v0, Ld/k/b/b$a;->e:F

    iget v1, v0, Ld/k/b/b$a;->b:I

    float-to-int v2, p2

    add-int/2addr v1, v2

    iput v1, v0, Ld/k/b/b$a;->f:I

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, v0, p2

    iget-object p1, p0, Ld/k/b/b$a$a;->a:Ld/k/b/b$a;

    invoke-static {p1}, Ld/k/b/b$a;->a(Ld/k/b/b$a;)F

    move-result p1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    const/4 p2, 0x3

    aput-object p1, v0, p2

    iget-object p1, p0, Ld/k/b/b$a$a;->a:Ld/k/b/b$a;

    invoke-static {p1}, Ld/k/b/b$a;->b(Ld/k/b/b$a;)F

    move-result p1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    const/4 p2, 0x4

    aput-object p1, v0, p2

    const-string p1, "%s updating value(%f), velocity(%f), min(%f), max(%f)"

    invoke-static {p1, v0}, Ld/k/b/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
