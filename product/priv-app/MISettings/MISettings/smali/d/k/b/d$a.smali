.class Ld/k/b/d$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/k/b/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# static fields
.field private static a:F

.field private static final b:[F

.field private static final c:[F


# instance fields
.field private d:Landroid/content/Context;

.field private e:D

.field private f:D

.field private g:D

.field private h:D

.field private i:D

.field private j:F

.field private k:J

.field private l:I

.field private m:Z

.field private n:F

.field private o:I

.field private p:F

.field private q:Lmiuix/animation/f/m;

.field private r:Z

.field private s:D

.field private t:[D


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const-wide v0, 0x3fe8f5c28f5c28f6L    # 0.78

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Ld/k/b/d$a;->a:F

    const/16 v0, 0x65

    new-array v1, v0, [F

    sput-object v1, Ld/k/b/d$a;->b:[F

    new-array v0, v0, [F

    sput-object v0, Ld/k/b/d$a;->c:[F

    const/4 v0, 0x0

    const/4 v1, 0x0

    move v2, v0

    :goto_0
    const/16 v3, 0x64

    const/high16 v4, 0x3f800000    # 1.0f

    if-ge v1, v3, :cond_4

    int-to-float v3, v1

    const/high16 v5, 0x42c80000    # 100.0f

    div-float v5, v3, v5

    move v3, v4

    :goto_1
    sub-float v6, v3, v0

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    const/high16 v8, 0x40400000    # 3.0f

    mul-float v9, v6, v8

    sub-float v10, v4, v6

    mul-float/2addr v9, v10

    const v11, 0x3e333333    # 0.175f

    mul-float v12, v10, v11

    const v13, 0x3eb33334    # 0.35000002f

    mul-float v14, v6, v13

    add-float/2addr v12, v14

    mul-float/2addr v12, v9

    mul-float v14, v6, v6

    mul-float/2addr v14, v6

    add-float/2addr v12, v14

    sub-float v15, v12, v5

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    move/from16 v16, v12

    float-to-double v11, v15

    const-wide v17, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v11, v11, v17

    if-gez v11, :cond_2

    sget-object v3, Ld/k/b/d$a;->b:[F

    const/high16 v11, 0x3f000000    # 0.5f

    mul-float/2addr v10, v11

    add-float/2addr v10, v6

    mul-float/2addr v9, v10

    add-float/2addr v9, v14

    aput v9, v3, v1

    move v3, v4

    :goto_2
    sub-float v6, v3, v2

    div-float/2addr v6, v7

    add-float/2addr v6, v2

    mul-float v9, v6, v8

    sub-float v10, v4, v6

    mul-float/2addr v9, v10

    mul-float v12, v10, v11

    add-float/2addr v12, v6

    mul-float/2addr v12, v9

    mul-float v14, v6, v6

    mul-float/2addr v14, v6

    add-float/2addr v12, v14

    sub-float v15, v12, v5

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    float-to-double v7, v15

    cmpg-double v7, v7, v17

    if-gez v7, :cond_0

    sget-object v3, Ld/k/b/d$a;->c:[F

    const v7, 0x3e333333    # 0.175f

    mul-float/2addr v10, v7

    mul-float/2addr v6, v13

    add-float/2addr v10, v6

    mul-float/2addr v9, v10

    add-float/2addr v9, v14

    aput v9, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const v7, 0x3e333333    # 0.175f

    cmpl-float v8, v12, v5

    if-lez v8, :cond_1

    move v3, v6

    goto :goto_3

    :cond_1
    move v2, v6

    :goto_3
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v8, 0x40400000    # 3.0f

    goto :goto_2

    :cond_2
    cmpl-float v7, v16, v5

    if-lez v7, :cond_3

    move v3, v6

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_1

    :cond_4
    sget-object v0, Ld/k/b/d$a;->b:[F

    sget-object v1, Ld/k/b/d$a;->c:[F

    aput v4, v1, v3

    aput v4, v0, v3

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iput v0, p0, Ld/k/b/d$a;->n:F

    const/4 v0, 0x0

    iput v0, p0, Ld/k/b/d$a;->o:I

    iput-object p1, p0, Ld/k/b/d$a;->d:Landroid/content/Context;

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld/k/b/d$a;->m:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v0, 0x43200000    # 160.0f

    mul-float/2addr p1, v0

    const v0, 0x43c10b3d

    mul-float/2addr p1, v0

    const v0, 0x3f570a3d    # 0.84f

    mul-float/2addr p1, v0

    iput p1, p0, Ld/k/b/d$a;->p:F

    return-void
.end method

.method static synthetic a(Ld/k/b/d$a;)Z
    .locals 0

    iget-boolean p0, p0, Ld/k/b/d$a;->m:Z

    return p0
.end method

.method static synthetic b(Ld/k/b/d$a;)D
    .locals 2

    iget-wide v0, p0, Ld/k/b/d$a;->f:D

    return-wide v0
.end method

.method static synthetic c(Ld/k/b/d$a;)D
    .locals 2

    iget-wide v0, p0, Ld/k/b/d$a;->i:D

    return-wide v0
.end method

.method static synthetic d(Ld/k/b/d$a;)D
    .locals 2

    iget-wide v0, p0, Ld/k/b/d$a;->e:D

    return-wide v0
.end method

.method static synthetic e(Ld/k/b/d$a;)D
    .locals 2

    iget-wide v0, p0, Ld/k/b/d$a;->g:D

    return-wide v0
.end method

.method static synthetic f(Ld/k/b/d$a;)I
    .locals 0

    iget p0, p0, Ld/k/b/d$a;->l:I

    return p0
.end method

.method static synthetic g(Ld/k/b/d$a;)J
    .locals 2

    iget-wide v0, p0, Ld/k/b/d$a;->k:J

    return-wide v0
.end method


# virtual methods
.method final a(F)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iput-wide v0, p0, Ld/k/b/d$a;->i:D

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    float-to-double v0, p1

    goto/32 :goto_0

    nop
.end method

.method a(FII)V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    iput-object p1, p0, Ld/k/b/d$a;->q:Lmiuix/animation/f/m;

    goto/32 :goto_15

    nop

    :goto_1
    int-to-double p1, p3

    goto/32 :goto_11

    nop

    :goto_2
    iput-wide p1, p0, Ld/k/b/d$a;->i:D

    goto/32 :goto_1a

    nop

    :goto_3
    iput-wide v0, p0, Ld/k/b/d$a;->f:D

    goto/32 :goto_6

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_8

    nop

    :goto_5
    iget-object p3, p0, Ld/k/b/d$a;->t:[D

    goto/32 :goto_9

    nop

    :goto_6
    int-to-float p2, p2

    goto/32 :goto_16

    nop

    :goto_7
    new-array p2, p1, [D

    goto/32 :goto_a

    nop

    :goto_8
    iput-boolean v0, p0, Ld/k/b/d$a;->m:Z

    goto/32 :goto_1b

    nop

    :goto_9
    invoke-virtual {p2, p1, p3}, Lmiuix/animation/f/m;->a([F[D)V

    goto/32 :goto_d

    nop

    :goto_a
    iput-object p2, p0, Ld/k/b/d$a;->t:[D

    goto/32 :goto_13

    nop

    :goto_b
    iput-wide v0, p0, Ld/k/b/d$a;->s:D

    goto/32 :goto_c

    nop

    :goto_c
    iput-wide v0, p0, Ld/k/b/d$a;->e:D

    goto/32 :goto_3

    nop

    :goto_d
    return-void

    nop

    :array_0
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3ecccccd    # 0.4f
    .end array-data

    :goto_e
    iput-wide p1, p0, Ld/k/b/d$a;->g:D

    goto/32 :goto_12

    nop

    :goto_f
    invoke-direct {p1}, Lmiuix/animation/f/m;-><init>()V

    goto/32 :goto_0

    nop

    :goto_10
    float-to-double v0, p1

    goto/32 :goto_b

    nop

    :goto_11
    iput-wide p1, p0, Ld/k/b/d$a;->h:D

    goto/32 :goto_2

    nop

    :goto_12
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide p1

    goto/32 :goto_19

    nop

    :goto_13
    iget-object p2, p0, Ld/k/b/d$a;->q:Lmiuix/animation/f/m;

    goto/32 :goto_17

    nop

    :goto_14
    float-to-double p1, p1

    goto/32 :goto_e

    nop

    :goto_15
    const/4 p1, 0x2

    goto/32 :goto_7

    nop

    :goto_16
    add-float/2addr p1, p2

    goto/32 :goto_14

    nop

    :goto_17
    new-array p1, p1, [F

    fill-array-data p1, :array_0

    goto/32 :goto_5

    nop

    :goto_18
    invoke-virtual {p0, v0}, Ld/k/b/d$a;->g(I)V

    goto/32 :goto_10

    nop

    :goto_19
    iput-wide p1, p0, Ld/k/b/d$a;->k:J

    goto/32 :goto_1

    nop

    :goto_1a
    new-instance p1, Lmiuix/animation/f/m;

    goto/32 :goto_f

    nop

    :goto_1b
    iput-boolean v0, p0, Ld/k/b/d$a;->r:Z

    goto/32 :goto_18

    nop
.end method

.method a(III)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    throw p0

    :goto_1
    const p0, 0x0

    goto/32 :goto_0

    nop
.end method

.method a(IIIII)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    throw p0

    :goto_1
    const p0, 0x0

    goto/32 :goto_0

    nop
.end method

.method final a(J)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-wide p1, p0, Ld/k/b/d$a;->k:J

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method final a(Z)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-boolean p1, p0, Ld/k/b/d$a;->m:Z

    goto/32 :goto_0

    nop
.end method

.method a()Z
    .locals 20

    goto/32 :goto_24

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_2f

    :cond_0
    goto/32 :goto_2e

    nop

    :goto_1
    iget-wide v4, v0, Ld/k/b/d$a;->g:D

    goto/32 :goto_a

    nop

    :goto_2
    iget-wide v1, v0, Ld/k/b/d$a;->g:D

    goto/32 :goto_3d

    nop

    :goto_3
    aget-wide v15, v1, v3

    goto/32 :goto_2a

    nop

    :goto_4
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    goto/32 :goto_7

    nop

    :goto_5
    return v3

    :goto_6
    goto/32 :goto_13

    nop

    :goto_7
    iget-wide v6, v0, Ld/k/b/d$a;->k:J

    goto/32 :goto_2c

    nop

    :goto_8
    add-double/2addr v4, v6

    goto/32 :goto_e

    nop

    :goto_9
    iput-boolean v3, v0, Ld/k/b/d$a;->m:Z

    goto/32 :goto_2

    nop

    :goto_a
    aput-wide v4, v1, v2

    goto/32 :goto_30

    nop

    :goto_b
    iget-object v1, v0, Ld/k/b/d$a;->t:[D

    goto/32 :goto_1c

    nop

    :goto_c
    iget-object v1, v0, Ld/k/b/d$a;->q:Lmiuix/animation/f/m;

    goto/32 :goto_39

    nop

    :goto_d
    if-nez v1, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_31

    nop

    :goto_e
    iput-wide v4, v0, Ld/k/b/d$a;->f:D

    goto/32 :goto_18

    nop

    :goto_f
    const/high16 v6, 0x447a0000    # 1000.0f

    goto/32 :goto_3b

    nop

    :goto_10
    goto :goto_29

    :goto_11
    goto/32 :goto_2d

    nop

    :goto_12
    if-nez v1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_35

    nop

    :goto_13
    return v2

    :goto_14
    const-wide v8, 0x3f90624de0000000L    # 0.01600000075995922

    goto/32 :goto_3c

    nop

    :goto_15
    iget-object v10, v0, Ld/k/b/d$a;->q:Lmiuix/animation/f/m;

    goto/32 :goto_2b

    nop

    :goto_16
    move-wide v6, v8

    :goto_17
    goto/32 :goto_20

    nop

    :goto_18
    iput-wide v1, v0, Ld/k/b/d$a;->i:D

    goto/32 :goto_19

    nop

    :goto_19
    iget-wide v1, v0, Ld/k/b/d$a;->f:D

    goto/32 :goto_36

    nop

    :goto_1a
    cmpl-double v1, v6, v10

    goto/32 :goto_32

    nop

    :goto_1b
    iget-boolean v1, v0, Ld/k/b/d$a;->r:Z

    goto/32 :goto_1f

    nop

    :goto_1c
    aget-wide v13, v1, v2

    goto/32 :goto_3

    nop

    :goto_1d
    const-wide/16 v10, 0x0

    goto/32 :goto_1a

    nop

    :goto_1e
    mul-double/2addr v6, v1

    goto/32 :goto_8

    nop

    :goto_1f
    const/4 v3, 0x1

    goto/32 :goto_3e

    nop

    :goto_20
    iput-wide v4, v0, Ld/k/b/d$a;->k:J

    goto/32 :goto_15

    nop

    :goto_21
    move-object/from16 v19, v1

    goto/32 :goto_38

    nop

    :goto_22
    aput-wide v4, v1, v3

    goto/32 :goto_33

    nop

    :goto_23
    float-to-double v6, v1

    goto/32 :goto_14

    nop

    :goto_24
    move-object/from16 v0, p0

    goto/32 :goto_c

    nop

    :goto_25
    invoke-virtual {v0, v1, v2, v4, v5}, Ld/k/b/d$a;->a(DD)Z

    move-result v1

    goto/32 :goto_d

    nop

    :goto_26
    return v3

    :goto_27
    goto/32 :goto_4

    nop

    :goto_28
    iput-wide v1, v0, Ld/k/b/d$a;->e:D

    :goto_29
    goto/32 :goto_5

    nop

    :goto_2a
    const/4 v1, 0x2

    goto/32 :goto_3a

    nop

    :goto_2b
    iget-wide v11, v0, Ld/k/b/d$a;->i:D

    goto/32 :goto_b

    nop

    :goto_2c
    sub-long v6, v4, v6

    goto/32 :goto_34

    nop

    :goto_2d
    iget-wide v1, v0, Ld/k/b/d$a;->f:D

    goto/32 :goto_28

    nop

    :goto_2e
    goto/16 :goto_6

    :goto_2f
    goto/32 :goto_1b

    nop

    :goto_30
    iget-wide v4, v0, Ld/k/b/d$a;->e:D

    goto/32 :goto_22

    nop

    :goto_31
    iput-boolean v3, v0, Ld/k/b/d$a;->r:Z

    goto/32 :goto_10

    nop

    :goto_32
    if-eqz v1, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_16

    nop

    :goto_33
    move-wide/from16 v17, v6

    goto/32 :goto_21

    nop

    :goto_34
    long-to-float v1, v6

    goto/32 :goto_f

    nop

    :goto_35
    iget-boolean v1, v0, Ld/k/b/d$a;->m:Z

    goto/32 :goto_0

    nop

    :goto_36
    iget-wide v4, v0, Ld/k/b/d$a;->g:D

    goto/32 :goto_25

    nop

    :goto_37
    iget-wide v4, v0, Ld/k/b/d$a;->e:D

    goto/32 :goto_1e

    nop

    :goto_38
    invoke-virtual/range {v10 .. v19}, Lmiuix/animation/f/m;->a(DDDD[D)D

    move-result-wide v1

    goto/32 :goto_37

    nop

    :goto_39
    const/4 v2, 0x0

    goto/32 :goto_12

    nop

    :goto_3a
    new-array v1, v1, [D

    goto/32 :goto_1

    nop

    :goto_3b
    div-float/2addr v1, v6

    goto/32 :goto_23

    nop

    :goto_3c
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    goto/32 :goto_1d

    nop

    :goto_3d
    iput-wide v1, v0, Ld/k/b/d$a;->f:D

    goto/32 :goto_26

    nop

    :goto_3e
    if-nez v1, :cond_4

    goto/32 :goto_27

    :cond_4
    goto/32 :goto_9

    nop
.end method

.method a(DD)Z
    .locals 0

    goto/32 :goto_3

    nop

    :goto_0
    const/4 p1, 0x0

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    return p1

    :goto_3
    sub-double/2addr p1, p3

    goto/32 :goto_8

    nop

    :goto_4
    cmpg-double p1, p1, p3

    goto/32 :goto_a

    nop

    :goto_5
    goto :goto_1

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    const-wide/high16 p3, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_4

    nop

    :goto_8
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    goto/32 :goto_7

    nop

    :goto_9
    const/4 p1, 0x1

    goto/32 :goto_5

    nop

    :goto_a
    if-ltz p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_9

    nop
.end method

.method b(F)V
    .locals 6

    goto/32 :goto_8

    nop

    :goto_0
    float-to-double v2, p1

    goto/32 :goto_4

    nop

    :goto_1
    mul-double/2addr v2, v4

    goto/32 :goto_6

    nop

    :goto_2
    iput-wide v0, p0, Ld/k/b/d$a;->f:D

    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    iget-wide v4, p0, Ld/k/b/d$a;->g:D

    goto/32 :goto_7

    nop

    :goto_5
    long-to-double v2, v2

    goto/32 :goto_9

    nop

    :goto_6
    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    goto/32 :goto_5

    nop

    :goto_7
    sub-double/2addr v4, v0

    goto/32 :goto_1

    nop

    :goto_8
    iget-wide v0, p0, Ld/k/b/d$a;->e:D

    goto/32 :goto_0

    nop

    :goto_9
    add-double/2addr v0, v2

    goto/32 :goto_2

    nop
.end method

.method final b(I)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    int-to-double v0, p1

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    iput-wide v0, p0, Ld/k/b/d$a;->f:D

    goto/32 :goto_1

    nop
.end method

.method b()Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method

.method b(III)Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method

.method c()V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    throw p0

    :goto_1
    const p0, 0x0

    goto/32 :goto_0

    nop
.end method

.method final c(I)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput p1, p0, Ld/k/b/d$a;->l:I

    goto/32 :goto_0

    nop
.end method

.method c(III)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iput-wide p1, p0, Ld/k/b/d$a;->k:J

    goto/32 :goto_e

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_d

    nop

    :goto_2
    return-void

    :goto_3
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide p1

    goto/32 :goto_0

    nop

    :goto_4
    iput-wide p1, p0, Ld/k/b/d$a;->g:D

    goto/32 :goto_3

    nop

    :goto_5
    add-int/2addr p1, p2

    goto/32 :goto_9

    nop

    :goto_6
    int-to-double v0, p1

    goto/32 :goto_a

    nop

    :goto_7
    iput p1, p0, Ld/k/b/d$a;->j:F

    goto/32 :goto_c

    nop

    :goto_8
    const/4 p1, 0x0

    goto/32 :goto_7

    nop

    :goto_9
    int-to-double p1, p1

    goto/32 :goto_4

    nop

    :goto_a
    iput-wide v0, p0, Ld/k/b/d$a;->e:D

    goto/32 :goto_f

    nop

    :goto_b
    iput-wide p1, p0, Ld/k/b/d$a;->h:D

    goto/32 :goto_2

    nop

    :goto_c
    const-wide/16 p1, 0x0

    goto/32 :goto_b

    nop

    :goto_d
    iput-boolean v0, p0, Ld/k/b/d$a;->m:Z

    goto/32 :goto_6

    nop

    :goto_e
    iput p3, p0, Ld/k/b/d$a;->l:I

    goto/32 :goto_8

    nop

    :goto_f
    iput-wide v0, p0, Ld/k/b/d$a;->f:D

    goto/32 :goto_5

    nop
.end method

.method final d()F
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-wide v0, p0, Ld/k/b/d$a;->i:D

    goto/32 :goto_1

    nop

    :goto_1
    double-to-float v0, v0

    goto/32 :goto_2

    nop

    :goto_2
    return v0
.end method

.method final d(I)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iput-wide v0, p0, Ld/k/b/d$a;->g:D

    goto/32 :goto_2

    nop

    :goto_1
    int-to-double v0, p1

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method final e()I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return v0

    :goto_1
    double-to-int v0, v0

    goto/32 :goto_0

    nop

    :goto_2
    iget-wide v0, p0, Ld/k/b/d$a;->f:D

    goto/32 :goto_1

    nop
.end method

.method e(I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    iput-boolean p1, p0, Ld/k/b/d$a;->m:Z

    goto/32 :goto_0

    nop

    :goto_2
    int-to-double v0, p1

    goto/32 :goto_4

    nop

    :goto_3
    const/4 p1, 0x0

    goto/32 :goto_1

    nop

    :goto_4
    iput-wide v0, p0, Ld/k/b/d$a;->g:D

    goto/32 :goto_3

    nop
.end method

.method final f()I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    double-to-int v0, v0

    goto/32 :goto_1

    nop

    :goto_1
    return v0

    :goto_2
    iget-wide v0, p0, Ld/k/b/d$a;->g:D

    goto/32 :goto_0

    nop
.end method

.method final f(I)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    int-to-double v0, p1

    goto/32 :goto_1

    nop

    :goto_1
    iput-wide v0, p0, Ld/k/b/d$a;->e:D

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method final g()I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return v0

    :goto_1
    double-to-int v0, v0

    goto/32 :goto_0

    nop

    :goto_2
    iget-wide v0, p0, Ld/k/b/d$a;->e:D

    goto/32 :goto_1

    nop
.end method

.method final g(I)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput p1, p0, Ld/k/b/d$a;->o:I

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method final h()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget v0, p0, Ld/k/b/d$a;->o:I

    goto/32 :goto_0

    nop
.end method

.method final i()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Ld/k/b/d$a;->m:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method j()Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method
