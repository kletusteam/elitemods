.class Ld/k/b/b;
.super Ld/k/b/d$a;

# interfaces
.implements Ld/k/a/a/a/t$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld/k/b/b$a;
    }
.end annotation


# instance fields
.field private u:Ld/k/a/a/a/v;

.field private v:Ld/k/a/a/a/x;

.field private w:Ld/k/a/a/a/t;

.field private x:Ld/k/b/b$a;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0, p1}, Ld/k/b/d$a;-><init>(Landroid/content/Context;)V

    new-instance p1, Ld/k/a/a/a/v;

    invoke-direct {p1}, Ld/k/a/a/a/v;-><init>()V

    iput-object p1, p0, Ld/k/b/b;->u:Ld/k/a/a/a/v;

    new-instance p1, Ld/k/a/a/a/x;

    iget-object v0, p0, Ld/k/b/b;->u:Ld/k/a/a/a/v;

    invoke-direct {p1, v0}, Ld/k/a/a/a/x;-><init>(Ld/k/a/a/a/v;)V

    iput-object p1, p0, Ld/k/b/b;->v:Ld/k/a/a/a/x;

    iget-object p1, p0, Ld/k/b/b;->v:Ld/k/a/a/a/x;

    new-instance v0, Ld/k/a/a/a/y;

    invoke-direct {v0}, Ld/k/a/a/a/y;-><init>()V

    invoke-virtual {p1, v0}, Ld/k/a/a/a/x;->a(Ld/k/a/a/a/y;)Ld/k/a/a/a/x;

    iget-object p1, p0, Ld/k/b/b;->v:Ld/k/a/a/a/x;

    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p1, v0}, Ld/k/a/a/a/s;->c(F)Ld/k/a/a/a/s;

    iget-object p1, p0, Ld/k/b/b;->v:Ld/k/a/a/a/x;

    invoke-virtual {p1}, Ld/k/a/a/a/x;->d()Ld/k/a/a/a/y;

    move-result-object p1

    const v1, 0x3f7851ec    # 0.97f

    invoke-virtual {p1, v1}, Ld/k/a/a/a/y;->a(F)Ld/k/a/a/a/y;

    iget-object p1, p0, Ld/k/b/b;->v:Ld/k/a/a/a/x;

    invoke-virtual {p1}, Ld/k/a/a/a/x;->d()Ld/k/a/a/a/y;

    move-result-object p1

    const v1, 0x43028000    # 130.5f

    invoke-virtual {p1, v1}, Ld/k/a/a/a/y;->c(F)Ld/k/a/a/a/y;

    iget-object p1, p0, Ld/k/b/b;->v:Ld/k/a/a/a/x;

    invoke-virtual {p1}, Ld/k/a/a/a/x;->d()Ld/k/a/a/a/y;

    move-result-object p1

    const-wide v1, 0x408f400000000000L    # 1000.0

    invoke-virtual {p1, v1, v2}, Ld/k/a/a/a/y;->a(D)Ld/k/a/a/a/y;

    new-instance p1, Ld/k/a/a/a/t;

    iget-object v1, p0, Ld/k/b/b;->u:Ld/k/a/a/a/v;

    invoke-direct {p1, v1, p0}, Ld/k/a/a/a/t;-><init>(Ld/k/a/a/a/v;Ld/k/a/a/a/t$b;)V

    iput-object p1, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    iget-object p1, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    invoke-virtual {p1, v0}, Ld/k/a/a/a/s;->c(F)Ld/k/a/a/a/s;

    iget-object p1, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    const v0, 0x3ef3cf3e

    invoke-virtual {p1, v0}, Ld/k/a/a/a/t;->i(F)Ld/k/a/a/a/t;

    return-void
.end method

.method static synthetic a(Ld/k/b/b;)Ld/k/b/b$a;
    .locals 0

    iget-object p0, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    return-object p0
.end method

.method private a(IIFII)V
    .locals 3

    const/high16 v0, 0x45fa0000    # 8000.0f

    cmpl-float v1, p3, v0

    const/4 v2, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p3

    aput-object p3, v1, v2

    const-string p3, "%f is too fast for spring, slow down"

    invoke-static {p3, v1}, Ld/k/b/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move p3, v0

    :cond_0
    invoke-virtual {p0, v2}, Ld/k/b/d$a;->a(Z)V

    invoke-virtual {p0, p3}, Ld/k/b/d$a;->a(F)V

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ld/k/b/d$a;->a(J)V

    invoke-virtual {p0, p2}, Ld/k/b/d$a;->b(I)V

    invoke-virtual {p0, p2}, Ld/k/b/d$a;->f(I)V

    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Ld/k/b/d$a;->c(I)V

    invoke-virtual {p0, p4}, Ld/k/b/d$a;->d(I)V

    invoke-virtual {p0, p1}, Ld/k/b/d$a;->g(I)V

    new-instance p1, Ld/k/b/b$a;

    iget-object v0, p0, Ld/k/b/b;->v:Ld/k/a/a/a/x;

    invoke-direct {p1, v0, p2, p3}, Ld/k/b/b$a;-><init>(Ld/k/a/a/a/s;IF)V

    iput-object p1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    iget-object p1, p0, Ld/k/b/b;->v:Ld/k/a/a/a/x;

    invoke-virtual {p1}, Ld/k/a/a/a/x;->d()Ld/k/a/a/a/y;

    move-result-object p1

    iget-object v0, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    invoke-virtual {v0, p4}, Ld/k/b/b$a;->a(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Ld/k/a/a/a/y;->b(F)Ld/k/a/a/a/y;

    if-eqz p5, :cond_2

    const/4 p1, 0x0

    cmpg-float p1, p3, p1

    if-gez p1, :cond_1

    iget-object p1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    sub-int p3, p4, p5

    invoke-virtual {p1, p3}, Ld/k/b/b$a;->c(I)V

    iget-object p1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    invoke-static {p4, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-virtual {p1, p2}, Ld/k/b/b$a;->b(I)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    invoke-static {p4, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-virtual {p1, p2}, Ld/k/b/b$a;->c(I)V

    iget-object p1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    add-int/2addr p4, p5

    invoke-virtual {p1, p4}, Ld/k/b/b$a;->b(I)V

    :cond_2
    :goto_0
    iget-object p1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    invoke-virtual {p1}, Ld/k/b/b$a;->d()V

    return-void
.end method

.method static synthetic a(Ld/k/b/b;IIFII)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Ld/k/b/b;->a(IIFII)V

    return-void
.end method

.method static synthetic b(Ld/k/b/b;)Ld/k/a/a/a/t;
    .locals 0

    iget-object p0, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    return-object p0
.end method

.method private b(IIIII)V
    .locals 6

    iget-object v0, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ld/k/a/a/a/s;->e(F)Ld/k/a/a/a/s;

    iget-object v0, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    int-to-float p2, p2

    invoke-virtual {v0, p2}, Ld/k/a/a/a/t;->f(F)Ld/k/a/a/a/t;

    int-to-long v0, p1

    iget-object v2, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    invoke-virtual {v2}, Ld/k/a/a/a/t;->e()F

    move-result v2

    float-to-long v2, v2

    add-long/2addr v0, v2

    int-to-long v2, p4

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v0, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    sub-int v1, p4, p1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Ld/k/a/a/a/t;->h(F)F

    move-result v0

    float-to-int v0, v0

    move v1, p4

    goto :goto_0

    :cond_0
    int-to-long v2, p3

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    iget-object v0, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    sub-int v1, p3, p1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Ld/k/a/a/a/t;->h(F)F

    move-result v0

    float-to-int v0, v0

    move v1, p3

    goto :goto_0

    :cond_1
    long-to-int v0, v0

    iget-object v1, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    invoke-virtual {v1}, Ld/k/a/a/a/t;->d()F

    move-result v1

    float-to-int v1, v1

    move v5, v1

    move v1, v0

    move v0, v5

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Ld/k/b/d$a;->a(Z)V

    invoke-virtual {p0, p2}, Ld/k/b/d$a;->a(F)V

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Ld/k/b/d$a;->a(J)V

    invoke-virtual {p0, p1}, Ld/k/b/d$a;->b(I)V

    invoke-virtual {p0, p1}, Ld/k/b/d$a;->f(I)V

    invoke-virtual {p0, v0}, Ld/k/b/d$a;->c(I)V

    invoke-virtual {p0, v1}, Ld/k/b/d$a;->d(I)V

    invoke-virtual {p0, v2}, Ld/k/b/d$a;->g(I)V

    invoke-static {p3, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {p4, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-instance v2, Ld/k/b/b$a;

    iget-object v3, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    invoke-direct {v2, v3, p1, p2}, Ld/k/b/b$a;-><init>(Ld/k/a/a/a/s;IF)V

    iput-object v2, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    iget-object p1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    new-instance p2, Ld/k/b/a;

    invoke-direct {p2, p0, p3, p4, p5}, Ld/k/b/a;-><init>(Ld/k/b/b;III)V

    invoke-virtual {p1, p2}, Ld/k/b/b$a;->a(Ld/k/b/b$a$b;)V

    iget-object p1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    invoke-virtual {p1, v0}, Ld/k/b/b$a;->c(I)V

    iget-object p1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    invoke-virtual {p1, v1}, Ld/k/b/b$a;->b(I)V

    iget-object p1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    invoke-virtual {p1}, Ld/k/b/b$a;->d()V

    return-void
.end method

.method private c(IIIII)V
    .locals 10

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v4, 0x2

    aput-object v1, v0, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v4, 0x3

    aput-object v1, v0, v4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v4, 0x4

    aput-object v1, v0, v4

    const-string v1, "startAfterEdge: start(%d) velocity(%d) boundary(%d, %d) over(%d)"

    invoke-static {v1, v0}, Ld/k/b/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    if-le p1, p2, :cond_0

    if-ge p1, p3, :cond_0

    invoke-virtual {p0, v3}, Ld/k/b/d$a;->a(Z)V

    return-void

    :cond_0
    if-le p1, p3, :cond_1

    move v0, v3

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    move v8, p3

    goto :goto_1

    :cond_2
    move v8, p2

    :goto_1
    sub-int v1, p1, v8

    if-eqz p4, :cond_3

    invoke-static {v1}, Ljava/lang/Integer;->signum(I)I

    move-result v1

    mul-int/2addr v1, p4

    if-ltz v1, :cond_3

    move v2, v3

    :cond_3
    if-eqz v2, :cond_4

    const-string p2, "spring forward"

    invoke-static {p2}, Ld/k/b/c;->a(Ljava/lang/String;)V

    const/4 v5, 0x2

    int-to-float v7, p4

    move-object v4, p0

    move v6, p1

    move v9, p5

    invoke-direct/range {v4 .. v9}, Ld/k/b/b;->a(IIFII)V

    goto :goto_2

    :cond_4
    iget-object v1, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    int-to-float v2, p1

    invoke-virtual {v1, v2}, Ld/k/a/a/a/s;->e(F)Ld/k/a/a/a/s;

    iget-object v1, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    int-to-float v7, p4

    invoke-virtual {v1, v7}, Ld/k/a/a/a/t;->f(F)Ld/k/a/a/a/t;

    iget-object v1, p0, Ld/k/b/b;->w:Ld/k/a/a/a/t;

    invoke-virtual {v1}, Ld/k/a/a/a/t;->e()F

    move-result v1

    if-eqz v0, :cond_5

    int-to-float v2, p3

    cmpg-float v2, v1, v2

    if-ltz v2, :cond_6

    :cond_5
    if-nez v0, :cond_7

    int-to-float v0, p2

    cmpl-float v0, v1, v0

    if-lez v0, :cond_7

    :cond_6
    const-string v0, "fling to content"

    invoke-static {v0}, Ld/k/b/c;->a(Ljava/lang/String;)V

    move-object v0, p0

    move v1, p1

    move v2, p4

    move v3, p2

    move v4, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Ld/k/b/b;->b(IIIII)V

    goto :goto_2

    :cond_7
    const-string p2, "spring backward"

    invoke-static {p2}, Ld/k/b/c;->a(Ljava/lang/String;)V

    const/4 v5, 0x1

    move-object v4, p0

    move v6, p1

    move v9, p5

    invoke-direct/range {v4 .. v9}, Ld/k/b/b;->a(IIFII)V

    :goto_2
    return-void
.end method

.method static synthetic c(Ld/k/b/b;)V
    .locals 0

    invoke-direct {p0}, Ld/k/b/b;->k()V

    return-void
.end method

.method private k()V
    .locals 3

    iget-object v0, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ld/k/b/d$a;->h()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    invoke-virtual {v2}, Ld/k/b/b$a;->c()Ld/k/a/a/a/s;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    iget v2, v2, Ld/k/b/b$a;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    iget v2, v2, Ld/k/b/b$a;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "resetting current handler: state(%d), anim(%s), value(%d), velocity(%f)"

    invoke-static {v1, v0}, Ld/k/b/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    invoke-virtual {v0}, Ld/k/b/b$a;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(D)V
    .locals 2

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    const-wide v0, 0x40b3880000000000L    # 5000.0

    cmpg-double p1, p1, v0

    if-gtz p1, :cond_0

    iget-object p1, p0, Ld/k/b/b;->v:Ld/k/a/a/a/x;

    invoke-virtual {p1}, Ld/k/a/a/a/x;->d()Ld/k/a/a/a/y;

    move-result-object p1

    const p2, 0x4376b333    # 246.7f

    invoke-virtual {p1, p2}, Ld/k/a/a/a/y;->c(F)Ld/k/a/a/a/y;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Ld/k/b/b;->v:Ld/k/a/a/a/x;

    invoke-virtual {p1}, Ld/k/a/a/a/x;->d()Ld/k/a/a/a/y;

    move-result-object p1

    const p2, 0x43028000    # 130.5f

    invoke-virtual {p1, p2}, Ld/k/a/a/a/y;->c(F)Ld/k/a/a/a/y;

    :goto_0
    return-void
.end method

.method public a(I)V
    .locals 1

    invoke-virtual {p0}, Ld/k/b/d$a;->g()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Ld/k/b/b;->e(I)V

    return-void
.end method

.method a(III)V
    .locals 7

    goto/32 :goto_2

    nop

    :goto_0
    move v3, p2

    goto/32 :goto_8

    nop

    :goto_1
    float-to-int v5, v0

    goto/32 :goto_d

    nop

    :goto_2
    invoke-virtual {p0}, Ld/k/b/d$a;->h()I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_5

    nop

    :goto_4
    move v6, p3

    goto/32 :goto_b

    nop

    :goto_5
    iget-object v0, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    goto/32 :goto_9

    nop

    :goto_6
    invoke-direct {p0}, Ld/k/b/b;->k()V

    :goto_7
    goto/32 :goto_a

    nop

    :goto_8
    move v4, p2

    goto/32 :goto_4

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_6

    nop

    :goto_a
    invoke-virtual {p0}, Ld/k/b/d$a;->d()F

    move-result v0

    goto/32 :goto_1

    nop

    :goto_b
    invoke-direct/range {v1 .. v6}, Ld/k/b/b;->c(IIIII)V

    :goto_c
    goto/32 :goto_e

    nop

    :goto_d
    move-object v1, p0

    goto/32 :goto_f

    nop

    :goto_e
    return-void

    :goto_f
    move v2, p1

    goto/32 :goto_0

    nop
.end method

.method a(IIIII)V
    .locals 6

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x5

    goto/32 :goto_1a

    nop

    :goto_1
    invoke-virtual {p0, p1}, Ld/k/b/d$a;->b(I)V

    goto/32 :goto_8

    nop

    :goto_2
    move v1, p1

    goto/32 :goto_f

    nop

    :goto_3
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_14

    nop

    :goto_4
    invoke-static {v1, v0}, Ld/k/b/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_6

    nop

    :goto_5
    const-string v1, "FLING: start(%d) velocity(%d) boundary(%d, %d) over(%d)"

    goto/32 :goto_4

    nop

    :goto_6
    invoke-direct {p0}, Ld/k/b/b;->k()V

    goto/32 :goto_1e

    nop

    :goto_7
    move v3, p4

    goto/32 :goto_25

    nop

    :goto_8
    invoke-virtual {p0, p1}, Ld/k/b/d$a;->f(I)V

    goto/32 :goto_29

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_13

    nop

    :goto_b
    goto :goto_16

    :goto_c
    goto/32 :goto_17

    nop

    :goto_d
    aput-object v1, v0, v3

    goto/32 :goto_e

    nop

    :goto_e
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_23

    nop

    :goto_f
    move v2, p3

    goto/32 :goto_7

    nop

    :goto_10
    invoke-virtual {p0, v2}, Ld/k/b/d$a;->c(I)V

    goto/32 :goto_27

    nop

    :goto_11
    aput-object v1, v0, v4

    goto/32 :goto_5

    nop

    :goto_12
    if-le p1, p4, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_2a

    nop

    :goto_13
    int-to-double v0, p2

    goto/32 :goto_1d

    nop

    :goto_14
    const/4 v4, 0x3

    goto/32 :goto_24

    nop

    :goto_15
    return-void

    :goto_16
    goto/32 :goto_1f

    nop

    :goto_17
    invoke-direct/range {p0 .. p5}, Ld/k/b/b;->b(IIIII)V

    goto/32 :goto_15

    nop

    :goto_18
    const/4 v4, 0x4

    goto/32 :goto_11

    nop

    :goto_19
    invoke-direct/range {v0 .. v5}, Ld/k/b/b;->c(IIIII)V

    goto/32 :goto_2b

    nop

    :goto_1a
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_1c

    nop

    :goto_1b
    const/4 v3, 0x1

    goto/32 :goto_d

    nop

    :goto_1c
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_21

    nop

    :goto_1d
    invoke-virtual {p0, v0, v1}, Ld/k/b/b;->a(D)V

    goto/32 :goto_12

    nop

    :goto_1e
    if-eqz p2, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_1

    nop

    :goto_1f
    move-object v0, p0

    goto/32 :goto_2

    nop

    :goto_20
    aput-object v1, v0, v4

    goto/32 :goto_3

    nop

    :goto_21
    const/4 v2, 0x0

    goto/32 :goto_2c

    nop

    :goto_22
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_23
    const/4 v4, 0x2

    goto/32 :goto_20

    nop

    :goto_24
    aput-object v1, v0, v4

    goto/32 :goto_26

    nop

    :goto_25
    move v4, p2

    goto/32 :goto_28

    nop

    :goto_26
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_18

    nop

    :goto_27
    invoke-virtual {p0, v3}, Ld/k/b/d$a;->a(Z)V

    goto/32 :goto_9

    nop

    :goto_28
    move v5, p5

    goto/32 :goto_19

    nop

    :goto_29
    invoke-virtual {p0, p1}, Ld/k/b/d$a;->d(I)V

    goto/32 :goto_10

    nop

    :goto_2a
    if-lt p1, p3, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_b

    nop

    :goto_2b
    return-void

    :goto_2c
    aput-object v1, v0, v2

    goto/32 :goto_22

    nop
.end method

.method b()Z
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_5

    nop

    :goto_1
    invoke-virtual {p0}, Ld/k/b/b;->j()Z

    goto/32 :goto_6

    nop

    :goto_2
    invoke-static {v0}, Ld/k/b/c;->a(Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_3
    return v0

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    const-string v0, "checking have more work when finish"

    goto/32 :goto_2

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_3

    nop

    :goto_7
    iget-object v0, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    goto/32 :goto_b

    nop

    :goto_8
    return v0

    :goto_9
    invoke-virtual {v0}, Ld/k/b/b$a;->b()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_a
    const/4 v0, 0x0

    goto/32 :goto_8

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_9

    nop
.end method

.method b(III)Z
    .locals 8

    goto/32 :goto_27

    nop

    :goto_0
    const/4 v1, 0x1

    goto/32 :goto_c

    nop

    :goto_1
    move-object v0, p0

    goto/32 :goto_4

    nop

    :goto_2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_3
    invoke-direct/range {v0 .. v5}, Ld/k/b/b;->a(IIFII)V

    goto/32 :goto_11

    nop

    :goto_4
    move v2, p1

    goto/32 :goto_1f

    nop

    :goto_5
    invoke-direct/range {v0 .. v5}, Ld/k/b/b;->a(IIFII)V

    goto/32 :goto_f

    nop

    :goto_6
    const/4 v6, 0x0

    goto/32 :goto_14

    nop

    :goto_7
    invoke-virtual {p0, p1}, Ld/k/b/d$a;->d(I)V

    goto/32 :goto_29

    nop

    :goto_8
    aput-object v1, v0, v3

    goto/32 :goto_1b

    nop

    :goto_9
    const/4 v5, 0x0

    goto/32 :goto_1

    nop

    :goto_a
    iget-object v0, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    goto/32 :goto_24

    nop

    :goto_b
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_2f

    nop

    :goto_c
    const/4 v3, 0x0

    goto/32 :goto_1e

    nop

    :goto_d
    invoke-virtual {p0, p1}, Ld/k/b/d$a;->f(I)V

    goto/32 :goto_7

    nop

    :goto_e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_f
    goto :goto_26

    :goto_10
    goto/32 :goto_15

    nop

    :goto_11
    goto :goto_26

    :goto_12
    goto/32 :goto_23

    nop

    :goto_13
    const/4 v7, 0x1

    goto/32 :goto_16

    nop

    :goto_14
    aput-object v1, v0, v6

    goto/32 :goto_e

    nop

    :goto_15
    invoke-virtual {p0, p1}, Ld/k/b/d$a;->b(I)V

    goto/32 :goto_d

    nop

    :goto_16
    aput-object v1, v0, v7

    goto/32 :goto_b

    nop

    :goto_17
    return v6

    :goto_18
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_19
    invoke-direct {p0}, Ld/k/b/b;->k()V

    :goto_1a
    goto/32 :goto_1c

    nop

    :goto_1b
    const-string v1, "SPRING_BACK start(%d) boundary(%d, %d)"

    goto/32 :goto_2d

    nop

    :goto_1c
    if-lt p1, p2, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_0

    nop

    :goto_1d
    move v4, p2

    goto/32 :goto_3

    nop

    :goto_1e
    const/4 v5, 0x0

    goto/32 :goto_2c

    nop

    :goto_1f
    move v4, p3

    goto/32 :goto_5

    nop

    :goto_20
    const/4 v3, 0x0

    goto/32 :goto_9

    nop

    :goto_21
    move v6, v7

    :goto_22
    goto/32 :goto_17

    nop

    :goto_23
    if-gt p1, p3, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_28

    nop

    :goto_24
    if-nez v0, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_19

    nop

    :goto_25
    invoke-virtual {p0, v7}, Ld/k/b/d$a;->a(Z)V

    :goto_26
    goto/32 :goto_2e

    nop

    :goto_27
    const/4 v0, 0x3

    goto/32 :goto_18

    nop

    :goto_28
    const/4 v1, 0x1

    goto/32 :goto_20

    nop

    :goto_29
    invoke-virtual {p0, v6}, Ld/k/b/d$a;->c(I)V

    goto/32 :goto_25

    nop

    :goto_2a
    if-eqz v0, :cond_3

    goto/32 :goto_22

    :cond_3
    goto/32 :goto_21

    nop

    :goto_2b
    move v2, p1

    goto/32 :goto_1d

    nop

    :goto_2c
    move-object v0, p0

    goto/32 :goto_2b

    nop

    :goto_2d
    invoke-static {v1, v0}, Ld/k/b/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_a

    nop

    :goto_2e
    invoke-virtual {p0}, Ld/k/b/d$a;->i()Z

    move-result v0

    goto/32 :goto_2a

    nop

    :goto_2f
    const/4 v3, 0x2

    goto/32 :goto_8

    nop
.end method

.method c()V
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0}, Ld/k/b/b;->k()V

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {p0}, Ld/k/b/d$a;->f()I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_3
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {p0, v0}, Ld/k/b/d$a;->a(Z)V

    goto/32 :goto_1

    nop

    :goto_5
    const-string v0, "finish scroller"

    goto/32 :goto_7

    nop

    :goto_6
    invoke-virtual {p0, v0}, Ld/k/b/d$a;->b(I)V

    goto/32 :goto_3

    nop

    :goto_7
    invoke-static {v0}, Ld/k/b/c;->a(Ljava/lang/String;)V

    goto/32 :goto_2

    nop
.end method

.method e(I)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-super {p0, p1}, Ld/k/b/d$a;->e(I)V

    goto/32 :goto_0

    nop
.end method

.method j()Z
    .locals 4

    goto/32 :goto_1d

    nop

    :goto_0
    const-string v1, "State Changed: BALLISTIC -> CUBIC"

    goto/32 :goto_21

    nop

    :goto_1
    if-ltz v1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_0

    nop

    :goto_2
    iget-object v1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    goto/32 :goto_14

    nop

    :goto_3
    const/4 v2, 0x0

    goto/32 :goto_12

    nop

    :goto_4
    iget v1, v1, Ld/k/b/b$a;->f:I

    goto/32 :goto_c

    nop

    :goto_5
    invoke-static {v0}, Ld/k/b/c;->a(Ljava/lang/String;)V

    goto/32 :goto_e

    nop

    :goto_6
    if-eqz v0, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_7
    iget-object v2, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    goto/32 :goto_1e

    nop

    :goto_8
    return v0

    :goto_9
    invoke-virtual {p0, v1}, Ld/k/b/d$a;->a(F)V

    goto/32 :goto_22

    nop

    :goto_a
    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    goto/32 :goto_7

    nop

    :goto_b
    invoke-virtual {p0, v1}, Ld/k/b/d$a;->b(I)V

    goto/32 :goto_2

    nop

    :goto_c
    int-to-float v1, v1

    goto/32 :goto_a

    nop

    :goto_d
    if-eq v1, v2, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_1c

    nop

    :goto_e
    const/4 v0, 0x0

    goto/32 :goto_15

    nop

    :goto_f
    invoke-virtual {p0, v3}, Ld/k/b/d$a;->g(I)V

    :goto_10
    goto/32 :goto_13

    nop

    :goto_11
    invoke-virtual {v0}, Ld/k/b/b$a;->e()Z

    move-result v0

    goto/32 :goto_1f

    nop

    :goto_12
    cmpg-float v1, v1, v2

    goto/32 :goto_1

    nop

    :goto_13
    xor-int/2addr v0, v3

    goto/32 :goto_8

    nop

    :goto_14
    iget v1, v1, Ld/k/b/b$a;->e:F

    goto/32 :goto_9

    nop

    :goto_15
    return v0

    :goto_16
    goto/32 :goto_11

    nop

    :goto_17
    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    goto/32 :goto_19

    nop

    :goto_18
    iget v1, v1, Ld/k/b/b$a;->f:I

    goto/32 :goto_b

    nop

    :goto_19
    mul-float/2addr v1, v2

    goto/32 :goto_3

    nop

    :goto_1a
    const-string v0, "no handler found, aborting"

    goto/32 :goto_5

    nop

    :goto_1b
    const/4 v2, 0x2

    goto/32 :goto_20

    nop

    :goto_1c
    iget-object v1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    goto/32 :goto_4

    nop

    :goto_1d
    iget-object v0, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    goto/32 :goto_6

    nop

    :goto_1e
    iget v2, v2, Ld/k/b/b$a;->e:F

    goto/32 :goto_17

    nop

    :goto_1f
    iget-object v1, p0, Ld/k/b/b;->x:Ld/k/b/b$a;

    goto/32 :goto_18

    nop

    :goto_20
    const/4 v3, 0x1

    goto/32 :goto_d

    nop

    :goto_21
    invoke-static {v1}, Ld/k/b/c;->a(Ljava/lang/String;)V

    goto/32 :goto_f

    nop

    :goto_22
    invoke-virtual {p0}, Ld/k/b/d$a;->h()I

    move-result v1

    goto/32 :goto_1b

    nop
.end method
