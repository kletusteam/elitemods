.class Ld/k/b/b$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/k/b/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld/k/b/b$a$b;,
        Ld/k/b/b$a$a;
    }
.end annotation


# instance fields
.field a:Ld/k/a/a/a/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ld/k/a/a/a/s<",
            "*>;"
        }
    .end annotation
.end field

.field b:I

.field private final c:I

.field private final d:I

.field e:F

.field f:I

.field private g:Ld/k/b/b$a$b;

.field private h:F

.field private i:F

.field private j:J

.field private k:Ld/k/b/b$a$a;


# direct methods
.method constructor <init>(Ld/k/a/a/a/s;IF)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld/k/a/a/a/s<",
            "*>;IF)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ld/k/b/b$a$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ld/k/b/b$a$a;-><init>(Ld/k/b/b$a;Ld/k/b/a;)V

    iput-object v0, p0, Ld/k/b/b$a;->k:Ld/k/b/b$a$a;

    iput-object p1, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    iget-object p1, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    const v0, -0x800001

    invoke-virtual {p1, v0}, Ld/k/a/a/a/s;->b(F)Ld/k/a/a/a/s;

    iget-object p1, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    invoke-virtual {p1, v0}, Ld/k/a/a/a/s;->a(F)Ld/k/a/a/a/s;

    iput p2, p0, Ld/k/b/b$a;->b:I

    iput p3, p0, Ld/k/b/b$a;->e:F

    const p1, 0x7fffffff

    const/high16 v0, -0x80000000

    if-lez p2, :cond_0

    add-int/2addr v0, p2

    goto :goto_0

    :cond_0
    if-gez p2, :cond_1

    add-int/2addr p1, p2

    :cond_1
    :goto_0
    iput v0, p0, Ld/k/b/b$a;->c:I

    iput p1, p0, Ld/k/b/b$a;->d:I

    iget-object p1, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ld/k/a/a/a/s;->e(F)Ld/k/a/a/a/s;

    iget-object p1, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    invoke-virtual {p1, p3}, Ld/k/a/a/a/s;->f(F)Ld/k/a/a/a/s;

    return-void
.end method

.method static synthetic a(Ld/k/b/b$a;)F
    .locals 0

    iget p0, p0, Ld/k/b/b$a;->h:F

    return p0
.end method

.method static synthetic b(Ld/k/b/b$a;)F
    .locals 0

    iget p0, p0, Ld/k/b/b$a;->i:F

    return p0
.end method


# virtual methods
.method a(I)I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Ld/k/b/b$a;->b:I

    goto/32 :goto_2

    nop

    :goto_1
    return p1

    :goto_2
    sub-int/2addr p1, v0

    goto/32 :goto_1

    nop
.end method

.method a()V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0, v1}, Ld/k/a/a/a/s;->b(Ld/k/a/a/a/s$c;)V

    goto/32 :goto_0

    nop

    :goto_2
    iput-wide v0, p0, Ld/k/b/b$a;->j:J

    goto/32 :goto_7

    nop

    :goto_3
    const-wide/16 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_4
    iget-object v0, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    goto/32 :goto_5

    nop

    :goto_5
    iget-object v1, p0, Ld/k/b/b$a;->k:Ld/k/b/b$a$a;

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {v0}, Ld/k/a/a/a/s;->a()V

    goto/32 :goto_4

    nop

    :goto_7
    iget-object v0, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    goto/32 :goto_6

    nop
.end method

.method a(Ld/k/b/b$a$b;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Ld/k/b/b$a;->g:Ld/k/b/b$a$b;

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method b(I)V
    .locals 1

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {v0, p1}, Ld/k/a/a/a/s;->a(F)Ld/k/a/a/a/s;

    goto/32 :goto_8

    nop

    :goto_1
    int-to-float p1, p1

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    sub-int/2addr p1, v0

    goto/32 :goto_7

    nop

    :goto_4
    iget-object v0, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    goto/32 :goto_0

    nop

    :goto_5
    move p1, v0

    :goto_6
    goto/32 :goto_c

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_8
    iput p1, p0, Ld/k/b/b$a;->i:F

    goto/32 :goto_2

    nop

    :goto_9
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto/32 :goto_1

    nop

    :goto_a
    iget v0, p0, Ld/k/b/b$a;->d:I

    goto/32 :goto_b

    nop

    :goto_b
    if-gt p1, v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_c
    iget v0, p0, Ld/k/b/b$a;->b:I

    goto/32 :goto_3

    nop
.end method

.method b()Z
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Ld/k/b/b$a;->g:Ld/k/b/b$a$b;

    goto/32 :goto_5

    nop

    :goto_1
    return v0

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    int-to-float v1, v1

    goto/32 :goto_9

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_8

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_6

    nop

    :goto_6
    iget v1, p0, Ld/k/b/b$a;->f:I

    goto/32 :goto_3

    nop

    :goto_7
    invoke-interface {v0, v1, v2}, Ld/k/b/b$a$b;->a(FF)Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_8
    return v0

    :goto_9
    iget v2, p0, Ld/k/b/b$a;->e:F

    goto/32 :goto_7

    nop
.end method

.method c()Ld/k/a/a/a/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ld/k/a/a/a/s<",
            "*>;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method c(I)V
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    invoke-virtual {v0, p1}, Ld/k/a/a/a/s;->b(F)Ld/k/a/a/a/s;

    goto/32 :goto_6

    nop

    :goto_1
    sub-int/2addr p1, v0

    goto/32 :goto_2

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_c

    nop

    :goto_3
    return-void

    :goto_4
    move p1, v0

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    iput p1, p0, Ld/k/b/b$a;->h:F

    goto/32 :goto_3

    nop

    :goto_7
    iget v0, p0, Ld/k/b/b$a;->c:I

    goto/32 :goto_9

    nop

    :goto_8
    int-to-float p1, p1

    goto/32 :goto_b

    nop

    :goto_9
    if-lt p1, v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_a
    iget v0, p0, Ld/k/b/b$a;->b:I

    goto/32 :goto_1

    nop

    :goto_b
    iget-object v0, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    goto/32 :goto_0

    nop

    :goto_c
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto/32 :goto_8

    nop
.end method

.method d()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    goto/32 :goto_6

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_7

    nop

    :goto_4
    iput-wide v0, p0, Ld/k/b/b$a;->j:J

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {v0, v1}, Ld/k/a/a/a/s;->a(Ld/k/a/a/a/s$c;)Ld/k/a/a/a/s;

    goto/32 :goto_1

    nop

    :goto_6
    iget-object v1, p0, Ld/k/b/b$a;->k:Ld/k/b/b$a$a;

    goto/32 :goto_5

    nop

    :goto_7
    invoke-virtual {v0, v1}, Ld/k/a/a/a/s;->a(Z)V

    goto/32 :goto_8

    nop

    :goto_8
    const-wide/16 v0, 0x0

    goto/32 :goto_4

    nop
.end method

.method e()Z
    .locals 7

    goto/32 :goto_25

    nop

    :goto_0
    iget-object v6, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    goto/32 :goto_1b

    nop

    :goto_1
    invoke-virtual {v0}, Ld/k/a/a/a/s;->c()Z

    move-result v0

    goto/32 :goto_13

    nop

    :goto_2
    return v0

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_12

    nop

    :goto_4
    iget-object v0, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    goto/32 :goto_f

    nop

    :goto_5
    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_19

    nop

    :goto_6
    const-string v0, "update done in this frame, dropping current update request"

    goto/32 :goto_18

    nop

    :goto_7
    invoke-static {v1, v4}, Ld/k/b/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_1f

    nop

    :goto_8
    new-array v4, v4, [Ljava/lang/Object;

    goto/32 :goto_1d

    nop

    :goto_9
    const/4 v1, 0x2

    goto/32 :goto_b

    nop

    :goto_a
    iget v5, p0, Ld/k/b/b$a;->f:I

    goto/32 :goto_16

    nop

    :goto_b
    iget v5, p0, Ld/k/b/b$a;->e:F

    goto/32 :goto_11

    nop

    :goto_c
    iput-wide v4, p0, Ld/k/b/b$a;->j:J

    :goto_d
    goto/32 :goto_1e

    nop

    :goto_e
    invoke-virtual {v1, v4}, Ld/k/a/a/a/s;->b(Ld/k/a/a/a/s$c;)V

    goto/32 :goto_15

    nop

    :goto_f
    invoke-virtual {v0, v2, v3}, Ld/k/a/a/a/s;->a(J)Z

    move-result v0

    goto/32 :goto_26

    nop

    :goto_10
    const-string v1, "%s finishing value(%d) velocity(%f)"

    goto/32 :goto_7

    nop

    :goto_11
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    goto/32 :goto_1a

    nop

    :goto_12
    if-eqz v0, :cond_0

    goto/32 :goto_22

    :cond_0
    goto/32 :goto_6

    nop

    :goto_13
    xor-int/2addr v0, v1

    goto/32 :goto_21

    nop

    :goto_14
    cmp-long v0, v2, v0

    goto/32 :goto_3

    nop

    :goto_15
    const-wide/16 v4, 0x0

    goto/32 :goto_c

    nop

    :goto_16
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_1c

    nop

    :goto_17
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    goto/32 :goto_14

    nop

    :goto_18
    invoke-static {v0}, Ld/k/b/c;->b(Ljava/lang/String;)V

    goto/32 :goto_24

    nop

    :goto_19
    aput-object v6, v4, v5

    goto/32 :goto_a

    nop

    :goto_1a
    aput-object v5, v4, v1

    goto/32 :goto_10

    nop

    :goto_1b
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    goto/32 :goto_5

    nop

    :goto_1c
    aput-object v5, v4, v1

    goto/32 :goto_9

    nop

    :goto_1d
    const/4 v5, 0x0

    goto/32 :goto_0

    nop

    :goto_1e
    iput-wide v2, p0, Ld/k/b/b$a;->j:J

    goto/32 :goto_2

    nop

    :goto_1f
    iget-object v1, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    goto/32 :goto_20

    nop

    :goto_20
    iget-object v4, p0, Ld/k/b/b$a;->k:Ld/k/b/b$a$a;

    goto/32 :goto_e

    nop

    :goto_21
    return v0

    :goto_22
    goto/32 :goto_4

    nop

    :goto_23
    const/4 v4, 0x3

    goto/32 :goto_8

    nop

    :goto_24
    iget-object v0, p0, Ld/k/b/b$a;->a:Ld/k/a/a/a/s;

    goto/32 :goto_1

    nop

    :goto_25
    iget-wide v0, p0, Ld/k/b/b$a;->j:J

    goto/32 :goto_17

    nop

    :goto_26
    if-nez v0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_23

    nop
.end method
