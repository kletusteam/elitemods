.class public Ld/g/a/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld/g/a/b$a;
    }
.end annotation


# instance fields
.field public a:J

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ld/g/a/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field public d:I

.field private e:Ld/g/a/a;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/32 v0, 0x100000

    iput-wide v0, p0, Ld/g/a/b;->a:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ld/g/a/b;->b:Ljava/util/List;

    return-void
.end method

.method private a(I)I
    .locals 1

    iget v0, p0, Ld/g/a/b;->d:I

    if-nez v0, :cond_0

    return p1

    :cond_0
    rem-int/2addr p1, v0

    return p1
.end method

.method private b()I
    .locals 2

    iget-object v0, p0, Ld/g/a/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld/g/a/b$a;

    iget v0, v0, Ld/g/a/b$a;->c:I

    return v0
.end method


# virtual methods
.method public a()V
    .locals 6

    iget-object v0, p0, Ld/g/a/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Ld/g/a/b;->c:I

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x3

    if-gt v1, v5, :cond_0

    if-gt v0, v3, :cond_1

    :goto_0
    move v2, v4

    goto :goto_1

    :cond_0
    div-int/2addr v1, v3

    if-gt v0, v1, :cond_1

    goto :goto_0

    :cond_1
    :goto_1
    if-nez v2, :cond_2

    return-void

    :cond_2
    invoke-direct {p0}, Ld/g/a/b;->b()I

    move-result v0

    add-int/2addr v0, v4

    invoke-direct {p0, v0}, Ld/g/a/b;->a(I)I

    move-result v0

    iget-object v1, p0, Ld/g/a/b;->e:Ld/g/a/a;

    invoke-virtual {v1, v0}, Ld/g/a/a;->a(I)V

    const/4 v0, 0x0

    throw v0
.end method
