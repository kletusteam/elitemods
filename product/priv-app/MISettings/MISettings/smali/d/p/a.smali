.class public final Ld/p/a;
.super Ljava/lang/Object;


# static fields
.field public static final alpha:I = 0x7f040074

.field public static final dependencyType:I = 0x7f040152

.field public static final fastScrollEnabled:I = 0x7f0401a7

.field public static final fastScrollHorizontalThumbDrawable:I = 0x7f0401a8

.field public static final fastScrollHorizontalTrackDrawable:I = 0x7f0401a9

.field public static final fastScrollVerticalThumbDrawable:I = 0x7f0401aa

.field public static final fastScrollVerticalTrackDrawable:I = 0x7f0401ab

.field public static final font:I = 0x7f0401c8

.field public static final fontProviderAuthority:I = 0x7f0401ca

.field public static final fontProviderCerts:I = 0x7f0401cb

.field public static final fontProviderFetchStrategy:I = 0x7f0401cc

.field public static final fontProviderFetchTimeout:I = 0x7f0401cd

.field public static final fontProviderPackage:I = 0x7f0401ce

.field public static final fontProviderQuery:I = 0x7f0401cf

.field public static final fontStyle:I = 0x7f0401d1

.field public static final fontVariationSettings:I = 0x7f0401d2

.field public static final fontWeight:I = 0x7f0401d3

.field public static final layoutManager:I = 0x7f040219

.field public static final level:I = 0x7f04025c

.field public static final maxLevel:I = 0x7f04028f

.field public static final minLevel:I = 0x7f04029a

.field public static final moduleContent:I = 0x7f0402c6

.field public static final name:I = 0x7f0402d9

.field public static final recyclerViewStyle:I = 0x7f040359

.field public static final reverseLayout:I = 0x7f040362

.field public static final spanCount:I = 0x7f0403b1

.field public static final stackFromEnd:I = 0x7f0403c6

.field public static final targetLevel:I = 0x7f0403f0

.field public static final ttcIndex:I = 0x7f040453
