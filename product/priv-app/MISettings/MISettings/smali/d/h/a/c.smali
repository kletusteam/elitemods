.class public Ld/h/a/c;
.super Ljava/lang/Object;


# static fields
.field private static volatile a:Z


# direct methods
.method public static a(Landroid/view/View;)V
    .locals 1

    sget-object v0, Lmiuix/animation/i$a;->a:Lmiuix/animation/i$a;

    invoke-static {p0, v0}, Ld/h/a/c;->a(Landroid/view/View;Lmiuix/animation/i$a;)V

    return-void
.end method

.method public static a(Landroid/view/View;Lmiuix/animation/i$a;)V
    .locals 5

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v1

    new-array v3, v2, [Lmiuix/animation/m$b;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v1, v4, v3}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    new-array v3, v2, [Lmiuix/animation/a/a;

    invoke-interface {v1, p0, v3}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    new-array v0, v0, [Landroid/view/View;

    aput-object p0, v0, v2

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->c()Lmiuix/animation/i;

    move-result-object v0

    invoke-interface {v0, p1}, Lmiuix/animation/i;->a(Lmiuix/animation/i$a;)Lmiuix/animation/i;

    new-array p1, v2, [Lmiuix/animation/a/a;

    invoke-interface {v0, p0, p1}, Lmiuix/animation/i;->b(Landroid/view/View;[Lmiuix/animation/a/a;)V

    return-void
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, Ld/h/a/c;->a:Z

    return v0
.end method

.method public static b(Landroid/view/View;)V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3, v3, v3, v3}, Lmiuix/animation/m;->a(FFFF)Lmiuix/animation/m;

    new-array v4, v2, [Lmiuix/animation/m$b;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v1, v5, v4}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    new-array v0, v0, [Landroid/view/View;

    aput-object p0, v0, v2

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->c()Lmiuix/animation/i;

    move-result-object v0

    invoke-interface {v0, v3, v3, v3, v3}, Lmiuix/animation/i;->a(FFFF)Lmiuix/animation/i;

    sget-object v4, Lmiuix/animation/i$a;->a:Lmiuix/animation/i$a;

    invoke-interface {v0, v4}, Lmiuix/animation/i;->a(Lmiuix/animation/i$a;)Lmiuix/animation/i;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Ld/h/a/i;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    const v3, 0x3e19999a    # 0.15f

    invoke-interface {v1, v3, v5, v5, v5}, Lmiuix/animation/m;->b(FFFF)Lmiuix/animation/m;

    invoke-interface {v0, v3, v5, v5, v5}, Lmiuix/animation/i;->b(FFFF)Lmiuix/animation/i;

    goto :goto_0

    :cond_0
    const v4, 0x3da3d70a    # 0.08f

    invoke-interface {v1, v4, v3, v3, v3}, Lmiuix/animation/m;->b(FFFF)Lmiuix/animation/m;

    invoke-interface {v0, v4, v3, v3, v3}, Lmiuix/animation/i;->b(FFFF)Lmiuix/animation/i;

    :goto_0
    new-array v3, v2, [Lmiuix/animation/a/a;

    invoke-interface {v1, p0, v3}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    new-array v1, v2, [Lmiuix/animation/a/a;

    invoke-interface {v0, p0, v1}, Lmiuix/animation/i;->b(Landroid/view/View;[Lmiuix/animation/a/a;)V

    return-void
.end method
