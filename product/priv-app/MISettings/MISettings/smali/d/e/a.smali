.class public Ld/e/a;
.super Ljava/lang/Object;


# static fields
.field public static A:I

.field public static B:I

.field private static C:I

.field private static D:I

.field private static E:I

.field private static F:I

.field static final a:Ljava/util/regex/Pattern;

.field static final b:Ljava/util/regex/Pattern;

.field static c:Ljava/lang/Boolean;

.field static d:I

.field private static e:I

.field private static f:Ljava/lang/Boolean;

.field private static g:Ljava/lang/Boolean;

.field static h:I

.field static i:I

.field static j:I

.field static k:I

.field static l:I

.field private static final m:[Ljava/lang/String;

.field private static n:Ljava/lang/Class;

.field private static o:Ldalvik/system/PathClassLoader;

.field private static p:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field private static q:Ljava/lang/Object;

.field private static r:Ljava/lang/reflect/Method;

.field private static s:Ljava/lang/reflect/Method;

.field private static t:Ljava/lang/reflect/Method;

.field private static u:Ljava/lang/reflect/Method;

.field private static v:Landroid/app/Application;

.field private static w:Landroid/content/Context;

.field public static x:I

.field private static y:I

.field public static z:I


# direct methods
.method static constructor <clinit>()V
    .locals 43

    const-string v0, "getDeviceLevel"

    const-string v1, "DeviceUtils"

    const-string v2, "Inc ([A-Z]+)([\\d]+)"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    sput-object v2, Ld/e/a;->a:Ljava/util/regex/Pattern;

    const-string v2, "MT([\\d]{2})([\\d]+)"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    sput-object v2, Ld/e/a;->b:Ljava/util/regex/Pattern;

    const/4 v2, 0x0

    sput-object v2, Ld/e/a;->c:Ljava/lang/Boolean;

    const/4 v3, -0x2

    sput v3, Ld/e/a;->d:I

    const/4 v3, -0x1

    sput v3, Ld/e/a;->e:I

    sput-object v2, Ld/e/a;->f:Ljava/lang/Boolean;

    sput-object v2, Ld/e/a;->g:Ljava/lang/Boolean;

    sput v3, Ld/e/a;->h:I

    sput v3, Ld/e/a;->i:I

    sput v3, Ld/e/a;->j:I

    sput v3, Ld/e/a;->k:I

    const v3, 0x7fffffff

    sput v3, Ld/e/a;->l:I

    const-string v4, "cactus"

    const-string v5, "cereus"

    const-string v6, "pine"

    const-string v7, "olive"

    const-string v8, "ginkgo"

    const-string v9, "olivelite"

    const-string v10, "olivewood"

    const-string v11, "willow"

    const-string v12, "wayne"

    const-string v13, "dandelion"

    const-string v14, "angelica"

    const-string v15, "angelicain"

    const-string v16, "whyred"

    const-string v17, "tulip"

    const-string v18, "onc"

    const-string v19, "onclite"

    const-string v20, "lavender"

    const-string v21, "lotus"

    const-string v22, "laurus"

    const-string v23, "merlinnfc"

    const-string v24, "merlin"

    const-string v25, "lancelot"

    const-string v26, "citrus"

    const-string v27, "pomelo"

    const-string v28, "lemon"

    const-string v29, "shiva"

    const-string v30, "lime"

    const-string v31, "cannon"

    const-string v32, "curtana"

    const-string v33, "durandal"

    const-string v34, "excalibur"

    const-string v35, "joyeuse"

    const-string v36, "gram"

    const-string v37, "sunny"

    const-string v38, "mojito"

    const-string v39, "rainbow"

    const-string v40, "cattail"

    const-string v41, "angelican"

    const-string v42, "camellia"

    filled-new-array/range {v4 .. v42}, [Ljava/lang/String;

    move-result-object v3

    sput-object v3, Ld/e/a;->m:[Ljava/lang/String;

    sput-object v2, Ld/e/a;->p:Ljava/lang/reflect/Constructor;

    sput-object v2, Ld/e/a;->q:Ljava/lang/Object;

    sput-object v2, Ld/e/a;->r:Ljava/lang/reflect/Method;

    sput-object v2, Ld/e/a;->s:Ljava/lang/reflect/Method;

    sput-object v2, Ld/e/a;->t:Ljava/lang/reflect/Method;

    sput-object v2, Ld/e/a;->u:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    sput v3, Ld/e/a;->x:I

    sget v4, Ld/e/a;->x:I

    sput v4, Ld/e/a;->y:I

    sput v3, Ld/e/a;->z:I

    const/4 v4, 0x2

    sput v4, Ld/e/a;->A:I

    const/4 v5, 0x3

    sput v5, Ld/e/a;->B:I

    const/4 v5, 0x0

    :try_start_0
    new-instance v6, Ldalvik/system/PathClassLoader;

    const-string v7, "/system/framework/MiuiBooster.jar"

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    sput-object v6, Ld/e/a;->o:Ldalvik/system/PathClassLoader;

    sget-object v6, Ld/e/a;->o:Ldalvik/system/PathClassLoader;

    const-string v7, "com.miui.performance.DeviceLevelUtils"

    invoke-virtual {v6, v7}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    sput-object v6, Ld/e/a;->n:Ljava/lang/Class;

    sget-object v6, Ld/e/a;->n:Ljava/lang/Class;

    new-array v7, v3, [Ljava/lang/Class;

    const-class v8, Landroid/content/Context;

    aput-object v8, v7, v5

    invoke-virtual {v6, v7}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v6

    sput-object v6, Ld/e/a;->p:Ljava/lang/reflect/Constructor;

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v3

    sget-object v6, Ld/e/a;->n:Ljava/lang/Class;

    invoke-virtual {v6, v0, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    sput-object v4, Ld/e/a;->r:Ljava/lang/reflect/Method;

    new-array v4, v3, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    sget-object v6, Ld/e/a;->n:Ljava/lang/Class;

    invoke-virtual {v6, v0, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Ld/e/a;->s:Ljava/lang/reflect/Method;

    sget-object v0, Ld/e/a;->n:Ljava/lang/Class;

    const-string v4, "isSupportPrune"

    new-array v6, v5, [Ljava/lang/Class;

    invoke-virtual {v0, v4, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Ld/e/a;->t:Ljava/lang/reflect/Method;

    sget-object v0, Ld/e/a;->n:Ljava/lang/Class;

    const-string v4, "getMiuiLiteVersion"

    new-array v6, v5, [Ljava/lang/Class;

    invoke-virtual {v0, v4, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Ld/e/a;->u:Ljava/lang/reflect/Method;

    sget-object v0, Ld/e/a;->n:Ljava/lang/Class;

    const-string v4, "DEVICE_LEVEL_FOR_RAM"

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v0, v4, v6}, Ld/e/a;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Ld/e/a;->z:I

    sget-object v0, Ld/e/a;->n:Ljava/lang/Class;

    const-string v4, "DEVICE_LEVEL_FOR_CPU"

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v0, v4, v6}, Ld/e/a;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Ld/e/a;->A:I

    sget-object v0, Ld/e/a;->n:Ljava/lang/Class;

    const-string v4, "DEVICE_LEVEL_FOR_GPU"

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v0, v4, v6}, Ld/e/a;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Ld/e/a;->B:I

    sget-object v0, Ld/e/a;->n:Ljava/lang/Class;

    const-string v4, "LOW_DEVICE"

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v0, v4, v6}, Ld/e/a;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Ld/e/a;->C:I

    sget-object v0, Ld/e/a;->n:Ljava/lang/Class;

    const-string v4, "MIDDLE_DEVICE"

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v0, v4, v6}, Ld/e/a;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Ld/e/a;->D:I

    sget-object v0, Ld/e/a;->n:Ljava/lang/Class;

    const-string v4, "HIGH_DEVICE"

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v0, v4, v6}, Ld/e/a;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Ld/e/a;->E:I

    sget-object v0, Ld/e/a;->n:Ljava/lang/Class;

    const-string v4, "DEVICE_LEVEL_UNKNOWN"

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v0, v4, v6}, Ld/e/a;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Ld/e/a;->F:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DeviceLevel(): Load Class Exception:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    sget-object v0, Ld/e/a;->w:Landroid/content/Context;

    if-nez v0, :cond_0

    const-string v0, "android.app.ActivityThread"

    :try_start_1
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v4, "currentApplication"

    new-array v6, v5, [Ljava/lang/Class;

    invoke-virtual {v0, v4, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    sput-object v0, Ld/e/a;->v:Landroid/app/Application;

    sget-object v0, Ld/e/a;->v:Landroid/app/Application;

    if-eqz v0, :cond_0

    sget-object v0, Ld/e/a;->v:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Ld/e/a;->w:Landroid/content/Context;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "android.app.ActivityThread Exception:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    sget-object v0, Ld/e/a;->w:Landroid/content/Context;

    if-nez v0, :cond_1

    const-string v0, "android.app.AppGlobals"

    :try_start_2
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v4, "getInitialApplication"

    new-array v6, v5, [Ljava/lang/Class;

    invoke-virtual {v0, v4, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    sput-object v0, Ld/e/a;->v:Landroid/app/Application;

    sget-object v0, Ld/e/a;->v:Landroid/app/Application;

    if-eqz v0, :cond_1

    sget-object v0, Ld/e/a;->v:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Ld/e/a;->w:Landroid/content/Context;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "android.app.AppGlobals Exception:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_2
    :try_start_3
    sget-object v0, Ld/e/a;->p:Ljava/lang/reflect/Constructor;

    if-eqz v0, :cond_2

    sget-object v0, Ld/e/a;->p:Ljava/lang/reflect/Constructor;

    new-array v2, v3, [Ljava/lang/Object;

    sget-object v3, Ld/e/a;->w:Landroid/content/Context;

    aput-object v3, v2, v5

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sput-object v0, Ld/e/a;->q:Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    :catch_3
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeviceLevelUtils(): newInstance Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    :goto_3
    return-void
.end method

.method public static a()I
    .locals 4

    invoke-static {}, Ld/e/a;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    sput v1, Ld/e/a;->d:I

    sget v0, Ld/e/a;->d:I

    return v0

    :cond_0
    sget v0, Ld/e/a;->d:I

    const/4 v2, -0x2

    if-ne v0, v2, :cond_2

    const/4 v0, -0x1

    :try_start_0
    sget-object v2, Ld/e/a;->u:Ljava/lang/reflect/Method;

    sget-object v3, Ld/e/a;->q:Ljava/lang/Object;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMiuiLiteVersion failed , e:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DeviceUtils"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    sput v0, Ld/e/a;->d:I

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    sput v0, Ld/e/a;->d:I

    :goto_1
    sget v0, Ld/e/a;->d:I

    :cond_2
    return v0
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object p0

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static b()Z
    .locals 4

    sget-object v0, Ld/e/a;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const-string v0, "miui.os.Build"

    const/4 v1, 0x0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v2, "IS_MIUI_LITE_VERSION"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Ld/e/a;->c:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "DeviceUtils"

    const-string v3, "isMiuiLiteRom failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sput-object v1, Ld/e/a;->c:Ljava/lang/Boolean;

    :cond_0
    :goto_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget-object v1, Ld/e/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .locals 3

    invoke-static {}, Ld/e/a;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-static {}, Ld/e/a;->a()I

    move-result v0

    const/4 v2, 0x2

    if-lt v0, v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method
