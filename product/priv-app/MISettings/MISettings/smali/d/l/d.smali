.class public final Ld/l/d;
.super Ljava/lang/Object;


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f070000

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f070001

.field public static final abc_action_bar_default_height_material:I = 0x7f070002

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f070003

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f070004

.field public static final abc_action_bar_elevation_material:I = 0x7f070005

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f070006

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f070007

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f070008

.field public static final abc_action_bar_stacked_max_height:I = 0x7f070009

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f07000a

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f07000b

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f07000c

.field public static final abc_action_button_min_height_material:I = 0x7f07000d

.field public static final abc_action_button_min_width_material:I = 0x7f07000e

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f07000f

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f070010

.field public static final abc_alert_dialog_button_dimen:I = 0x7f070011

.field public static final abc_button_inset_horizontal_material:I = 0x7f070012

.field public static final abc_button_inset_vertical_material:I = 0x7f070013

.field public static final abc_button_padding_horizontal_material:I = 0x7f070014

.field public static final abc_button_padding_vertical_material:I = 0x7f070015

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f070016

.field public static final abc_config_prefDialogWidth:I = 0x7f070017

.field public static final abc_control_corner_material:I = 0x7f070018

.field public static final abc_control_inset_material:I = 0x7f070019

.field public static final abc_control_padding_material:I = 0x7f07001a

.field public static final abc_dialog_corner_radius_material:I = 0x7f07001b

.field public static final abc_dialog_fixed_height_major:I = 0x7f07001c

.field public static final abc_dialog_fixed_height_minor:I = 0x7f07001d

.field public static final abc_dialog_fixed_width_major:I = 0x7f07001e

.field public static final abc_dialog_fixed_width_minor:I = 0x7f07001f

.field public static final abc_dialog_list_padding_bottom_no_buttons:I = 0x7f070020

.field public static final abc_dialog_list_padding_top_no_title:I = 0x7f070021

.field public static final abc_dialog_min_width_major:I = 0x7f070022

.field public static final abc_dialog_min_width_minor:I = 0x7f070023

.field public static final abc_dialog_padding_material:I = 0x7f070024

.field public static final abc_dialog_padding_top_material:I = 0x7f070025

.field public static final abc_dialog_title_divider_material:I = 0x7f070026

.field public static final abc_disabled_alpha_material_dark:I = 0x7f070027

.field public static final abc_disabled_alpha_material_light:I = 0x7f070028

.field public static final abc_dropdownitem_icon_width:I = 0x7f070029

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f07002a

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f07002b

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f07002c

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f07002d

.field public static final abc_edit_text_inset_top_material:I = 0x7f07002e

.field public static final abc_floating_window_z:I = 0x7f07002f

.field public static final abc_list_item_height_large_material:I = 0x7f070030

.field public static final abc_list_item_height_material:I = 0x7f070031

.field public static final abc_list_item_height_small_material:I = 0x7f070032

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f070033

.field public static final abc_panel_menu_list_width:I = 0x7f070034

.field public static final abc_progress_bar_height_material:I = 0x7f070035

.field public static final abc_search_view_preferred_height:I = 0x7f070036

.field public static final abc_search_view_preferred_width:I = 0x7f070037

.field public static final abc_seekbar_track_background_height_material:I = 0x7f070038

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f070039

.field public static final abc_select_dialog_padding_start_material:I = 0x7f07003a

.field public static final abc_switch_padding:I = 0x7f07003e

.field public static final abc_text_size_body_1_material:I = 0x7f07003f

.field public static final abc_text_size_body_2_material:I = 0x7f070040

.field public static final abc_text_size_button_material:I = 0x7f070041

.field public static final abc_text_size_caption_material:I = 0x7f070042

.field public static final abc_text_size_display_1_material:I = 0x7f070043

.field public static final abc_text_size_display_2_material:I = 0x7f070044

.field public static final abc_text_size_display_3_material:I = 0x7f070045

.field public static final abc_text_size_display_4_material:I = 0x7f070046

.field public static final abc_text_size_headline_material:I = 0x7f070047

.field public static final abc_text_size_large_material:I = 0x7f070048

.field public static final abc_text_size_medium_material:I = 0x7f070049

.field public static final abc_text_size_menu_header_material:I = 0x7f07004a

.field public static final abc_text_size_menu_material:I = 0x7f07004b

.field public static final abc_text_size_small_material:I = 0x7f07004c

.field public static final abc_text_size_subhead_material:I = 0x7f07004d

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f07004e

.field public static final abc_text_size_title_material:I = 0x7f07004f

.field public static final abc_text_size_title_material_toolbar:I = 0x7f070050

.field public static final compat_button_inset_horizontal_material:I = 0x7f070114

.field public static final compat_button_inset_vertical_material:I = 0x7f070115

.field public static final compat_button_padding_horizontal_material:I = 0x7f070116

.field public static final compat_button_padding_vertical_material:I = 0x7f070117

.field public static final compat_control_corner_material:I = 0x7f070118

.field public static final compat_notification_large_icon_max_height:I = 0x7f070119

.field public static final compat_notification_large_icon_max_width:I = 0x7f07011a

.field public static final disabled_alpha_material_dark:I = 0x7f070150

.field public static final disabled_alpha_material_light:I = 0x7f070151

.field public static final highlight_alpha_material_colored:I = 0x7f0701af

.field public static final highlight_alpha_material_dark:I = 0x7f0701b0

.field public static final highlight_alpha_material_light:I = 0x7f0701b1

.field public static final hint_alpha_material_dark:I = 0x7f0701b2

.field public static final hint_alpha_material_light:I = 0x7f0701b3

.field public static final hint_pressed_alpha_material_dark:I = 0x7f0701b4

.field public static final hint_pressed_alpha_material_light:I = 0x7f0701b5

.field public static final miuix_appcompat_date_picker_text_size:I = 0x7f070243

.field public static final miuix_appcompat_number_picker_bg_corners:I = 0x7f0702eb

.field public static final miuix_appcompat_number_picker_bg_height:I = 0x7f0702ec

.field public static final miuix_appcompat_number_picker_bg_width:I = 0x7f0702ed

.field public static final miuix_appcompat_number_picker_big_bg_width:I = 0x7f0702ee

.field public static final miuix_appcompat_number_picker_highlight_region_height:I = 0x7f0702ef

.field public static final miuix_appcompat_number_picker_label_margin_left:I = 0x7f0702f0

.field public static final miuix_appcompat_number_picker_label_margin_top:I = 0x7f0702f1

.field public static final miuix_appcompat_number_picker_label_padding:I = 0x7f0702f2

.field public static final miuix_appcompat_number_picker_label_text_size:I = 0x7f0702f3

.field public static final miuix_appcompat_number_picker_text_size_highlight_large:I = 0x7f0702f4

.field public static final miuix_appcompat_number_picker_text_size_highlight_medium:I = 0x7f0702f5

.field public static final miuix_appcompat_number_picker_text_size_highlight_normal:I = 0x7f0702f6

.field public static final miuix_appcompat_number_picker_text_size_hint_large:I = 0x7f0702f7

.field public static final miuix_appcompat_number_picker_text_size_hint_medium:I = 0x7f0702f8

.field public static final miuix_appcompat_number_picker_text_size_hint_normal:I = 0x7f0702f9

.field public static final miuix_appcompat_picker_horizontal_padding:I = 0x7f0702fc

.field public static final miuix_appcompat_picker_vertical_padding_bottom:I = 0x7f0702fd

.field public static final miuix_appcompat_picker_vertical_padding_top:I = 0x7f0702fe

.field public static final miuix_label_text_size_small:I = 0x7f07036c

.field public static final miuix_text_size_small:I = 0x7f0703eb

.field public static final notification_action_icon_size:I = 0x7f070404

.field public static final notification_action_text_size:I = 0x7f070405

.field public static final notification_big_circle_margin:I = 0x7f070406

.field public static final notification_content_margin_start:I = 0x7f070408

.field public static final notification_large_icon_height:I = 0x7f070409

.field public static final notification_large_icon_width:I = 0x7f07040a

.field public static final notification_main_column_padding_top:I = 0x7f07040b

.field public static final notification_media_narrow_margin:I = 0x7f07040c

.field public static final notification_right_icon_size:I = 0x7f07040e

.field public static final notification_right_side_padding_top:I = 0x7f07040f

.field public static final notification_small_icon_background_padding:I = 0x7f070410

.field public static final notification_small_icon_size_as_large:I = 0x7f070411

.field public static final notification_subtext_size:I = 0x7f070412

.field public static final notification_top_pad:I = 0x7f070413

.field public static final notification_top_pad_large_text:I = 0x7f070414

.field public static final tooltip_corner_radius:I = 0x7f070473

.field public static final tooltip_horizontal_padding:I = 0x7f070474

.field public static final tooltip_margin:I = 0x7f070475

.field public static final tooltip_precise_anchor_extra_offset:I = 0x7f070476

.field public static final tooltip_precise_anchor_threshold:I = 0x7f070477

.field public static final tooltip_vertical_padding:I = 0x7f070478

.field public static final tooltip_y_offset_non_touch:I = 0x7f070479

.field public static final tooltip_y_offset_touch:I = 0x7f07047a
