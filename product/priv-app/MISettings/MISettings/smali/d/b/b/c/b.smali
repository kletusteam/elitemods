.class public Ld/b/b/c/b;
.super Landroid/view/ActionMode;

# interfaces
.implements Lmiuix/appcompat/internal/view/menu/i$a;
.implements Lmiuix/view/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld/b/b/c/b$a;
    }
.end annotation


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lmiuix/appcompat/internal/app/widget/K;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/view/ActionMode$Callback;

.field private d:Lmiuix/appcompat/internal/view/menu/i;

.field private e:Ld/b/b/c/b$a;

.field f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V
    .locals 1

    invoke-direct {p0}, Landroid/view/ActionMode;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Ld/b/b/c/b;->f:Z

    iput-object p1, p0, Ld/b/b/c/b;->a:Landroid/content/Context;

    iput-object p2, p0, Ld/b/b/c/b;->c:Landroid/view/ActionMode$Callback;

    new-instance p2, Lmiuix/appcompat/internal/view/menu/i;

    invoke-direct {p2, p1}, Lmiuix/appcompat/internal/view/menu/i;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    invoke-virtual {p2, p1}, Lmiuix/appcompat/internal/view/menu/i;->c(I)Lmiuix/appcompat/internal/view/menu/i;

    iput-object p2, p0, Ld/b/b/c/b;->d:Lmiuix/appcompat/internal/view/menu/i;

    iget-object p1, p0, Ld/b/b/c/b;->d:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {p1, p0}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/i$a;)V

    return-void
.end method


# virtual methods
.method public a(Ld/b/b/c/b$a;)V
    .locals 0

    iput-object p1, p0, Ld/b/b/c/b;->e:Ld/b/b/c/b$a;

    return-void
.end method

.method public a(Lmiuix/appcompat/internal/app/widget/K;)V
    .locals 1

    invoke-interface {p1, p0}, Lmiuix/appcompat/internal/app/widget/K;->a(Lmiuix/view/a;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ld/b/b/c/b;->b:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public a(Z)V
    .locals 0

    if-nez p1, :cond_0

    iget-object p1, p0, Ld/b/b/c/b;->c:Landroid/view/ActionMode$Callback;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    const/4 p1, 0x0

    iput-object p1, p0, Ld/b/b/c/b;->c:Landroid/view/ActionMode$Callback;

    :cond_0
    return-void
.end method

.method public a(ZF)V
    .locals 0

    return-void
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .locals 0

    iget-object p1, p0, Ld/b/b/c/b;->c:Landroid/view/ActionMode$Callback;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0, p2}, Landroid/view/ActionMode$Callback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/i;)V
    .locals 0

    iget-object p1, p0, Ld/b/b/c/b;->c:Landroid/view/ActionMode$Callback;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Ld/b/b/c/b;->invalidate()V

    return-void
.end method

.method public b(Z)V
    .locals 0

    return-void
.end method

.method public b()Z
    .locals 2

    iget-object v0, p0, Ld/b/b/c/b;->d:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->q()V

    :try_start_0
    iget-object v0, p0, Ld/b/b/c/b;->c:Landroid/view/ActionMode$Callback;

    iget-object v1, p0, Ld/b/b/c/b;->d:Lmiuix/appcompat/internal/view/menu/i;

    invoke-interface {v0, p0, v1}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Ld/b/b/c/b;->d:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/i;->p()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ld/b/b/c/b;->d:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/i;->p()V

    throw v0
.end method

.method public finish()V
    .locals 1

    iget-boolean v0, p0, Ld/b/b/c/b;->f:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ld/b/b/c/b;->f:Z

    iget-object v0, p0, Ld/b/b/c/b;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/K;

    invoke-interface {v0}, Lmiuix/appcompat/internal/app/widget/K;->b()V

    iget-object v0, p0, Ld/b/b/c/b;->e:Ld/b/b/c/b$a;

    if-eqz v0, :cond_1

    invoke-interface {v0, p0}, Ld/b/b/c/b$a;->a(Landroid/view/ActionMode;)V

    :cond_1
    iget-object v0, p0, Ld/b/b/c/b;->c:Landroid/view/ActionMode$Callback;

    if-eqz v0, :cond_2

    invoke-interface {v0, p0}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    const/4 v0, 0x0

    iput-object v0, p0, Ld/b/b/c/b;->c:Landroid/view/ActionMode$Callback;

    :cond_2
    return-void
.end method

.method public getCustomView()Landroid/view/View;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getCustomView not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getMenu()Landroid/view/Menu;
    .locals 1

    iget-object v0, p0, Ld/b/b/c/b;->d:Lmiuix/appcompat/internal/view/menu/i;

    return-object v0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 2

    new-instance v0, Landroid/view/MenuInflater;

    iget-object v1, p0, Ld/b/b/c/b;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getSubtitle not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getTitle not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public invalidate()V
    .locals 2

    iget-object v0, p0, Ld/b/b/c/b;->d:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->q()V

    :try_start_0
    iget-object v0, p0, Ld/b/b/c/b;->c:Landroid/view/ActionMode$Callback;

    iget-object v1, p0, Ld/b/b/c/b;->d:Lmiuix/appcompat/internal/view/menu/i;

    invoke-interface {v0, p0, v1}, Landroid/view/ActionMode$Callback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Ld/b/b/c/b;->d:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->p()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ld/b/b/c/b;->d:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/i;->p()V

    throw v0
.end method

.method public setCustomView(Landroid/view/View;)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "setCustomView not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setSubtitle(I)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "setSubTitle not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "setSubTitle not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setTitle(I)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "setTitle not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "setTitle not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
