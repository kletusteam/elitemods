.class public final Lkotlin/f/m;
.super Ljava/lang/Object;

# interfaces
.implements Lkotlin/f/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/f/d<",
        "TR;>;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/f/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/f/d<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/jvm/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/a/b<",
            "TT;TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/f/d;Lkotlin/jvm/a/b;)V
    .locals 1
    .param p1    # Lkotlin/f/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/jvm/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/f/d<",
            "+TT;>;",
            "Lkotlin/jvm/a/b<",
            "-TT;+TR;>;)V"
        }
    .end annotation

    const-string v0, "sequence"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transformer"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/f/m;->a:Lkotlin/f/d;

    iput-object p2, p0, Lkotlin/f/m;->b:Lkotlin/jvm/a/b;

    return-void
.end method

.method public static final synthetic a(Lkotlin/f/m;)Lkotlin/f/d;
    .locals 0

    iget-object p0, p0, Lkotlin/f/m;->a:Lkotlin/f/d;

    return-object p0
.end method

.method public static final synthetic b(Lkotlin/f/m;)Lkotlin/jvm/a/b;
    .locals 0

    iget-object p0, p0, Lkotlin/f/m;->b:Lkotlin/jvm/a/b;

    return-object p0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TR;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lkotlin/f/l;

    invoke-direct {v0, p0}, Lkotlin/f/l;-><init>(Lkotlin/f/m;)V

    return-object v0
.end method
