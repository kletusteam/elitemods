.class public final Lkotlin/f/b;
.super Ljava/lang/Object;

# interfaces
.implements Lkotlin/f/d;
.implements Lkotlin/f/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/f/d<",
        "TT;>;",
        "Lkotlin/f/c<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/f/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/f/d<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:I


# direct methods
.method public constructor <init>(Lkotlin/f/d;I)V
    .locals 1
    .param p1    # Lkotlin/f/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/f/d<",
            "+TT;>;I)V"
        }
    .end annotation

    const-string v0, "sequence"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/f/b;->a:Lkotlin/f/d;

    iput p2, p0, Lkotlin/f/b;->b:I

    iget p1, p0, Lkotlin/f/b;->b:I

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    return-void

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "count must be non-negative, but was "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lkotlin/f/b;->b:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p2, 0x2e

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public static final synthetic a(Lkotlin/f/b;)I
    .locals 0

    iget p0, p0, Lkotlin/f/b;->b:I

    return p0
.end method

.method public static final synthetic b(Lkotlin/f/b;)Lkotlin/f/d;
    .locals 0

    iget-object p0, p0, Lkotlin/f/b;->a:Lkotlin/f/d;

    return-object p0
.end method


# virtual methods
.method public a(I)Lkotlin/f/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lkotlin/f/d<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget v0, p0, Lkotlin/f/b;->b:I

    add-int/2addr v0, p1

    if-gez v0, :cond_0

    new-instance v0, Lkotlin/f/b;

    invoke-direct {v0, p0, p1}, Lkotlin/f/b;-><init>(Lkotlin/f/d;I)V

    move-object p1, v0

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/f/b;

    iget-object v1, p0, Lkotlin/f/b;->a:Lkotlin/f/d;

    invoke-direct {p1, v1, v0}, Lkotlin/f/b;-><init>(Lkotlin/f/d;I)V

    :goto_0
    return-object p1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lkotlin/f/a;

    invoke-direct {v0, p0}, Lkotlin/f/a;-><init>(Lkotlin/f/b;)V

    return-object v0
.end method
