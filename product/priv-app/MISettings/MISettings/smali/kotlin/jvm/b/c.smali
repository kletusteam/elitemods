.class public abstract Lkotlin/jvm/b/c;
.super Ljava/lang/Object;

# interfaces
.implements Lkotlin/e/b;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/jvm/b/c$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Object;
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation
.end field


# instance fields
.field private transient b:Lkotlin/e/b;

.field protected final c:Ljava/lang/Object;
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lkotlin/jvm/b/c$a;->a()Lkotlin/jvm/b/c$a;

    move-result-object v0

    sput-object v0, Lkotlin/jvm/b/c;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lkotlin/jvm/b/c;->a:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lkotlin/jvm/b/c;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method protected constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/jvm/b/c;->c:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public b()Lkotlin/e/b;
    .locals 1
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    iget-object v0, p0, Lkotlin/jvm/b/c;->b:Lkotlin/e/b;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lkotlin/jvm/b/c;->c()Lkotlin/e/b;

    iput-object p0, p0, Lkotlin/jvm/b/c;->b:Lkotlin/e/b;

    move-object v0, p0

    :cond_0
    return-object v0
.end method

.method protected abstract c()Lkotlin/e/b;
.end method

.method public d()Ljava/lang/Object;
    .locals 1
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    iget-object v0, p0, Lkotlin/jvm/b/c;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/lang/AbstractMethodError;

    invoke-direct {v0}, Ljava/lang/AbstractMethodError;-><init>()V

    throw v0
.end method

.method public f()Lkotlin/e/e;
    .locals 1

    new-instance v0, Ljava/lang/AbstractMethodError;

    invoke-direct {v0}, Ljava/lang/AbstractMethodError;-><init>()V

    throw v0
.end method

.method protected g()Lkotlin/e/b;
    .locals 1
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    invoke-virtual {p0}, Lkotlin/jvm/b/c;->b()Lkotlin/e/b;

    move-result-object v0

    if-eq v0, p0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Lkotlin/jvm/b;

    invoke-direct {v0}, Lkotlin/jvm/b;-><init>()V

    throw v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/lang/AbstractMethodError;

    invoke-direct {v0}, Ljava/lang/AbstractMethodError;-><init>()V

    throw v0
.end method
