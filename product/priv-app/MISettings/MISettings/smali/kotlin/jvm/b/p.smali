.class public Lkotlin/jvm/b/p;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lkotlin/jvm/b/q;

.field private static final b:[Lkotlin/e/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "kotlin.reflect.jvm.internal.ReflectionFactoryImpl"

    const/4 v1, 0x0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/jvm/b/q;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    :catch_0
    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lkotlin/jvm/b/q;

    invoke-direct {v1}, Lkotlin/jvm/b/q;-><init>()V

    :goto_0
    sput-object v1, Lkotlin/jvm/b/p;->a:Lkotlin/jvm/b/q;

    const/4 v0, 0x0

    new-array v0, v0, [Lkotlin/e/c;

    sput-object v0, Lkotlin/jvm/b/p;->b:[Lkotlin/e/c;

    return-void
.end method

.method public static a(Lkotlin/jvm/b/j;)Ljava/lang/String;
    .locals 1
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    sget-object v0, Lkotlin/jvm/b/p;->a:Lkotlin/jvm/b/q;

    invoke-virtual {v0, p0}, Lkotlin/jvm/b/q;->a(Lkotlin/jvm/b/j;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Class;)Lkotlin/e/c;
    .locals 1

    sget-object v0, Lkotlin/jvm/b/p;->a:Lkotlin/jvm/b/q;

    invoke-virtual {v0, p0}, Lkotlin/jvm/b/q;->a(Ljava/lang/Class;)Lkotlin/e/c;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lkotlin/jvm/b/k;)Lkotlin/e/h;
    .locals 1

    sget-object v0, Lkotlin/jvm/b/p;->a:Lkotlin/jvm/b/q;

    invoke-virtual {v0, p0}, Lkotlin/jvm/b/q;->a(Lkotlin/jvm/b/k;)Lkotlin/e/h;

    return-object p0
.end method
