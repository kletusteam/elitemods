.class final Lkotlin/coroutines/f;
.super Lkotlin/jvm/b/j;

# interfaces
.implements Lkotlin/jvm/a/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/coroutines/e$a;->a(Lkotlin/coroutines/e;Lkotlin/coroutines/e;)Lkotlin/coroutines/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/b/j;",
        "Lkotlin/jvm/a/c<",
        "Lkotlin/coroutines/e;",
        "Lkotlin/coroutines/e$b;",
        "Lkotlin/coroutines/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Lkotlin/coroutines/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlin/coroutines/f;

    invoke-direct {v0}, Lkotlin/coroutines/f;-><init>()V

    sput-object v0, Lkotlin/coroutines/f;->b:Lkotlin/coroutines/f;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/jvm/b/j;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlin/coroutines/e;

    check-cast p2, Lkotlin/coroutines/e$b;

    invoke-virtual {p0, p1, p2}, Lkotlin/coroutines/f;->a(Lkotlin/coroutines/e;Lkotlin/coroutines/e$b;)Lkotlin/coroutines/e;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lkotlin/coroutines/e;Lkotlin/coroutines/e$b;)Lkotlin/coroutines/e;
    .locals 3
    .param p1    # Lkotlin/coroutines/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/coroutines/e$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "acc"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "element"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2}, Lkotlin/coroutines/e$b;->getKey()Lkotlin/coroutines/e$c;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/coroutines/e;->minusKey(Lkotlin/coroutines/e$c;)Lkotlin/coroutines/e;

    move-result-object p1

    sget-object v0, Lkotlin/coroutines/g;->a:Lkotlin/coroutines/g;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lkotlin/coroutines/d;->c:Lkotlin/coroutines/d$b;

    invoke-interface {p1, v0}, Lkotlin/coroutines/e;->get(Lkotlin/coroutines/e$c;)Lkotlin/coroutines/e$b;

    move-result-object v0

    check-cast v0, Lkotlin/coroutines/d;

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/coroutines/c;

    invoke-direct {v0, p1, p2}, Lkotlin/coroutines/c;-><init>(Lkotlin/coroutines/e;Lkotlin/coroutines/e$b;)V

    move-object p2, v0

    goto :goto_0

    :cond_1
    sget-object v1, Lkotlin/coroutines/d;->c:Lkotlin/coroutines/d$b;

    invoke-interface {p1, v1}, Lkotlin/coroutines/e;->minusKey(Lkotlin/coroutines/e$c;)Lkotlin/coroutines/e;

    move-result-object p1

    sget-object v1, Lkotlin/coroutines/g;->a:Lkotlin/coroutines/g;

    if-ne p1, v1, :cond_2

    new-instance p1, Lkotlin/coroutines/c;

    invoke-direct {p1, p2, v0}, Lkotlin/coroutines/c;-><init>(Lkotlin/coroutines/e;Lkotlin/coroutines/e$b;)V

    move-object p2, p1

    goto :goto_0

    :cond_2
    new-instance v1, Lkotlin/coroutines/c;

    new-instance v2, Lkotlin/coroutines/c;

    invoke-direct {v2, p1, p2}, Lkotlin/coroutines/c;-><init>(Lkotlin/coroutines/e;Lkotlin/coroutines/e$b;)V

    invoke-direct {v1, v2, v0}, Lkotlin/coroutines/c;-><init>(Lkotlin/coroutines/e;Lkotlin/coroutines/e$b;)V

    move-object p2, v1

    :goto_0
    return-object p2
.end method
