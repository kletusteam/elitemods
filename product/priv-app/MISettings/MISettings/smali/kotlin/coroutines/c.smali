.class public final Lkotlin/coroutines/c;
.super Ljava/lang/Object;

# interfaces
.implements Lkotlin/coroutines/e;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lkotlin/SinceKotlin;
    version = "1.3"
.end annotation


# instance fields
.field private final a:Lkotlin/coroutines/e;

.field private final b:Lkotlin/coroutines/e$b;


# direct methods
.method public constructor <init>(Lkotlin/coroutines/e;Lkotlin/coroutines/e$b;)V
    .locals 1
    .param p1    # Lkotlin/coroutines/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/coroutines/e$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "left"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "element"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/coroutines/c;->a:Lkotlin/coroutines/e;

    iput-object p2, p0, Lkotlin/coroutines/c;->b:Lkotlin/coroutines/e$b;

    return-void
.end method

.method private final a()I
    .locals 3

    const/4 v0, 0x2

    move v1, v0

    move-object v0, p0

    :goto_0
    iget-object v0, v0, Lkotlin/coroutines/c;->a:Lkotlin/coroutines/e;

    instance-of v2, v0, Lkotlin/coroutines/c;

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlin/coroutines/c;

    if-eqz v0, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private final a(Lkotlin/coroutines/c;)Z
    .locals 1

    :goto_0
    iget-object v0, p1, Lkotlin/coroutines/c;->b:Lkotlin/coroutines/e$b;

    invoke-direct {p0, v0}, Lkotlin/coroutines/c;->a(Lkotlin/coroutines/e$b;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object p1, p1, Lkotlin/coroutines/c;->a:Lkotlin/coroutines/e;

    instance-of v0, p1, Lkotlin/coroutines/c;

    if-eqz v0, :cond_1

    check-cast p1, Lkotlin/coroutines/c;

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    check-cast p1, Lkotlin/coroutines/e$b;

    invoke-direct {p0, p1}, Lkotlin/coroutines/c;->a(Lkotlin/coroutines/e$b;)Z

    move-result p1

    return p1

    :cond_2
    new-instance p1, Lkotlin/o;

    const-string v0, "null cannot be cast to non-null type kotlin.coroutines.CoroutineContext.Element"

    invoke-direct {p1, v0}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final a(Lkotlin/coroutines/e$b;)Z
    .locals 1

    invoke-interface {p1}, Lkotlin/coroutines/e$b;->getKey()Lkotlin/coroutines/e$c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/coroutines/c;->get(Lkotlin/coroutines/e$c;)Lkotlin/coroutines/e$b;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/jvm/b/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lkotlin/coroutines/c;

    if-eqz v0, :cond_0

    check-cast p1, Lkotlin/coroutines/c;

    invoke-direct {p1}, Lkotlin/coroutines/c;->a()I

    move-result v0

    invoke-direct {p0}, Lkotlin/coroutines/c;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-direct {p1, p0}, Lkotlin/coroutines/c;->a(Lkotlin/coroutines/c;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public fold(Ljava/lang/Object;Lkotlin/jvm/a/c;)Ljava/lang/Object;
    .locals 1
    .param p2    # Lkotlin/jvm/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Lkotlin/jvm/a/c<",
            "-TR;-",
            "Lkotlin/coroutines/e$b;",
            "+TR;>;)TR;"
        }
    .end annotation

    const-string v0, "operation"

    invoke-static {p2, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/coroutines/c;->a:Lkotlin/coroutines/e;

    invoke-interface {v0, p1, p2}, Lkotlin/coroutines/e;->fold(Ljava/lang/Object;Lkotlin/jvm/a/c;)Ljava/lang/Object;

    move-result-object p1

    iget-object v0, p0, Lkotlin/coroutines/c;->b:Lkotlin/coroutines/e$b;

    invoke-interface {p2, p1, v0}, Lkotlin/jvm/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public get(Lkotlin/coroutines/e$c;)Lkotlin/coroutines/e$b;
    .locals 2
    .param p1    # Lkotlin/coroutines/e$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lkotlin/coroutines/e$b;",
            ">(",
            "Lkotlin/coroutines/e$c<",
            "TE;>;)TE;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    :goto_0
    iget-object v1, v0, Lkotlin/coroutines/c;->b:Lkotlin/coroutines/e$b;

    invoke-interface {v1, p1}, Lkotlin/coroutines/e$b;->get(Lkotlin/coroutines/e$c;)Lkotlin/coroutines/e$b;

    move-result-object v1

    if-eqz v1, :cond_0

    return-object v1

    :cond_0
    iget-object v0, v0, Lkotlin/coroutines/c;->a:Lkotlin/coroutines/e;

    instance-of v1, v0, Lkotlin/coroutines/c;

    if-eqz v1, :cond_1

    check-cast v0, Lkotlin/coroutines/c;

    goto :goto_0

    :cond_1
    invoke-interface {v0, p1}, Lkotlin/coroutines/e;->get(Lkotlin/coroutines/e$c;)Lkotlin/coroutines/e$b;

    move-result-object p1

    return-object p1
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lkotlin/coroutines/c;->a:Lkotlin/coroutines/e;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lkotlin/coroutines/c;->b:Lkotlin/coroutines/e$b;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public minusKey(Lkotlin/coroutines/e$c;)Lkotlin/coroutines/e;
    .locals 2
    .param p1    # Lkotlin/coroutines/e$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/e$c<",
            "*>;)",
            "Lkotlin/coroutines/e;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/coroutines/c;->b:Lkotlin/coroutines/e$b;

    invoke-interface {v0, p1}, Lkotlin/coroutines/e$b;->get(Lkotlin/coroutines/e$c;)Lkotlin/coroutines/e$b;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lkotlin/coroutines/c;->a:Lkotlin/coroutines/e;

    return-object p1

    :cond_0
    iget-object v0, p0, Lkotlin/coroutines/c;->a:Lkotlin/coroutines/e;

    invoke-interface {v0, p1}, Lkotlin/coroutines/e;->minusKey(Lkotlin/coroutines/e$c;)Lkotlin/coroutines/e;

    move-result-object p1

    iget-object v0, p0, Lkotlin/coroutines/c;->a:Lkotlin/coroutines/e;

    if-ne p1, v0, :cond_1

    move-object p1, p0

    goto :goto_0

    :cond_1
    sget-object v0, Lkotlin/coroutines/g;->a:Lkotlin/coroutines/g;

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lkotlin/coroutines/c;->b:Lkotlin/coroutines/e$b;

    goto :goto_0

    :cond_2
    new-instance v0, Lkotlin/coroutines/c;

    iget-object v1, p0, Lkotlin/coroutines/c;->b:Lkotlin/coroutines/e$b;

    invoke-direct {v0, p1, v1}, Lkotlin/coroutines/c;-><init>(Lkotlin/coroutines/e;Lkotlin/coroutines/e$b;)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lkotlin/coroutines/b;->b:Lkotlin/coroutines/b;

    const-string v2, ""

    invoke-virtual {p0, v2, v1}, Lkotlin/coroutines/c;->fold(Ljava/lang/Object;Lkotlin/jvm/a/c;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
