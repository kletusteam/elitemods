.class public final Lkotlin/g/d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;
.implements Lkotlin/jvm/b/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/g/e;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "Lkotlin/d/d;",
        ">;",
        "Lkotlin/jvm/b/a/a;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lkotlin/d/d;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private e:I

.field final synthetic f:Lkotlin/g/e;


# direct methods
.method constructor <init>(Lkotlin/g/e;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lkotlin/g/d;->f:Lkotlin/g/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lkotlin/g/d;->a:I

    invoke-static {p1}, Lkotlin/g/e;->d(Lkotlin/g/e;)I

    move-result v0

    invoke-static {p1}, Lkotlin/g/e;->b(Lkotlin/g/e;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lkotlin/d/e;->a(III)I

    move-result p1

    iput p1, p0, Lkotlin/g/d;->b:I

    iget p1, p0, Lkotlin/g/d;->b:I

    iput p1, p0, Lkotlin/g/d;->c:I

    return-void
.end method

.method private final a()V
    .locals 6

    iget v0, p0, Lkotlin/g/d;->c:I

    const/4 v1, 0x0

    if-gez v0, :cond_0

    iput v1, p0, Lkotlin/g/d;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Lkotlin/g/d;->d:Lkotlin/d/d;

    goto/16 :goto_1

    :cond_0
    iget-object v0, p0, Lkotlin/g/d;->f:Lkotlin/g/e;

    invoke-static {v0}, Lkotlin/g/e;->c(Lkotlin/g/e;)I

    move-result v0

    const/4 v2, -0x1

    const/4 v3, 0x1

    if-lez v0, :cond_1

    iget v0, p0, Lkotlin/g/d;->e:I

    add-int/2addr v0, v3

    iput v0, p0, Lkotlin/g/d;->e:I

    iget v0, p0, Lkotlin/g/d;->e:I

    iget-object v4, p0, Lkotlin/g/d;->f:Lkotlin/g/e;

    invoke-static {v4}, Lkotlin/g/e;->c(Lkotlin/g/e;)I

    move-result v4

    if-ge v0, v4, :cond_2

    :cond_1
    iget v0, p0, Lkotlin/g/d;->c:I

    iget-object v4, p0, Lkotlin/g/d;->f:Lkotlin/g/e;

    invoke-static {v4}, Lkotlin/g/e;->b(Lkotlin/g/e;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-le v0, v4, :cond_3

    :cond_2
    iget v0, p0, Lkotlin/g/d;->b:I

    new-instance v1, Lkotlin/d/d;

    iget-object v4, p0, Lkotlin/g/d;->f:Lkotlin/g/e;

    invoke-static {v4}, Lkotlin/g/e;->b(Lkotlin/g/e;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Lkotlin/g/v;->c(Ljava/lang/CharSequence;)I

    move-result v4

    invoke-direct {v1, v0, v4}, Lkotlin/d/d;-><init>(II)V

    iput-object v1, p0, Lkotlin/g/d;->d:Lkotlin/d/d;

    iput v2, p0, Lkotlin/g/d;->c:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lkotlin/g/d;->f:Lkotlin/g/e;

    invoke-static {v0}, Lkotlin/g/e;->a(Lkotlin/g/e;)Lkotlin/jvm/a/c;

    move-result-object v0

    iget-object v4, p0, Lkotlin/g/d;->f:Lkotlin/g/e;

    invoke-static {v4}, Lkotlin/g/e;->b(Lkotlin/g/e;)Ljava/lang/CharSequence;

    move-result-object v4

    iget v5, p0, Lkotlin/g/d;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Lkotlin/jvm/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/j;

    if-nez v0, :cond_4

    iget v0, p0, Lkotlin/g/d;->b:I

    new-instance v1, Lkotlin/d/d;

    iget-object v4, p0, Lkotlin/g/d;->f:Lkotlin/g/e;

    invoke-static {v4}, Lkotlin/g/e;->b(Lkotlin/g/e;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Lkotlin/g/v;->c(Ljava/lang/CharSequence;)I

    move-result v4

    invoke-direct {v1, v0, v4}, Lkotlin/d/d;-><init>(II)V

    iput-object v1, p0, Lkotlin/g/d;->d:Lkotlin/d/d;

    iput v2, p0, Lkotlin/g/d;->c:I

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lkotlin/j;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-virtual {v0}, Lkotlin/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    iget v4, p0, Lkotlin/g/d;->b:I

    invoke-static {v4, v2}, Lkotlin/d/e;->d(II)Lkotlin/d/d;

    move-result-object v4

    iput-object v4, p0, Lkotlin/g/d;->d:Lkotlin/d/d;

    add-int/2addr v2, v0

    iput v2, p0, Lkotlin/g/d;->b:I

    iget v2, p0, Lkotlin/g/d;->b:I

    if-nez v0, :cond_5

    move v1, v3

    :cond_5
    add-int/2addr v2, v1

    iput v2, p0, Lkotlin/g/d;->c:I

    :goto_0
    iput v3, p0, Lkotlin/g/d;->a:I

    :goto_1
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    iget v0, p0, Lkotlin/g/d;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lkotlin/g/d;->a()V

    :cond_0
    iget v0, p0, Lkotlin/g/d;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lkotlin/g/d;->next()Lkotlin/d/d;

    move-result-object v0

    return-object v0
.end method

.method public next()Lkotlin/d/d;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget v0, p0, Lkotlin/g/d;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lkotlin/g/d;->a()V

    :cond_0
    iget v0, p0, Lkotlin/g/d;->a:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkotlin/g/d;->d:Lkotlin/d/d;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    iput-object v2, p0, Lkotlin/g/d;->d:Lkotlin/d/d;

    iput v1, p0, Lkotlin/g/d;->a:I

    return-object v0

    :cond_1
    new-instance v0, Lkotlin/o;

    const-string v1, "null cannot be cast to non-null type kotlin.ranges.IntRange"

    invoke-direct {v0, v1}, Lkotlin/o;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Operation is not supported for read-only collection"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
