.class final Lkotlin/g/e;
.super Ljava/lang/Object;

# interfaces
.implements Lkotlin/f/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/f/d<",
        "Lkotlin/d/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/CharSequence;

.field private final b:I

.field private final c:I

.field private final d:Lkotlin/jvm/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/a/c<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Integer;",
            "Lkotlin/j<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;IILkotlin/jvm/a/c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lkotlin/jvm/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "II",
            "Lkotlin/jvm/a/c<",
            "-",
            "Ljava/lang/CharSequence;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/j<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getNextMatch"

    invoke-static {p4, v0}, Lkotlin/jvm/b/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/g/e;->a:Ljava/lang/CharSequence;

    iput p2, p0, Lkotlin/g/e;->b:I

    iput p3, p0, Lkotlin/g/e;->c:I

    iput-object p4, p0, Lkotlin/g/e;->d:Lkotlin/jvm/a/c;

    return-void
.end method

.method public static final synthetic a(Lkotlin/g/e;)Lkotlin/jvm/a/c;
    .locals 0

    iget-object p0, p0, Lkotlin/g/e;->d:Lkotlin/jvm/a/c;

    return-object p0
.end method

.method public static final synthetic b(Lkotlin/g/e;)Ljava/lang/CharSequence;
    .locals 0

    iget-object p0, p0, Lkotlin/g/e;->a:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public static final synthetic c(Lkotlin/g/e;)I
    .locals 0

    iget p0, p0, Lkotlin/g/e;->c:I

    return p0
.end method

.method public static final synthetic d(Lkotlin/g/e;)I
    .locals 0

    iget p0, p0, Lkotlin/g/e;->b:I

    return p0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lkotlin/d/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lkotlin/g/d;

    invoke-direct {v0, p0}, Lkotlin/g/d;-><init>(Lkotlin/g/e;)V

    return-object v0
.end method
