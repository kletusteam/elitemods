.class final Lc/a/a/f/g/l$a;
.super Lc/a/a/b/h$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc/a/a/f/g/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field final a:Ljava/util/concurrent/ScheduledExecutorService;

.field final b:Lc/a/a/c/a;

.field volatile c:Z


# direct methods
.method constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0

    invoke-direct {p0}, Lc/a/a/b/h$b;-><init>()V

    iput-object p1, p0, Lc/a/a/f/g/l$a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance p1, Lc/a/a/c/a;

    invoke-direct {p1}, Lc/a/a/c/a;-><init>()V

    iput-object p1, p0, Lc/a/a/f/g/l$a;->b:Lc/a/a/c/a;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lc/a/a/c/b;
    .locals 3
    .param p1    # Ljava/lang/Runnable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/TimeUnit;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    iget-boolean v0, p0, Lc/a/a/f/g/l$a;->c:Z

    if-eqz v0, :cond_0

    sget-object p1, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    return-object p1

    :cond_0
    invoke-static {p1}, Lc/a/a/h/a;->a(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object p1

    new-instance v0, Lc/a/a/f/g/i;

    iget-object v1, p0, Lc/a/a/f/g/l$a;->b:Lc/a/a/c/a;

    invoke-direct {v0, p1, v1}, Lc/a/a/f/g/i;-><init>(Ljava/lang/Runnable;Lc/a/a/c/c;)V

    iget-object p1, p0, Lc/a/a/f/g/l$a;->b:Lc/a/a/c/a;

    invoke-virtual {p1, v0}, Lc/a/a/c/a;->b(Lc/a/a/c/b;)Z

    const-wide/16 v1, 0x0

    cmp-long p1, p2, v1

    if-gtz p1, :cond_1

    :try_start_0
    iget-object p1, p0, Lc/a/a/f/g/l$a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {p1, v0}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lc/a/a/f/g/l$a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {p1, v0, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Lc/a/a/f/g/i;->a(Ljava/util/concurrent/Future;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    invoke-virtual {p0}, Lc/a/a/f/g/l$a;->b()V

    invoke-static {p1}, Lc/a/a/h/a;->b(Ljava/lang/Throwable;)V

    sget-object p1, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    return-object p1
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lc/a/a/f/g/l$a;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lc/a/a/f/g/l$a;->c:Z

    iget-object v0, p0, Lc/a/a/f/g/l$a;->b:Lc/a/a/c/a;

    invoke-virtual {v0}, Lc/a/a/c/a;->b()V

    :cond_0
    return-void
.end method
