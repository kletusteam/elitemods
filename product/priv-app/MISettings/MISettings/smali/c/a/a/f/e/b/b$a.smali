.class final Lc/a/a/f/e/b/b$a;
.super Ljava/util/concurrent/atomic/AtomicReference;

# interfaces
.implements Lc/a/a/b/j;
.implements Lc/a/a/c/b;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc/a/a/f/e/b/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference<",
        "Lc/a/a/c/b;",
        ">;",
        "Lc/a/a/b/j<",
        "TT;>;",
        "Lc/a/a/c/b;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field final a:Lc/a/a/b/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/b/j<",
            "-TT;>;"
        }
    .end annotation
.end field

.field final b:Lc/a/a/b/h;

.field c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field d:Ljava/lang/Throwable;


# direct methods
.method constructor <init>(Lc/a/a/b/j;Lc/a/a/b/h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/b/j<",
            "-TT;>;",
            "Lc/a/a/b/h;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object p1, p0, Lc/a/a/f/e/b/b$a;->a:Lc/a/a/b/j;

    iput-object p2, p0, Lc/a/a/f/e/b/b$a;->b:Lc/a/a/b/h;

    return-void
.end method


# virtual methods
.method public a(Lc/a/a/c/b;)V
    .locals 0

    invoke-static {p0, p1}, Lc/a/a/f/a/a;->b(Ljava/util/concurrent/atomic/AtomicReference;Lc/a/a/c/b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lc/a/a/f/e/b/b$a;->a:Lc/a/a/b/j;

    invoke-interface {p1, p0}, Lc/a/a/b/j;->a(Lc/a/a/c/b;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iput-object p1, p0, Lc/a/a/f/e/b/b$a;->c:Ljava/lang/Object;

    iget-object p1, p0, Lc/a/a/f/e/b/b$a;->b:Lc/a/a/b/h;

    invoke-virtual {p1, p0}, Lc/a/a/b/h;->a(Ljava/lang/Runnable;)Lc/a/a/c/b;

    move-result-object p1

    invoke-static {p0, p1}, Lc/a/a/f/a/a;->a(Ljava/util/concurrent/atomic/AtomicReference;Lc/a/a/c/b;)Z

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 0

    iput-object p1, p0, Lc/a/a/f/e/b/b$a;->d:Ljava/lang/Throwable;

    iget-object p1, p0, Lc/a/a/f/e/b/b$a;->b:Lc/a/a/b/h;

    invoke-virtual {p1, p0}, Lc/a/a/b/h;->a(Ljava/lang/Runnable;)Lc/a/a/c/b;

    move-result-object p1

    invoke-static {p0, p1}, Lc/a/a/f/a/a;->a(Ljava/util/concurrent/atomic/AtomicReference;Lc/a/a/c/b;)Z

    return-void
.end method

.method public b()V
    .locals 0

    invoke-static {p0}, Lc/a/a/f/a/a;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    return-void
.end method

.method public run()V
    .locals 2

    iget-object v0, p0, Lc/a/a/f/e/b/b$a;->d:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lc/a/a/f/e/b/b$a;->a:Lc/a/a/b/j;

    invoke-interface {v1, v0}, Lc/a/a/b/j;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lc/a/a/f/e/b/b$a;->a:Lc/a/a/b/j;

    iget-object v1, p0, Lc/a/a/f/e/b/b$a;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lc/a/a/b/j;->a(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
