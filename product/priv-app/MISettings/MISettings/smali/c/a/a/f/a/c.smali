.class public final Lc/a/a/f/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Lc/a/a/c/b;
.implements Lc/a/a/c/c;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lc/a/a/c/b;",
            ">;"
        }
    .end annotation
.end field

.field volatile b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lc/a/a/c/b;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_1a

    nop

    :goto_0
    throw p1

    :goto_1
    goto/32 :goto_c

    nop

    :goto_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_12

    nop

    :goto_5
    invoke-direct {p1, v0}, Lc/a/a/d/a;-><init>(Ljava/lang/Iterable;)V

    goto/32 :goto_14

    nop

    :goto_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_19

    nop

    :goto_7
    return-void

    :goto_8
    goto :goto_3

    :catch_0
    move-exception v1

    goto/32 :goto_9

    nop

    :goto_9
    invoke-static {v1}, Lc/a/a/d/b;->b(Ljava/lang/Throwable;)V

    goto/32 :goto_1c

    nop

    :goto_a
    goto :goto_3

    :goto_b
    goto/32 :goto_11

    nop

    :goto_c
    new-instance p1, Lc/a/a/d/a;

    goto/32 :goto_5

    nop

    :goto_d
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_e

    nop

    :goto_e
    check-cast p1, Ljava/lang/Throwable;

    goto/32 :goto_17

    nop

    :goto_f
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_a

    nop

    :goto_10
    const/4 p1, 0x0

    goto/32 :goto_d

    nop

    :goto_11
    if-nez v0, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_1e

    nop

    :goto_12
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_13
    goto/32 :goto_f

    nop

    :goto_14
    throw p1

    :goto_15
    goto/32 :goto_7

    nop

    :goto_16
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_17
    invoke-static {p1}, Lio/reactivex/rxjava3/internal/util/a;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_18
    if-eq p1, v1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_10

    nop

    :goto_19
    if-nez v1, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_16

    nop

    :goto_1a
    if-eqz p1, :cond_3

    goto/32 :goto_21

    :cond_3
    goto/32 :goto_20

    nop

    :goto_1b
    check-cast v1, Lc/a/a/c/b;

    :try_start_0
    invoke-interface {v1}, Lc/a/a/c/b;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_8

    nop

    :goto_1c
    if-eqz v0, :cond_4

    goto/32 :goto_13

    :cond_4
    goto/32 :goto_4

    nop

    :goto_1d
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_1e
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    goto/32 :goto_1f

    nop

    :goto_1f
    const/4 v1, 0x1

    goto/32 :goto_18

    nop

    :goto_20
    return-void

    :goto_21
    goto/32 :goto_1d

    nop
.end method

.method public a(Lc/a/a/c/b;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lc/a/a/f/a/c;->c(Lc/a/a/c/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lc/a/a/c/b;->b()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public b()V
    .locals 2

    iget-boolean v0, p0, Lc/a/a/f/a/c;->b:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lc/a/a/f/a/c;->b:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lc/a/a/f/a/c;->b:Z

    iget-object v0, p0, Lc/a/a/f/a/c;->a:Ljava/util/List;

    const/4 v1, 0x0

    iput-object v1, p0, Lc/a/a/f/a/c;->a:Ljava/util/List;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v0}, Lc/a/a/f/a/c;->a(Ljava/util/List;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lc/a/a/c/b;)Z
    .locals 1

    const-string v0, "d is null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-boolean v0, p0, Lc/a/a/f/a/c;->b:Z

    if-nez v0, :cond_2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lc/a/a/f/a/c;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lc/a/a/f/a/c;->a:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lc/a/a/f/a/c;->a:Ljava/util/List;

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    monitor-exit p0

    return p1

    :cond_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Lc/a/a/c/b;->b()V

    const/4 p1, 0x0

    return p1
.end method

.method public c(Lc/a/a/c/b;)Z
    .locals 2

    const-string v0, "Disposable item is null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-boolean v0, p0, Lc/a/a/f/a/c;->b:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lc/a/a/f/a/c;->b:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    return v1

    :cond_1
    iget-object v0, p0, Lc/a/a/f/a/c;->a:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    monitor-exit p0

    const/4 p1, 0x1

    return p1

    :cond_3
    :goto_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
