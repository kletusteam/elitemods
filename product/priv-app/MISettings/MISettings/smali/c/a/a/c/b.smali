.class public interface abstract Lc/a/a/c/b;
.super Ljava/lang/Object;


# direct methods
.method public static a()Lc/a/a/c/b;
    .locals 1
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    sget-object v0, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    return-object v0
.end method

.method public static a(Ljava/lang/Runnable;)Lc/a/a/c/b;
    .locals 1
    .param p0    # Ljava/lang/Runnable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    const-string v0, "run is null"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance v0, Lc/a/a/c/e;

    invoke-direct {v0, p0}, Lc/a/a/c/e;-><init>(Ljava/lang/Runnable;)V

    return-object v0
.end method


# virtual methods
.method public abstract b()V
.end method
