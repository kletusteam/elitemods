.class public abstract Lc/a/a/b/c;
.super Ljava/lang/Object;

# interfaces
.implements Lc/a/a/b/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lc/a/a/b/f<",
        "TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()I
    .locals 1
    .annotation runtime Lio/reactivex/rxjava3/annotations/CheckReturnValue;
    .end annotation

    invoke-static {}, Lc/a/a/b/b;->a()I

    move-result v0

    return v0
.end method

.method public static a(Lc/a/a/b/e;)Lc/a/a/b/c;
    .locals 1
    .param p0    # Lc/a/a/b/e;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lc/a/a/b/e<",
            "TT;>;)",
            "Lc/a/a/b/c<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/rxjava3/annotations/CheckReturnValue;
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    .annotation runtime Lio/reactivex/rxjava3/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance v0, Lc/a/a/f/e/a/b;

    invoke-direct {v0, p0}, Lc/a/a/f/e/a/b;-><init>(Lc/a/a/b/e;)V

    invoke-static {v0}, Lc/a/a/h/a;->a(Lc/a/a/b/c;)Lc/a/a/b/c;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(Lc/a/a/b/h;)Lc/a/a/b/c;
    .locals 2
    .param p1    # Lc/a/a/b/h;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/b/h;",
            ")",
            "Lc/a/a/b/c<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/rxjava3/annotations/CheckReturnValue;
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    .annotation runtime Lio/reactivex/rxjava3/annotations/SchedulerSupport;
        value = "custom"
    .end annotation

    invoke-static {}, Lc/a/a/b/c;->a()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lc/a/a/b/c;->a(Lc/a/a/b/h;ZI)Lc/a/a/b/c;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lc/a/a/b/h;ZI)Lc/a/a/b/c;
    .locals 1
    .param p1    # Lc/a/a/b/h;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/b/h;",
            "ZI)",
            "Lc/a/a/b/c<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/rxjava3/annotations/CheckReturnValue;
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    .annotation runtime Lio/reactivex/rxjava3/annotations/SchedulerSupport;
        value = "custom"
    .end annotation

    const-string v0, "scheduler is null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "bufferSize"

    invoke-static {p3, v0}, Lc/a/a/f/b/b;->a(ILjava/lang/String;)I

    new-instance v0, Lc/a/a/f/e/a/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lc/a/a/f/e/a/c;-><init>(Lc/a/a/b/f;Lc/a/a/b/h;ZI)V

    invoke-static {v0}, Lc/a/a/h/a;->a(Lc/a/a/b/c;)Lc/a/a/b/c;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lc/a/a/e/d;Lc/a/a/e/d;)Lc/a/a/c/b;
    .locals 1
    .param p1    # Lc/a/a/e/d;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p2    # Lc/a/a/e/d;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/e/d<",
            "-TT;>;",
            "Lc/a/a/e/d<",
            "-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lc/a/a/c/b;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/rxjava3/annotations/CheckReturnValue;
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    .annotation runtime Lio/reactivex/rxjava3/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    sget-object v0, Lc/a/a/f/b/a;->c:Lc/a/a/e/a;

    invoke-virtual {p0, p1, p2, v0}, Lc/a/a/b/c;->a(Lc/a/a/e/d;Lc/a/a/e/d;Lc/a/a/e/a;)Lc/a/a/c/b;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lc/a/a/e/d;Lc/a/a/e/d;Lc/a/a/e/a;)Lc/a/a/c/b;
    .locals 2
    .param p1    # Lc/a/a/e/d;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p2    # Lc/a/a/e/d;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p3    # Lc/a/a/e/a;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/e/d<",
            "-TT;>;",
            "Lc/a/a/e/d<",
            "-",
            "Ljava/lang/Throwable;",
            ">;",
            "Lc/a/a/e/a;",
            ")",
            "Lc/a/a/c/b;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/rxjava3/annotations/CheckReturnValue;
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    .annotation runtime Lio/reactivex/rxjava3/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "onNext is null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onError is null"

    invoke-static {p2, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onComplete is null"

    invoke-static {p3, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance v0, Lc/a/a/f/d/c;

    invoke-static {}, Lc/a/a/f/b/a;->a()Lc/a/a/e/d;

    move-result-object v1

    invoke-direct {v0, p1, p2, p3, v1}, Lc/a/a/f/d/c;-><init>(Lc/a/a/e/d;Lc/a/a/e/d;Lc/a/a/e/a;Lc/a/a/e/d;)V

    invoke-virtual {p0, v0}, Lc/a/a/b/c;->a(Lc/a/a/b/g;)V

    return-object v0
.end method

.method public final a(Lc/a/a/b/g;)V
    .locals 2
    .param p1    # Lc/a/a/b/g;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/b/g<",
            "-TT;>;)V"
        }
    .end annotation

    .annotation runtime Lio/reactivex/rxjava3/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "observer is null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    :try_start_0
    invoke-static {p0, p1}, Lc/a/a/h/a;->a(Lc/a/a/b/c;Lc/a/a/b/g;)Lc/a/a/b/g;

    move-result-object p1

    const-string v0, "The RxJavaPlugins.onSubscribe hook returned a null Observer. Please change the handler provided to RxJavaPlugins.setOnObservableSubscribe for invalid null returns. Further reading: https://github.com/ReactiveX/RxJava/wiki/Plugins"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lc/a/a/b/c;->b(Lc/a/a/b/g;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lc/a/a/d/b;->b(Ljava/lang/Throwable;)V

    invoke-static {p1}, Lc/a/a/h/a;->b(Ljava/lang/Throwable;)V

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Actually not, but can\'t throw other exceptions due to RS"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/NullPointerException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v0

    :catch_1
    move-exception p1

    throw p1
.end method

.method protected abstract b(Lc/a/a/b/g;)V
    .param p1    # Lc/a/a/b/g;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/b/g<",
            "-TT;>;)V"
        }
    .end annotation
.end method
