.class final Lc/a/a/b/h$a;
.super Ljava/lang/Object;

# interfaces
.implements Lc/a/a/c/b;
.implements Ljava/lang/Runnable;
.implements Lc/a/a/i/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc/a/a/b/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field final a:Ljava/lang/Runnable;
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation
.end field

.field final b:Lc/a/a/b/h$b;
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation
.end field

.field c:Ljava/lang/Thread;
    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Runnable;Lc/a/a/b/h$b;)V
    .locals 0
    .param p1    # Ljava/lang/Runnable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p2    # Lc/a/a/b/h$b;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc/a/a/b/h$a;->a:Ljava/lang/Runnable;

    iput-object p2, p0, Lc/a/a/b/h$a;->b:Lc/a/a/b/h$b;

    return-void
.end method


# virtual methods
.method public b()V
    .locals 2

    iget-object v0, p0, Lc/a/a/b/h$a;->c:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lc/a/a/b/h$a;->b:Lc/a/a/b/h$b;

    instance-of v1, v0, Lc/a/a/f/g/e;

    if-eqz v1, :cond_0

    check-cast v0, Lc/a/a/f/g/e;

    invoke-virtual {v0}, Lc/a/a/f/g/e;->c()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lc/a/a/b/h$a;->b:Lc/a/a/b/h$b;

    invoke-interface {v0}, Lc/a/a/c/b;->b()V

    :goto_0
    return-void
.end method

.method public run()V
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lc/a/a/b/h$a;->c:Ljava/lang/Thread;

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lc/a/a/b/h$a;->a:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lc/a/a/b/h$a;->b()V

    iput-object v0, p0, Lc/a/a/b/h$a;->c:Ljava/lang/Thread;

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {p0}, Lc/a/a/b/h$a;->b()V

    iput-object v0, p0, Lc/a/a/b/h$a;->c:Ljava/lang/Thread;

    throw v1
.end method
