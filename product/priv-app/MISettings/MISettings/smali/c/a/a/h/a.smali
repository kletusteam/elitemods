.class public final Lc/a/a/h/a;
.super Ljava/lang/Object;


# static fields
.field static volatile a:Lc/a/a/e/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/d<",
            "-",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field

.field static volatile b:Lc/a/a/e/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/e<",
            "-",
            "Ljava/lang/Runnable;",
            "+",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field

.field static volatile c:Lc/a/a/e/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/e<",
            "-",
            "Lc/a/a/e/h<",
            "Lc/a/a/b/h;",
            ">;+",
            "Lc/a/a/b/h;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field

.field static volatile d:Lc/a/a/e/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/e<",
            "-",
            "Lc/a/a/e/h<",
            "Lc/a/a/b/h;",
            ">;+",
            "Lc/a/a/b/h;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field

.field static volatile e:Lc/a/a/e/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/e<",
            "-",
            "Lc/a/a/e/h<",
            "Lc/a/a/b/h;",
            ">;+",
            "Lc/a/a/b/h;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field

.field static volatile f:Lc/a/a/e/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/e<",
            "-",
            "Lc/a/a/e/h<",
            "Lc/a/a/b/h;",
            ">;+",
            "Lc/a/a/b/h;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field

.field static volatile g:Lc/a/a/e/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/e<",
            "-",
            "Lc/a/a/b/h;",
            "+",
            "Lc/a/a/b/h;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field

.field static volatile h:Lc/a/a/e/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/e<",
            "-",
            "Lc/a/a/b/h;",
            "+",
            "Lc/a/a/b/h;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field

.field static volatile i:Lc/a/a/e/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/e<",
            "-",
            "Lc/a/a/b/c;",
            "+",
            "Lc/a/a/b/c;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field

.field static volatile j:Lc/a/a/e/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/e<",
            "-",
            "Lc/a/a/b/i;",
            "+",
            "Lc/a/a/b/i;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field

.field static volatile k:Lc/a/a/e/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/b<",
            "-",
            "Lc/a/a/b/c;",
            "-",
            "Lc/a/a/b/g;",
            "+",
            "Lc/a/a/b/g;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field

.field static volatile l:Lc/a/a/e/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/b<",
            "-",
            "Lc/a/a/b/i;",
            "-",
            "Lc/a/a/b/j;",
            "+",
            "Lc/a/a/b/j;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public static a(Lc/a/a/b/c;)Lc/a/a/b/c;
    .locals 1
    .param p0    # Lc/a/a/b/c;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lc/a/a/b/c<",
            "TT;>;)",
            "Lc/a/a/b/c<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    sget-object v0, Lc/a/a/h/a;->i:Lc/a/a/e/e;

    if-eqz v0, :cond_0

    invoke-static {v0, p0}, Lc/a/a/h/a;->a(Lc/a/a/e/e;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc/a/a/b/c;

    :cond_0
    return-object p0
.end method

.method public static a(Lc/a/a/b/c;Lc/a/a/b/g;)Lc/a/a/b/g;
    .locals 1
    .param p0    # Lc/a/a/b/c;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p1    # Lc/a/a/b/g;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lc/a/a/b/c<",
            "TT;>;",
            "Lc/a/a/b/g<",
            "-TT;>;)",
            "Lc/a/a/b/g<",
            "-TT;>;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    sget-object v0, Lc/a/a/h/a;->k:Lc/a/a/e/b;

    if-eqz v0, :cond_0

    invoke-static {v0, p0, p1}, Lc/a/a/h/a;->a(Lc/a/a/e/b;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc/a/a/b/g;

    return-object p0

    :cond_0
    return-object p1
.end method

.method public static a(Lc/a/a/b/h;)Lc/a/a/b/h;
    .locals 1
    .param p0    # Lc/a/a/b/h;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    sget-object v0, Lc/a/a/h/a;->g:Lc/a/a/e/e;

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    invoke-static {v0, p0}, Lc/a/a/h/a;->a(Lc/a/a/e/e;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc/a/a/b/h;

    return-object p0
.end method

.method static a(Lc/a/a/e/e;Lc/a/a/e/h;)Lc/a/a/b/h;
    .locals 0
    .param p0    # Lc/a/a/e/e;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/e/e<",
            "-",
            "Lc/a/a/e/h<",
            "Lc/a/a/b/h;",
            ">;+",
            "Lc/a/a/b/h;",
            ">;",
            "Lc/a/a/e/h<",
            "Lc/a/a/b/h;",
            ">;)",
            "Lc/a/a/b/h;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    invoke-static {p0, p1}, Lc/a/a/h/a;->a(Lc/a/a/e/e;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    const-string p1, "Scheduler Supplier result can\'t be null"

    invoke-static {p0, p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc/a/a/b/h;

    return-object p0
.end method

.method static a(Lc/a/a/e/h;)Lc/a/a/b/h;
    .locals 1
    .param p0    # Lc/a/a/e/h;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/e/h<",
            "Lc/a/a/b/h;",
            ">;)",
            "Lc/a/a/b/h;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    :try_start_0
    invoke-interface {p0}, Lc/a/a/e/h;->get()Ljava/lang/Object;

    move-result-object p0

    const-string v0, "Scheduler Supplier result can\'t be null"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc/a/a/b/h;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lio/reactivex/rxjava3/internal/util/a;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
.end method

.method public static a(Lc/a/a/b/i;)Lc/a/a/b/i;
    .locals 1
    .param p0    # Lc/a/a/b/i;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lc/a/a/b/i<",
            "TT;>;)",
            "Lc/a/a/b/i<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    sget-object v0, Lc/a/a/h/a;->j:Lc/a/a/e/e;

    if-eqz v0, :cond_0

    invoke-static {v0, p0}, Lc/a/a/h/a;->a(Lc/a/a/e/e;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc/a/a/b/i;

    :cond_0
    return-object p0
.end method

.method public static a(Lc/a/a/b/i;Lc/a/a/b/j;)Lc/a/a/b/j;
    .locals 1
    .param p0    # Lc/a/a/b/i;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p1    # Lc/a/a/b/j;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lc/a/a/b/i<",
            "TT;>;",
            "Lc/a/a/b/j<",
            "-TT;>;)",
            "Lc/a/a/b/j<",
            "-TT;>;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    sget-object v0, Lc/a/a/h/a;->l:Lc/a/a/e/b;

    if-eqz v0, :cond_0

    invoke-static {v0, p0, p1}, Lc/a/a/h/a;->a(Lc/a/a/e/b;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc/a/a/b/j;

    return-object p0

    :cond_0
    return-object p1
.end method

.method static a(Lc/a/a/e/b;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lc/a/a/e/b;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lc/a/a/e/b<",
            "TT;TU;TR;>;TT;TU;)TR;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    :try_start_0
    invoke-interface {p0, p1, p2}, Lc/a/a/e/b;->apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lio/reactivex/rxjava3/internal/util/a;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
.end method

.method static a(Lc/a/a/e/e;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lc/a/a/e/e;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lc/a/a/e/e<",
            "TT;TR;>;TT;)TR;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    :try_start_0
    invoke-interface {p0, p1}, Lc/a/a/e/e;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lio/reactivex/rxjava3/internal/util/a;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
.end method

.method public static a(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Ljava/lang/Runnable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    const-string v0, "run is null"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    sget-object v0, Lc/a/a/h/a;->b:Lc/a/a/e/e;

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    invoke-static {v0, p0}, Lc/a/a/h/a;->a(Lc/a/a/e/e;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Runnable;

    return-object p0
.end method

.method static a(Ljava/lang/Throwable;)Z
    .locals 2

    instance-of v0, p0, Lc/a/a/d/c;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    instance-of v0, p0, Ljava/lang/IllegalStateException;

    if-eqz v0, :cond_1

    return v1

    :cond_1
    instance-of v0, p0, Ljava/lang/NullPointerException;

    if-eqz v0, :cond_2

    return v1

    :cond_2
    instance-of v0, p0, Ljava/lang/IllegalArgumentException;

    if-eqz v0, :cond_3

    return v1

    :cond_3
    instance-of p0, p0, Lc/a/a/d/a;

    if-eqz p0, :cond_4

    return v1

    :cond_4
    const/4 p0, 0x0

    return p0
.end method

.method public static b(Lc/a/a/b/h;)Lc/a/a/b/h;
    .locals 1
    .param p0    # Lc/a/a/b/h;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    sget-object v0, Lc/a/a/h/a;->h:Lc/a/a/e/e;

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    invoke-static {v0, p0}, Lc/a/a/h/a;->a(Lc/a/a/e/e;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc/a/a/b/h;

    return-object p0
.end method

.method public static b(Lc/a/a/e/h;)Lc/a/a/b/h;
    .locals 1
    .param p0    # Lc/a/a/e/h;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/e/h<",
            "Lc/a/a/b/h;",
            ">;)",
            "Lc/a/a/b/h;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    const-string v0, "Scheduler Supplier can\'t be null"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    sget-object v0, Lc/a/a/h/a;->c:Lc/a/a/e/e;

    if-nez v0, :cond_0

    invoke-static {p0}, Lc/a/a/h/a;->a(Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-static {v0, p0}, Lc/a/a/h/a;->a(Lc/a/a/e/e;Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/lang/Throwable;)V
    .locals 2
    .param p0    # Ljava/lang/Throwable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param

    sget-object v0, Lc/a/a/h/a;->a:Lc/a/a/e/d;

    if-nez p0, :cond_0

    const-string p0, "onError called with a null Throwable."

    invoke-static {p0}, Lio/reactivex/rxjava3/internal/util/a;->a(Ljava/lang/String;)Ljava/lang/NullPointerException;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lc/a/a/h/a;->a(Ljava/lang/Throwable;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lc/a/a/d/e;

    invoke-direct {v1, p0}, Lc/a/a/d/e;-><init>(Ljava/lang/Throwable;)V

    move-object p0, v1

    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    :try_start_0
    invoke-interface {v0, p0}, Lc/a/a/e/d;->accept(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-static {v0}, Lc/a/a/h/a;->c(Ljava/lang/Throwable;)V

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-static {p0}, Lc/a/a/h/a;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static c(Lc/a/a/e/h;)Lc/a/a/b/h;
    .locals 1
    .param p0    # Lc/a/a/e/h;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/e/h<",
            "Lc/a/a/b/h;",
            ">;)",
            "Lc/a/a/b/h;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    const-string v0, "Scheduler Supplier can\'t be null"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    sget-object v0, Lc/a/a/h/a;->e:Lc/a/a/e/e;

    if-nez v0, :cond_0

    invoke-static {p0}, Lc/a/a/h/a;->a(Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-static {v0, p0}, Lc/a/a/h/a;->a(Lc/a/a/e/e;Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object p0

    return-object p0
.end method

.method static c(Ljava/lang/Throwable;)V
    .locals 2
    .param p0    # Ljava/lang/Throwable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    invoke-interface {v1, v0, p0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static d(Lc/a/a/e/h;)Lc/a/a/b/h;
    .locals 1
    .param p0    # Lc/a/a/e/h;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/e/h<",
            "Lc/a/a/b/h;",
            ">;)",
            "Lc/a/a/b/h;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    const-string v0, "Scheduler Supplier can\'t be null"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    sget-object v0, Lc/a/a/h/a;->f:Lc/a/a/e/e;

    if-nez v0, :cond_0

    invoke-static {p0}, Lc/a/a/h/a;->a(Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-static {v0, p0}, Lc/a/a/h/a;->a(Lc/a/a/e/e;Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object p0

    return-object p0
.end method

.method public static e(Lc/a/a/e/h;)Lc/a/a/b/h;
    .locals 1
    .param p0    # Lc/a/a/e/h;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/e/h<",
            "Lc/a/a/b/h;",
            ">;)",
            "Lc/a/a/b/h;"
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    const-string v0, "Scheduler Supplier can\'t be null"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    sget-object v0, Lc/a/a/h/a;->d:Lc/a/a/e/e;

    if-nez v0, :cond_0

    invoke-static {p0}, Lc/a/a/h/a;->a(Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-static {v0, p0}, Lc/a/a/h/a;->a(Lc/a/a/e/e;Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object p0

    return-object p0
.end method
