.class final Lc/a/a/f/g/c$b;
.super Lc/a/a/b/h$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc/a/a/f/g/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation


# instance fields
.field private final a:Lc/a/a/c/a;

.field private final b:Lc/a/a/f/g/c$a;

.field private final c:Lc/a/a/f/g/c$c;

.field final d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lc/a/a/f/g/c$a;)V
    .locals 1

    invoke-direct {p0}, Lc/a/a/b/h$b;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lc/a/a/f/g/c$b;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, Lc/a/a/f/g/c$b;->b:Lc/a/a/f/g/c$a;

    new-instance v0, Lc/a/a/c/a;

    invoke-direct {v0}, Lc/a/a/c/a;-><init>()V

    iput-object v0, p0, Lc/a/a/f/g/c$b;->a:Lc/a/a/c/a;

    invoke-virtual {p1}, Lc/a/a/f/g/c$a;->a()Lc/a/a/f/g/c$c;

    move-result-object p1

    iput-object p1, p0, Lc/a/a/f/g/c$b;->c:Lc/a/a/f/g/c$c;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lc/a/a/c/b;
    .locals 6
    .param p1    # Ljava/lang/Runnable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/TimeUnit;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    iget-object v0, p0, Lc/a/a/f/g/c$b;->a:Lc/a/a/c/a;

    invoke-virtual {v0}, Lc/a/a/c/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    return-object p1

    :cond_0
    iget-object v0, p0, Lc/a/a/f/g/c$b;->c:Lc/a/a/f/g/c$c;

    iget-object v5, p0, Lc/a/a/f/g/c$b;->a:Lc/a/a/c/a;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lc/a/a/f/g/e;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;Lc/a/a/c/c;)Lc/a/a/f/g/i;

    move-result-object p1

    return-object p1
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lc/a/a/f/g/c$b;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lc/a/a/f/g/c$b;->a:Lc/a/a/c/a;

    invoke-virtual {v0}, Lc/a/a/c/a;->b()V

    iget-object v0, p0, Lc/a/a/f/g/c$b;->b:Lc/a/a/f/g/c$a;

    iget-object v1, p0, Lc/a/a/f/g/c$b;->c:Lc/a/a/f/g/c$c;

    invoke-virtual {v0, v1}, Lc/a/a/f/g/c$a;->a(Lc/a/a/f/g/c$c;)V

    :cond_0
    return-void
.end method
