.class final Lc/a/a/f/g/m$c;
.super Lc/a/a/b/h$b;

# interfaces
.implements Lc/a/a/c/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc/a/a/f/g/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc/a/a/f/g/m$c$a;
    }
.end annotation


# instance fields
.field final a:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lc/a/a/f/g/m$b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field final c:Ljava/util/concurrent/atomic/AtomicInteger;

.field volatile d:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lc/a/a/b/h$b;-><init>()V

    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lc/a/a/f/g/m$c;->a:Ljava/util/concurrent/PriorityBlockingQueue;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lc/a/a/f/g/m$c;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lc/a/a/f/g/m$c;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method


# virtual methods
.method a(Ljava/lang/Runnable;J)Lc/a/a/c/b;
    .locals 1

    goto/32 :goto_f

    nop

    :goto_0
    invoke-virtual {p2}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_12

    nop

    :goto_1
    invoke-virtual {p1, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    goto/32 :goto_17

    nop

    :goto_2
    iget-object p2, p0, Lc/a/a/f/g/m$c;->a:Ljava/util/concurrent/PriorityBlockingQueue;

    goto/32 :goto_0

    nop

    :goto_3
    return-object p1

    :goto_4
    goto/32 :goto_10

    nop

    :goto_5
    const/4 p1, 0x1

    :goto_6
    goto/32 :goto_11

    nop

    :goto_7
    iget-object p2, p0, Lc/a/a/f/g/m$c;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_25

    nop

    :goto_8
    iget-object p2, p2, Lc/a/a/f/g/m$b;->a:Ljava/lang/Runnable;

    goto/32 :goto_b

    nop

    :goto_9
    iget-object p1, p0, Lc/a/a/f/g/m$c;->a:Ljava/util/concurrent/PriorityBlockingQueue;

    goto/32 :goto_23

    nop

    :goto_a
    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result p3

    goto/32 :goto_20

    nop

    :goto_b
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto/32 :goto_c

    nop

    :goto_c
    goto :goto_6

    :goto_d
    goto/32 :goto_1d

    nop

    :goto_e
    sget-object p1, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    goto/32 :goto_18

    nop

    :goto_f
    iget-boolean v0, p0, Lc/a/a/f/g/m$c;->d:Z

    goto/32 :goto_22

    nop

    :goto_10
    new-instance v0, Lc/a/a/f/g/m$b;

    goto/32 :goto_21

    nop

    :goto_11
    iget-boolean p2, p0, Lc/a/a/f/g/m$c;->d:Z

    goto/32 :goto_14

    nop

    :goto_12
    check-cast p2, Lc/a/a/f/g/m$b;

    goto/32 :goto_1c

    nop

    :goto_13
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result p1

    goto/32 :goto_1e

    nop

    :goto_14
    if-nez p2, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_9

    nop

    :goto_15
    return-object p1

    :goto_16
    goto/32 :goto_2b

    nop

    :goto_17
    iget-object p1, p0, Lc/a/a/f/g/m$c;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_13

    nop

    :goto_18
    return-object p1

    :goto_19
    goto/32 :goto_2

    nop

    :goto_1a
    iget-object p3, p0, Lc/a/a/f/g/m$c;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_a

    nop

    :goto_1b
    return-object p1

    :goto_1c
    if-eqz p2, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_7

    nop

    :goto_1d
    new-instance p1, Lc/a/a/f/g/m$c$a;

    goto/32 :goto_2a

    nop

    :goto_1e
    if-eqz p1, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_5

    nop

    :goto_1f
    iget-object p1, p0, Lc/a/a/f/g/m$c;->a:Ljava/util/concurrent/PriorityBlockingQueue;

    goto/32 :goto_1

    nop

    :goto_20
    invoke-direct {v0, p1, p2, p3}, Lc/a/a/f/g/m$b;-><init>(Ljava/lang/Runnable;Ljava/lang/Long;I)V

    goto/32 :goto_1f

    nop

    :goto_21
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    goto/32 :goto_1a

    nop

    :goto_22
    if-nez v0, :cond_3

    goto/32 :goto_4

    :cond_3
    goto/32 :goto_27

    nop

    :goto_23
    invoke-virtual {p1}, Ljava/util/concurrent/PriorityBlockingQueue;->clear()V

    goto/32 :goto_e

    nop

    :goto_24
    invoke-static {p1}, Lc/a/a/c/b;->a(Ljava/lang/Runnable;)Lc/a/a/c/b;

    move-result-object p1

    goto/32 :goto_1b

    nop

    :goto_25
    neg-int p1, p1

    goto/32 :goto_29

    nop

    :goto_26
    sget-object p1, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    goto/32 :goto_15

    nop

    :goto_27
    sget-object p1, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    goto/32 :goto_3

    nop

    :goto_28
    if-eqz p3, :cond_4

    goto/32 :goto_6

    :cond_4
    goto/32 :goto_8

    nop

    :goto_29
    invoke-virtual {p2, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result p1

    goto/32 :goto_2c

    nop

    :goto_2a
    invoke-direct {p1, p0, v0}, Lc/a/a/f/g/m$c$a;-><init>(Lc/a/a/f/g/m$c;Lc/a/a/f/g/m$b;)V

    goto/32 :goto_24

    nop

    :goto_2b
    iget-boolean p3, p2, Lc/a/a/f/g/m$b;->d:Z

    goto/32 :goto_28

    nop

    :goto_2c
    if-eqz p1, :cond_5

    goto/32 :goto_6

    :cond_5
    goto/32 :goto_26

    nop
.end method

.method public a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lc/a/a/c/b;
    .locals 2
    .param p1    # Ljava/lang/Runnable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/TimeUnit;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lc/a/a/b/h$b;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p2

    add-long/2addr v0, p2

    new-instance p2, Lc/a/a/f/g/m$a;

    invoke-direct {p2, p1, p0, v0, v1}, Lc/a/a/f/g/m$a;-><init>(Ljava/lang/Runnable;Lc/a/a/f/g/m$c;J)V

    invoke-virtual {p0, p2, v0, v1}, Lc/a/a/f/g/m$c;->a(Ljava/lang/Runnable;J)Lc/a/a/c/b;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/Runnable;)Lc/a/a/c/b;
    .locals 2
    .param p1    # Ljava/lang/Runnable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lc/a/a/b/h$b;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lc/a/a/f/g/m$c;->a(Ljava/lang/Runnable;J)Lc/a/a/c/b;

    move-result-object p1

    return-object p1
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lc/a/a/f/g/m$c;->d:Z

    return-void
.end method
