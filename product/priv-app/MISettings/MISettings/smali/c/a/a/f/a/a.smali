.class public final enum Lc/a/a/f/a/a;
.super Ljava/lang/Enum;

# interfaces
.implements Lc/a/a/c/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lc/a/a/f/a/a;",
        ">;",
        "Lc/a/a/c/b;"
    }
.end annotation


# static fields
.field public static final enum a:Lc/a/a/f/a/a;

.field private static final synthetic b:[Lc/a/a/f/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lc/a/a/f/a/a;

    const/4 v1, 0x0

    const-string v2, "DISPOSED"

    invoke-direct {v0, v2, v1}, Lc/a/a/f/a/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lc/a/a/f/a/a;->a:Lc/a/a/f/a/a;

    const/4 v0, 0x1

    new-array v0, v0, [Lc/a/a/f/a/a;

    sget-object v2, Lc/a/a/f/a/a;->a:Lc/a/a/f/a/a;

    aput-object v2, v0, v1

    sput-object v0, Lc/a/a/f/a/a;->b:[Lc/a/a/f/a/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Lc/a/a/c/b;)Z
    .locals 1

    sget-object v0, Lc/a/a/f/a/a;->a:Lc/a/a/f/a/a;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static a(Lc/a/a/c/b;Lc/a/a/c/b;)Z
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "next is null"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lc/a/a/h/a;->b(Ljava/lang/Throwable;)V

    return v0

    :cond_0
    if-eqz p0, :cond_1

    invoke-interface {p1}, Lc/a/a/c/b;->b()V

    invoke-static {}, Lc/a/a/f/a/a;->c()V

    return v0

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method public static a(Ljava/util/concurrent/atomic/AtomicReference;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lc/a/a/c/b;",
            ">;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc/a/a/c/b;

    sget-object v1, Lc/a/a/f/a/a;->a:Lc/a/a/f/a/a;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc/a/a/c/b;

    if-eq p0, v1, :cond_1

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lc/a/a/c/b;->b()V

    :cond_0
    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method public static a(Ljava/util/concurrent/atomic/AtomicReference;Lc/a/a/c/b;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lc/a/a/c/b;",
            ">;",
            "Lc/a/a/c/b;",
            ")Z"
        }
    .end annotation

    :cond_0
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc/a/a/c/b;

    sget-object v1, Lc/a/a/f/a/a;->a:Lc/a/a/f/a/a;

    if-ne v0, v1, :cond_2

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lc/a/a/c/b;->b()V

    :cond_1
    const/4 p0, 0x0

    return p0

    :cond_2
    invoke-virtual {p0, v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0
.end method

.method public static b(Ljava/util/concurrent/atomic/AtomicReference;Lc/a/a/c/b;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lc/a/a/c/b;",
            ">;",
            "Lc/a/a/c/b;",
            ")Z"
        }
    .end annotation

    const-string v0, "d is null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lc/a/a/c/b;->b()V

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p0

    sget-object p1, Lc/a/a/f/a/a;->a:Lc/a/a/f/a/a;

    if-eq p0, p1, :cond_0

    invoke-static {}, Lc/a/a/f/a/a;->c()V

    :cond_0
    const/4 p0, 0x0

    return p0

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method public static c()V
    .locals 2

    new-instance v0, Lc/a/a/d/d;

    const-string v1, "Disposable already set!"

    invoke-direct {v0, v1}, Lc/a/a/d/d;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lc/a/a/h/a;->b(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lc/a/a/f/a/a;
    .locals 1

    const-class v0, Lc/a/a/f/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lc/a/a/f/a/a;

    return-object p0
.end method

.method public static values()[Lc/a/a/f/a/a;
    .locals 1

    sget-object v0, Lc/a/a/f/a/a;->b:[Lc/a/a/f/a/a;

    invoke-virtual {v0}, [Lc/a/a/f/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lc/a/a/f/a/a;

    return-object v0
.end method


# virtual methods
.method public b()V
    .locals 0

    return-void
.end method
