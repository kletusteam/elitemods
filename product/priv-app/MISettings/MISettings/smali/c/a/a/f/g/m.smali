.class public final Lc/a/a/f/g/m;
.super Lc/a/a/b/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc/a/a/f/g/m$a;,
        Lc/a/a/f/g/m$b;,
        Lc/a/a/f/g/m$c;
    }
.end annotation


# static fields
.field private static final b:Lc/a/a/f/g/m;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lc/a/a/f/g/m;

    invoke-direct {v0}, Lc/a/a/f/g/m;-><init>()V

    sput-object v0, Lc/a/a/f/g/m;->b:Lc/a/a/f/g/m;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lc/a/a/b/h;-><init>()V

    return-void
.end method

.method public static b()Lc/a/a/f/g/m;
    .locals 1

    sget-object v0, Lc/a/a/f/g/m;->b:Lc/a/a/f/g/m;

    return-object v0
.end method


# virtual methods
.method public a()Lc/a/a/b/h$b;
    .locals 1
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    new-instance v0, Lc/a/a/f/g/m$c;

    invoke-direct {v0}, Lc/a/a/f/g/m$c;-><init>()V

    return-object v0
.end method

.method public a(Ljava/lang/Runnable;)Lc/a/a/c/b;
    .locals 0
    .param p1    # Ljava/lang/Runnable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    invoke-static {p1}, Lc/a/a/h/a;->a(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    sget-object p1, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    return-object p1
.end method

.method public a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lc/a/a/c/b;
    .locals 0
    .param p1    # Ljava/lang/Runnable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    :try_start_0
    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->sleep(J)V

    invoke-static {p1}, Lc/a/a/h/a;->a(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Thread;->interrupt()V

    invoke-static {p1}, Lc/a/a/h/a;->b(Ljava/lang/Throwable;)V

    :goto_0
    sget-object p1, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    return-object p1
.end method
