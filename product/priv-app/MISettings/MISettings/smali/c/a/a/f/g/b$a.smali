.class final Lc/a/a/f/g/b$a;
.super Lc/a/a/b/h$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc/a/a/f/g/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private final a:Lc/a/a/f/a/c;

.field private final b:Lc/a/a/c/a;

.field private final c:Lc/a/a/f/a/c;

.field private final d:Lc/a/a/f/g/b$c;

.field volatile e:Z


# direct methods
.method constructor <init>(Lc/a/a/f/g/b$c;)V
    .locals 1

    invoke-direct {p0}, Lc/a/a/b/h$b;-><init>()V

    iput-object p1, p0, Lc/a/a/f/g/b$a;->d:Lc/a/a/f/g/b$c;

    new-instance p1, Lc/a/a/f/a/c;

    invoke-direct {p1}, Lc/a/a/f/a/c;-><init>()V

    iput-object p1, p0, Lc/a/a/f/g/b$a;->a:Lc/a/a/f/a/c;

    new-instance p1, Lc/a/a/c/a;

    invoke-direct {p1}, Lc/a/a/c/a;-><init>()V

    iput-object p1, p0, Lc/a/a/f/g/b$a;->b:Lc/a/a/c/a;

    new-instance p1, Lc/a/a/f/a/c;

    invoke-direct {p1}, Lc/a/a/f/a/c;-><init>()V

    iput-object p1, p0, Lc/a/a/f/g/b$a;->c:Lc/a/a/f/a/c;

    iget-object p1, p0, Lc/a/a/f/g/b$a;->c:Lc/a/a/f/a/c;

    iget-object v0, p0, Lc/a/a/f/g/b$a;->a:Lc/a/a/f/a/c;

    invoke-virtual {p1, v0}, Lc/a/a/f/a/c;->b(Lc/a/a/c/b;)Z

    iget-object p1, p0, Lc/a/a/f/g/b$a;->c:Lc/a/a/f/a/c;

    iget-object v0, p0, Lc/a/a/f/g/b$a;->b:Lc/a/a/c/a;

    invoke-virtual {p1, v0}, Lc/a/a/f/a/c;->b(Lc/a/a/c/b;)Z

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lc/a/a/c/b;
    .locals 6
    .param p1    # Ljava/lang/Runnable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/TimeUnit;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    iget-boolean v0, p0, Lc/a/a/f/g/b$a;->e:Z

    if-eqz v0, :cond_0

    sget-object p1, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    return-object p1

    :cond_0
    iget-object v0, p0, Lc/a/a/f/g/b$a;->d:Lc/a/a/f/g/b$c;

    iget-object v5, p0, Lc/a/a/f/g/b$a;->b:Lc/a/a/c/a;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lc/a/a/f/g/e;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;Lc/a/a/c/c;)Lc/a/a/f/g/i;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/Runnable;)Lc/a/a/c/b;
    .locals 6
    .param p1    # Ljava/lang/Runnable;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    iget-boolean v0, p0, Lc/a/a/f/g/b$a;->e:Z

    if-eqz v0, :cond_0

    sget-object p1, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    return-object p1

    :cond_0
    iget-object v0, p0, Lc/a/a/f/g/b$a;->d:Lc/a/a/f/g/b$c;

    const-wide/16 v2, 0x0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lc/a/a/f/g/b$a;->a:Lc/a/a/f/a/c;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lc/a/a/f/g/e;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;Lc/a/a/c/c;)Lc/a/a/f/g/i;

    move-result-object p1

    return-object p1
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lc/a/a/f/g/b$a;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lc/a/a/f/g/b$a;->e:Z

    iget-object v0, p0, Lc/a/a/f/g/b$a;->c:Lc/a/a/f/a/c;

    invoke-virtual {v0}, Lc/a/a/f/a/c;->b()V

    :cond_0
    return-void
.end method
