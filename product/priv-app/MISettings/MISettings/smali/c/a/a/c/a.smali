.class public final Lc/a/a/c/a;
.super Ljava/lang/Object;

# interfaces
.implements Lc/a/a/c/b;
.implements Lc/a/a/c/c;


# instance fields
.field a:Lio/reactivex/rxjava3/internal/util/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/internal/util/b<",
            "Lc/a/a/c/b;",
            ">;"
        }
    .end annotation
.end field

.field volatile b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a(Lio/reactivex/rxjava3/internal/util/b;)V
    .locals 6
    .param p1    # Lio/reactivex/rxjava3/internal/util/b;
        .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/rxjava3/internal/util/b<",
            "Lc/a/a/c/b;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_25

    nop

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result p1

    goto/32 :goto_1c

    nop

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_e

    nop

    :goto_2
    instance-of v5, v4, Lc/a/a/c/b;

    goto/32 :goto_17

    nop

    :goto_3
    if-eqz v3, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_24

    nop

    :goto_4
    goto :goto_9

    :catch_0
    move-exception v4

    goto/32 :goto_23

    nop

    :goto_5
    if-lt v0, v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_19

    nop

    :goto_6
    const/4 v2, 0x0

    goto/32 :goto_20

    nop

    :goto_7
    if-nez v3, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_0

    nop

    :goto_8
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_9
    goto/32 :goto_1

    nop

    :goto_a
    throw p1

    :goto_b
    goto/32 :goto_16

    nop

    :goto_c
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :goto_d
    goto/32 :goto_8

    nop

    :goto_e
    goto :goto_1f

    :goto_f
    goto/32 :goto_7

    nop

    :goto_10
    array-length v1, p1

    goto/32 :goto_6

    nop

    :goto_11
    return-void

    :goto_12
    goto/32 :goto_13

    nop

    :goto_13
    const/4 v0, 0x0

    goto/32 :goto_14

    nop

    :goto_14
    invoke-virtual {p1}, Lio/reactivex/rxjava3/internal/util/b;->a()[Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_10

    nop

    :goto_15
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_1b

    nop

    :goto_16
    return-void

    :goto_17
    if-nez v5, :cond_3

    goto/32 :goto_9

    :cond_3
    :try_start_0
    check-cast v4, Lc/a/a/c/b;

    invoke-interface {v4}, Lc/a/a/c/b;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_4

    nop

    :goto_18
    invoke-static {p1}, Lio/reactivex/rxjava3/internal/util/a;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_21

    nop

    :goto_19
    aget-object v4, p1, v0

    goto/32 :goto_2

    nop

    :goto_1a
    new-instance p1, Lc/a/a/d/a;

    goto/32 :goto_1d

    nop

    :goto_1b
    check-cast p1, Ljava/lang/Throwable;

    goto/32 :goto_18

    nop

    :goto_1c
    const/4 v0, 0x1

    goto/32 :goto_26

    nop

    :goto_1d
    invoke-direct {p1, v3}, Lc/a/a/d/a;-><init>(Ljava/lang/Iterable;)V

    goto/32 :goto_a

    nop

    :goto_1e
    move v0, v2

    :goto_1f
    goto/32 :goto_5

    nop

    :goto_20
    move-object v3, v0

    goto/32 :goto_1e

    nop

    :goto_21
    throw p1

    :goto_22
    goto/32 :goto_1a

    nop

    :goto_23
    invoke-static {v4}, Lc/a/a/d/b;->b(Ljava/lang/Throwable;)V

    goto/32 :goto_3

    nop

    :goto_24
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_c

    nop

    :goto_25
    if-eqz p1, :cond_4

    goto/32 :goto_12

    :cond_4
    goto/32 :goto_11

    nop

    :goto_26
    if-eq p1, v0, :cond_5

    goto/32 :goto_22

    :cond_5
    goto/32 :goto_15

    nop
.end method

.method public a(Lc/a/a/c/b;)Z
    .locals 1
    .param p1    # Lc/a/a/c/b;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0, p1}, Lc/a/a/c/a;->c(Lc/a/a/c/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lc/a/a/c/b;->b()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public b()V
    .locals 2

    iget-boolean v0, p0, Lc/a/a/c/a;->b:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lc/a/a/c/a;->b:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lc/a/a/c/a;->b:Z

    iget-object v0, p0, Lc/a/a/c/a;->a:Lio/reactivex/rxjava3/internal/util/b;

    const/4 v1, 0x0

    iput-object v1, p0, Lc/a/a/c/a;->a:Lio/reactivex/rxjava3/internal/util/b;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v0}, Lc/a/a/c/a;->a(Lio/reactivex/rxjava3/internal/util/b;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lc/a/a/c/b;)Z
    .locals 1
    .param p1    # Lc/a/a/c/b;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param

    const-string v0, "disposable is null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-boolean v0, p0, Lc/a/a/c/a;->b:Z

    if-nez v0, :cond_2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lc/a/a/c/a;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lc/a/a/c/a;->a:Lio/reactivex/rxjava3/internal/util/b;

    if-nez v0, :cond_0

    new-instance v0, Lio/reactivex/rxjava3/internal/util/b;

    invoke-direct {v0}, Lio/reactivex/rxjava3/internal/util/b;-><init>()V

    iput-object v0, p0, Lc/a/a/c/a;->a:Lio/reactivex/rxjava3/internal/util/b;

    :cond_0
    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/internal/util/b;->a(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    monitor-exit p0

    return p1

    :cond_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Lc/a/a/c/b;->b()V

    const/4 p1, 0x0

    return p1
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lc/a/a/c/a;->b:Z

    return v0
.end method

.method public c(Lc/a/a/c/b;)Z
    .locals 2
    .param p1    # Lc/a/a/c/b;
        .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
        .end annotation
    .end param

    const-string v0, "disposable is null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-boolean v0, p0, Lc/a/a/c/a;->b:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lc/a/a/c/a;->b:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    return v1

    :cond_1
    iget-object v0, p0, Lc/a/a/c/a;->a:Lio/reactivex/rxjava3/internal/util/b;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/internal/util/b;->b(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    monitor-exit p0

    const/4 p1, 0x1

    return p1

    :cond_3
    :goto_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
