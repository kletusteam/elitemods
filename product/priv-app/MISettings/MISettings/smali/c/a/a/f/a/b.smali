.class public final enum Lc/a/a/f/a/b;
.super Ljava/lang/Enum;

# interfaces
.implements Lc/a/a/f/c/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lc/a/a/f/a/b;",
        ">;",
        "Lc/a/a/f/c/b<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lc/a/a/f/a/b;

.field public static final enum b:Lc/a/a/f/a/b;

.field private static final synthetic c:[Lc/a/a/f/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lc/a/a/f/a/b;

    const/4 v1, 0x0

    const-string v2, "INSTANCE"

    invoke-direct {v0, v2, v1}, Lc/a/a/f/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    new-instance v0, Lc/a/a/f/a/b;

    const/4 v2, 0x1

    const-string v3, "NEVER"

    invoke-direct {v0, v3, v2}, Lc/a/a/f/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lc/a/a/f/a/b;->b:Lc/a/a/f/a/b;

    const/4 v0, 0x2

    new-array v0, v0, [Lc/a/a/f/a/b;

    sget-object v3, Lc/a/a/f/a/b;->a:Lc/a/a/f/a/b;

    aput-object v3, v0, v1

    sget-object v1, Lc/a/a/f/a/b;->b:Lc/a/a/f/a/b;

    aput-object v1, v0, v2

    sput-object v0, Lc/a/a/f/a/b;->c:[Lc/a/a/f/a/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lc/a/a/f/a/b;
    .locals 1

    const-class v0, Lc/a/a/f/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lc/a/a/f/a/b;

    return-object p0
.end method

.method public static values()[Lc/a/a/f/a/b;
    .locals 1

    sget-object v0, Lc/a/a/f/a/b;->c:[Lc/a/a/f/a/b;

    invoke-virtual {v0}, [Lc/a/a/f/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lc/a/a/f/a/b;

    return-object v0
.end method


# virtual methods
.method public a(I)I
    .locals 0

    and-int/lit8 p1, p1, 0x2

    return p1
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public clear()V
    .locals 0

    return-void
.end method

.method public isEmpty()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public offer(Ljava/lang/Object;)Z
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Should not be called!"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public poll()Ljava/lang/Object;
    .locals 1
    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method
