.class final Lc/a/a/f/g/c$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc/a/a/f/g/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private final a:J

.field private final b:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lc/a/a/f/g/c$c;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lc/a/a/c/a;

.field private final d:Ljava/util/concurrent/ScheduledExecutorService;

.field private final e:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future<",
            "*>;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/ThreadFactory;


# direct methods
.method constructor <init>(JLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/ThreadFactory;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p3, :cond_0

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide p1

    goto :goto_0

    :cond_0
    const-wide/16 p1, 0x0

    :goto_0
    iput-wide p1, p0, Lc/a/a/f/g/c$a;->a:J

    new-instance p1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object p1, p0, Lc/a/a/f/g/c$a;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance p1, Lc/a/a/c/a;

    invoke-direct {p1}, Lc/a/a/c/a;-><init>()V

    iput-object p1, p0, Lc/a/a/f/g/c$a;->c:Lc/a/a/c/a;

    iput-object p4, p0, Lc/a/a/f/g/c$a;->f:Ljava/util/concurrent/ThreadFactory;

    const/4 p1, 0x0

    if-eqz p3, :cond_1

    const/4 p1, 0x1

    sget-object p2, Lc/a/a/f/g/c;->c:Lc/a/a/f/g/g;

    invoke-static {p1, p2}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object p1

    iget-wide v4, p0, Lc/a/a/f/g/c$a;->a:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v0, p1

    move-object v1, p0

    move-wide v2, v4

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p2

    goto :goto_1

    :cond_1
    move-object p2, p1

    :goto_1
    iput-object p1, p0, Lc/a/a/f/g/c$a;->d:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p2, p0, Lc/a/a/f/g/c$a;->e:Ljava/util/concurrent/Future;

    return-void
.end method

.method static a(Ljava/util/concurrent/ConcurrentLinkedQueue;Lc/a/a/c/a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lc/a/a/f/g/c$c;",
            ">;",
            "Lc/a/a/c/a;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lc/a/a/f/g/c$a;->b()J

    move-result-wide v0

    invoke-virtual {p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lc/a/a/f/g/c$c;

    invoke-virtual {v3}, Lc/a/a/f/g/c$c;->d()J

    move-result-wide v4

    cmp-long v4, v4, v0

    if-gtz v4, :cond_1

    invoke-virtual {p0, v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v3}, Lc/a/a/c/a;->a(Lc/a/a/c/b;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method static b()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method a()Lc/a/a/f/g/c$c;
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    invoke-virtual {v1, v0}, Lc/a/a/c/a;->b(Lc/a/a/c/b;)Z

    goto/32 :goto_d

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_b

    nop

    :goto_2
    check-cast v0, Lc/a/a/f/g/c$c;

    goto/32 :goto_14

    nop

    :goto_3
    iget-object v0, p0, Lc/a/a/f/g/c$a;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_c

    nop

    :goto_4
    iget-object v1, p0, Lc/a/a/f/g/c$a;->f:Ljava/util/concurrent/ThreadFactory;

    goto/32 :goto_12

    nop

    :goto_5
    invoke-virtual {v0}, Lc/a/a/c/a;->c()Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_6
    iget-object v0, p0, Lc/a/a/f/g/c$a;->c:Lc/a/a/c/a;

    goto/32 :goto_5

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_8
    return-object v0

    :goto_9
    goto/32 :goto_3

    nop

    :goto_a
    if-eqz v0, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_11

    nop

    :goto_b
    sget-object v0, Lc/a/a/f/g/c;->f:Lc/a/a/f/g/c$c;

    goto/32 :goto_8

    nop

    :goto_c
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_d
    return-object v0

    :goto_e
    iget-object v1, p0, Lc/a/a/f/g/c$a;->c:Lc/a/a/c/a;

    goto/32 :goto_0

    nop

    :goto_f
    return-object v0

    :goto_10
    goto/32 :goto_13

    nop

    :goto_11
    iget-object v0, p0, Lc/a/a/f/g/c$a;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_7

    nop

    :goto_12
    invoke-direct {v0, v1}, Lc/a/a/f/g/c$c;-><init>(Ljava/util/concurrent/ThreadFactory;)V

    goto/32 :goto_e

    nop

    :goto_13
    new-instance v0, Lc/a/a/f/g/c$c;

    goto/32 :goto_4

    nop

    :goto_14
    if-nez v0, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_f

    nop
.end method

.method a(Lc/a/a/f/g/c$c;)V
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    add-long/2addr v0, v2

    goto/32 :goto_4

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    goto/32 :goto_1

    nop

    :goto_3
    invoke-static {}, Lc/a/a/f/g/c$a;->b()J

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_4
    invoke-virtual {p1, v0, v1}, Lc/a/a/f/g/c$c;->a(J)V

    goto/32 :goto_5

    nop

    :goto_5
    iget-object v0, p0, Lc/a/a/f/g/c$a;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_2

    nop

    :goto_6
    iget-wide v2, p0, Lc/a/a/f/g/c$a;->a:J

    goto/32 :goto_0

    nop
.end method

.method c()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iget-object v0, p0, Lc/a/a/f/g/c$a;->d:Ljava/util/concurrent/ScheduledExecutorService;

    goto/32 :goto_9

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lc/a/a/f/g/c$a;->c:Lc/a/a/c/a;

    goto/32 :goto_8

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_6

    nop

    :goto_4
    iget-object v0, p0, Lc/a/a/f/g/c$a;->e:Ljava/util/concurrent/Future;

    goto/32 :goto_5

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_3

    nop

    :goto_6
    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    :goto_7
    goto/32 :goto_0

    nop

    :goto_8
    invoke-virtual {v0}, Lc/a/a/c/a;->b()V

    goto/32 :goto_4

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    :goto_b
    goto/32 :goto_1

    nop
.end method

.method public run()V
    .locals 2

    iget-object v0, p0, Lc/a/a/f/g/c$a;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v1, p0, Lc/a/a/f/g/c$a;->c:Lc/a/a/c/a;

    invoke-static {v0, v1}, Lc/a/a/f/g/c$a;->a(Ljava/util/concurrent/ConcurrentLinkedQueue;Lc/a/a/c/a;)V

    return-void
.end method
