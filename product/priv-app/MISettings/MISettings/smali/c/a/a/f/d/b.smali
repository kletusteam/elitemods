.class public final Lc/a/a/f/d/b;
.super Ljava/util/concurrent/atomic/AtomicReference;

# interfaces
.implements Lc/a/a/b/j;
.implements Lc/a/a/c/b;
.implements Lc/a/a/g/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference<",
        "Lc/a/a/c/b;",
        ">;",
        "Lc/a/a/b/j<",
        "TT;>;",
        "Lc/a/a/c/b;",
        "Lc/a/a/g/a;"
    }
.end annotation


# instance fields
.field final a:Lc/a/a/e/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/d<",
            "-TT;>;"
        }
    .end annotation
.end field

.field final b:Lc/a/a/e/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/e/d<",
            "-",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lc/a/a/e/d;Lc/a/a/e/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/e/d<",
            "-TT;>;",
            "Lc/a/a/e/d<",
            "-",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object p1, p0, Lc/a/a/f/d/b;->a:Lc/a/a/e/d;

    iput-object p2, p0, Lc/a/a/f/d/b;->b:Lc/a/a/e/d;

    return-void
.end method


# virtual methods
.method public a(Lc/a/a/c/b;)V
    .locals 0

    invoke-static {p0, p1}, Lc/a/a/f/a/a;->b(Ljava/util/concurrent/atomic/AtomicReference;Lc/a/a/c/b;)Z

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    sget-object v0, Lc/a/a/f/a/a;->a:Lc/a/a/f/a/a;

    invoke-virtual {p0, v0}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lc/a/a/f/d/b;->a:Lc/a/a/e/d;

    invoke-interface {v0, p1}, Lc/a/a/e/d;->accept(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lc/a/a/d/b;->b(Ljava/lang/Throwable;)V

    invoke-static {p1}, Lc/a/a/h/a;->b(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 4

    sget-object v0, Lc/a/a/f/a/a;->a:Lc/a/a/f/a/a;

    invoke-virtual {p0, v0}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lc/a/a/f/d/b;->b:Lc/a/a/e/d;

    invoke-interface {v0, p1}, Lc/a/a/e/d;->accept(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lc/a/a/d/b;->b(Ljava/lang/Throwable;)V

    new-instance v1, Lc/a/a/d/a;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Throwable;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    aput-object v0, v2, p1

    invoke-direct {v1, v2}, Lc/a/a/d/a;-><init>([Ljava/lang/Throwable;)V

    invoke-static {v1}, Lc/a/a/h/a;->b(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public b()V
    .locals 0

    invoke-static {p0}, Lc/a/a/f/a/a;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    return-void
.end method
