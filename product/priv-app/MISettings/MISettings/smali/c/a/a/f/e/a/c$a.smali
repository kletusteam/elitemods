.class final Lc/a/a/f/e/a/c$a;
.super Lc/a/a/f/d/a;

# interfaces
.implements Lc/a/a/b/g;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc/a/a/f/e/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lc/a/a/f/d/a<",
        "TT;>;",
        "Lc/a/a/b/g<",
        "TT;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field final a:Lc/a/a/b/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/b/g<",
            "-TT;>;"
        }
    .end annotation
.end field

.field final b:Lc/a/a/b/h$b;

.field final c:Z

.field final d:I

.field e:Lc/a/a/f/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/a/a/f/c/e<",
            "TT;>;"
        }
    .end annotation
.end field

.field f:Lc/a/a/c/b;

.field g:Ljava/lang/Throwable;

.field volatile h:Z

.field volatile i:Z

.field j:I

.field k:Z


# direct methods
.method constructor <init>(Lc/a/a/b/g;Lc/a/a/b/h$b;ZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/a/a/b/g<",
            "-TT;>;",
            "Lc/a/a/b/h$b;",
            "ZI)V"
        }
    .end annotation

    invoke-direct {p0}, Lc/a/a/f/d/a;-><init>()V

    iput-object p1, p0, Lc/a/a/f/e/a/c$a;->a:Lc/a/a/b/g;

    iput-object p2, p0, Lc/a/a/f/e/a/c$a;->b:Lc/a/a/b/h$b;

    iput-boolean p3, p0, Lc/a/a/f/e/a/c$a;->c:Z

    iput p4, p0, Lc/a/a/f/e/a/c$a;->d:I

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    const/4 v0, 0x2

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lc/a/a/f/e/a/c$a;->k:Z

    return v0

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public a(Lc/a/a/c/b;)V
    .locals 2

    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->f:Lc/a/a/c/b;

    invoke-static {v0, p1}, Lc/a/a/f/a/a;->a(Lc/a/a/c/b;Lc/a/a/c/b;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object p1, p0, Lc/a/a/f/e/a/c$a;->f:Lc/a/a/c/b;

    instance-of v0, p1, Lc/a/a/f/c/b;

    if-eqz v0, :cond_1

    check-cast p1, Lc/a/a/f/c/b;

    const/4 v0, 0x7

    invoke-interface {p1, v0}, Lc/a/a/f/c/c;->a(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iput v0, p0, Lc/a/a/f/e/a/c$a;->j:I

    iput-object p1, p0, Lc/a/a/f/e/a/c$a;->e:Lc/a/a/f/c/e;

    iput-boolean v1, p0, Lc/a/a/f/e/a/c$a;->h:Z

    iget-object p1, p0, Lc/a/a/f/e/a/c$a;->a:Lc/a/a/b/g;

    invoke-interface {p1, p0}, Lc/a/a/b/g;->a(Lc/a/a/c/b;)V

    invoke-virtual {p0}, Lc/a/a/f/e/a/c$a;->f()V

    return-void

    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iput v0, p0, Lc/a/a/f/e/a/c$a;->j:I

    iput-object p1, p0, Lc/a/a/f/e/a/c$a;->e:Lc/a/a/f/c/e;

    iget-object p1, p0, Lc/a/a/f/e/a/c$a;->a:Lc/a/a/b/g;

    invoke-interface {p1, p0}, Lc/a/a/b/g;->a(Lc/a/a/c/b;)V

    return-void

    :cond_1
    new-instance p1, Lc/a/a/f/f/a;

    iget v0, p0, Lc/a/a/f/e/a/c$a;->d:I

    invoke-direct {p1, v0}, Lc/a/a/f/f/a;-><init>(I)V

    iput-object p1, p0, Lc/a/a/f/e/a/c$a;->e:Lc/a/a/f/c/e;

    iget-object p1, p0, Lc/a/a/f/e/a/c$a;->a:Lc/a/a/b/g;

    invoke-interface {p1, p0}, Lc/a/a/b/g;->a(Lc/a/a/c/b;)V

    :cond_2
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lc/a/a/f/e/a/c$a;->h:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lc/a/a/f/e/a/c$a;->j:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->e:Lc/a/a/f/c/e;

    invoke-interface {v0, p1}, Lc/a/a/f/c/e;->offer(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Lc/a/a/f/e/a/c$a;->f()V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    iget-boolean v0, p0, Lc/a/a/f/e/a/c$a;->h:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lc/a/a/h/a;->b(Ljava/lang/Throwable;)V

    return-void

    :cond_0
    iput-object p1, p0, Lc/a/a/f/e/a/c$a;->g:Ljava/lang/Throwable;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lc/a/a/f/e/a/c$a;->h:Z

    invoke-virtual {p0}, Lc/a/a/f/e/a/c$a;->f()V

    return-void
.end method

.method a(ZZLc/a/a/b/g;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lc/a/a/b/g<",
            "-TT;>;)Z"
        }
    .end annotation

    goto/32 :goto_1d

    nop

    :goto_0
    iget-object p2, p0, Lc/a/a/f/e/a/c$a;->e:Lc/a/a/f/c/e;

    goto/32 :goto_e

    nop

    :goto_1
    iget-object p1, p0, Lc/a/a/f/e/a/c$a;->g:Ljava/lang/Throwable;

    goto/32 :goto_4

    nop

    :goto_2
    return v1

    :goto_3
    goto/32 :goto_14

    nop

    :goto_4
    iget-boolean v0, p0, Lc/a/a/f/e/a/c$a;->c:Z

    goto/32 :goto_11

    nop

    :goto_5
    iput-boolean v1, p0, Lc/a/a/f/e/a/c$a;->i:Z

    goto/32 :goto_9

    nop

    :goto_6
    if-nez p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_1

    nop

    :goto_7
    iget-object p1, p0, Lc/a/a/f/e/a/c$a;->b:Lc/a/a/b/h$b;

    goto/32 :goto_b

    nop

    :goto_8
    iget-object p1, p0, Lc/a/a/f/e/a/c$a;->b:Lc/a/a/b/h$b;

    goto/32 :goto_1b

    nop

    :goto_9
    if-nez p1, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_26

    nop

    :goto_a
    if-nez p1, :cond_2

    goto/32 :goto_18

    :cond_2
    goto/32 :goto_c

    nop

    :goto_b
    invoke-interface {p1}, Lc/a/a/c/b;->b()V

    goto/32 :goto_2

    nop

    :goto_c
    iput-boolean v1, p0, Lc/a/a/f/e/a/c$a;->i:Z

    goto/32 :goto_0

    nop

    :goto_d
    iget-object p1, p0, Lc/a/a/f/e/a/c$a;->e:Lc/a/a/f/c/e;

    goto/32 :goto_f

    nop

    :goto_e
    invoke-interface {p2}, Lc/a/a/f/c/e;->clear()V

    goto/32 :goto_10

    nop

    :goto_f
    invoke-interface {p1}, Lc/a/a/f/c/e;->clear()V

    goto/32 :goto_12

    nop

    :goto_10
    invoke-interface {p3, p1}, Lc/a/a/b/g;->a(Ljava/lang/Throwable;)V

    goto/32 :goto_1c

    nop

    :goto_11
    if-nez v0, :cond_3

    goto/32 :goto_24

    :cond_3
    goto/32 :goto_27

    nop

    :goto_12
    return v1

    :goto_13
    goto/32 :goto_6

    nop

    :goto_14
    const/4 p1, 0x0

    goto/32 :goto_28

    nop

    :goto_15
    if-nez v0, :cond_4

    goto/32 :goto_13

    :cond_4
    goto/32 :goto_d

    nop

    :goto_16
    const/4 v1, 0x1

    goto/32 :goto_15

    nop

    :goto_17
    return v1

    :goto_18
    goto/32 :goto_21

    nop

    :goto_19
    goto :goto_1f

    :goto_1a
    goto/32 :goto_1e

    nop

    :goto_1b
    invoke-interface {p1}, Lc/a/a/c/b;->b()V

    goto/32 :goto_23

    nop

    :goto_1c
    iget-object p1, p0, Lc/a/a/f/e/a/c$a;->b:Lc/a/a/b/h$b;

    goto/32 :goto_20

    nop

    :goto_1d
    iget-boolean v0, p0, Lc/a/a/f/e/a/c$a;->i:Z

    goto/32 :goto_16

    nop

    :goto_1e
    invoke-interface {p3}, Lc/a/a/b/g;->c()V

    :goto_1f
    goto/32 :goto_8

    nop

    :goto_20
    invoke-interface {p1}, Lc/a/a/c/b;->b()V

    goto/32 :goto_17

    nop

    :goto_21
    if-nez p2, :cond_5

    goto/32 :goto_3

    :cond_5
    goto/32 :goto_25

    nop

    :goto_22
    invoke-interface {p3}, Lc/a/a/b/g;->c()V

    goto/32 :goto_7

    nop

    :goto_23
    return v1

    :goto_24
    goto/32 :goto_a

    nop

    :goto_25
    iput-boolean v1, p0, Lc/a/a/f/e/a/c$a;->i:Z

    goto/32 :goto_22

    nop

    :goto_26
    invoke-interface {p3, p1}, Lc/a/a/b/g;->a(Ljava/lang/Throwable;)V

    goto/32 :goto_19

    nop

    :goto_27
    if-nez p2, :cond_6

    goto/32 :goto_3

    :cond_6
    goto/32 :goto_5

    nop

    :goto_28
    return p1
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lc/a/a/f/e/a/c$a;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lc/a/a/f/e/a/c$a;->i:Z

    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->f:Lc/a/a/c/b;

    invoke-interface {v0}, Lc/a/a/c/b;->b()V

    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->b:Lc/a/a/b/h$b;

    invoke-interface {v0}, Lc/a/a/c/b;->b()V

    iget-boolean v0, p0, Lc/a/a/f/e/a/c$a;->k:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->e:Lc/a/a/f/c/e;

    invoke-interface {v0}, Lc/a/a/f/c/e;->clear()V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    iget-boolean v0, p0, Lc/a/a/f/e/a/c$a;->h:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lc/a/a/f/e/a/c$a;->h:Z

    invoke-virtual {p0}, Lc/a/a/f/e/a/c$a;->f()V

    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->e:Lc/a/a/f/c/e;

    invoke-interface {v0}, Lc/a/a/f/c/e;->clear()V

    return-void
.end method

.method d()V
    .locals 5

    goto/32 :goto_20

    nop

    :goto_0
    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->a:Lc/a/a/b/g;

    goto/32 :goto_2

    nop

    :goto_1
    if-nez v2, :cond_0

    goto/32 :goto_25

    :cond_0
    goto/32 :goto_9

    nop

    :goto_2
    invoke-interface {v0, v3}, Lc/a/a/b/g;->a(Ljava/lang/Throwable;)V

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {p0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v1

    goto/32 :goto_6

    nop

    :goto_4
    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->b:Lc/a/a/b/h$b;

    goto/32 :goto_b

    nop

    :goto_5
    iget-boolean v2, p0, Lc/a/a/f/e/a/c$a;->h:Z

    goto/32 :goto_1b

    nop

    :goto_6
    if-eqz v1, :cond_1

    goto/32 :goto_15

    :cond_1
    goto/32 :goto_28

    nop

    :goto_7
    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->b:Lc/a/a/b/h$b;

    goto/32 :goto_21

    nop

    :goto_8
    if-nez v2, :cond_2

    goto/32 :goto_1d

    :cond_2
    goto/32 :goto_26

    nop

    :goto_9
    if-nez v3, :cond_3

    goto/32 :goto_25

    :cond_3
    goto/32 :goto_27

    nop

    :goto_a
    neg-int v1, v1

    goto/32 :goto_3

    nop

    :goto_b
    invoke-interface {v0}, Lc/a/a/c/b;->b()V

    goto/32 :goto_1c

    nop

    :goto_c
    goto :goto_10

    :goto_d
    goto/32 :goto_18

    nop

    :goto_e
    invoke-interface {v3, v4}, Lc/a/a/b/g;->a(Ljava/lang/Object;)V

    goto/32 :goto_8

    nop

    :goto_f
    invoke-interface {v0}, Lc/a/a/b/g;->c()V

    :goto_10
    goto/32 :goto_4

    nop

    :goto_11
    iget-object v1, p0, Lc/a/a/f/e/a/c$a;->a:Lc/a/a/b/g;

    goto/32 :goto_1f

    nop

    :goto_12
    if-eqz v4, :cond_4

    goto/32 :goto_25

    :cond_4
    goto/32 :goto_1

    nop

    :goto_13
    iget-boolean v4, p0, Lc/a/a/f/e/a/c$a;->c:Z

    goto/32 :goto_12

    nop

    :goto_14
    move v1, v0

    :goto_15
    goto/32 :goto_29

    nop

    :goto_16
    iget-object v3, p0, Lc/a/a/f/e/a/c$a;->a:Lc/a/a/b/g;

    goto/32 :goto_1e

    nop

    :goto_17
    if-nez v0, :cond_5

    goto/32 :goto_d

    :cond_5
    goto/32 :goto_11

    nop

    :goto_18
    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->a:Lc/a/a/b/g;

    goto/32 :goto_f

    nop

    :goto_19
    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->g:Ljava/lang/Throwable;

    goto/32 :goto_17

    nop

    :goto_1a
    if-nez v2, :cond_6

    goto/32 :goto_23

    :cond_6
    goto/32 :goto_22

    nop

    :goto_1b
    iget-object v3, p0, Lc/a/a/f/e/a/c$a;->g:Ljava/lang/Throwable;

    goto/32 :goto_13

    nop

    :goto_1c
    return-void

    :goto_1d
    goto/32 :goto_a

    nop

    :goto_1e
    const/4 v4, 0x0

    goto/32 :goto_e

    nop

    :goto_1f
    invoke-interface {v1, v0}, Lc/a/a/b/g;->a(Ljava/lang/Throwable;)V

    goto/32 :goto_c

    nop

    :goto_20
    const/4 v0, 0x1

    goto/32 :goto_14

    nop

    :goto_21
    invoke-interface {v0}, Lc/a/a/c/b;->b()V

    goto/32 :goto_24

    nop

    :goto_22
    return-void

    :goto_23
    goto/32 :goto_5

    nop

    :goto_24
    return-void

    :goto_25
    goto/32 :goto_16

    nop

    :goto_26
    iput-boolean v0, p0, Lc/a/a/f/e/a/c$a;->i:Z

    goto/32 :goto_19

    nop

    :goto_27
    iput-boolean v0, p0, Lc/a/a/f/e/a/c$a;->i:Z

    goto/32 :goto_0

    nop

    :goto_28
    return-void

    :goto_29
    iget-boolean v2, p0, Lc/a/a/f/e/a/c$a;->i:Z

    goto/32 :goto_1a

    nop
.end method

.method e()V
    .locals 7

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0}, Lc/a/a/f/c/e;->isEmpty()Z

    move-result v5

    goto/32 :goto_f

    nop

    :goto_1
    invoke-virtual {p0, v4, v6, v1}, Lc/a/a/f/e/a/c$a;->a(ZZLc/a/a/b/g;)Z

    move-result v4

    goto/32 :goto_14

    nop

    :goto_2
    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->e:Lc/a/a/f/c/e;

    goto/32 :goto_7

    nop

    :goto_3
    const/4 v6, 0x0

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    invoke-static {v3}, Lc/a/a/d/b;->b(Ljava/lang/Throwable;)V

    goto/32 :goto_1f

    nop

    :goto_6
    iget-object v2, p0, Lc/a/a/f/e/a/c$a;->f:Lc/a/a/c/b;

    goto/32 :goto_18

    nop

    :goto_7
    iget-object v1, p0, Lc/a/a/f/e/a/c$a;->a:Lc/a/a/b/g;

    goto/32 :goto_a

    nop

    :goto_8
    if-eqz v3, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_23

    nop

    :goto_9
    neg-int v3, v3

    goto/32 :goto_21

    nop

    :goto_a
    const/4 v2, 0x1

    goto/32 :goto_16

    nop

    :goto_b
    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->b:Lc/a/a/b/h$b;

    goto/32 :goto_26

    nop

    :goto_c
    iget-boolean v4, p0, Lc/a/a/f/e/a/c$a;->h:Z

    goto/32 :goto_0

    nop

    :goto_d
    if-nez v4, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_12

    nop

    :goto_e
    if-eqz v5, :cond_2

    goto/32 :goto_1d

    :cond_2
    goto/32 :goto_19

    nop

    :goto_f
    invoke-virtual {p0, v4, v5, v1}, Lc/a/a/f/e/a/c$a;->a(ZZLc/a/a/b/g;)Z

    move-result v4

    goto/32 :goto_d

    nop

    :goto_10
    iget-boolean v4, p0, Lc/a/a/f/e/a/c$a;->h:Z

    :try_start_0
    invoke-interface {v0}, Lc/a/a/f/c/e;->poll()Ljava/lang/Object;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_e

    nop

    :goto_11
    goto :goto_13

    :catch_0
    move-exception v3

    goto/32 :goto_5

    nop

    :goto_12
    return-void

    :goto_13
    goto/32 :goto_10

    nop

    :goto_14
    if-nez v4, :cond_3

    goto/32 :goto_1b

    :cond_3
    goto/32 :goto_1a

    nop

    :goto_15
    invoke-interface {v1, v3}, Lc/a/a/b/g;->a(Ljava/lang/Throwable;)V

    goto/32 :goto_b

    nop

    :goto_16
    move v3, v2

    :goto_17
    goto/32 :goto_c

    nop

    :goto_18
    invoke-interface {v2}, Lc/a/a/c/b;->b()V

    goto/32 :goto_22

    nop

    :goto_19
    move v6, v2

    goto/32 :goto_1c

    nop

    :goto_1a
    return-void

    :goto_1b
    goto/32 :goto_20

    nop

    :goto_1c
    goto/16 :goto_4

    :goto_1d
    goto/32 :goto_3

    nop

    :goto_1e
    return-void

    :goto_1f
    iput-boolean v2, p0, Lc/a/a/f/e/a/c$a;->i:Z

    goto/32 :goto_6

    nop

    :goto_20
    if-nez v6, :cond_4

    goto/32 :goto_24

    :cond_4
    goto/32 :goto_9

    nop

    :goto_21
    invoke-virtual {p0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v3

    goto/32 :goto_8

    nop

    :goto_22
    invoke-interface {v0}, Lc/a/a/f/c/e;->clear()V

    goto/32 :goto_15

    nop

    :goto_23
    return-void

    :goto_24
    goto/32 :goto_25

    nop

    :goto_25
    invoke-interface {v1, v5}, Lc/a/a/b/g;->a(Ljava/lang/Object;)V

    goto/32 :goto_11

    nop

    :goto_26
    invoke-interface {v0}, Lc/a/a/c/b;->b()V

    goto/32 :goto_1e

    nop
.end method

.method f()V
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0, p0}, Lc/a/a/b/h$b;->b(Ljava/lang/Runnable;)Lc/a/a/c/b;

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_4

    nop

    :goto_4
    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->b:Lc/a/a/b/h$b;

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    goto/32 :goto_3

    nop
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->e:Lc/a/a/f/c/e;

    invoke-interface {v0}, Lc/a/a/f/c/e;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public poll()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .annotation build Lio/reactivex/rxjava3/annotations/Nullable;
    .end annotation

    iget-object v0, p0, Lc/a/a/f/e/a/c$a;->e:Lc/a/a/f/c/e;

    invoke-interface {v0}, Lc/a/a/f/c/e;->poll()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 1

    iget-boolean v0, p0, Lc/a/a/f/e/a/c$a;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lc/a/a/f/e/a/c$a;->d()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lc/a/a/f/e/a/c$a;->e()V

    :goto_0
    return-void
.end method
