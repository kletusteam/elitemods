.class public final Lc/a/a/i/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc/a/a/i/b$b;,
        Lc/a/a/i/b$h;,
        Lc/a/a/i/b$f;,
        Lc/a/a/i/b$c;,
        Lc/a/a/i/b$e;,
        Lc/a/a/i/b$d;,
        Lc/a/a/i/b$a;,
        Lc/a/a/i/b$g;
    }
.end annotation


# static fields
.field static final a:Lc/a/a/b/h;
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation
.end field

.field static final b:Lc/a/a/b/h;
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation
.end field

.field static final c:Lc/a/a/b/h;
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation
.end field

.field static final d:Lc/a/a/b/h;
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation
.end field

.field static final e:Lc/a/a/b/h;
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lc/a/a/i/b$h;

    invoke-direct {v0}, Lc/a/a/i/b$h;-><init>()V

    invoke-static {v0}, Lc/a/a/h/a;->e(Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object v0

    sput-object v0, Lc/a/a/i/b;->a:Lc/a/a/b/h;

    new-instance v0, Lc/a/a/i/b$b;

    invoke-direct {v0}, Lc/a/a/i/b$b;-><init>()V

    invoke-static {v0}, Lc/a/a/h/a;->b(Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object v0

    sput-object v0, Lc/a/a/i/b;->b:Lc/a/a/b/h;

    new-instance v0, Lc/a/a/i/b$c;

    invoke-direct {v0}, Lc/a/a/i/b$c;-><init>()V

    invoke-static {v0}, Lc/a/a/h/a;->c(Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object v0

    sput-object v0, Lc/a/a/i/b;->c:Lc/a/a/b/h;

    invoke-static {}, Lc/a/a/f/g/m;->b()Lc/a/a/f/g/m;

    move-result-object v0

    sput-object v0, Lc/a/a/i/b;->d:Lc/a/a/b/h;

    new-instance v0, Lc/a/a/i/b$f;

    invoke-direct {v0}, Lc/a/a/i/b$f;-><init>()V

    invoke-static {v0}, Lc/a/a/h/a;->d(Lc/a/a/e/h;)Lc/a/a/b/h;

    move-result-object v0

    sput-object v0, Lc/a/a/i/b;->e:Lc/a/a/b/h;

    return-void
.end method

.method public static a()Lc/a/a/b/h;
    .locals 1
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    sget-object v0, Lc/a/a/i/b;->b:Lc/a/a/b/h;

    invoke-static {v0}, Lc/a/a/h/a;->a(Lc/a/a/b/h;)Lc/a/a/b/h;

    move-result-object v0

    return-object v0
.end method

.method public static b()Lc/a/a/b/h;
    .locals 1
    .annotation build Lio/reactivex/rxjava3/annotations/NonNull;
    .end annotation

    sget-object v0, Lc/a/a/i/b;->c:Lc/a/a/b/h;

    invoke-static {v0}, Lc/a/a/h/a;->b(Lc/a/a/b/h;)Lc/a/a/b/h;

    move-result-object v0

    return-object v0
.end method
