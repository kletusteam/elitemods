.class Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->updateFollowItem()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->access$000(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v0, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowItem:Landroid/view/View;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiui/util/SimRingtoneUtils;->isDefaultSoundUniform(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v0, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->clearSelectView()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->access$700(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)Lcom/android/settings/coolsound/MediaPlayerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settings/coolsound/MediaPlayerListener;->stopIntelligentSound()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v0, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowItem:Landroid/view/View;

    const v2, 0x1020010

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x40

    invoke-static {v2, v3}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundSettingUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2, v1}, Landroid/media/ExtraRingtone;->getRingtoneTitle(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method
