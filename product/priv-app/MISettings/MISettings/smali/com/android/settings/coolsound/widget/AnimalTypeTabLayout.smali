.class public Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;
.super Landroid/widget/HorizontalScrollView;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout$OnTabItemClickListener;
    }
.end annotation


# instance fields
.field private mContainer:Landroid/widget/LinearLayout;

.field private mCurrentTab:I

.field private mDefaultTabType:Ljava/lang/String;

.field private mListener:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout$OnTabItemClickListener;

.field private mSpaceSize:I

.field private final mTabs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/coolsound/data/MixMatchTab;",
            ">;"
        }
    .end annotation
.end field

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mTabs:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mTabs:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mTabs:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mTabs:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->initView()V

    return-void
.end method

.method static synthetic a(Landroid/view/View;I)V
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v1, p1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private getTabView(Ljava/lang/String;Z)Landroid/view/View;
    .locals 4

    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const v1, 0x4182e148    # 16.36f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06005e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getColorStateList(ILandroid/content/res/Resources$Theme;)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setSelected(Z)V

    return-object v0
.end method

.method private initView()V
    .locals 5

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mContainer:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f07014e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mSpaceSize:I

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mContainer:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v2}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v1}, Landroid/widget/HorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/HorizontalScrollView;->setSmoothScrollingEnabled(Z)V

    new-instance v0, Lcom/android/settings/coolsound/widget/a;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/widget/a;-><init>(Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;)V

    invoke-virtual {p0, v0}, Landroid/widget/HorizontalScrollView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public synthetic a()V
    .locals 1

    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mWidth:I

    return-void
.end method

.method public addTab(Lcom/android/settings/coolsound/data/MixMatchTab;)V
    .locals 5

    iget-object v0, p1, Lcom/android/settings/coolsound/data/MixMatchTab;->tabName:Ljava/lang/String;

    iget-boolean v1, p1, Lcom/android/settings/coolsound/data/MixMatchTab;->isSelected:Z

    invoke-direct {p0, v0, v1}, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->getTabView(Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    if-nez v1, :cond_0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    :cond_0
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07014f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070174

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iget-object v4, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p1, Lcom/android/settings/coolsound/data/MixMatchTab;->tabTypeName:Ljava/lang/String;

    iput-object v4, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mDefaultTabType:Ljava/lang/String;

    iget v4, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mSpaceSize:I

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/android/settings/coolsound/widget/b;

    invoke-direct {v1, v0, v3}, Lcom/android/settings/coolsound/widget/b;-><init>(Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getCurrentTabPosition()I
    .locals 1

    iget v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mCurrentTab:I

    return v0
.end method

.method public getDefaultTabType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mDefaultTabType:Ljava/lang/String;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result p1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mListener:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout$OnTabItemClickListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout$OnTabItemClickListener;->onTabItemClicked(I)V

    :cond_0
    return-void
.end method

.method public registerListener(Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout$OnTabItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mListener:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout$OnTabItemClickListener;

    return-void
.end method

.method public setSelect(I)V
    .locals 8

    iput p1, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mCurrentTab:I

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchTab;

    iget-object v0, v0, Lcom/android/settings/coolsound/data/MixMatchTab;->tabTypeName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mDefaultTabType:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x2

    new-array v2, v1, [I

    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationInWindow([I)V

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v3, :cond_1

    iget-object v6, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-ne p1, v5, :cond_0

    const/4 v7, 0x1

    goto :goto_1

    :cond_0
    move v7, v4

    :goto_1
    invoke-virtual {v6, v7}, Landroid/view/View;->setSelected(Z)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    aget p1, v2, v4

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/2addr v0, v1

    add-int/2addr p1, v0

    iget v0, p0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->mWidth:I

    div-int/2addr v0, v1

    sub-int/2addr p1, v0

    invoke-virtual {p0, p1, v4}, Landroid/widget/HorizontalScrollView;->smoothScrollBy(II)V

    return-void
.end method
