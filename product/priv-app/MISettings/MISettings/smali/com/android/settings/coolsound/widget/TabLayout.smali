.class public Lcom/android/settings/coolsound/widget/TabLayout;
.super Lmiuix/miuixbasewidget/widget/FilterSortView;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final INDEX_LEFT:I = 0x0

.field private static final INDEX_RIGHT:I = 0x1

.field private static final MSG_UPDATE_RINGTONE_TAB1:I = 0xb

.field private static final MSG_UPDATE_RINGTONE_TAB2:I = 0xc


# instance fields
.field private indexLeft:I

.field private isUpdateFiltered:Z

.field private final mHandler:Landroid/os/Handler;

.field mTab1:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

.field mTab2:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

.field private final mUpdateUIRunnable:Ljava/lang/Runnable;

.field mViewPager:Landroidx/viewpager/widget/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lmiuix/miuixbasewidget/widget/FilterSortView;-><init>(Landroid/content/Context;)V

    new-instance p1, Lcom/android/settings/coolsound/widget/TabLayout$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, p0, v0}, Lcom/android/settings/coolsound/widget/TabLayout$1;-><init>(Lcom/android/settings/coolsound/widget/TabLayout;Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mHandler:Landroid/os/Handler;

    new-instance p1, Lcom/android/settings/coolsound/widget/TabLayout$2;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/TabLayout$2;-><init>(Lcom/android/settings/coolsound/widget/TabLayout;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mUpdateUIRunnable:Ljava/lang/Runnable;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->isUpdateFiltered:Z

    iput p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->indexLeft:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/miuixbasewidget/widget/FilterSortView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Lcom/android/settings/coolsound/widget/TabLayout$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/android/settings/coolsound/widget/TabLayout$1;-><init>(Lcom/android/settings/coolsound/widget/TabLayout;Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mHandler:Landroid/os/Handler;

    new-instance p1, Lcom/android/settings/coolsound/widget/TabLayout$2;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/TabLayout$2;-><init>(Lcom/android/settings/coolsound/widget/TabLayout;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mUpdateUIRunnable:Ljava/lang/Runnable;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->isUpdateFiltered:Z

    iput p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->indexLeft:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiuix/miuixbasewidget/widget/FilterSortView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Lcom/android/settings/coolsound/widget/TabLayout$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/android/settings/coolsound/widget/TabLayout$1;-><init>(Lcom/android/settings/coolsound/widget/TabLayout;Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mHandler:Landroid/os/Handler;

    new-instance p1, Lcom/android/settings/coolsound/widget/TabLayout$2;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/TabLayout$2;-><init>(Lcom/android/settings/coolsound/widget/TabLayout;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mUpdateUIRunnable:Ljava/lang/Runnable;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->isUpdateFiltered:Z

    iput p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->indexLeft:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiuix/miuixbasewidget/widget/FilterSortView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Lcom/android/settings/coolsound/widget/TabLayout$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/android/settings/coolsound/widget/TabLayout$1;-><init>(Lcom/android/settings/coolsound/widget/TabLayout;Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mHandler:Landroid/os/Handler;

    new-instance p1, Lcom/android/settings/coolsound/widget/TabLayout$2;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/TabLayout$2;-><init>(Lcom/android/settings/coolsound/widget/TabLayout;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mUpdateUIRunnable:Ljava/lang/Runnable;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->isUpdateFiltered:Z

    iput p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->indexLeft:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/coolsound/widget/TabLayout;Ljava/lang/String;Ljava/lang/Object;)Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/coolsound/widget/TabLayout;->addTab(Ljava/lang/String;Ljava/lang/Object;)Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/android/settings/coolsound/widget/TabLayout;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/TabLayout;->getRingtoneTabTitle(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/android/settings/coolsound/widget/TabLayout;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method private addTab(Ljava/lang/String;Ljava/lang/Object;)Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/miuixbasewidget/widget/FilterSortView;->addTab(Ljava/lang/CharSequence;)Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;->setIndicatorVisibility(I)V

    invoke-virtual {p1, p0}, Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method private getRingtoneTabTitle(I)Ljava/lang/String;
    .locals 1

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private updateSelected(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mTab1:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lmiuix/miuixbasewidget/widget/FilterSortView;->setFilteredTab(Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mTab2:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lmiuix/miuixbasewidget/widget/FilterSortView;->setFilteredTab(Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/TabLayout;->updateSelected(I)V

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "parent onLayout"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->isUpdateFiltered:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "miuix:FilterSortView"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/TabLayout;->isUpdateFiltered:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move p1, v2

    goto :goto_1

    :cond_1
    :goto_0
    move p1, v3

    :goto_1
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :try_start_0
    const-class v5, Lmiuix/miuixbasewidget/widget/FilterSortView;

    const-string v6, "mFilteredId"

    invoke-static {p0, v5, v6}, Lb/c/a/b/b;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, v0, :cond_3

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "indexLeft onLayout"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v7, p0, Lcom/android/settings/coolsound/widget/TabLayout;->indexLeft:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, "__"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->indexLeft:I

    if-eq v1, v4, :cond_2

    const-class v1, Lmiuix/miuixbasewidget/widget/FilterSortView;

    const-string v6, "mFilteredUpdated"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p0, v1, v6, v2}, Lb/c/a/b/b;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2
    iput v4, p0, Lcom/android/settings/coolsound/widget/TabLayout;->indexLeft:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    move-object v5, v4

    :goto_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    :goto_3
    move-object v1, v5

    move-object v4, p0

    move v5, p1

    move v6, p2

    move v7, p3

    move v8, p4

    move v9, p5

    invoke-super/range {v4 .. v9}, Lmiuix/miuixbasewidget/widget/FilterSortView;->onLayout(ZIIII)V

    iget-boolean p2, p0, Lcom/android/settings/coolsound/widget/TabLayout;->isUpdateFiltered:Z

    if-eqz p2, :cond_4

    return-void

    :cond_4
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result p2

    if-eq p2, v0, :cond_5

    if-eqz p1, :cond_5

    iput-boolean v3, p0, Lcom/android/settings/coolsound/widget/TabLayout;->isUpdateFiltered:Z

    :cond_5
    return-void
.end method

.method public onPageSelected(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/TabLayout;->updateSelected(I)V

    return-void
.end method

.method public setViewPager(Landroidx/viewpager/widget/ViewPager;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/TabLayout;->updateSelected(I)V

    new-instance p1, Ljava/lang/Thread;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/TabLayout;->mUpdateUIRunnable:Ljava/lang/Runnable;

    invoke-direct {p1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    return-void
.end method
