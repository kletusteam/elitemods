.class Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$9;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$9;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$9;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    iget-object v1, v0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result p1

    iput p1, v0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->position:I

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$9;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {p1}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$100(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/coolsound/CoolCommonUtils;->isZenMode(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$9;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {p1}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$100(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$9;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {v0}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$100(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1302b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/misettings/common/utils/r;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    :cond_0
    return-void
.end method
