.class Lcom/android/settings/coolsound/widget/RingtoneGridView$8;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/widget/RingtoneGridView;->saveClassicRingtoneUri()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$8;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$8;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$800(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$8;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1200(Lcom/android/settings/coolsound/widget/RingtoneGridView;I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$8;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$800(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$8;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$500(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$8;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v2}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$8;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v3}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v3

    invoke-static {v2, v3}, Lcom/android/settings/coolsound/CoolCommonUtils;->getMoreRingtone(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/widget/BaseItem;->setNormal(Z)V

    :cond_1
    return-void
.end method
