.class public Lcom/android/settings/coolsound/MixMatchAnimalAnimator;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/settings/coolsound/widget/MixMatchAnimalView$AnimalSelectListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_SCALE:F = 1.15f

.field private static final PKG_NAME:Ljava/lang/String; = "com.xiaomi.misettings"

.field private static final SCALE_FIVE_ANIMAL:F = 0.57f

.field private static final SCALE_FOUR_ANIMAL:F = 0.57f

.field private static final SCALE_ONE_ANIMAL:F = 1.0f

.field private static final SCALE_THREE_ANIMAL:F = 0.71f

.field private static final SCALE_TWO_ANIMAL:F = 0.78f


# instance fields
.field private final ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

.field private final ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

.field private final ANIMAL_POSITION_ONE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

.field private final ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

.field private final ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

.field private final mAnchorPosition:[I

.field private mAnchorView:Landroid/view/View;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mListener:Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;

.field private mOriginalSize:I

.field private mRootView:Landroid/view/ViewGroup;

.field public final mViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/coolsound/widget/MixMatchAnimalView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_ONE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const/4 v0, 0x2

    new-array v1, v0, [Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    iput-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    iput-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    iput-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    iput-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mAnchorPosition:[I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->initDefaultPosition()V

    return-void
.end method

.method static synthetic a(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Landroid/animation/ValueAnimator;)V
    .locals 1

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0, v0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->addFive(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;)Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;

    return-object p0
.end method

.method static synthetic access$200(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;)[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    return-object p0
.end method

.method static synthetic access$300(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;)Landroid/view/ViewGroup;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mRootView:Landroid/view/ViewGroup;

    return-object p0
.end method

.method private addFive(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v2, v2, v1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    invoke-virtual {v2, v3}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->setAnimalEntry(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    invoke-direct {p0, v2}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->fly(Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    invoke-interface {v2, v3}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;->onRandom(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$2;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$2;-><init>(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;)V

    const-wide/16 v1, 0x258

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private addInitialAnimal([Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    invoke-direct {p0, v2, v3}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getInitialAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mRootView:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {v2}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->startShaking()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private calcAnchorPosition(Landroid/view/View;)[I
    .locals 4

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mAnchorPosition:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v3, 0x1

    if-lez v2, :cond_0

    aget v2, v0, v3

    if-lez v2, :cond_0

    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mAnchorPosition:[I

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mAnchorPosition:[I

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result p1

    float-to-int p1, p1

    aput p1, v0, v3

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mAnchorPosition:[I

    return-object p1

    :cond_1
    const/4 p1, 0x2

    new-array p1, p1, [I

    return-object p1
.end method

.method private fillOther(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_3

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_ONE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v3, v3, v2

    goto :goto_1

    :cond_0
    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    iget-object v3, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v3, v3, v2

    goto :goto_1

    :cond_1
    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    iget-object v3, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v3, v3, v2

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v3, v3, v2

    :goto_1
    iget-object v4, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v5, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$3;

    invoke-direct {v6, p0, v4, v3, v2}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$3;-><init>(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;I)V

    const-wide/16 v3, 0x64

    invoke-virtual {v5, v6, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_5

    add-int v3, v1, v0

    iget-object v4, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    array-length v5, v4

    if-le v3, v5, :cond_4

    goto :goto_3

    :cond_4
    aget-object v3, v4, v3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    invoke-virtual {v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->setAnimalEntry(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    invoke-direct {p0, v3}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->fly(Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/animation/Animator;->start()V

    iget-object v3, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    invoke-interface {v3, v4}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;->onRandom(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    :goto_3
    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$4;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$4;-><init>(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;)V

    const-wide/16 v1, 0x258

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private findAnimalView(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)Lcom/android/settings/coolsound/widget/MixMatchAnimalView;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    invoke-virtual {v1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->getEntry()Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object v2

    if-ne v2, p1, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private fly(Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;
    .locals 14

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getScale()F

    move-result v0

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getAnimalEntry()Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getAnimal(FLcom/android/settings/coolsound/data/MixMatchAnimalItem;)Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mRootView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionX()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getScale()F

    move-result v4

    invoke-virtual {v0, v3, v2, v4}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setTarget(FFF)V

    const/4 v2, 0x2

    new-array v4, v2, [F

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getX()F

    move-result v5

    const/4 v6, 0x0

    aput v5, v4, v6

    const/4 v5, 0x1

    aput v3, v4, v5

    const-string v3, "x"

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-wide/16 v7, 0x1f4

    invoke-virtual {v3, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v4, Lmiuix/view/a/f;

    invoke-direct {v4}, Lmiuix/view/a/f;-><init>()V

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v4, Lmiuix/animation/b/a;

    const-string v9, "y"

    invoke-direct {v4, v9}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v9, Lmiuix/animation/g/A;->k:Lmiuix/animation/g/A;

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionY()I

    move-result p1

    int-to-double v10, p1

    invoke-virtual {v4, v9, v10, v11}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-instance p1, Lmiuix/animation/a/a;

    invoke-direct {p1}, Lmiuix/animation/a/a;-><init>()V

    new-array v9, v5, [Lmiuix/animation/e/b;

    new-instance v10, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$6;

    invoke-direct {v10, p0, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$6;-><init>(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    aput-object v10, v9, v6

    invoke-virtual {p1, v9}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    new-array v9, v2, [F

    const v10, 0x3f333333    # 0.7f

    aput v10, v9, v6

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v10

    const-wide v12, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v10, v12

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    add-double/2addr v10, v12

    double-to-float v10, v10

    aput v10, v9, v5

    const/4 v10, -0x2

    invoke-virtual {p1, v10, v9}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    new-array v9, v5, [Landroid/view/View;

    aput-object v0, v9, v6

    invoke-static {v9}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v9

    invoke-interface {v9}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v9

    new-array v10, v5, [Lmiuix/animation/a/a;

    aput-object p1, v10, v6

    invoke-interface {v9, v4, v10}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    new-array p1, v2, [F

    fill-array-data p1, :array_0

    const-string v4, "scaleX"

    invoke-static {v0, v4, p1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    const-wide/16 v9, 0x12c

    invoke-virtual {p1, v9, v10}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object p1

    new-array v4, v2, [F

    fill-array-data v4, :array_1

    const-string v11, "scaleY"

    invoke-static {v0, v11, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v4, v9, v10}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v4

    new-instance v9, Lmiuix/view/a/f;

    invoke-direct {v9}, Lmiuix/view/a/f;-><init>()V

    invoke-virtual {p1, v9}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v9, Lmiuix/view/a/f;

    invoke-direct {v9}, Lmiuix/view/a/f;-><init>()V

    invoke-virtual {v4, v9}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-array v9, v2, [F

    fill-array-data v9, :array_2

    const-string v10, "alpha"

    invoke-static {v0, v10, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v7, Lmiuix/view/a/f;

    invoke-direct {v7}, Lmiuix/view/a/f;-><init>()V

    invoke-virtual {v0, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const/4 v7, 0x4

    new-array v7, v7, [Landroid/animation/Animator;

    aput-object v3, v7, v6

    aput-object p1, v7, v5

    aput-object v4, v7, v2

    const/4 p1, 0x3

    aput-object v0, v7, p1

    invoke-virtual {v1, v7}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    return-object v1

    nop

    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private flyOut(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V
    .locals 14

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mAnchorView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->calcAnchorPosition(Landroid/view/View;)[I

    move-result-object v0

    new-instance v1, Lmiuix/animation/b/a;

    const-string v2, "start"

    invoke-direct {v1, v2}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v2, Lmiuix/animation/g/A;->j:Lmiuix/animation/g/A;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getX()F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v2, Lmiuix/animation/g/A;->k:Lmiuix/animation/g/A;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getY()F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v2, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v2, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v2, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    invoke-virtual {v1, v2, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v2, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    invoke-virtual {v1, v2, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-instance v2, Lmiuix/animation/b/a;

    const-string v3, "end"

    invoke-direct {v2, v3}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v3, Lmiuix/animation/g/A;->j:Lmiuix/animation/g/A;

    const/4 v4, 0x0

    aget v5, v0, v4

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    const/4 v7, 0x3

    div-int/2addr v6, v7

    add-int/2addr v5, v6

    int-to-double v5, v5

    invoke-virtual {v2, v3, v5, v6}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v3, Lmiuix/animation/g/A;->k:Lmiuix/animation/g/A;

    const/4 v5, 0x1

    aget v0, v0, v5

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    div-int/2addr v6, v7

    add-int/2addr v0, v6

    int-to-double v8, v0

    invoke-virtual {v2, v3, v8, v9}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v0, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    const-wide v8, 0x3fe99999a0000000L    # 0.800000011920929

    invoke-virtual {v2, v0, v8, v9}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v0, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    invoke-virtual {v2, v0, v8, v9}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v0, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    const-wide v8, 0x3fd3333340000000L    # 0.30000001192092896

    invoke-virtual {v2, v0, v8, v9}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-instance v0, Lmiuix/animation/a/a;

    invoke-direct {v0}, Lmiuix/animation/a/a;-><init>()V

    sget-object v3, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    new-array v6, v4, [F

    const-wide/16 v8, 0x9

    invoke-virtual {v0, v3, v8, v9, v6}, Lmiuix/animation/a/a;->a(Lmiuix/animation/g/b;J[F)Lmiuix/animation/a/a;

    move-result-object v0

    const-wide/16 v10, 0x12c

    invoke-virtual {v0, v10, v11}, Lmiuix/animation/a/a;->b(J)Lmiuix/animation/a/a;

    new-instance v3, Lmiuix/animation/a/a;

    invoke-direct {v3}, Lmiuix/animation/a/a;-><init>()V

    sget-object v6, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    new-array v12, v4, [F

    invoke-virtual {v3, v6, v8, v9, v12}, Lmiuix/animation/a/a;->a(Lmiuix/animation/g/b;J[F)Lmiuix/animation/a/a;

    move-result-object v3

    sget-object v6, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    new-array v12, v4, [F

    invoke-virtual {v3, v6, v8, v9, v12}, Lmiuix/animation/a/a;->a(Lmiuix/animation/g/b;J[F)Lmiuix/animation/a/a;

    move-result-object v3

    sget-object v6, Lmiuix/animation/g/A;->j:Lmiuix/animation/g/A;

    new-array v12, v4, [F

    invoke-virtual {v3, v6, v8, v9, v12}, Lmiuix/animation/a/a;->a(Lmiuix/animation/g/b;J[F)Lmiuix/animation/a/a;

    move-result-object v3

    const-wide/16 v8, 0x190

    invoke-virtual {v3, v8, v9}, Lmiuix/animation/a/a;->b(J)Lmiuix/animation/a/a;

    new-instance v6, Lmiuix/animation/a/a;

    invoke-direct {v6}, Lmiuix/animation/a/a;-><init>()V

    sget-object v8, Lmiuix/animation/g/A;->k:Lmiuix/animation/g/A;

    new-array v9, v4, [F

    const-wide/16 v12, 0x10

    invoke-virtual {v6, v8, v12, v13, v9}, Lmiuix/animation/a/a;->a(Lmiuix/animation/g/b;J[F)Lmiuix/animation/a/a;

    move-result-object v6

    invoke-virtual {v6, v10, v11}, Lmiuix/animation/a/a;->b(J)Lmiuix/animation/a/a;

    new-array v8, v5, [Lmiuix/animation/e/b;

    new-instance v9, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$5;

    invoke-direct {v9, p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$5;-><init>(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    aput-object v9, v8, v4

    invoke-virtual {v6, v8}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    new-array v8, v5, [Landroid/view/View;

    aput-object p1, v8, v4

    invoke-static {v8}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p1

    new-array v7, v7, [Lmiuix/animation/a/a;

    aput-object v0, v7, v4

    aput-object v3, v7, v5

    const/4 v0, 0x2

    aput-object v6, v7, v0

    invoke-interface {p1, v1, v2, v7}, Lmiuix/animation/k;->a(Ljava/lang/Object;Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method private flyOutAndAdd(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    invoke-virtual {v1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->stopShaking()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;

    invoke-virtual {v1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->getEntry()Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4, v4}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;->onReduce(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;ZZ)V

    invoke-direct {p0, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->flyOut(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$1;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$1;-><init>(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;Ljava/util/List;)V

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private getAnimal(FLcom/android/settings/coolsound/data/MixMatchAnimalItem;)Lcom/android/settings/coolsound/widget/MixMatchAnimalView;
    .locals 4

    new-instance v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setEntry(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    invoke-virtual {v0, p0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->registerAnimalSelectListener(Lcom/android/settings/coolsound/widget/MixMatchAnimalView$AnimalSelectListener;)V

    iget-object p2, p2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->icon:Landroid/graphics/Bitmap;

    invoke-virtual {v0, p2}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setImageBitMap(Landroid/graphics/Bitmap;)V

    iget-object p2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v1, 0x7f07044c

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    int-to-float p2, p2

    mul-float/2addr p2, p1

    float-to-int p1, p2

    iget-object p2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mAnchorView:Landroid/view/View;

    invoke-direct {p0, p2}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->calcAnchorPosition(Landroid/view/View;)[I

    move-result-object p2

    const/4 v1, 0x0

    aget v1, p2, v1

    int-to-float v1, v1

    int-to-float v2, p1

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    const/4 v3, 0x1

    aget p2, p2, v3

    int-to-float p2, p2

    add-float/2addr p2, v2

    float-to-int p2, p2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setX(F)V

    int-to-float p2, p2

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->setY(F)V

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    check-cast p2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    if-nez p2, :cond_0

    new-instance p2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    invoke-direct {p2, p1, p1}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    :cond_0
    invoke-virtual {v0, p2}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method private getDimensionPixelSize(Ljava/lang/String;Z)I
    .locals 1

    iget-object p2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mContext:Landroid/content/Context;

    const-string v0, "com.xiaomi.misettings"

    invoke-static {p2, p1, v0}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method private getInitialAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)Lcom/android/settings/coolsound/widget/MixMatchAnimalView;
    .locals 5

    new-instance v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setEntry(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    invoke-virtual {v0, p0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->registerAnimalSelectListener(Lcom/android/settings/coolsound/widget/MixMatchAnimalView$AnimalSelectListener;)V

    iget v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mOriginalSize:I

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getScale()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mOriginalSize:I

    int-to-float v2, v2

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getScale()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionX()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setX(F)V

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionY()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setY(F)V

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionX()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionY()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getScale()F

    move-result p1

    invoke-virtual {v0, v3, v4, p1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setTarget(FFF)V

    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {p1, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iget p2, p2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalIconRes:I

    invoke-virtual {v0, p2}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setBackgroundResource(I)V

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method private getViewOrder(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method private initDefaultPosition()V
    .locals 10

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07044c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mOriginalSize:I

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_ONE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v3, "x_animal_position_one"

    invoke-direct {p0, v3, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v3

    const-string v4, "y_animal_position_one"

    invoke-direct {p0, v4, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v2, v3, v4, v5}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v4, "x_animal_position_two_fir"

    invoke-direct {p0, v4, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v4

    const-string v5, "y_animal_position_two_fir"

    invoke-direct {p0, v5, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v5

    const v6, 0x3f47ae14    # 0.78f

    invoke-direct {v2, v4, v5, v6}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    aput-object v2, v1, v3

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v4, "x_animal_position_two_sec"

    invoke-direct {p0, v4, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v4

    const-string v5, "y_animal_position_two_sec"

    invoke-direct {p0, v5, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v5

    invoke-direct {v2, v4, v5, v6}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    const/4 v4, 0x1

    aput-object v2, v1, v4

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v5, "x_animal_position_three_fir"

    invoke-direct {p0, v5, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v5

    const-string v6, "y_animal_position_three_fir"

    invoke-direct {p0, v6, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v6

    const v7, 0x3f35c28f    # 0.71f

    invoke-direct {v2, v5, v6, v7}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    aput-object v2, v1, v3

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v5, "x_animal_position_three_sec"

    invoke-direct {p0, v5, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v5

    const-string v6, "y_animal_position_three_sec"

    invoke-direct {p0, v6, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v6

    invoke-direct {v2, v5, v6, v7}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    aput-object v2, v1, v4

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v5, "x_animal_position_three_third"

    invoke-direct {p0, v5, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v5

    const-string v6, "y_animal_position_three_third"

    invoke-direct {p0, v6, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v6

    invoke-direct {v2, v5, v6, v7}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    const/4 v5, 0x2

    aput-object v2, v1, v5

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v6, "x_animal_position_four_fir"

    invoke-direct {p0, v6, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v6

    const-string v7, "y_animal_position_four_fir"

    invoke-direct {p0, v7, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v7

    const v8, 0x3f11eb85    # 0.57f

    invoke-direct {v2, v6, v7, v8}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    aput-object v2, v1, v3

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v6, "x_animal_position_four_sec"

    invoke-direct {p0, v6, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v6

    const-string v7, "y_animal_position_four_sec"

    invoke-direct {p0, v7, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v7

    invoke-direct {v2, v6, v7, v8}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    aput-object v2, v1, v4

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v6, "x_animal_position_four_third"

    invoke-direct {p0, v6, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v6

    const-string v7, "y_animal_position_four_third"

    invoke-direct {p0, v7, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v7

    invoke-direct {v2, v6, v7, v8}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    aput-object v2, v1, v5

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v6, "x_animal_position_four_forth"

    invoke-direct {p0, v6, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v6

    const-string v7, "y_animal_position_four_forth"

    invoke-direct {p0, v7, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v7

    invoke-direct {v2, v6, v7, v8}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    const/4 v6, 0x3

    aput-object v2, v1, v6

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v7, "x_animal_position_five_fir"

    invoke-direct {p0, v7, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v7

    const-string v9, "y_animal_position_five_fir"

    invoke-direct {p0, v9, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v9

    invoke-direct {v2, v7, v9, v8}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    aput-object v2, v1, v3

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v3, "x_animal_position_five_sec"

    invoke-direct {p0, v3, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v3

    const-string v7, "y_animal_position_five_sec"

    invoke-direct {p0, v7, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v7

    invoke-direct {v2, v3, v7, v8}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    aput-object v2, v1, v4

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v3, "x_animal_position_five_third"

    invoke-direct {p0, v3, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v3

    const-string v4, "y_animal_position_five_third"

    invoke-direct {p0, v4, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v4

    invoke-direct {v2, v3, v4, v8}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    aput-object v2, v1, v5

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v3, "x_animal_position_five_forth"

    invoke-direct {p0, v3, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v3

    const-string v4, "y_animal_position_five_forth"

    invoke-direct {p0, v4, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v4

    invoke-direct {v2, v3, v4, v8}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    aput-object v2, v1, v6

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    new-instance v2, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const-string v3, "x_animal_position_five_fifth"

    invoke-direct {p0, v3, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v3

    const-string v4, "y_animal_position_five_fifth"

    invoke-direct {p0, v4, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getDimensionPixelSize(Ljava/lang/String;Z)I

    move-result v0

    invoke-direct {v2, v3, v0, v8}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;-><init>(IIF)V

    const/4 v0, 0x4

    aput-object v2, v1, v0

    return-void
.end method

.method private move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;
    .locals 11

    invoke-virtual {p1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->stopShaking()V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-virtual {p3}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p3}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p3}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getScale()F

    move-result v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setTarget(FFF)V

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07044c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p3}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getScale()F

    move-result v2

    int-to-float v1, v1

    mul-float/2addr v2, v1

    float-to-int v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, v2}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->setAnimalScale(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;F)V

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->showDelete(Z)V

    const/4 v3, 0x2

    new-array v4, v3, [I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v5

    aput v5, v4, v2

    const/4 v5, 0x1

    aput v1, v4, v5

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    const-wide/16 v6, 0x12c

    invoke-virtual {v1, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    const-wide/16 v6, 0x32

    invoke-virtual {v1, v6, v7}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    new-instance v4, Lmiuix/view/a/f;

    invoke-direct {v4}, Lmiuix/view/a/f;-><init>()V

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v4, Lcom/android/settings/coolsound/b;

    invoke-direct {v4, p1}, Lcom/android/settings/coolsound/b;-><init>(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v4, Lmiuix/view/a/f;

    invoke-direct {v4}, Lmiuix/view/a/f;-><init>()V

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-array v4, v3, [F

    invoke-virtual {p2}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionX()I

    move-result p2

    int-to-float p2, p2

    aput p2, v4, v2

    invoke-virtual {p3}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionX()I

    move-result p2

    int-to-float p2, p2

    aput p2, v4, v5

    const-string p2, "x"

    invoke-static {p1, p2, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object p2

    const-wide/16 v6, 0x1f4

    invoke-virtual {p2, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object p2

    new-instance v4, Lmiuix/view/a/f;

    invoke-direct {v4}, Lmiuix/view/a/f;-><init>()V

    invoke-virtual {p2, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v4, Lmiuix/animation/b/a;

    const-string v6, "y"

    invoke-direct {v4, v6}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v6, Lmiuix/animation/g/A;->k:Lmiuix/animation/g/A;

    invoke-virtual {p3}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->getPositionY()I

    move-result p3

    int-to-double v7, p3

    invoke-virtual {v4, v6, v7, v8}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-instance p3, Lmiuix/animation/a/a;

    invoke-direct {p3}, Lmiuix/animation/a/a;-><init>()V

    new-array v6, v3, [F

    const v7, 0x3f333333    # 0.7f

    aput v7, v6, v2

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v7

    const-wide v9, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v7, v9

    const-wide/high16 v9, 0x3fe0000000000000L    # 0.5

    add-double/2addr v7, v9

    double-to-float v7, v7

    aput v7, v6, v5

    const/4 v7, -0x2

    invoke-virtual {p3, v7, v6}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    new-array v6, v5, [Landroid/view/View;

    aput-object p1, v6, v2

    invoke-static {v6}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v6

    invoke-interface {v6}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v6

    new-array v7, v5, [Lmiuix/animation/a/a;

    aput-object p3, v7, v2

    invoke-interface {v6, v4, v7}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    new-array p3, v3, [Landroid/animation/Animator;

    aput-object p2, p3, v2

    aput-object v1, p3, v5

    invoke-virtual {v0, p3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    new-instance p2, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$7;

    invoke-direct {p2, p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$7;-><init>(Lcom/android/settings/coolsound/MixMatchAnimalAnimator;Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    invoke-virtual {v0, p2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-object v0
.end method

.method private reduceAnimalWhenRemainFour(I)V
    .locals 10

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v4, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v6, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    const/4 v8, 0x4

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v3

    iget-object v9, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v9, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v5

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v7

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v5

    invoke-direct {p0, v4, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v8

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v7

    invoke-direct {p0, v6, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto/16 :goto_0

    :cond_0
    if-ne p1, v3, :cond_1

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v1

    iget-object v9, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v9, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v5

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v7

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v5

    invoke-direct {p0, v4, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v8

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v7

    invoke-direct {p0, v6, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto/16 :goto_0

    :cond_1
    if-ne p1, v5, :cond_2

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v1

    iget-object v9, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v9, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v3

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v7

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v5

    invoke-direct {p0, v4, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v8

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v7

    invoke-direct {p0, v6, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v1

    iget-object v8, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v8, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v3

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v5

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v5

    invoke-direct {p0, v4, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v7

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v7

    invoke-direct {p0, v6, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    :goto_0
    return-void
.end method

.method private reduceAnimalWhenRemainOne(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    const/4 v2, 0x1

    aget-object p1, p1, v2

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v1

    :goto_0
    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_ONE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v2, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    return-void
.end method

.method private reduceAnimalWhenRemainThree(I)V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v4, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    const/4 v6, 0x3

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v3

    iget-object v7, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v7, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v5

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v6

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v5

    invoke-direct {p0, v4, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto/16 :goto_0

    :cond_0
    if-ne p1, v3, :cond_1

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v1

    iget-object v7, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v7, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v5

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v6

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v5

    invoke-direct {p0, v4, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :cond_1
    if-ne p1, v5, :cond_2

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v1

    iget-object v7, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v7, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v3

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v6

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v5

    invoke-direct {p0, v4, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v1

    iget-object v6, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v6, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v3

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v5

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v5

    invoke-direct {p0, v4, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    :goto_0
    return-void
.end method

.method private reduceAnimalWhenRemainTwo(I)V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    const/4 v4, 0x2

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v3

    iget-object v5, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v5, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v4

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :cond_0
    if-ne p1, v3, :cond_1

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v1

    iget-object v5, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v5, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v4

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v1

    iget-object v4, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v4, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object p1, p1, v3

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    :goto_0
    return-void
.end method

.method private scaleOtherAnimal(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    if-eq v1, p1, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->showDelete(Z)V

    invoke-virtual {v1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->hidePlayView()V

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {p0, v1, v2}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->setAnimalScale(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;F)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private setAnimalScale(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;F)V
    .locals 0

    invoke-virtual {p1, p2}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setAnimalScale(F)V

    return-void
.end method


# virtual methods
.method public addAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V
    .locals 13

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->stopAll()V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_5

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eq v2, v5, :cond_4

    const/4 v6, 0x3

    if-eq v2, v4, :cond_3

    const/4 v7, 0x4

    if-eq v2, v6, :cond_2

    if-eq v2, v7, :cond_1

    goto/16 :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v2, v2, v7

    invoke-virtual {v2, p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->setAnimalEntry(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    invoke-direct {p0, v2}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->fly(Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v8, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v8, v8, v3

    iget-object v9, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v9, v9, v3

    invoke-direct {p0, v2, v8, v9}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v2

    iget-object v8, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v9, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v9, v9, v5

    iget-object v10, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v10, v10, v5

    invoke-direct {p0, v8, v9, v10}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v8

    iget-object v9, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v10, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v10, v10, v4

    iget-object v11, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v11, v11, v4

    invoke-direct {p0, v9, v10, v11}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v9

    iget-object v10, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v11, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v11, v11, v6

    iget-object v12, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v12, v12, v6

    invoke-direct {p0, v10, v11, v12}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v10

    new-array v1, v1, [Landroid/animation/Animator;

    aput-object p1, v1, v3

    aput-object v2, v1, v5

    aput-object v8, v1, v4

    aput-object v9, v1, v6

    aput-object v10, v1, v7

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v1, v6

    invoke-virtual {v1, p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->setAnimalEntry(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    invoke-direct {p0, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->fly(Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v2, v2, v3

    iget-object v8, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v8, v8, v3

    invoke-direct {p0, v1, v2, v8}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v8, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v8, v8, v5

    iget-object v9, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v9, v9, v5

    invoke-direct {p0, v2, v8, v9}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v2

    iget-object v8, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v9, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v9, v9, v4

    iget-object v10, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v10, v10, v4

    invoke-direct {p0, v8, v9, v10}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v8

    new-array v7, v7, [Landroid/animation/Animator;

    aput-object p1, v7, v3

    aput-object v1, v7, v5

    aput-object v2, v7, v4

    aput-object v8, v7, v6

    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v1, v4

    invoke-virtual {v1, p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->setAnimalEntry(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    invoke-direct {p0, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->fly(Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v2, v2, v3

    iget-object v7, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v7, v7, v3

    invoke-direct {p0, v1, v2, v7}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v7, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v7, v7, v5

    iget-object v8, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v8, v8, v5

    invoke-direct {p0, v2, v7, v8}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v2

    new-array v6, v6, [Landroid/animation/Animator;

    aput-object p1, v6, v3

    aput-object v1, v6, v5

    aput-object v2, v6, v4

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v1, v1, v5

    invoke-virtual {v1, p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->setAnimalEntry(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    invoke-direct {p0, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->fly(Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_ONE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v2, v2, v3

    iget-object v6, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v6, v6, v3

    invoke-direct {p0, v1, v2, v6}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->move(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object v1

    new-array v2, v4, [Landroid/animation/Animator;

    aput-object p1, v2, v3

    aput-object v1, v2, v5

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_ONE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    aget-object v0, v0, v3

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;->setAnimalEntry(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->fly(Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    :goto_0
    return-void
.end method

.method public addInitialAnimal(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FIVE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->addInitialAnimal([Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Ljava/util/List;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_FOUR:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->addInitialAnimal([Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Ljava/util/List;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_THREE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->addInitialAnimal([Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Ljava/util/List;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_TWO:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->addInitialAnimal([Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Ljava/util/List;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->ANIMAL_POSITION_ONE:[Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->addInitialAnimal([Lcom/android/settings/coolsound/data/MixMatchAnimalPoint;Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method public addRandomAnimal(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->stopAll()V

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->flyOutAndAdd(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->addFive(Ljava/util/List;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->fillOther(Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method public onAnimalSelected(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V
    .locals 1

    const v0, 0x3f933333    # 1.15f

    invoke-direct {p0, p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->setAnimalScale(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;F)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->showDelete(Z)V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->scaleOtherAnimal(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;->onSelectedAnimalPlay(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    :cond_0
    return-void
.end method

.method public onDeleteClicked(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->reduceAnimal(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    return-void
.end method

.method public reduceAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->findAnimalView(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->reduceAnimal(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    :cond_0
    return-void
.end method

.method public reduceAnimal(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->stopAll()V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->flyOut(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->getViewOrder(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_4

    const/4 v3, 0x2

    if-eq v1, v3, :cond_3

    const/4 v3, 0x3

    if-eq v1, v3, :cond_2

    const/4 v3, 0x4

    if-eq v1, v3, :cond_1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->reduceAnimalWhenRemainFour(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->reduceAnimalWhenRemainThree(I)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->reduceAnimalWhenRemainTwo(I)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->reduceAnimalWhenRemainOne(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->getEntry()Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object p1

    invoke-interface {v0, p1, v2, v2}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;->onReduce(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;ZZ)V

    :cond_5
    return-void
.end method

.method public registerReduceListener(Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;

    return-void
.end method

.method public reloadInitialAnimalBitmap()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    invoke-virtual {v1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->resetBitmap()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public resume()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    invoke-virtual {v1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->startShaking()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setRootView(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mRootView:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mAnchorView:Landroid/view/View;

    return-void
.end method

.method public stopAll()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;

    invoke-virtual {v1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->stopShaking()V

    goto :goto_0

    :cond_0
    return-void
.end method
