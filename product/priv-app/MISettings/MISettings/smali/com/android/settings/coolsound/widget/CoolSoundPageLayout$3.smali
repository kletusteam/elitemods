.class Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->initIntelligentRecognitionIfNeeded()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$3;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$3;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->access$500(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$3;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    const v1, 0x7f0b0232

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/IntelligentLayout;

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->access$602(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;Lcom/android/settings/coolsound/widget/IntelligentLayout;)Lcom/android/settings/coolsound/widget/IntelligentLayout;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$3;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->access$600(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)Lcom/android/settings/coolsound/widget/IntelligentLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$3;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v1, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->access$600(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)Lcom/android/settings/coolsound/widget/IntelligentLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$3;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-static {v1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->access$700(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)Lcom/android/settings/coolsound/MediaPlayerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$3;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v2, v2, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->init(Lcom/android/settings/coolsound/MediaPlayerListener;I)V

    :cond_0
    return-void
.end method
