.class public Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;
.super Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;
    }
.end annotation


# static fields
.field private static TYPE_NOTES:I = 0x2000

.field private static ringType:I


# instance fields
.field private ValidList:[Ljava/lang/Integer;

.field playCardSound:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x6

    new-array p1, p1, [Ljava/lang/Integer;

    const/4 p2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    const/16 v0, 0x10

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, p2

    const/4 p2, 0x4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, p1, v1

    const/16 v0, 0x1000

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x3

    aput-object v0, p1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, p2

    sget p2, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->TYPE_NOTES:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x5

    aput-object p2, p1, v0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->ValidList:[Ljava/lang/Integer;

    return-void
.end method

.method public static addMiuiNaturalSound(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/H;->a()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    new-array v1, v2, [Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lmiui/system/R$array;->miui_nature_sound_array:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    :try_start_0
    array-length v3, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x2

    if-gt v3, v4, :cond_2

    goto :goto_1

    :catch_0
    move-exception v3

    goto :goto_2

    :cond_1
    :goto_1
    const-string v3, "android.miui.R$array"

    :try_start_1
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-string v4, "miui_nature_sound_array"

    invoke-static {v3, v4}, Lb/c/a/b/b;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :goto_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    :goto_3
    if-nez v1, :cond_3

    new-array v1, v2, [Ljava/lang/String;

    :cond_3
    array-length v3, v1

    :goto_4
    if-ge v2, v3, :cond_4

    aget-object v4, v1, v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "file://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    new-instance v6, Landroid/net/Uri$Builder;

    invoke-direct {v6}, Landroid/net/Uri$Builder;-><init>()V

    const-string v7, "theme"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "ringtonePick"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "extraRingtoneInfo"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {p0, v5, v7}, Landroid/media/ExtraRingtone;->getRingtoneTitle(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v5

    const-string v7, "title"

    invoke-virtual {v6, v7, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "path"

    invoke-virtual {v5, v6, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_4
    const-string p0, "miui.intent.extra.ringtone.EXTRA_RINGTONE_URI_LIST"

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-void
.end method

.method public static isMultiSimAndNoUniform(Landroid/content/Context;I)Z
    .locals 2

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-static {p0, p1}, Lmiui/util/SimRingtoneUtils;->isDefaultSoundUniform(Landroid/content/Context;I)Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private onItemClicked()V
    .locals 6

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneItem;->getRingtoneType()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/a/a;->b(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v3, v0, :cond_0

    const/16 v4, 0x8

    if-eq v4, v0, :cond_0

    const/16 v4, 0x10

    if-ne v4, v0, :cond_1

    :cond_0
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I

    move-result v4

    if-le v4, v3, :cond_1

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoCount()I

    move-result v4

    sub-int/2addr v4, v1

    if-le v4, v3, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/BaseItem;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "android.intent.extra.ringtone.TYPE"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-static {}, Lcom/android/settings/ringtone/MultiRingtoneSettingUtils;->getMultiRingtoneSettingFragmentName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/coolsound/widget/BaseItem;->setFragment(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/android/settings/MultiSimRingtoneActivity;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0, v2}, Lcom/android/settings/coolsound/widget/BaseItem;->setIntent(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v2}, Lcom/android/settings/coolsound/widget/BaseItem;->setFragment(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/BaseItem;->getFragment()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.RINGTONE_PICKER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->onPrepareRingtonePickerIntent(Landroid/content/Intent;)V

    invoke-static {}, Lcom/misettings/common/utils/f;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.android.thememanager"

    const-string v3, "com.android.thememanager.activity.ThemeTabActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :cond_2
    invoke-virtual {p0, v0}, Lcom/android/settings/coolsound/widget/BaseItem;->setIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f130329

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_3
    :goto_1
    return-void
.end method


# virtual methods
.method protected onAllRingtoneClick()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->onItemClicked()V

    return-void
.end method

.method protected onClick()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->playCardSound:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/BaseItem;->isNormal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->playCardSound:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;

    invoke-interface {v0}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;->onPlayCardSound()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->onItemClicked()V

    return-void
.end method

.method public onPrepareRingtonePickerIntent(Landroid/content/Intent;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->onPrepareRingtonePickerIntent(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "REQUEST_ENTRY_TYPE"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneItem;->getRingtoneType()I

    move-result v0

    const/16 v1, 0x10

    const/16 v2, 0x1000

    if-eq v2, v0, :cond_0

    const/16 v3, 0x8

    if-eq v3, v0, :cond_0

    if-ne v1, v0, :cond_1

    :cond_0
    sget-object v3, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    const-string v4, "android.intent.extra.ringtone.DEFAULT_URI"

    invoke-virtual {p1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    iget-object v3, p0, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->ValidList:[Ljava/lang/Integer;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "android.intent.extra.ringtone.TYPE"

    invoke-virtual {p1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/BaseItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    const-string v4, ":miui:starting_window_label"

    invoke-virtual {p1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    const-string v3, "miui.intent.action.RINGTONE_PICKER"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneItem;->getRingtoneType()I

    move-result v3

    sput v3, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->ringType:I

    const/4 v3, 0x2

    if-eq v3, v0, :cond_3

    if-eq v2, v0, :cond_3

    if-eq v1, v0, :cond_3

    sget v1, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->TYPE_NOTES:I

    if-ne v1, v0, :cond_4

    :cond_3
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->addMiuiNaturalSound(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_4
    return-void
.end method

.method public setPlayCardSound(Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->playCardSound:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;

    return-void
.end method

.method public setRingtoneType(I)V
    .locals 2

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lmiui/util/SimRingtoneUtils;->isDefaultSoundUniform(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v0

    invoke-static {p1, v0}, Lmiui/util/SimRingtoneUtils;->getExtraRingtoneTypeBySlot(II)I

    move-result p1

    invoke-super {p0, p1}, Lcom/android/settings/coolsound/widget/RingtoneItem;->setRingtoneType(I)V

    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/coolsound/widget/RingtoneItem;->setRingtoneType(I)V

    return-void
.end method

.method public setSummary(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/coolsound/widget/BaseItem;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method
