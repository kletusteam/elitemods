.class public Lcom/android/settings/coolsound/widget/RingtoneGridView;
.super Landroid/widget/GridLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;,
        Lcom/android/settings/coolsound/widget/RingtoneGridView$DynamicHolder;,
        Lcom/android/settings/coolsound/widget/RingtoneGridView$MyHandler;
    }
.end annotation


# static fields
.field private static final AUTO_PLAY_TAG:Ljava/lang/String; = "auto"

.field static final COLUMN_COUNT:I = 0x2

.field private static final DEBUG:Z = true

.field private static final FILE_PREFIX:Ljava/lang/String; = "file://"

.field private static final MSG_UPDATE_RINGTONE_SUMMARY:I = 0x1

.field static final PER_AREA_ANIMAL_SIZE:I = 0x5

.field private static final TAG:Ljava/lang/String; = "RingtoneGridView"


# instance fields
.field private audioFocusRunnable:Lcom/android/settings/coolsound/widget/AudioFocusRunnable;

.field private dynamicHolders:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/settings/coolsound/widget/RingtoneGridView$DynamicHolder;",
            ">;"
        }
    .end annotation
.end field

.field private isNatureArea:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/CoolSoundResource;",
            ">;"
        }
    .end annotation
.end field

.field private mItemHeight:I

.field private mItemMargin:I

.field private mItemWidth:I

.field private mListener:Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;

.field private final mMashupClickListener:Landroid/view/View$OnClickListener;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMediaPlayerListener:Lcom/android/settings/coolsound/MediaPlayerListener;

.field private mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mPlayCardSound:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;

.field private mPlayingIndex:I

.field private mResType:I

.field private mRingtoneH:Lcom/android/settings/coolsound/widget/RingtoneGridView$MyHandler;

.field private mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

.field private mSelectView:Landroid/view/View;

.field private mUtils:Lcom/android/settings/coolsound/data/CoolSoundUtils;

.field private mot:Lmiui/util/MiSettingsOT;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;)V

    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->getInstance()Lcom/android/settings/coolsound/data/CoolSoundUtils;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mUtils:Lcom/android/settings/coolsound/data/CoolSoundUtils;

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mResType:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayingIndex:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->dynamicHolders:Landroid/util/SparseArray;

    new-instance p1, Lcom/android/settings/coolsound/widget/RingtoneGridView$6;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$6;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMashupClickListener:Landroid/view/View$OnClickListener;

    new-instance p1, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayCardSound:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->getInstance()Lcom/android/settings/coolsound/data/CoolSoundUtils;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mUtils:Lcom/android/settings/coolsound/data/CoolSoundUtils;

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mResType:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayingIndex:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->dynamicHolders:Landroid/util/SparseArray;

    new-instance p1, Lcom/android/settings/coolsound/widget/RingtoneGridView$6;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$6;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMashupClickListener:Landroid/view/View$OnClickListener;

    new-instance p1, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayCardSound:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->getInstance()Lcom/android/settings/coolsound/data/CoolSoundUtils;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mUtils:Lcom/android/settings/coolsound/data/CoolSoundUtils;

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mResType:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayingIndex:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->dynamicHolders:Landroid/util/SparseArray;

    new-instance p1, Lcom/android/settings/coolsound/widget/RingtoneGridView$6;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$6;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMashupClickListener:Landroid/view/View$OnClickListener;

    new-instance p1, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayCardSound:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->getInstance()Lcom/android/settings/coolsound/data/CoolSoundUtils;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mUtils:Lcom/android/settings/coolsound/data/CoolSoundUtils;

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mResType:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayingIndex:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->dynamicHolders:Landroid/util/SparseArray;

    new-instance p1, Lcom/android/settings/coolsound/widget/RingtoneGridView$6;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$6;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMashupClickListener:Landroid/view/View$OnClickListener;

    new-instance p1, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayCardSound:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/coolsound/widget/RingtoneGridView;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mResType:I

    return p0
.end method

.method static synthetic access$100(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lmiui/util/MiSettingsOT;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mot:Lmiui/util/MiSettingsOT;

    return-object p0
.end method

.method static synthetic access$1002(Lcom/android/settings/coolsound/widget/RingtoneGridView;Lmiui/util/MiSettingsOT;)Lmiui/util/MiSettingsOT;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mot:Lmiui/util/MiSettingsOT;

    return-object p1
.end method

.method static synthetic access$102(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/AudioFocusRunnable;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->audioFocusRunnable:Lcom/android/settings/coolsound/widget/AudioFocusRunnable;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/android/settings/coolsound/widget/RingtoneGridView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->enableSelectedStubView(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/settings/coolsound/widget/RingtoneGridView;I)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->validIndex(I)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1400(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNotificationType()Z

    move-result p0

    return p0
.end method

.method static synthetic access$1500(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/data/CoolSoundUtils;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mUtils:Lcom/android/settings/coolsound/data/CoolSoundUtils;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/view/View;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->ensurePlayViewStub(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/media/MediaPlayer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->addAudioAttributesForHapticIfNeed(Landroid/media/MediaPlayer;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-object p0
.end method

.method static synthetic access$1900(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/AudioManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mAudioManager:Landroid/media/AudioManager;

    return-object p0
.end method

.method static synthetic access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object p0
.end method

.method static synthetic access$300(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->ensureSoundItemStub(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$500(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    return p0
.end method

.method static synthetic access$600(Lcom/android/settings/coolsound/widget/RingtoneGridView;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayingIndex:I

    return p0
.end method

.method static synthetic access$602(Lcom/android/settings/coolsound/widget/RingtoneGridView;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayingIndex:I

    return p1
.end method

.method static synthetic access$700(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/MediaPlayerListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayerListener:Lcom/android/settings/coolsound/MediaPlayerListener;

    return-object p0
.end method

.method static synthetic access$800(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    return-object p0
.end method

.method static synthetic access$900(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mListener:Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;

    return-object p0
.end method

.method private addAudioAttributesForHapticIfNeed(Landroid/media/MediaPlayer;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x20

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v0}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioAttributes$Builder;->setHapticChannelsMuted(Z)Landroid/media/AudioAttributes$Builder;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setAudioAttributes(Landroid/media/AudioAttributes;)V

    :cond_0
    return-void
.end method

.method private cacaluate()V
    .locals 3

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getPaddingStart()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getPaddingEnd()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070126

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemMargin:I

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v1

    const/4 v2, -0x1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/misettings/common/utils/m;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    :goto_1
    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    :cond_2
    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    iget v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemMargin:I

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemWidth:I

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/m;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v1, 0x3f266666    # 0.65f

    goto :goto_2

    :cond_3
    const v1, 0x3fa8469f

    :goto_2
    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-eqz v0, :cond_4

    const v0, 0x3f84fe01

    goto :goto_3

    :cond_4
    const v0, 0x3f686fd3

    :goto_3
    move v1, v0

    :cond_5
    iget v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemWidth:I

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemHeight:I

    return-void
.end method

.method private enableSelectedStubView(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    const v1, 0x7f0b0306

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private enableUnselectedStubView(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0b03a7

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method private ensurePlayViewStub(Landroid/view/View;I)V
    .locals 1

    const v0, 0x7f0b02be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewStub;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/ViewStub;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private ensureSoundItemStub(Landroid/view/View;)V
    .locals 1

    const v0, 0x7f0b031d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewStub;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private getInSampleSize()I
    .locals 1

    invoke-static {}, Lcom/android/settings/coolsound/CoolCommonUtils;->isLowDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    return v0

    :cond_0
    const/4 v0, 0x2

    return v0
.end method

.method private getSelectedAnimalsSummary(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    invoke-direct {v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;-><init>()V

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->getSelectedAnimals(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private initNormalView()V
    .locals 7

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0e0136

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/coolsound/CoolCommonUtils;->setFolme(Landroid/view/View;)V

    div-int/lit8 v3, v1, 0x2

    rem-int/lit8 v4, v1, 0x2

    const/4 v5, 0x1

    invoke-static {v3, v5}, Landroid/widget/GridLayout;->spec(II)Landroid/widget/GridLayout$Spec;

    move-result-object v3

    invoke-static {v4, v5}, Landroid/widget/GridLayout;->spec(II)Landroid/widget/GridLayout$Spec;

    move-result-object v4

    new-instance v6, Landroid/widget/GridLayout$LayoutParams;

    invoke-direct {v6, v3, v4}, Landroid/widget/GridLayout$LayoutParams;-><init>(Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Spec;)V

    iget v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemHeight:I

    iput v3, v6, Landroid/widget/GridLayout$LayoutParams;->height:I

    iget v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemWidth:I

    iput v3, v6, Landroid/widget/GridLayout$LayoutParams;->width:I

    iget v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemMargin:I

    invoke-virtual {v6, v3, v3, v3, v3}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {p0, v2, v6}, Landroid/widget/GridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const v3, 0x7f0b0223

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    new-instance v4, Lcom/android/settings/coolsound/widget/RingtoneGridView$2;

    invoke-direct {v4, p0, v2, v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView$2;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/view/View;I)V

    invoke-virtual {p0, v4}, Landroid/widget/GridLayout;->post(Ljava/lang/Runnable;)Z

    iget-object v4, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/coolsound/data/CoolSoundResource;

    invoke-virtual {v4}, Lcom/android/settings/coolsound/data/CoolSoundResource;->getShowRes()I

    move-result v4

    iget-object v6, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/settings/coolsound/data/CoolSoundResource;

    invoke-virtual {v6}, Lcom/android/settings/coolsound/data/CoolSoundResource;->getShowType()I

    move-result v6

    if-ne v6, v5, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    const v5, 0x7f0800ae

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    new-instance v5, Lcom/android/settings/coolsound/widget/RingtoneGridView$3;

    invoke-direct {v5, p0, v3, v4}, Lcom/android/settings/coolsound/widget/RingtoneGridView$3;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/widget/ImageView;I)V

    invoke-virtual {p0, v5}, Landroid/widget/GridLayout;->post(Ljava/lang/Runnable;)Z

    :cond_0
    new-instance v3, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;

    invoke-direct {v3, p0, v6, v1, v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;IILandroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_1
    return-void
.end method

.method private initRingtoneMore()V
    .locals 5

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNotificationType()Z

    move-result v0

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    if-eqz v0, :cond_0

    const v2, 0x7f0e00fe

    goto :goto_0

    :cond_0
    const v2, 0x7f0e0138

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    iput-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-static {v1}, Lcom/android/settings/coolsound/CoolCommonUtils;->setFolme(Landroid/view/View;)V

    new-instance v1, Landroid/widget/GridLayout$LayoutParams;

    invoke-direct {v1}, Landroid/widget/GridLayout$LayoutParams;-><init>()V

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    div-int/2addr v2, v3

    const/4 v4, 0x1

    invoke-static {v2, v4}, Landroid/widget/GridLayout;->spec(II)Landroid/widget/GridLayout$Spec;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {v4, v3}, Landroid/widget/GridLayout;->spec(II)Landroid/widget/GridLayout$Spec;

    move-result-object v3

    const/4 v4, -0x2

    iput v4, v1, Landroid/widget/GridLayout$LayoutParams;->height:I

    const/4 v4, -0x1

    iput v4, v1, Landroid/widget/GridLayout$LayoutParams;->width:I

    iput-object v2, v1, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    iput-object v3, v1, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    goto :goto_1

    :cond_1
    iget v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemHeight:I

    iput v2, v1, Landroid/widget/GridLayout$LayoutParams;->height:I

    iget v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemWidth:I

    iput v2, v1, Landroid/widget/GridLayout$LayoutParams;->width:I

    :goto_1
    iget v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemMargin:I

    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {p0, v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->initNotificationMoreRingtoneUI(Z)V

    iget-boolean v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/misettings/common/utils/n;->a()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {p0, v2, v1}, Landroid/widget/GridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    :cond_2
    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {p0, v2, v1}, Landroid/widget/GridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    :goto_2
    new-instance v1, Lcom/android/settings/coolsound/widget/RingtoneGridView$MyHandler;

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-direct {v1, v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView$MyHandler;-><init>(Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;)V

    iput-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneH:Lcom/android/settings/coolsound/widget/RingtoneGridView$MyHandler;

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mUtils:Lcom/android/settings/coolsound/data/CoolSoundUtils;

    iget v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mResType:I

    invoke-virtual {v2, v3}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->transferToRingtoneType(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->setRingtoneType(I)V

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    const v1, 0x7f0800ac

    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/widget/BaseItem;->setShowImage(I)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    new-instance v1, Lcom/android/settings/coolsound/widget/RingtoneGridView$5;

    invoke-direct {v1, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$5;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/widget/BaseItem;->setOnClickListener(Lcom/android/settings/coolsound/widget/BaseItem$OnClickListener;)V

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayCardSound:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;

    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->setPlayCardSound(Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;)V

    :cond_5
    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMashupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    return-void
.end method

.method private isDynamicSound()Z
    .locals 2

    iget v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mResType:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isNotificationType()Z
    .locals 2

    iget v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mResType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private loadBitmap(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 2

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iput p3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 p3, 0x0

    iput-boolean p3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/16 p3, 0xa0

    iput p3, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    iput p3, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    sget-object p3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object p3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method private realPlaySoundIfNeeded(IZ)V
    .locals 4

    const-string v0, "RingtoneGridView"

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->stopPlayingMediaPlayer()V

    if-eqz p2, :cond_1

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayingIndex:I

    return-void

    :cond_1
    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p2}, Landroid/media/MediaPlayer;->reset()V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->validIndex(I)Z

    move-result p2

    if-eqz p2, :cond_4

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/android/settings/coolsound/data/CoolSoundResource;

    invoke-virtual {p2}, Lcom/android/settings/coolsound/data/CoolSoundResource;->getSoundPath()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isDynamicSound()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/android/settings/coolsound/data/CoolSoundResource;

    invoke-virtual {p2}, Lcom/android/settings/coolsound/data/CoolSoundResource;->getDynamicSound()Ljava/lang/String;

    move-result-object p2

    :cond_2
    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/android/settings/coolsound/widget/RingtoneGridView$7;

    invoke-direct {v2, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$7;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "realPlaySound...soundPath="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x2

    invoke-virtual {p2, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0, p2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->addAudioAttributesForHapticIfNeed(Landroid/media/MediaPlayer;)V

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p2}, Landroid/media/MediaPlayer;->prepare()V

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-static {p2, v2}, Lcom/android/settings/coolsound/CoolCommonUtils;->setAudioState(Landroid/media/MediaPlayer;Z)V

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {p2, v3, v1, v1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result p2

    if-ne p2, v2, :cond_3

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p2}, Landroid/media/MediaPlayer;->start()V

    :cond_3
    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p0, p2, v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updatePlayView(Landroid/view/View;I)V

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayingIndex:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "realPlaySound err"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :cond_4
    :goto_0
    return-void
.end method

.method private updateRingtoneName(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneH:Lcom/android/settings/coolsound/widget/RingtoneGridView$MyHandler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneH:Lcom/android/settings/coolsound/widget/RingtoneGridView$MyHandler;

    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private updateVideoBackground()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->dynamicHolders:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->dynamicHolders:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/RingtoneGridView$DynamicHolder;

    iget-object v2, v1, Lcom/android/settings/coolsound/widget/RingtoneGridView$DynamicHolder;->videoView:Landroid/widget/VideoView;

    iget v1, v1, Lcom/android/settings/coolsound/widget/RingtoneGridView$DynamicHolder;->backgoundRes:I

    invoke-virtual {v2, v1}, Landroid/widget/VideoView;->setBackgroundResource(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private validIndex(I)Z
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public clearSelectView()V
    .locals 3

    iget v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mResType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->stopPlayingMediaPlayer()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayingIndex:I

    iput-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    :cond_1
    return-void
.end method

.method public clearSelectedViewBg()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    instance-of v0, v0, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-boolean v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-eqz v2, :cond_0

    const v2, 0x7f130258

    goto :goto_0

    :cond_0
    const v2, 0x7f130023

    :goto_0
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    check-cast v2, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {v2, v0}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mPlayingIndex:I

    iput-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    return-void
.end method

.method public getRingtoneType()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneItem;->getRingtoneType()I

    move-result v0

    return v0
.end method

.method public initNatureAreaView(ILcom/android/settings/coolsound/MediaPlayerListener;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->setType(ILcom/android/settings/coolsound/MediaPlayerListener;)V

    return-void
.end method

.method public initNotificationMoreRingtoneUI(Z)V
    .locals 5

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    if-eqz p1, :cond_7

    const v0, 0x7f0b0059

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->enableUnselectedStubView(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    const v2, 0x1020016

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    const v3, 0x1020010

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f130022

    if-eqz v0, :cond_4

    if-eqz v2, :cond_4

    iget-boolean v4, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-eqz v4, :cond_1

    const v4, 0x7f130257

    goto :goto_1

    :cond_1
    move v4, v3

    :goto_1
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-eqz v0, :cond_2

    const v0, 0x7f130258

    goto :goto_2

    :cond_2
    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f130024

    goto :goto_2

    :cond_3
    const v0, 0x7f130023

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/m;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLines(I)V

    :cond_4
    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-eqz v0, :cond_6

    const v0, 0x7f0b0273

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0805b3

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :cond_6
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0805b1

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {p1, v3}, Lcom/android/settings/coolsound/widget/BaseItem;->setTitle(I)V

    :cond_7
    :goto_3
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/GridLayout;->onAttachedToWindow()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateVideoBackground()V

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/widget/GridLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onConfigurationChanged ring"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getChildCount()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "RingtoneGridView"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->cacaluate()V

    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getChildCount()I

    move-result v1

    if-ge p1, v1, :cond_2

    invoke-virtual {p0, p1}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onConfigurationChanged ring:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    instance-of v3, v2, Landroid/widget/GridLayout$LayoutParams;

    if-eqz v3, :cond_0

    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemWidth:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mItemHeight:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    const-string v2, "onConfigurationChanged ring size"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/data/CoolSoundResource;

    invoke-virtual {v2}, Lcom/android/settings/coolsound/data/CoolSoundResource;->getShowType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const v2, 0x7f0b0223

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/data/CoolSoundResource;

    invoke-virtual {v2}, Lcom/android/settings/coolsound/data/CoolSoundResource;->getShowRes()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "onConfigurationChanged: reset ImageView error"

    invoke-static {v0, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    :goto_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/widget/GridLayout;->requestLayout()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->dynamicHolders:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->dynamicHolders:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/RingtoneGridView$DynamicHolder;

    iget-object v1, v1, Lcom/android/settings/coolsound/widget/RingtoneGridView$DynamicHolder;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->suspend()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->dynamicHolders:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    :cond_2
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/widget/GridLayout;->onFinishInflate()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateVideoBackground()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateVideoBackground()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->saveClassicRingtoneUri()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNotificationType()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateNotificationSelectedView()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateSelectRingtone()V

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateOtherRingtoneMore()V

    :goto_0
    return-void
.end method

.method public onSoundSelected(IZ)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSoundSelected...position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " pause:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RingtoneGridView"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->validIndex(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->realPlaySoundIfNeeded(IZ)V

    new-instance p2, Ljava/io/File;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/android/settings/coolsound/data/CoolSoundResource;

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/CoolSoundResource;->getSoundPath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mUtils:Lcom/android/settings/coolsound/data/CoolSoundUtils;

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v1

    invoke-virtual {p2, v0, v1, p1}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->save(Landroid/content/Context;ILandroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->stopPlayingMediaPlayer()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneH:Lcom/android/settings/coolsound/widget/RingtoneGridView$MyHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneH:Lcom/android/settings/coolsound/widget/RingtoneGridView$MyHandler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    return-void
.end method

.method public registerListener(Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mListener:Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;

    return-void
.end method

.method public saveClassicRingtoneUri()V
    .locals 4

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNotificationType()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolCommonUtils;->isAnimalSoundSelected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mUtils:Lcom/android/settings/coolsound/data/CoolSoundUtils;

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {v3}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->getMatchedIndex(Landroid/content/Context;ILandroid/net/Uri;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->validIndex(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->getUri()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveMore:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ExtraRingtoneDelegate"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/android/settings/coolsound/CoolCommonUtils;->saveMoreRingtone(Landroid/content/Context;Ljava/lang/String;I)V

    :cond_2
    new-instance v0, Lcom/android/settings/coolsound/widget/RingtoneGridView$8;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$8;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V

    invoke-virtual {p0, v0}, Landroid/widget/GridLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public setNatureArea(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    return-void
.end method

.method public setType(ILcom/android/settings/coolsound/MediaPlayerListener;)V
    .locals 1

    iput-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayerListener:Lcom/android/settings/coolsound/MediaPlayerListener;

    iput p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mResType:I

    iget-boolean p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/coolsound/data/ResourceWrapper;->loadNatureAreaResource(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, p1}, Lcom/android/settings/coolsound/data/ResourceWrapper;->loadResource(Landroid/content/Context;I)Ljava/util/List;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mData:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 p2, 0x2

    div-int/2addr p1, p2

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/GridLayout;->setRowCount(I)V

    invoke-virtual {p0, p2}, Landroid/widget/GridLayout;->setColumnCount(I)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->cacaluate()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->initNormalView()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->initRingtoneMore()V

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "audio"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/AudioManager;

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mAudioManager:Landroid/media/AudioManager;

    new-instance p1, Lcom/android/settings/coolsound/widget/AudioFocusHelper;

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p1, p2}, Lcom/android/settings/coolsound/widget/AudioFocusHelper;-><init>(Landroid/media/MediaPlayer;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    new-instance p1, Lcom/android/settings/coolsound/widget/AudioFocusRunnable;

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mAudioManager:Landroid/media/AudioManager;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-direct {p1, p2, v0}, Lcom/android/settings/coolsound/widget/AudioFocusRunnable;-><init>(Landroid/media/AudioManager;Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->audioFocusRunnable:Lcom/android/settings/coolsound/widget/AudioFocusRunnable;

    return-void
.end method

.method public stopPlayingMediaPlayer()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->unFocusAudio()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mSelectView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updatePlayView(Landroid/view/View;I)V

    return-void
.end method

.method public unFocusAudio()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :cond_0
    return-void
.end method

.method public updateAllRingtone()V
    .locals 7

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNotificationType()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-nez v0, :cond_a

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/CoolCommonUtils;->getMoreRingtone(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    move v3, v1

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    const/16 v4, 0x8

    if-eqz v3, :cond_1

    move v5, v4

    goto :goto_1

    :cond_1
    move v5, v2

    :goto_1
    invoke-direct {p0, v5}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->enableUnselectedStubView(I)V

    iget-object v5, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    const v6, 0x7f0b03a6

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_3

    if-eqz v3, :cond_2

    move v6, v4

    goto :goto_2

    :cond_2
    move v6, v2

    :goto_2
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    if-eqz v3, :cond_4

    move v5, v2

    goto :goto_3

    :cond_4
    move v5, v4

    :goto_3
    invoke-direct {p0, v5}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->enableSelectedStubView(I)V

    iget-object v5, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    const v6, 0x7f0b0305

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_6

    if-eqz v3, :cond_5

    goto :goto_4

    :cond_5
    move v2, v4

    :goto_4
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNotificationType()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->initNotificationMoreRingtoneUI(Z)V

    if-eqz v3, :cond_a

    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v0, 0x0

    goto :goto_5

    :cond_7
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_5
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/media/ExtraRingtone;->getRingtoneTitle(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {v3}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->getUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_8

    if-nez v0, :cond_9

    :cond_8
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f130260

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_9
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    const v3, 0x7f0b0307

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/widget/BaseItem;->setNormal(Z)V

    :cond_a
    return-void
.end method

.method protected updateMoreRingtoneSummary(Landroid/net/Uri;Z)V
    .locals 1

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {p1, p2}, Lcom/android/settings/coolsound/widget/BaseItem;->setMoreView(Z)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNotificationType()Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/coolsound/CoolCommonUtils;->isAnimalSoundSelected(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getSelectedAnimalsSummary(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f130258

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result p2

    invoke-static {p1, p2}, Lcom/android/settings/coolsound/CoolCommonUtils;->getMoreRingtone(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    return-void

    :cond_2
    const-string p2, ""

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    const/4 p1, 0x0

    goto :goto_0

    :cond_3
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    :goto_0
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, p1, v0}, Landroid/media/ExtraRingtone;->getRingtoneTitle(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object p1

    :goto_1
    invoke-direct {p0, v0, p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateRingtoneName(ILjava/lang/String;)V

    return-void
.end method

.method public updateNotificationSelectedView()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolCommonUtils;->isAnimalSoundSelected(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->clearSelectedViewBg()V

    iget-boolean v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-nez v1, :cond_2

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateSelectRingtone()V

    :cond_2
    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNatureArea:Z

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateAllRingtone()V

    :cond_3
    return-void
.end method

.method public updateOtherRingtoneMore()V
    .locals 4

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/CoolCommonUtils;->getMoreRingtone(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f130022

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/coolsound/widget/BaseItem;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/android/settings/coolsound/widget/RingtoneGridView$9;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$9;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;Z)V

    invoke-virtual {p0, v1}, Landroid/widget/GridLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public updatePlayView(Landroid/view/View;I)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->ensurePlayViewStub(Landroid/view/View;I)V

    const v0, 0x7f0b02bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/android/settings/coolsound/widget/PlayView;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public updateSelectRingtone()V
    .locals 4

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->isNotificationType()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolCommonUtils;->isAnimalSoundSelected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolCommonUtils;->getSelectedIndex(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mUtils:Lcom/android/settings/coolsound/data/CoolSoundUtils;

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {v3}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->getMatchedIndex(Landroid/content/Context;ILandroid/net/Uri;)I

    move-result v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->validIndex(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v0}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateSelectView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->getUri()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateMoreRingtoneSummary(Landroid/net/Uri;Z)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {p0, v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateSelectView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView;->mRingtoneMore:Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->getUri()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateMoreRingtoneSummary(Landroid/net/Uri;Z)V

    :goto_1
    return-void
.end method

.method public updateSelectView(Landroid/view/View;)V
    .locals 1

    new-instance v0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Landroid/widget/GridLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
