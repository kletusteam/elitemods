.class public Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lcom/android/settings/coolsound/MediaPlayerListener;
.implements Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;


# instance fields
.field private isClickChange:Z

.field mAnimalAreaView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

.field mFollowBtn:Lmiuix/slidingwidget/widget/SlidingButton;

.field mFollowItem:Landroid/view/View;

.field mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

.field private mIntellLayout:Lcom/android/settings/coolsound/widget/IntelligentLayout;

.field private mMediaPlayerListener:Lcom/android/settings/coolsound/MediaPlayerListener;

.field private mNatureArea:Landroid/view/View;

.field private mType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->isClickChange:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->isClickChange:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->isClickChange:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->isClickChange:Z

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->ensureFollowItemStub(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->updateFollowBtn()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->isClickChange:Z

    return p0
.end method

.method static synthetic access$202(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->isClickChange:Z

    return p1
.end method

.method static synthetic access$300(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->followItemSelect()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->followItemClick()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->ensureIntellLayoutStub()V

    return-void
.end method

.method static synthetic access$600(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)Lcom/android/settings/coolsound/widget/IntelligentLayout;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mIntellLayout:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    return-object p0
.end method

.method static synthetic access$602(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;Lcom/android/settings/coolsound/widget/IntelligentLayout;)Lcom/android/settings/coolsound/widget/IntelligentLayout;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mIntellLayout:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    return-object p1
.end method

.method static synthetic access$700(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)Lcom/android/settings/coolsound/MediaPlayerListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mMediaPlayerListener:Lcom/android/settings/coolsound/MediaPlayerListener;

    return-object p0
.end method

.method static synthetic access$800(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->updateFollowItem()V

    return-void
.end method

.method private ensureFollowItemStub(I)V
    .locals 1

    const v0, 0x7f0b013b

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/ViewStub;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private ensureIntellLayoutStub()V
    .locals 2

    const v0, 0x7f0b0233

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private followItemClick()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lmiui/util/SimRingtoneUtils;->setDefaultSoundUniform(Landroid/content/Context;IZ)V

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundSettingUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x80

    invoke-static {v1, v2, v0}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->saveDefaultSound(Landroid/content/Context;ILandroid/net/Uri;)V

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->updateFollowItem()V

    return-void
.end method

.method private followItemSelect()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2, v0}, Lmiui/util/SimRingtoneUtils;->setDefaultSoundUniform(Landroid/content/Context;IZ)V

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundSettingUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x80

    invoke-static {v1, v2, v0}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->saveDefaultSound(Landroid/content/Context;ILandroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method private initIntelligentRecognitionIfNeeded()V
    .locals 2

    iget v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mType:I

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    :cond_0
    new-instance v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$3;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$3;-><init>(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method private setFollowVisibility(I)V
    .locals 1

    new-instance v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;-><init>(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;I)V

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private updateFollowBtn()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiui/util/SimRingtoneUtils;->isDefaultSoundUniform(Landroid/content/Context;I)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method private updateFollowItem()V
    .locals 2

    iget v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$5;-><init>(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public init(I)V
    .locals 3

    iput-object p0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mMediaPlayerListener:Lcom/android/settings/coolsound/MediaPlayerListener;

    iput p1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mType:I

    const v0, 0x7f0b02dd

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mMediaPlayerListener:Lcom/android/settings/coolsound/MediaPlayerListener;

    invoke-virtual {v0, p1, v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->setType(ILcom/android/settings/coolsound/MediaPlayerListener;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v0, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->registerListener(Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;)V

    :cond_0
    invoke-static {}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->needRemoveAnimalRingtone()Z

    move-result v0

    const/16 v1, 0x8

    if-nez v0, :cond_2

    iget v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mType:I

    const/4 v2, 0x5

    if-eq v0, v2, :cond_1

    goto :goto_0

    :cond_1
    const v0, 0x7f0b029b

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mAnimalAreaView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mAnimalAreaView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_3

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->setNatureArea(Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mAnimalAreaView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mMediaPlayerListener:Lcom/android/settings/coolsound/MediaPlayerListener;

    invoke-virtual {v0, p1, v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->initNatureAreaView(ILcom/android/settings/coolsound/MediaPlayerListener;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mAnimalAreaView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v0, p0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->registerListener(Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;)V

    goto :goto_1

    :cond_2
    :goto_0
    const v0, 0x7f0b029a

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mNatureArea:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mNatureArea:Landroid/view/View;

    if-eqz v0, :cond_3

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->setFollowVisibility(I)V

    new-instance p1, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;-><init>(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    :cond_4
    invoke-direct {p0, v1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->setFollowVisibility(I)V

    :goto_2
    sget-boolean p1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez p1, :cond_5

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->initIntelligentRecognitionIfNeeded()V

    :cond_5
    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mIntellLayout:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->onDestroy()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->onDestroy()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mAnimalAreaView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->onDestroy()V

    :cond_2
    return-void
.end method

.method public onMoreRingtoneSelected()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mAnimalAreaView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->clearSelectedViewBg()V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->onPause()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mIntellLayout:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->onPause()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mAnimalAreaView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->onPause()V

    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 1

    new-instance v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$4;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$4;-><init>(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->onResume()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mAnimalAreaView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->onResume()V

    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mIntellLayout:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->onStop()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->onStop()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mAnimalAreaView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->onStop()V

    :cond_2
    return-void
.end method

.method public stopIntelligentSound()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->stopPlayingSound()V

    return-void
.end method

.method public stopPlayingSound()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mIntellLayout:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->stopPlayingSound()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mIntellLayout:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->pauseVideo(Z)V

    :cond_0
    return-void
.end method

.method public stopRingtoneGridSound()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mGridView:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->stopPlayingMediaPlayer()V

    :cond_0
    return-void
.end method
