.class Lcom/android/settings/coolsound/widget/TabLayout$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/coolsound/widget/TabLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/TabLayout;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/TabLayout;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout$1;->this$0:Lcom/android/settings/coolsound/widget/TabLayout;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout$1;->this$0:Lcom/android/settings/coolsound/widget/TabLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f1302ba

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/TabLayout$1;->this$0:Lcom/android/settings/coolsound/widget/TabLayout;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/settings/coolsound/widget/TabLayout;->access$000(Lcom/android/settings/coolsound/widget/TabLayout;Ljava/lang/String;Ljava/lang/Object;)Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    move-result-object p1

    iput-object p1, v0, Lcom/android/settings/coolsound/widget/TabLayout;->mTab1:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout$1;->this$0:Lcom/android/settings/coolsound/widget/TabLayout;

    iget-object v0, p1, Lcom/android/settings/coolsound/widget/TabLayout;->mTab1:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    invoke-virtual {p1, v0}, Lmiuix/miuixbasewidget/widget/FilterSortView;->setFilteredTab(Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;)V

    goto :goto_0

    :cond_0
    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/TabLayout$1;->this$0:Lcom/android/settings/coolsound/widget/TabLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f1302bb

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/TabLayout$1;->this$0:Lcom/android/settings/coolsound/widget/TabLayout;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/settings/coolsound/widget/TabLayout;->access$000(Lcom/android/settings/coolsound/widget/TabLayout;Ljava/lang/String;Ljava/lang/Object;)Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    move-result-object p1

    iput-object p1, v0, Lcom/android/settings/coolsound/widget/TabLayout;->mTab2:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    :cond_1
    :goto_0
    return-void
.end method
