.class Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->init(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->access$000(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v2, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowItem:Landroid/view/View;

    if-eqz v2, :cond_2

    const v3, 0x7f0b0138

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v2, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v0, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v2, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2$1;

    invoke-direct {v2, p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2$1;-><init>(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x3e666666    # 0.225f

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/view/View;

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v3, v3, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowItem:Landroid/view/View;

    aput-object v3, v2, v1

    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    new-array v4, v1, [Lmiuix/animation/m$b;

    invoke-interface {v2, v3, v4}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/utils/H;->h(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    const v3, 0x3f4ccccd    # 0.8f

    goto :goto_1

    :cond_1
    const v3, 0x3da3d70a    # 0.08f

    :goto_1
    invoke-interface {v2, v3, v0, v0, v0}, Lmiuix/animation/m;->b(FFFF)Lmiuix/animation/m;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v0, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowItem:Landroid/view/View;

    new-array v1, v1, [Lmiuix/animation/a/a;

    invoke-interface {v2, v0, v1}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v0, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowItem:Landroid/view/View;

    new-instance v1, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2$2;

    invoke-direct {v1, p0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2$2;-><init>(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$2;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    return-void
.end method
