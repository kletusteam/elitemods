.class Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$10;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$10;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$10;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$10;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {v0}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$700(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)I

    move-result v0

    invoke-static {p1, v0}, Lcom/android/settings/coolsound/data/ResourceWrapper;->getAnimalSounds(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$10;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "notification_sound"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$10;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$2100(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;Z)V

    new-instance v0, Lmiui/util/MiSettingsOT;

    iget-object v1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$10;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {v1}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$100(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/util/MiSettingsOT;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "animal_res_path"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "click_area_type"

    invoke-virtual {v0, p1, v1}, Lmiui/util/MiSettingsOT;->tk(Ljava/lang/String;Ljava/util/Map;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$10;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-virtual {p1}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void
.end method
