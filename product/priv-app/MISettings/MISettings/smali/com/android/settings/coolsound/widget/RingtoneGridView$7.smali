.class Lcom/android/settings/coolsound/widget/RingtoneGridView$7;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/widget/RingtoneGridView;->realPlaySoundIfNeeded(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$7;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$7;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$100(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updatePlayView(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$7;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$602(Lcom/android/settings/coolsound/widget/RingtoneGridView;I)I

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$7;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1100(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/AudioFocusRunnable;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/settings/coolsound/widget/AudioFocusRunnable;->postAudioFocusRunnable(Landroid/view/View;Landroid/media/MediaPlayer;Lcom/android/settings/coolsound/widget/AudioFocusRunnable;)V

    return-void
.end method
