.class public Lcom/android/settings/coolsound/data/MixMatchAnimalController;
.super Ljava/lang/Object;


# static fields
.field public static final AFRICA:Ljava/lang/String; = "Africa"

.field public static final AMAZON:Ljava/lang/String; = "Amazon"

.field public static final AUSTRALIA:Ljava/lang/String; = "Australia"

.field public static final GALAPAGOS_ISLANDS:Ljava/lang/String; = "Galapagos"

.field public static final MAX_ANIMAL_COUNT:I = 0x5

.field public static final POLARREGION:Ljava/lang/String; = "PolarRegion"

.field static final RINGTONE_PATH_PREFIX:Ljava/lang/String; = "/system/media/audio/ui/"

.field public static final SOUTHWEST_MOUNTAIN:Ljava/lang/String; = "SouthWestMountain"

.field public static final TAG:Ljava/lang/String; = "MixMatchAnimalController"


# instance fields
.field private mAnimalList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    return-void
.end method

.method private addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    :cond_0
    iput-object p1, p2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalType:Ljava/lang/String;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    invoke-virtual {p2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 3

    iget v0, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget p1, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/4 v1, 0x1

    if-gt v0, p3, :cond_0

    if-le p1, p2, :cond_1

    :cond_0
    div-int/lit8 v0, v0, 0x2

    div-int/lit8 p1, p1, 0x2

    :goto_0
    div-int v2, v0, v1

    if-lt v2, p3, :cond_1

    div-int v2, p1, v1

    if-lt v2, p2, :cond_1

    mul-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_1
    return v1
.end method

.method private loadBitmap(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 2

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    const/16 v1, 0xa0

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    invoke-direct {p0, v0, p3, p3}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result p3

    iput p3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 p3, 0x0

    iput-boolean p3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method private recycleBitMap(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->icon:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->icon:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public destroyData()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v1, "Amazon"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v2, "Africa"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v3, "Australia"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    iget-object v3, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v4, "PolarRegion"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->recycleBitMap(Ljava/util/List;)V

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    if-eqz v1, :cond_1

    invoke-direct {p0, v1}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->recycleBitMap(Ljava/util/List;)V

    invoke-interface {v1}, Ljava/util/List;->clear()V

    :cond_1
    if-eqz v2, :cond_2

    invoke-direct {p0, v2}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->recycleBitMap(Ljava/util/List;)V

    invoke-interface {v2}, Ljava/util/List;->clear()V

    :cond_2
    if-eqz v3, :cond_3

    invoke-direct {p0, v3}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->recycleBitMap(Ljava/util/List;)V

    invoke-interface {v3}, Ljava/util/List;->clear()V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public doLoadBitmap(Landroid/content/Context;Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;I)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    iget v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalIconRes:I

    invoke-direct {p0, p1, v1, p3}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->loadBitmap(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->icon:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getAnimal(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/coolsound/data/MixMatchAnimalItem;
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-nez p1, :cond_1

    return-object v1

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    iget-object v2, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->name:Ljava/lang/String;

    invoke-static {v2, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    return-object v0

    :cond_3
    :goto_0
    return-object v1
.end method

.method public getAnimalList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance p1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    return-object p1

    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070447

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    if-nez p2, :cond_1

    new-instance p1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    return-object p1

    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    iget v3, v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalIconRes:I

    invoke-direct {p0, p1, v3, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->loadBitmap(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->icon:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_2
    return-object p2
.end method

.method public getRandomAnimal([Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v2, "Amazon"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v3, "Africa"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    iget-object v3, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v4, "Australia"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    iget-object v4, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v5, "PolarRegion"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    iget-object v5, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v6, "Galapagos"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    iget-object v6, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v7, "SouthWestMountain"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_3
    if-eqz v5, :cond_4

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_4
    if-eqz v6, :cond_5

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    array-length v2, p1

    const/4 v3, 0x5

    if-ne v3, v2, :cond_6

    goto :goto_0

    :cond_6
    sub-int/2addr v3, v2

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x0

    :goto_1
    const/16 v5, 0x32

    if-ge v4, v5, :cond_a

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v5

    int-to-double v7, v2

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    iget-object v7, v5, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->name:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    invoke-interface {v1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    goto :goto_2

    :cond_7
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-lt v6, v3, :cond_8

    goto :goto_3

    :cond_8
    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->added:Z

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_a
    :goto_3
    return-object v1
.end method

.method public getSelectedAnimals(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->loadData()V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "notification_sound"

    invoke-static {p1, v0}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "mashup_sound#"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0xd

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v1, p1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, p1, v3

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    const/4 v6, 0x2

    if-lt v5, v6, :cond_1

    aget-object v5, v4, v2

    const/4 v6, 0x1

    aget-object v4, v4, v6

    invoke-virtual {p0, v5, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->getAnimal(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object v4

    if-nez v4, :cond_0

    goto :goto_1

    :cond_0
    iget v4, v4, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalNameRes:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public loadBitmap(Landroid/content/Context;)V
    .locals 6

    const-string v0, "MixMatchAnimalController"

    const-string v1, "reload bitmap after recycled"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v1, "Amazon"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v2, "Africa"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v3, "Australia"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    iget-object v3, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v4, "PolarRegion"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070447

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v0, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->doLoadBitmap(Landroid/content/Context;Ljava/util/List;I)V

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {p0, p1, v1, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->doLoadBitmap(Landroid/content/Context;Ljava/util/List;I)V

    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {p0, p1, v2, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->doLoadBitmap(Landroid/content/Context;Ljava/util/List;I)V

    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {p0, p1, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->doLoadBitmap(Landroid/content/Context;Ljava/util/List;I)V

    :cond_3
    return-void
.end method

.method public loadData()V
    .locals 5

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v1, 0x7f1301e8

    const v2, 0x7f0801da

    const-string v3, "Jaguar"

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    const-string v1, "Amazon"

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13026a

    const v3, 0x7f0801ef

    const-string v4, "Ocelot"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13034c

    const v3, 0x7f080214

    const-string v4, "Two-ToedSloths"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130072

    const v3, 0x7f0801b8

    const-string v4, "Capybara"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301ea

    const v3, 0x7f0801db

    const-string v4, "Kinkajou"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13025e

    const v3, 0x7f0801ec

    const-string v4, "Nine-BandedArmadillo"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301a3

    const v3, 0x7f0801d0

    const-string v4, "Golden-HeadedLionTamarin"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130049

    const v3, 0x7f0801b4

    const-string v4, "BlackHowlerMonkey"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301a0

    const v3, 0x7f0801cd

    const-string v4, "GiantOtter"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13029c

    const v3, 0x7f080203

    const-string v4, "ScarletMacaw"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301b4

    const v3, 0x7f0801d7

    const-string v4, "HarpyEagle"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13033b

    const v3, 0x7f080212

    const-string v4, "TocoToucan"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13026d

    const v3, 0x7f0801f1

    const-string v4, "Orange-WingedAmazon"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13004d

    const v3, 0x7f0801b5

    const-string v4, "BluePoisonDartFrog"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v1, 0x7f1301f2

    const v2, 0x7f0801e0

    const-string v3, "Lion"

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    const-string v1, "Africa"

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301f0

    const v3, 0x7f0801df

    const-string v4, "Leopard"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130079

    const v3, 0x7f0801ba

    const-string v4, "Cheetah"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302e5

    const v3, 0x7f08020d

    const-string v4, "SpottedHyaena"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130046

    const v3, 0x7f0801af

    const-string v4, "Black-BackedJackal"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13001f

    const v3, 0x7f0801a7

    const-string v4, "AfricanBushElephant"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13027a

    const v3, 0x7f0801f8

    const-string v4, "PlainsZebra"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301c5

    const v3, 0x7f0801d9

    const-string v4, "Hippopotamus"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13026b

    const v3, 0x7f0801f0

    const-string v4, "OliveBaboon"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130078

    const v3, 0x7f0801b9

    const-string v4, "ChacmaBaboon"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130461

    const v3, 0x7f080216

    const-string v4, "VervetMonkey"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1300c9

    const v3, 0x7f0801bd

    const-string v4, "CommonOstrich"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301c1

    const v3, 0x7f0801d8

    const-string v4, "HelmetedGuineafowl"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130107

    const v3, 0x7f0801c2

    const-string v4, "EgyptianGoose"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130020

    const v3, 0x7f0801a8

    const-string v4, "AfricanWhite-backedVulture"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301fd

    const v3, 0x7f0801e5

    const-string v4, "MarabouStork"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130297

    const v3, 0x7f080201

    const-string v4, "SacredIbis"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130288

    const v3, 0x7f0801fd

    const-string v4, "Red-billedHornbill"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1300d7

    const v3, 0x7f0801be

    const-string v4, "CrownedHornbill"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302e2

    const v3, 0x7f08020c

    const-string v4, "SouthernGroundHornbill"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130279

    const v3, 0x7f0801f7

    const-string v4, "PiedCrow"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302fd

    const v3, 0x7f08020f

    const-string v4, "SuperbStarling"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130048

    const v3, 0x7f0801b0

    const-string v4, "Black-HeadedBulbul"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13004b

    const v3, 0x7f0801b2

    const-string v4, "NorthWhiteRhinoceros"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130343

    const v3, 0x7f080213

    const-string v4, "TreeHyrax"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302e7

    const v3, 0x7f08020e

    const-string v4, "Spur-wingedPlover"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130472

    const v3, 0x7f08021f

    const-string v4, "Yellow-neckedSpurfowl"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301a4

    const v3, 0x7f0801d1

    const-string v4, "Golden-rumpedTinkerbird"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v1, 0x7f13027c

    const v2, 0x7f0801f9

    const-string v3, "Platypus"

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    const-string v1, "Australia"

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302fe

    const v3, 0x7f080210

    const-string v4, "TasmanianDevil"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130470

    const v3, 0x7f08021e

    const-string v4, "Wombat"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130276

    const v3, 0x7f0801f5

    const-string v4, "PetaurusBreviceps"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130105

    const v3, 0x7f0801c1

    const-string v4, "EasternGreyKangaroo"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13004c

    const v3, 0x7f0801b3

    const-string v4, "BlackSwan"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301ee

    const v3, 0x7f0801de

    const-string v4, "LaughingKookaburra"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130300

    const v3, 0x7f080211

    const-string v4, "TasmanianNativeHen"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13046b

    const v3, 0x7f08021a

    const-string v4, "White-BelliedSeaEagle"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301ec

    const v3, 0x7f0801dc

    const-string v4, "Koala"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1300c8

    const v3, 0x7f0801bc

    const-string v4, "CommonBrushtail"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130469

    const v3, 0x7f080219

    const-string v4, "Whipbird"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13010d

    const v3, 0x7f0801c5

    const-string v4, "Emu"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301a8

    const v3, 0x7f0801d5

    const-string v4, "FlyingFox"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130278

    const v3, 0x7f0801f6

    const-string v4, "Butcher"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13003f

    const v3, 0x7f0801ae

    const-string v4, "Magpie"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v1, 0x7f13046d

    const v2, 0x7f08021c

    const-string v3, "WillowGrouse"

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    const-string v1, "PolarRegion"

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302c8

    const v3, 0x7f08020a

    const-string v4, "SnowyOwl"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130071

    const v3, 0x7f0801b7

    const-string v4, "CanadaGoose"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130298

    const v3, 0x7f080202

    const-string v4, "Sanderling"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13027f

    const v3, 0x7f0801fb

    const-string v4, "PomarineJaeger"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130274

    const v3, 0x7f0801f4

    const-string v4, "ParasiticJaeger"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301f6

    const v3, 0x7f0801e2

    const-string v4, "Long-TailedJaeger"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130286

    const v3, 0x7f0801fc

    const-string v4, "Puffin"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130266

    const v3, 0x7f0801ee

    const-string v4, "NorwayLemmings"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130253

    const v3, 0x7f0801e9

    const-string v4, "MusOx"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1300dd

    const v3, 0x7f0801bf

    const-string v4, "DallSheep"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13024c

    const v3, 0x7f0801e8

    const-string v4, "Moose"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130109

    const v3, 0x7f0801c3

    const-string v4, "Elk"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130034

    const v3, 0x7f0801aa

    const-string v4, "ArcticFox"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130036

    const v3, 0x7f0801ab

    const-string v4, "ArcticWolf"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13027e

    const v3, 0x7f0801fa

    const-string v4, "PolarBear"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130110

    const v3, 0x7f0801c6

    const-string v4, "Ermine"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302ab

    const v3, 0x7f080204

    const-string v4, "SeaOtter"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301b3

    const v3, 0x7f0801d6

    const-string v4, "HarpSeal"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130463

    const v3, 0x7f080217

    const-string v4, "Walrus"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13026e

    const v3, 0x7f0801f2

    const-string v4, "Orca"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130254

    const v3, 0x7f0801ea

    const-string v4, "Narwhal"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130465

    const v3, 0x7f080218

    const-string v4, "WeddellSeal"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302e1

    const v3, 0x7f08020b

    const-string v4, "ElephantSeal"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13010b

    const v3, 0x7f0801c4

    const-string v4, "EmperorPenguin"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1300bf

    const v3, 0x7f0801bb

    const-string v4, "ChinstrapPenguin"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13003d

    const v3, 0x7f0801a9

    const-string v4, "AntarcticShag"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v1, 0x7f1301fb

    const v2, 0x7f0801e3

    const-string v3, "Lynx"

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    const-string v1, "SouthWestMountain"

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130273

    const v3, 0x7f0801f3

    const-string v4, "OtocolobusManul"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13046f

    const v3, 0x7f08021d

    const-string v4, "Wolf"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13028a

    const v3, 0x7f0801ff

    const-string v4, "RedFox"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13003c

    const v3, 0x7f0801ad

    const-string v4, "AsianBlackBear"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301a1

    const v3, 0x7f0801ce

    const-string v4, "Panda"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13028b

    const v3, 0x7f080200

    const-string v4, "RedPanda"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301ff

    const v3, 0x7f0801e6

    const-string v4, "MaskedPalmCivet"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130473

    const v3, 0x7f080220

    const-string v4, "Yellow-throatedMarten"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13046c

    const v3, 0x7f08021b

    const-string v4, "WildBoar"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130475

    const v3, 0x7f080221

    const-string v4, "YunnanSnub-NosedMonkey"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302c3

    const v3, 0x7f080207

    const-string v4, "SkywalkerHoolockGibbon"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302b9

    const v3, 0x7f080206

    const-string v4, "SilverPheasant"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301a2

    const v3, 0x7f0801cf

    const-string v4, "GoldenEagle"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13004a

    const v3, 0x7f0801b1

    const-string v4, "Black-NeckedCrane"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13003b

    const v3, 0x7f0801ac

    const-string v4, "AsianBadger"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302c7

    const v3, 0x7f080209

    const-string v4, "SnowLeopard"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301a6

    const v3, 0x7f0801d3

    const-string v4, "GreenPeacock"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v1, 0x7f1301fc

    const v2, 0x7f0801e4

    const-string v3, "MagnificentFrigatebird"

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    const-string v1, "Galapagos"

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301a5

    const v3, 0x7f0801d2

    const-string v4, "GreatFrigatebird"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130259

    const v3, 0x7f0801eb

    const-string v4, "NazcaBooby"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130289

    const v3, 0x7f0801fe

    const-string v4, "Red-BilledTropicbird"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13006e

    const v3, 0x7f0801b6

    const-string v4, "BrownPelican"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13019d

    const v3, 0x7f0801cb

    const-string v4, "GalapagosPenguin"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13019b

    const v3, 0x7f0801c9

    const-string v4, "GalapagosHawk"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302b6

    const v3, 0x7f080205

    const-string v4, "Short-EaredOwl"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f13019c

    const v3, 0x7f0801ca

    const-string v4, "GalapagosMockingbird"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1302c4

    const v3, 0x7f080208

    const-string v4, "SmallGround-Finch"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301ed

    const v3, 0x7f0801dd

    const-string v4, "LargeGround-Finch"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Geospizaconirostris"

    goto :goto_0

    :cond_0
    const-string v2, "GeospizaConirostris"

    :goto_0
    const v3, 0x7f13019f

    const v4, 0x7f0801cc

    invoke-direct {v0, v3, v4, v2}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130460

    const v3, 0x7f080215

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "VampireGround-Finch"

    goto :goto_1

    :cond_1
    const-string v4, "VampireFinch"

    :goto_1
    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130200

    const v3, 0x7f0801e7

    const-string v4, "MediumTree-Finch"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f1301a7

    const v3, 0x7f0801d4

    const-string v4, "GreenWarbler-Finch"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130199

    const v3, 0x7f0801c7

    const-string v4, "GalapagosCormorant"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const v2, 0x7f130277

    const v3, 0x7f0801c8

    const-string v4, "WavedAlbatross"

    invoke-direct {v0, v2, v3, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->addItem(Ljava/lang/String;Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    return-void
.end method

.method public recycleBitMap()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v1, "Amazon"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v2, "Africa"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v3, "Australia"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    iget-object v3, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v4, "PolarRegion"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->recycleBitMap(Ljava/util/List;)V

    :cond_0
    if-eqz v1, :cond_1

    invoke-direct {p0, v1}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->recycleBitMap(Ljava/util/List;)V

    :cond_1
    if-eqz v2, :cond_2

    invoke-direct {p0, v2}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->recycleBitMap(Ljava/util/List;)V

    :cond_2
    if-eqz v3, :cond_3

    invoke-direct {p0, v3}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->recycleBitMap(Ljava/util/List;)V

    :cond_3
    const-string v0, "MixMatchAnimalController"

    const-string v1, "do recycle bitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public unSelectAll()V
    .locals 8

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v2, "Amazon"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v3, "Africa"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    iget-object v3, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v4, "Australia"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    iget-object v4, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v5, "PolarRegion"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    iget-object v5, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v6, "Galapagos"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    iget-object v6, p0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->mAnimalList:Ljava/util/HashMap;

    const-string v7, "SouthWestMountain"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_3
    if-eqz v5, :cond_4

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_4
    if-eqz v6, :cond_5

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    if-eqz v1, :cond_6

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showAdd:Z

    goto :goto_0

    :cond_7
    return-void
.end method
