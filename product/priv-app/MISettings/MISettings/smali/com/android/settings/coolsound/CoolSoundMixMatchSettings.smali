.class public Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;
.super Lcom/android/settings/coolsound/base/CoolSoundBaseActivity;

# interfaces
.implements Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;
.implements Landroid/os/Handler$Callback;
.implements Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout$OnTabItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;
    }
.end annotation


# static fields
.field private static final FULL_ANIMAL_COUNT:I = 0x5

.field private static final KEY_MASHUP_SOUND:Ljava/lang/String; = "mashup_sound"

.field private static final KEY_SAVE_MASHUP_SOUND:Ljava/lang/String; = "save_mashup_sound"

.field private static final MSG_WHAT_INITIAL_FRAGMENTS:I = 0x1

.field private static final MSG_WHAT_RECYCLER:I = 0x2

.field private static final RANDOM_ANIMAL_DELAY:I = 0xc8

.field private static final RECYCLER_DELAY:I = 0x1d4c0

.field private static final TAB_NAME_RES:[I

.field private static final TAB_TYPE_NAME:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "CoolSoundMixMatchSettings"


# instance fields
.field private initSelectTabIndex:I

.field private mAnimalAdded:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimalController:Lcom/android/settings/coolsound/data/MixMatchAnimalController;

.field private mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBitMapRecycled:Z

.field private mCurrentFragment:Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

.field private mCurrentTab:Ljava/lang/String;

.field private final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private mFlag:I

.field private final mFragments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private mInitialCenterView:Landroid/view/View;

.field private mInitialLeftBottomView:Landroid/view/View;

.field private mInitialLeftTopView:Landroid/view/View;

.field private mInitialRightBottomView:Landroid/view/View;

.field private mInitialRightTopView:Landroid/view/View;

.field private mLoading:Landroid/view/View;

.field private final mPlayer:Landroid/media/MediaPlayer;

.field private mRandomBtn:Landroid/widget/Button;

.field private final mRandomRunnable:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;

.field private mSaveBtn:Landroid/widget/Button;

.field private mSblView:Landroid/view/View;

.field private mTabLayout:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;

.field private mTitleTv:Landroid/widget/TextView;

.field private mViewPager:Landroidx/viewpager/widget/ViewPager;

.field private mVpAdapter:Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;

.field private mot:Lmiui/util/MiSettingsOT;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->TAB_NAME_RES:[I

    const-string v1, "Amazon"

    const-string v2, "Africa"

    const-string v3, "Australia"

    const-string v4, "PolarRegion"

    const-string v5, "SouthWestMountain"

    const-string v6, "Galapagos"

    filled-new-array/range {v1 .. v6}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->TAB_TYPE_NAME:[Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f130038
        0x7f130037
        0x7f13003a
        0x7f130221
        0x7f130223
        0x7f13021f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/coolsound/base/CoolSoundBaseActivity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFragments:Ljava/util/ArrayList;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;-><init>(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mRandomRunnable:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mPlayer:Landroid/media/MediaPlayer;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mExecutor:Ljava/util/concurrent/ExecutorService;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initSelectTabIndex:I

    return-void
.end method

.method static synthetic access$100(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->getTabType(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1000(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->setCurrentTab(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->recordSelectPositions()V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Lcom/android/settings/coolsound/MixMatchAnimalAnimator;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->bring2Front()V

    return-void
.end method

.method static synthetic access$1500(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->partialNotifyCurrent(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Landroid/media/MediaPlayer;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mPlayer:Landroid/media/MediaPlayer;

    return-object p0
.end method

.method static synthetic access$200(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;Ljava/lang/String;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->getAnimals(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFragments:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->TAB_TYPE_NAME:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->fragmentContained(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$600(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Lcom/android/settings/coolsound/data/MixMatchAnimalController;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalController:Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    return-object p0
.end method

.method static synthetic access$700(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$800(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTabLayout:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;

    return-object p0
.end method

.method static synthetic access$900(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFlag:I

    return p0
.end method

.method static synthetic access$908(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)I
    .locals 2

    iget v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFlag:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFlag:I

    return v0
.end method

.method private addAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->addAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->showSaveTips()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->bring2Front()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->hideDefaultAnimal()V

    return-void
.end method

.method private alphaHide(Landroid/view/View;)V
    .locals 3

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->b()Lmiuix/animation/o;

    move-result-object p1

    new-array v0, v0, [Lmiuix/animation/a/a;

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lmiuix/animation/o;->b([Lmiuix/animation/a/a;)V

    return-void
.end method

.method private alphaShow(Landroid/view/View;)V
    .locals 3

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->b()Lmiuix/animation/o;

    move-result-object p1

    new-array v0, v0, [Lmiuix/animation/a/a;

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lmiuix/animation/o;->e([Lmiuix/animation/a/a;)V

    return-void
.end method

.method private animalExist(Ljava/lang/String;)Z
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->name:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_2
    return v1
.end method

.method private bring2Front()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mRandomBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->bringToFront()V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mSaveBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->bringToFront()V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTabLayout:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->bringToFront()V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mSblView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    return-void
.end method

.method private clearAll()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    invoke-virtual {v1}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->clearAll()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalController:Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->destroyData()V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->stopAll()V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    :cond_1
    return-void
.end method

.method private doRecycler()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mBitMapRecycled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalController:Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->recycleBitMap()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mBitMapRecycled:Z

    :cond_0
    return-void
.end method

.method private fragmentContained(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    invoke-virtual {v1}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->getCurrentType()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private getAnimals(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalController:Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    invoke-virtual {v0, p0, p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->getAnimalList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private getCurrentFragment()Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mCurrentFragment:Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    return-object v0
.end method

.method private getSavedInitAnimal()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "save_mashup_sound"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mashup_sound#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0xd

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method private getTabType(I)Ljava/lang/String;
    .locals 2

    if-ltz p1, :cond_1

    sget-object v0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->TAB_TYPE_NAME:[Ljava/lang/String;

    array-length v1, v0

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_0
    aget-object p1, v0, p1

    return-object p1

    :cond_1
    :goto_0
    sget-object p1, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->TAB_TYPE_NAME:[Ljava/lang/String;

    const/4 v0, 0x0

    aget-object p1, p1, v0

    return-object p1
.end method

.method private hideDefaultAnimal()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialCenterView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->alphaHide(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialLeftTopView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->alphaHide(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialLeftBottomView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->alphaHide(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialRightTopView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->alphaHide(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialRightBottomView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->alphaHide(Landroid/view/View;)V

    return-void
.end method

.method private init()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initData()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initView()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initAnimal()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initViewPager()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initTab()V

    return-void
.end method

.method private initActionBar()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/d;->e(I)V

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/d;->g(Z)V

    :cond_0
    return-void
.end method

.method private initAnimal()V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    if-eqz v3, :cond_0

    iget-object v4, v3, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalType:Ljava/lang/String;

    iget-object v3, v3, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalController:Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    invoke-virtual {v5, v4, v3}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->getAnimal(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object v3

    iput-boolean v1, v3, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->added:Z

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->showInitAnimal()V

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->getSavedInitAnimal()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v2, :cond_6

    aget-object v5, v0, v4

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    if-eqz v5, :cond_5

    array-length v6, v5

    const/4 v7, 0x2

    if-eq v6, v7, :cond_4

    goto :goto_3

    :cond_4
    aget-object v6, v5, v3

    aget-object v5, v5, v1

    iget-object v7, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalController:Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    invoke-virtual {v7, v6, v5}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->getAnimal(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object v5

    if-eqz v5, :cond_5

    iput-boolean v1, v5, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->added:Z

    iget-object v6, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->showInitAnimal()V

    return-void
.end method

.method private initData()V
    .locals 1

    new-instance v0, Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    invoke-direct {v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalController:Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalController:Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->loadData()V

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAudioManager:Landroid/media/AudioManager;

    return-void
.end method

.method private initFragments()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;

    invoke-direct {v1, p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;-><init>(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private initTab()V
    .locals 7

    const v0, 0x7f0b0372

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTabLayout:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTabLayout:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;

    invoke-virtual {v0, p0}, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->registerListener(Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout$OnTabItemClickListener;)V

    sget-object v0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->TAB_NAME_RES:[I

    array-length v0, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const/4 v3, 0x1

    if-ge v2, v0, :cond_2

    new-instance v4, Lcom/android/settings/coolsound/data/MixMatchTab;

    invoke-direct {v4}, Lcom/android/settings/coolsound/data/MixMatchTab;-><init>()V

    sget-object v5, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->TAB_NAME_RES:[I

    aget v5, v5, v2

    invoke-virtual {p0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/settings/coolsound/data/MixMatchTab;->tabName:Ljava/lang/String;

    sget-object v5, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->TAB_TYPE_NAME:[Ljava/lang/String;

    aget-object v5, v5, v2

    iput-object v5, v4, Lcom/android/settings/coolsound/data/MixMatchTab;->tabTypeName:Ljava/lang/String;

    iget-object v5, v4, Lcom/android/settings/coolsound/data/MixMatchTab;->tabTypeName:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mCurrentTab:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    iput-boolean v5, v4, Lcom/android/settings/coolsound/data/MixMatchTab;->isSelected:Z

    if-nez v2, :cond_0

    iget-boolean v5, v4, Lcom/android/settings/coolsound/data/MixMatchTab;->isSelected:Z

    if-nez v5, :cond_0

    iput-boolean v3, v4, Lcom/android/settings/coolsound/data/MixMatchTab;->isSelected:Z

    :cond_0
    iget-boolean v3, v4, Lcom/android/settings/coolsound/data/MixMatchTab;->isSelected:Z

    if-eqz v3, :cond_1

    move v3, v2

    goto :goto_1

    :cond_1
    iget v3, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initSelectTabIndex:I

    :goto_1
    iput v3, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initSelectTabIndex:I

    iget-object v3, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTabLayout:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;

    invoke-virtual {v3, v4}, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->addTab(Lcom/android/settings/coolsound/data/MixMatchTab;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTabLayout:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;

    iget v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initSelectTabIndex:I

    invoke-virtual {v0, v2}, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->setSelect(I)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v0

    if-ne v0, v3, :cond_3

    goto :goto_2

    :cond_3
    move v3, v1

    :goto_2
    if-eqz v3, :cond_4

    iput v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFlag:I

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTabLayout:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$2;-><init>(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_4
    return-void
.end method

.method private initView()V
    .locals 5

    const v0, 0x7f0b0399

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTitleTv:Landroid/widget/TextView;

    const v0, 0x7f0b03c0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mLoading:Landroid/view/View;

    const v0, 0x7f0b008d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mRandomBtn:Landroid/widget/Button;

    const v0, 0x7f0b008e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mSaveBtn:Landroid/widget/Button;

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mRandomBtn:Landroid/widget/Button;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mRandomBtn:Landroid/widget/Button;

    new-array v4, v3, [Lmiuix/animation/a/a;

    invoke-interface {v1, v2, v4}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mSaveBtn:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mSaveBtn:Landroid/widget/Button;

    new-array v2, v3, [Lmiuix/animation/a/a;

    invoke-interface {v0, v1, v2}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    const v0, 0x7f0b03c3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0b03be

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mRandomBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mSaveBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0b03b3

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialCenterView:Landroid/view/View;

    const v2, 0x7f0b03b6

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialLeftTopView:Landroid/view/View;

    const v2, 0x7f0b03b5

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialLeftBottomView:Landroid/view/View;

    const v2, 0x7f0b03b8

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialRightTopView:Landroid/view/View;

    const v2, 0x7f0b03b7

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialRightBottomView:Landroid/view/View;

    const v2, 0x7f0b02e3

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mSblView:Landroid/view/View;

    new-instance v2, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    invoke-direct {v2, p0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    invoke-virtual {v2, v0, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->setRootView(Landroid/view/ViewGroup;Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    invoke-virtual {v0, p0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->registerReduceListener(Lcom/android/settings/coolsound/MixMatchAnimalAnimator$ReduceAnimalListener;)V

    return-void
.end method

.method private initViewPager()V
    .locals 2

    const v0, 0x7f0b03cf

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    new-instance v0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;-><init>(Landroidx/fragment/app/FragmentManager;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mVpAdapter:Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mVpAdapter:Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/f;)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initFragments()V

    return-void
.end method

.method private isAnimalFull()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isRingerNormal()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private notifyAllFragmentDelay()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->setDataChanged(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private notifyCurrent(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->getCurrentFragment()Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->setDataChanged(Z)V

    invoke-virtual {v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method private notifyExist(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFragments:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    invoke-virtual {v1, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->tryRefresh(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method private partialNotifyCurrent(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->getCurrentFragment()Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->refreshOnlyItemChange(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method private playSound(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->canPlaySound()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$4;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$4;-><init>(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    :cond_0
    return-void
.end method

.method private recordSelectPositions()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->getCurrentFragment()Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->recordCurrentSelectPositions()V

    :cond_0
    return-void
.end method

.method private reduceAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;ZZ)V
    .locals 0

    iget-object p3, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 p3, 0x0

    iput-boolean p3, p1, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->added:Z

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->notifyExist(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->showSaveTips()V

    :cond_0
    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->showDefaultAnimal()V

    :cond_1
    return-void
.end method

.method private refreshBitMap()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mBitMapRecycled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalController:Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    invoke-virtual {v0, p0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->loadBitmap(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->notifyAllFragmentDelay()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->notifyCurrent(Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->reloadInitialAnimalBitmap()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mBitMapRecycled:Z

    :cond_0
    return-void
.end method

.method private restoreAnimal(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, "saved_animal"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    :cond_0
    return-void
.end method

.method private saveCurrentTab(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_1

    const-string v0, "current_tab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mCurrentTab:Ljava/lang/String;

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mCurrentTab:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "Amazon"

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mCurrentTab:Ljava/lang/String;

    :goto_0
    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mCurrentTab:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method private saveRingtone()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mashup_sound"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    if-eqz v2, :cond_1

    iget-object v3, v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalType:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "notification_sound"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "save_mashup_sound"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_3
    return-void
.end method

.method private setCurrentFragment(Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mCurrentFragment:Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    return-void
.end method

.method private setCurrentTab(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTabLayout:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->getDefaultTabType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mCurrentTab:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mVpAdapter:Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->getItem(I)Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->setCurrentFragment(Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;)V

    return-void
.end method

.method private showDefaultAnimal()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialCenterView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->alphaShow(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialLeftTopView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->alphaShow(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialLeftBottomView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->alphaShow(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialRightTopView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->alphaShow(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mInitialRightBottomView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->alphaShow(Landroid/view/View;)V

    return-void
.end method

.method private showDeleteTips()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTitleTv:Landroid/widget/TextView;

    const v1, 0x7f1300ec

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTitleTv:Landroid/widget/TextView;

    const v1, 0x7f0602fe

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->isRingerNormal()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->showDeleteToast()V

    :cond_0
    return-void
.end method

.method private showDeleteToast()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f13002c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/misettings/common/utils/r;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method private showInitAnimal()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->hideDefaultAnimal()V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->addInitialAnimal(Ljava/util/List;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->showSaveTips()V

    return-void
.end method

.method private showMuteToast()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1302b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/misettings/common/utils/r;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method private showSaveTips()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTitleTv:Landroid/widget/TextView;

    const v1, 0x7f0602fd

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mSaveBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTitleTv:Landroid/widget/TextView;

    const v1, 0x7f130338

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTitleTv:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mSaveBtn:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method private toRecycler()V
    .locals 4

    const-string v0, "CoolSoundMixMatchSettings"

    const-string v1, "prepare to recycle bitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private trackPage()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mot:Lmiui/util/MiSettingsOT;

    const-string v1, "view_mashup_page"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmiui/util/MiSettingsOT;->tk(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method private trackSave()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mot:Lmiui/util/MiSettingsOT;

    const-string v1, "click_mashup_save"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmiui/util/MiSettingsOT;->tk(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method private trackSaveWithAnimals()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v1}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->getRingtonePath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "animal_res_path"

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v1, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalType:Ljava/lang/String;

    const-string v4, "animal_type"

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, v1, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalNameRes:I

    if-lez v1, :cond_0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "animal_name"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mot:Lmiui/util/MiSettingsOT;

    const-string v3, "click_mashup_save_animals"

    invoke-virtual {v1, v3, v2}, Lmiui/util/MiSettingsOT;->tk(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public canAddAnimal(Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->isAnimalFull()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->showDeleteTips()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->showSaveTips()V

    :goto_0
    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->animalExist(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method public canPlaySound()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->isRingerNormal()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->showMuteToast()V

    :cond_0
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1    # Landroid/os/Message;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    new-instance v2, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$3;

    invoke-direct {v2, p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$3;-><init>(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)V

    invoke-virtual {p1, v2}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$d;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mVpAdapter:Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {p1, v2}, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->setFragments(Ljava/util/ArrayList;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mLoading:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mTabLayout:Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;

    invoke-virtual {p1}, Lcom/android/settings/coolsound/widget/AnimalTypeTabLayout;->getCurrentTabPosition()I

    move-result p1

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v2, p1, v0}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->setCurrentTab(I)V

    return v1

    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    const-string p1, "CoolSoundMixMatchSettings"

    const-string v1, "recycle bitmap"

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->doRecycler()V

    :cond_1
    return v0
.end method

.method public onAddAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->stopAll()V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->addAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b008e

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->saveRingtone()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->trackSave()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->trackSaveWithAnimals()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b008d

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->stopAll()V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mRandomRunnable:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;

    const-wide/16 v1, 0xc8

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/coolsound/base/CoolSoundBaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->restoreAnimal(Landroid/os/Bundle;)V

    const v0, 0x7f130337

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(I)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initActionBar()V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->saveCurrentTab(Landroid/os/Bundle;)V

    const p1, 0x7f0e007c

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->init()V

    new-instance p1, Lmiui/util/MiSettingsOT;

    invoke-direct {p1, p0}, Lmiui/util/MiSettingsOT;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mot:Lmiui/util/MiSettingsOT;

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->trackPage()V

    invoke-static {p0}, Lcom/android/settings/coolsound/CoolCommonUtils;->setInFullWindowGestureMode(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    const/16 v0, 0x2000

    invoke-virtual {p1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/xiaomi/misettings/usagestats/ExitMultiWindowActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onDeleteAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->reduceAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->clearAll()V

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    return-void
.end method

.method public onItemClicked(I)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->getCurrentFragment()Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->getAnimalItem(I)Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->getRingtonePath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->playSound(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onLoadCompleted()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mLoading:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->stopAll()V

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    return-void
.end method

.method public onRandom(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->added:Z

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->showSaveTips()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->bring2Front()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->hideDefaultAnimal()V

    return-void
.end method

.method public onRandomCompleted()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mRandomBtn:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->notifyAllFragmentDelay()V

    return-void
.end method

.method public onReduce(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->reduceAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;ZZ)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->bring2Front()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onResume()V

    const v0, 0x7f0b0041

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f130337

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimator:Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->resume()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->refreshBitMap()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mCurrentTab:Ljava/lang/String;

    const-string v1, "current_tab"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mAnimalAdded:Ljava/util/ArrayList;

    const-string v1, "saved_animal"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSelectedAnimalPlay(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V
    .locals 1

    invoke-virtual {p1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->getEntry()Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->getRingtonePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->playSound(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->showPlayView()V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStop()V

    return-void
.end method

.method public onTabItemClicked(I)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->setCurrentTab(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    return-void
.end method
