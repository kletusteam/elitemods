.class Lcom/android/settings/coolsound/CoolSoundPhoneActivity$1;
.super Landroidx/viewpager/widget/ViewPager$g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->initTab()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/CoolSoundPhoneActivity;

.field final synthetic val$tab:Lcom/android/settings/coolsound/widget/TabLayout;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;Lcom/android/settings/coolsound/widget/TabLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$1;->this$0:Lcom/android/settings/coolsound/CoolSoundPhoneActivity;

    iput-object p2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$1;->val$tab:Lcom/android/settings/coolsound/widget/TabLayout;

    invoke-direct {p0}, Landroidx/viewpager/widget/ViewPager$g;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPageSelected:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CoolSoundPhoneActivity"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$1;->this$0:Lcom/android/settings/coolsound/CoolSoundPhoneActivity;

    iget-object v1, v1, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    if-eq v0, p1, :cond_1

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$1;->this$0:Lcom/android/settings/coolsound/CoolSoundPhoneActivity;

    iget-object v1, v1, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->onStop()V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$1;->this$0:Lcom/android/settings/coolsound/CoolSoundPhoneActivity;

    iget-object v1, v1, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->onResume()V

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$1;->val$tab:Lcom/android/settings/coolsound/widget/TabLayout;

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/widget/TabLayout;->onPageSelected(I)V

    return-void
.end method
