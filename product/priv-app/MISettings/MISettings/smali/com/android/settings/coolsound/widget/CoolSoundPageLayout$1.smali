.class Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->setFollowVisibility(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

.field final synthetic val$visibility:I


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iput p2, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;->val$visibility:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget v1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;->val$visibility:I

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->access$000(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    const v1, 0x7f0b0139

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowItem:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v1, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowItem:Landroid/view/View;

    if-eqz v1, :cond_0

    const v2, 0x7f0b0138

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v1, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->access$100(Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;->this$0:Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v0, v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->mFollowItem:Landroid/view/View;

    iget v1, p0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout$1;->val$visibility:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method
