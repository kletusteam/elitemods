.class public Lcom/android/settings/coolsound/data/ResourceOverlay;
.super Ljava/lang/Object;


# static fields
.field protected static final natureAreaAnimalImage:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/ShowResource;",
            ">;"
        }
    .end annotation
.end field

.field protected static final notificationImageVideo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/ShowResource;",
            ">;"
        }
    .end annotation
.end field

.field protected static final notificationOnlyImageVideo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/ShowResource;",
            ">;"
        }
    .end annotation
.end field

.field protected static final ringtoneImageVideo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/ShowResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->ringtoneImageVideo:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->notificationImageVideo:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->natureAreaAnimalImage:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->notificationOnlyImageVideo:Ljava/util/List;

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->notificationImageVideo:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const/4 v2, 0x0

    const v3, 0x7f120007

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->notificationImageVideo:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f12000f

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->notificationImageVideo:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const/4 v2, 0x1

    const v3, 0x7f080601

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->notificationImageVideo:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f080602

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->notificationImageVideo:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f0805da

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->notificationOnlyImageVideo:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f080257

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->notificationOnlyImageVideo:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f0800db

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->ringtoneImageVideo:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f080605

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->ringtoneImageVideo:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f080607

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->ringtoneImageVideo:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f080606

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->natureAreaAnimalImage:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f080086

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->natureAreaAnimalImage:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f080083

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/android/settings/coolsound/CoolCommonUtils;->isLiteDevice()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->natureAreaAnimalImage:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f080094

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceOverlay;->natureAreaAnimalImage:Ljava/util/List;

    new-instance v1, Lcom/android/settings/coolsound/data/ShowResource;

    const v3, 0x7f08008e

    invoke-direct {v1, v3, v2}, Lcom/android/settings/coolsound/data/ShowResource;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
