.class Lcom/android/settings/coolsound/widget/RingtoneGridView$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/widget/RingtoneGridView;->initNormalView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

.field final synthetic val$child:Landroid/view/View;

.field final synthetic val$index:I


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/view/View;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$2;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iput-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$2;->val$child:Landroid/view/View;

    iput p3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$2;->val$index:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$2;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$2;->val$child:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$300(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$2;->val$child:Landroid/view/View;

    const v1, 0x1020010

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$2;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$400(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Ljava/util/List;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$2;->val$index:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/data/CoolSoundResource;

    invoke-virtual {v1}, Lcom/android/settings/coolsound/data/CoolSoundResource;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$2;->val$child:Landroid/view/View;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
