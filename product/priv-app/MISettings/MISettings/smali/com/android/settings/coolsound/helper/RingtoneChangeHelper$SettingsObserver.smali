.class Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;
.super Landroid/database/ContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;


# direct methods
.method public constructor <init>(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onChange"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "RingtoneChangeHelper"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object p1, Landroid/provider/MiuiSettings$System;->DEFAULT_RINGTONE_URI_SLOT_1:Landroid/net/Uri;

    invoke-virtual {p1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_0
    sget-object p1, Landroid/provider/MiuiSettings$System;->DEFAULT_RINGTONE_URI_SLOT_2:Landroid/net/Uri;

    invoke-virtual {p1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    const/4 p2, 0x2

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_1
    sget-object p1, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    invoke-virtual {p1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    iget-object p1, p1, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mRingtoneSoundUseUniform:Landroid/net/Uri;

    if-eqz p1, :cond_3

    invoke-virtual {p1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    const/4 p2, 0x3

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_3
    sget-object p1, Landroid/provider/MiuiSettings$System;->DEFAULT_SMS_RECEIVED_RINGTONE_URI:Landroid/net/Uri;

    invoke-virtual {p1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_4
    sget-object p1, Landroid/provider/MiuiSettings$System;->DEFAULT_SMS_RECEIVED_SOUND_URI_SLOT_1:Landroid/net/Uri;

    invoke-virtual {p1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;->this$0:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_5
    :goto_0
    return-void
.end method
