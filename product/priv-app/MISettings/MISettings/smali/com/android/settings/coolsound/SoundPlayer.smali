.class public Lcom/android/settings/coolsound/SoundPlayer;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/SoundPlayer$RingModeReceiver;
    }
.end annotation


# static fields
.field private static final RINGER_MODE_CHANGED_ACTION:Ljava/lang/String; = "android.media.RINGER_MODE_CHANGED"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mExoPlayer:Lcom/google/android/exoplayer2/oa;

.field private mRecever:Lcom/android/settings/coolsound/SoundPlayer$RingModeReceiver;

.field private soundPath:I

.field private volume_per:F


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->volume_per:F

    iput-object p1, p0, Lcom/android/settings/coolsound/SoundPlayer;->mContext:Landroid/content/Context;

    iput p2, p0, Lcom/android/settings/coolsound/SoundPlayer;->soundPath:I

    iget-object p1, p0, Lcom/android/settings/coolsound/SoundPlayer;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/exoplayer2/L;->a(Landroid/content/Context;)Lcom/google/android/exoplayer2/oa;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/SoundPlayer;->mExoPlayer:Lcom/google/android/exoplayer2/oa;

    return-void
.end method


# virtual methods
.method public getVolume_per()F
    .locals 1
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    iget v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->volume_per:F

    return v0
.end method

.method public prepare()V
    .locals 3

    new-instance v0, Lcom/google/android/exoplayer2/upstream/s;

    iget-object v1, p0, Lcom/android/settings/coolsound/SoundPlayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/upstream/s;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/exoplayer2/source/q$a;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer2/source/q$a;-><init>(Lcom/google/android/exoplayer2/upstream/l$a;)V

    iget v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->soundPath:I

    invoke-static {v0}, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;->buildRawResourceUri(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/source/q$a;->a(Landroid/net/Uri;)Lcom/google/android/exoplayer2/source/q;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/SoundPlayer;->mExoPlayer:Lcom/google/android/exoplayer2/oa;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/oa;->setRepeatMode(I)V

    iget-object v1, p0, Lcom/android/settings/coolsound/SoundPlayer;->mExoPlayer:Lcom/google/android/exoplayer2/oa;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/oa;->b(I)V

    iget-object v1, p0, Lcom/android/settings/coolsound/SoundPlayer;->mExoPlayer:Lcom/google/android/exoplayer2/oa;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/source/y;)V

    new-instance v0, Lcom/android/settings/coolsound/SoundPlayer$RingModeReceiver;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/SoundPlayer$RingModeReceiver;-><init>(Lcom/android/settings/coolsound/SoundPlayer;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->mRecever:Lcom/android/settings/coolsound/SoundPlayer$RingModeReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/coolsound/SoundPlayer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/coolsound/SoundPlayer;->mRecever:Lcom/android/settings/coolsound/SoundPlayer$RingModeReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public release()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->mExoPlayer:Lcom/google/android/exoplayer2/oa;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/oa;->C()V

    :cond_0
    return-void
.end method

.method public setVolume_per(F)V
    .locals 2
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    iput p1, p0, Lcom/android/settings/coolsound/SoundPlayer;->volume_per:F

    iget-object v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->mExoPlayer:Lcom/google/android/exoplayer2/oa;

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr p1, v1

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/oa;->a(F)V

    return-void
.end method

.method public start()V
    .locals 7

    iget-object v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->mExoPlayer:Lcom/google/android/exoplayer2/oa;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/oa;->b(Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->mExoPlayer:Lcom/google/android/exoplayer2/oa;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/oa;->a(F)V

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v0, v3

    invoke-static {v0}, Lmiuix/animation/d;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    move-result-object v0

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Object;

    const-string v6, "volume_per"

    aput-object v6, v5, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-interface {v0, v5}, Lmiuix/animation/k;->d([Ljava/lang/Object;)Lmiuix/animation/k;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v6, v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v1

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    const/4 v3, -0x2

    new-array v5, v4, [F

    fill-array-data v5, :array_0

    invoke-virtual {v1, v3, v5}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    aput-object v1, v2, v4

    invoke-interface {v0, v2}, Lmiuix/animation/k;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x3f99999a    # 1.2f
        0x40000000    # 2.0f
    .end array-data
.end method

.method public stop()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->mExoPlayer:Lcom/google/android/exoplayer2/oa;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/oa;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->mExoPlayer:Lcom/google/android/exoplayer2/oa;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/oa;->b(Z)V

    :cond_0
    return-void
.end method

.method public unregisterRecevier()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/coolsound/SoundPlayer;->mRecever:Lcom/android/settings/coolsound/SoundPlayer$RingModeReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/coolsound/SoundPlayer;->mRecever:Lcom/android/settings/coolsound/SoundPlayer$RingModeReceiver;

    return-void
.end method
