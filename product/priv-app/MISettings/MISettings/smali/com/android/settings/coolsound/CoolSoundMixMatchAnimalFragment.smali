.class public Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;
.super Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;


# instance fields
.field private changePositions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

.field private mAnimalListRcv:Lmiuix/recyclerview/widget/RecyclerView;

.field private mLoadingView:Landroid/view/View;

.field private mSpanCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mSpanCount:I

    return-void
.end method

.method private getSpanCount(Landroid/content/Context;)I
    .locals 3

    const/4 v0, 0x4

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-static {p1}, Lcom/misettings/common/utils/m;->e(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/misettings/common/utils/m;->c(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_1

    return v0

    :cond_1
    const/4 p1, 0x3

    return p1

    :cond_2
    invoke-static {p1}, Lcom/misettings/common/utils/m;->f(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x6

    if-eqz v1, :cond_3

    invoke-static {p1}, Lcom/misettings/common/utils/m;->f(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    return v2

    :cond_3
    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p1}, Lcom/misettings/common/utils/n;->a(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_4

    return v2

    :cond_4
    return v0
.end method

.method private hideLoading()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mLoadingView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;->onLoadCompleted()V

    :cond_0
    return-void
.end method


# virtual methods
.method public clearAll()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->clearAll()V

    :cond_0
    return-void
.end method

.method protected createView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const p3, 0x7f0e0062

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getAnimalItem(I)Lcom/android/settings/coolsound/data/MixMatchAnimalItem;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->getItem(I)Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getChangePositions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->changePositions:Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->changePositions:Ljava/util/Set;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->changePositions:Ljava/util/Set;

    return-object v0
.end method

.method protected initView(Landroid/view/View;)V
    .locals 3

    const v0, 0x7f0b02cf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListRcv:Lmiuix/recyclerview/widget/RecyclerView;

    const v0, 0x7f0b03c0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mLoadingView:Landroid/view/View;

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListRcv:Lmiuix/recyclerview/widget/RecyclerView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListRcv:Lmiuix/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->getSpanCount(Landroid/content/Context;)I

    move-result p1

    iput p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mSpanCount:I

    new-instance p1, Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mSpanCount:I

    invoke-direct {p1, v1, v2}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListRcv:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    new-instance p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

    invoke-virtual {p1, v1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->registerListener(Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListRcv:Lmiuix/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListRcv:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    new-instance p1, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0703f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070455

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mSpanCount:I

    invoke-direct {p1, v0, v1, v2}, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;-><init>(III)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070456

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070457

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v0, v2, v2}, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->setMargin(IIII)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListRcv:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$f;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListRcv:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/misettings/common/base/BaseActivity;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/misettings/common/base/BaseActivity;

    invoke-virtual {v0}, Lcom/misettings/common/base/BaseActivity;->isClassicalNavBar()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07040d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    :goto_0
    iput v2, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListRcv:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method

.method protected lazyLoad()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mDefaultList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->setData(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListRcv:Lmiuix/recyclerview/widget/RecyclerView;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const-string v2, "alpha"

    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v1, 0x15e

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->hideLoading()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public notifyDataSetChanged()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mHasDataChanged:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mHasDataChanged:Z

    :cond_0
    return-void
.end method

.method public recordCurrentSelectPositions()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->getChangePositions()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mDefaultList:Ljava/util/List;

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mDefaultList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mDefaultList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    iget-boolean v2, v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->added:Z

    if-eqz v2, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected refreshLoad()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mHasDataChanged:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mDefaultList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->setData(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->hideLoading()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mHasDataChanged:Z

    :cond_0
    return-void
.end method

.method public refreshOnlyItemChange(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->getChangePositions()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p1, :cond_5

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mDefaultList:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    if-nez v2, :cond_0

    goto :goto_4

    :cond_0
    move v2, v1

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    move v3, v1

    :goto_1
    iget-object v4, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mDefaultList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    iget v4, v4, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalIconRes:I

    iget-object v5, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mDefaultList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    iget v5, v5, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalIconRes:I

    if-ne v4, v5, :cond_1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(ILjava/lang/Object;)V

    goto :goto_3

    :cond_4
    iput-boolean v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mHasDataChanged:Z

    return-void

    :cond_5
    :goto_4
    iput-boolean v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mHasDataChanged:Z

    return-void
.end method

.method public registerListener(Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->registerListener(Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;)V

    :cond_0
    return-void
.end method

.method public setDataChanged(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mHasDataChanged:Z

    return-void
.end method

.method public setPlaying(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->setPlaying(IZ)V

    return-void
.end method

.method public tryRefresh(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)Z
    .locals 4

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    invoke-virtual {v2}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->getItemCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    invoke-virtual {v2, v0}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->getItem(I)Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object v2

    iget-object v2, v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->icon:Landroid/graphics/Bitmap;

    iget-object v3, p1, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->icon:Landroid/graphics/Bitmap;

    if-ne v2, v3, :cond_1

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->mAnimalListAdapter:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    const/4 p1, 0x1

    return p1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v1
.end method
