.class Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$4;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->playSound(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

.field final synthetic val$soundPath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$4;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    iput-object p2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$4;->val$soundPath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$4;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1600(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$4;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1600(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$4;->val$soundPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$4;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1600(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Landroid/media/MediaPlayer;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$4;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1600(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$4;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1600(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "playSound: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CoolSoundMixMatchSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method
