.class public Lcom/android/settings/coolsound/widget/MixMatchAnimalView;
.super Landroidx/constraintlayout/widget/ConstraintLayout;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/widget/MixMatchAnimalView$AnimalSelectListener;
    }
.end annotation


# static fields
.field private static final HIDE_ADD_DELAY:I = 0xbb8

.field private static final HIDE_PLAYING_DELAY:I = 0x5dc

.field private static final MSG_HIDE_DELETE:I = 0x400

.field private static final MSG_HIDE_PLAYING:I = 0x1000


# instance fields
.field animatorSet:Landroid/animation/AnimatorSet;

.field private mAnimalSeed:I

.field private mAnimator:Landroid/animation/ValueAnimator;

.field private mCoverView:Landroid/view/View;

.field private mDeleteBtn:Landroid/widget/ImageButton;

.field private mEntry:Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

.field private final mHandler:Landroid/os/Handler;

.field private mIconView:Landroid/widget/ImageView;

.field private mListener:Lcom/android/settings/coolsound/widget/MixMatchAnimalView$AnimalSelectListener;

.field private mPlayView:Lcom/android/settings/coolsound/widget/PlayView;

.field private mRootView:Landroid/view/View;

.field private mScale:F

.field private mShakeStoped:Z

.field private mTargetPositionX:F

.field private mTargetPositionY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->initView()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->initValueAnimator()V

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mShakeStoped:Z

    return p0
.end method

.method static synthetic access$100(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)Landroid/animation/ValueAnimator;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimator:Landroid/animation/ValueAnimator;

    return-object p0
.end method

.method private getShakePosition(DF)[F
    .locals 10

    float-to-double v0, p3

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    div-double v2, p1, v2

    add-double/2addr v2, v0

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    const-wide v6, 0x4066800000000000L    # 180.0

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    mul-double/2addr v2, v8

    double-to-float p3, v2

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    div-double/2addr p1, v2

    add-double/2addr v0, p1

    mul-double/2addr v0, v4

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide p1

    mul-double/2addr p1, v8

    double-to-float p1, p1

    const/4 p2, 0x2

    new-array p2, p2, [F

    const/4 v0, 0x0

    aput p3, p2, v0

    const/4 p3, 0x1

    aput p1, p2, p3

    return-object p2
.end method

.method private initValueAnimator()V
    .locals 4

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x4076800000000000L    # 360.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimalSeed:I

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private initView()V
    .locals 3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0e008e

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0b03b4

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mIconView:Landroid/widget/ImageView;

    const v0, 0x7f0b008c

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mDeleteBtn:Landroid/widget/ImageButton;

    const v0, 0x7f0b03bb

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mCoverView:Landroid/view/View;

    const v0, 0x7f0b02cc

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/widget/PlayView;

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mPlayView:Lcom/android/settings/coolsound/widget/PlayView;

    const v0, 0x7f0b03c3

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mRootView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mDeleteBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mRootView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mRootView:Landroid/view/View;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mRootView:Landroid/view/View;

    new-array v2, v2, [Lmiuix/animation/a/a;

    invoke-interface {v0, v1, v2}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    return-void
.end method

.method private sendHideDeleteMsg()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method private sendHidePlayingMsg()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method private setDeleteLayoutParams()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mDeleteBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;->q:I

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mDeleteBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public synthetic a([ILandroid/animation/ValueAnimator;)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mShakeStoped:Z

    if-nez v0, :cond_1

    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p2

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    aget v0, p1, p2

    add-int/lit8 v0, v0, 0x3

    aput v0, p1, p2

    aget p1, p1, p2

    int-to-double v0, p1

    iget p1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimalSeed:I

    int-to-float p1, p1

    invoke-direct {p0, v0, v1, p1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->getShakePosition(DF)[F

    move-result-object p1

    aget p2, p1, p2

    iget v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mTargetPositionX:F

    add-float/2addr p2, v0

    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->setX(F)V

    const/4 p2, 0x1

    aget p1, p1, p2

    iget p2, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mTargetPositionY:F

    add-float/2addr p1, p2

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->setY(F)V

    :cond_1
    :goto_0
    return-void
.end method

.method public getEntry()Lcom/android/settings/coolsound/data/MixMatchAnimalItem;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mEntry:Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    return-object v0
.end method

.method public getScale()F
    .locals 1

    iget v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mScale:F

    return v0
.end method

.method public getTargetX()F
    .locals 1

    iget v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mTargetPositionX:F

    return v0
.end method

.method public getTargetY()F
    .locals 1

    iget v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mTargetPositionY:F

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2
    .param p1    # Landroid/os/Message;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x1

    const/16 v1, 0x400

    if-ne p1, v1, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mDeleteBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setAnimalScale(F)V

    return v0

    :cond_0
    const/16 v1, 0x1000

    if-ne p1, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->hidePlayView()V

    return v0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public hidePlayView()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mCoverView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mPlayView:Lcom/android/settings/coolsound/widget/PlayView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b03c3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mListener:Lcom/android/settings/coolsound/widget/MixMatchAnimalView$AnimalSelectListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView$AnimalSelectListener;->onAnimalSelected(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->sendHideDeleteMsg()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->sendHidePlayingMsg()V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0b008c

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mListener:Lcom/android/settings/coolsound/widget/MixMatchAnimalView$AnimalSelectListener;

    if-eqz p1, :cond_1

    invoke-interface {p1, p0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView$AnimalSelectListener;->onDeleteClicked(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setDeleteLayoutParams()V

    return-void
.end method

.method public registerAnimalSelectListener(Lcom/android/settings/coolsound/widget/MixMatchAnimalView$AnimalSelectListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mListener:Lcom/android/settings/coolsound/widget/MixMatchAnimalView$AnimalSelectListener;

    return-void
.end method

.method public resetBitmap()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mEntry:Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->icon:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setImageBitMap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public setAnimalScale(F)V
    .locals 4

    new-instance v0, Lmiuix/animation/b/a;

    const-string v1, "scale"

    invoke-direct {v0, v1}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    float-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object p1, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {v0, p1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    const/4 p1, 0x1

    new-array p1, p1, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p0, p1, v1

    invoke-static {p1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p1

    new-array v1, v1, [Lmiuix/animation/a/a;

    invoke-interface {p1, v0, v1}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-void
.end method

.method public setEntry(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mEntry:Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget p1, p1, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalNameRes:I

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->setTalkBackTag(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setImageBitMap(Landroid/graphics/Bitmap;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mCoverView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    iget p1, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    return-void
.end method

.method public setTalkBackTag(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mRootView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTarget(FFF)V
    .locals 0

    iput p1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mTargetPositionX:F

    iput p2, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mTargetPositionY:F

    iput p3, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mScale:F

    return-void
.end method

.method public showDelete(Z)V
    .locals 5

    const v0, 0x3f59999a    # 0.85f

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mDeleteBtn:Landroid/widget/ImageButton;

    invoke-virtual {p1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    new-array p1, v1, [Landroid/view/View;

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mDeleteBtn:Landroid/widget/ImageButton;

    aput-object v3, p1, v2

    invoke-static {p1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->b()Lmiuix/animation/o;

    move-result-object p1

    new-array v3, v1, [Lmiuix/animation/o$a;

    sget-object v4, Lmiuix/animation/o$a;->a:Lmiuix/animation/o$a;

    aput-object v4, v3, v2

    invoke-interface {p1, v0, v3}, Lmiuix/animation/o;->a(F[Lmiuix/animation/o$a;)Lmiuix/animation/o;

    new-array v0, v1, [Lmiuix/animation/a/a;

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lmiuix/animation/o;->e([Lmiuix/animation/a/a;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mDeleteBtn:Landroid/widget/ImageButton;

    invoke-virtual {p1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result p1

    if-nez p1, :cond_1

    new-array p1, v1, [Landroid/view/View;

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mDeleteBtn:Landroid/widget/ImageButton;

    aput-object v3, p1, v2

    invoke-static {p1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->b()Lmiuix/animation/o;

    move-result-object p1

    new-array v3, v1, [Lmiuix/animation/o$a;

    sget-object v4, Lmiuix/animation/o$a;->b:Lmiuix/animation/o$a;

    aput-object v4, v3, v2

    invoke-interface {p1, v0, v3}, Lmiuix/animation/o;->a(F[Lmiuix/animation/o$a;)Lmiuix/animation/o;

    new-array v0, v1, [Lmiuix/animation/a/a;

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lmiuix/animation/o;->b([Lmiuix/animation/a/a;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public showPlayView()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mCoverView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mPlayView:Lcom/android/settings/coolsound/widget/PlayView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public startShaking()V
    .locals 18

    move-object/from16 v0, p0

    invoke-static {}, Lcom/android/settings/coolsound/CoolCommonUtils;->isLowDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mShakeStoped:Z

    const/4 v2, 0x1

    new-array v3, v2, [I

    aput v1, v3, v1

    const-wide/16 v4, 0x258

    aget v6, v3, v1

    int-to-double v6, v6

    iget v8, v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimalSeed:I

    int-to-float v8, v8

    invoke-direct {v0, v6, v7, v8}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->getShakePosition(DF)[F

    move-result-object v6

    aget v7, v6, v1

    aget v6, v6, v2

    new-instance v8, Landroid/animation/AnimatorSet;

    invoke-direct {v8}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v8, v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->animatorSet:Landroid/animation/AnimatorSet;

    iget-object v8, v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->animatorSet:Landroid/animation/AnimatorSet;

    const/4 v9, 0x2

    new-array v10, v9, [Landroid/animation/Animator;

    const/4 v11, 0x5

    new-array v12, v11, [F

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getX()F

    move-result v13

    aput v13, v12, v1

    iget v13, v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mTargetPositionX:F

    aput v13, v12, v2

    const/high16 v14, 0x40400000    # 3.0f

    div-float v15, v7, v14

    add-float/2addr v15, v13

    aput v15, v12, v9

    const/high16 v15, 0x40000000    # 2.0f

    div-float v16, v7, v15

    add-float v16, v13, v16

    const/16 v17, 0x3

    aput v16, v12, v17

    add-float/2addr v13, v7

    const/4 v7, 0x4

    aput v13, v12, v7

    const-string v13, "translationX"

    invoke-static {v0, v13, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v12

    invoke-virtual {v12, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v12

    aput-object v12, v10, v1

    new-array v11, v11, [F

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getY()F

    move-result v12

    aput v12, v11, v1

    iget v1, v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mTargetPositionY:F

    aput v1, v11, v2

    div-float v12, v6, v14

    add-float/2addr v12, v1

    aput v12, v11, v9

    div-float v9, v6, v15

    add-float/2addr v9, v1

    aput v9, v11, v17

    add-float/2addr v1, v6

    aput v1, v11, v7

    const-string v1, "translationY"

    invoke-static {v0, v1, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    aput-object v1, v10, v2

    invoke-virtual {v8, v10}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v1, v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/android/settings/coolsound/widget/c;

    invoke-direct {v2, v0, v3}, Lcom/android/settings/coolsound/widget/c;-><init>(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;[I)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v1, v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->animatorSet:Landroid/animation/AnimatorSet;

    new-instance v2, Lcom/android/settings/coolsound/widget/MixMatchAnimalView$1;

    invoke-direct {v2, v0}, Lcom/android/settings/coolsound/widget/MixMatchAnimalView$1;-><init>(Lcom/android/settings/coolsound/widget/MixMatchAnimalView;)V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v1, v0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method public stopShaking()V
    .locals 1

    invoke-static {}, Lcom/android/settings/coolsound/CoolCommonUtils;->isLowDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mShakeStoped:Z

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->animatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/MixMatchAnimalView;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    :cond_2
    return-void
.end method
