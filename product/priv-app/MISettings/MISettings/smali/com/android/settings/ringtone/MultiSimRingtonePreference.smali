.class public Lcom/android/settings/ringtone/MultiSimRingtonePreference;
.super Lmiuix/preference/TextPreference;


# instance fields
.field private final MSG_UPDATE_TITLE:I

.field private final MSG_UPDATE_VALUE:I

.field private mExtraRingtoneType:I

.field private mHandler:Landroid/os/Handler;

.field private mUpdateUIRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lmiuix/preference/TextPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->MSG_UPDATE_TITLE:I

    const/4 p2, 0x1

    iput p2, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->MSG_UPDATE_VALUE:I

    new-instance v0, Lcom/android/settings/ringtone/MultiSimRingtonePreference$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/ringtone/MultiSimRingtonePreference$1;-><init>(Lcom/android/settings/ringtone/MultiSimRingtonePreference;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/ringtone/MultiSimRingtonePreference$2;

    invoke-direct {v0, p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference$2;-><init>(Lcom/android/settings/ringtone/MultiSimRingtonePreference;)V

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mUpdateUIRunnable:Ljava/lang/Runnable;

    iput p1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->setShowRightArrow(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/ringtone/MultiSimRingtonePreference;)Landroid/util/Pair;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getRingtoneTitle()Landroid/util/Pair;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/android/settings/ringtone/MultiSimRingtonePreference;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$200(Lcom/android/settings/ringtone/MultiSimRingtonePreference;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getRingtoneValue()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private getDeviceSlotID()I
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->isSlot1Position()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->isSlot2Position()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    sget v0, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    return v0
.end method

.method private getRingtoneIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->isSlot1Position()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0805e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->isSlot2Position()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0805e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private getRingtoneTitle()Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget v0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f130296

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f1302c5

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/16 v2, 0x10

    if-ne v0, v2, :cond_2

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f1302c6

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getDeviceSlotID()I

    move-result v2

    invoke-virtual {v0, v2}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v0

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_0

    :cond_3
    move-object v0, v1

    :goto_0
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method private getRingtoneValue()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    invoke-static {v0, v1}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isSlot1Position()Z
    .locals 2

    iget v0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    const/16 v1, 0x40

    if-eq v0, v1, :cond_1

    const/16 v1, 0x400

    if-eq v0, v1, :cond_1

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isSlot2Position()Z
    .locals 2

    iget v0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    const/16 v1, 0x80

    if-eq v0, v1, :cond_1

    const/16 v1, 0x800

    if-eq v0, v1, :cond_1

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private setShowRightArrow(Ljava/lang/Boolean;)V
    .locals 0

    return-void
.end method


# virtual methods
.method getRingtonePickerIntent()Landroid/content/Intent;
    .locals 7

    goto/32 :goto_a

    nop

    :goto_0
    const/4 v5, 0x1

    goto/32 :goto_3

    nop

    :goto_1
    const-string v1, "com.android.thememanager"

    goto/32 :goto_20

    nop

    :goto_2
    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/32 :goto_e

    nop

    :goto_3
    if-ne v1, v5, :cond_0

    goto/32 :goto_2a

    :cond_0
    goto/32 :goto_14

    nop

    :goto_4
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_30

    nop

    :goto_5
    invoke-static {v1, v0}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->addMiuiNaturalSound(Landroid/content/Context;Landroid/content/Intent;)V

    goto/32 :goto_1d

    nop

    :goto_6
    if-ne v1, v6, :cond_1

    goto/32 :goto_27

    :cond_1
    goto/32 :goto_7

    nop

    :goto_7
    const/16 v6, 0x200

    goto/32 :goto_f

    nop

    :goto_8
    if-ne v1, v6, :cond_2

    goto/32 :goto_27

    :cond_2
    goto/32 :goto_22

    nop

    :goto_9
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/32 :goto_29

    nop

    :goto_a
    new-instance v0, Landroid/content/Intent;

    goto/32 :goto_1c

    nop

    :goto_b
    if-ne v1, v6, :cond_3

    goto/32 :goto_27

    :cond_3
    goto/32 :goto_1f

    nop

    :goto_c
    const/4 v2, 0x0

    goto/32 :goto_28

    nop

    :goto_d
    iget v2, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    goto/32 :goto_11

    nop

    :goto_e
    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/32 :goto_25

    nop

    :goto_f
    if-ne v1, v6, :cond_4

    goto/32 :goto_27

    :cond_4
    goto/32 :goto_13

    nop

    :goto_10
    const-string v2, "android.intent.extra.ringtone.DEFAULT_URI"

    goto/32 :goto_9

    nop

    :goto_11
    invoke-static {v1, v2}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundSettingUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v1

    goto/32 :goto_1e

    nop

    :goto_12
    if-ne v1, v6, :cond_5

    goto/32 :goto_27

    :cond_5
    goto/32 :goto_26

    nop

    :goto_13
    const/16 v6, 0x400

    goto/32 :goto_b

    nop

    :goto_14
    const/16 v6, 0x8

    goto/32 :goto_24

    nop

    :goto_15
    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    goto/32 :goto_2

    nop

    :goto_16
    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/32 :goto_31

    nop

    :goto_17
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2b

    nop

    :goto_18
    const/16 v6, 0x10

    goto/32 :goto_8

    nop

    :goto_19
    if-ne v1, v6, :cond_6

    goto/32 :goto_2a

    :cond_6
    goto/32 :goto_2c

    nop

    :goto_1a
    const-string v4, "android.intent.extra.ringtone.TYPE"

    goto/32 :goto_0

    nop

    :goto_1b
    if-ne v1, v6, :cond_7

    goto/32 :goto_2a

    :cond_7
    goto/32 :goto_23

    nop

    :goto_1c
    const-string v1, "miui.intent.action.RINGTONE_PICKER"

    goto/32 :goto_17

    nop

    :goto_1d
    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    goto/32 :goto_10

    nop

    :goto_1e
    const-string v2, "android.intent.extra.ringtone.EXISTING_URI"

    goto/32 :goto_2e

    nop

    :goto_1f
    const/16 v6, 0x800

    goto/32 :goto_12

    nop

    :goto_20
    const-string v2, "com.android.thememanager.activity.ThemeTabActivity"

    goto/32 :goto_4

    nop

    :goto_21
    const-string v1, "android.intent.extra.ringtone.SHOW_SILENT"

    goto/32 :goto_2f

    nop

    :goto_22
    const/16 v6, 0x40

    goto/32 :goto_1b

    nop

    :goto_23
    const/16 v6, 0x80

    goto/32 :goto_19

    nop

    :goto_24
    if-ne v1, v6, :cond_8

    goto/32 :goto_27

    :cond_8
    goto/32 :goto_18

    nop

    :goto_25
    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_26
    goto :goto_32

    :goto_27
    goto/32 :goto_15

    nop

    :goto_28
    const-string v3, "android.intent.extra.ringtone.SHOW_DEFAULT"

    goto/32 :goto_1a

    nop

    :goto_29
    goto :goto_32

    :goto_2a
    goto/32 :goto_33

    nop

    :goto_2b
    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    goto/32 :goto_c

    nop

    :goto_2c
    const/16 v6, 0x100

    goto/32 :goto_6

    nop

    :goto_2d
    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_2e
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/32 :goto_21

    nop

    :goto_2f
    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/32 :goto_1

    nop

    :goto_30
    return-object v0

    :goto_31
    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_32
    goto/32 :goto_2d

    nop

    :goto_33
    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    goto/32 :goto_16

    nop
.end method

.method public onBindViewHolder(Landroidx/preference/B;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/preference/TextPreference;->onBindViewHolder(Landroidx/preference/B;)V

    const v0, 0x7f0b0366

    invoke-virtual {p1, v0}, Landroidx/preference/B;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setGravity(I)V

    :cond_0
    return-void
.end method

.method onSaveRingtone(Landroid/net/Uri;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-static {v0, v1, p1}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->saveDefaultSound(Landroid/content/Context;ILandroid/net/Uri;)V

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    goto/32 :goto_0

    nop
.end method

.method public updateUI(I)V
    .locals 1

    iget v0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mExtraRingtoneType:I

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getRingtoneIcon()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    new-instance p1, Ljava/lang/Thread;

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->mUpdateUIRunnable:Ljava/lang/Runnable;

    invoke-direct {p1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    return-void
.end method
