.class public Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$UpdateCallback;,
        Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;,
        Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;
    }
.end annotation


# static fields
.field private static final MSG_RINGTONE_CHANGE:I = 0x0

.field private static final MSG_RINGTONE_SLOT1_CHANGE:I = 0x1

.field private static final MSG_RINGTONE_SLOT2_CHANGE:I = 0x2

.field private static final MSG_RINGTONE_UNIFORM_CHANGE:I = 0x3

.field private static final MSG_SMS_RINGTONE_CHANGE:I = 0x4

.field private static final MSG_SMS_RINGTONE_SLOT1_CHANGE:I = 0x5

.field private static final TAG:Ljava/lang/String; = "RingtoneChangeHelper"

.field private static updateCallback:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$UpdateCallback;


# instance fields
.field private mObserver:Landroid/database/ContentObserver;

.field private mObserverHandler:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

.field mRingtoneSoundUseUniform:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->isUriEqual(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$100()Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$UpdateCallback;
    .locals 1

    sget-object v0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->updateCallback:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$UpdateCallback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;)Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mObserverHandler:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    return-object p0
.end method

.method public static init(Lcom/xiaomi/misettings/Application;)V
    .locals 1

    new-instance v0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;

    invoke-direct {v0}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;-><init>()V

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->registerObserver(Lcom/xiaomi/misettings/Application;)V

    return-void
.end method

.method private static isUriEqual(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 1

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 v0, 0x0

    if-nez p0, :cond_1

    if-eqz p1, :cond_1

    return v0

    :cond_1
    if-eqz p0, :cond_2

    invoke-virtual {p0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0

    :cond_2
    return v0
.end method

.method private registerObserver(Lcom/xiaomi/misettings/Application;)V
    .locals 4

    new-instance v0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    invoke-virtual {p1}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mObserverHandler:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    new-instance v0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;

    iget-object v1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mObserverHandler:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$SettingsObserver;-><init>(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {p1}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$System;->DEFAULT_RINGTONE_URI_SLOT_1:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p1}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$System;->DEFAULT_RINGTONE_URI_SLOT_2:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p1}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const-string v0, "ringtone_sound_use_uniform"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mRingtoneSoundUseUniform:Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mRingtoneSoundUseUniform:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p1}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$System;->DEFAULT_SMS_RECEIVED_RINGTONE_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p1}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    sget-object v0, Landroid/provider/MiuiSettings$System;->DEFAULT_SMS_RECEIVED_SOUND_URI_SLOT_1:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {p1, v0, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public static registerUpdateCallback(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$UpdateCallback;)V
    .locals 0

    sput-object p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->updateCallback:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$UpdateCallback;

    return-void
.end method

.method public static removeUpdateCallback()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->updateCallback:Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$UpdateCallback;

    return-void
.end method
