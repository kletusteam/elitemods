.class public Lcom/android/settings/coolsound/data/CoolSoundResource;
.super Ljava/lang/Object;


# instance fields
.field private description:Ljava/lang/String;

.field private dynamicSoundName:Ljava/lang/String;

.field private lastDynamicIndex:I

.field private showRes:I

.field private showType:I

.field private soundPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->lastDynamicIndex:I

    iput p1, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->showRes:I

    iput p2, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->showType:I

    iput-object p3, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->description:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->soundPath:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->dynamicSoundName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDynamicSound()Ljava/lang/String;
    .locals 7

    iget-object v0, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->dynamicSoundName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->soundPath:Ljava/lang/String;

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->dynamicSoundName:Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/system/media/audio/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_3

    :cond_2
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    iget v2, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->lastDynamicIndex:I

    if-eq v1, v2, :cond_2

    iput v1, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->lastDynamicIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->soundPath:Ljava/lang/String;

    return-object v0
.end method

.method public getShowRes()I
    .locals 1

    iget v0, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->showRes:I

    return v0
.end method

.method public getShowType()I
    .locals 1

    iget v0, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->showType:I

    return v0
.end method

.method public getSoundPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/data/CoolSoundResource;->soundPath:Ljava/lang/String;

    return-object v0
.end method
