.class Lcom/android/settings/coolsound/widget/AnimalItem$CornerOutlineProvider;
.super Landroid/view/ViewOutlineProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/coolsound/widget/AnimalItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CornerOutlineProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/AnimalItem;


# direct methods
.method private constructor <init>(Lcom/android/settings/coolsound/widget/AnimalItem;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/AnimalItem$CornerOutlineProvider;->this$0:Lcom/android/settings/coolsound/widget/AnimalItem;

    invoke-direct {p0}, Landroid/view/ViewOutlineProvider;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/coolsound/widget/AnimalItem;Lcom/android/settings/coolsound/widget/AnimalItem$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/AnimalItem$CornerOutlineProvider;-><init>(Lcom/android/settings/coolsound/widget/AnimalItem;)V

    return-void
.end method


# virtual methods
.method public getOutline(Landroid/view/View;Landroid/graphics/Outline;)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/AnimalItem$CornerOutlineProvider;->this$0:Lcom/android/settings/coolsound/widget/AnimalItem;

    invoke-static {p1}, Lcom/android/settings/coolsound/widget/AnimalItem;->access$100(Lcom/android/settings/coolsound/widget/AnimalItem;)F

    move-result v5

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Outline;->setRoundRect(IIIIF)V

    return-void
.end method
