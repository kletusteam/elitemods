.class public Lcom/android/settings/coolsound/widget/AnimalItem;
.super Landroid/widget/LinearLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/widget/AnimalItem$CornerOutlineProvider;
    }
.end annotation


# instance fields
.field private corner:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/AnimalItem;->initOutline(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/AnimalItem;->initOutline(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/AnimalItem;->initOutline(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/settings/coolsound/widget/AnimalItem;)F
    .locals 0

    iget p0, p0, Lcom/android/settings/coolsound/widget/AnimalItem;->corner:F

    return p0
.end method

.method private initOutline(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0700ae

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/android/settings/coolsound/widget/AnimalItem;->corner:F

    iget p1, p0, Lcom/android/settings/coolsound/widget/AnimalItem;->corner:F

    const/4 v0, 0x0

    cmpl-float p1, p1, v0

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setClipToOutline(Z)V

    new-instance p1, Lcom/android/settings/coolsound/widget/AnimalItem$CornerOutlineProvider;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/android/settings/coolsound/widget/AnimalItem$CornerOutlineProvider;-><init>(Lcom/android/settings/coolsound/widget/AnimalItem;Lcom/android/settings/coolsound/widget/AnimalItem$1;)V

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    :cond_0
    return-void
.end method
