.class Lcom/android/settings/coolsound/widget/RingtoneGridView$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateSelectView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iput-object p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiui/util/SimRingtoneUtils;->isDefaultSoundUniform(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$000(Lcom/android/settings/coolsound/widget/RingtoneGridView;)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const v0, 0x7f0800b1

    goto :goto_0

    :cond_0
    const v0, 0x7f0800d7

    :goto_0
    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$100(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$100(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/view/View;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$100(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$100(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v2, v4, v5}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updatePlayView(Landroid/view/View;I)V

    :cond_1
    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iget-object v4, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->val$view:Landroid/view/View;

    invoke-static {v2, v4}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$102(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/view/View;)Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->val$view:Landroid/view/View;

    if-eqz v2, :cond_2

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->val$view:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$1;->val$view:Landroid/view/View;

    invoke-virtual {v0, v1, v3}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updatePlayView(Landroid/view/View;I)V

    :cond_2
    return-void
.end method
