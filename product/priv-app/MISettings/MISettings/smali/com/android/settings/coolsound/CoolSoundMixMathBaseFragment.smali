.class public abstract Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;
.super Landroidx/fragment/app/Fragment;


# instance fields
.field protected mCurrentType:Ljava/lang/String;

.field protected mDefaultList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mHasDataChanged:Z

.field private mIsCreateView:Z

.field private mIsLoaded:Z

.field public mIsVisible:Z

.field protected mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

.field private mRoot:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract createView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public getCurrentType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mCurrentType:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract initView(Landroid/view/View;)V
.end method

.method protected abstract lazyLoad()V
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mRoot:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->createView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mRoot:Landroid/view/View;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mIsCreateView:Z

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mRoot:Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->initView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->onVisible()V

    :cond_0
    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mRoot:Landroid/view/View;

    return-object p1
.end method

.method protected onInvisible()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mIsVisible:Z

    return-void
.end method

.method protected onVisible()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mIsVisible:Z

    iget-boolean v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mIsLoaded:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->refreshLoad()V

    :cond_0
    iget-boolean v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mIsLoaded:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mIsCreateView:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mIsLoaded:Z

    invoke-virtual {p0}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->lazyLoad()V

    :cond_1
    return-void
.end method

.method protected refreshLoad()V
    .locals 0

    return-void
.end method

.method public setData(Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0, p2}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->setType(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mDefaultList:Ljava/util/List;

    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->mCurrentType:Ljava/lang/String;

    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->setUserVisibleHint(Z)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getUserVisibleHint()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->onVisible()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->onInvisible()V

    :goto_0
    return-void
.end method
