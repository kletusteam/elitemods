.class public Lcom/android/settings/coolsound/data/ResourceWrapper;
.super Ljava/lang/Object;


# static fields
.field public static AREA_BG_SOUND:[I = null

.field public static AREA_CARD_BG:[I = null

.field public static final RES_BASE_PATH:Ljava/lang/String; = "/system/media/audio/"

.field public static final RES_TYPE_ALARM:I = 0x3

.field public static final RES_TYPE_CALENDAR:I = 0x4

.field public static final RES_TYPE_NOTIFICATION:I = 0x5

.field public static final RES_TYPE_RINGTONE:I = 0x0

.field public static final RES_TYPE_RINGTONE_SLOT_1:I = 0x1

.field public static final RES_TYPE_RINGTONE_SLOT_2:I = 0x2

.field public static final SHOW_TYPE_IMAGE:I = 0x1

.field public static final SHOW_TYPE_VIDEO:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ResourceWrapper"

.field public static final VIDEO_RES_SOURCE_PKG:Ljava/lang/String; = "com.xiaomi.misettings"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x4

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/settings/coolsound/data/ResourceWrapper;->AREA_BG_SOUND:[I

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/settings/coolsound/data/ResourceWrapper;->AREA_CARD_BG:[I

    return-void

    :array_0
    .array-data 4
        0x7f120001
        0x7f120000
        0x7f120004
        0x7f120003
    .end array-data

    :array_1
    .array-data 4
        0x7f06001d
        0x7f06001c
        0x7f060030
        0x7f06002c
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static ensureResource()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static getAllAreaSound(Landroid/content/Context;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v1, 0x7f03000a

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p0

    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p0, v2

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static getAnimalSounds(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v0, 0x7f03000a

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p0

    if-ltz p1, :cond_0

    array-length v0, p0

    if-ge p1, v0, :cond_0

    aget-object p0, p0, p1

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static loadAreaAnimalResource(Landroid/content/Context;I)Lcom/android/settings/coolsound/data/AreaResource;
    .locals 6

    invoke-static {p0}, Lcom/android/settings/coolsound/data/ParseAnimalDataUtil;->init(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settings/coolsound/data/ParseAnimalDataUtil;->getAreaAnimalInfo(I)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v2, 0x7f03001f

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p0

    sget-object v2, Lcom/android/settings/coolsound/data/ResourceWrapper;->AREA_BG_SOUND:[I

    aget v3, v2, p1

    if-ltz p1, :cond_0

    array-length v4, p0

    if-ge p1, v4, :cond_0

    array-length v4, v1

    array-length v5, p0

    if-ne v4, v5, :cond_0

    array-length v4, v1

    array-length v2, v2

    if-ne v4, v2, :cond_0

    new-instance v2, Lcom/android/settings/coolsound/data/AreaResource;

    aget-object v1, v1, p1

    aget-object p0, p0, p1

    invoke-direct {v2, v1, p0, v0, v3}, Lcom/android/settings/coolsound/data/AreaResource;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V

    return-object v2

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static loadAreaBg(Landroid/content/Context;I)I
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object p0

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p0

    return p0
.end method

.method public static loadAreaCardBg(I)I
    .locals 2

    if-ltz p0, :cond_0

    sget-object v0, Lcom/android/settings/coolsound/data/ResourceWrapper;->AREA_CARD_BG:[I

    array-length v1, v0

    if-ge p0, v1, :cond_0

    aget p0, v0, p0

    return p0

    :cond_0
    sget-object p0, Lcom/android/settings/coolsound/data/ResourceWrapper;->AREA_CARD_BG:[I

    const/4 v0, 0x0

    aget p0, p0, v0

    return p0
.end method

.method private static loadDynamicSoundResource(Landroid/content/Context;I)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    if-eq p1, v2, :cond_0

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    const/4 v2, 0x3

    if-eq p1, v2, :cond_0

    const/4 v2, 0x4

    if-eq p1, v2, :cond_2

    const/4 v2, 0x5

    if-eq p1, v2, :cond_1

    :cond_0
    move p1, v1

    goto :goto_0

    :cond_1
    const p1, 0x7f03000c

    goto :goto_0

    :cond_2
    const p1, 0x7f03000b

    :goto_0
    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p0

    array-length p1, p0

    :goto_1
    if-ge v1, p1, :cond_3

    aget-object v2, p0, v1

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/android/settings/coolsound/data/ResourceWrapper;->ensureResource()Z

    move-result p0

    if-nez p0, :cond_4

    const-string p0, "ResourceWrapper"

    const-string p1, "ops! resources is not valid."

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-object v0
.end method

.method public static loadNatureAreaResource(Landroid/content/Context;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/CoolSoundResource;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/android/settings/coolsound/data/ResourceOverlay;->natureAreaAnimalImage:Ljava/util/List;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v2, 0x7f030020

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/coolsound/data/ShowResource;

    new-instance v10, Lcom/android/settings/coolsound/data/CoolSoundResource;

    iget v5, v3, Lcom/android/settings/coolsound/data/ShowResource;->resID:I

    iget v6, v3, Lcom/android/settings/coolsound/data/ShowResource;->type:I

    aget-object v7, p0, v2

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, v10

    invoke-direct/range {v4 .. v9}, Lcom/android/settings/coolsound/data/CoolSoundResource;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static loadResource(Landroid/content/Context;I)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/CoolSoundResource;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0, p1}, Lcom/android/settings/coolsound/data/ResourceWrapper;->loadSoundResource(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/android/settings/coolsound/data/ResourceWrapper;->loadDynamicSoundResource(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v2

    invoke-static {p0, p1}, Lcom/android/settings/coolsound/data/ResourceWrapper;->loadShowResource(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v3

    const-string v4, "ResourceWrapper"

    if-nez v3, :cond_0

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Couldn\'t load show resources for type="

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :cond_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result p1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-eq p1, v5, :cond_1

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "Show resources is not match the sound resources. showResIds.length="

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ",sounds.length="

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge p1, v4, :cond_3

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/4 v5, 0x0

    if-ltz p1, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-ge p1, v6, :cond_2

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    :cond_2
    move-object v11, v5

    invoke-static {}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->getInstance()Lcom/android/settings/coolsound/data/CoolSoundUtils;

    move-result-object v5

    invoke-virtual {v5, p0, v4}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->queryTitle(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/coolsound/data/ShowResource;

    new-instance v5, Lcom/android/settings/coolsound/data/CoolSoundResource;

    iget v7, v4, Lcom/android/settings/coolsound/data/ShowResource;->resID:I

    iget v8, v4, Lcom/android/settings/coolsound/data/ShowResource;->type:I

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Ljava/lang/String;

    move-object v6, v5

    invoke-direct/range {v6 .. v11}, Lcom/android/settings/coolsound/data/CoolSoundResource;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method private static loadShowResFromSettings(Landroid/content/Context;Ljava/lang/String;)[I
    .locals 6

    const-string v0, "com.xiaomi.misettings"

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const-string v3, "array"

    invoke-virtual {p0, p1, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p1

    array-length v3, p1

    new-array v3, v3, [I

    :goto_0
    array-length v4, p1

    if-ge v2, v4, :cond_0

    aget-object v4, p1, v2

    const-string v5, "raw"

    invoke-virtual {p0, v4, v5, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    aput v4, v3, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    move-object v1, v3

    :catch_0
    :cond_1
    return-object v1
.end method

.method private static loadShowResource(Landroid/content/Context;I)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/ShowResource;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_2

    const/4 p0, 0x1

    if-eq p1, p0, :cond_2

    const/4 p0, 0x2

    if-eq p1, p0, :cond_2

    const/4 p0, 0x4

    if-eq p1, p0, :cond_1

    const/4 p0, 0x5

    if-eq p1, p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/android/settings/coolsound/data/ResourceOverlay;->notificationOnlyImageVideo:Ljava/util/List;

    goto :goto_0

    :cond_1
    sget-object p0, Lcom/android/settings/coolsound/data/ResourceOverlay;->notificationImageVideo:Ljava/util/List;

    goto :goto_0

    :cond_2
    sget-object p0, Lcom/android/settings/coolsound/data/ResourceOverlay;->ringtoneImageVideo:Ljava/util/List;

    :goto_0
    return-object p0
.end method

.method public static loadSoundResource(Landroid/content/Context;I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const/4 v2, 0x1

    if-eq p1, v2, :cond_3

    const/4 v2, 0x2

    if-eq p1, v2, :cond_3

    const/4 v2, 0x3

    if-eq p1, v2, :cond_0

    const/4 v2, 0x4

    if-eq p1, v2, :cond_2

    const/4 v2, 0x5

    if-eq p1, v2, :cond_1

    :cond_0
    move p1, v1

    goto :goto_0

    :cond_1
    const p1, 0x7f03000f

    goto :goto_0

    :cond_2
    const p1, 0x7f03000e

    goto :goto_0

    :cond_3
    const p1, 0x7f030010

    :goto_0
    if-eqz p1, :cond_4

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p0

    array-length p1, p0

    :goto_1
    if-ge v1, p1, :cond_4

    aget-object v2, p0, v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/system/media/audio/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/android/settings/coolsound/data/ResourceWrapper;->ensureResource()Z

    move-result p0

    if-nez p0, :cond_5

    const-string p0, "ResourceWrapper"

    const-string p1, "ops! resources is not valid."

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return-object v0
.end method
