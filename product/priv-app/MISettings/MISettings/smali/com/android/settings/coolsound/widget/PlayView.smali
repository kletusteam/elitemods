.class public Lcom/android/settings/coolsound/widget/PlayView;
.super Landroid/view/View;


# instance fields
.field private mAxisX1:F

.field private mAxisX2:F

.field private mAxisX3:F

.field private mAxisX4:F

.field private mAxisX5:F

.field private mBottom:F

.field private mDelta1:I

.field private mDelta2:I

.field private mDelta3:I

.field private mDelta4:I

.field private mDelta5:I

.field private mHeight:I

.field private mLoop:Z

.field private mPaint:Landroid/graphics/Paint;

.field private mRadius:F

.field private mTop1:F

.field private mTop2:F

.field private mTop3:F

.field private mTop4:F

.field private mTop5:F

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/coolsound/widget/PlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/coolsound/widget/PlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta1:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta2:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta3:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta4:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta5:I

    const/4 p2, 0x0

    iput-boolean p2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mLoop:Z

    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method private loop()V
    .locals 4

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop1:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta1:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop1:F

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop2:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta2:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop2:F

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop3:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta3:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop3:F

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop4:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta4:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop4:F

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop5:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta5:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop5:F

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop1:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    cmpg-float v1, v0, v1

    const/4 v2, -0x1

    const/4 v3, 0x1

    if-gtz v1, :cond_0

    iput v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta1:I

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    iput v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta1:I

    :cond_1
    :goto_0
    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop2:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_2

    iput v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta2:I

    goto :goto_1

    :cond_2
    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    iput v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta2:I

    :cond_3
    :goto_1
    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop3:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_4

    iput v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta3:I

    goto :goto_2

    :cond_4
    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_5

    iput v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta3:I

    :cond_5
    :goto_2
    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop4:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_6

    iput v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta4:I

    goto :goto_3

    :cond_6
    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_7

    iput v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta4:I

    :cond_7
    :goto_3
    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop5:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_8

    iput v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta5:I

    goto :goto_4

    :cond_8
    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_9

    iput v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mDelta5:I

    :cond_9
    :goto_4
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mLoop:Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mLoop:Z

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX1:F

    iget v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop1:F

    iget v4, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    iget-object v5, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX1:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop1:F

    iget v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX1:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    iget v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v7, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX2:F

    iget v6, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop2:F

    iget v8, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    iget-object v9, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    move-object v4, p1

    move v5, v7

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX2:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop2:F

    iget v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX2:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    iget v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v7, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX3:F

    iget v6, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop3:F

    iget v8, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    iget-object v9, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    move v5, v7

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX3:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop3:F

    iget v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX3:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    iget v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v7, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX4:F

    iget v6, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop4:F

    iget v8, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    iget-object v9, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    move v5, v7

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX4:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop4:F

    iget v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX4:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    iget v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v7, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX5:F

    iget v6, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop5:F

    iget v8, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    iget-object v9, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    move v5, v7

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX5:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop5:F

    iget v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX5:F

    iget v1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    iget v2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-boolean p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mLoop:Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/PlayView;->loop()V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mWidth:I

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mHeight:I

    iget p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mWidth:I

    iget p2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mHeight:I

    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    iget p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mWidth:I

    int-to-double v0, p1

    const-wide/high16 v2, 0x4032000000000000L    # 18.0

    div-double/2addr v0, v2

    double-to-float p2, v0

    iput p2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    int-to-double v0, p1

    const-wide/high16 v2, 0x4022000000000000L    # 9.0

    div-double/2addr v0, v2

    const-wide/16 v4, 0x0

    mul-double/2addr v0, v4

    iget p2, p0, Lcom/android/settings/coolsound/widget/PlayView;->mRadius:F

    float-to-double v4, p2

    add-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX1:F

    int-to-double v0, p1

    div-double/2addr v0, v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v4

    float-to-double v4, p2

    add-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX2:F

    int-to-double v0, p1

    div-double/2addr v0, v2

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    mul-double/2addr v0, v4

    float-to-double v4, p2

    add-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX3:F

    int-to-double v0, p1

    div-double/2addr v0, v2

    const-wide/high16 v4, 0x4018000000000000L    # 6.0

    mul-double/2addr v0, v4

    float-to-double v4, p2

    add-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX4:F

    int-to-double v0, p1

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    mul-double/2addr v0, v2

    float-to-double v2, p2

    add-double/2addr v0, v2

    double-to-float p1, v0

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mAxisX5:F

    iget p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mHeight:I

    int-to-float v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    sub-float/2addr v0, p2

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop1:F

    int-to-float v0, p1

    const v1, 0x3f333333    # 0.7f

    mul-float/2addr v0, v1

    sub-float/2addr v0, p2

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop2:F

    int-to-float v0, p1

    const v1, 0x3e99999a    # 0.3f

    mul-float/2addr v0, v1

    sub-float/2addr v0, p2

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop3:F

    int-to-float v0, p1

    const v1, 0x3dcccccd    # 0.1f

    mul-float/2addr v0, v1

    sub-float/2addr v0, p2

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop4:F

    int-to-float v0, p1

    const v1, 0x3f666666    # 0.9f

    mul-float/2addr v0, v1

    sub-float/2addr v0, p2

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayView;->mTop5:F

    int-to-float p1, p1

    sub-float/2addr p1, p2

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mBottom:F

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/PlayView;->mPaint:Landroid/graphics/Paint;

    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr p2, v0

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method
