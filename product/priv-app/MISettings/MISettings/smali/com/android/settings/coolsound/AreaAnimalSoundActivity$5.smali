.class Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/coolsound/AreaAnimalSoundActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    invoke-static {v0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->access$100(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolCommonUtils;->isZenMode(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    invoke-static {p1}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->access$100(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    invoke-static {v0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->access$100(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f1302b8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v1}, Lcom/misettings/common/utils/r;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void

    :cond_0
    const/4 v0, -0x1

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    iget v0, v2, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->position:I

    iget-object v2, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->access$400(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;Z)V

    iget-object v2, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    iget-object v3, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    iget-object v3, v3, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v3, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result p1

    iput p1, v2, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->position:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getChildAdapterPosition error"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v2, "AreaAnimalSound"

    invoke-static {v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    invoke-static {p1}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->access$200(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)Lcom/android/settings/coolsound/MediaSoundPlayer;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    invoke-static {p1}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->access$200(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)Lcom/android/settings/coolsound/MediaSoundPlayer;

    move-result-object p1

    iget-object v2, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    invoke-static {v2}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->access$300(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    iget v3, v3, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->position:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/data/AnimalInfo;

    invoke-virtual {v2}, Lcom/android/settings/coolsound/data/AnimalInfo;->getSoundPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/android/settings/coolsound/MediaSoundPlayer;->startPlaySound(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    invoke-static {p1, v1}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->access$400(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;Z)V

    :cond_2
    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundActivity;

    iput v0, p1, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->position:I

    :cond_3
    return-void
.end method
