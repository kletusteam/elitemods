.class Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ViewHolder"
.end annotation


# instance fields
.field mAddBtn:Landroid/widget/Button;

.field mAnimalIcon:Landroid/widget/ImageView;

.field mAnimalName:Landroid/widget/TextView;

.field mCoverView:Landroid/view/View;

.field mDeleteBtn:Landroid/widget/Button;

.field mPlayView:Landroid/view/View;

.field mRootView:Landroid/view/View;

.field mSelectedView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$t;-><init>(Landroid/view/View;)V

    const v0, 0x7f0b023d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mAnimalIcon:Landroid/widget/ImageView;

    const v0, 0x7f0b0388

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mAnimalName:Landroid/widget/TextView;

    const v0, 0x7f0b03c3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mRootView:Landroid/view/View;

    const v0, 0x7f0b008b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mAddBtn:Landroid/widget/Button;

    const v0, 0x7f0b008c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mDeleteBtn:Landroid/widget/Button;

    const v0, 0x7f0b02cc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mPlayView:Landroid/view/View;

    const v0, 0x7f0b03c4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mSelectedView:Landroid/view/View;

    const v0, 0x7f0b03bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mCoverView:Landroid/view/View;

    return-void
.end method
