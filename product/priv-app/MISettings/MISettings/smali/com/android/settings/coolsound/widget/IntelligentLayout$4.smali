.class Lcom/android/settings/coolsound/widget/IntelligentLayout$4;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/widget/IntelligentLayout;->playSound()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/IntelligentLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$4;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$4;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->access$500(Lcom/android/settings/coolsound/widget/IntelligentLayout;Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$4;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->pauseVideo(Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$4;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->access$600(Lcom/android/settings/coolsound/widget/IntelligentLayout;)Lcom/android/settings/coolsound/widget/AudioFocusRunnable;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/settings/coolsound/widget/AudioFocusRunnable;->postAudioFocusRunnable(Landroid/view/View;Landroid/media/MediaPlayer;Lcom/android/settings/coolsound/widget/AudioFocusRunnable;)V

    return-void
.end method
