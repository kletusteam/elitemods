.class public Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;
.super Landroidx/fragment/app/ha;


# instance fields
.field private fragmentManager:Landroidx/fragment/app/FragmentManager;

.field private mFragments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;",
            ">;"
        }
    .end annotation
.end field

.field private tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroidx/fragment/app/FragmentManager;)V
    .locals 0

    invoke-direct {p0, p1}, Landroidx/fragment/app/ha;-><init>(Landroidx/fragment/app/FragmentManager;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->tags:Ljava/util/List;

    return-void
.end method

.method private static makeFragmentName(IJ)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android:switcher:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ":"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    iget-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/fragment/app/Fragment;

    iget-object p2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {p2}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroidx/fragment/app/qa;->c(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {p2}, Landroidx/fragment/app/qa;->b()I

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->mFragments:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public bridge synthetic getItem(I)Landroidx/fragment/app/Fragment;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->getItem(I)Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    move-result-object p1

    return-object p1
.end method

.method public getItem(I)Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;
    .locals 1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 0

    const/4 p1, -0x2

    return p1
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->tags:Ljava/util/List;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    invoke-virtual {p0, p2}, Landroidx/fragment/app/ha;->getItemId(I)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->makeFragmentName(IJ)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/ha;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/fragment/app/Fragment;

    iget-object p2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {p2}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroidx/fragment/app/qa;->e(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {p2}, Landroidx/fragment/app/qa;->b()I

    return-object p1
.end method

.method public setFragments(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->tags:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->tags:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    iget-object v3, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->tags:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v2}, Landroidx/fragment/app/qa;->d(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroidx/fragment/app/qa;->b()I

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->p()Z

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->tags:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_2
    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroidx/viewpager/widget/f;->notifyDataSetChanged()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-void
.end method
