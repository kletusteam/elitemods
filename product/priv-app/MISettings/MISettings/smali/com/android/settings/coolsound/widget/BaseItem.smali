.class public Lcom/android/settings/coolsound/widget/BaseItem;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/widget/BaseItem$OnClickListener;,
        Lcom/android/settings/coolsound/widget/BaseItem$OnChangeListener;,
        Lcom/android/settings/coolsound/widget/BaseItem$OnChangeInternalListener;
    }
.end annotation


# instance fields
.field private allRingtoneClickListener:Landroid/view/View$OnClickListener;

.field private isNormal:Z

.field private mAllRingtoneView:Landroid/widget/TextView;

.field private mExtras:Landroid/os/Bundle;

.field private mFragment:Ljava/lang/String;

.field private mIntent:Landroid/content/Intent;

.field private mLayoutResId:I

.field private mListener:Lcom/android/settings/coolsound/widget/BaseItem$OnChangeInternalListener;

.field private mOnChangeListener:Lcom/android/settings/coolsound/widget/BaseItem$OnChangeListener;

.field private mOnClickListener:Lcom/android/settings/coolsound/widget/BaseItem$OnClickListener;

.field private mRightValue:Ljava/lang/CharSequence;

.field private mRightValueView:Landroid/widget/TextView;

.field private mSummary:Ljava/lang/CharSequence;

.field private mSummaryView:Landroid/widget/TextView;

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/coolsound/widget/BaseItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/coolsound/widget/BaseItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/coolsound/widget/BaseItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->isNormal:Z

    const/4 p4, -0x1

    iput p4, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mLayoutResId:I

    new-instance p4, Lcom/android/settings/coolsound/widget/BaseItem$1;

    invoke-direct {p4, p0}, Lcom/android/settings/coolsound/widget/BaseItem$1;-><init>(Lcom/android/settings/coolsound/widget/BaseItem;)V

    iput-object p4, p0, Lcom/android/settings/coolsound/widget/BaseItem;->allRingtoneClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p4

    sget-object v0, Lcom/xiaomi/misettings/g;->MiuiDefaultRingtoneItem:[I

    invoke-virtual {p4, p2, v0, p3, p1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    iget p3, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mLayoutResId:I

    invoke-virtual {p2, p1, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p1

    iput p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mLayoutResId:I

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/BaseItem;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/coolsound/widget/BaseItem;)Landroid/content/Intent;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mIntent:Landroid/content/Intent;

    return-object p0
.end method

.method private init()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mLayoutResId:I

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x1020010

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mSummaryView:Landroid/widget/TextView;

    const v0, 0x1020016

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mTitleView:Landroid/widget/TextView;

    const v0, 0x7f0b03ac

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mRightValueView:Landroid/widget/TextView;

    invoke-virtual {p0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private performClickSelf()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/BaseItem;->onClick()V

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/BaseItem;->callClickListener()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->isNormal:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected callChangeListener(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mOnChangeListener:Lcom/android/settings/coolsound/widget/BaseItem$OnChangeListener;

    if-eqz v0, :cond_1

    invoke-interface {v0, p0, p1}, Lcom/android/settings/coolsound/widget/BaseItem$OnChangeListener;->onChange(Lcom/android/settings/coolsound/widget/BaseItem;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method protected callClickListener()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mOnClickListener:Lcom/android/settings/coolsound/widget/BaseItem$OnClickListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/settings/coolsound/widget/BaseItem$OnClickListener;->onClick()V

    :cond_0
    return-void
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mExtras:Landroid/os/Bundle;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mExtras:Landroid/os/Bundle;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method public getFragment()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mFragment:Ljava/lang/String;

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method protected getPersistedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mSummary:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public isNormal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->isNormal:Z

    return v0
.end method

.method protected notifyChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mListener:Lcom/android/settings/coolsound/widget/BaseItem$OnChangeInternalListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lcom/android/settings/coolsound/widget/BaseItem$OnChangeInternalListener;->onChange(Lcom/android/settings/coolsound/widget/BaseItem;)V

    :cond_0
    return-void
.end method

.method protected onAllRingtoneClick()V
    .locals 0

    return-void
.end method

.method protected onClick()V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/BaseItem;->performClickSelf()V

    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 0

    return-void
.end method

.method protected persistString(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public setFragment(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mFragment:Ljava/lang/String;

    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mIntent:Landroid/content/Intent;

    return-void
.end method

.method public setMoreView(Z)V
    .locals 1

    const v0, 0x1020014

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public setNormal(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->isNormal:Z

    const p1, 0x1020015

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mAllRingtoneView:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mAllRingtoneView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->allRingtoneClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method final setOnChangeInternalListener(Lcom/android/settings/coolsound/widget/BaseItem$OnChangeInternalListener;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mListener:Lcom/android/settings/coolsound/widget/BaseItem$OnChangeInternalListener;

    goto/32 :goto_0

    nop
.end method

.method public setOnChangeListener(Lcom/android/settings/coolsound/widget/BaseItem$OnChangeListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mOnChangeListener:Lcom/android/settings/coolsound/widget/BaseItem$OnChangeListener;

    return-void
.end method

.method public setOnClickListener(Lcom/android/settings/coolsound/widget/BaseItem$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mOnClickListener:Lcom/android/settings/coolsound/widget/BaseItem$OnClickListener;

    return-void
.end method

.method public setRightValue(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/coolsound/widget/BaseItem;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setRightValue(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mRightValue:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    :cond_0
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mRightValue:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mRightValue:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mRightValueView:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/BaseItem;->notifyChanged()V

    :cond_3
    return-void
.end method

.method public setShowImage(I)V
    .locals 2

    const v0, 0x7f0b0223

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    return-void
.end method

.method public setSummary(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/coolsound/widget/BaseItem;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSummary(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mSummary:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    :cond_0
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mSummary:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mSummary:Ljava/lang/CharSequence;

    const v0, 0x1020010

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mSummaryView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mSummaryView:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/BaseItem;->notifyChanged()V

    :cond_3
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/coolsound/widget/BaseItem;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mTitle:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    :cond_0
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mTitle:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BaseItem;->mTitleView:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/BaseItem;->notifyChanged()V

    :cond_3
    return-void
.end method
