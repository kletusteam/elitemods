.class Lcom/android/settings/coolsound/widget/RingtoneGridView$4;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/widget/RingtoneGridView;->initNormalView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

.field final synthetic val$child:Landroid/view/View;

.field final synthetic val$index:I

.field final synthetic val$showType:I


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;IILandroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iput p2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->val$showType:I

    iput p3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->val$index:I

    iput-object p4, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->val$child:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onClick showType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->val$showType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RingtoneGridView"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolCommonUtils;->isZenMode(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {p1}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v0}, Landroid/widget/GridLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f1302b8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v1}, Lcom/misettings/common/utils/r;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    goto/16 :goto_2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$500(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "com.android.settings.coolsound.AreaAnimalSoundPadActivity"

    goto :goto_0

    :cond_1
    const-string v1, "com.android.settings.coolsound.AreaAnimalSoundActivity"

    :goto_0
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.xiaomi.misettings"

    invoke-direct {v2, v3, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->val$index:I

    const-string v2, "ring_area"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/misettings/common/utils/n;->a(Landroid/content/Intent;I)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/misettings/common/utils/m;->d(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-static {v0, v1}, Lcom/misettings/common/utils/m;->a(Landroid/content/Intent;I)V

    :cond_2
    iget-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {p1}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_3
    iget v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->val$showType:I

    const/4 v2, 0x0

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->val$index:I

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v3}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$600(Lcom/android/settings/coolsound/widget/RingtoneGridView;)I

    move-result v3

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iget v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->val$index:I

    invoke-virtual {v0, v3, v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->onSoundSelected(IZ)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$700(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/MediaPlayerListener;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$700(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/MediaPlayerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settings/coolsound/MediaPlayerListener;->stopIntelligentSound()V

    :cond_5
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iget v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->val$index:I

    invoke-virtual {v0, v3, v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->onSoundSelected(IZ)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$800(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateMoreRingtoneSummary(Landroid/net/Uri;Z)V

    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$800(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    move-result-object v0

    if-eq p1, v0, :cond_7

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$000(Lcom/android/settings/coolsound/widget/RingtoneGridView;)I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_7

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {p1}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1, v2}, Lmiui/util/SimRingtoneUtils;->setDefaultSoundUniform(Landroid/content/Context;IZ)V

    :cond_7
    iget-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->val$child:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->updateSelectView(Landroid/view/View;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$900(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;->onMoreRingtoneSelected()V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    new-instance v0, Lmiui/util/MiSettingsOT;

    invoke-virtual {p1}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/util/MiSettingsOT;-><init>(Landroid/content/Context;)V

    invoke-static {p1, v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1002(Lcom/android/settings/coolsound/widget/RingtoneGridView;Lmiui/util/MiSettingsOT;)Lmiui/util/MiSettingsOT;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v1}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$400(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Ljava/util/List;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->val$index:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/data/CoolSoundResource;

    invoke-virtual {v1}, Lcom/android/settings/coolsound/data/CoolSoundResource;->getSoundPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "animal_res_path"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$4;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1000(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lmiui/util/MiSettingsOT;

    move-result-object v0

    const-string v1, "click_area_type"

    invoke-virtual {v0, v1, p1}, Lmiui/util/MiSettingsOT;->tk(Ljava/lang/String;Ljava/util/Map;)V

    :cond_8
    :goto_2
    return-void
.end method
