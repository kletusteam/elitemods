.class public Lcom/android/settings/ringtone/MultiSimRingtoneSettings;
.super Lmiuix/preference/PreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$b;
.implements Landroidx/preference/Preference$c;


# static fields
.field private static final KEY_RINGTONE_0:Ljava/lang/String; = "ringtone_0"

.field private static final KEY_RINGTONE_1:Ljava/lang/String; = "ringtone_1"

.field private static final KEY_RINGTONE_2:Ljava/lang/String; = "ringtone_2"

.field private static final KEY_RINGTONE_SLOT_SETTING:Ljava/lang/String; = "ringtone_slot_setting"

.field private static final PICK_RINGTONE_REQUEST_CODE_0:I = 0x0

.field private static final PICK_RINGTONE_REQUEST_CODE_1:I = 0x1

.field private static final PICK_RINGTONE_REQUEST_CODE_2:I = 0x2


# instance fields
.field private mRingtone0:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

.field private mRingtone1:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

.field private mRingtone2:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

.field private mRingtoneScreen:Landroidx/preference/PreferenceScreen;

.field private mRingtoneType:I

.field private mSimInfoChangeListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

.field private mSlot1ExtraType:I

.field private mSlot2ExtraType:I

.field private mSlotSetting:Landroidx/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/preference/PreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlotSetting:Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneScreen:Landroidx/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone0:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone1:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone2:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneType:I

    iput v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlot1ExtraType:I

    iput v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlot2ExtraType:I

    new-instance v0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings$1;-><init>(Lcom/android/settings/ringtone/MultiSimRingtoneSettings;)V

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSimInfoChangeListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/ringtone/MultiSimRingtoneSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->updateUI()V

    return-void
.end method

.method private setupUI()V
    .locals 3

    const-string v0, "ringtone_slot_setting"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlotSetting:Landroidx/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlotSetting:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$b;)V

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlotSetting:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneType:I

    invoke-static {v1, v2}, Lmiui/util/SimRingtoneUtils;->isDefaultSoundUniform(Landroid/content/Context;I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneScreen:Landroidx/preference/PreferenceScreen;

    const-string v0, "ringtone_0"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone0:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone0:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    const-string v0, "ringtone_1"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone1:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone1:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    const-string v0, "ringtone_2"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone2:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone2:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    return-void
.end method

.method private updateUI()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlotSetting:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneScreen:Landroidx/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone0:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->d(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneScreen:Landroidx/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone1:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->b(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneScreen:Landroidx/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone2:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->b(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone1:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlot1ExtraType:I

    invoke-virtual {v0, v1}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->updateUI(I)V

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone2:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlot2ExtraType:I

    invoke-virtual {v0, v1}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->updateUI(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneScreen:Landroidx/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone0:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->b(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneScreen:Landroidx/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone1:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->d(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneScreen:Landroidx/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone2:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->d(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone0:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneType:I

    invoke-virtual {v0, v1}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->updateUI(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    const-string v1, "android.intent.extra.ringtone.TYPE"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    :goto_0
    iput p1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneType:I

    iget p1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneType:I

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/16 v2, 0x8

    if-eq p1, v2, :cond_1

    const/16 v2, 0x10

    if-eq p1, v2, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    iget p1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneType:I

    invoke-static {p1, v0}, Lmiui/util/SimRingtoneUtils;->getExtraRingtoneTypeBySlot(II)I

    move-result p1

    iput p1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlot1ExtraType:I

    iget p1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneType:I

    invoke-static {p1, v1}, Lmiui/util/SimRingtoneUtils;->getExtraRingtoneTypeBySlot(II)I

    move-result p1

    iput p1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlot2ExtraType:I

    const p1, 0x7f160007

    invoke-virtual {p0, p1, p2}, Landroidx/preference/PreferenceFragmentCompat;->setPreferencesFromResource(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->setupUI()V

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->updateUI()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSimInfoChangeListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v0, v1}, Lmiui/telephony/SubscriptionManager;->removeOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    iget-object p2, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlotSetting:Landroidx/preference/CheckBoxPreference;

    const/4 v0, 0x1

    if-ne p1, p2, :cond_2

    invoke-virtual {p2}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result p1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_0
    invoke-virtual {p2, p1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iget p2, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtoneType:I

    iget-object v2, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSlotSetting:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    move v1, v0

    :cond_1
    invoke-static {p1, p2, v1}, Lmiui/util/SimRingtoneUtils;->setDefaultSoundUniform(Landroid/content/Context;IZ)V

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->updateUI()V

    :cond_2
    return v0
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone0:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getRingtonePickerIntent()Landroid/content/Intent;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone1:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    if-ne p1, v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getRingtonePickerIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1, v1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mRingtone2:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    if-ne p1, v0, :cond_2

    invoke-virtual {v0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getRingtonePickerIntent()Landroid/content/Intent;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_2
    :goto_0
    return v1
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->mSimInfoChangeListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v0, v1}, Lmiui/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->updateUI()V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Landroidx/preference/PreferenceFragmentCompat;->onStart()V

    return-void
.end method
