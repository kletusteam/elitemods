.class Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->initFragments()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v1, v2, :cond_0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->u()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v4, v1

    :goto_1
    if-ge v4, v3, :cond_2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroidx/fragment/app/Fragment;

    instance-of v6, v5, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    if-eqz v6, :cond_1

    check-cast v5, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    iget-object v6, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v6, v4}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$100(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v7, v6}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$200(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    invoke-virtual {v5, v7, v6}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->setData(Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->setDataChanged(Z)V

    iget-object v6, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-virtual {v5, v6}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->registerListener(Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;)V

    iget-object v6, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v6}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$300(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$400()[Ljava/lang/String;

    move-result-object v0

    array-length v3, v0

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v0, v1

    iget-object v5, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v5, v4}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$500(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_3

    :cond_3
    new-instance v5, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;

    invoke-direct {v5}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;-><init>()V

    iget-object v6, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-virtual {v5, v6}, Lcom/android/settings/coolsound/CoolSoundMixMatchAnimalFragment;->registerListener(Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;)V

    iget-object v6, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v6}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$600(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    move-result-object v6

    iget-object v7, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-virtual {v6, v7, v4}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->getAnimalList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Lcom/android/settings/coolsound/CoolSoundMixMathBaseFragment;->setData(Ljava/util/List;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v4}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$300(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$700(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
