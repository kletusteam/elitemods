.class final Lcom/android/settings/coolsound/data/CoolSoundUtils$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/data/CoolSoundUtils;->fillInVideoView(Landroid/net/Uri;Landroid/widget/VideoView;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$loop:Z

.field final synthetic val$vv:Landroid/widget/VideoView;


# direct methods
.method constructor <init>(Landroid/widget/VideoView;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/data/CoolSoundUtils$1;->val$vv:Landroid/widget/VideoView;

    iput-boolean p2, p0, Lcom/android/settings/coolsound/data/CoolSoundUtils$1;->val$loop:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2

    const-string v0, "CoolSoundUtils"

    const-string v1, "onPrepared..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settings/coolsound/data/CoolSoundUtils$1$1;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/data/CoolSoundUtils$1$1;-><init>(Lcom/android/settings/coolsound/data/CoolSoundUtils$1;)V

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    iget-boolean v0, p0, Lcom/android/settings/coolsound/data/CoolSoundUtils$1;->val$loop:Z

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setLooping(Z)V

    return-void
.end method
