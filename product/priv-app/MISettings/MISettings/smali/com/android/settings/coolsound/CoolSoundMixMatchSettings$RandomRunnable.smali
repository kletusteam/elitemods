.class final Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RandomRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;-><init>(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1100(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1200(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v2}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1200(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v2}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1200(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    iget-object v2, v2, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$600(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->unSelectAll()V

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$600(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Lcom/android/settings/coolsound/data/MixMatchAnimalController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/coolsound/data/MixMatchAnimalController;->getRandomAnimal([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1300(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)Lcom/android/settings/coolsound/MixMatchAnimalAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalAnimator;->addRandomAnimal(Ljava/util/List;)V

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v1}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1400(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;)V

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings$RandomRunnable;->this$0:Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;

    invoke-static {v1, v0}, Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;->access$1500(Lcom/android/settings/coolsound/CoolSoundMixMatchSettings;Ljava/util/List;)V

    return-void
.end method
