.class public Lcom/android/settings/coolsound/widget/PlayButton;
.super Landroid/view/View;


# instance fields
.field private mIsPlaying:Z

.field private mPaint:Landroid/graphics/Paint;

.field private mPlayCircleColor:I

.field private mPlayCircleColorDisabled:I

.field private mPlaySuspendStrokeWidth:I

.field private mPlaySuspendWidth:I

.field private mPlayTriangleColor:I

.field private mPlayTriangleColorDisabled:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColor:I

    const v0, -0x777778

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColor:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColorDisabled:I

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColorDisabled:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mIsPlaying:Z

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/PlayButton;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColor:I

    const p2, -0x777778

    iput p2, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColor:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColorDisabled:I

    iput p2, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColorDisabled:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mIsPlaying:Z

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/PlayButton;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColor:I

    const p2, -0x777778

    iput p2, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColor:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColorDisabled:I

    iput p2, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColorDisabled:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mIsPlaying:Z

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/PlayButton;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColor:I

    const p2, -0x777778

    iput p2, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColor:I

    iput p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColorDisabled:I

    iput p2, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColorDisabled:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mIsPlaying:Z

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/PlayButton;->init()V

    return-void
.end method

.method private drawCircle(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColor:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColorDisabled:I

    :goto_0
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    int-to-float v0, v0

    invoke-virtual {p1, v1, v2, v0, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawSuspend(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6

    invoke-direct {p0, p2}, Lcom/android/settings/coolsound/widget/PlayButton;->initPaint(Landroid/graphics/Paint;)V

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    new-instance v2, Landroid/graphics/Rect;

    mul-int/lit8 v3, v0, 0x11

    div-int/lit8 v3, v3, 0x28

    iget v4, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlaySuspendWidth:I

    sub-int v4, v3, v4

    mul-int/lit8 v5, v1, 0xe

    div-int/lit8 v5, v5, 0x28

    mul-int/lit8 v1, v1, 0x1a

    div-int/lit8 v1, v1, 0x28

    invoke-direct {v2, v4, v5, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/Rect;

    mul-int/lit8 v0, v0, 0x17

    div-int/lit8 v0, v0, 0x28

    iget v4, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlaySuspendWidth:I

    add-int/2addr v4, v0

    invoke-direct {v3, v0, v5, v4, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p1, v2, p2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {p1, v3, p2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-void
.end method

.method private drawTriangle(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6

    invoke-direct {p0, p2}, Lcom/android/settings/coolsound/widget/PlayButton;->initPaint(Landroid/graphics/Paint;)V

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    new-instance v2, Landroid/graphics/Point;

    mul-int/lit8 v3, v0, 0xf

    div-int/lit8 v3, v3, 0x28

    mul-int/lit8 v4, v1, 0xe

    div-int/lit8 v4, v4, 0x28

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    new-instance v4, Landroid/graphics/Point;

    mul-int/lit8 v5, v1, 0x1a

    div-int/lit8 v5, v5, 0x28

    invoke-direct {v4, v3, v5}, Landroid/graphics/Point;-><init>(II)V

    new-instance v3, Landroid/graphics/Point;

    mul-int/lit8 v0, v0, 0x1b

    div-int/lit8 v0, v0, 0x28

    mul-int/lit8 v1, v1, 0x14

    div-int/lit8 v1, v1, 0x28

    invoke-direct {v3, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iget v1, v2, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    iget v1, v4, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, v4, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    iget v1, v3, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, v3, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method private init()V
    .locals 3

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f06030c

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColor:I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f06030a

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColor:I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f06030d

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColorDisabled:I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f06030b

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayCircleColorDisabled:I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070419

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlaySuspendWidth:I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070418

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlaySuspendStrokeWidth:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPaint:Landroid/graphics/Paint;

    return-void
.end method

.method private initPaint(Landroid/graphics/Paint;)V
    .locals 1

    invoke-virtual {p1}, Landroid/graphics/Paint;->reset()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlaySuspendStrokeWidth:I

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColor:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPlayTriangleColorDisabled:I

    :goto_0
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/coolsound/widget/PlayButton;->drawCircle(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    iget-boolean v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mIsPlaying:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/coolsound/widget/PlayButton;->drawTriangle(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/coolsound/widget/PlayButton;->drawSuspend(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    :goto_0
    return-void
.end method

.method public setPlaying(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/widget/PlayButton;->mIsPlaying:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method
