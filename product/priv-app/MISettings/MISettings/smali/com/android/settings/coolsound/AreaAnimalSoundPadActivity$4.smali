.class Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;
.super Landroidx/recyclerview/widget/RecyclerView$k;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$k;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollStateChanged(Landroidx/recyclerview/widget/RecyclerView;I)V
    .locals 6
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$k;->onScrollStateChanged(Landroidx/recyclerview/widget/RecyclerView;I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    iget v0, v0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->position:I

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$ViewHolder;

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p2, :cond_2

    if-eq p2, v2, :cond_1

    if-eq p2, v0, :cond_0

    goto/16 :goto_3

    :cond_0
    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    iput-boolean v2, p1, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->change:Z

    goto/16 :goto_3

    :cond_1
    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {p1}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$200(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Lcom/android/settings/coolsound/MediaSoundPlayer;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/settings/coolsound/MediaSoundPlayer;->pausePlaySound()V

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {p1, v1}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$400(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;Z)V

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    iput-boolean v1, p1, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->change:Z

    goto/16 :goto_3

    :cond_2
    iget-object p2, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    iget-boolean p2, p2, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->change:Z

    if-eqz p2, :cond_8

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object p2

    instance-of v3, p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz v3, :cond_7

    check-cast p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->G()I

    move-result v3

    invoke-virtual {p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->H()I

    move-result p2

    sub-int v4, p2, v3

    const/4 v5, -0x1

    if-ne v4, v2, :cond_4

    if-nez v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v5

    :goto_0
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$a;

    move-result-object v3

    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemCount()I

    move-result v3

    sub-int/2addr v3, v2

    if-ne p2, v3, :cond_5

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$a;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemCount()I

    move-result p1

    add-int/lit8 v0, p1, -0x1

    goto :goto_1

    :cond_4
    add-int/2addr v3, p2

    div-int/lit8 v0, v3, 0x2

    :cond_5
    :goto_1
    if-eq v0, v5, :cond_7

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    iget p2, p1, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->position:I

    if-eq p2, v0, :cond_7

    iput v0, p1, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->position:I

    invoke-static {p1}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$100(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/coolsound/CoolCommonUtils;->isZenMode(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {p1}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$100(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {p2}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$100(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f1302b8

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2, v2}, Lcom/misettings/common/utils/r;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    goto :goto_2

    :cond_6
    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {p1}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$500(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    new-instance p2, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4$1;

    invoke-direct {p2, p0}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4$1;-><init>(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;)V

    invoke-interface {p1, p2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    :cond_7
    :goto_2
    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    iput-boolean v1, p1, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->change:Z

    :cond_8
    :goto_3
    return-void
.end method
