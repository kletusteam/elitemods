.class public abstract Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;
.super Landroidx/recyclerview/widget/RecyclerView$f;


# instance fields
.field private mAvgUsedWidth:I

.field private mEndInset:[I

.field private mMarginMiddle:I

.field private mMarginSide:I

.field protected mSpanCount:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$f;-><init>()V

    iput p1, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mMarginSide:I

    iput p2, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mMarginMiddle:I

    iput p3, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mSpanCount:I

    new-array p1, p3, [I

    iput-object p1, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mEndInset:[I

    iget p1, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mMarginMiddle:I

    iget p2, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mSpanCount:I

    add-int/lit8 p3, p2, -0x1

    mul-int/2addr p1, p3

    iget p3, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mMarginSide:I

    mul-int/lit8 p3, p3, 0x2

    add-int/2addr p1, p3

    div-int/2addr p1, p2

    iput p1, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mAvgUsedWidth:I

    return-void
.end method

.method public static getDesirdWidth(IIII)I
    .locals 1

    add-int/lit8 v0, p3, -0x1

    mul-int/2addr p2, v0

    mul-int/lit8 p1, p1, 0x2

    add-int/2addr p2, p1

    div-int/2addr p2, p3

    sub-int/2addr p0, p2

    div-int/2addr p0, p3

    return p0
.end method


# virtual methods
.method public final getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$q;)V
    .locals 7
    .param p1    # Landroid/graphics/Rect;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Landroidx/recyclerview/widget/RecyclerView$q;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v5

    const/4 v0, -0x1

    if-ne v5, v0, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p2

    instance-of p3, p2, Landroid/graphics/Rect;

    if-eqz p3, :cond_0

    check-cast p2, Landroid/graphics/Rect;

    invoke-virtual {p1, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0, p2, p3, v5}, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->isTargetView(Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;I)Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, p2, p3, v5}, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->getSpanIndex(Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;I)I

    move-result v6

    if-nez v6, :cond_5

    if-eqz v1, :cond_4

    iget v0, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mMarginSide:I

    iput v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mAvgUsedWidth:I

    sub-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mEndInset:[I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    aput v1, v0, v6

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mMarginSide:I

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iget v0, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mAvgUsedWidth:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mEndInset:[I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    aput v1, v0, v6

    goto :goto_1

    :cond_5
    if-eqz v1, :cond_6

    iget v0, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mMarginMiddle:I

    iget-object v1, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mEndInset:[I

    add-int/lit8 v2, v6, -0x1

    aget v2, v1, v2

    sub-int/2addr v0, v2

    iput v0, p1, Landroid/graphics/Rect;->right:I

    iget v0, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mAvgUsedWidth:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v2

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iget v0, p1, Landroid/graphics/Rect;->left:I

    aput v0, v1, v6

    goto :goto_1

    :cond_6
    iget v0, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mMarginMiddle:I

    iget-object v1, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mEndInset:[I

    add-int/lit8 v2, v6, -0x1

    aget v2, v1, v2

    sub-int/2addr v0, v2

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iget v0, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mAvgUsedWidth:I

    iget v2, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    iput v0, p1, Landroid/graphics/Rect;->right:I

    iget v0, p1, Landroid/graphics/Rect;->right:I

    aput v0, v1, v6

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->onGetItemOffsetsFinished(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$q;II)V

    new-instance p3, Landroid/graphics/Rect;

    invoke-direct {p3, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method public abstract getSpanIndex(Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;I)I
.end method

.method public abstract isTargetView(Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;I)Z
.end method

.method public onGetItemOffsetsFinished(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$q;II)V
    .locals 0

    return-void
.end method
