.class public Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;
.super Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem$AppInfo;
    }
.end annotation


# static fields
.field private static final CALENDAR_NAME:Ljava/lang/String; = "calendar_sound_item"

.field private static final NOTE_NAME:Ljava/lang/String; = "note_sound_item"

.field private static final SMS_NAME:Ljava/lang/String; = "sms_sound_item"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;)Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem$AppInfo;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;->getMmsInfo()Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem$AppInfo;

    move-result-object p0

    return-object p0
.end method

.method private getMmsInfo()Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem$AppInfo;
    .locals 5

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    if-eqz v2, :cond_0

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v1, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v1, v0}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v2, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem$AppInfo;

    invoke-direct {v2, p0, v1, v0}, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem$AppInfo;-><init>(Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V

    return-object v2

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private getPackageName()Ljava/lang/String;
    .locals 7

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, ""

    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v4, -0x5a574d17

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-eq v3, v4, :cond_3

    const v4, -0x2ba8a950

    if-eq v3, v4, :cond_2

    const v4, -0x2583bc1c

    if-eq v3, v4, :cond_1

    goto :goto_0

    :cond_1
    const-string v3, "calendar_sound_item"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const-string v3, "note_sound_item"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v1, v5

    goto :goto_0

    :cond_3
    const-string v3, "sms_sound_item"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v1, v6

    :cond_4
    :goto_0
    if-eqz v1, :cond_7

    if-eq v1, v6, :cond_6

    if-eq v1, v5, :cond_5

    goto :goto_1

    :cond_5
    const-string v0, "com.miui.notes"

    goto :goto_1

    :cond_6
    const-string v0, "com.android.mms"

    goto :goto_1

    :cond_7
    const-string v0, "com.android.calendar"

    :goto_1
    return-object v0
.end method

.method private init()V
    .locals 1

    new-instance v0, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem$1;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem$1;-><init>(Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;)V

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public getTitle()Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f130022

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setRightValue(Ljava/lang/CharSequence;)V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/RingtoneItem;->getRingtoneType()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->isMultiSimAndNoUniform(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/coolsound/widget/BaseItem;->setRightValue(Ljava/lang/CharSequence;)V

    return-void
.end method
