.class Lcom/android/settings/coolsound/widget/RingtoneGridView$10;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem$OnPlayCardSound;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/coolsound/widget/RingtoneGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/RingtoneGridView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPlayCardSound()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/coolsound/CoolCommonUtils;->isZenMode(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v2}, Landroid/widget/GridLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f1302b8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/misettings/common/utils/r;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$700(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/MediaPlayerListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$700(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/MediaPlayerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settings/coolsound/MediaPlayerListener;->stopIntelligentSound()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$600(Lcom/android/settings/coolsound/widget/RingtoneGridView;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1300(Lcom/android/settings/coolsound/widget/RingtoneGridView;I)Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    const/4 v2, -0x1

    invoke-static {v0, v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$602(Lcom/android/settings/coolsound/widget/RingtoneGridView;I)I

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$900(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView$OnMoreRingtoneSelectedListener;->onMoreRingtoneSelected()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v2

    invoke-static {v0, v2}, Lcom/android/settings/coolsound/CoolCommonUtils;->getMoreRingtone(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    return-void

    :cond_3
    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1400(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "notification_sound"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1500(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/data/CoolSoundUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v1}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v3}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v3

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->save(Landroid/content/Context;ILandroid/net/Uri;)V

    :goto_0
    return-void

    :cond_5
    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$800(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1600(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/view/View;I)V

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$800(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;

    move-result-object v2

    const v3, 0x7f0b02bd

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/widget/PlayView;

    if-eqz v2, :cond_6

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v3}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->isFileExist(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isFileExist"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "RingtoneGridView"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v3}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1500(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Lcom/android/settings/coolsound/data/CoolSoundUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v4}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v5}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v5

    invoke-virtual {v3, v4, v5, v0}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->save(Landroid/content/Context;ILandroid/net/Uri;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v3}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->getRingtoneType()I

    move-result v3

    invoke-static {v0, v3}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->adapterSaveUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_1
    :try_start_0
    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v3}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->reset()V

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v3}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v4}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-virtual {v5}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v0}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->adapterUri(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/CoolCommonUtils;->setAudioState(Landroid/media/MediaPlayer;Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    new-instance v3, Lcom/android/settings/coolsound/widget/RingtoneGridView$10$1;

    invoke-direct {v3, p0, v2}, Lcom/android/settings/coolsound/widget/RingtoneGridView$10$1;-><init>(Lcom/android/settings/coolsound/widget/RingtoneGridView$10;Lcom/android/settings/coolsound/widget/PlayView;)V

    invoke-virtual {v0, v3}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v3}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1700(Lcom/android/settings/coolsound/widget/RingtoneGridView;Landroid/media/MediaPlayer;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1900(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v3}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$1800(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/AudioManager$OnAudioFocusChangeListener;

    move-result-object v3

    invoke-virtual {v0, v3, v2, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/RingtoneGridView$10;->this$0:Lcom/android/settings/coolsound/widget/RingtoneGridView;

    invoke-static {v0}, Lcom/android/settings/coolsound/widget/RingtoneGridView;->access$200(Lcom/android/settings/coolsound/widget/RingtoneGridView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_8
    :goto_2
    return-void
.end method
