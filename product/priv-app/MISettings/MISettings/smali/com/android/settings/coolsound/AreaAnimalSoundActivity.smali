.class public Lcom/android/settings/coolsound/AreaAnimalSoundActivity;
.super Lcom/android/settings/coolsound/base/CoolSoundBaseActivity;

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/AreaAnimalSoundActivity$ViewHolder;,
        Lcom/android/settings/coolsound/AreaAnimalSoundActivity$MyAdapter;
    }
.end annotation


# static fields
.field private static final BUBBLE_HAS_SHOW:Ljava/lang/String; = "bubble_has_show"

.field private static final EXT_TYPE:Ljava/lang/String; = "ring_area"

.field private static final MSG_WHAT_RECYCLE_BITMAP:I = 0x1

.field private static final POS_KEY:Ljava/lang/String; = "select_position"

.field private static final RECYCLER_DELAY:I = 0x1d4c0

.field private static final TAG:Ljava/lang/String; = "AreaAnimalSound"

.field private static final TYPE_ARCTIC:I = 0x3


# instance fields
.field private applyBtn:Landroid/widget/Button;

.field cardClickListener:Landroid/view/View$OnClickListener;

.field change:Z

.field private completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/AnimalInfo;",
            ">;"
        }
    .end annotation
.end field

.field private descView:Landroid/widget/TextView;

.field private lastShowCardView:Landroid/view/View;

.field private listener:Landroid/view/View$OnClickListener;

.field private mBitMap:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mHandler:Landroid/os/Handler;

.field private mPlayer:Lcom/android/settings/coolsound/SoundPlayer;

.field private mScrollListener:Landroidx/recyclerview/widget/RecyclerView$k;

.field private mediaPlayer:Lcom/android/settings/coolsound/MediaSoundPlayer;

.field position:I

.field recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/coolsound/base/CoolSoundBaseActivity;-><init>()V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mExecutor:Ljava/util/concurrent/ExecutorService;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->position:I

    iput-boolean v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->change:Z

    new-instance v0, Lcom/android/settings/coolsound/MediaSoundPlayer;

    invoke-direct {v0}, Lcom/android/settings/coolsound/MediaSoundPlayer;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mediaPlayer:Lcom/android/settings/coolsound/MediaSoundPlayer;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$4;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$4;-><init>(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mScrollListener:Landroidx/recyclerview/widget/RecyclerView$k;

    new-instance v0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$5;-><init>(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->cardClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$6;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$6;-><init>(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$8;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$8;-><init>(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->listener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$100(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->getStringResource(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1500(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->getImageResource(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1600(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->updateApplyBtn(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)Lcom/android/settings/coolsound/MediaSoundPlayer;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mediaPlayer:Lcom/android/settings/coolsound/MediaSoundPlayer;

    return-object p0
.end method

.method static synthetic access$300(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->data:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$400(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->updatePlayViewUI(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)Ljava/util/concurrent/ExecutorService;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mExecutor:Ljava/util/concurrent/ExecutorService;

    return-object p0
.end method

.method static synthetic access$600(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)I
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->getAreaType()I

    move-result p0

    return p0
.end method

.method private doRecycleBitmap()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mBitMap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mBitMap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mBitMap:Landroid/graphics/Bitmap;

    const-string v0, "AreaAnimalSound"

    const-string v1, "bitmap recycled"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private getAreaType()I
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ring_area"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method private getBubbleHasShow(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "bubble_has_show"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/provider/SystemSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method private getImageResource(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    const-string v1, "drawable"

    invoke-virtual {v0, p2, v1, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private getStringResource(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    const-string v1, "string"

    invoke-virtual {v0, p2, v1, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, ""

    return-object p1
.end method

.method private isSelected(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p1}, Lcom/android/settings/coolsound/CoolCommonUtils;->getSelectedIndex(Landroid/content/Context;)I

    move-result p1

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->getAreaType()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private loadBitmap(Landroid/content/Context;I)Landroid/graphics/drawable/BitmapDrawable;
    .locals 2

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/16 v1, 0xa0

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mBitMap:Landroid/graphics/Bitmap;

    new-instance p1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mBitMap:Landroid/graphics/Bitmap;

    invoke-direct {p1, p2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object p1
.end method

.method private loadWindowBg()V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->getAreaType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/settings/coolsound/data/ResourceWrapper;->loadAreaBg(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {p0, p0, v1}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->loadBitmap(Landroid/content/Context;I)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private reloadBitMap()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mBitMap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "AreaAnimalSound"

    const-string v1, "reload bitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->getAreaType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/settings/coolsound/data/ResourceWrapper;->loadAreaBg(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {p0, p0, v1}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->loadBitmap(Landroid/content/Context;I)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-void
.end method

.method private setBubbleHasShow(Landroid/content/Context;Z)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "bubble_has_show"

    invoke-static {p1, v0, p2}, Landroid/provider/MiuiSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method private toRecycleBitMap()V
    .locals 4

    const-string v0, "AreaAnimalSound"

    const-string v1, "prepare to recycle bitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private updateApplyBtn(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->applyBtn:Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_0

    const v2, 0x7f1301b5

    goto :goto_0

    :cond_0
    const v2, 0x7f130033

    :goto_0
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->applyBtn:Landroid/widget/Button;

    xor-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->applyBtn:Landroid/widget/Button;

    if-eqz p1, :cond_1

    const p1, 0x7f060028

    goto :goto_1

    :cond_1
    const p1, 0x7f060029

    :goto_1
    invoke-virtual {p0, p1}, Landroid/app/Activity;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextColor(I)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->getAreaType()I

    move-result p1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->applyBtn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->isSelected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f06002a

    goto :goto_2

    :cond_2
    const v0, 0x7f06002b

    :goto_2
    invoke-virtual {p0, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    :cond_3
    return-void
.end method

.method private updatePlayViewUI(Z)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->position:I

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView$g;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->lastShowCardView:Landroid/view/View;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->lastShowCardView:Landroid/view/View;

    :goto_0
    if-nez v0, :cond_1

    return-void

    :cond_1
    const v1, 0x7f0b02bd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/app/AppCompatActivity;

    new-instance v2, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$7;

    invoke-direct {v2, p0, v0, p1}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$7;-><init>(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;Landroid/view/View;Z)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private updateUIView()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->titleView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06002e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->applyBtn:Landroid/widget/Button;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->isSelected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f06002a

    goto :goto_0

    :cond_1
    const v1, 0x7f06002b

    :goto_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->descView:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->applyBtn:Landroid/widget/Button;

    if-eqz v0, :cond_4

    const v1, 0x7f08008d

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    :cond_4
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 1
    .param p1    # Landroid/os/Message;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->doRecycleBitmap()V

    return v0

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->loadWindowBg()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/misettings/common/utils/m;->a(Landroid/content/Intent;I)V

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/coolsound/base/CoolSoundBaseActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string v0, "select_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->position:I

    :cond_1
    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mediaPlayer:Lcom/android/settings/coolsound/MediaSoundPlayer;

    if-nez p1, :cond_2

    new-instance p1, Lcom/android/settings/coolsound/MediaSoundPlayer;

    invoke-direct {p1}, Lcom/android/settings/coolsound/MediaSoundPlayer;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mediaPlayer:Lcom/android/settings/coolsound/MediaSoundPlayer;

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mContext:Landroid/content/Context;

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->getAreaType()I

    move-result v0

    invoke-static {p1, v0}, Lcom/android/settings/coolsound/data/ResourceWrapper;->loadAreaAnimalResource(Landroid/content/Context;I)Lcom/android/settings/coolsound/data/AreaResource;

    move-result-object p1

    const v0, 0x7f0e0030

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/appcompat/app/ActionBar;->i()V

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->loadWindowBg()V

    const v0, 0x7f0b007b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$1;

    invoke-direct {v1, p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$1;-><init>(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b006b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->applyBtn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->applyBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->isSelected(Landroid/content/Context;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->updateApplyBtn(Z)V

    const v0, 0x7f0b0063

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/AreaResource;->getAnimals()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/AreaResource;->getAnimals()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->data:Ljava/util/List;

    :cond_4
    const v0, 0x7f0b006d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->titleView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->titleView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/AreaResource;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0b006c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->descView:Landroid/widget/TextView;

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->descView:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070074

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    :cond_5
    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->descView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/AreaResource;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iget-object v2, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v2, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$MyAdapter;

    iget-object v3, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->data:Ljava/util/List;

    const/4 v4, 0x0

    invoke-direct {v2, p0, v3, v4}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$MyAdapter;-><init>(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;Ljava/util/List;Lcom/android/settings/coolsound/AreaAnimalSoundActivity$1;)V

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/m;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Landroidx/recyclerview/widget/F;

    invoke-direct {v0}, Landroidx/recyclerview/widget/F;-><init>()V

    iget-object v2, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/U;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    :cond_6
    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v2, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mScrollListener:Landroidx/recyclerview/widget/RecyclerView$k;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$k;)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->getAreaType()I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_7

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->updateUIView()V

    :cond_7
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    new-instance v0, Lcom/android/settings/coolsound/SoundPlayer;

    iget-object v1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/android/settings/coolsound/data/AreaResource;->getSoundPath()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/android/settings/coolsound/SoundPlayer;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mPlayer:Lcom/android/settings/coolsound/SoundPlayer;

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$2;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$2;-><init>(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    invoke-static {p0}, Lcom/android/settings/coolsound/CoolCommonUtils;->setInFullWindowGestureMode(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mediaPlayer:Lcom/android/settings/coolsound/MediaSoundPlayer;

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {p1, v0}, Lcom/android/settings/coolsound/MediaSoundPlayer;->setCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result p1

    if-eqz p1, :cond_8

    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/xiaomi/misettings/usagestats/ExitMultiWindowActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_8
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mediaPlayer:Lcom/android/settings/coolsound/MediaSoundPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/MediaSoundPlayer;->releasePlaySound()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mPlayer:Lcom/android/settings/coolsound/SoundPlayer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/coolsound/SoundPlayer;->release()V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mPlayer:Lcom/android/settings/coolsound/SoundPlayer;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/SoundPlayer;->unregisterRecevier()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->doRecycleBitmap()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mPlayer:Lcom/android/settings/coolsound/SoundPlayer;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/SoundPlayer;->stop()V

    return-void
.end method

.method protected onResume()V
    .locals 5

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onResume()V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mPlayer:Lcom/android/settings/coolsound/SoundPlayer;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/SoundPlayer;->prepare()V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mPlayer:Lcom/android/settings/coolsound/SoundPlayer;

    invoke-virtual {v0}, Lcom/android/settings/coolsound/SoundPlayer;->start()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->reloadBitMap()V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->getBubbleHasShow(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0b0091

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/widget/BubbleLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$3;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity$3;-><init>(Lcom/android/settings/coolsound/AreaAnimalSoundActivity;Lcom/android/settings/coolsound/widget/BubbleLayout;)V

    const-wide/16 v3, 0xbb8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->setBubbleHasShow(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->position:I

    const-string v1, "select_position"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->toRecycleBitMap()V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundActivity;->mediaPlayer:Lcom/android/settings/coolsound/MediaSoundPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/MediaSoundPlayer;->stopPlaySound()V

    :cond_0
    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStop()V

    return-void

    :cond_1
    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStop()V

    return-void
.end method
