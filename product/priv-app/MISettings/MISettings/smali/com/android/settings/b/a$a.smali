.class public abstract Lcom/android/settings/b/a$a;
.super Landroid/os/Binder;

# interfaces
.implements Lcom/android/settings/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/b/a$a$a;
    }
.end annotation


# direct methods
.method public static a()Lcom/android/settings/b/a;
    .locals 1

    sget-object v0, Lcom/android/settings/b/a$a$a;->a:Lcom/android/settings/b/a;

    return-object v0
.end method

.method public static a(Landroid/os/IBinder;)Lcom/android/settings/b/a;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v0, "com.android.settings.services.IMemoryOptimizationInterface"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/android/settings/b/a;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/android/settings/b/a;

    return-object v0

    :cond_1
    new-instance v0, Lcom/android/settings/b/a$a$a;

    invoke-direct {v0, p0}, Lcom/android/settings/b/a$a$a;-><init>(Landroid/os/IBinder;)V

    return-object v0
.end method
