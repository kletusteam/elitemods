.class public Lcom/android/settings/coolsound/widget/IntelligentLayout;
.super Landroid/widget/LinearLayout;


# static fields
.field private static final DEFAULT_RINGTONE_FILE:Ljava/lang/String; = "file:///system/media/audio/ringtones/MiRemix.ogg"

.field private static final INTELLIGENT_SVR_KEY:Ljava/lang/String; = "intelligent_recognition_service"

.field private static final INTELLIGENT_SVR_RINGTONE:Ljava/lang/String; = "/system/media/audio/ui/MIUI_Ringtone_Special_Service.ogg"

.field private static final INTELLIGENT_SVR_SLOT2_KEY:Ljava/lang/String; = "intelligent_recognition_service_slot2"


# instance fields
.field private audioFocusRunnable:Lcom/android/settings/coolsound/widget/AudioFocusRunnable;

.field private cardView:Landroid/view/View;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mIntellItem:Landroid/view/View;

.field private mIntelligentBtn:Lmiuix/slidingwidget/widget/SlidingButton;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMediaPlayerListener:Lcom/android/settings/coolsound/MediaPlayerListener;

.field private mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mPlayBtn:Lcom/android/settings/coolsound/widget/PlayButton;

.field private mRingtoneType:I

.field private mSvrKey:Ljava/lang/String;

.field private mVideoView:Landroid/widget/VideoView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/coolsound/widget/IntelligentLayout;)Landroid/widget/VideoView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    return-object p0
.end method

.method static synthetic access$100(Lcom/android/settings/coolsound/widget/IntelligentLayout;)Lcom/android/settings/coolsound/MediaPlayerListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayerListener:Lcom/android/settings/coolsound/MediaPlayerListener;

    return-object p0
.end method

.method static synthetic access$200(Lcom/android/settings/coolsound/widget/IntelligentLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->playSound()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/settings/coolsound/widget/IntelligentLayout;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mSvrKey:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/android/settings/coolsound/widget/IntelligentLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->updateIntelligentBtn()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/settings/coolsound/widget/IntelligentLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->updatePlayButton(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/settings/coolsound/widget/IntelligentLayout;)Lcom/android/settings/coolsound/widget/AudioFocusRunnable;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->audioFocusRunnable:Lcom/android/settings/coolsound/widget/AudioFocusRunnable;

    return-object p0
.end method

.method private adapterDefaultRingtone(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    if-nez p2, :cond_1

    return p1

    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v1, "media"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "MiRemix"

    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    :cond_2
    return p1
.end method

.method private getDefaultRingtoneUri()Landroid/net/Uri;
    .locals 1

    const-string v0, "file:///system/media/audio/ringtones/MiRemix.ogg"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private playSound()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->stopPlayingSound()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    const-string v1, "/system/media/audio/ui/MIUI_Ringtone_Special_Service.ogg"

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/android/settings/coolsound/widget/IntelligentLayout$4;

    invoke-direct {v1, p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout$4;-><init>(Lcom/android/settings/coolsound/widget/IntelligentLayout;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/CoolCommonUtils;->setAudioState(Landroid/media/MediaPlayer;Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    const/4 v4, 0x2

    invoke-virtual {v0, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    :cond_1
    invoke-direct {p0, v1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->updatePlayButton(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private updateIntelligentBtn()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntelligentBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mSvrKey:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/SystemSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method private updatePlayButton(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mPlayBtn:Lcom/android/settings/coolsound/widget/PlayButton;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/widget/PlayButton;->setPlaying(Z)V

    :cond_0
    return-void
.end method

.method private updateVideoBackground()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    const v1, 0x7f080222

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setBackgroundResource(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public init(Lcom/android/settings/coolsound/MediaPlayerListener;I)V
    .locals 3

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayerListener:Lcom/android/settings/coolsound/MediaPlayerListener;

    iput p2, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mRingtoneType:I

    iget p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mRingtoneType:I

    const/16 p2, 0x80

    if-ne p1, p2, :cond_0

    const-string p1, "intelligent_recognition_service_slot2"

    goto :goto_0

    :cond_0
    const-string p1, "intelligent_recognition_service"

    :goto_0
    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mSvrKey:Ljava/lang/String;

    const p1, 0x7f0b0234

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/VideoView;

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    const p1, 0x7f0b022f

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/android/settings/coolsound/widget/PlayButton;

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mPlayBtn:Lcom/android/settings/coolsound/widget/PlayButton;

    const p1, 0x7f0b0230

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->cardView:Landroid/view/View;

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    const/4 p2, 0x1

    if-eqz p1, :cond_1

    const-string p1, "android.resource://com.xiaomi.misettings/2131886086"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-static {p1, v0, p2}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->fillInVideoView(Landroid/net/Uri;Landroid/widget/VideoView;Z)V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->cardView:Landroid/view/View;

    new-instance v0, Lcom/android/settings/coolsound/widget/IntelligentLayout$1;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout$1;-><init>(Lcom/android/settings/coolsound/widget/IntelligentLayout;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const p1, 0x7f0b022e

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntellItem:Landroid/view/View;

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntellItem:Landroid/view/View;

    if-eqz p1, :cond_2

    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1301cf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntellItem:Landroid/view/View;

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-static {}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->getInstance()Lcom/android/settings/coolsound/data/CoolSoundUtils;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->getDefaultRingtoneUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/coolsound/data/CoolSoundUtils;->queryTitle(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1301ce

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, p2, v2

    invoke-static {v1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntellItem:Landroid/view/View;

    const p2, 0x7f0b022d

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntelligentBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntelligentBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance p2, Lcom/android/settings/coolsound/widget/IntelligentLayout$2;

    invoke-direct {p2, p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout$2;-><init>(Lcom/android/settings/coolsound/widget/IntelligentLayout;)V

    invoke-virtual {p1, p2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntellItem:Landroid/view/View;

    new-instance p2, Lcom/android/settings/coolsound/widget/IntelligentLayout$3;

    invoke-direct {p2, p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout$3;-><init>(Lcom/android/settings/coolsound/widget/IntelligentLayout;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "audio"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/AudioManager;

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mAudioManager:Landroid/media/AudioManager;

    new-instance p1, Lcom/android/settings/coolsound/widget/AudioFocusHelper;

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p1, p2}, Lcom/android/settings/coolsound/widget/AudioFocusHelper;-><init>(Landroid/media/MediaPlayer;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    new-instance p1, Lcom/android/settings/coolsound/widget/AudioFocusRunnable;

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mAudioManager:Landroid/media/AudioManager;

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-direct {p1, p2, v0}, Lcom/android/settings/coolsound/widget/AudioFocusRunnable;-><init>(Landroid/media/AudioManager;Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->audioFocusRunnable:Lcom/android/settings/coolsound/widget/AudioFocusRunnable;

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->updateVideoBackground()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/VideoView;->suspend()V

    iput-object v1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->updateVideoBackground()V

    return-void
.end method

.method public onPause()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->pauseVideo(Z)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->updateVideoBackground()V

    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntelligentBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->updateIntelligentBtn()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->getDefaultRingtoneUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mRingtoneType:I

    invoke-static {v1, v2}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundSettingUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->adapterDefaultRingtone(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setFocusable(Z)V

    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->pauseVideo(Z)V

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->stopPlayingSound()V

    return-void
.end method

.method public pauseVideo(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/VideoView;->seekTo(I)V

    :cond_0
    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {p1}, Landroid/widget/VideoView;->pause()V

    :cond_1
    return-void
.end method

.method public setEnabled(Z)V
    .locals 4

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->cardView:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    const/4 v1, 0x0

    const v2, 0x7f080133

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Landroid/widget/VideoView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mVideoView:Landroid/widget/VideoView;

    if-eqz p1, :cond_1

    move-object v3, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/VideoView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mPlayBtn:Lcom/android/settings/coolsound/widget/PlayButton;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntelligentBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    if-eqz v0, :cond_5

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntelligentBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    if-eqz p1, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setForeground(Landroid/graphics/drawable/Drawable;)V

    :cond_5
    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mIntellItem:Landroid/view/View;

    if-eqz v0, :cond_6

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_6
    const v0, 0x1020016

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    const v0, 0x1020010

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public stopPlayingSound()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->unFocusAudio()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->updatePlayButton(Z)V

    return-void
.end method

.method public unFocusAudio()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :cond_0
    return-void
.end method
