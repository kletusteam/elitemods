.class Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ObserverHandler"
.end annotation


# instance fields
.field context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiui/util/SimRingtoneUtils;->isDefaultSoundUniform(Landroid/content/Context;I)Z

    move-result v0

    iget-object v2, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    const/16 v3, 0x40

    invoke-static {v2, v3}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundSettingUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v2

    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v4, 0x80

    if-eqz p1, :cond_5

    if-eq p1, v1, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_6

    const/4 v0, 0x4

    const/16 v1, 0x400

    const/16 v2, 0x10

    if-eq p1, v0, :cond_2

    const/4 v0, 0x5

    if-eq p1, v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    invoke-static {p1, v1}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundSettingUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    invoke-static {v0, v2, p1}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->saveDefaultSound(Landroid/content/Context;ILandroid/net/Uri;)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    invoke-static {p1, v2}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundSettingUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    invoke-static {v0, v1, p1}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->saveDefaultSound(Landroid/content/Context;ILandroid/net/Uri;)V

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    invoke-static {p1, v4}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundSettingUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object p1

    invoke-static {p1, v2}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$000(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result p1

    if-nez p1, :cond_6

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    const/4 v0, 0x0

    invoke-static {p1, v1, v0}, Lmiui/util/SimRingtoneUtils;->setDefaultSoundUniform(Landroid/content/Context;IZ)V

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    invoke-static {p1, v1, v2}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->saveDefaultSound(Landroid/content/Context;ILandroid/net/Uri;)V

    if-eqz v0, :cond_6

    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    invoke-static {p1, v4, v2}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->saveDefaultSound(Landroid/content/Context;ILandroid/net/Uri;)V

    goto :goto_0

    :cond_5
    iget-object p1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    invoke-static {p1, v1}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundSettingUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    invoke-static {v1, v3, p1}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->saveDefaultSound(Landroid/content/Context;ILandroid/net/Uri;)V

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$ObserverHandler;->context:Landroid/content/Context;

    invoke-static {v0, v4, p1}, Lcom/android/settings/coolsound/helper/ExtraRingtoneDelegate;->saveDefaultSound(Landroid/content/Context;ILandroid/net/Uri;)V

    :cond_6
    :goto_0
    invoke-static {}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$100()Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$UpdateCallback;

    move-result-object p1

    if-eqz p1, :cond_7

    invoke-static {}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->access$100()Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$UpdateCallback;

    move-result-object p1

    invoke-interface {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$UpdateCallback;->update()V

    :cond_7
    return-void
.end method
