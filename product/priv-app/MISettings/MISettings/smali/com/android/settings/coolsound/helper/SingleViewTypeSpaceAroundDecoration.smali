.class public Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;
.super Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;


# instance fields
.field protected mHeadMarginTop:I

.field protected mMarginBottom:I

.field protected mMarginTop:I

.field protected mTailMarginBottom:I

.field protected spanCount:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;-><init>(III)V

    iput p3, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->spanCount:I

    return-void
.end method


# virtual methods
.method public getSpanIndex(Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;I)I
    .locals 0

    iget p1, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->spanCount:I

    rem-int/2addr p3, p1

    return p3
.end method

.method public isTargetView(Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;I)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onGetItemOffsetsFinished(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$q;II)V
    .locals 4

    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$a;

    move-result-object p2

    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemCount()I

    move-result p2

    int-to-double p2, p2

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    mul-double/2addr p2, v0

    iget p4, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mSpanCount:I

    int-to-double v2, p4

    div-double/2addr p2, v2

    invoke-static {p2, p3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p2

    double-to-int p2, p2

    const/4 p3, 0x1

    add-int/2addr p5, p3

    int-to-double p4, p5

    mul-double/2addr p4, v0

    iget p6, p0, Lcom/android/settings/coolsound/helper/BaseSpaceAroundDecoration;->mSpanCount:I

    int-to-double v0, p6

    div-double/2addr p4, v0

    invoke-static {p4, p5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p4

    double-to-int p4, p4

    if-ne p4, p3, :cond_1

    iget p3, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->mHeadMarginTop:I

    iput p3, p1, Landroid/graphics/Rect;->top:I

    if-ne p4, p2, :cond_0

    iget p2, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->mTailMarginBottom:I

    iput p2, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    :cond_0
    iget p2, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->mMarginBottom:I

    iput p2, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    :cond_1
    if-ne p4, p2, :cond_2

    iget p2, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->mMarginTop:I

    iput p2, p1, Landroid/graphics/Rect;->top:I

    iget p2, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->mTailMarginBottom:I

    iput p2, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    :cond_2
    iget p2, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->mMarginTop:I

    iput p2, p1, Landroid/graphics/Rect;->top:I

    iget p2, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->mMarginBottom:I

    iput p2, p1, Landroid/graphics/Rect;->bottom:I

    :goto_0
    return-void
.end method

.method public setMargin(IIII)V
    .locals 0

    iput p1, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->mHeadMarginTop:I

    iput p2, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->mMarginTop:I

    iput p3, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->mMarginBottom:I

    iput p4, p0, Lcom/android/settings/coolsound/helper/SingleViewTypeSpaceAroundDecoration;->mTailMarginBottom:I

    return-void
.end method
