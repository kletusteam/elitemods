.class Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->onScrollStateChanged(Landroidx/recyclerview/widget/RecyclerView;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4$1;->this$1:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4$1;->this$1:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;

    iget-object v0, v0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {v0}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$200(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Lcom/android/settings/coolsound/MediaSoundPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4$1;->this$1:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;

    iget-object v0, v0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {v0}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$200(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Lcom/android/settings/coolsound/MediaSoundPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4$1;->this$1:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;

    iget-object v1, v1, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    invoke-static {v1}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$300(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4$1;->this$1:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;

    iget-object v2, v2, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    iget v2, v2, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->position:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/data/AnimalInfo;

    invoke-virtual {v1}, Lcom/android/settings/coolsound/data/AnimalInfo;->getSoundPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/MediaSoundPlayer;->startPlaySound(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4$1;->this$1:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;

    iget-object v0, v0, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity$4;->this$0:Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;->access$400(Lcom/android/settings/coolsound/AreaAnimalSoundPadActivity;Z)V

    :cond_0
    return-void
.end method
