.class public Lcom/android/settings/coolsound/CoolSoundPhoneActivity;
.super Lcom/misettings/common/base/BaseActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/CoolSoundPhoneActivity$SettingsObserver;,
        Lcom/android/settings/coolsound/CoolSoundPhoneActivity$MyPagerAdapter;,
        Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;,
        Lcom/android/settings/coolsound/CoolSoundPhoneActivity$ObserverHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field public static final INTENT_EXT_KEY:Ljava/lang/String; = "ringtone_type"

.field public static final INTENT_NORMAL:Ljava/lang/String; = "miui.intent.action.COOL_SOUND_PHONE"

.field public static final INTENT_SEARCH:Ljava/lang/String; = "miui.intent.action.COOL_SOUND_PHONE_SEARCH"

.field private static final MSG_RINGTONE_CHANGE:I = 0x0

.field private static final MSG_RINGTONE_SLOT1_CHANGE:I = 0x1

.field private static final MSG_RINGTONE_SLOT2_CHANGE:I = 0x2

.field private static final MSG_RINGTONE_UNIFORM_CHANGE:I = 0x3

.field private static final MSG_SMS_RINGTONE_CHANGE:I = 0x4

.field private static final MSG_SMS_RINGTONE_SLOT1_CHANGE:I = 0x5

.field private static final MSG_UPDATE_CALENDAR_SUMMARY:I = 0xb

.field private static final MSG_UPDATE_NOTES_SUMMARY:I = 0xc

.field private static final MSG_UPDATE_SMS_SUMMARY:I = 0xa

.field private static final PAGE_NUM_1:I = 0x1

.field private static final PAGE_NUM_2:I = 0x2

.field public static final TAG:Ljava/lang/String; = "CoolSoundPhoneActivity"

.field private static final TYPE_NOTES:I = 0x2000


# instance fields
.field private final PACKAGE_NAME_CALENDAR:Ljava/lang/String;

.field private final PACKAGE_NAME_NOTE:Ljava/lang/String;

.field private isScrolled:Z

.field private mCalendarRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

.field private mNoteRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

.field private mObserver:Landroid/database/ContentObserver;

.field private mObserverHandler:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$ObserverHandler;

.field mPageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mPageType:I

.field private mRingtoneH:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;

.field mRingtoneSoundUseUniform:Landroid/net/Uri;

.field private mSmsRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    const-string v0, "com.miui.notes"

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->PACKAGE_NAME_NOTE:Ljava/lang/String;

    const-string v0, "com.android.calendar"

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->PACKAGE_NAME_CALENDAR:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->isScrolled:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->isUriEqual(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;Ljava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->updateSmsSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;Ljava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->updateCalendarSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;Ljava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->updateNotesSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$402(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->isScrolled:Z

    return p1
.end method

.method static synthetic access$500(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->initSmsSoundIfNeeded()V

    return-void
.end method

.method static synthetic access$600(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->initSMS()V

    return-void
.end method

.method static synthetic access$700(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;)Lcom/android/settings/coolsound/CoolSoundPhoneActivity$ObserverHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserverHandler:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$ObserverHandler;

    return-object p0
.end method

.method public static getActionBarTitle(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    const v0, 0x7f130295

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    if-eq p1, v1, :cond_1

    const/4 v1, 0x5

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const p1, 0x7f130269

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const p1, 0x7f13006f

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private getResType()I
    .locals 5

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "miui.intent.action.COOL_SOUND_PHONE_SEARCH"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "ringtone_type"

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v4, "miui.intent.action.COOL_SOUND_PHONE"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0

    :cond_1
    return v1
.end method

.method private initNotificationView()V
    .locals 2

    const v0, 0x7f0b03ba

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->getResType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->init(I)V

    return-void
.end method

.method private initSMS()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mSmsRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->getUri()Landroid/net/Uri;

    move-result-object v0

    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->updateItemRingtone(Landroid/net/Uri;I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mCalendarRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->getUri()Landroid/net/Uri;

    move-result-object v0

    const/16 v1, 0xb

    invoke-direct {p0, v0, v1}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->updateItemRingtone(Landroid/net/Uri;I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mNoteRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/settings/coolsound/widget/DefaultRingtoneItem;->getUri()Landroid/net/Uri;

    move-result-object v0

    const/16 v1, 0xc

    invoke-direct {p0, v0, v1}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->updateItemRingtone(Landroid/net/Uri;I)V

    :cond_2
    return-void
.end method

.method private initSMSView()V
    .locals 2

    iget v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$4;

    invoke-direct {v1, p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$4;-><init>(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private initSmsSoundIfNeeded()V
    .locals 11

    const v0, 0x7f0b03c5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->getResType()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_a

    const v0, 0x7f0b031a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f0b0319

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    iput-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mSmsRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    const v2, 0x7f0b02a3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    iput-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mNoteRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    const v2, 0x7f0b0096

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mCalendarRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mNoteRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mSmsRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    const/16 v3, 0x8

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/misettings/common/utils/SettingsFeatures;->isNeedRemoveSmsReceivedSound(Landroid/content/Context;)Z

    move-result v2

    const/4 v2, 0x0

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mSmsRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    const/16 v4, 0x10

    invoke-virtual {v2, v4}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->setRingtoneType(I)V

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mSmsRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x3f4ccccd    # 0.8f

    goto :goto_1

    :cond_2
    const v0, 0x3da3d70a    # 0.08f

    :goto_1
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/utils/H;->h(Landroid/content/Context;)Z

    move-result v2

    const v4, 0x3e666666    # 0.225f

    const/4 v5, 0x0

    if-eqz v2, :cond_3

    move v2, v4

    goto :goto_2

    :cond_3
    move v2, v5

    :goto_2
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/xiaomi/misettings/usagestats/utils/H;->h(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    move v6, v4

    goto :goto_3

    :cond_4
    move v6, v5

    :goto_3
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/xiaomi/misettings/usagestats/utils/H;->h(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_5

    goto :goto_4

    :cond_5
    move v4, v5

    :goto_4
    const/4 v5, 0x1

    new-array v7, v5, [Landroid/view/View;

    iget-object v8, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mSmsRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    aput-object v8, v7, v1

    invoke-static {v7}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v7

    invoke-interface {v7}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v7

    new-array v8, v1, [Lmiuix/animation/m$b;

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-interface {v7, v9, v8}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    invoke-interface {v7, v0, v2, v6, v4}, Lmiuix/animation/m;->b(FFFF)Lmiuix/animation/m;

    iget-object v8, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mSmsRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    new-array v10, v1, [Lmiuix/animation/a/a;

    invoke-interface {v7, v8, v10}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    new-array v7, v5, [Landroid/view/View;

    iget-object v8, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mNoteRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    aput-object v8, v7, v1

    invoke-static {v7}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v7

    invoke-interface {v7}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v7

    new-array v8, v1, [Lmiuix/animation/m$b;

    invoke-interface {v7, v9, v8}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    invoke-interface {v7, v0, v2, v6, v4}, Lmiuix/animation/m;->b(FFFF)Lmiuix/animation/m;

    iget-object v8, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mNoteRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    new-array v10, v1, [Lmiuix/animation/a/a;

    invoke-interface {v7, v8, v10}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    new-array v5, v5, [Landroid/view/View;

    iget-object v7, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mCalendarRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    aput-object v7, v5, v1

    invoke-static {v5}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v5

    invoke-interface {v5}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v5

    new-array v7, v1, [Lmiuix/animation/m$b;

    invoke-interface {v5, v9, v7}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    invoke-interface {v5, v0, v2, v6, v4}, Lmiuix/animation/m;->b(FFFF)Lmiuix/animation/m;

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mCalendarRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    new-array v2, v1, [Lmiuix/animation/a/a;

    invoke-interface {v5, v0, v2}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mCalendarRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    if-eqz v0, :cond_6

    const/16 v2, 0x1000

    invoke-virtual {v0, v2}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->setRingtoneType(I)V

    :cond_6
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mNoteRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    if-eqz v0, :cond_7

    const/16 v2, 0x2000

    invoke-virtual {v0, v2}, Lcom/android/settings/coolsound/widget/MiuiDefaultRingtoneItem;->setRingtoneType(I)V

    :cond_7
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mCalendarRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    const-string v4, "com.android.calendar"

    invoke-static {v0, v4}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v4

    if-eqz v4, :cond_8

    move v4, v1

    goto :goto_5

    :cond_8
    move v4, v3

    :goto_5
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mNoteRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    const-string v4, "com.miui.notes"

    invoke-static {v0, v4}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-eqz v0, :cond_9

    goto :goto_6

    :cond_9
    move v1, v3

    :goto_6
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mSmsRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    if-eqz v0, :cond_a

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mCalendarRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mNoteRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    if-eqz v1, :cond_a

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_a

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mNoteRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_a

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mCalendarRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_a

    const v0, 0x7f0b031b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_a
    return-void
.end method

.method private initTab()V
    .locals 8

    const v0, 0x7f0b031e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->isDoubleSimTab()Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v1, :cond_3

    const v6, 0x7f0e0035

    const/4 v7, 0x0

    invoke-static {p0, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    iget-object v7, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageList:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->isDoubleSimTab()Z

    move-result v7

    if-eqz v7, :cond_2

    if-nez v5, :cond_1

    move v7, v3

    goto :goto_2

    :cond_1
    move v7, v2

    :goto_2
    invoke-virtual {v6, v7}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->init(I)V

    goto :goto_3

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->getResType()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->init(I)V

    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    new-instance v5, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$MyPagerAdapter;

    iget-object v6, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageList:Ljava/util/List;

    invoke-direct {v5, v6}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$MyPagerAdapter;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v5}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/f;)V

    const v5, 0x7f0b02b4

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/android/settings/coolsound/widget/TabLayout;

    if-ne v1, v2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v4, 0x8

    :goto_4
    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-ne v1, v2, :cond_5

    invoke-virtual {v5, v0}, Lcom/android/settings/coolsound/widget/TabLayout;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    :cond_5
    new-instance v1, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$1;

    invoke-direct {v1, p0, v5}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$1;-><init>(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;Lcom/android/settings/coolsound/widget/TabLayout;)V

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$d;)V

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v5, v3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    invoke-virtual {v5, v3}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    invoke-virtual {v5}, Landroid/view/ViewGroup;->requestFocus()Z

    :cond_6
    return-void
.end method

.method private isDoubleSimTab()Z
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->getResType()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoCount()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private static isUriEqual(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 1

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 v0, 0x0

    if-nez p0, :cond_1

    if-eqz p1, :cond_1

    return v0

    :cond_1
    if-eqz p0, :cond_2

    invoke-virtual {p0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0

    :cond_2
    return v0
.end method

.method private notificationPageResize()V
    .locals 8

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070121

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const v1, 0x7f0b02dd

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/view/View;->setPadding(IIII)V

    const v1, 0x7f0b02a6

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070400

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f0b029b

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/view/View;->setPadding(IIII)V

    const v0, 0x7f0b0294

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070453

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070402

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070401

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v1, v3, v4, v3, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f0b031a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3, v2, v3, v2}, Landroid/view/View;->setPadding(IIII)V

    const v0, 0x7f0b031b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private registerObserver()V
    .locals 4

    new-instance v0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$ObserverHandler;

    invoke-direct {v0, p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$ObserverHandler;-><init>(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserverHandler:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$ObserverHandler;

    new-instance v0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$SettingsObserver;

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserverHandler:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$ObserverHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$SettingsObserver;-><init>(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$System;->DEFAULT_RINGTONE_URI_SLOT_1:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$System;->DEFAULT_RINGTONE_URI_SLOT_2:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const-string v0, "ringtone_sound_use_uniform"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mRingtoneSoundUseUniform:Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mRingtoneSoundUseUniform:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$System;->DEFAULT_SMS_RECEIVED_RINGTONE_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$System;->DEFAULT_SMS_RECEIVED_SOUND_URI_SLOT_1:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method private scrollToTopIfNeed()V
    .locals 4

    iget-boolean v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->isScrolled:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$3;

    invoke-direct {v1, p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$3;-><init>(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private updateCalendarSummary(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mCalendarRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;->setRightValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateItemRingtone(Landroid/net/Uri;I)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/media/ExtraRingtone;->getRingtoneTitle(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mRingtoneH:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mRingtoneH:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;

    invoke-virtual {v0, p2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method private updateNotesSummary(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mNoteRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;->setRightValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateSmsSummary(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mSmsRingtoneItem:Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;

    invoke-virtual {v0, p1}, Lcom/android/settings/coolsound/widget/MiuiSmsRingtoneItem;->setRightValue(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    iget v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v1, "dagu"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->notificationPageResize()V

    :cond_0
    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->getResType()I

    move-result p1

    iput p1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageType:I

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object p1

    iget v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageType:I

    invoke-static {p0, v0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->getActionBarTitle(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/appcompat/app/ActionBar;->a(Ljava/lang/CharSequence;)V

    new-instance p1, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;-><init>(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mRingtoneH:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;

    iget p1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageType:I

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    const p1, 0x7f0e0034

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->initNotificationView()V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->initSMSView()V

    goto :goto_0

    :cond_0
    const p1, 0x7f0e0033

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->initTab()V

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->scrollToTopIfNeed()V

    sget-boolean p1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->registerObserver()V

    goto :goto_1

    :cond_1
    new-instance p1, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$2;

    invoke-direct {p1, p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity$2;-><init>(Lcom/android/settings/coolsound/CoolSoundPhoneActivity;)V

    invoke-static {p1}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->registerUpdateCallback(Lcom/android/settings/coolsound/helper/RingtoneChangeHelper$UpdateCallback;)V

    :goto_1
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->onDestroy()V

    goto :goto_0

    :cond_1
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_2
    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->onPause()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->removeUpdateCallback()V

    :cond_2
    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->updateUI()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStop()V

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->onStop()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mRingtoneH:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;

    if-eqz v0, :cond_2

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mRingtoneH:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mRingtoneH:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;

    if-eqz v0, :cond_3

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mRingtoneH:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mRingtoneH:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;

    if-eqz v0, :cond_4

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mRingtoneH:Lcom/android/settings/coolsound/CoolSoundPhoneActivity$UpdateHandler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_4
    return-void
.end method

.method public updateUI()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->mPageList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/settings/coolsound/widget/CoolSoundPageLayout;->onResume()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/coolsound/CoolSoundPhoneActivity;->initSMS()V

    return-void
.end method
