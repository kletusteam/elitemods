.class Lcom/android/settings/coolsound/widget/IntelligentLayout$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/coolsound/widget/IntelligentLayout;->init(Lcom/android/settings/coolsound/MediaPlayerListener;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;


# direct methods
.method constructor <init>(Lcom/android/settings/coolsound/widget/IntelligentLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$1;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$1;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    invoke-static {p1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->access$000(Lcom/android/settings/coolsound/widget/IntelligentLayout;)Landroid/widget/VideoView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/VideoView;->isPlaying()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$1;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    invoke-static {p1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->access$000(Lcom/android/settings/coolsound/widget/IntelligentLayout;)Landroid/widget/VideoView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/VideoView;->start()V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$1;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    invoke-static {p1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->access$100(Lcom/android/settings/coolsound/widget/IntelligentLayout;)Lcom/android/settings/coolsound/MediaPlayerListener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$1;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    invoke-static {p1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->access$100(Lcom/android/settings/coolsound/widget/IntelligentLayout;)Lcom/android/settings/coolsound/MediaPlayerListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/android/settings/coolsound/MediaPlayerListener;->stopRingtoneGridSound()V

    :cond_0
    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$1;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    invoke-static {p1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->access$200(Lcom/android/settings/coolsound/widget/IntelligentLayout;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$1;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->pauseVideo(Z)V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/IntelligentLayout$1;->this$0:Lcom/android/settings/coolsound/widget/IntelligentLayout;

    invoke-virtual {p1}, Lcom/android/settings/coolsound/widget/IntelligentLayout;->stopPlayingSound()V

    :goto_0
    return-void
.end method
