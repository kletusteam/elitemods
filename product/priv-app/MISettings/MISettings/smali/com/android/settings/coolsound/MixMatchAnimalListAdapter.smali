.class public Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$a;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;,
        Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/os/Handler$Callback;",
        "Landroid/view/View$OnTouchListener;"
    }
.end annotation


# static fields
.field private static final HIDE_ALL_DELAY:I = 0xbb8

.field private static final HIDE_PLAYING_DELAY:I = 0x5dc

.field private static final MSG_HIDE_ALL:I = 0x400

.field private static final MSG_HIDE_PLAYING:I = 0x1000


# instance fields
.field private mAnimalList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mIconSizeNarrow:I

.field private final mIconSizeWide:I

.field private mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

.field private final mStateShrink:Lmiuix/animation/b/a;

.field private final mStateZoom:Lmiuix/animation/b/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    new-instance v0, Lmiuix/animation/b/a;

    const-string v1, "scale"

    invoke-direct {v0, v1}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v2, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    const-wide v3, 0x3fecccccc0000000L    # 0.8999999761581421

    invoke-virtual {v0, v2, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v2, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {v0, v2, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mStateShrink:Lmiuix/animation/b/a;

    new-instance v0, Lmiuix/animation/b/a;

    invoke-direct {v0, v1}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v1, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    iput-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mStateZoom:Lmiuix/animation/b/a;

    iput-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mContext:Landroid/content/Context;

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mHandler:Landroid/os/Handler;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->setHasStableIds(Z)V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f070447

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mIconSizeNarrow:I

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f070448

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mIconSizeWide:I

    return-void
.end method

.method private hideAdd(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showAdd:Z

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->isPlaying:Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private hidePlaying(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->isPlaying:Z

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :cond_0
    return-void
.end method

.method private hideSomeThing(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showAdd:Z

    if-nez v1, :cond_0

    iget-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showDelete:Z

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showAdd:Z

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showDelete:Z

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :cond_1
    return-void
.end method

.method private onAddClick(Landroid/view/View;)V
    .locals 3

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0b0206

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showAdd:Z

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

    iget-object v2, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;->canAddAnimal(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->added:Z

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

    invoke-interface {v1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;->onAddAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    :cond_0
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    return-void
.end method

.method private onDeleteClick(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f0b0206

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showAdd:Z

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->added:Z

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showDelete:Z

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

    invoke-interface {p1, v0}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;->onDeleteAnimal(Lcom/android/settings/coolsound/data/MixMatchAnimalItem;)V

    return-void
.end method

.method private onRootClick(Landroid/view/View;)V
    .locals 3

    const v0, 0x7f0b0206

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    iget-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->isPlaying:Z

    if-nez v1, :cond_1

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->sendHideSomeThingMsg(I)V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->sendHidePlayingMsg(I)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->isPlaying:Z

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

    invoke-interface {v2, p1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;->onItemClicked(I)V

    iget-boolean v2, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->added:Z

    if-eqz v2, :cond_0

    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showDelete:Z

    goto :goto_0

    :cond_0
    iput-boolean v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showAdd:Z

    :goto_0
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :cond_1
    return-void
.end method

.method private sendHidePlayingMsg(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mHandler:Landroid/os/Handler;

    add-int/lit16 v2, p1, 0x1000

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput v2, v0, Landroid/os/Message;->what:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x5dc

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    return-void
.end method

.method private sendHideSomeThingMsg(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    iget-object v1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mHandler:Landroid/os/Handler;

    add-int/lit16 v2, p1, 0x400

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput v2, v0, Landroid/os/Message;->what:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0xbb8

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public clearAll()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public getItem(I)Lcom/android/settings/coolsound/data/MixMatchAnimalItem;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    if-ltz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->getItemCount()I

    move-result v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    return-object p1

    :cond_1
    return-object v1
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getItemId(I)J
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->getItem(I)Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object p1

    if-nez p1, :cond_0

    const-wide/16 v0, -0x1

    return-wide v0

    :cond_0
    iget-object p1, p1, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->name:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget p1, p1, Landroid/os/Message;->what:I

    add-int/lit16 v1, v0, 0x400

    if-ne p1, v1, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->hideSomeThing(I)V

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit16 v1, v0, 0x1000

    if-ne p1, v1, :cond_1

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->hidePlaying(I)V

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->onBindViewHolder(Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;I)V
    .locals 5
    .param p1    # Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    iget-object v1, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mAnimalName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mContext:Landroid/content/Context;

    iget v3, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalNameRes:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mAnimalIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mContext:Landroid/content/Context;

    iget v3, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->animalNameRes:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->icon:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mAnimalIcon:Landroid/widget/ImageView;

    iget-object v2, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->icon:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    iget-object v1, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mAddBtn:Landroid/widget/Button;

    iget-boolean v2, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showAdd:Z

    const/4 v3, 0x0

    const/16 v4, 0x8

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_0

    :cond_1
    move v2, v4

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mDeleteBtn:Landroid/widget/Button;

    iget-boolean v2, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->showDelete:Z

    if-eqz v2, :cond_2

    move v2, v3

    goto :goto_1

    :cond_2
    move v2, v4

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mSelectedView:Landroid/view/View;

    iget-boolean v2, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->added:Z

    if-eqz v2, :cond_3

    move v2, v3

    goto :goto_2

    :cond_3
    move v2, v4

    :goto_2
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mPlayView:Landroid/view/View;

    iget-boolean v2, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->isPlaying:Z

    if-eqz v2, :cond_4

    move v2, v3

    goto :goto_3

    :cond_4
    move v2, v4

    :goto_3
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mCoverView:Landroid/view/View;

    iget-boolean v0, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->isPlaying:Z

    if-eqz v0, :cond_5

    goto :goto_4

    :cond_5
    move v3, v4

    :goto_4
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mAddBtn:Landroid/widget/Button;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0b0206

    invoke-virtual {v0, v2, v1}, Landroid/widget/Button;->setTag(ILjava/lang/Object;)V

    iget-object v0, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mDeleteBtn:Landroid/widget/Button;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/Button;->setTag(ILjava/lang/Object;)V

    iget-object v0, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mAnimalName:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mAnimalName:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mRootView:Landroid/view/View;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, v2, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object p2, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mRootView:Landroid/view/View;

    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mAddBtn:Landroid/widget/Button;

    invoke-virtual {p2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mDeleteBtn:Landroid/widget/Button;

    invoke-virtual {p2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p1, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;->mRootView:Landroid/view/View;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b03c3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->onRootClick(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b008b

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->onAddClick(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b008c

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->onDeleteClick(Landroid/view/View;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object p2, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v0, 0x7f0e008f

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;

    invoke-direct {p2, p1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object p2
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    new-array p2, v0, [Landroid/view/View;

    aput-object p1, p2, v1

    invoke-static {p2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mStateShrink:Lmiuix/animation/b/a;

    new-array v0, v1, [Lmiuix/animation/a/a;

    invoke-interface {p1, p2, v0}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    goto :goto_0

    :cond_0
    new-array p2, v0, [Landroid/view/View;

    aput-object p1, p2, v1

    invoke-static {p2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mStateZoom:Lmiuix/animation/b/a;

    new-array v0, v1, [Lmiuix/animation/a/a;

    invoke-interface {p1, p2, v0}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    :goto_0
    return v1
.end method

.method public registerListener(Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mListener:Lcom/android/settings/coolsound/MixMatchAnimalListAdapter$AnimalItemAddListener;

    return-void
.end method

.method public setData(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/coolsound/data/MixMatchAnimalItem;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->hideAdd(Ljava/util/List;)V

    iput-object p1, p0, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->mAnimalList:Ljava/util/List;

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    return-void
.end method

.method public setPlaying(IZ)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/coolsound/MixMatchAnimalListAdapter;->getItem(I)Lcom/android/settings/coolsound/data/MixMatchAnimalItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-boolean p2, v0, Lcom/android/settings/coolsound/data/MixMatchAnimalItem;->isPlaying:Z

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :cond_0
    return-void
.end method
