.class public Lcom/android/settings/coolsound/widget/BubbleLayout;
.super Landroid/widget/FrameLayout;


# instance fields
.field private mDatumPoint:Landroid/graphics/Point;

.field private mOffset:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field private mRadius:I

.field private mRectf:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1, p2}, Lcom/android/settings/coolsound/widget/BubbleLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private drawTriangle(Landroid/graphics/Canvas;)V
    .locals 6

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mRectf:Landroid/graphics/RectF;

    iget v3, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mRadius:I

    int-to-float v4, v3

    int-to-float v3, v3

    sget-object v5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mDatumPoint:Landroid/graphics/Point;

    iget v3, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v3, v0

    int-to-float v3, v3

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mDatumPoint:Landroid/graphics/Point;

    iget v3, v2, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget v2, v2, Landroid/graphics/Point;->y:I

    add-int/2addr v2, v0

    int-to-float v2, v2

    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mDatumPoint:Landroid/graphics/Point;

    iget v3, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v0

    int-to-float v0, v3

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    iget-object v0, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    sget-object v0, Lcom/xiaomi/misettings/g;->BubbleLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mRadius:I

    const/4 v2, 0x2

    invoke-virtual {p1, v2, p2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mOffset:I

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mPaint:Landroid/graphics/Paint;

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object p1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mPath:Landroid/graphics/Path;

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mRectf:Landroid/graphics/RectF;

    new-instance p1, Landroid/graphics/Point;

    invoke-direct {p1}, Landroid/graphics/Point;-><init>()V

    iput-object p1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mDatumPoint:Landroid/graphics/Point;

    invoke-virtual {p0, p2}, Landroid/widget/FrameLayout;->setWillNotDraw(Z)V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/android/settings/coolsound/widget/BubbleLayout;->drawTriangle(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    iget-object p3, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mRectf:Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getPaddingLeft()I

    move-result p4

    int-to-float p4, p4

    iput p4, p3, Landroid/graphics/RectF;->left:F

    iget-object p3, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mRectf:Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getPaddingTop()I

    move-result p4

    int-to-float p4, p4

    iput p4, p3, Landroid/graphics/RectF;->top:F

    iget-object p3, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mRectf:Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getPaddingRight()I

    move-result p4

    sub-int p4, p1, p4

    int-to-float p4, p4

    iput p4, p3, Landroid/graphics/RectF;->right:F

    iget-object p3, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mRectf:Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result p4

    sub-int p4, p2, p4

    int-to-float p4, p4

    iput p4, p3, Landroid/graphics/RectF;->bottom:F

    iget-object p3, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mDatumPoint:Landroid/graphics/Point;

    div-int/lit8 p1, p1, 0x2

    iput p1, p3, Landroid/graphics/Point;->x:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result p1

    sub-int/2addr p2, p1

    iput p2, p3, Landroid/graphics/Point;->y:I

    iget p1, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mOffset:I

    if-eqz p1, :cond_0

    iget-object p2, p0, Lcom/android/settings/coolsound/widget/BubbleLayout;->mDatumPoint:Landroid/graphics/Point;

    iget p3, p2, Landroid/graphics/Point;->x:I

    add-int/2addr p3, p1

    iput p3, p2, Landroid/graphics/Point;->x:I

    :cond_0
    return-void
.end method
