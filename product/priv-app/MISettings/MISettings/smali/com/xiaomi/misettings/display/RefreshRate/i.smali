.class public Lcom/xiaomi/misettings/display/RefreshRate/i;
.super Ljava/lang/Object;


# static fields
.field private static volatile a:Lcom/xiaomi/misettings/display/RefreshRate/i;


# instance fields
.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field private d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/i;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/i;->c:Landroid/content/Context;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/i;->b:Ljava/util/Map;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/xiaomi/misettings/display/RefreshRate/i;
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/i;->a:Lcom/xiaomi/misettings/display/RefreshRate/i;

    if-nez v0, :cond_1

    const-class v0, Lcom/xiaomi/misettings/display/RefreshRate/i;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/display/RefreshRate/i;->a:Lcom/xiaomi/misettings/display/RefreshRate/i;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/display/RefreshRate/i;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/display/RefreshRate/i;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/misettings/display/RefreshRate/i;->a:Lcom/xiaomi/misettings/display/RefreshRate/i;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    sget-object p0, Lcom/xiaomi/misettings/display/RefreshRate/i;->a:Lcom/xiaomi/misettings/display/RefreshRate/i;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/i;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/i;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method
