.class public Lcom/xiaomi/misettings/usagestats/focusmode/data/g;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/xiaomi/misettings/usagestats/focusmode/data/g;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:Z

.field private d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b:Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->c:Z

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->d:I

    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/usagestats/focusmode/data/g;)I
    .locals 1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    sub-int/2addr v0, p1

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->d:I

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b:Z

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->d:I

    return v0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->c:Z

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b:Z

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a(Lcom/xiaomi/misettings/usagestats/focusmode/data/g;)I

    move-result p1

    return p1
.end method
