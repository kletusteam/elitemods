.class public Lcom/xiaomi/misettings/usagestats/f/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected transient c:Landroid/content/pm/PackageInfo;

.field protected d:Ljava/lang/String;

.field protected e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/b;->a:Ljava/lang/String;

    const-string p1, ""

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/b;->b:Ljava/lang/String;

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/b;->c:Landroid/content/pm/PackageInfo;

    const/4 p1, -0x1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/f/b;->e:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/content/pm/PackageInfo;)V
    .locals 1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/b;->c:Landroid/content/pm/PackageInfo;

    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_0

    iget-object p1, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->category:I

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/h;->a(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/f/b;->e:I

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/b;->b:Ljava/lang/String;

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/f/b;->e:I

    return v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/b;->d:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/b;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/b;->a:Ljava/lang/String;

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/b;->a:Ljava/lang/String;

    return-object v0
.end method
