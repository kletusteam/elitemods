.class public Lcom/xiaomi/misettings/display/ExpertRadioPreference;
.super Lmiuix/preference/RadioButtonPreference;


# instance fields
.field private m:Landroid/widget/CheckedTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/preference/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/preference/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiuix/preference/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroidx/preference/B;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/preference/RadioButtonPreference;->onBindViewHolder(Landroidx/preference/B;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckedTextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ExpertRadioPreference;->m:Landroid/widget/CheckedTextView;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ExpertRadioPreference;->m:Landroid/widget/CheckedTextView;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/display/c;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/display/c;-><init>(Lcom/xiaomi/misettings/display/ExpertRadioPreference;)V

    invoke-virtual {p1, v0}, Landroid/widget/CheckedTextView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    :cond_0
    return-void
.end method
