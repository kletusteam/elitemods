.class Lcom/xiaomi/misettings/usagestats/home/category/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/category/a/f;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/home/category/c/c;

.field final synthetic b:Lcom/xiaomi/misettings/usagestats/home/category/a/f;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/category/a/f;Lcom/xiaomi/misettings/usagestats/home/category/c/c;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/e;->b:Lcom/xiaomi/misettings/usagestats/home/category/a/f;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    iget-boolean v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->a:Z

    const-string v1, "isWeek"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->c:Ljava/lang/String;

    const-string v1, "packageName"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->a:Z

    if-nez v1, :cond_0

    iget-wide v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->i:J

    const-string v2, "dayBeginTime"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    iget-wide v2, v1, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->i:J

    iput-wide v2, v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    iget-wide v1, v1, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->j:J

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    const-string v1, "weekInfo"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/e;->b:Lcom/xiaomi/misettings/usagestats/home/category/a/f;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->c(Lcom/xiaomi/misettings/usagestats/home/category/a/f;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    return-void
.end method
