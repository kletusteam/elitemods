.class final Lcom/xiaomi/misettings/usagestats/focusmode/c/k;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;->a:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const-string v0, "FocusModeUtils"

    const-string v1, "ensureFinishFocusMode: finish focus mode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->D(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;->b:Z

    const-string v2, "misettings_key_has_finish_focus"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "settings_focus_mode_status"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->c(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->B(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->c(Landroid/content/Context;)V

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->d(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/j;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/j;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/c/k;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_0
    return-void
.end method
