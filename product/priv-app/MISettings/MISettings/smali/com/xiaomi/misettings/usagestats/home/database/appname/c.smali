.class Lcom/xiaomi/misettings/usagestats/home/database/appname/c;
.super Landroidx/room/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/database/appname/d;-><init>(Landroidx/room/v;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/room/c<",
        "Lcom/xiaomi/misettings/usagestats/home/database/appname/a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic d:Lcom/xiaomi/misettings/usagestats/home/database/appname/d;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/database/appname/d;Landroidx/room/v;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/c;->d:Lcom/xiaomi/misettings/usagestats/home/database/appname/d;

    invoke-direct {p0, p2}, Landroidx/room/c;-><init>(Landroidx/room/v;)V

    return-void
.end method


# virtual methods
.method public a(La/p/a/f;Lcom/xiaomi/misettings/usagestats/home/database/appname/a;)V
    .locals 3

    iget v0, p2, Lcom/xiaomi/misettings/usagestats/home/database/appname/a;->a:I

    int-to-long v0, v0

    const/4 v2, 0x1

    invoke-interface {p1, v2, v0, v1}, La/p/a/d;->a(IJ)V

    iget-object v0, p2, Lcom/xiaomi/misettings/usagestats/home/database/appname/a;->b:Ljava/lang/String;

    const/4 v1, 0x2

    if-nez v0, :cond_0

    invoke-interface {p1, v1}, La/p/a/d;->a(I)V

    goto :goto_0

    :cond_0
    invoke-interface {p1, v1, v0}, La/p/a/d;->a(ILjava/lang/String;)V

    :goto_0
    iget-object p2, p2, Lcom/xiaomi/misettings/usagestats/home/database/appname/a;->c:Ljava/lang/String;

    const/4 v0, 0x3

    if-nez p2, :cond_1

    invoke-interface {p1, v0}, La/p/a/d;->a(I)V

    goto :goto_1

    :cond_1
    invoke-interface {p1, v0, p2}, La/p/a/d;->a(ILjava/lang/String;)V

    :goto_1
    return-void
.end method

.method public bridge synthetic a(La/p/a/f;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/xiaomi/misettings/usagestats/home/database/appname/a;

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/database/appname/c;->a(La/p/a/f;Lcom/xiaomi/misettings/usagestats/home/database/appname/a;)V

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    const-string v0, "INSERT OR REPLACE INTO `app_name`(`id`,`packageName`,`appName`) VALUES (nullif(?, 0),?,?)"

    return-object v0
.end method
