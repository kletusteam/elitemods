.class public Lcom/xiaomi/misettings/display/RefreshRate/RefreshRateActivity;
.super Lcom/misettings/common/base/BaseActivity;


# static fields
.field private static final a:Ljava/lang/Boolean;


# instance fields
.field private b:Landroidx/fragment/app/Fragment;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "support_smart_fps"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lb/c/a/b/a/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/misettings/display/RefreshRate/RefreshRateActivity;->a:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/display/b;->f(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "RefreshRateActivity"

    const-string v0, "The current device does not support refresh rate adjustment"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const v0, 0x1020002

    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentManager;->a(I)Landroidx/fragment/app/Fragment;

    move-result-object p1

    sget-object v1, Lcom/xiaomi/misettings/display/RefreshRate/RefreshRateActivity;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/RefreshRateActivity;->b:Landroidx/fragment/app/Fragment;

    if-nez p1, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object p1

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/RefreshRateActivity;->b:Landroidx/fragment/app/Fragment;

    invoke-virtual {p1, v0, v1}, Landroidx/fragment/app/qa;->b(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {p1}, Landroidx/fragment/app/qa;->a()I

    :cond_2
    return-void
.end method
