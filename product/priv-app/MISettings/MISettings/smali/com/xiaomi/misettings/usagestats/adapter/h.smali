.class public Lcom/xiaomi/misettings/usagestats/adapter/h;
.super Landroidx/recyclerview/widget/RecyclerView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/adapter/h$a;,
        Lcom/xiaomi/misettings/usagestats/adapter/h$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/xiaomi/misettings/usagestats/adapter/h$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/xiaomi/misettings/usagestats/adapter/h$b;

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/FamilyBean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/FamilyBean;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/h;->b:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->isOwner()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->isChild()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->isOwner()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->isChild()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    invoke-interface {p1, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/h;->c:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/FamilyBean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/h;->c:Ljava/util/List;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/h;->b:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/h;->a:Lcom/xiaomi/misettings/usagestats/adapter/h$b;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/adapter/h$b;->a(I)V

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/adapter/h$a;I)V
    .locals 6
    .param p1    # Lcom/xiaomi/misettings/usagestats/adapter/h$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41d00000    # 26.0f

    if-nez p2, :cond_0

    move v3, v2

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-static {v1, v3}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/adapter/h;->getItemCount()I

    move-result v3

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    if-ne p2, v3, :cond_1

    goto :goto_1

    :cond_1
    const/high16 v2, 0x41a00000    # 20.0f

    :goto_1
    invoke-static {v1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/h;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/greenguard/entity/FamilyBean;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/h$a;->a(Lcom/xiaomi/misettings/usagestats/adapter/h$a;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean;->getIcon()Ljava/lang/String;

    move-result-object v3

    const v5, 0x7f080183

    invoke-static {v2, v3, v5}, Lb/c/b/e;->a(Landroid/widget/ImageView;Ljava/lang/String;I)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/h$a;->b(Lcom/xiaomi/misettings/usagestats/adapter/h$a;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean;->getNickName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/h;->b:I

    const/4 v2, 0x0

    if-ne v0, p2, :cond_2

    goto :goto_2

    :cond_2
    move v4, v2

    :goto_2
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/h$a;->c(Lcom/xiaomi/misettings/usagestats/adapter/h$a;)Landroid/widget/FrameLayout;

    move-result-object p2

    if-eqz v4, :cond_3

    const v2, 0x7f08009f

    :cond_3
    invoke-virtual {p2, v2}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/h$a;->b(Lcom/xiaomi/misettings/usagestats/adapter/h$a;)Landroid/widget/TextView;

    move-result-object p2

    if-eqz v4, :cond_4

    const v0, 0x7f060323

    goto :goto_3

    :cond_4
    const v0, 0x7f060300

    :goto_3
    invoke-static {v1, v0}, Landroidx/core/content/a;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "width"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/h$a;->a(Lcom/xiaomi/misettings/usagestats/adapter/h$a;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/ImageView;->getWidth()I

    move-result p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "HomeAccountAdapter"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public synthetic a(Lcom/xiaomi/misettings/usagestats/adapter/h$a;Landroid/view/View;)V
    .locals 0

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/adapter/h;->a:Lcom/xiaomi/misettings/usagestats/adapter/h$b;

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$t;->getAdapterPosition()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/adapter/h;->a(I)V

    :cond_0
    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/adapter/h$b;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/h;->a:Lcom/xiaomi/misettings/usagestats/adapter/h$b;

    return-void
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/h;->c:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/xiaomi/misettings/usagestats/adapter/h$a;

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/adapter/h;->a(Lcom/xiaomi/misettings/usagestats/adapter/h$a;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/adapter/h;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/adapter/h$a;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/adapter/h$a;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance p2, Lcom/xiaomi/misettings/usagestats/adapter/h$a;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0e006e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/xiaomi/misettings/usagestats/adapter/h$a;-><init>(Landroid/view/View;)V

    iget-object p1, p2, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/adapter/a;

    invoke-direct {v0, p0, p2}, Lcom/xiaomi/misettings/usagestats/adapter/a;-><init>(Lcom/xiaomi/misettings/usagestats/adapter/h;Lcom/xiaomi/misettings/usagestats/adapter/h$a;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2
.end method
