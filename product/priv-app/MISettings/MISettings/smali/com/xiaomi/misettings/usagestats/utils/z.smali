.class Lcom/xiaomi/misettings/usagestats/utils/z;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/utils/A;-><init>(Ljava/lang/Runnable;Ljava/lang/Runnable;Ljava/lang/Runnable;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Runnable;

.field final synthetic b:Z

.field final synthetic c:Ljava/lang/Runnable;

.field final synthetic d:Ljava/lang/Runnable;

.field final synthetic e:Lcom/xiaomi/misettings/usagestats/utils/A;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/utils/A;Landroid/os/Looper;Ljava/lang/Runnable;ZLjava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->e:Lcom/xiaomi/misettings/usagestats/utils/A;

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->a:Ljava/lang/Runnable;

    iput-boolean p4, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->b:Z

    iput-object p5, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->c:Ljava/lang/Runnable;

    iput-object p6, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->d:Ljava/lang/Runnable;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget p1, p1, Landroid/os/Message;->what:I

    const-string v0, "BackgroundRecyclerManager"

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->a:Ljava/lang/Runnable;

    if-eqz p1, :cond_3

    const-string p1, "\u5f00\u59cb\u8fdb\u884c\u6062\u590d"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->a:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_3

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->b:Z

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->c:Ljava/lang/Runnable;

    if-eqz p1, :cond_1

    const-string p1, "\u7ed3\u675f\u5f53\u524d\u9875\u9762"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->c:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->e:Lcom/xiaomi/misettings/usagestats/utils/A;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/utils/A;->d()V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/z;->d:Ljava/lang/Runnable;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_3
    :goto_0
    return-void
.end method
