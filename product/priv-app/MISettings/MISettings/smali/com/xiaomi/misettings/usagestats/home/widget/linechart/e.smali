.class Lcom/xiaomi/misettings/usagestats/home/widget/linechart/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Z)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/e;->b:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/e;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/e;->b:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/e;->a:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/e;->b:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/e;->b:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method
