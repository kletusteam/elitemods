.class Lcom/xiaomi/misettings/display/RefreshRate/s;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/view/f$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    check-cast p1, Lmiuix/view/f;

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    iget-object p2, p2, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->m:Landroid/view/View;

    invoke-interface {p1, p2}, Lmiuix/view/f;->c(Landroid/view/View;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {p2}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->b(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p2

    invoke-interface {p1, p2}, Lmiuix/view/f;->a(Landroid/view/View;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-virtual {p2}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    if-nez p2, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-virtual {p2}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->a()Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lmiuix/appcompat/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-interface {p1, v0}, Lmiuix/view/f;->b(Landroid/view/View;)V

    invoke-virtual {p2}, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->i()V

    :cond_1
    invoke-interface {p1}, Lmiuix/view/f;->a()Landroid/widget/EditText;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {p2}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->g(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Landroid/text/TextWatcher;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 7

    check-cast p1, Lmiuix/view/f;

    invoke-interface {p1}, Lmiuix/view/f;->a()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->g(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Landroid/text/TextWatcher;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-virtual {p1}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-virtual {p1}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->b()Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    move-result-object p1

    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->b(Ljava/lang/String;)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-virtual {p1}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->a(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-virtual {p1}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->c()V

    const-string p1, "HighRefreshOptionsFragment"

    const-string v1, " here is onDestroyActionMode "

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/display/RefreshRate/e;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->i(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-static {}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->k()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->j(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->k(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->m(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Lcom/xiaomi/misettings/display/RefreshRate/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/misettings/display/RefreshRate/l;->a()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->a(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;Ljava/util/List;)Ljava/util/List;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->m(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Lcom/xiaomi/misettings/display/RefreshRate/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/misettings/display/RefreshRate/l;->b()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->a(Ljava/util/List;)Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->k()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v2}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->j(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;

    move-result-object v2

    new-instance v4, Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object v5, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-virtual {v5}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v5

    invoke-static {}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->i()I

    move-result v6

    invoke-direct {v4, v5, v1, v3, v6}, Lcom/xiaomi/misettings/display/RefreshRate/m;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v2}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->i(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v2}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->k(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object v4, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-virtual {v4}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v4

    invoke-static {}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->j()I

    move-result v5

    invoke-direct {v3, v4, v1, v0, v5}, Lcom/xiaomi/misettings/display/RefreshRate/m;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v2}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;

    move-result-object v2

    new-instance v4, Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object v5, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-virtual {v5}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v5

    invoke-static {}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->j()I

    move-result v6

    invoke-direct {v4, v5, v1, v3, v6}, Lcom/xiaomi/misettings/display/RefreshRate/m;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->c(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/s;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->d(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Lcom/xiaomi/misettings/display/RefreshRate/p;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method
