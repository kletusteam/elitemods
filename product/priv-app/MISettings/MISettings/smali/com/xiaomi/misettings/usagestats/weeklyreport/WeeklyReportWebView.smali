.class public Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;
.super Lcom/misettings/common/widget/webview/BaseWebView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$b;,
        Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;
    }
.end annotation


# instance fields
.field private d:Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/misettings/common/widget/webview/BaseWebView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/misettings/common/widget/webview/BaseWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/misettings/common/widget/webview/BaseWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;)Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->d:Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/weeklyreport/d;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/weeklyreport/d;-><init>(Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->e:Landroid/os/Handler;

    return-object p0
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->d:Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->e:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/weeklyreport/b;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/b;-><init>(Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->c()V

    return-void
.end method

.method private d()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->d:Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;->b()V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->d()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    invoke-super {p0}, Lcom/misettings/common/widget/webview/BaseWebView;->a()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->e:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->getSettings()Lcom/miui/webkit_api/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/WebSettings;->setAllowFileAccess(Z)V

    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->getSettings()Lcom/miui/webkit_api/WebSettings;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/WebSettings;->setCacheMode(I)V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$b;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$b;-><init>(Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;)V

    const-string v1, "JSMiSettings"

    invoke-virtual {p0, v0, v1}, Lcom/miui/webkit_api/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method protected a(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->d:Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;->a(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "javascript:requestAppIconFinish("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/a;-><init>(Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;)V

    invoke-virtual {p0, p1, v0}, Lcom/miui/webkit_api/WebView;->evaluateJavascript(Ljava/lang/String;Lcom/miui/webkit_api/ValueCallback;)V

    return-void
.end method

.method protected b()V
    .locals 2

    const-string v0, "WeeklyReportWebView"

    const-string v1, "onRequestTimeOut: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->d:Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;->c()V

    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->e:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    invoke-super {p0}, Lcom/misettings/common/widget/webview/BaseWebView;->destroy()V

    return-void
.end method

.method public setListener(Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView;->d:Lcom/xiaomi/misettings/usagestats/weeklyreport/WeeklyReportWebView$a;

    return-void
.end method
