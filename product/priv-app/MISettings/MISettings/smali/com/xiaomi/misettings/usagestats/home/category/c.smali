.class public Lcom/xiaomi/misettings/usagestats/home/category/c;
.super Landroidx/recyclerview/widget/RecyclerView$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/xiaomi/misettings/usagestats/d/b/b<",
        "Lb/c/a/a/a;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb/c/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb/c/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private final f:I

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lb/c/a/a/a;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->e:Z

    const/4 v1, 0x3

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->f:I

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->g:Z

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->b:Ljava/util/List;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/c;->b()V

    return-void
.end method

.method private b()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->b:Ljava/util/List;

    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->d:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->d:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->g:Z

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lb/c/a/a/a;

    iget v3, v2, Lb/c/a/a/a;->type:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_4

    instance-of v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->e:Z

    const/4 v4, 0x3

    if-nez v3, :cond_3

    if-ne v0, v4, :cond_3

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->d:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    if-lt v0, v4, :cond_2

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->g:Z

    goto :goto_0

    :cond_4
    iget v3, v2, Lb/c/a/a/a;->type:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_5

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->g:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->d:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->d:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    return-void

    :cond_7
    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->d:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/category/c;->getItemCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :cond_0
    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/d/b/b;I)V
    .locals 1
    .param p1    # Lcom/xiaomi/misettings/usagestats/d/b/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/misettings/usagestats/d/b/b<",
            "Lb/c/a/a/a;",
            ">;I)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, p0, v0, p2}, Lcom/xiaomi/misettings/usagestats/d/b/b;->a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/c;->a(Ljava/util/List;Z)V

    return-void
.end method

.method public a(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->c:Ljava/util/List;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->b:Ljava/util/List;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->c:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/c/a/a/a;

    iget v1, v0, Lb/c/a/a/a;->type:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->c:Ljava/util/List;

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->c:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->d:Z

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/c;->b()V

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->e:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/c;->b()V

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    return-void
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->d:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lb/c/a/a/a;

    iget p1, p1, Lb/c/a/a/a;->type:I

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/b/b;

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/c;->a(Lcom/xiaomi/misettings/usagestats/d/b/b;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/c;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/d/b/b;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/d/b/b;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/xiaomi/misettings/usagestats/d/b/b<",
            "Lb/c/a/a/a;",
            ">;"
        }
    .end annotation

    const/4 p1, 0x0

    if-eqz p2, :cond_4

    const/4 v0, 0x1

    if-eq p2, v0, :cond_3

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    new-instance p2, Lcom/xiaomi/misettings/usagestats/focusmode/b/g;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->a:Landroid/content/Context;

    const v1, 0x7f0e0155

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/b/g;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/xiaomi/misettings/usagestats/home/category/a/g;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->a:Landroid/content/Context;

    const v1, 0x7f0e0072

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/g;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    :cond_1
    new-instance p2, Lcom/xiaomi/misettings/usagestats/home/category/a/n;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->a:Landroid/content/Context;

    const v1, 0x7f0e013e

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    :cond_2
    new-instance p2, Lcom/xiaomi/misettings/usagestats/home/category/a/f;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->a:Landroid/content/Context;

    const v1, 0x7f0e0031

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/f;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    :cond_3
    new-instance p2, Lcom/xiaomi/misettings/usagestats/d/b/a;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->a:Landroid/content/Context;

    const v1, 0x7f0e0069

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/a;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    :cond_4
    new-instance p2, Lcom/xiaomi/misettings/usagestats/home/category/a/h;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c;->a:Landroid/content/Context;

    const v1, 0x7f0e0032

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/h;-><init>(Landroid/content/Context;Landroid/view/View;)V

    :goto_0
    return-object p2
.end method
