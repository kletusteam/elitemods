.class public Lcom/xiaomi/misettings/usagestats/utils/Y;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/Y;->a:Ljava/util/List;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/Y;->a:Ljava/util/List;

    const-string v1, "grus"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/Y;->a:Ljava/util/List;

    const-string v1, "cepheus"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static a(Landroid/content/Context;)J
    .locals 6

    invoke-static {p0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    const-string v1, "settings_extra_time"

    invoke-virtual {v0, v1}, Lb/e/a/b/h;->a(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    long-to-double v4, v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    double-to-long v2, v2

    invoke-static {p0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object p0

    invoke-virtual {p0, v1, v2, v3}, Lb/e/a/b/h;->b(Ljava/lang/String;J)V

    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "getUploadExtraTime: extraTime = "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "UploadDataUtils"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-wide v2
.end method

.method public static a(JLandroidx/work/d;)Landroidx/work/p;
    .locals 4

    new-instance v0, Landroidx/work/p$a;

    const-class v1, Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;

    invoke-direct {v0, v1}, Landroidx/work/p$a;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v0, p2}, Landroidx/work/z$a;->a(Landroidx/work/d;)Landroidx/work/z$a;

    check-cast v0, Landroidx/work/p$a;

    sget-object p2, Landroidx/work/a;->b:Landroidx/work/a;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, p2, v2, v3, v1}, Landroidx/work/z$a;->a(Landroidx/work/a;JLjava/util/concurrent/TimeUnit;)Landroidx/work/z$a;

    check-cast v0, Landroidx/work/p$a;

    new-instance p2, Landroidx/work/f$a;

    invoke-direct {p2}, Landroidx/work/f$a;-><init>()V

    const-string v1, "input_date"

    invoke-virtual {p2, v1, p0, p1}, Landroidx/work/f$a;->a(Ljava/lang/String;J)Landroidx/work/f$a;

    invoke-virtual {p2}, Landroidx/work/f$a;->a()Landroidx/work/f;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroidx/work/z$a;->a(Landroidx/work/f;)Landroidx/work/z$a;

    check-cast v0, Landroidx/work/p$a;

    invoke-virtual {v0}, Landroidx/work/z$a;->a()Landroidx/work/z;

    move-result-object p0

    check-cast p0, Landroidx/work/p;

    return-object p0
.end method

.method public static a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 6

    invoke-static {p0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object p0

    const-string v0, "settings_use_apptimer_time"

    invoke-virtual {p0, v0}, Lb/e/a/b/h;->a(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/16 v4, 0x0

    cmp-long p0, v0, v4

    if-lez p0, :cond_0

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    const-wide/16 v4, 0x38

    mul-long/2addr v0, v4

    cmp-long p0, v2, v0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 7

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/Y;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-eqz v0, :cond_1

    const-string v1, "UploadDataUtils"

    const-string v2, "registerUploadServiceAlarm: "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/miui/greenguard/UploadDataService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.miu"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v2, 0x65

    const/4 v3, 0x0

    invoke-static {p0, v2, v1, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const/4 v2, 0x1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v3

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    add-long/2addr v3, v5

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/Y;->a(Landroid/content/Context;)J

    move-result-wide v5

    add-long/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    :cond_1
    return-void
.end method

.method public static d(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Lb/e/a/b/b;->h(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/q;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/a/q;->c(Landroid/content/Context;)V

    return-void

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/Y;->e(Landroid/content/Context;)V

    return-void
.end method

.method private static e(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/utils/X;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/utils/X;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method
