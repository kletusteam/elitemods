.class public Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/Context;I)V
    .locals 1

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "holiday_local_version"

    invoke-virtual {p0, v0, p1}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Landroid/content/Context;J)V
    .locals 1

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "last_update_time"

    invoke-virtual {p0, v0, p1, p2}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;J)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "statutory_holiday"

    invoke-virtual {p0, v0, p1}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 1

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "register_update_alarm_state"

    invoke-virtual {p0, v0, p1}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "register_update_alarm_state"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method public static b(Landroid/content/Context;)J
    .locals 3

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "last_update_time"

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "statutory_holiday"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static d(Landroid/content/Context;)I
    .locals 2

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "holiday_local_version"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static e(Landroid/content/Context;)V
    .locals 6

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-eqz v0, :cond_0

    const-string v1, "StatutoryHolidayUtils"

    const-string v2, "registerUpdateHolidayServiceAlarm: "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/xiaomi/misettings/usagestats/statutoryholidays/UpdateHolidayService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.xiaomi.misettings"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v2, 0x66

    const/high16 v3, 0x4000000

    invoke-static {p0, v2, v1, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    add-long/2addr v2, v4

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->j(Landroid/content/Context;)J

    move-result-wide v4

    add-long/2addr v2, v4

    const/4 v4, 0x1

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    invoke-static {p0, v4}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->a(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method

.method public static f(Landroid/content/Context;)V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    new-instance v1, Lb/c/b/c/g;

    sget-object v2, Lb/c/b/c/a$a;->a:Lb/c/b/c/a$a;

    new-instance v3, Lcom/xiaomi/misettings/usagestats/statutoryholidays/f;

    invoke-direct {v3, p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/f;-><init>(Landroid/content/Context;)V

    const-string p0, "/holiday/detail"

    invoke-direct {v1, p0, v0, v2, v3}, Lb/c/b/c/g;-><init>(Ljava/lang/String;Ljava/util/Map;Lb/c/b/c/a$a;Lb/c/b/c/h;)V

    const/4 p0, 0x3

    invoke-virtual {v1, p0}, Lb/c/b/c/g;->a(I)V

    invoke-virtual {v1}, Lb/c/b/c/g;->a()V

    return-void
.end method

.method public static g(Landroid/content/Context;)V
    .locals 4

    new-instance v0, Lb/c/b/c/g;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sget-object v2, Lb/c/b/c/a$a;->a:Lb/c/b/c/a$a;

    new-instance v3, Lcom/xiaomi/misettings/usagestats/statutoryholidays/e;

    invoke-direct {v3, p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/e;-><init>(Landroid/content/Context;)V

    const-string p0, "/holiday/version"

    invoke-direct {v0, p0, v1, v2, v3}, Lb/c/b/c/g;-><init>(Ljava/lang/String;Ljava/util/Map;Lb/c/b/c/a$a;Lb/c/b/c/h;)V

    invoke-virtual {v0}, Lb/c/b/c/g;->a()V

    return-void
.end method

.method public static h(Landroid/content/Context;)Z
    .locals 5

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->b(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "shouldRequestVersionCode: last time : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " , is same day : "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StatutoryHolidayUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v2, :cond_0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->a(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static i(Landroid/content/Context;)V
    .locals 4

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-eqz v0, :cond_0

    const-string v1, "StatutoryHolidayUtils"

    const-string v2, "unRegisterUpdateHolidayServiceAlarm: "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/xiaomi/misettings/usagestats/statutoryholidays/UpdateHolidayService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.xiaomi.misettings"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v2, 0x66

    const/high16 v3, 0x4000000

    invoke-static {p0, v2, v1, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->a(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method

.method private static j(Landroid/content/Context;)J
    .locals 6

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v1, "last_random_time"

    invoke-virtual {v0, v1}, Lcom/misettings/common/utils/p;->c(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    long-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-long v2, v2

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    invoke-virtual {p0, v1, v2, v3}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;J)V

    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "getUploadExtraTime: extraTime = "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "StatutoryHolidayUtils"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-wide v2
.end method
