.class public Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;
.super Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$b;,
        Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$c;,
        Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$d;,
        Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$a;
    }
.end annotation


# instance fields
.field private k:Lcom/xiaomi/misettings/usagestats/f/m;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ">;"
        }
    .end annotation
.end field

.field private m:Landroid/content/BroadcastReceiver;

.field private n:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;-><init>()V

    return-void
.end method

.method public static a(ZLjava/lang/String;)Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    move-result-object p0

    return-object p0
.end method

.method public static a(ZLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "misettings_from_page"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "screen_time_home_intent_key"

    invoke-virtual {v1, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string p1, "isWeek"

    invoke-virtual {v1, p1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/f/g;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->c:Landroid/os/Handler;

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->c:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$c;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-direct {v1, v2, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$c;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a;Lcom/xiaomi/misettings/usagestats/f/g;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->q()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;Lcom/xiaomi/misettings/usagestats/f/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->a(Lcom/xiaomi/misettings/usagestats/f/g;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .locals 1

    instance-of v0, p1, Lcom/xiaomi/misettings/usagestats/f/m;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/m;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->k:Lcom/xiaomi/misettings/usagestats/f/m;

    goto :goto_0

    :cond_0
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->l:Ljava/util/List;

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->c:Landroid/os/Handler;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$a;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$a;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->p()V

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->s()V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->r()V

    return-void
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method static synthetic h(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private o()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->j:Z

    const/4 v1, 0x1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->h:Ljava/lang/String;

    const-string v2, "steady_on"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->h:Ljava/lang/String;

    const-string v2, "disallow_limit_app"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->h:Ljava/lang/String;

    const-string v2, "device_limit"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d(I)V

    goto :goto_1

    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d(I)V

    :cond_4
    :goto_1
    return-void
.end method

.method private p()V
    .locals 6

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->k()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->k:Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/m;->h()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/k;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/k;->d()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v4

    iget-wide v4, v4, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->k:Lcom/xiaomi/misettings/usagestats/f/m;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->h:Ljava/lang/String;

    invoke-static {v0, v2, v4}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/m;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Ljava/util/List;I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->o()V

    return-void
.end method

.method private q()V
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->k()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->l:Ljava/util/List;

    invoke-static {v0, v2}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v0, v2}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Ljava/util/List;I)V

    return-void
.end method

.method private r()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$d;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$d;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private s()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->l()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->r()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SubContentFragment"

    const-string v2, "refreshUnlock error"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    return-void
.end method

.method private t()V
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/G;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/G;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->n:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/H;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/H;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->m:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "misettings.action.EXCHANGE_STEADY_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "misettings.action.EXCHANGE_DEVICE_LIMIT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "misettings.action.EXCHANGE_DETAIL_LIST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "misettings.action.NOTIFY_TODAY_DATA"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "misettings.action.FORCE_NOTIFY_DATA"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "miui.token.change"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-static {v1}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "miui.android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->n:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method


# virtual methods
.method public b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const p3, 0x7f0e0061

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method protected l()V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$b;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$b;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->t()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->m:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->n:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->n:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->g()V

    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    return-void
.end method
