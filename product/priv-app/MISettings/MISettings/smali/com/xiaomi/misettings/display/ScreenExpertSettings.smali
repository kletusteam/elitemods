.class public Lcom/xiaomi/misettings/display/ScreenExpertSettings;
.super Lmiuix/preference/PreferenceFragment;

# interfaces
.implements Lcom/xiaomi/misettings/display/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/s;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;

.field private c:Landroidx/preference/PreferenceScreen;

.field private d:Lcom/xiaomi/misettings/display/RestoreExpertPreference;

.field private e:Lcom/xiaomi/misettings/display/ResetExpertPreference;

.field private f:Landroid/view/View;

.field private g:Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;

.field private h:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/preference/PreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->a:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/misettings/display/r;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/display/r;-><init>(Lcom/xiaomi/misettings/display/ScreenExpertSettings;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->h:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/ScreenExpertSettings;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->a:Ljava/util/List;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/display/ScreenExpertSettings;)Landroidx/preference/PreferenceScreen;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->c:Landroidx/preference/PreferenceScreen;

    return-object p0
.end method


# virtual methods
.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->e:Lcom/xiaomi/misettings/display/ResetExpertPreference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/display/ResetExpertPreference;->b()V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->d:Lcom/xiaomi/misettings/display/RestoreExpertPreference;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/xiaomi/misettings/display/RestoreExpertPreference;->b()V

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-static {}, Lcom/xiaomi/misettings/display/d;->a()V

    invoke-super {p0, p1}, Lmiuix/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    sget p1, Lcom/xiaomi/misettings/display/o;->screen_expert_settings:I

    invoke-virtual {p0, p1, p2}, Landroidx/preference/PreferenceFragmentCompat;->setPreferencesFromResource(ILjava/lang/String;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    sget v0, Lcom/xiaomi/misettings/display/k;->screen_expert_settings:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->f:Landroid/view/View;

    sget v1, Lcom/xiaomi/misettings/display/j;->prefs_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Lmiuix/preference/PreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->f:Landroid/view/View;

    return-object p1
.end method

.method public onDestroy()V
    .locals 0

    invoke-static {}, Lcom/xiaomi/misettings/display/a/b;->a()V

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 2

    invoke-super {p0}, Landroidx/preference/PreferenceFragmentCompat;->onDestroyView()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->g:Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->b()V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    return-void
.end method

.method public onPreferenceTreeClick(Landroidx/preference/Preference;)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->b:Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->e(Landroidx/preference/Preference;)V

    :cond_0
    invoke-super {p0, p1}, Lmiuix/preference/PreferenceFragment;->onPreferenceTreeClick(Landroidx/preference/Preference;)Z

    move-result p1

    return p1
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Landroidx/preference/PreferenceFragmentCompat;->onStart()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Landroidx/preference/PreferenceFragmentCompat;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->a:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    sget-object p1, Lcom/xiaomi/misettings/display/a/a;->H:[Ljava/lang/String;

    array-length p2, p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    move v2, v0

    move v3, v1

    :goto_0
    if-ge v2, p2, :cond_1

    aget-object v4, p1, v2

    invoke-virtual {p0, v4}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/display/s;

    iget-object v5, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->a:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    instance-of v5, v4, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;

    if-eqz v5, :cond_0

    check-cast v4, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;

    invoke-virtual {v4, v3, p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->a(ILcom/xiaomi/misettings/display/s;)V

    add-int/lit8 v3, v3, 0x1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string p1, "root_preference"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceScreen;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->c:Landroidx/preference/PreferenceScreen;

    const-string p1, "color_gamut"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->b:Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->b:Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;

    if-eqz p1, :cond_2

    invoke-virtual {p1, v0, p0}, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->a(ILcom/xiaomi/misettings/display/s;)V

    :cond_2
    new-instance p1, Landroid/content/IntentFilter;

    const-string p2, "com.xiaomi.action.REFRESH_EXPERT"

    invoke-direct {p1, p2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p2

    iget-object v2, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {p2, v2, p1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string p1, "restore_expert"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/display/RestoreExpertPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->d:Lcom/xiaomi/misettings/display/RestoreExpertPreference;

    const-string p1, "reset_edit"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/display/ResetExpertPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->e:Lcom/xiaomi/misettings/display/ResetExpertPreference;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->d:Lcom/xiaomi/misettings/display/RestoreExpertPreference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getListView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/display/RestoreExpertPreference;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->e:Lcom/xiaomi/misettings/display/ResetExpertPreference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getListView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/display/ResetExpertPreference;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    sget-boolean p1, Lcom/xiaomi/misettings/display/d;->c:Z

    if-eqz p1, :cond_3

    const-string p1, "original_gamut_key"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/display/ExpertRadioPreference;

    if-eqz p1, :cond_3

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_3
    sget-boolean p1, Lcom/xiaomi/misettings/display/d;->b:Z

    if-eqz p1, :cond_4

    const-string p1, "primary_color"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/display/ExpertRadioPreference;

    if-eqz p1, :cond_4

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_4
    iget-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->b:Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->h()V

    :cond_5
    new-instance p1, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;-><init>(Lcom/xiaomi/misettings/display/ScreenExpertSettings;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->g:Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->g:Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->a()V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->g:Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;

    invoke-virtual {p1, v1}, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->onChange(Z)V

    :cond_6
    return-void
.end method
