.class public Lcom/xiaomi/misettings/usagestats/f/l;
.super Ljava/lang/Object;


# static fields
.field public static a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation
.end field

.field protected static b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;"
        }
    .end annotation
.end field

.field protected static c:Lcom/xiaomi/misettings/usagestats/f/i;


# instance fields
.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/xiaomi/misettings/usagestats/f/i;)V
    .locals 0

    sput-object p0, Lcom/xiaomi/misettings/usagestats/f/l;->c:Lcom/xiaomi/misettings/usagestats/f/i;

    return-void
.end method

.method public static b()V
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/f/l;->a:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/f/l;->a:Ljava/util/List;

    :cond_0
    sput-object v1, Lcom/xiaomi/misettings/usagestats/f/l;->c:Lcom/xiaomi/misettings/usagestats/f/i;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/xiaomi/misettings/usagestats/f/l;->a:Ljava/util/List;

    return-object v0
.end method

.method public declared-synchronized a(Landroid/content/Context;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/f/l;->a:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v0, "UsageFloorData"

    const-string v1, "setDayAppUsageStatsWeekList: reloadWeekData"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/usagestats/f/l;->a:Ljava/util/List;

    goto :goto_0

    :cond_0
    const-string p1, "UsageFloorData"

    const-string v0, "setDayAppUsageStatsWeekList: useCacheWeekData"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public b(Landroid/content/Context;)V
    .locals 5

    sget-object v0, Lcom/xiaomi/misettings/usagestats/f/l;->b:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/Q;->b(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/usagestats/f/l;->b:Ljava/util/List;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/f/l;->c:Lcom/xiaomi/misettings/usagestats/f/i;

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/f/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lcom/xiaomi/misettings/usagestats/f/l;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/i;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v1

    iget-wide v1, v1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    sget-object v3, Lcom/xiaomi/misettings/usagestats/f/l;->c:Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/i;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v3

    iget-wide v3, v3, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    sget-object p1, Lcom/xiaomi/misettings/usagestats/f/l;->b:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    sget-object p1, Lcom/xiaomi/misettings/usagestats/f/l;->b:Ljava/util/List;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/f/l;->c:Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/xiaomi/misettings/usagestats/f/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/Q;->b(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/usagestats/f/l;->b:Ljava/util/List;

    :cond_2
    :goto_0
    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/l;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    return-void
.end method

.method public declared-synchronized c(Landroid/content/Context;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/l;->d:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/l;->d:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
