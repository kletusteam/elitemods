.class public Lcom/xiaomi/misettings/usagestats/home/category/a/g;
.super Lcom/xiaomi/misettings/usagestats/d/b/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/usagestats/d/b/b<",
        "Lb/c/a/a/a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ImageView;

.field private d:Lcom/xiaomi/misettings/usagestats/home/category/c/d;

.field e:Lcom/xiaomi/misettings/usagestats/home/category/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/b/b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const p1, 0x7f0b0386

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->b:Landroid/widget/TextView;

    const p1, 0x7f0b023c

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->c:Landroid/widget/ImageView;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/view/View;)V

    new-instance p1, Lcom/xiaomi/misettings/usagestats/home/category/a/a;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/a;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/a/g;)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private c()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->d:Lcom/xiaomi/misettings/usagestats/home/category/c/d;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/category/c/d;->a()Z

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    const v3, 0x7f130076

    goto :goto_0

    :cond_0
    const v3, 0x7f130075

    :goto_0
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    const v0, 0x7f08018c

    goto :goto_1

    :cond_1
    const v0, 0x7f08017b

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->d:Lcom/xiaomi/misettings/usagestats/home/category/c/d;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/category/c/d;->b()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->c()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->e:Lcom/xiaomi/misettings/usagestats/home/category/c;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->d:Lcom/xiaomi/misettings/usagestats/home/category/c/d;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/category/c/d;->a()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/c;->a(Z)V

    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V
    .locals 1

    const-string p3, "CategoryExpandHolder"

    const-string v0, "CategoryLimitHolder bindView"

    invoke-static {p3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/category/c;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->e:Lcom/xiaomi/misettings/usagestats/home/category/c;

    check-cast p2, Lcom/xiaomi/misettings/usagestats/home/category/c/d;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->d:Lcom/xiaomi/misettings/usagestats/home/category/c/d;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->c()V

    return-void
.end method

.method public bridge synthetic a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V
    .locals 0

    check-cast p2, Lb/c/a/a/a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/home/category/a/g;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V

    return-void
.end method
