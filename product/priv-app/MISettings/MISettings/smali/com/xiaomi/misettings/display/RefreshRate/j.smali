.class public Lcom/xiaomi/misettings/display/RefreshRate/j;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/display/RefreshRate/j$a;
    }
.end annotation


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/display/RefreshRate/j;->a:Ljava/util/Map;

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/j;->a:Ljava/util/Map;

    const/16 v1, 0x3c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "fps_60hz.json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/j;->a:Ljava/util/Map;

    const/16 v1, 0x5a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "fps_90hz.json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/j;->a:Ljava/util/Map;

    const/16 v1, 0x78

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "fps_120hz.json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/j;->a:Ljava/util/Map;

    const/16 v1, 0x90

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "fps_144hz.json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/j;->b:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/RefreshRate/j;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/j;->b:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public a(I)Lcom/xiaomi/misettings/display/RefreshRate/j$a;
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/display/RefreshRate/j$a;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/j;)V

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->c(I)V

    sget-object v1, Lcom/xiaomi/misettings/display/RefreshRate/j;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->a(Ljava/lang/String;)V

    const/16 v1, 0x3c

    if-eq p1, v1, :cond_2

    const/16 v1, 0x5a

    if-eq p1, v1, :cond_1

    const/16 v1, 0x78

    if-eq p1, v1, :cond_0

    const/16 v1, 0x90

    if-eq p1, v1, :cond_0

    const/4 p1, -0x1

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->c(I)V

    goto :goto_0

    :cond_0
    sget p1, Lcom/xiaomi/misettings/display/l;->fps_nature_title:I

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->b(I)V

    sget p1, Lcom/xiaomi/misettings/display/l;->fps_nature_summary:I

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->a(I)V

    goto :goto_0

    :cond_1
    sget p1, Lcom/xiaomi/misettings/display/l;->fps_smooth_title:I

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->b(I)V

    sget p1, Lcom/xiaomi/misettings/display/l;->fps_smooth_summary:I

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->a(I)V

    goto :goto_0

    :cond_2
    sget p1, Lcom/xiaomi/misettings/display/l;->fps_standard_title:I

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->b(I)V

    sget p1, Lcom/xiaomi/misettings/display/l;->fps_standard_summary:I

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->a(I)V

    :goto_0
    return-object v0
.end method
