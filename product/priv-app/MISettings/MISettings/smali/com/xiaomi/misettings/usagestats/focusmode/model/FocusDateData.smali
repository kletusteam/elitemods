.class public Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;
.super Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;


# instance fields
.field private isFirstData:Z

.field private isToday:Z

.field private strDate:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->setType(I)V

    return-void
.end method


# virtual methods
.method public getStrDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;->strDate:Ljava/lang/String;

    return-object v0
.end method

.method public isFirstData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;->isFirstData:Z

    return v0
.end method

.method public isToday()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;->isToday:Z

    return v0
.end method

.method public setFirstData(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;->isFirstData:Z

    return-void
.end method

.method public setStrDate(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;->strDate:Ljava/lang/String;

    return-void
.end method

.method public setToday(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;->isToday:Z

    return-void
.end method
