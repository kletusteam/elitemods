.class public Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;
.super Landroid/widget/LinearLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;,
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$b;,
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$CustomEditText;,
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;,
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;,
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;,
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$e;,
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$d;,
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$g;,
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$h;,
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;,
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$f;
    }
.end annotation


# static fields
.field private static final PRESSED_STATE_SET:[I

.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field static final b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$d;

.field private static final c:[C


# instance fields
.field private A:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$d;

.field private Aa:Ljava/lang/String;

.field private B:J

.field private final C:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private D:[I

.field private final E:Landroid/graphics/Paint;

.field private F:I

.field private G:I

.field private H:I

.field private final I:Landroid/widget/Scroller;

.field private final J:Landroid/widget/Scroller;

.field private K:I

.field private L:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;

.field private M:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;

.field private N:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$b;

.field private O:F

.field private P:J

.field private Q:F

.field private R:Landroid/view/VelocityTracker;

.field private S:I

.field private T:I

.field private U:I

.field private V:Z

.field private final W:Z

.field private final aa:I

.field private ba:I

.field private ca:Z

.field private d:I

.field private da:Z

.field private e:I

.field private ea:I

.field private final f:I

.field private fa:I

.field private g:I

.field private ga:I

.field private h:I

.field private ha:Z

.field private i:Z

.field private ia:Z

.field private j:I

.field private ja:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;

.field private k:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;

.field private final ka:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;

.field private final l:Landroid/widget/EditText;

.field private la:I

.field private final m:I

.field private ma:Landroid/graphics/Paint;

.field private final n:I

.field private na:I

.field private o:I

.field private oa:F

.field private final p:I

.field private pa:I

.field private q:I

.field private qa:I

.field private final r:Z

.field private ra:I

.field private final s:I

.field private sa:I

.field private t:I

.field private ta:I

.field private u:[Ljava/lang/String;

.field private ua:I

.field private v:I

.field private va:I

.field private w:I

.field private wa:F

.field private x:I

.field private xa:I

.field private y:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$h;

.field private ya:Ljava/lang/CharSequence;

.field private z:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$g;

.field private za:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$f;

    const/4 v2, 0x2

    invoke-direct {v0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$f;-><init>(I)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$d;

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v2, 0x10100a7

    aput v2, v0, v1

    sput-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->PRESSED_STATE_SET:[I

    const/16 v0, 0xa

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->c:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x7f0402e1

    invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x5

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->d:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->d:I

    const/4 v2, 0x2

    div-int/2addr v1, v2

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    sget-object v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->f:I

    const/4 v1, 0x1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->g:I

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->h:I

    const v3, 0x10000006

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->j:I

    const/16 v3, 0x190

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->q:I

    const-wide/16 v3, 0x12c

    iput-wide v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->B:J

    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->C:Landroid/util/SparseArray;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->d:I

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->D:[I

    const/high16 v3, -0x80000000

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    const/4 v3, 0x0

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ba:I

    const/4 v4, -0x1

    iput v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->la:I

    const/16 v5, 0x1e

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->na:I

    const/16 v5, 0x19

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->pa:I

    const/16 v5, 0xe

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->qa:I

    const/16 v5, 0xa

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ra:I

    const v5, -0x49ffd

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->sa:I

    const/high16 v6, 0x7f000000

    iput v6, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ta:I

    const v6, -0xb2b2b3

    iput v6, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ua:I

    const v6, -0x333334

    iput v6, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->va:I

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->xa:I

    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->za:F

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v5, v6, v5

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->qa:I

    int-to-float v5, v5

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->qa:I

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->pa:I

    int-to-float v5, v5

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->pa:I

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ra:I

    int-to-float v5, v5

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ra:I

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->g:I

    int-to-float v5, v5

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->g:I

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->h:I

    int-to-float v5, v5

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->h:I

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v7, Lcom/xiaomi/misettings/g;->NumberPicker:[I

    invoke-virtual {v5, p2, v7, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    const/4 p3, 0x3

    invoke-virtual {p2, p3}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p3

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ya:Ljava/lang/CharSequence;

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->pa:I

    const/16 v5, 0x8

    invoke-virtual {p2, v5, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->pa:I

    const/16 p3, 0x9

    iget v7, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->qa:I

    invoke-virtual {p2, p3, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->qa:I

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ra:I

    const/4 v7, 0x4

    invoke-virtual {p2, v7, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ra:I

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->sa:I

    invoke-virtual {p2, v3, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->sa:I

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ta:I

    invoke-virtual {p2, v1, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ta:I

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->xa:I

    const/4 v8, 0x6

    invoke-virtual {p2, v8, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->xa:I

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->na:I

    invoke-virtual {p2, v0, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->na:I

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->c()V

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->W:Z

    const/high16 p2, 0x40000000    # 2.0f

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p3

    invoke-static {v1, p2, p3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p2

    float-to-int p2, p2

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->aa:I

    const/high16 p2, 0x42340000    # 45.0f

    mul-float/2addr p2, v6

    float-to-int p2, p2

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->m:I

    iput v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->n:I

    const/high16 p2, 0x43680000    # 232.0f

    mul-float/2addr v6, p2

    float-to-int p2, v6

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o:I

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->n:I

    if-eq p2, v4, :cond_2

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o:I

    if-eq p3, v4, :cond_2

    if-gt p2, p3, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "minHeight > maxHeight"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_0
    iput v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->p:I

    iput v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->q:I

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->p:I

    if-eq p2, v4, :cond_4

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->q:I

    if-eq p3, v4, :cond_4

    if-gt p2, p3, :cond_3

    goto :goto_1

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "minWidth > maxWidth"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    :goto_1
    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->q:I

    if-ne p2, v4, :cond_5

    move p2, v1

    goto :goto_2

    :cond_5
    move p2, v3

    :goto_2
    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->r:Z

    new-instance p2, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ka:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;

    iget-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->W:Z

    xor-int/2addr p2, v1

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->setWillNotDraw(Z)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string p3, "layout_inflater"

    invoke-virtual {p2, p3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/LayoutInflater;

    const p3, 0x7f0e0112

    invoke-virtual {p2, p3, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const p2, 0x7f0b02aa

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/EditText;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    new-instance p3, Lcom/xiaomi/misettings/usagestats/focusmode/widget/f;

    invoke-direct {p3, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/f;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)V

    invoke-virtual {p2, p3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    new-array p3, v1, [Landroid/text/InputFilter;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$e;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$e;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)V

    aput-object v0, p3, v3

    invoke-virtual {p2, p3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {p2, v2}, Landroid/widget/EditText;->setRawInputType(I)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {p2, v8}, Landroid/widget/EditText;->setImeOptions(I)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {p2, v7}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    const p3, 0x800003

    invoke-virtual {p2, p3}, Landroid/widget/EditText;->setGravity(I)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Landroid/widget/EditText;->setScaleX(F)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {p2, v3}, Landroid/widget/EditText;->setSaveEnabled(Z)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->na:I

    invoke-virtual {p2}, Landroid/widget/EditText;->getPaddingTop()I

    move-result v0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->na:I

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v6

    invoke-virtual {p2, p3, v0, v2, v6}, Landroid/widget/EditText;->setPadding(IIII)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p2

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->S:I

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result p2

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->T:I

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result p1

    div-int/2addr p1, v5

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->U:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getTextSize()F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->s:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->s:I

    int-to-float p2, p2

    invoke-virtual {p1, v3, p2}, Landroid/widget/EditText;->setTextSize(IF)V

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object p2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->pa:I

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object p2

    sget-object p3, Landroid/widget/LinearLayout;->ENABLED_STATE_SET:[I

    invoke-virtual {p2, p3, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->E:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->ascent()F

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->wa:F

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ma:Landroid/graphics/Paint;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ma:Landroid/graphics/Paint;

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ma:Landroid/graphics/Paint;

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ma:Landroid/graphics/Paint;

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->xa:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ma:Landroid/graphics/Paint;

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ra:I

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ma:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance p1, Landroid/widget/Scroller;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    const/4 p3, 0x0

    invoke-direct {p1, p2, p3, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    new-instance p1, Landroid/widget/Scroller;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    new-instance p3, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v0, 0x40200000    # 2.5f

    invoke-direct {p3, v0}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-direct {p1, p2, p3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->J:Landroid/widget/Scroller;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o()Z

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getImportantForAccessibility()I

    move-result p1

    if-nez p1, :cond_6

    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->setImportantForAccessibility(I)V

    :cond_6
    return-void
.end method

.method private a(FII)F
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    int-to-float v1, v0

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_0

    int-to-float p1, v0

    :cond_0
    int-to-float v0, p2

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    int-to-float v1, v1

    div-float/2addr p1, v1

    sub-int/2addr p2, p3

    int-to-float p2, p2

    mul-float/2addr p1, p2

    sub-float/2addr v0, p1

    return v0
.end method

.method private a(FIZ)I
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    return p2

    :cond_0
    if-eqz p3, :cond_1

    neg-float p1, p1

    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result p3

    int-to-float p3, p3

    mul-float/2addr p1, p3

    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result p3

    int-to-float p3, p3

    add-float/2addr p1, p3

    goto :goto_0

    :cond_1
    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result p3

    int-to-float p3, p3

    mul-float/2addr p1, p3

    :goto_0
    float-to-int p1, p1

    const p3, 0xffffff

    and-int/2addr p2, p3

    shl-int/lit8 p1, p1, 0x18

    or-int/2addr p1, p2

    return p1
.end method

.method private a(II)I
    .locals 4

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    return p1

    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    const/high16 v2, -0x80000000

    const/high16 v3, 0x40000000    # 2.0f

    if-eq v1, v2, :cond_3

    if-eqz v1, :cond_2

    if-ne v1, v3, :cond_1

    return p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown measure mode: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    invoke-static {p2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    return p1

    :cond_3
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {p1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    return p1
.end method

.method private a(III)I
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    const/4 p2, 0x0

    invoke-static {p1, p3, p2}, Landroid/widget/LinearLayout;->resolveSizeAndState(III)I

    move-result p1

    return p1

    :cond_0
    return p2
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;I)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->d(I)I

    move-result p0

    return p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;Ljava/lang/String;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method private a(Ljava/lang/String;)I
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    add-int/2addr p1, v0

    return p1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    return p1

    :catch_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    return p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)Landroid/widget/EditText;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    return-object p0
.end method

.method private a(I)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->C:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    if-lt p1, v1, :cond_3

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    if-le p1, v2, :cond_1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    if-eqz v2, :cond_2

    sub-int v1, p1, v1

    aget-object v1, v2, v1

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->c(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    :goto_0
    const-string v1, ""

    :goto_1
    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method private a(IZ)V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->d(I)I

    move-result p1

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    :goto_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    if-ne v0, p1, :cond_1

    return-void

    :cond_1
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o()Z

    if-eqz p2, :cond_2

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e(I)V

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o()Z

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(Ljava/lang/String;)I

    move-result p1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(IZ)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->b(II)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(Landroid/view/View;)V

    return-void
.end method

.method private a(Z)V
    .locals 13

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->W:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(Landroid/widget/Scroller;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->J:Landroid/widget/Scroller;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(Landroid/widget/Scroller;)Z

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->K:I

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    neg-int v5, p1

    const/16 v6, 0x12c

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget v11, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    const/16 v12, 0x12c

    invoke-virtual/range {v7 .. v12}, Landroid/widget/Scroller;->startScroll(IIIII)V

    :goto_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    if-eqz p1, :cond_3

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    add-int/2addr p1, v0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(IZ)V

    goto :goto_1

    :cond_3
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    sub-int/2addr p1, v0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(IZ)V

    :goto_1
    return-void
.end method

.method private a(ZJ)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->M:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->M:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->M:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;->a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->M:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;

    invoke-virtual {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private a([I)V
    .locals 3

    array-length v0, p1

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    :goto_0
    if-lez v0, :cond_0

    add-int/lit8 v2, v0, -0x1

    aget v2, p1, v2

    aput v2, p1, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    aget v0, p1, v1

    sub-int/2addr v0, v1

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    :cond_1
    const/4 v1, 0x0

    aput v0, p1, v1

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(I)V

    return-void
.end method

.method private a(Landroid/widget/Scroller;)Z
    .locals 6

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/Scroller;->forceFinished(Z)V

    invoke-virtual {p1}, Landroid/widget/Scroller;->getFinalY()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/Scroller;->getCurrY()I

    move-result p1

    sub-int/2addr v1, p1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    add-int/2addr p1, v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    rem-int/2addr p1, v2

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    sub-int/2addr v2, p1

    const/4 p1, 0x0

    if-eqz v2, :cond_2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    div-int/lit8 v5, v4, 0x2

    if-le v3, v5, :cond_1

    if-lez v2, :cond_0

    sub-int/2addr v2, v4

    goto :goto_0

    :cond_0
    add-int/2addr v2, v4

    :cond_1
    :goto_0
    add-int/2addr v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->scrollBy(II)V

    return v0

    :cond_2
    return p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ha:Z

    return p1
.end method

.method static synthetic a()[C
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->c:[C

    return-object v0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->c(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private b(I)V
    .locals 10

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->K:I

    if-lez p1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7fffffff

    move v5, p1

    invoke-virtual/range {v1 .. v9}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const v3, 0x7fffffff

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7fffffff

    move v5, p1

    invoke-virtual/range {v1 .. v9}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    :goto_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    return-void
.end method

.method private b(II)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->L:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->L:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->L:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;->a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;I)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->L:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;

    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;->b(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;I)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->L:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private b(Landroid/widget/Scroller;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    if-ne p1, v0, :cond_1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->b()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o()Z

    :cond_0
    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->f(I)V

    goto :goto_0

    :cond_1
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ba:I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o()Z

    :cond_2
    :goto_0
    return-void
.end method

.method private b([I)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    add-int/lit8 v1, v0, 0x1

    aget v2, p1, v1

    aput v2, p1, v0

    move v0, v1

    goto :goto_0

    :cond_0
    array-length v0, p1

    add-int/lit8 v0, v0, -0x2

    aget v0, p1, v0

    add-int/lit8 v0, v0, 0x1

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    :cond_1
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    aput v0, p1, v1

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(I)V

    return-void
.end method

.method private b()Z
    .locals 7

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->K:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    div-int/lit8 v3, v2, 0x2

    if-le v1, v3, :cond_1

    if-lez v0, :cond_0

    neg-int v2, v2

    :cond_0
    add-int/2addr v0, v2

    :cond_1
    move v5, v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->J:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0x320

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    const/4 v0, 0x1

    return v0

    :cond_2
    return v1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ha:Z

    return p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ia:Z

    return p1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->fa:I

    return p0
.end method

.method private c(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->A:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$d;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$d;->format(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/r;->a(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->k:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;

    if-nez v0, :cond_0

    const-string v0, "NumberPicker_sound_play"

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/a/a;->a(Ljava/lang/String;)Landroid/os/Looper;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->k:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->k:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->f:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;->a(I)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(Z)V

    return-void
.end method

.method private d(I)I
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    if-le p1, v0, :cond_0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    sub-int/2addr p1, v0

    sub-int/2addr v0, v1

    rem-int/2addr p1, v0

    add-int/2addr v1, p1

    add-int/lit8 v1, v1, -0x1

    return v1

    :cond_0
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    if-ge p1, v1, :cond_1

    sub-int p1, v1, p1

    sub-int v1, v0, v1

    rem-int/2addr p1, v1

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_1
    return p1
.end method

.method private d()V
    .locals 4

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->D:[I

    array-length v1, v0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->s:I

    mul-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v1

    int-to-float v1, v2

    const/4 v2, 0x0

    cmpg-float v3, v1, v2

    if-gez v3, :cond_0

    move v1, v2

    :cond_0
    array-length v0, v0

    int-to-float v0, v0

    div-float/2addr v1, v0

    const/high16 v0, 0x3f000000    # 0.5f

    add-float/2addr v1, v0

    float-to-int v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->t:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->s:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->t:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getBaseline()I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTop()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o()Z

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ia:Z

    return p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ca:Z

    return p1
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ea:I

    return p0
.end method

.method private e()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->C:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->D:[I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->getValue()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->D:[I

    array-length v3, v3

    if-ge v2, v3, :cond_1

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    sub-int v3, v2, v3

    add-int/2addr v3, v1

    iget-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    if-eqz v4, :cond_0

    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->d(I)I

    move-result v3

    :cond_0
    aput v3, v0, v2

    aget v3, v0, v2

    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private e(I)V
    .locals 2

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->f()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->i:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->j:I

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->performHapticFeedback(I)Z

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->y:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$h;

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    invoke-interface {v0, p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$h;->a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;II)V

    :cond_1
    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->B:J

    return-wide v0
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->k:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;->a()V

    :cond_0
    return-void
.end method

.method private f(I)V
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ba:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->Aa:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->Aa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->Aa:Ljava/lang/String;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->m()V

    :cond_2
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ba:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->z:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$g;

    if-eqz v0, :cond_3

    invoke-interface {v0, p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$g;->a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;I)V

    :cond_3
    return-void
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->aa:I

    return p0
.end method

.method private g()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->N:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$b;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$b;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->N:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$b;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->N:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$b;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Landroid/widget/LinearLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic h(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    return p0
.end method

.method private h()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    return-void
.end method

.method private i()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->k:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->f:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;->b(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->k:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;

    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    return p0
.end method

.method static synthetic j(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    return p0
.end method

.method private j()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->M:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->L:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$j;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->N:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$b;

    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ka:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;->a()V

    return-void
.end method

.method private k()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->N:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$b;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method static synthetic k(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)[Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    return-object p0
.end method

.method static synthetic l(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    return p0
.end method

.method private l()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->M:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$c;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private m()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->k:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;->b()V

    :cond_0
    return-void
.end method

.method private n()V
    .locals 5

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->r:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->E:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->pa:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    const/4 v2, 0x0

    if-nez v1, :cond_3

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x9

    if-ge v2, v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->E:Landroid/graphics/Paint;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    cmpl-float v3, v1, v0

    if-lez v3, :cond_1

    move v0, v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v0, v1

    int-to-float v0, v0

    goto :goto_2

    :cond_3
    array-length v1, v1

    :goto_1
    if-ge v2, v1, :cond_5

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    aget-object v3, v3, v2

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->E:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    cmpl-float v4, v3, v0

    if-lez v4, :cond_4

    move v0, v3

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    :goto_2
    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->oa:F

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->q:I

    int-to-float v1, v1

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_7

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->p:I

    int-to-float v2, v1

    cmpl-float v2, v0, v2

    if-lez v2, :cond_6

    float-to-int v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->q:I

    goto :goto_3

    :cond_6
    iput v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->q:I

    :cond_7
    :goto_3
    return-void
.end method

.method private o()Z
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    sub-int/2addr v1, v2

    aget-object v0, v0, v1

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ba:I

    if-eqz v1, :cond_1

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->Aa:Ljava/lang/String;

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_1
    const/4 v0, 0x1

    return v0

    :cond_3
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public computeScroll()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->J:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->K:I

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/widget/Scroller;->getStartY()I

    move-result v2

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->K:I

    :cond_1
    const/4 v2, 0x0

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->K:I

    sub-int v3, v1, v3

    invoke-virtual {p0, v2, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->scrollBy(II)V

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->K:I

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->b(Landroid/widget/Scroller;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    :goto_0
    return-void
.end method

.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->W:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ea:I

    if-ge v0, v1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->fa:I

    if-le v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result p1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;

    const/4 v2, 0x7

    const/4 v3, -0x1

    const/16 v4, 0x100

    const/4 v5, 0x0

    const/16 v6, 0x40

    const/16 v7, 0x80

    if-eq p1, v2, :cond_5

    const/16 v2, 0x9

    if-eq p1, v2, :cond_4

    const/16 v2, 0xa

    if-eq p1, v2, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {v1, v0, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;->a(II)V

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ga:I

    goto :goto_1

    :cond_4
    invoke-virtual {v1, v0, v7}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;->a(II)V

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ga:I

    invoke-virtual {v1, v0, v6, v5}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;->performAction(IILandroid/os/Bundle;)Z

    goto :goto_1

    :cond_5
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ga:I

    if-eq p1, v0, :cond_6

    if-eq p1, v3, :cond_6

    invoke-virtual {v1, p1, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;->a(II)V

    invoke-virtual {v1, v0, v7}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;->a(II)V

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ga:I

    invoke-virtual {v1, v0, v6, v5}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;->performAction(IILandroid/os/Bundle;)Z

    :cond_6
    :goto_1
    const/4 p1, 0x0

    return p1
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x13

    const/16 v2, 0x14

    if-eq v0, v1, :cond_1

    if-eq v0, v2, :cond_1

    const/16 v1, 0x17

    if-eq v0, v1, :cond_0

    const/16 v1, 0x42

    if-eq v0, v1, :cond_0

    goto :goto_3

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->j()V

    goto :goto_3

    :cond_1
    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->W:Z

    if-nez v1, :cond_2

    goto :goto_3

    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_4

    if-eq v1, v3, :cond_3

    goto :goto_3

    :cond_3
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->la:I

    if-ne v1, v0, :cond_9

    const/4 p1, -0x1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->la:I

    return v3

    :cond_4
    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    if-nez v1, :cond_6

    if-ne v0, v2, :cond_5

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->getValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->getMinValue()I

    move-result v4

    if-le v1, v4, :cond_9

    goto :goto_1

    :cond_6
    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->getValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->getMaxValue()I

    move-result v4

    if-ge v1, v4, :cond_9

    :goto_1
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->requestFocus()Z

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->la:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->j()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z

    move-result p1

    if-eqz p1, :cond_8

    if-ne v0, v2, :cond_7

    move p1, v3

    goto :goto_2

    :cond_7
    const/4 p1, 0x0

    :goto_2
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(Z)V

    :cond_8
    return v3

    :cond_9
    :goto_3
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 0

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const/4 p1, 0x1

    return p1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->j()V

    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->j()V

    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected drawableStateChanged()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->n()V

    return-void
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->W:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/LinearLayout;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ja:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;

    if-nez v0, :cond_1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ja:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ja:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$a;

    return-object v0
.end method

.method public getDisplayedValues()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    return-object v0
.end method

.method public getMaxValue()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    return v0
.end method

.method public getMinValue()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    return v0
.end method

.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    return v0
.end method

.method public getWrapSelectorWheel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->c()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->i()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->j()V

    const-string v0, "NumberPicker_sound_play"

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/a/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->W:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getRight()I

    move-result v2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/2addr v2, v0

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    int-to-float v0, v2

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    int-to-float v1, v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->d:I

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->D:[I

    array-length v4, v3

    const/4 v5, 0x0

    move v6, v1

    move v1, v5

    :goto_0
    const/high16 v7, 0x40000000    # 2.0f

    if-ge v1, v4, :cond_2

    aget v8, v3, v1

    iget-object v9, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->C:Landroid/util/SparseArray;

    invoke-virtual {v9, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    sub-float v9, v2, v6

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget v10, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    iget v10, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->pa:I

    iget v11, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->qa:I

    invoke-direct {p0, v9, v10, v11}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(FII)F

    move-result v10

    iget-object v11, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->E:Landroid/graphics/Paint;

    invoke-virtual {v11, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v11, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->E:Landroid/graphics/Paint;

    iget v12, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ta:I

    invoke-direct {p0, v9, v12, v5}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(FIZ)I

    move-result v12

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setColor(I)V

    iget v11, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->qa:I

    int-to-float v11, v11

    sub-float v11, v10, v11

    div-float/2addr v11, v7

    add-float/2addr v11, v6

    iget-object v12, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->E:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0, v11, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const/high16 v11, 0x3f800000    # 1.0f

    cmpg-float v11, v9, v11

    if-gez v11, :cond_1

    iget-object v11, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->E:Landroid/graphics/Paint;

    iget v12, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->sa:I

    const/4 v13, 0x1

    invoke-direct {p0, v9, v12, v13}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(FIZ)I

    move-result v9

    invoke-virtual {v11, v9}, Landroid/graphics/Paint;->setColor(I)V

    iget v9, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->qa:I

    int-to-float v9, v9

    sub-float/2addr v10, v9

    div-float/2addr v10, v7

    add-float/2addr v10, v6

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->E:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0, v10, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_1
    iget v7, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ya:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    sget-boolean v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/c;->d:Z

    if-nez v1, :cond_4

    invoke-static {p0}, Ld/h/a/i;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->oa:F

    div-float/2addr v1, v7

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->g:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ma:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ya:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    sub-float/2addr v0, v1

    goto :goto_1

    :cond_3
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->oa:F

    div-float/2addr v1, v7

    add-float/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->g:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    :goto_1
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->pa:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v2, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ra:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v2, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->h:I

    int-to-float v1, v1

    add-float/2addr v2, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ya:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ma:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_4
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const-class v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    mul-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollY(I)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    mul-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollY(I)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->W:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->j()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->O:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->Q:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->P:J

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ca:Z

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->da:Z

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->O:F

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ea:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    const/4 v2, 0x1

    if-gez v0, :cond_2

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ba:I

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ka:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;->a(I)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->fa:I

    int-to-float v0, v0

    cmpl-float p1, p1, v0

    if-lez p1, :cond_3

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ba:I

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ka:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;

    invoke-virtual {p1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;->a(I)V

    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z

    move-result p1

    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->J:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->f(I)V

    goto :goto_1

    :cond_4
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->J:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z

    move-result p1

    if-nez p1, :cond_5

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->I:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->J:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    goto :goto_1

    :cond_5
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->O:F

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ea:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_6

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result p1

    int-to-long v3, p1

    invoke-direct {p0, v1, v3, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(ZJ)V

    goto :goto_1

    :cond_6
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->fa:I

    int-to-float v0, v0

    cmpl-float p1, p1, v0

    if-lez p1, :cond_7

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result p1

    int-to-long v0, p1

    invoke-direct {p0, v2, v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(ZJ)V

    goto :goto_1

    :cond_7
    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->da:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->g()V

    :goto_1
    return v2

    :cond_8
    :goto_2
    return v1
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->W:Z

    if-nez v0, :cond_0

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result p2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result p3

    iget-object p4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {p4}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result p4

    iget-object p5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {p5}, Landroid/widget/EditText;->getMeasuredHeight()I

    move-result p5

    sub-int/2addr p2, p4

    div-int/lit8 p2, p2, 0x2

    sub-int/2addr p3, p5

    div-int/lit8 p3, p3, 0x2

    add-int/2addr p4, p2

    add-int/2addr p5, p3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/widget/EditText;->layout(IIII)V

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->d()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result p1

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->m:I

    sub-int/2addr p1, p2

    div-int/lit8 p1, p1, 0x2

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->aa:I

    sub-int/2addr p1, p3

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ea:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ea:I

    mul-int/lit8 p3, p3, 0x2

    add-int/2addr p1, p3

    add-int/2addr p1, p2

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->fa:I

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->W:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->q:I

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(II)I

    move-result v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o:I

    invoke-direct {p0, p2, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(II)I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/widget/LinearLayout;->onMeasure(II)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->p:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v1

    invoke-direct {p0, v0, v1, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(III)I

    move-result p1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->n:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v1

    invoke-direct {p0, v0, v1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(III)I

    move-result p2

    invoke-virtual {p0, p1, p2}, Landroid/widget/LinearLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ba:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->Aa:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ya:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_d

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->W:Z

    if-nez v0, :cond_0

    goto/16 :goto_4

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->R:Landroid/view/VelocityTracker;

    if-nez v0, :cond_1

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->R:Landroid/view/VelocityTracker;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->R:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v3, :cond_6

    if-eq v0, v2, :cond_2

    goto/16 :goto_3

    :cond_2
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ca:Z

    if-eqz v0, :cond_3

    goto/16 :goto_3

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ba:I

    if-eq v0, v3, :cond_4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->O:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->S:I

    if-le v0, v1, :cond_5

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->j()V

    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->f(I)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->Q:F

    sub-float v0, p1, v0

    float-to-int v0, v0

    invoke-virtual {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->scrollBy(II)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    :cond_5
    :goto_0
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->Q:F

    goto/16 :goto_3

    :cond_6
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->k()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ka:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;->a()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->R:Landroid/view/VelocityTracker;

    const/16 v4, 0x3e8

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->U:I

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->U:I

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-lt v4, v5, :cond_7

    int-to-float v0, v0

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->za:F

    mul-float/2addr v0, v4

    float-to-int v0, v0

    :cond_7
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->T:I

    if-le v4, v5, :cond_8

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->b(I)V

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->f(I)V

    goto :goto_2

    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    int-to-float v4, v0

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->O:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->P:J

    sub-long/2addr v5, v7

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->S:I

    if-gt v4, p1, :cond_b

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result p1

    int-to-long v7, p1

    cmp-long p1, v5, v7

    if-gez p1, :cond_b

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->da:Z

    if-eqz p1, :cond_9

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->da:Z

    goto :goto_1

    :cond_9
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    div-int/2addr v0, p1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    sub-int/2addr v0, p1

    if-lez v0, :cond_a

    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ka:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;

    invoke-virtual {p1, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;->b(I)V

    goto :goto_1

    :cond_a
    if-gez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ka:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;

    invoke-virtual {p1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$i;->b(I)V

    goto :goto_1

    :cond_b
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->b()Z

    :cond_c
    :goto_1
    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->f(I)V

    :goto_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->R:Landroid/view/VelocityTracker;

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->recycle()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->R:Landroid/view/VelocityTracker;

    :goto_3
    return v3

    :cond_d
    :goto_4
    return v1
.end method

.method public scrollBy(II)V
    .locals 3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->D:[I

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    if-nez v0, :cond_0

    if-lez p2, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    aget v0, p1, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    if-gt v0, v1, :cond_0

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    if-nez v0, :cond_1

    if-gez p2, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    aget v0, p1, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    if-lt v0, v1, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    return-void

    :cond_1
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    :cond_2
    :goto_0
    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    sub-int v0, p2, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->t:I

    const/4 v2, 0x1

    if-le v0, v1, :cond_3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    sub-int/2addr p2, v0

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a([I)V

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    aget p2, p1, p2

    invoke-direct {p0, p2, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(IZ)V

    iget-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    if-nez p2, :cond_2

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    aget p2, p1, p2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    if-gt p2, v0, :cond_2

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    goto :goto_0

    :cond_3
    :goto_1
    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    sub-int v0, p2, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->t:I

    neg-int v1, v1

    if-ge v0, v1, :cond_4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->F:I

    add-int/2addr p2, v0

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->b([I)V

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    aget p2, p1, p2

    invoke-direct {p0, p2, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(IZ)V

    iget-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    if-nez p2, :cond_3

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    aget p2, p1, p2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    if-lt p2, v0, :cond_3

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->G:I

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->H:I

    goto :goto_1

    :cond_4
    return-void
.end method

.method public setDisplayedValues([Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->u:[Ljava/lang/String;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    const v0, 0x80001

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setRawInputType(I)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->l:Landroid/widget/EditText;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setRawInputType(I)V

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o()Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->n()V

    return-void
.end method

.method public setFormatter(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$d;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->A:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$d;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->A:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$d;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o()Z

    return-void
.end method

.method public setHapticMesh(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->j:I

    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ya:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ya:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->ya:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    :cond_2
    return-void
.end method

.method public setMaxFlingSpeedFactor(F)V
    .locals 1

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->za:F

    :cond_0
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o:I

    return-void
.end method

.method public setMaxValue(I)V
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-ltz p1, :cond_3

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    if-ge p1, v0, :cond_1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    :cond_1
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    sub-int/2addr p1, v0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->D:[I

    array-length v0, v0

    if-le p1, v0, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o()Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->n()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "maxValue must be >= 0"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setMinValue(I)V
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-ltz p1, :cond_3

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    if-le p1, v0, :cond_1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->x:I

    :cond_1
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    sub-int/2addr p1, v0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->D:[I

    array-length v0, v0

    if-le p1, v0, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->o()Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->n()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "minValue must be >= 0"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setOnLongPressUpdateInterval(J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->B:J

    return-void
.end method

.method public setOnScrollListener(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$g;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->z:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$g;

    return-void
.end method

.method public setOnValueChangedListener(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$h;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->y:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$h;

    return-void
.end method

.method public setSelectorIndicesCount(I)V
    .locals 1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->d:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->d:I

    div-int/lit8 v0, p1, 0x2

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->e:I

    new-array p1, p1, [I

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->D:[I

    return-void
.end method

.method public setValue(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->a(IZ)V

    return-void
.end method

.method public setWrapSelectorWheel(Z)V
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->w:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->v:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->D:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz p1, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    if-eq p1, v0, :cond_2

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->V:Z

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;->h()V

    return-void
.end method
