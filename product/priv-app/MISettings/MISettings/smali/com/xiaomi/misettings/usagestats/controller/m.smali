.class Lcom/xiaomi/misettings/usagestats/controller/m;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/m;->a:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/m;->a:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;

    invoke-virtual {v0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/m;->a:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->f(Landroid/content/Context;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;I)I

    iget p1, p1, Landroid/os/Message;->what:I

    const-string v1, "LR-DeviceUsageMonitorService"

    const/16 v2, 0x6f

    if-eq p1, v2, :cond_2

    const/16 v3, 0xde

    if-eq p1, v3, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "handleMessage(MSG_WHAT_MONITOR_TERMINAL)"

    invoke-static {v1, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Landroid/os/Handler;->removeMessages(I)V

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->i(Landroid/content/Context;)J

    move-result-wide v1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/m;->a:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;Landroid/content/Context;I)V

    goto :goto_0

    :cond_1
    new-instance p1, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string p1, "handleMessage(MSG_WHAT_MONITOR)"

    invoke-static {v1, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/m;->a:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;Landroid/content/Context;)V

    :goto_0
    return-void
.end method
