.class public Lcom/xiaomi/misettings/usagestats/devicelimit/k;
.super Landroid/widget/BaseAdapter;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/a;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->a:Landroid/content/Context;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->c:Z

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->d:Ljava/util/List;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->d:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/c/c;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/c/c;->c()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->c:Z

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->b:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/a;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/a;->a:I

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    if-nez p2, :cond_2

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->getItemViewType(I)I

    move-result p1

    if-eqz p1, :cond_1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_0

    new-instance p1, Lcom/xiaomi/misettings/usagestats/c/d;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->a:Landroid/content/Context;

    invoke-direct {p1, p2}, Lcom/xiaomi/misettings/usagestats/c/d;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->a:Landroid/content/Context;

    invoke-direct {p1, p2}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->a:Landroid/content/Context;

    invoke-direct {p1, p2, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;-><init>(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/devicelimit/k;)V

    :goto_0
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->d:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/c/c;->a()Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/c/c;

    :goto_1
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/c/c;->d()V

    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method
