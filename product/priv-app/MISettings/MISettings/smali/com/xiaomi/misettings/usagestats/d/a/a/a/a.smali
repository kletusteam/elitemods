.class Lcom/xiaomi/misettings/usagestats/d/a/a/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;->a(Lcom/xiaomi/misettings/usagestats/d/d/i;Landroidx/recyclerview/widget/RecyclerView$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/d/d/i;

.field final synthetic b:Landroidx/recyclerview/widget/RecyclerView$a;

.field final synthetic c:Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;Lcom/xiaomi/misettings/usagestats/d/d/i;Landroidx/recyclerview/widget/RecyclerView$a;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/a/a;->c:Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/a/a;->a:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/a/a;->b:Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/a/a;->a:Lcom/xiaomi/misettings/usagestats/d/d/i;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;->b()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/FamilyBean;->getCurrentSelectDeviceIndex()I

    move-result p1

    if-ne p1, p3, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/a/a;->b:Landroidx/recyclerview/widget/RecyclerView$a;

    instance-of p1, p1, Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/a/a;->a:Lcom/xiaomi/misettings/usagestats/d/d/i;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;->b()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/miui/greenguard/entity/FamilyBean;->setCurrentSelectDevice(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/a/a;->c:Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;->a(Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object p1

    new-instance p2, Landroid/content/Intent;

    const-string p3, "misettings.action.switch.DEVICE"

    invoke-direct {p2, p3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, La/m/a/b;->a(Landroid/content/Intent;)Z

    :cond_1
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;)V"
        }
    .end annotation

    return-void
.end method
