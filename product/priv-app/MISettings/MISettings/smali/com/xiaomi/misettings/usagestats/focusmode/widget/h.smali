.class Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->b(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/U;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;Ljava/lang/String;)Ljava/lang/String;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->d(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)V

    :cond_0
    :try_start_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->b(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "FocusModeShare"

    invoke-static {p1, v0, v1, v2}, Landroid/provider/MediaStore$Images$Media;->insertImage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    :goto_0
    new-instance p1, Landroid/content/Intent;

    const-string v0, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/U;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->e(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)V

    return-void
.end method
