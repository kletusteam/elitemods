.class public Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;
.super Lcom/misettings/common/base/BaseActivity;


# instance fields
.field private a:Lmiuix/appcompat/app/j;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 5

    new-instance v0, Lcom/xiaomi/misettings/usagestats/p;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/p;-><init>(Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f13045a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    div-int/lit8 v3, p1, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    rem-int/lit8 p1, p1, 0x3c

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lmiuix/appcompat/app/j$a;

    const v2, 0x7f140007

    invoke-direct {v1, p0, v2}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f13045d

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/j$a;->c(I)Lmiuix/appcompat/app/j$a;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1c

    if-ge v2, v3, :cond_0

    const v2, 0x7f13045b

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/xiaomi/misettings/usagestats/q;

    invoke-direct {v3, p0}, Lcom/xiaomi/misettings/usagestats/q;-><init>(Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;)V

    invoke-virtual {v1, v2, v3}, Lmiuix/appcompat/app/j$a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    :cond_0
    invoke-virtual {v1, p1}, Lmiuix/appcompat/app/j$a;->a(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/j$a;

    const p1, 0x7f13045c

    invoke-virtual {p0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    new-instance v2, Lcom/xiaomi/misettings/usagestats/r;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/r;-><init>(Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;)V

    invoke-virtual {v1, p1, v2}, Lmiuix/appcompat/app/j$a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v1, v4}, Lmiuix/appcompat/app/j$a;->a(Z)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v1, v0}, Lmiuix/appcompat/app/j$a;->a(Landroid/content/DialogInterface$OnDismissListener;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v1}, Lmiuix/appcompat/app/j$a;->a()Lmiuix/appcompat/app/j;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;->a:Lmiuix/appcompat/app/j;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1, v4}, Lmiuix/appcompat/app/j;->setCanceledOnTouchOutside(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1, v4}, Lmiuix/appcompat/app/j;->setCancelable(Z)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->isFinishing()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    :cond_1
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "LR-DeviceTimeoverActivity"

    if-eqz p1, :cond_0

    const-string v1, "finish"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "====FinishDirectly===="

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void

    :cond_1
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->f(Landroid/content/Context;)I

    move-result p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Display DeviceTimeoverActivity\uff0c limitedTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;->a(I)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStop()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;->a:Lmiuix/appcompat/app/j;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j;->dismiss()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->m(Landroid/content/Context;)V

    :cond_0
    return-void
.end method
