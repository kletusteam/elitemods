.class public Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;
.super Landroid/widget/RelativeLayout;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:I

.field private b:Lcom/xiaomi/misettings/display/RefreshRate/A;

.field private c:Lcom/airbnb/lottie/LottieAnimationView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/FrameLayout;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/content/Context;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->a()V

    return-void
.end method

.method private a()V
    .locals 3

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->h:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->h:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/xiaomi/misettings/display/k;->fps_view_layout:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    sget v0, Lcom/xiaomi/misettings/display/j;->lottie_view:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->c:Lcom/airbnb/lottie/LottieAnimationView;

    sget v0, Lcom/xiaomi/misettings/display/j;->selector_view:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->f:Landroid/widget/FrameLayout;

    sget v0, Lcom/xiaomi/misettings/display/j;->fps_title:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->d:Landroid/widget/TextView;

    sget v0, Lcom/xiaomi/misettings/display/j;->fps_value:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->e:Landroid/widget/TextView;

    sget v0, Lcom/xiaomi/misettings/display/j;->fps_view_summary:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->g:Landroid/widget/TextView;

    sget v0, Lcom/xiaomi/misettings/display/j;->selected_bg:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->i:Landroid/view/View;

    sget v0, Lcom/xiaomi/misettings/display/j;->selected_fps_view:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->j:Landroid/view/View;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->j:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->a:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->b:Lcom/xiaomi/misettings/display/RefreshRate/A;

    if-eqz p1, :cond_1

    invoke-interface {p1, p0}, Lcom/xiaomi/misettings/display/RefreshRate/A;->a(Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;)V

    new-instance p1, Lmiui/util/MiSettingsOT;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->h:Landroid/content/Context;

    invoke-direct {p1, v0}, Lmiui/util/MiSettingsOT;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fps_rate"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "click_fps_rate"

    invoke-virtual {p1, v1, v0}, Lmiui/util/MiSettingsOT;->tk(Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    return-void
.end method

.method public setAnimViewDescription(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->j:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setFpsData(Lcom/xiaomi/misettings/display/RefreshRate/j$a;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->setTitle(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->setValue(I)V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->setSummary(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->e:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->setAnimViewDescription(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->setupAnim(Ljava/lang/String;)V

    return-void
.end method

.method public setOnSelectedChangedListener(Lcom/xiaomi/misettings/display/RefreshRate/A;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->b:Lcom/xiaomi/misettings/display/RefreshRate/A;

    return-void
.end method

.method public setSelected(Z)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->c:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->i:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->j:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setSelected(Z)V

    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setValue(I)V
    .locals 5

    iput p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->a:I

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->h:Landroid/content/Context;

    sget v2, Lcom/xiaomi/misettings/display/l;->screen_fps_unit:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setupAnim(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->c:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0, p1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->c:Lcom/airbnb/lottie/LottieAnimationView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->b(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->c:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->d()V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->c:Lcom/airbnb/lottie/LottieAnimationView;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->f:Landroid/widget/FrameLayout;

    invoke-virtual {p1, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array p1, v0, [Landroid/view/View;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->f:Landroid/widget/FrameLayout;

    aput-object v0, p1, v1

    invoke-static {p1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->f:Landroid/widget/FrameLayout;

    new-array v1, v1, [Lmiuix/animation/a/a;

    invoke-interface {p1, v0, v1}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    return-void
.end method
