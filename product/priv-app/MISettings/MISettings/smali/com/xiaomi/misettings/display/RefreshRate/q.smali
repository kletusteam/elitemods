.class Lcom/xiaomi/misettings/display/RefreshRate/q;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/q;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    const-string p1, "HighRefreshOptionsActivity"

    const-string v0, " the back tab is clicked "

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/q;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->a(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/q;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->a(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object v1, v1, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/q;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->b(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "miui.intent.action.HIGH_REFRESH_STATISTICS"

    invoke-static {v0, v1, p1}, Lcom/xiaomi/misettings/display/RefreshRate/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/q;->a:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;

    invoke-virtual {p1}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void
.end method
