.class public Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;
.super Landroidx/appcompat/widget/AppCompatImageView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;
    }
.end annotation


# instance fields
.field private c:F

.field private d:F

.field private e:F

.field private f:Landroid/graphics/Movie;

.field private g:J

.field private h:J

.field private i:J

.field j:F

.field private k:I

.field private volatile l:Z

.field private volatile m:Z

.field private volatile n:Z

.field private o:Z

.field private p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

.field private q:I

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->c:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->d:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->e:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->k:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->l:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->o:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->r:Z

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    const/4 p1, 0x1

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, Landroid/widget/ImageView;->setLayerType(ILandroid/graphics/Paint;)V

    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->e:F

    const/high16 v1, 0x3f800000    # 1.0f

    div-float v2, v1, v0

    div-float/2addr v1, v0

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, v1}, Landroid/graphics/Movie;->draw(Landroid/graphics/Canvas;FF)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private f()V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->o:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/ImageView;->postInvalidateOnAnimation()V

    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->l:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->g:J

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->m:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->n:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->h:J

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->i:J

    return-void
.end method

.method private getCurrentFrameTime()I
    .locals 8

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->q:I

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->i:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->g:J

    sub-long v4, v2, v4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->q:I

    int-to-long v6, v0

    div-long/2addr v4, v6

    long-to-int v0, v4

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->k:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    if-lt v0, v4, :cond_3

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->n:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;->c()V

    :cond_1
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->r:Z

    if-eqz v0, :cond_2

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->q:I

    :cond_2
    return v1

    :cond_3
    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->g:J

    sub-long/2addr v2, v0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->q:I

    int-to-long v4, v0

    rem-long/2addr v2, v4

    long-to-float v1, v2

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->j:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->n:Z

    if-eqz v0, :cond_5

    new-instance v0, Ljava/math/BigDecimal;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->j:F

    float-to-double v2, v2

    invoke-direct {v0, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V

    const/4 v2, 0x2

    const/4 v3, 0x4

    invoke-virtual {v0, v2, v3}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v2

    const-wide v4, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v0, v2, v4

    if-nez v0, :cond_4

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    :cond_4
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

    double-to-float v2, v2

    invoke-interface {v0, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;->a(F)V

    :cond_5
    float-to-int v0, v1

    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->k:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->g()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;->b()V

    :cond_0
    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->n:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->n:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->m:Z

    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->h:J

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;->a(Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;->a(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public e()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->n:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->m:Z

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->m:Z

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->i:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->h:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->i:J

    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;->a()V

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->a(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method public getDuration()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Movie;->duration()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->m:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->n:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->q:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->getCurrentFrameTime()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Movie;->setTime(I)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->getCurrentFrameTime()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Movie;->setTime(I)Z

    :goto_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->a(Landroid/graphics/Canvas;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f()V

    goto :goto_1

    :cond_1
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->a(Landroid/graphics/Canvas;)V

    :cond_2
    :goto_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/graphics/Movie;->width()I

    move-result p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    invoke-virtual {p2}, Landroid/graphics/Movie;->height()I

    move-result p2

    const/high16 v4, 0x40000000    # 2.0f

    if-ne v0, v4, :cond_0

    int-to-float v5, p1

    int-to-float v6, v2

    div-float/2addr v5, v6

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->c:F

    :cond_0
    if-ne v1, v4, :cond_1

    int-to-float v5, p2

    int-to-float v6, v3

    div-float/2addr v5, v6

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->d:F

    :cond_1
    iget v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->c:F

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->d:F

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->e:F

    if-ne v0, v4, :cond_2

    move p1, v2

    :cond_2
    if-ne v1, v4, :cond_3

    move p2, v3

    :cond_3
    invoke-virtual {p0, p1, p2}, Landroid/widget/ImageView;->setMeasuredDimension(II)V

    goto :goto_0

    :cond_4
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    :goto_0
    return-void
.end method

.method public onScreenStateChanged(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onScreenStateChanged(I)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->o:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f()V

    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onVisibilityChanged(Landroid/view/View;I)V

    if-nez p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->o:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f()V

    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onWindowVisibilityChanged(I)V

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->o:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f()V

    return-void
.end method

.method public setGifResource(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->setGifResource(ILcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;)V

    return-void
.end method

.method public setGifResource(ILcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;)V
    .locals 1

    if-eqz p2, :cond_0

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->g()V

    invoke-virtual {p0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object p2

    invoke-static {p2}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object p2

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    if-nez p2, :cond_1

    :try_start_0
    invoke-virtual {p0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-static {p2, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    const-string p2, "GifImageView"

    const-string v0, "setGifResource: "

    invoke-static {p2, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    if-nez p1, :cond_2

    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Movie;->duration()I

    move-result p1

    if-nez p1, :cond_3

    const/16 p1, 0x3e8

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    invoke-virtual {p1}, Landroid/graphics/Movie;->duration()I

    move-result p1

    :goto_0
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->q:I

    invoke-virtual {p0}, Landroid/widget/ImageView;->requestLayout()V

    return-void
.end method

.method public setGifResource(Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;)V
    .locals 1

    invoke-static {p1}, Landroid/graphics/Movie;->decodeFile(Ljava/lang/String;)Landroid/graphics/Movie;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->g()V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    if-nez p2, :cond_0

    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    invoke-virtual {p1}, Landroid/graphics/Movie;->duration()I

    move-result p1

    if-nez p1, :cond_1

    const/16 p1, 0x3e8

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    invoke-virtual {p1}, Landroid/graphics/Movie;->duration()I

    move-result p1

    :goto_0
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->q:I

    invoke-virtual {p0}, Landroid/widget/ImageView;->requestLayout()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;->b()V

    :cond_2
    return-void
.end method

.method public setPercent(F)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f:Landroid/graphics/Movie;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->q:I

    if-lez v1, :cond_0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->j:F

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Movie;->setTime(I)Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->f()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->p:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView$a;->a(F)V

    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->o:Z

    invoke-super {p0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method
