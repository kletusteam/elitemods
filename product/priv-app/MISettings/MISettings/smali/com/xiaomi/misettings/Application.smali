.class public Lcom/xiaomi/misettings/Application;
.super Lmiuix/autodensity/MiuixApplication;

# interfaces
.implements Landroidx/work/c$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/Application$a;
    }
.end annotation


# static fields
.field private static a:Landroid/content/Context;

.field private static b:Z

.field private static c:Lcom/xiaomi/misettings/Application$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiuix/autodensity/MiuixApplication;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/Application;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    sput-object p0, Lcom/xiaomi/misettings/Application;->a:Landroid/content/Context;

    :cond_0
    return-void
.end method

.method public static a(Lcom/xiaomi/misettings/Application$a;)V
    .locals 1

    sput-object p0, Lcom/xiaomi/misettings/Application;->c:Lcom/xiaomi/misettings/Application$a;

    sget-boolean v0, Lcom/xiaomi/misettings/Application;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/Application;->c:Lcom/xiaomi/misettings/Application$a;

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/xiaomi/misettings/Application$a;->call()V

    :cond_0
    return-void
.end method

.method public static c()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/Application;->a:Landroid/content/Context;

    return-object v0
.end method

.method private d()V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/d;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/d;-><init>(Lcom/xiaomi/misettings/Application;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public b()Landroidx/work/c;
    .locals 2

    new-instance v0, Landroidx/work/c$a;

    invoke-direct {v0}, Landroidx/work/c$a;-><init>()V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroidx/work/c$a;->a(I)Landroidx/work/c$a;

    invoke-virtual {v0}, Landroidx/work/c$a;->a()Landroidx/work/c;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Lmiuix/autodensity/MiuixApplication;->onCreate()V

    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/misettings/Application;->a:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/greenguard/a;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/e/a/a;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/c/b/f;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/e/b/m;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/misettings/common/utils/c;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/Application;->d()V

    :cond_0
    const-string v1, "MiSettingsApplication"

    const-string v2, "registerActivityLifecycleCallbacks"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/c/e;->a(Landroid/content/Context;)V

    if-eqz v0, :cond_4

    new-instance v0, Lcom/xiaomi/misettings/tools/h;

    invoke-direct {v0}, Lcom/xiaomi/misettings/tools/h;-><init>()V

    invoke-virtual {p0, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/manager/a/g;->l()V

    invoke-static {p0}, Lcom/miui/greenguard/manager/l;->a(Landroid/content/Context;)V

    :try_start_0
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/android/settings/coolsound/helper/RingtoneChangeHelper;->init(Lcom/xiaomi/misettings/Application;)V

    :cond_1
    invoke-static {p0}, Lcom/misettings/common/utils/p;->c(Landroid/content/Context;)Z

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isOutMemoryOptimized:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->J(Landroid/content/Context;)V

    :cond_2
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_3

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v2, Lcom/xiaomi/misettings/b;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/b;-><init>(Lcom/xiaomi/misettings/Application;)V

    invoke-virtual {v0, v2}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MainProcess init error"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    :goto_0
    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->c(Landroid/content/Context;)V

    new-instance v0, Lcom/xiaomi/misettings/c;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/c;-><init>(Lcom/xiaomi/misettings/Application;)V

    invoke-virtual {p0, v0}, Landroid/app/Application;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    :cond_4
    const/4 v0, 0x1

    sput-boolean v0, Lcom/xiaomi/misettings/Application;->b:Z

    sget-object v0, Lcom/xiaomi/misettings/Application;->c:Lcom/xiaomi/misettings/Application$a;

    if-eqz v0, :cond_5

    invoke-interface {v0}, Lcom/xiaomi/misettings/Application$a;->call()V

    :cond_5
    return-void
.end method
