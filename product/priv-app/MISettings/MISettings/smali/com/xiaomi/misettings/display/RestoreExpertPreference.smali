.class public Lcom/xiaomi/misettings/display/RestoreExpertPreference;
.super Lcom/xiaomi/misettings/widget/ButtonPreference;

# interfaces
.implements Landroidx/preference/Preference$c;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/widget/ButtonPreference;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RestoreExpertPreference;->d()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/widget/ButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RestoreExpertPreference;->d()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/misettings/widget/ButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RestoreExpertPreference;->d()V

    return-void
.end method

.method private d()V
    .locals 1

    sget v0, Lcom/xiaomi/misettings/display/k;->preference_text_restore:I

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setLayoutResource(I)V

    invoke-virtual {p0, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RestoreExpertPreference;->b:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RestoreExpertPreference;->b:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v1, Lcom/xiaomi/misettings/display/q;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/display/q;-><init>(Lcom/xiaomi/misettings/display/RestoreExpertPreference;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onBindViewHolder(Landroidx/preference/B;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/widget/ButtonPreference;->onBindViewHolder(Landroidx/preference/B;)V

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/widget/ButtonPreference;->a(Landroid/view/View;)V

    sget v0, Lcom/xiaomi/misettings/display/j;->preference_tv:I

    invoke-virtual {p1, v0}, Landroidx/preference/B;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RestoreExpertPreference;->a:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RestoreExpertPreference;->a:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    sget v0, Lcom/xiaomi/misettings/display/l;->restore_default:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/display/RestoreExpertPreference;->b()V

    return-void
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x0

    const/16 v1, 0x9

    invoke-static {p1, v1, v0}, Lcom/xiaomi/misettings/display/a/c;->a(Landroid/content/Context;II)V

    invoke-static {}, Lcom/xiaomi/misettings/display/a/a;->a()Lcom/xiaomi/misettings/display/a/a;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, v0}, Lcom/xiaomi/misettings/display/a/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/display/a/a;Z)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/display/RestoreExpertPreference;->b()V

    invoke-static {}, Lcom/xiaomi/misettings/display/a/b;->a()V

    new-instance p1, Landroid/content/Intent;

    const-string v0, "com.xiaomi.action.REFRESH_EXPERT"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string p1, "RestoreExpertPreference"

    const-string v0, "restore Expert Data"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x1

    return p1
.end method
