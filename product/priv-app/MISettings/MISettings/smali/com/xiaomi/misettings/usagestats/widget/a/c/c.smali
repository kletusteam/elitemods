.class public Lcom/xiaomi/misettings/usagestats/widget/a/c/c;
.super Ljava/lang/Object;


# static fields
.field public static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/xiaomi/misettings/usagestats/widget/a/b/c;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Class;",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->b:Ljava/util/HashMap;

    return-void
.end method

.method public static a(I)Lcom/xiaomi/misettings/usagestats/widget/a/b/c;
    .locals 1

    const v0, 0x7f0e003f

    if-ne p0, v0, :cond_0

    new-instance p0, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;-><init>()V

    return-object p0

    :cond_0
    new-instance p0, Lcom/xiaomi/misettings/usagestats/widget/a/b/a;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/a;-><init>()V

    return-object p0
.end method

.method protected static a(Landroid/content/Context;[I)Lcom/xiaomi/misettings/usagestats/widget/a/b/c;
    .locals 3

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object p0

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget v2, p1, v1

    invoke-virtual {p0, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    iget p0, v2, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, -0x1

    :goto_1
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->a:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->a:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->a(I)Lcom/xiaomi/misettings/usagestats/widget/a/b/c;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->a:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/xiaomi/misettings/usagestats/widget/a/b/c;

    return-object p0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    sget-object v2, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashSet;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->a(Ljava/util/HashSet;)[I

    move-result-object v1

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->b(Landroid/content/Context;[I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/content/Context;[ILcom/xiaomi/misettings/usagestats/widget/a/b/c;Landroid/appwidget/AppWidgetManager;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->b(Landroid/content/Context;[ILcom/xiaomi/misettings/usagestats/widget/a/b/c;Landroid/appwidget/AppWidgetManager;)V

    return-void
.end method

.method public static a(Ljava/lang/Class;)V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/Class;[I)V
    .locals 4

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->b:Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget v2, p1, v1

    sget-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->b:Ljava/util/HashMap;

    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashSet;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a(Ljava/util/HashSet;)[I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;)[I"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    new-array p0, v0, [I

    return-object p0

    :cond_0
    invoke-virtual {p0}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v1, v1, [I

    invoke-virtual {p0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v1, v0

    move v0, v3

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static b(Landroid/content/Context;[I)V
    .locals 4

    if-eqz p1, :cond_3

    array-length v0, p1

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->a(Landroid/content/Context;[I)Lcom/xiaomi/misettings/usagestats/widget/a/b/c;

    move-result-object v0

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    invoke-static {}, Lcom/xiaomi/misettings/f;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, p0, v1, p1}, Lcom/xiaomi/misettings/usagestats/widget/a/b/c;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    return-void

    :cond_1
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0, p0, v1, p1}, Lcom/xiaomi/misettings/usagestats/widget/a/b/c;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    return-void

    :cond_2
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a(Landroid/content/Context;)V

    const-string v2, "UpdateWidgetHelper"

    const-string v3, "renderStart"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->b(Landroid/content/Context;[ILcom/xiaomi/misettings/usagestats/widget/a/b/c;Landroid/appwidget/AppWidgetManager;)V

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v2

    new-instance v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/b;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/a/c/b;-><init>(Landroid/content/Context;[ILcom/xiaomi/misettings/usagestats/widget/a/b/c;Landroid/appwidget/AppWidgetManager;)V

    invoke-virtual {v2, v3}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private static b(Landroid/content/Context;[ILcom/xiaomi/misettings/usagestats/widget/a/b/c;Landroid/appwidget/AppWidgetManager;)V
    .locals 1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2, p0, p3, p1}, Lcom/xiaomi/misettings/usagestats/widget/a/b/c;->c(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    :cond_0
    return-void
.end method

.method protected static b(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "default_category"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/desktop/sp/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method protected static c(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "key_has_accredit"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/desktop/sp/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method public static d(Landroid/content/Context;)V
    .locals 4

    const-class v0, Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/SimpleUsageStatsWidget;

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/SimpleUsageStatsWidget;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->a(Ljava/lang/Class;[I)V

    const-class v0, Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/NormalUsageStatsWidget;

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/NormalUsageStatsWidget;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object p0

    invoke-static {v0, p0}, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->a(Ljava/lang/Class;[I)V

    return-void
.end method
