.class public Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;
.super Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;


# instance fields
.field private o:Lcom/xiaomi/misettings/usagestats/adapter/d;

.field private p:Landroid/os/Handler;

.field public q:Lcom/xiaomi/misettings/usagestats/f/g;

.field public r:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;>;"
        }
    .end annotation
.end field

.field protected s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field private t:Z

.field private u:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->p:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->r:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->s:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->t:Z

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->o()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->n()V

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->p()V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)Lcom/xiaomi/misettings/usagestats/adapter/d;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->o:Lcom/xiaomi/misettings/usagestats/adapter/d;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->p:Landroid/os/Handler;

    return-object p0
.end method

.method private n()V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->q:Lcom/xiaomi/misettings/usagestats/f/g;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->q:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/d;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gez v3, :cond_2

    goto :goto_0

    :cond_2
    new-instance v3, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-direct {v3}, Lcom/xiaomi/misettings/usagestats/f/e;-><init>()V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/xiaomi/misettings/usagestats/f/e;->b(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v1

    invoke-virtual {v3, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/e;->a(J)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->i:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    :cond_4
    :goto_1
    return-void
.end method

.method private o()V
    .locals 19

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v2, v1, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->r:Ljava/util/HashMap;

    sget-object v3, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->m:Ljava/util/List;

    invoke-static {v2, v3, v0}, Lcom/xiaomi/misettings/usagestats/utils/ca;->a(Ljava/util/HashMap;Ljava/util/List;Ljava/util/Map;)V

    sget-object v2, Lcom/xiaomi/misettings/usagestats/d/a;->a:Ljava/util/HashMap;

    iput-object v2, v1, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->r:Ljava/util/HashMap;

    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v3

    if-lez v3, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v3}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->s:Ljava/util/List;

    iget-object v0, v1, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->s:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, v1, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->s:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v13

    iget-object v15, v1, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->s:Ljava/util/List;

    monitor-enter v15

    :try_start_0
    iget-object v0, v1, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/e;

    if-eqz v2, :cond_0

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v12, v1, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->k:Ljava/util/List;

    new-instance v11, Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v8

    iget-object v6, v1, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->e:Ljava/util/List;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v16

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v17

    move-object v3, v11

    move-wide v6, v13

    move-object/from16 v18, v0

    move-object v0, v11

    move/from16 v11, v16

    move-object v1, v12

    move-object/from16 v12, v17

    invoke-direct/range {v3 .. v12}, Lcom/xiaomi/misettings/usagestats/c/b$a;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;JJLjava/lang/CharSequence;ZLjava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    move-object/from16 v18, v0

    :goto_1
    move-object/from16 v1, p0

    move-object/from16 v0, v18

    goto :goto_0

    :cond_1
    monitor-exit v15

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :goto_2
    return-void
.end method

.method private p()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->j:Ljava/util/List;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/f/a;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->i:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/xiaomi/misettings/usagestats/f/a;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e001c

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public k()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public m()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/ui/r;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/ui/r;-><init>(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->onCreate(Landroid/os/Bundle;)V

    sget-object p1, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->m:Lcom/xiaomi/misettings/usagestats/f/g;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->q:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->e:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->p:Landroid/os/Handler;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->u:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->u:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;)V

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/D;->a()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->f:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->o:Lcom/xiaomi/misettings/usagestats/adapter/d;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->f:Lmiuix/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->r:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->r:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->s:Ljava/util/List;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->onResume()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->o:Lcom/xiaomi/misettings/usagestats/adapter/d;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/adapter/d;->a()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->t:Z

    :goto_0
    return-void
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onStart()V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/ui/o;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/ui/o;-><init>(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->u:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "misettings.action.FORCE_NOTIFY_DATA"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-static {v1}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->u:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/xiaomi/misettings/usagestats/adapter/d;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/xiaomi/misettings/usagestats/adapter/d;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->o:Lcom/xiaomi/misettings/usagestats/adapter/d;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->o:Lcom/xiaomi/misettings/usagestats/adapter/d;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/adapter/d;->setHasStableIds(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->m()V

    return-void
.end method
