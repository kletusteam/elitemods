.class public Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;
.super Lcom/misettings/common/base/BaseActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:Z


# instance fields
.field private c:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    const-string v1, "defaultFps"

    invoke-static {v1, v0}, Lb/c/a/b/a/b;->a(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->a:I

    const-string v1, "dc_backlight_fps_incompatible"

    invoke-static {v1, v0}, Lb/c/a/b/a/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->c:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic a()Z
    .locals 1

    sget-boolean v0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->b:Z

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/display/b;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AntiFlickerActivity"

    const-string v1, "The current device does not support anti-flicker mode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/xiaomi/misettings/display/b;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lcom/xiaomi/misettings/display/l;->low_flicker_mode:I

    goto :goto_0

    :cond_1
    sget v1, Lcom/xiaomi/misettings/display/l;->anti_flicker_mode:I

    :goto_0
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->d(I)V

    :cond_2
    sget v0, Lcom/xiaomi/misettings/display/k;->anti_flicker:I

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    if-nez p1, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object p1

    sget v0, Lcom/xiaomi/misettings/display/j;->prefs_contain:I

    new-instance v1, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;

    invoke-direct {v1}, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;-><init>()V

    invoke-virtual {p1, v0, v1}, Landroidx/fragment/app/qa;->a(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {p1}, Landroidx/fragment/app/qa;->a()I

    :cond_3
    sget p1, Lcom/xiaomi/misettings/display/j;->low_bright_preview:I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->c:Landroid/widget/ImageView;

    invoke-static {p0}, Lcom/xiaomi/misettings/display/b;->d(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->c:Landroid/widget/ImageView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    sget p1, Lcom/xiaomi/misettings/display/j;->low_bright_text:I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    return-void
.end method
