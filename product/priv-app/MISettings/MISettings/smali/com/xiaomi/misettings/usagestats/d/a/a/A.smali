.class Lcom/xiaomi/misettings/usagestats/d/a/a/A;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/d/a/a/B;->a(Ljava/util/List;ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/f/o;

.field final synthetic b:I

.field final synthetic c:Lcom/xiaomi/misettings/usagestats/d/a/a/B;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/d/a/a/B;Lcom/xiaomi/misettings/usagestats/f/o;I)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/A;->c:Lcom/xiaomi/misettings/usagestats/d/a/a/B;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/A;->a:Lcom/xiaomi/misettings/usagestats/f/o;

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/A;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/A;->c:Lcom/xiaomi/misettings/usagestats/d/a/a/B;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->a(Lcom/xiaomi/misettings/usagestats/d/a/a/B;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/A;->c:Lcom/xiaomi/misettings/usagestats/d/a/a/B;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->b(Lcom/xiaomi/misettings/usagestats/d/a/a/B;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/d/i;->a()Lcom/miui/greenguard/entity/DashBordBean;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean;->getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/FamilyBean;->getUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean;->getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/FamilyBean;->getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/A;->a:Lcom/xiaomi/misettings/usagestats/f/o;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/o;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean;->getDateType()I

    move-result v5

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean;->getDate()I

    move-result v6

    invoke-static/range {v1 .. v6}, Lcom/miui/greenguard/manager/ExtraRouteManager;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    return-void

    :cond_0
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/A;->a:Lcom/xiaomi/misettings/usagestats/f/o;

    const-string v1, "key_category_data"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const/4 v0, 0x1

    const-string v1, "key_is_week"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/A;->c:Lcom/xiaomi/misettings/usagestats/d/a/a/B;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->c(Lcom/xiaomi/misettings/usagestats/d/a/a/B;)Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    move-result-object v0

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->c:Ljava/util/List;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/A;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    const-string v1, "weekInfo"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/A;->c:Lcom/xiaomi/misettings/usagestats/d/a/a/B;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/A;->a:Lcom/xiaomi/misettings/usagestats/f/o;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/o;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method
