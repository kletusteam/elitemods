.class public Lcom/xiaomi/misettings/usagestats/d/a/a/E;
.super Lcom/xiaomi/misettings/usagestats/d/a/a/s;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/d/a/a$a;


# instance fields
.field private c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/LinearLayout;

.field private h:Landroid/view/View;

.field private i:Ljava/text/SimpleDateFormat;

.field private j:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;-><init>(Landroid/content/Context;Landroid/view/View;)V

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->j:Landroid/graphics/Rect;

    const p1, 0x7f0b039c

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->e:Landroid/widget/TextView;

    const p1, 0x7f0b039b

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->f:Landroid/widget/TextView;

    const p1, 0x7f0b039a

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->d:Landroid/widget/TextView;

    const p1, 0x7f0b0174

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const p1, 0x7f0b0187

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->g:Landroid/widget/LinearLayout;

    const p1, 0x7f0b01d7

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->h:Landroid/view/View;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const/16 p2, 0x9

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setBarType(I)V

    invoke-static {}, Ljava/text/DateFormat;->getTimeInstance()Ljava/text/DateFormat;

    move-result-object p1

    check-cast p1, Ljava/text/SimpleDateFormat;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->i:Ljava/text/SimpleDateFormat;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->i:Ljava/text/SimpleDateFormat;

    const-string p2, "HH:mm"

    invoke-virtual {p1, p2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/xiaomi/misettings/usagestats/d/a/a/E;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->b()V

    return-void
.end method

.method private a(Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;I)V"
        }
    .end annotation

    if-nez p2, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->f:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->f:Landroid/widget/TextView;

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->b(Ljava/util/List;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->g:Landroid/widget/LinearLayout;

    new-instance p2, Lcom/xiaomi/misettings/usagestats/d/a/a/f;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/f;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/E;)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method private b(Ljava/util/List;I)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result v1

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/i;->g()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/i;->f()I

    move-result p1

    goto :goto_0

    :cond_0
    sub-int/2addr p2, v3

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result p1

    :goto_0
    if-nez p1, :cond_1

    const-string p1, ""

    return-object p1

    :cond_1
    if-ne v1, p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const p2, 0x7f1303a8

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    sub-int p2, v1, p1

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    int-to-float v0, p2

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v0, v2

    int-to-float v2, p1

    div-float/2addr v0, v2

    const/high16 v2, 0x42c80000    # 100.0f

    const/4 v4, 0x0

    if-le v1, p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v1, 0x7f130398

    new-array v5, v3, [Ljava/lang/Object;

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v4

    invoke-virtual {p1, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v1, 0x7f130399

    new-array v5, v3, [Ljava/lang/Object;

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v4

    invoke-virtual {p1, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_1
    const-string v0, "\\d+.\\d+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11002e

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private b()V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->f:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->f:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getTextSize()F

    move-result v4

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/CharSequence;FI)I

    move-result v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->e:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->e:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getTextSize()F

    move-result v6

    invoke-static {v3, v4, v6, v5}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/CharSequence;FI)I

    move-result v3

    invoke-virtual {v1}, Landroid/widget/LinearLayout$LayoutParams;->getMarginStart()I

    move-result v4

    add-int/2addr v4, v2

    add-int/2addr v4, v3

    if-ge v0, v4, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v2, 0x412e6666    # 10.9f

    invoke-static {v0, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->h:Landroid/view/View;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->j:Landroid/graphics/Rect;

    invoke-virtual {p1, p2}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a()V

    :cond_0
    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V
    .locals 9

    check-cast p2, Lcom/xiaomi/misettings/usagestats/d/d/g;

    iget-object p2, p2, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast p2, Ljava/util/List;

    invoke-interface {p2, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/xiaomi/misettings/usagestats/f/i;

    const/4 v0, 0x2

    const v1, 0x7f11001f

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result v5

    new-array v6, v0, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/f/i;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v7

    iget-wide v7, v7, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v7, v8}, Lcom/xiaomi/misettings/usagestats/d/f/a;->a(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v1, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v3

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/f/i;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v6

    iget-wide v6, v6, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v6, v7}, Lcom/xiaomi/misettings/usagestats/d/f/a;->a(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v2

    invoke-virtual {v4, v1, v5, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/f/i;->e()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setDayUnlockList(Ljava/util/List;)V

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/f/i;->b()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v4, 0x7f1303a3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->i:Ljava/text/SimpleDateFormat;

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/f/i;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {v5, p3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    aput-object p3, v2, v3

    invoke-virtual {v1, v4, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-direct {p0, p2, p4}, Lcom/xiaomi/misettings/usagestats/d/a/a/E;->a(Ljava/util/List;I)V

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Lcom/xiaomi/misettings/usagestats/d/a/a$a;)V

    return-void
.end method
