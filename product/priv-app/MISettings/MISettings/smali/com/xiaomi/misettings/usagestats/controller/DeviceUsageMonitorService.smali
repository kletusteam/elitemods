.class public Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;
.super Landroid/app/Service;


# static fields
.field private static a:Landroid/app/PendingIntent;


# instance fields
.field private b:Landroid/app/NotificationManager;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private final h:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/controller/m;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/controller/m;-><init>(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->h:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->g:I

    return p1
.end method

.method private a(Landroid/content/Context;I)Landroid/app/Notification;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Landroid/content/Context;IZ)Landroid/app/Notification;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/content/Context;IZ)Landroid/app/Notification;
    .locals 5

    new-instance v0, Landroid/app/Notification$Builder;

    const-string v1, "com.android.settings.usagestats_monitor"

    invoke-direct {v0, p1, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v1, 0x1

    if-lez p2, :cond_1

    const/4 v2, 0x0

    if-eqz p3, :cond_0

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->e:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->g:I

    invoke-direct {p0, v4}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v3, v1

    invoke-static {p3, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->d:Ljava/lang/String;

    new-array v3, v1, [Ljava/lang/Object;

    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v3, v2

    invoke-static {p3, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_1
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->f:Ljava/lang/String;

    :goto_0
    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->c:Ljava/lang/String;

    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    const p2, 0x7f08018b

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const p2, 0x7f08018f

    invoke-static {p1, p2}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/drawable/Icon;)Landroid/app/Notification$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    sget-object v0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.action.usagestas.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    const/high16 v2, 0x4000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a:Landroid/app/PendingIntent;

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a:Landroid/app/PendingIntent;

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->h:Landroid/os/Handler;

    return-object p0
.end method

.method private a(I)Ljava/lang/String;
    .locals 8

    div-int/lit8 v0, p1, 0x3c

    rem-int/lit8 p1, p1, 0x3c

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-lez v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f110036

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v0, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    if-lez p1, :cond_1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f110037

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-virtual {v0, v4, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/app/NotificationManager;)V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->d(Landroid/content/Context;)Landroid/app/NotificationChannelGroup;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/NotificationManager;->createNotificationChannelGroup(Landroid/app/NotificationChannelGroup;)V

    new-instance v0, Landroid/app/NotificationChannel;

    const v1, 0x7f1303d4

    invoke-virtual {p0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.android.settings.usagestats_monitor"

    const/4 v3, 0x2

    invoke-direct {v0, v2, v1, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->enableLights(Z)V

    const-string v1, "app_timer"

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->setGroup(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->b(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->c(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;Landroid/content/Context;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->b(Landroid/content/Context;IZ)V

    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->g:I

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->b(Landroid/content/Context;I)V

    return-void
.end method

.method private b(Landroid/content/Context;I)V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/controller/n;

    invoke-direct {v1, p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/controller/n;-><init>(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private b(Landroid/content/Context;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Landroid/content/Context;IZ)Landroid/app/Notification;

    move-result-object p1

    const p2, 0x1aef9

    invoke-virtual {p0, p2, p1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    return-void
.end method

.method private c(Landroid/content/Context;I)V
    .locals 5

    const/16 v0, 0x3c

    if-le p2, v0, :cond_0

    sub-int/2addr p2, v0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->i(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p2, 0x5

    goto :goto_0

    :cond_1
    const/4 p2, 0x1

    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->h:Landroid/os/Handler;

    const/16 v0, 0x6f

    int-to-long v1, p2

    sget-wide v3, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v1, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "LR-DeviceUsageMonitorService"

    const-string v1, "[DeviceUsageMonitorService]...onCreate"

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->b:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->b:Landroid/app/NotificationManager;

    if-nez v1, :cond_0

    const-string v1, "[FATAL] Fail to get NotificationManager!"

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Landroid/app/NotificationManager;)V

    :goto_0
    invoke-virtual {p0}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f130459

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->c:Ljava/lang/String;

    const v1, 0x7f130456

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->d:Ljava/lang/String;

    const v1, 0x7f130458

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->e:Ljava/lang/String;

    const v1, 0x7f130457

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->f:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->g:I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "LR-DeviceUsageMonitorService"

    const-string v1, "[DeviceUsageMonitorService]...onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Service;->stopForeground(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->h:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->b(Landroid/content/Context;)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    const-string v0, "LR-DeviceUsageMonitorService"

    const-string v1, "[DeviceUsageMonitorService]...onStartCommond"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->f(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->g:I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->h:Landroid/os/Handler;

    const/16 v2, 0x6f

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->g:I

    if-lez v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x1aef9

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->g:I

    invoke-direct {p0, v0, v2}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Landroid/content/Context;I)Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->g:I

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->b(Landroid/content/Context;I)V

    goto :goto_0

    :cond_0
    const-string v1, "[DeviceUsageMonitorService]... invalid extra for total limited time."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result p1

    return p1
.end method
