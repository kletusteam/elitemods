.class public Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;
.super Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;


# instance fields
.field private e:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Lcom/airbnb/lottie/LottieAnimationView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/ImageView;

.field private p:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

.field private q:Landroid/animation/ValueAnimator;

.field private r:Landroid/os/Handler;

.field private s:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->r:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->o:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->p:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    return-object p1
.end method

.method private a(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->a()V

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->e()V

    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;)Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->p:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    return-object p0
.end method

.method private b(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->d()V

    :cond_0
    return-void
.end method

.method private c(I)I
    .locals 2

    const/4 v0, 0x1

    const v1, 0x7f13037b

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const v1, 0x7f13037f

    goto :goto_0

    :cond_1
    const v1, 0x7f13037e

    goto :goto_0

    :cond_2
    const v1, 0x7f13037d

    goto :goto_0

    :cond_3
    const v1, 0x7f13037c

    :cond_4
    :goto_0
    return v1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;)Lcom/airbnb/lottie/LottieAnimationView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->l:Lcom/airbnb/lottie/LottieAnimationView;

    return-object p0
.end method

.method private d(I)I
    .locals 2

    const/4 v0, 0x1

    const v1, 0x7f130380

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const v1, 0x7f130384

    goto :goto_0

    :cond_1
    const v1, 0x7f130383

    goto :goto_0

    :cond_2
    const v1, 0x7f130382

    goto :goto_0

    :cond_3
    const v1, 0x7f130381

    :cond_4
    :goto_0
    return v1
.end method

.method private e(I)V
    .locals 3

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v1, 0x5

    if-le p1, v1, :cond_1

    :cond_0
    move p1, v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "images_lv"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sweep"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ".json"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->l:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->l:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0, p1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->l:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->b(Lcom/airbnb/lottie/LottieAnimationView;)V

    return-void
.end method

.method private g(Landroid/view/View;)V
    .locals 1

    const v0, 0x7f0b0168

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->f:Landroid/widget/TextView;

    const v0, 0x7f0b0166

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->g:Landroid/widget/TextView;

    const v0, 0x7f0b01db

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->h:Landroid/widget/TextView;

    const v0, 0x7f0b0164

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->i:Landroid/widget/TextView;

    const v0, 0x7f0b01b5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->j:Landroid/widget/TextView;

    const v0, 0x7f0b0175

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->k:Landroid/widget/TextView;

    const v0, 0x7f0b01b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->l:Lcom/airbnb/lottie/LottieAnimationView;

    const v0, 0x7f0b01f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->m:Landroid/widget/TextView;

    const v0, 0x7f0b01f2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->n:Landroid/widget/TextView;

    const v0, 0x7f0b0248

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->o:Landroid/widget/ImageView;

    return-void
.end method

.method private m()V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "levelData"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->e:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    return-void
.end method

.method private n()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v1, 0x2ee0

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->s:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->s:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/u;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/u;-><init>(Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->s:Landroid/animation/ValueAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->s:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private o()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->p:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    if-nez v0, :cond_0

    const v0, 0x7f03001b

    const/16 v1, 0x18

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a(IILandroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a(Landroid/widget/ImageView;)Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->p:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->p:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->a()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->r:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/s;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/s;-><init>(Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;)V

    const-wide/16 v2, 0x2ee0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private p()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v1, 0x7d0

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->q:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->q:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/t;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/t;-><init>(Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->q:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->q:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private q()V
    .locals 10

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->e:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->i()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-wide v3, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->totalTime:J

    invoke-static {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11002e

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->usedTimes:I

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v6, v8

    invoke-virtual {v2, v3, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->consecutiveDays:I

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v8

    const v6, 0x7f11001c

    invoke-virtual {v2, v6, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->totalDays:I

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v8

    invoke-virtual {v2, v6, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f1303eb

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->levelName:Ljava/lang/String;

    aput-object v6, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->beat:I

    if-ltz v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->k:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f130366

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f13040b

    new-array v5, v5, [Ljava/lang/Object;

    iget v9, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->beat:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->k:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->j:Landroid/widget/TextView;

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f1303fc

    goto :goto_0

    :cond_2
    const v2, 0x7f1303fb

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->m:Landroid/widget/TextView;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->currentLevel:I

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->d(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->n:Landroid/widget/TextView;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->currentLevel:I

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->c(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->l:Lcom/airbnb/lottie/LottieAnimationView;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->currentLevel:I

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setImageResource(I)V

    iget v0, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->currentLevel:I

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->e(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->p()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->n()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e0088

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method protected k()Ljava/lang/String;
    .locals 1

    const-string v0, "FocusHistoryShareFragment"

    return-object v0
.end method

.method protected l()Lcom/xiaomi/misettings/usagestats/focusmode/b/a;
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->e:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->a(Ljava/lang/Object;)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->l:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->a(Lcom/airbnb/lottie/LottieAnimationView;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->l:Lcom/airbnb/lottie/LottieAnimationView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->q:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->m()V

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->g(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->q()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/FocusHistoryShareFragment;->o()V

    return-void
.end method
