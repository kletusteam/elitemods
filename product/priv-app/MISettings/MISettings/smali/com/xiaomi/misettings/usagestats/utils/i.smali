.class final Lcom/xiaomi/misettings/usagestats/utils/i;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/utils/j;->a(Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->a:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->e(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "limit_category_list"

    const-string v2, "[]"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initAllCategory="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0xbbc

    invoke-static {v2, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/E;->a(Ljava/util/List;)Z

    move-result v0

    const-string v2, "BizSvr_cate_Utils"

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initAllLimitCategory: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ",isNewDay="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v6, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->b:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v5, 0x0

    move v6, v5

    move v7, v6

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iget-object v8, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->a:Landroid/content/Context;

    invoke-static {v8, v6}, Lcom/xiaomi/misettings/usagestats/utils/j;->e(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v3, v4, v8, v9}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v8

    const/4 v9, 0x1

    xor-int/2addr v8, v9

    iget-boolean v10, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->b:Z

    if-nez v10, :cond_1

    if-eqz v8, :cond_0

    goto :goto_1

    :cond_0
    iget-object v9, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->a:Landroid/content/Context;

    invoke-static {v9, v6, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_2

    :cond_1
    :goto_1
    iget-object v10, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->a:Landroid/content/Context;

    invoke-static {v10, v6, v5}, Lcom/xiaomi/misettings/usagestats/controller/b;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-object v10, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->a:Landroid/content/Context;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v11

    invoke-static {v10, v6, v11}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v11

    invoke-static {v10, v6, v11}, Lcom/xiaomi/misettings/usagestats/utils/j;->b(Landroid/content/Context;Ljava/lang/String;I)V

    if-nez v7, :cond_2

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->a:Landroid/content/Context;

    const-wide/16 v10, 0x0

    invoke-static {v6, v10, v11}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;J)V

    move v7, v9

    :cond_2
    :goto_2
    move v6, v8

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->b:Z

    if-nez v0, :cond_4

    if-eqz v6, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->b(Landroid/content/Context;)V

    :cond_5
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/i;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->k(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initAllLimitCategory: registerLimitTime duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v3

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_6
    const-string v0, "initAllLimitCategory: no limit apps"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    return-void
.end method
