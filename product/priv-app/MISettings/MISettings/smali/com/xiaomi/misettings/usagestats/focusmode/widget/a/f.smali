.class public Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    const-string v1, "images_morning"

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    return-object v1

    :cond_0
    const-string p1, "images_night"

    return-object p1

    :cond_1
    const-string p1, "images_afternoon"

    return-object p1

    :cond_2
    return-object v1
.end method

.method public b(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    const-string v1, "morning.json"

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    return-object v1

    :cond_0
    const-string p1, "night.json"

    return-object p1

    :cond_1
    const-string p1, "afternoon.json"

    return-object p1

    :cond_2
    return-object v1
.end method
