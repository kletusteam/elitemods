.class public Lcom/xiaomi/misettings/usagestats/widget/a/b/a;
.super Lcom/xiaomi/misettings/usagestats/widget/a/b/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;-><init>()V

    const-class v0, Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/NormalUsageStatsWidget;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/a/b/c;->b:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    const v0, 0x7f0700b9

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result p1

    int-to-float p1, p1

    const v0, 0x7f0b039d

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1, p1}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V
    .locals 7

    if-eqz p3, :cond_0

    const v0, 0x7f0700f0

    goto :goto_0

    :cond_0
    const v0, 0x7f0700e9

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v4

    const/4 v5, 0x0

    if-eqz p3, :cond_1

    const p3, 0x7f0700ef

    goto :goto_1

    :cond_1
    const p3, 0x7f0700e5

    :goto_1
    invoke-virtual {p0, p1, p3}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v6

    const v2, 0x7f0b039d

    const/4 v3, 0x0

    move-object v1, p2

    invoke-virtual/range {v1 .. v6}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    return-void
.end method

.method protected b()I
    .locals 1

    const v0, 0x7f0e0040

    return v0
.end method

.method protected b(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x0

    if-eqz p3, :cond_0

    const v3, 0x7f0700e5

    invoke-virtual {v0, v1, v3}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v3

    move v9, v3

    goto :goto_0

    :cond_0
    move v9, v2

    :goto_0
    const v5, 0x7f0b038e

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v4 .. v9}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    const v11, 0x7f0b039d

    const/4 v12, 0x0

    if-eqz p3, :cond_1

    const v3, 0x7f0700f3

    goto :goto_1

    :cond_1
    const v3, 0x7f0700f9

    :goto_1
    invoke-virtual {v0, v1, v3}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v13

    const/4 v14, 0x0

    if-eqz p3, :cond_2

    move v15, v2

    goto :goto_2

    :cond_2
    const v3, 0x7f0700dc

    invoke-virtual {v0, v1, v3}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v3

    move v15, v3

    :goto_2
    move-object/from16 v10, p2

    invoke-virtual/range {v10 .. v15}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    const v3, 0x7f0b039d

    if-eqz p3, :cond_3

    const v4, 0x7f070107

    goto :goto_3

    :cond_3
    const v4, 0x7f070109

    :goto_3
    invoke-virtual {v0, v1, v4}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v4, p2

    invoke-virtual {v4, v3, v2, v1}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    return-void
.end method

.method protected c(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/xiaomi/misettings/usagestats/widget/a/a/a;)V
    .locals 3

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const v1, 0x7f0b0389

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f0b039d

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->d()J

    move-result-wide v1

    invoke-static {p1, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v0, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    return-void
.end method

.method protected c(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V
    .locals 4

    const p3, 0x7f0b0257

    invoke-virtual {p2, p3}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, 0x5

    if-ge v1, v0, :cond_1

    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0e003b

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2, p3, v0}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(I)Lcom/xiaomi/misettings/usagestats/widget/a/a/a;

    move-result-object v2

    invoke-virtual {p0, p1, v0, v2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/xiaomi/misettings/usagestats/widget/a/a/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    :goto_1
    const p1, 0x7f0b025a

    invoke-virtual {p2, p1, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const/16 p1, 0x8

    invoke-virtual {p2, p3, p1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    return-void
.end method
