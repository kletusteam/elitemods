.class Lcom/xiaomi/misettings/usagestats/devicelimit/b/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/c;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onClick: which="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "renderView"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/c;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->e(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->a(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/c;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;

    invoke-static {v0, p2}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->b(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;Z)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/c;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->f(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;)V

    return-void
.end method
