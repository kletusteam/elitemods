.class public Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/a;
.super Ljava/lang/Object;


# direct methods
.method private static a(I)Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/b;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    new-instance p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/f;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/f;-><init>()V

    return-object p0

    :cond_0
    new-instance p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/c;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/c;-><init>()V

    return-object p0

    :cond_1
    new-instance p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/d;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/d;-><init>()V

    return-object p0

    :cond_2
    new-instance p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/e;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/e;-><init>()V

    return-object p0

    :cond_3
    new-instance p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/f;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/f;-><init>()V

    return-object p0
.end method

.method public static a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/b;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IAnima type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/a;->b(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FocusAnimationHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/a;->b(Landroid/content/Context;)I

    move-result p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/a;->a(I)Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/b;

    move-result-object p0

    return-object p0
.end method

.method public static b(Landroid/content/Context;)I
    .locals 1

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/misettings/common/utils/m;->d(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x4

    goto :goto_0

    :cond_0
    const/4 p0, 0x3

    :goto_0
    return p0

    :cond_1
    invoke-static {p0}, Lcom/misettings/common/utils/f;->m(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_2

    const/4 p0, 0x2

    return p0

    :cond_2
    const/4 p0, 0x1

    return p0
.end method
