.class public Lcom/xiaomi/misettings/usagestats/weeklyreport/b/a;
.super Ljava/lang/Object;


# direct methods
.method public static final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    if-eqz p0, :cond_8

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, -0x5d073cfb

    const/4 v4, 0x1

    if-eq v2, v3, :cond_2

    const v3, 0x610fd69b

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "focusMode"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    move v1, v4

    goto :goto_0

    :cond_2
    const-string v2, "deviceLimit"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v1, 0x0

    :cond_3
    :goto_0
    if-eqz v1, :cond_5

    if-eq v1, v4, :cond_4

    goto :goto_1

    :cond_4
    new-instance p1, Lcom/misettings/common/base/a;

    invoke-direct {p1, p0}, Lcom/misettings/common/base/a;-><init>(Landroid/content/Context;)V

    const-string v0, "com.xiaomi.misettings.usagestats.focusmode.FocusSettingsFragment"

    invoke-virtual {p1, v0}, Lcom/misettings/common/base/a;->a(Ljava/lang/String;)Lcom/misettings/common/base/a;

    invoke-virtual {p1}, Lcom/misettings/common/base/a;->c()Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :cond_5
    new-instance v0, Landroid/content/Intent;

    const-class p1, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetActivity;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :goto_1
    if-nez v0, :cond_6

    return-void

    :cond_6
    instance-of p1, p0, Landroid/app/Activity;

    if-nez p1, :cond_7

    const/high16 p1, 0x10000000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_7
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_8
    :goto_2
    return-void
.end method
