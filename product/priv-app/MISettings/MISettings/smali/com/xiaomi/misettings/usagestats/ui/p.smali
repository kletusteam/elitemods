.class Lcom/xiaomi/misettings/usagestats/ui/p;
.super Landroidx/recyclerview/widget/RecyclerView$k;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/ui/q;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/ui/q;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/ui/q;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/p;->a:Lcom/xiaomi/misettings/usagestats/ui/q;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$k;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollStateChanged(Landroidx/recyclerview/widget/RecyclerView;I)V
    .locals 2
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$k;->onScrollStateChanged(Landroidx/recyclerview/widget/RecyclerView;I)V

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/p;->a:Lcom/xiaomi/misettings/usagestats/ui/q;

    iget-object p2, p2, Lcom/xiaomi/misettings/usagestats/ui/q;->a:Lcom/xiaomi/misettings/usagestats/ui/r;

    iget-object p2, p2, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->e(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/D;

    move-result-object p2

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/utils/D;->c()V

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object p2

    check-cast p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->G()I

    move-result p2

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->H()I

    move-result p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/p;->a:Lcom/xiaomi/misettings/usagestats/ui/q;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/ui/q;->a:Lcom/xiaomi/misettings/usagestats/ui/r;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->d(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)Lcom/xiaomi/misettings/usagestats/adapter/d;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/p;->a:Lcom/xiaomi/misettings/usagestats/ui/q;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/ui/q;->a:Lcom/xiaomi/misettings/usagestats/ui/r;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->d(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)Lcom/xiaomi/misettings/usagestats/adapter/d;

    move-result-object v0

    sub-int/2addr p1, p2

    const/4 v1, 0x1

    add-int/2addr p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p2, p1, v1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemRangeChanged(IILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/p;->a:Lcom/xiaomi/misettings/usagestats/ui/q;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/ui/q;->a:Lcom/xiaomi/misettings/usagestats/ui/r;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->f(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/D;

    move-result-object p1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/utils/D;->b()V

    :cond_1
    :goto_0
    return-void
.end method
