.class public Lcom/xiaomi/misettings/usagestats/h/d;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/hapjs/features/channel/h$b;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z

.field private c:Lorg/hapjs/features/channel/b/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/h/c;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/h/c;-><init>(Lcom/xiaomi/misettings/usagestats/h/d;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/h/d;->c:Lorg/hapjs/features/channel/b/b;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/h/d;->a:Landroid/content/Context;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/h/d;->b:Z

    return-void
.end method

.method private a(Ljava/lang/String;Lorg/hapjs/features/channel/d;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p2}, Lorg/hapjs/features/channel/d;->b()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, [B

    if-eqz v1, :cond_0

    check-cast v0, [B

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive msg from hap app, pkgName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", code:"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p2, Lorg/hapjs/features/channel/d;->a:I

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", data:"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "Niel-EventHandlerImpl"

    invoke-static {p2, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Landroid/content/Context;Lorg/hapjs/features/channel/j;)V
    .locals 4

    const-string v0, "Niel-EventHandlerImpl"

    const-string v1, "checkThenSend...begin..."

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "key_has_accredit"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/xiaomi/misettings/usagestats/provider/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    invoke-static {}, Lcom/xiaomi/misettings/f;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v1, "-1"

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    const-string v1, "1"

    goto :goto_0

    :cond_1
    const-string v1, "0"

    :goto_0
    const-string v3, "code"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "isDarkMode"

    invoke-static {p1}, Lcom/xiaomi/misettings/f;->c(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "appVersion"

    invoke-static {p1}, Lcom/misettings/common/utils/f;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_1
    const/16 p1, 0x3ec

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, p1, v1}, Lcom/xiaomi/misettings/usagestats/h/d;->c(Lorg/hapjs/features/channel/j;ILjava/lang/String;)V

    const-string p1, "checkThenSend...end..."

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/h/d;Lorg/hapjs/features/channel/j;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/h/d;->c(Lorg/hapjs/features/channel/j;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/h/d;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/h/d;->b:Z

    return p1
.end method

.method private b(Landroid/content/Context;Lorg/hapjs/features/channel/j;)V
    .locals 3

    const-string v0, "Niel-EventHandlerImpl"

    const-string v1, "syncThenSend...begin..."

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v1

    new-instance v2, Lcom/xiaomi/misettings/usagestats/h/b;

    invoke-direct {v2, p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/h/b;-><init>(Lcom/xiaomi/misettings/usagestats/h/d;Landroid/content/Context;Lorg/hapjs/features/channel/j;)V

    invoke-virtual {v1, v2}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    const-string p1, "syncThenSend...end..."

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(Lorg/hapjs/features/channel/j;)V
    .locals 2

    new-instance v0, Lorg/hapjs/features/channel/d;

    invoke-direct {v0}, Lorg/hapjs/features/channel/d;-><init>()V

    const/16 v1, 0x4b4

    iput v1, v0, Lorg/hapjs/features/channel/d;->a:I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/h/d;->c:Lorg/hapjs/features/channel/b/b;

    invoke-interface {p1, v0, v1}, Lorg/hapjs/features/channel/j;->a(Lorg/hapjs/features/channel/d;Lorg/hapjs/features/channel/b/b;)V

    return-void
.end method

.method private c(Lorg/hapjs/features/channel/j;ILjava/lang/String;)V
    .locals 1

    new-instance v0, Lorg/hapjs/features/channel/d;

    invoke-direct {v0}, Lorg/hapjs/features/channel/d;-><init>()V

    iput p2, v0, Lorg/hapjs/features/channel/d;->a:I

    invoke-virtual {v0, p3}, Lorg/hapjs/features/channel/d;->a(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/h/d;->c:Lorg/hapjs/features/channel/b/b;

    invoke-interface {p1, v0, p2}, Lorg/hapjs/features/channel/j;->a(Lorg/hapjs/features/channel/d;Lorg/hapjs/features/channel/b/b;)V

    return-void
.end method


# virtual methods
.method public a(Lorg/hapjs/features/channel/j;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "New channel opened, from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lorg/hapjs/features/channel/j;->a()Lorg/hapjs/features/channel/a/b;

    move-result-object v1

    iget-object v1, v1, Lorg/hapjs/features/channel/a/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Niel-EventHandlerImpl"

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x3eb

    const-string v1, ""

    invoke-direct {p0, p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/h/d;->c(Lorg/hapjs/features/channel/j;ILjava/lang/String;)V

    return-void
.end method

.method public a(Lorg/hapjs/features/channel/j;ILjava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Channel opened by "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lorg/hapjs/features/channel/j;->a()Lorg/hapjs/features/channel/a/b;

    move-result-object p1

    iget-object p1, p1, Lorg/hapjs/features/channel/a/b;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " closed, code "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", reason:"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "Niel-EventHandlerImpl"

    invoke-static {p2, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lorg/hapjs/features/channel/j;Lorg/hapjs/features/channel/d;)V
    .locals 4

    invoke-interface {p1}, Lorg/hapjs/features/channel/j;->a()Lorg/hapjs/features/channel/a/b;

    move-result-object v0

    iget-object v0, v0, Lorg/hapjs/features/channel/a/b;->a:Ljava/lang/String;

    iget v1, p2, Lorg/hapjs/features/channel/d;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceiveMessage(), from="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ",code="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Niel-EventHandlerImpl"

    invoke-static {v3, v2}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0x3ec

    if-eq v1, v2, :cond_3

    const/16 v2, 0x44f

    if-eq v1, v2, :cond_1

    const/16 p2, 0x4b3

    if-eq v1, p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/h/d;->b(Lorg/hapjs/features/channel/j;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0, p2}, Lcom/xiaomi/misettings/usagestats/h/d;->a(Ljava/lang/String;Lorg/hapjs/features/channel/d;)Ljava/lang/String;

    iget-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/h/d;->b:Z

    if-nez p2, :cond_2

    const/4 p2, 0x1

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/h/d;->b:Z

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/h/d;->a:Landroid/content/Context;

    invoke-direct {p0, p2, p1}, Lcom/xiaomi/misettings/usagestats/h/d;->b(Landroid/content/Context;Lorg/hapjs/features/channel/j;)V

    goto :goto_0

    :cond_2
    const-string p1, "[sync in on going, skip this request...]"

    invoke-static {v3, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/h/d;->a:Landroid/content/Context;

    invoke-direct {p0, p2, p1}, Lcom/xiaomi/misettings/usagestats/h/d;->a(Landroid/content/Context;Lorg/hapjs/features/channel/j;)V

    :goto_0
    return-void
.end method

.method public b(Lorg/hapjs/features/channel/j;ILjava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Channel opened by "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lorg/hapjs/features/channel/j;->a()Lorg/hapjs/features/channel/a/b;

    move-result-object p1

    iget-object p1, p1, Lorg/hapjs/features/channel/a/b;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " error, errorCode "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", errorMessage:"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "Niel-EventHandlerImpl"

    invoke-static {p2, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
