.class public Lcom/xiaomi/misettings/usagestats/controller/l;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/lang/String; = "LR-DeviceUsageController"

.field private static final b:I

.field private static final c:I

.field private static d:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Lcom/xiaomi/misettings/usagestats/utils/L;->j:I

    sput v0, Lcom/xiaomi/misettings/usagestats/controller/l;->b:I

    mul-int/lit8 v0, v0, 0x2

    sput v0, Lcom/xiaomi/misettings/usagestats/controller/l;->c:I

    return-void
.end method

.method static synthetic a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->p(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/controller/l;->a:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/controller/j;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/j;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a(Landroid/content/Context;IZ)V
    .locals 0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    if-eqz p2, :cond_0

    const-string p2, "key_stat_limited_time_weekday"

    goto :goto_0

    :cond_0
    const-string p2, "key_stat_limited_time_weekend"

    :goto_0
    invoke-static {p0, p2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method public static a(Landroid/content/Context;J)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "key_stat_today_notify_time"

    invoke-static {p0, v0, p1, p2}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;",
            ">;Z)V"
        }
    .end annotation

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    if-eqz p2, :cond_1

    const-string p2, "misetting_device_unusable_time_weekday"

    goto :goto_0

    :cond_1
    const-string p2, "misetting_device_unusable_time_weekend"

    :goto_0
    invoke-static {p1}, Lb/e/a/b/c;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p2, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 1

    sget-object p1, Lcom/xiaomi/misettings/usagestats/controller/l;->a:Ljava/lang/String;

    const-string v0, "IMPORTANT: ensureServiceRunning()....."

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object p1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/controller/k;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/controller/k;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a(Landroid/content/Context;ZZ)V
    .locals 0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    if-eqz p2, :cond_0

    const-string p2, "device_limited_enable_weekday"

    goto :goto_0

    :cond_0
    const-string p2, "device_limited_enable_weekend"

    :goto_0
    invoke-static {p0, p2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method static synthetic b()Ljava/lang/Class;
    .locals 1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/controller/l;->c()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    if-eqz p1, :cond_0

    const-string p1, "misetting_device_unusable_time_weekday"

    goto :goto_0

    :cond_0
    const-string p1, "misetting_device_unusable_time_weekend"

    :goto_0
    invoke-static {p0, p1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static b(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 1

    const-string v0, ""

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "miui.intent.action.settings.SCHEDULE_DEVICE_USAGE_MONITOR"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "from"

    const-string v2, "broadCastUsageMonitor"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public static b(Landroid/content/Context;I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    invoke-virtual {v0}, Lb/e/a/b/h;->b()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {p0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object p0

    const-string v1, "device_continuous_duration"

    invoke-virtual {p0, v0, v1, p1}, Lb/e/a/b/h;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    return-void
.end method

.method public static c(Landroid/content/Context;Z)I
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    if-eqz p1, :cond_0

    const-string p1, "device_limited_enable_weekday"

    goto :goto_0

    :cond_0
    const-string p1, "device_limited_enable_weekend"

    :goto_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method private static c()Ljava/lang/Class;
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-ge v0, v1, :cond_0

    const-class v0, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/miui/greenguard/a/b;->a()Ljava/lang/Class;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "key_stat_monitor_enable"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method public static c(Landroid/content/Context;I)V
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_stat_limited_time_today"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    const-string p1, "key_stat_today"

    invoke-static {p0, p1, v0, v1}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    return-void
.end method

.method public static d(Landroid/content/Context;Z)I
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    if-eqz p1, :cond_0

    const-string p1, "key_stat_limited_time_weekday"

    goto :goto_0

    :cond_0
    const-string p1, "key_stat_limited_time_weekend"

    :goto_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static d(Landroid/content/Context;)V
    .locals 4

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v0

    sget-object v1, Lcom/xiaomi/misettings/usagestats/controller/l;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ensureNotifyNotificationText: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->l(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->m(Landroid/content/Context;)V

    :goto_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;->a(Landroid/content/Context;Z)Z

    return-void
.end method

.method public static d(Landroid/content/Context;I)V
    .locals 2

    invoke-static {p0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    invoke-virtual {v0}, Lb/e/a/b/h;->b()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {p0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object p0

    const-string v1, "device_mandatory_rest_time"

    invoke-virtual {p0, v0, v1, p1}, Lb/e/a/b/h;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    return-void
.end method

.method public static e(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;Z)V

    return-void
.end method

.method public static e(Landroid/content/Context;Z)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_stat_monitor_enable"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->c(Landroid/content/Context;)V

    return-void
.end method

.method public static f(Landroid/content/Context;)I
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_stat_today"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "key_stat_limited_time_today"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    :cond_0
    if-gtz v1, :cond_1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result p0

    return p0

    :cond_1
    return v1
.end method

.method public static f(Landroid/content/Context;Z)V
    .locals 2

    const-string v0, "startMonitor"

    invoke-static {v0}, Lcom/misettings/common/utils/p;->e(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->p(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "isProlong"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "ACTION_RESET"

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->q(Landroid/content/Context;)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/l;->a:Ljava/lang/String;

    const-string p1, "start DeviceUsageMonitorService.........."

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static g(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;Z)I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static h(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const/4 v0, 0x0

    const-string v1, "key_stat_monitor_enable"

    invoke-static {p0, v1, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    move v0, v1

    :cond_0
    return v0
.end method

.method public static i(Landroid/content/Context;)J
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "key_stat_today_notify_time"

    const-wide/16 v1, 0x0

    invoke-static {p0, v0, v1, v2}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static j(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;Z)I

    move-result v1

    const/4 v2, 0x0

    if-eq v1, v0, :cond_1

    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;Z)I

    move-result p0

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v2

    :cond_1
    :goto_0
    return v0
.end method

.method public static k(Landroid/content/Context;)V
    .locals 3

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->p(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->n(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-wide/16 v0, 0x0

    const-string v2, "key_stat_today_notify_time"

    invoke-static {p0, v2, v0, v1}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    const-string v2, "key_stat_today"

    invoke-static {p0, v2, v0, v1}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    const-string v2, "apptimer_load_data_time"

    invoke-static {p0, v2, v0, v1}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    const-string v0, "key_stat_monitor_enable"

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/ContentResolver;Ljava/lang/String;)V

    const-string v0, "device_limited_enable_weekday"

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/ContentResolver;Ljava/lang/String;)V

    const-string v0, "device_limited_enable_weekend"

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/ContentResolver;Ljava/lang/String;)V

    const-string v0, "key_stat_limited_time_weekday"

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/ContentResolver;Ljava/lang/String;)V

    const-string v0, "key_stat_limited_time_weekend"

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/ContentResolver;Ljava/lang/String;)V

    const-string v0, "key_stat_limited_time_today"

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/ContentResolver;Ljava/lang/String;)V

    const-string v0, "misetting_device_unusable_time_weekday"

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->b(Landroid/content/ContentResolver;Ljava/lang/String;)V

    const-string v0, "misetting_device_unusable_time_weekend"

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->b(Landroid/content/ContentResolver;Ljava/lang/String;)V

    return-void
.end method

.method public static l(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->f(Landroid/content/Context;Z)V

    return-void
.end method

.method public static m(Landroid/content/Context;)V
    .locals 2

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->p(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    sget-object v0, Lcom/xiaomi/misettings/usagestats/controller/l;->a:Ljava/lang/String;

    const-string v1, "stop DeviceUsageMonitorService.........."

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->q(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;I)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    return-void
.end method

.method private static n(Landroid/content/Context;)V
    .locals 1

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->o(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    :cond_0
    return-void
.end method

.method private static o(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "miui.intent.action.settings.SCHEDULE_DEVICE_USAGE_MONITOR"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "from"

    const-string v2, "getPendingIntent"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    const-string v2, "key_modify_notification_text"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v2, 0x4000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p0

    return-object p0
.end method

.method private static p(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/controller/l;->d:Landroid/content/Intent;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/controller/l;->c()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/controller/l;->d:Landroid/content/Intent;

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/l;->d:Landroid/content/Intent;

    return-object p0
.end method

.method private static q(Landroid/content/Context;)V
    .locals 6

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->o(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3, p0}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/l;->a:Ljava/lang/String;

    const-string v0, "registerNextAlarm()....Set!"

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/l;->a:Ljava/lang/String;

    const-string v0, "registerNextAlarm()....Cancel!"

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method
