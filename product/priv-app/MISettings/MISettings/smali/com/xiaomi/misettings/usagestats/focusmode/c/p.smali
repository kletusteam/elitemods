.class final Lcom/xiaomi/misettings/usagestats/focusmode/c/p;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->L(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/p;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/p;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/f;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/f;

    move-result-object v0

    const-string v1, "mode_ringer"

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/f;->a(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "FocusModeUtils"

    if-eqz v0, :cond_0

    const-string v0, "ensureStartObserverRinger: start observer ringer"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/p;->a:Landroid/content/Context;

    const-class v2, Lcom/xiaomi/misettings/usagestats/focusmode/service/FocusModeForeBackGroundMonitorService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    const-string v2, "observer_ringer"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/p;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_0
    const-string v0, "ensureStartObserverRinger: need not observer ringer"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method
