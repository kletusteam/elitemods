.class public Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;
.super Landroid/view/View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;
    }
.end annotation


# instance fields
.field private A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field protected Aa:F

.field private B:F

.field protected Ba:F

.field private C:F

.field private Ca:I

.field private D:Z

.field private Da:F

.field private E:F

.field private Ea:Landroid/graphics/Rect;

.field private F:J

.field private Fa:Landroid/graphics/RectF;

.field public G:Z

.field private Ga:F

.field private H:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;

.field private Ha:F

.field private I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private Ia:F

.field private J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private Ja:Landroid/graphics/RectF;

.field private K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;",
            ">;"
        }
    .end annotation
.end field

.field private Ka:Landroid/graphics/PointF;

.field private L:Landroid/graphics/Path;

.field private La:Landroid/graphics/PointF;

.field private M:Landroid/animation/ValueAnimator;

.field private Ma:F

.field private N:F

.field private Na:F

.field private O:Landroid/graphics/PathMeasure;

.field private Oa:Z

.field private P:F

.field private Pa:Ljava/lang/String;

.field private Q:Landroid/graphics/Path;

.field private Qa:Ljava/lang/String;

.field private R:F

.field protected Ra:F

.field private S:F

.field private Sa:F

.field private T:F

.field private Ta:F

.field private U:F

.field private Ua:Landroid/graphics/Paint;

.field private V:F

.field private Va:Landroid/graphics/Paint;

.field private W:Landroid/graphics/Paint;

.field private Wa:F

.field private Xa:F

.field private Ya:F

.field private Za:I

.field private _a:F

.field private final a:Ljava/lang/String;

.field private aa:F

.field private ab:F

.field protected b:I

.field private ba:F

.field private bb:F

.field protected c:I

.field private ca:F

.field private cb:I

.field protected d:I

.field private da:I

.field private db:I

.field protected e:I

.field private ea:F

.field private eb:Landroid/os/Handler;

.field protected f:I

.field private fa:Z

.field private fb:I

.field protected g:I

.field private ga:Z

.field private gb:I

.field protected h:I

.field private ha:Landroid/animation/ValueAnimator;

.field private hb:Z

.field i:[F

.field private volatile ia:I

.field private ib:Landroid/animation/ValueAnimator;

.field j:[F

.field private ja:I

.field private jb:I

.field k:I

.field private ka:F

.field private kb:F

.field private l:I

.field private la:F

.field private lb:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;",
            ">;"
        }
    .end annotation
.end field

.field private m:I

.field private ma:F

.field private n:Landroid/graphics/Paint;

.field private na:I

.field private o:Landroid/graphics/Paint;

.field private oa:I

.field private p:Landroid/graphics/Paint;

.field private pa:I

.field private q:Landroid/graphics/Paint;

.field private qa:Z

.field private r:Landroid/graphics/Paint;

.field private ra:I

.field private s:I

.field private sa:Ljava/math/BigDecimal;

.field private t:I

.field private ta:I

.field private u:I

.field private ua:I

.field private v:I

.field private va:I

.field private w:Ljava/lang/Long;

.field private wa:I

.field private x:Ljava/lang/Long;

.field private xa:I

.field private y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ya:I

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private za:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const-string v0, "LineChart"

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a:Ljava/lang/String;

    const/4 v0, 0x3

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->h:I

    const/4 v0, 0x2

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->i:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->j:[F

    const/4 v1, 0x0

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    const/16 v2, 0xb6

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->u:I

    const/16 v2, 0xa0

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->v:I

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    const-wide v2, 0x7fffffffffffffffL

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->x:Ljava/lang/Long;

    const/4 v2, 0x0

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->E:F

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->F:J

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->G:Z

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->K:Ljava/util/List;

    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Q:Landroid/graphics/Path;

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->R:F

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->S:F

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->T:F

    const/high16 v3, 0x41f00000    # 30.0f

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->U:F

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ba:F

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->da:I

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ea:F

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->fa:Z

    iput-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ga:Z

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ha:Landroid/animation/ValueAnimator;

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ia:I

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ja:I

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ka:F

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->la:F

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ma:F

    const/16 v0, 0x62

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->na:I

    const/16 v0, 0x1d

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->oa:I

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->pa:I

    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ya:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Da:F

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    iput-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->hb:Z

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->kb:F

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->lb:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Landroid/content/Context;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string p2, "LineChart"

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a:Ljava/lang/String;

    const/4 p2, 0x3

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->h:I

    const/4 p2, 0x2

    new-array v0, p2, [F

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->i:[F

    new-array v0, p2, [F

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->j:[F

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    const/16 v1, 0xb6

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->u:I

    const/16 v1, 0xa0

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->v:I

    const-wide/16 v1, -0x1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    const-wide v1, 0x7fffffffffffffffL

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->x:Ljava/lang/Long;

    const/4 v1, 0x0

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->E:F

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->F:J

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->G:Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->K:Ljava/util/List;

    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Q:Landroid/graphics/Path;

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->R:F

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->S:F

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->T:F

    const/high16 v2, 0x41f00000    # 30.0f

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->U:F

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ba:F

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->da:I

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ea:F

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->fa:Z

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ga:Z

    new-array p2, p2, [F

    fill-array-data p2, :array_0

    invoke-static {p2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p2

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ha:Landroid/animation/ValueAnimator;

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ia:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ja:I

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ka:F

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->la:F

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ma:F

    const/16 p2, 0x62

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->na:I

    const/16 p2, 0x1d

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->oa:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->pa:I

    new-instance p2, Ljava/math/BigDecimal;

    invoke-direct {p2, v0}, Ljava/math/BigDecimal;-><init>(I)V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ya:I

    const/high16 p2, 0x3f800000    # 1.0f

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Da:F

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->hb:Z

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->kb:F

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->lb:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Landroid/content/Context;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string p2, "LineChart"

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a:Ljava/lang/String;

    const/4 p2, 0x3

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->h:I

    const/4 p2, 0x2

    new-array p3, p2, [F

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->i:[F

    new-array p3, p2, [F

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->j:[F

    const/4 p3, 0x0

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    const/16 v0, 0xb6

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->u:I

    const/16 v0, 0xa0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->v:I

    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->x:Ljava/lang/Long;

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    iput-boolean p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->E:F

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->F:J

    iput-boolean p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->G:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->K:Ljava/util/List;

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Q:Landroid/graphics/Path;

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->R:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->S:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->T:F

    const/high16 v1, 0x41f00000    # 30.0f

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->U:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ba:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->da:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ea:F

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->fa:Z

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ga:Z

    new-array p2, p2, [F

    fill-array-data p2, :array_0

    invoke-static {p2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p2

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ha:Landroid/animation/ValueAnimator;

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ia:I

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ja:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ka:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->la:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ma:F

    const/16 p2, 0x62

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->na:I

    const/16 p2, 0x1d

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->oa:I

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->pa:I

    new-instance p2, Ljava/math/BigDecimal;

    invoke-direct {p2, p3}, Ljava/math/BigDecimal;-><init>(I)V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ya:I

    const/high16 p2, 0x3f800000    # 1.0f

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Da:F

    iput-boolean p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->hb:Z

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->kb:F

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->lb:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Landroid/content/Context;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private a(FFLandroid/graphics/Paint;)F
    .locals 1

    invoke-direct {p0, p3}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Landroid/graphics/Paint;)F

    move-result p3

    sub-float p2, p1, p2

    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr p3, v0

    const/high16 v0, 0x40400000    # 3.0f

    div-float/2addr p3, v0

    cmpg-float p2, p2, p3

    const/high16 v0, 0x41000000    # 8.0f

    if-gez p2, :cond_0

    add-float/2addr p1, p3

    add-float/2addr p1, v0

    return p1

    :cond_0
    add-float/2addr p1, v0

    return p1
.end method

.method private a(Landroid/graphics/Paint;)F
    .locals 1

    invoke-virtual {p1}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object p1

    iget v0, p1, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget p1, p1, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v0, p1

    return v0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->N:F

    return p1
.end method

.method private a(II)Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;
    .locals 5

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/math/BigDecimal;

    invoke-virtual {v4}, Ljava/math/BigDecimal;->floatValue()F

    move-result v4

    iput v4, v0, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/math/BigDecimal;->floatValue()F

    move-result p1

    iput p1, v0, Landroid/graphics/PointF;->y:F

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/math/BigDecimal;->floatValue()F

    move-result p1

    iput p1, v1, Landroid/graphics/PointF;->x:F

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/math/BigDecimal;->floatValue()F

    move-result p1

    iput p1, v1, Landroid/graphics/PointF;->y:F

    iget p1, v0, Landroid/graphics/PointF;->x:F

    iget p2, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr p1, p2

    const/high16 p2, 0x40000000    # 2.0f

    div-float/2addr p1, p2

    iput p1, v2, Landroid/graphics/PointF;->x:F

    iget p2, v0, Landroid/graphics/PointF;->y:F

    iput p2, v2, Landroid/graphics/PointF;->y:F

    iput p1, v3, Landroid/graphics/PointF;->x:F

    iget p1, v1, Landroid/graphics/PointF;->y:F

    iput p1, v3, Landroid/graphics/PointF;->y:F

    new-instance p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    invoke-direct {p1, v0, v2, v3, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    return-object p1
.end method

.method private a(F)V
    .locals 7

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->da:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v0, v0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    sub-float/2addr v0, v2

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    invoke-static {v0, p1, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/a;->a(Ljava/util/List;FF)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    add-int/lit8 v4, v0, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gtz v3, :cond_0

    goto :goto_1

    :cond_0
    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ea:F

    sub-float/2addr p1, v0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->kb:F

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->kb:F

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    cmpl-float p1, p1, v0

    if-lez p1, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    if-eq p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->fa:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ga:Z

    if-eq p1, v2, :cond_1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tempIndex is : "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v2, "LineChart"

    invoke-static {v2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->getInfoPointByRTL()Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    move-result-object p1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->lb:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    sub-int/2addr v2, v1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long p1, v1, v5

    if-lez p1, :cond_1

    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->lb:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ge v0, p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->lb:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private a(FF)V
    .locals 3

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ma:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->lb:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->lb:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->S:F

    sub-float v1, p1, v0

    sub-float v0, p1, v0

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->T:F

    sub-float v2, p2, v0

    sub-float/2addr p2, v0

    mul-float/2addr v2, p2

    add-float/2addr v1, v2

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->V:F

    mul-float/2addr p2, p2

    cmpg-float p2, v1, p2

    if-gez p2, :cond_1

    const/4 p2, 0x0

    invoke-static {p0, p2}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;Z)V

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p2

    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->da:I

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p2

    invoke-interface {p2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ea:F

    :cond_1
    return-void
.end method

.method private a(ILandroid/graphics/Path;)V
    .locals 9

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(II)Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    move-result-object v0

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->a:Landroid/graphics/PointF;

    iget v2, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p2, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->b:Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->y:F

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->c:Landroid/graphics/PointF;

    iget v5, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->d:Landroid/graphics/PointF;

    iget v7, v1, Landroid/graphics/PointF;->x:F

    iget v8, v1, Landroid/graphics/PointF;->y:F

    move-object v2, p2

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ea:F

    :goto_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    if-ge v0, p1, :cond_0

    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(II)Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    move-result-object v0

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->b:Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->y:F

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->c:Landroid/graphics/PointF;

    iget v5, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->d:Landroid/graphics/PointF;

    iget v7, v1, Landroid/graphics/PointF;->x:F

    iget v8, v1, Landroid/graphics/PointF;->y:F

    move-object v2, p2

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ea:F

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0704b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0704b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/high16 v2, 0x40400000    # 3.0f

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v2

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->pa:I

    const/high16 v2, 0x42040000    # 33.0f

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v2

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->na:I

    const/high16 v2, 0x41000000    # 8.0f

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v2

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->oa:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0704ed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->U:F

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/a;->a()Z

    move-result v2

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    const v2, 0x7f06036a

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(I)I

    move-result v2

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->va:I

    const v2, 0x7f06035f

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(I)I

    move-result v2

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->wa:I

    new-instance v2, Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->n:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->n:Landroid/graphics/Paint;

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->va:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->n:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->n:Landroid/graphics/Paint;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->n:Landroid/graphics/Paint;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->n:Landroid/graphics/Paint;

    const v4, 0x408ccccd    # 4.4f

    invoke-static {p1, v4}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->n:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    const v2, 0x7f06037a

    invoke-virtual {p1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ta:I

    const v2, 0x7f060379

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(I)I

    move-result v2

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ua:I

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p:Landroid/graphics/Paint;

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ta:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->r:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->r:Landroid/graphics/Paint;

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->va:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->r:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->r:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    const v0, 0x7f060094

    invoke-virtual {p1, v0}, Landroid/content/Context;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->xa:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->xa:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ia:F

    const v0, 0x7f06035c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->s:I

    const v0, 0x7f060361

    invoke-virtual {p1, v0}, Landroid/content/Context;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->t:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    const v1, 0x3f3a2e8c

    invoke-static {p1, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->g()V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->z:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    return-void

    :array_0
    .array-data 4
        0x40c00000    # 6.0f
        0x40800000    # 4.0f
    .end array-data
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->S:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->T:F

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->U:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->h:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->U:F

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->W:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private a(Landroid/graphics/Canvas;I)V
    .locals 5

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->db:I

    if-nez v0, :cond_0

    const v0, 0x7f06039a

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->db:I

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->db:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Sa:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ya:F

    add-float/2addr v1, v3

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ab:F

    add-float/2addr v1, v3

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Pa:Ljava/lang/String;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->bb:F

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v0, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->cb:I

    if-nez v1, :cond_1

    const v1, 0x7f06039b

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(I)I

    move-result v1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->cb:I

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->cb:I

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    invoke-virtual {v1, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ta:F

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x3f8b851f    # 1.09f

    invoke-static {v1, v3}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;)F

    move-result v1

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-static {p2, v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result p2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Qa:Ljava/lang/String;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->bb:F

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, p2, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->d()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->d(I)V

    return-void
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;)V
    .locals 8

    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    iget-object v0, p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->a:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v0, p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->b:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v2, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->c:Landroid/graphics/PointF;

    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v4, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->d:Landroid/graphics/PointF;

    iget v5, v0, Landroid/graphics/PointF;->x:F

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object v0, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    new-instance v0, Landroid/graphics/PathMeasure;

    const/4 v1, 0x0

    invoke-direct {v0, v7, v1}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    invoke-virtual {v0}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->la:F

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ha:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ha:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/h;

    invoke-direct {v2, p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/h;-><init>(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Landroid/graphics/PathMeasure;Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ga:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ha:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/e;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/e;-><init>(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/f;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/f;-><init>(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->R:F

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ja:I

    return p1
.end method

.method private b(F)V
    .locals 5

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;Z)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->da:I

    const/4 v2, 0x0

    if-ne v1, v0, :cond_0

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->da:I

    goto :goto_1

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ma:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x41a00000    # 20.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    invoke-static {v0, p1, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/a;->a(Ljava/util/List;FF)I

    move-result p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "now is : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LineChart"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, p1, -0x1

    if-ltz v0, :cond_4

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    goto :goto_1

    :cond_1
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    if-eq v1, p1, :cond_4

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ga:Z

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    if-le v1, p1, :cond_2

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(ILandroid/graphics/Path;)V

    goto :goto_0

    :cond_2
    if-ge v1, p1, :cond_3

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(ILandroid/graphics/Path;)V

    :cond_3
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(ILandroid/graphics/Path;)V

    nop

    :cond_4
    :goto_1
    return-void
.end method

.method private b(ILandroid/graphics/Path;)V
    .locals 9

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(II)Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    move-result-object v0

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->a:Landroid/graphics/PointF;

    iget v2, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p2, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->b:Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->y:F

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->c:Landroid/graphics/PointF;

    iget v5, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->d:Landroid/graphics/PointF;

    iget v7, v1, Landroid/graphics/PointF;->x:F

    iget v8, v1, Landroid/graphics/PointF;->y:F

    move-object v2, p2

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ea:F

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    :goto_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    if-le v0, p1, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(II)Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    move-result-object v0

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->b:Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->y:F

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->c:Landroid/graphics/PointF;

    iget v5, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->d:Landroid/graphics/PointF;

    iget v7, v1, Landroid/graphics/PointF;->x:F

    iget v8, v1, Landroid/graphics/PointF;->y:F

    move-object v2, p2

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ea:F

    goto :goto_0

    :cond_0
    return-void
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->n:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->za:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Q:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Z)V

    return-void
.end method

.method private b(FF)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Fa:Landroid/graphics/RectF;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->eb:Landroid/os/Handler;

    if-nez p1, :cond_0

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->eb:Landroid/os/Handler;

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->eb:Landroid/os/Handler;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->getAvgTipRect()V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Z)V

    return p1

    :cond_1
    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    const/4 p2, 0x0

    if-eqz p1, :cond_2

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_2
    return p2
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Na:F

    return p1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    return p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    return p1
.end method

.method private c(FF)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Fa:Landroid/graphics/RectF;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ea:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ea:Landroid/graphics/Rect;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ea:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ea:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ma:F

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    if-eqz p1, :cond_1

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ga:F

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ga:F

    int-to-float p2, v0

    add-float/2addr p1, p2

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ha:F

    goto :goto_0

    :cond_1
    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ha:F

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ha:F

    int-to-float p2, v0

    sub-float/2addr p1, p2

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ga:F

    :goto_0
    new-instance p1, Landroid/graphics/RectF;

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ga:F

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ma:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ia:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float v3, v1, v2

    sub-float v3, v0, v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ha:F

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-direct {p1, p2, v3, v4, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Fa:Landroid/graphics/RectF;

    :cond_2
    return-void
.end method

.method private c(ILandroid/graphics/Path;)V
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->qa:Z

    if-eqz v0, :cond_0

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->H:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;

    add-int/lit8 p1, p1, -0x1

    invoke-interface {p2, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;->a(I)V

    return-void

    :cond_0
    new-instance v0, Landroid/graphics/PathMeasure;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    invoke-virtual {v0}, Landroid/graphics/PathMeasure;->getLength()F

    move-result p2

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->la:F

    const/4 p2, 0x2

    new-array p2, p2, [F

    fill-array-data p2, :array_0

    invoke-static {p2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p2

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;

    invoke-direct {v1, p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;-><init>(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Landroid/graphics/PathMeasure;I)V

    invoke-virtual {p2, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ga:Z

    if-nez p1, :cond_1

    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->start()V

    :cond_1
    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_7

    :cond_0
    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->m:I

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->na:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Aa:F

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    invoke-virtual {v2}, Ljava/math/BigDecimal;->floatValue()F

    move-result v2

    iget-wide v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->F:J

    long-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->E:F

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    if-eqz v1, :cond_1

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Na:F

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->t:I

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->gb:I

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(FII)I

    move-result v1

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Na:F

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->s:I

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->fb:I

    invoke-static {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(FII)I

    move-result v2

    goto :goto_0

    :cond_1
    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->t:I

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->s:I

    :goto_0
    move v8, v1

    move v9, v2

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v11, 0x2

    const/4 v12, 0x0

    const/high16 v13, 0x41000000    # 8.0f

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x1

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v14}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->xa:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    iget v5, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v4, v1

    iget-object v6, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v3, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    add-float/2addr v2, v13

    iget-object v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v7, v1, v6, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->v:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->hb:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setColor(I)V

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Na:F

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->xa:I

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->va:I

    invoke-static {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(FII)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->xa:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :goto_1
    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    iget v5, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->E:F

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v4, v1

    iget-object v9, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v3, v5

    move v10, v6

    move-object v6, v9

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-interface {v1, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->E:F

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ba:F

    iget-object v4, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    invoke-direct {v0, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(FFLandroid/graphics/Paint;)F

    move-result v2

    iget-object v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v10, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->E:F

    invoke-direct {v0, v1, v10}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(FF)V

    goto :goto_2

    :cond_3
    move v10, v6

    :goto_2
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v14}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setColor(I)V

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->v:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ba:F

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->xa:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    iget v5, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v4, v1

    iget-object v6, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v3, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    add-float/2addr v2, v13

    iget-object v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v10, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    :cond_4
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v14}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v2, 0x0

    iget v5, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v1, v1

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    sub-float v4, v1, v3

    iget-object v6, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v3, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->xa:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v2, v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    add-float/2addr v3, v13

    iget-object v4, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->v:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->hb:Z

    if-eqz v1, :cond_6

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v2, 0x0

    iget v5, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->E:F

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v1, v1

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    sub-float v4, v1, v3

    iget-object v6, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v3, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Na:F

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->xa:I

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->va:I

    invoke-static {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(FII)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_3

    :cond_5
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->xa:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :goto_3
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-interface {v1, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v2, v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->E:F

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ba:F

    iget-object v5, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    invoke-direct {v0, v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(FFLandroid/graphics/Paint;)F

    move-result v3

    iget-object v4, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->E:F

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(FF)V

    :cond_6
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v14}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setColor(I)V

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->v:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ba:F

    const/4 v2, 0x0

    iget v5, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v1, v1

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    sub-float v4, v1, v3

    iget-object v6, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v3, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->xa:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v2, v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    add-float/2addr v3, v13

    iget-object v4, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :goto_4
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->z:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v12, v1, :cond_9

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->r:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    invoke-direct/range {p0 .. p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->getCurrentIndex()I

    move-result v1

    if-ne v12, v1, :cond_7

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->r:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->za:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->z:Ljava/util/List;

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    add-int/lit8 v3, v12, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v2}, Ljava/math/BigDecimal;->floatValue()F

    move-result v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->C:F

    iget-object v4, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->r:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_6

    :cond_7
    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Na:F

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ta:I

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ua:I

    invoke-static {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(FII)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_5

    :cond_8
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ta:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :goto_5
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->z:Ljava/util/List;

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    add-int/lit8 v3, v12, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v2}, Ljava/math/BigDecimal;->floatValue()F

    move-result v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->C:F

    iget-object v4, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :goto_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    :cond_9
    :goto_7
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->fa:Z

    return p1
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    return p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ka:F

    return p1
.end method

.method private d()V
    .locals 3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->m:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->na:I

    sub-int v2, v0, v1

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ya:I

    sub-int v1, v0, v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->oa:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->v:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->pa:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->C:F

    new-instance v0, Ljava/math/BigDecimal;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ba:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    return-void
.end method

.method private d(I)V
    .locals 8

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    const/16 v1, 0xa

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, 0x0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/math/BigDecimal;

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v4, v4

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ba:F

    div-float/2addr v5, v2

    sub-float/2addr v4, v5

    float-to-double v4, v4

    invoke-direct {v0, v4, v5}, Ljava/math/BigDecimal;-><init>(D)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    new-instance v4, Ljava/math/BigDecimal;

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    sub-int/2addr v5, v1

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    new-instance v2, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    new-instance v5, Ljava/math/BigDecimal;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->intValue()I

    move-result v4

    sub-int v4, p1, v4

    invoke-direct {v2, v4}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_3

    if-nez v3, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v3, v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    new-instance v2, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    new-instance v5, Ljava/math/BigDecimal;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->intValue()I

    move-result v4

    sub-int v4, p1, v4

    invoke-direct {v2, v4}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/math/BigDecimal;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    float-to-double v4, v2

    invoke-direct {v1, v4, v5}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    new-instance v1, Ljava/math/BigDecimal;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-direct {v1, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    new-instance v1, Ljava/math/BigDecimal;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    new-instance v3, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->intValue()I

    move-result v2

    sub-int/2addr p1, v2

    invoke-direct {v1, p1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_4
    new-instance v0, Ljava/math/BigDecimal;

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ba:F

    div-float/2addr v4, v2

    float-to-double v4, v4

    invoke-direct {v0, v4, v5}, Ljava/math/BigDecimal;-><init>(D)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    new-instance v4, Ljava/math/BigDecimal;

    invoke-direct {v4, v1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    new-instance v2, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    new-instance v5, Ljava/math/BigDecimal;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->intValue()I

    move-result v4

    sub-int v4, p1, v4

    invoke-direct {v2, v4}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_7

    if-nez v3, :cond_5

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    new-instance v2, Ljava/math/BigDecimal;

    const/4 v4, 0x3

    invoke-direct {v2, v4}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    new-instance v4, Ljava/math/BigDecimal;

    const/4 v5, 0x7

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v2, v4}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v3, v1, :cond_6

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_3
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    new-instance v2, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    new-instance v5, Ljava/math/BigDecimal;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->intValue()I

    move-result v4

    sub-int v4, p1, v4

    invoke-direct {v2, v4}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/math/BigDecimal;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    float-to-double v4, v2

    invoke-direct {v1, v4, v5}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    new-instance v1, Ljava/math/BigDecimal;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v2, v2

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x41700000    # 15.0f

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-direct {v1, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    new-instance v1, Ljava/math/BigDecimal;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    new-instance v3, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->intValue()I

    move-result v2

    sub-int/2addr p1, v2

    invoke-direct {v1, p1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    return-void
.end method

.method private d(Landroid/graphics/Canvas;)V
    .locals 9

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ja:Landroid/graphics/RectF;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Na:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ca:I

    if-nez v1, :cond_0

    const v1, 0x7f06036a

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(I)I

    move-result v1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ca:I

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Va:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ca:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Va:Landroid/graphics/Paint;

    const/high16 v2, 0x41200000    # 10.0f

    const/4 v3, 0x0

    const/high16 v4, 0x40c00000    # 6.0f

    const/high16 v5, 0x42c80000    # 100.0f

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Na:F

    mul-float/2addr v6, v5

    float-to-int v5, v6

    const/4 v6, 0x0

    const/16 v7, 0x96

    const/16 v8, 0xdf

    invoke-static {v5, v6, v7, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Va:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ja:Landroid/graphics/RectF;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Za:I

    int-to-float v3, v2

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Da:F

    mul-float/2addr v3, v4

    int-to-float v2, v2

    mul-float/2addr v2, v4

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Va:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v2, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Va:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->clearShadowLayer()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->La:Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ka:Landroid/graphics/PointF;

    iget v5, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Va:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Landroid/graphics/Canvas;I)V

    :cond_1
    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ga:Z

    return p1
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->S:F

    return p1
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ya:I

    return p0
.end method

.method private e()V
    .locals 14

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->L:Landroid/graphics/Path;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->L:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/math/BigDecimal;

    invoke-virtual {v1}, Ljava/math/BigDecimal;->floatValue()F

    move-result v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/math/BigDecimal;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->floatValue()F

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v2, v0, :cond_1

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/math/BigDecimal;

    invoke-virtual {v5}, Ljava/math/BigDecimal;->floatValue()F

    move-result v5

    iput v5, v0, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/math/BigDecimal;

    invoke-virtual {v5}, Ljava/math/BigDecimal;->floatValue()F

    move-result v5

    iput v5, v0, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/math/BigDecimal;

    invoke-virtual {v5}, Ljava/math/BigDecimal;->floatValue()F

    move-result v5

    iput v5, v1, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/math/BigDecimal;

    invoke-virtual {v5}, Ljava/math/BigDecimal;->floatValue()F

    move-result v5

    iput v5, v1, Landroid/graphics/PointF;->y:F

    iget v5, v0, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/PointF;->x:F

    iget v6, v0, Landroid/graphics/PointF;->y:F

    iput v6, v3, Landroid/graphics/PointF;->y:F

    iput v5, v4, Landroid/graphics/PointF;->x:F

    iget v5, v1, Landroid/graphics/PointF;->y:F

    iput v5, v4, Landroid/graphics/PointF;->y:F

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->L:Landroid/graphics/Path;

    iget v7, v3, Landroid/graphics/PointF;->x:F

    iget v8, v3, Landroid/graphics/PointF;->y:F

    iget v9, v4, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->y:F

    iget v11, v1, Landroid/graphics/PointF;->x:F

    iget v12, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual/range {v6 .. v12}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    new-instance v5, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    invoke-direct {v5, v0, v3, v4, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->K:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    iget v5, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v5, v0}, Landroid/graphics/Path;->moveTo(FF)V

    iget v8, v3, Landroid/graphics/PointF;->x:F

    iget v9, v3, Landroid/graphics/PointF;->y:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    iget v11, v4, Landroid/graphics/PointF;->y:F

    iget v12, v1, Landroid/graphics/PointF;->x:F

    iget v13, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual/range {v7 .. v13}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto/16 :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->T:F

    return p1
.end method

.method private f()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->eb:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    if-eqz v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/g;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/g;-><init>(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V

    const-wide/16 v2, 0x640

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->i()V

    return-void
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ea:F

    return p1
.end method

.method private g()V
    .locals 3

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x410bae14    # 8.73f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Za:I

    const v0, 0x7f0704fb

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->_a:F

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Wa:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const v0, 0x7f0704c4

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Wa:F

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Xa:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    const v0, 0x7f0704c5

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Xa:F

    :cond_1
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Sa:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    const v0, 0x7f0704f5

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Sa:F

    :cond_2
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ta:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    const v0, 0x7f0704f6

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ta:F

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Va:Landroid/graphics/Paint;

    const/4 v2, 0x1

    if-nez v0, :cond_4

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Va:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Va:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :cond_4
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    if-nez v0, :cond_5

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :cond_5
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ya:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_6

    const v0, 0x7f0704c6

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ya:F

    :cond_6
    const v0, 0x7f06035e

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->fb:I

    const v0, 0x7f060360

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->gb:I

    return-void
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->e()V

    return-void
.end method

.method private getAvgTipRect()V
    .locals 8

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->h()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->m()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ka:Landroid/graphics/PointF;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    const/high16 v2, 0x40c00000    # 6.0f

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ha:F

    add-float/2addr v1, v2

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ga:F

    sub-float/2addr v1, v2

    :goto_0
    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ka:Landroid/graphics/PointF;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ma:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->La:Landroid/graphics/PointF;

    iput v1, v2, Landroid/graphics/PointF;->y:F

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, -0x1

    :goto_1
    mul-int/lit8 v1, v1, 0x1e

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, v2, Landroid/graphics/PointF;->x:F

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ka:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->La:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v1, v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    sub-float/2addr v1, v2

    :goto_2
    sub-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ka:Landroid/graphics/PointF;

    iget v2, v1, Landroid/graphics/PointF;->x:F

    int-to-float v0, v0

    sub-float/2addr v2, v0

    iput v2, v1, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->La:Landroid/graphics/PointF;

    iget v2, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v0

    iput v2, v1, Landroid/graphics/PointF;->x:F

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAvgTipRect: avgStart="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ka:Landroid/graphics/PointF;

    invoke-virtual {v2}, Landroid/graphics/PointF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",avgEnd="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->La:Landroid/graphics/PointF;

    invoke-virtual {v2}, Landroid/graphics/PointF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LineChart"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    const/high16 v2, 0x41f00000    # 30.0f

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ha:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ra:F

    add-float/2addr v2, v1

    goto :goto_3

    :cond_3
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ga:F

    sub-float v2, v1, v2

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ra:F

    sub-float v1, v2, v1

    :goto_3
    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ma:F

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Wa:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ba:F

    cmpg-float v5, v3, v4

    if-gez v5, :cond_4

    move v3, v4

    :cond_4
    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Wa:F

    add-float v5, v3, v4

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Aa:F

    cmpl-float v7, v5, v6

    if-lez v7, :cond_5

    sub-float v3, v6, v4

    move v5, v6

    :cond_5
    iput v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ab:F

    sub-float/2addr v1, v0

    sub-float/2addr v2, v0

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->_a:F

    sub-float v0, v2, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->bb:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_4

    :cond_6
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->_a:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->bb:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    :goto_4
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ja:Landroid/graphics/RectF;

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iput v2, v0, Landroid/graphics/RectF;->right:F

    iput v3, v0, Landroid/graphics/RectF;->top:F

    iput v5, v0, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method private getCurrentIndex()I
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->qa:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ra:I

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/f/h;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/d/f/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/f/h;->a()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private getInfoPointByRTL()Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->kb:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(II)Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ia:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(II)Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ia:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->kb:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(II)Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ia:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(II)Lcom/xiaomi/misettings/usagestats/home/widget/linechart/b;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ia:I

    :goto_0
    return-object v0
.end method

.method private getValidWeeks()I
    .locals 8

    const/4 v0, 0x0

    const/4 v1, -0x1

    move v2, v0

    move v3, v1

    :goto_0
    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    if-ne v3, v1, :cond_0

    move v2, v0

    move v3, v2

    goto :goto_1

    :cond_0
    move v2, v0

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    return v2
.end method

.method static synthetic h(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ba:F

    return p1
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ja:Landroid/graphics/RectF;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ja:Landroid/graphics/RectF;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ka:Landroid/graphics/PointF;

    if-nez v0, :cond_1

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ka:Landroid/graphics/PointF;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->La:Landroid/graphics/PointF;

    if-nez v0, :cond_2

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->La:Landroid/graphics/PointF;

    :cond_2
    return-void
.end method

.method static synthetic h(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->j()V

    return-void
.end method

.method static synthetic i(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->N:F

    return p0
.end method

.method static synthetic i(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    return p1
.end method

.method private i()V
    .locals 5

    const/high16 v0, 0x432a0000    # 170.0f

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->V:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->getCurrentIndex()I

    move-result v2

    add-int/2addr v2, v1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->S:F

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->getCurrentIndex()I

    move-result v2

    add-int/2addr v2, v1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->T:F

    :cond_1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->W:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->W:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->W:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->W:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->W:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setWillNotDraw(Z)V

    const/16 v1, 0x64

    const/16 v2, 0x96

    const/16 v3, 0xdf

    invoke-static {v1, v0, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    const/high16 v1, 0x41100000    # 9.0f

    const/4 v2, 0x0

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->h:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->W:Landroid/graphics/Paint;

    invoke-virtual {v4, v1, v2, v3, v0}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    return-void
.end method

.method static synthetic j(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->R:F

    return p0
.end method

.method private j()V
    .locals 3

    new-instance v0, Landroid/graphics/PathMeasure;

    invoke-direct {v0}, Landroid/graphics/PathMeasure;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->O:Landroid/graphics/PathMeasure;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->L:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->O:Landroid/graphics/PathMeasure;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->L:Landroid/graphics/Path;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PathMeasure;->setPath(Landroid/graphics/Path;Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->O:Landroid/graphics/PathMeasure;

    invoke-virtual {v0}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->P:F

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->M:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->M:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->M:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;-><init>(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic k(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->P:F

    return p0
.end method

.method private k()V
    .locals 3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->m:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->na:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->z:Ljava/util/List;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ba:F

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ba:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->z:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41800000    # 16.0f

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    return-void
.end method

.method private l()V
    .locals 12

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->x:Ljava/lang/Long;

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    move-wide v5, v0

    goto :goto_1

    :cond_0
    move-wide v5, v0

    move v2, v4

    :goto_0
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v3, v7, v9

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    :cond_1
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->x:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v3, v7, v9

    if-gez v3, :cond_2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->x:Ljava/lang/Long;

    :cond_2
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    add-long/2addr v5, v7

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->getValidWeeks()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "judgeMaxValue: validWeeks="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "LineChart"

    invoke-static {v7, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    int-to-long v2, v2

    div-long/2addr v5, v2

    iput-wide v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->F:J

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->F:J

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v2, v2, v5

    const/4 v3, 0x1

    if-eqz v2, :cond_4

    move v2, v3

    goto :goto_2

    :cond_4
    move v2, v4

    :goto_2
    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->hb:Z

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const v5, 0x7f130441

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Pa:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-wide v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->F:J

    invoke-static {v2, v5, v6}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Qa:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    :cond_5
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    const-string v5, "0"

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f130394

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    long-to-float v2, v5

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float/2addr v2, v5

    const v6, 0x4a5bba00    # 3600000.0f

    div-float/2addr v2, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "judgeMaxValue: f = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    cmpg-float v5, v2, v5

    const-wide/16 v8, 0x3e8

    const-wide/16 v10, 0x3c

    if-gez v5, :cond_8

    const/high16 v5, 0x42700000    # 60.0f

    mul-float/2addr v2, v5

    float-to-int v5, v2

    int-to-float v5, v5

    sub-float v5, v2, v5

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_6

    float-to-double v5, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-long v5, v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    goto :goto_3

    :cond_6
    float-to-long v5, v2

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    :goto_3
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v0, v5, v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f1303a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11002b

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->intValue()I

    move-result v5

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->intValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-virtual {v1, v2, v5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    mul-long/2addr v0, v8

    mul-long/2addr v0, v10

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    goto :goto_5

    :cond_8
    float-to-double v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "judgeMaxValue: maxValue = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110028

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->intValue()I

    move-result v5

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->intValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-virtual {v1, v2, v5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    mul-long/2addr v0, v10

    mul-long/2addr v0, v8

    mul-long/2addr v0, v10

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    :goto_5
    return-void
.end method

.method static synthetic l(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k()V

    return-void
.end method

.method static synthetic m(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Landroid/graphics/Path;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Q:Landroid/graphics/Path;

    return-object p0
.end method

.method private m()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Sa:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Pa:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ta:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Qa:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    invoke-virtual {p0, v1, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ra:F

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Xa:F

    sub-float/2addr v1, v2

    cmpg-float v1, v1, v0

    if-gez v1, :cond_0

    const/high16 v1, 0x40000000    # 2.0f

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->_a:F

    mul-float/2addr v2, v1

    add-float/2addr v0, v2

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ra:F

    :cond_0
    return-void
.end method

.method static synthetic n(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Landroid/graphics/PathMeasure;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->O:Landroid/graphics/PathMeasure;

    return-object p0
.end method

.method static synthetic o(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Na:F

    return p0
.end method

.method static synthetic p(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ka:F

    return p0
.end method

.method static synthetic q(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->la:F

    return p0
.end method

.method static synthetic r(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Ljava/lang/Long;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    return-object p0
.end method

.method static synthetic s(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ja:I

    return p0
.end method

.method static synthetic t(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ia:I

    return p0
.end method

.method static synthetic u(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->H:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;

    return-object p0
.end method

.method static synthetic v(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->v:I

    return p0
.end method

.method static synthetic w(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->z:Ljava/util/List;

    return-object p0
.end method

.method static synthetic x(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ba:F

    return p0
.end method

.method static synthetic y(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Landroid/graphics/Paint;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p:Landroid/graphics/Paint;

    return-object p0
.end method


# virtual methods
.method protected a(Ljava/lang/String;Landroid/graphics/Paint;)F
    .locals 0

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result p1

    return p1
.end method

.method protected a(I)I
    .locals 1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    return p1
.end method

.method public a()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->getCurrentIndex()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->getCurrentIndex()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ia:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->getCurrentIndex()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ja:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->getCurrentIndex()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ma:F

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->getCurrentIndex()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;-><init>(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->H:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;

    return-void
.end method

.method protected b(I)F
    .locals 1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    return p1
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->qa:Z

    return-void
.end method

.method public c()V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->G:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->M:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_1
    return-void
.end method

.method public c(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->qa:Z

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ra:I

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/f/h;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/d/f/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/f/h;->a(I)V

    :cond_1
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Na:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->va:I

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->wa:I

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(FII)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->za:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->va:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->za:I

    :goto_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->G:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Landroid/graphics/Canvas;)V

    :cond_1
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->d(Landroid/graphics/Canvas;)V

    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, 0x0

    if-ne v0, v2, :cond_0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    move p1, v3

    :goto_0
    if-ne v1, v2, :cond_1

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->m:I

    goto :goto_1

    :cond_1
    iget p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->u:I

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->m:I

    move p2, v3

    :goto_1
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->e:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->d:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingStart()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->f:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingEnd()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->g:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->m:I

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->e:I

    sub-int/2addr p1, p2

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->m:I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->kb:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 v2, 0x1

    if-eqz p1, :cond_3

    if-eq p1, v2, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->f()V

    goto :goto_0

    :cond_1
    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->qa:Z

    if-nez p1, :cond_7

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(F)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(F)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->f()V

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(FF)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_0

    :cond_4
    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    if-eqz p1, :cond_5

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    cmpg-float p1, v0, p1

    if-gtz p1, :cond_6

    goto :goto_0

    :cond_5
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    int-to-float p1, p1

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    sub-float/2addr p1, v3

    cmpl-float p1, v0, p1

    if-ltz p1, :cond_6

    goto :goto_0

    :cond_6
    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(FF)V

    :cond_7
    :goto_0
    return v2
.end method

.method public setXLabel(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->z:Ljava/util/List;

    return-void
.end method

.method public setYData(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    return-void
.end method

.method public setYLabel(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LineChart{TAG=\'LineChart\', mPaddingLeft="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mPaddingRight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mPaddingTop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mPaddingBottom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mPaddingStart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mPaddingEnd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", CIRCLE_SHADOW_Y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", tan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->i:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", pos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->j:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tempIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", brokenLinePaint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->n:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", YLabelPaint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->o:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", XLabelPaint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", straightLinePaint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", nowIndexPaint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->r:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mMiddleLineColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->s:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mNormalLineColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->t:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mDefaultHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->u:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", straightLineInterval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->v:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", maxValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", minValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->x:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", YLabel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", XLabel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->z:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", YData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->A:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", YLabelWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->B:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", XLabelStartHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->C:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", isRTL="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->D:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", YAverageHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->E:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", averageY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->F:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", shouldShowCircle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->G:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", changedCallBack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->H:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", XPointData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->I:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", YPointData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->J:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", infoPoints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->K:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pathForAnimation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->L:Landroid/graphics/Path;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", curveShowAnimator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->M:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", animValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->N:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", pathMeasure="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->O:Landroid/graphics/PathMeasure;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->P:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", dst="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Q:Landroid/graphics/Path;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->R:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", circleX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->S:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", circleY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->T:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", circleRadius="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->U:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", circleTouchRadius="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->V:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", circlePaint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->W:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->aa:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", XLabelItemWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ba:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", XWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ca:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->da:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", first_x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ea:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", isAnimationFinish="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->fa:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isInRightPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ga:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", valueAnimator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ha:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ia:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", lastIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ja:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", distance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ka:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", allLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->la:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", click_Down_x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ma:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", bottomLineBottom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->na:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", topEdge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->oa:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", textEgeg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->pa:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isInterceptSwitchIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->qa:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", mInterceptCurrentIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ra:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", Unit_y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->sa:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mXLabelTextColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ta:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mXLabelAvgModeTextColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ua:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mBrokenLineColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->va:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mBrokenLineAvgModeColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->wa:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mYLabelTextColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->xa:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", YStart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ya:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mCurrentDrawLineColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->za:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mBottomLineY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Aa:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTopLineY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ba:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTipRectColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ca:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mTipWidthFraction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Da:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mAvgTextBounds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ea:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mAvgTextLocationRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Fa:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mAvgTextStartX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ga:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mAvgTextEndX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ha:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mAvgTextHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ia:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mAvgTipRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ja:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mAvgTipLineStartPoint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ka:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mAvgTipLineEndPoint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->La:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mAvgLineY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ma:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTipAlphaFraction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Na:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", isShowingTipRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Oa:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", mTipTitle=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Pa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", mTipSummary=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Qa:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", mTipRectWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ra:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTipTitleTextSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Sa:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTipSummaryTextSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ta:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTipTextPaint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ua:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mTipRectPaint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Va:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mTipRectHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Wa:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTipPaddingLeft="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Xa:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTipPaddingTop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Ya:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTipRectCorner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->Za:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mTipTextLeftMargin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->_a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTipRectTop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ab:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTipTextStart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->bb:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mTipSummaryColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->cb:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mTipTitleColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->db:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mHandler="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->eb:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mAvgModeMiddleLineColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->fb:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mAvgModeLineColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->gb:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isShouldDrawAvgLine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->hb:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", mTipShowAnimator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->ib:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currentIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->jb:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", drag_x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->kb:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", infoPointsForZeroYData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->lb:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
