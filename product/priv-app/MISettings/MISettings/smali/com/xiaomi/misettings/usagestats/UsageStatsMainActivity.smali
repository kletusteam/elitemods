.class public Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;
.super Lcom/misettings/common/base/BaseActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$b;,
        Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$c;,
        Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$a;,
        Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$d;,
        Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$e;
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:[Ljava/lang/String;

.field private d:Z

.field private e:Landroidx/appcompat/app/ActionBar$c;

.field private f:Landroid/view/View;

.field private g:Landroid/view/ViewStub;

.field private h:Landroid/widget/Button;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Z

.field private m:Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$d;

.field private n:J

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a:Z

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->c:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->d:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->k:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->l:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->n:J

    return-void
.end method

.method static synthetic a(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->b(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/DialogInterface;)V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v1, "key_has_accredit"

    invoke-virtual {v0, v1}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Z)V

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    goto :goto_1

    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->g:Landroid/view/ViewStub;

    if-nez p1, :cond_3

    const p1, 0x7f0b0074

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewStub;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->g:Landroid/view/ViewStub;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->g:Landroid/view/ViewStub;

    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->f:Landroid/view/View;

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->f:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->f:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->f:Landroid/view/View;

    const v0, 0x7f0b0072

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->h:Landroid/widget/Button;

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->h:Landroid/widget/Button;

    invoke-static {p1}, Ld/h/a/c;->a(Landroid/view/View;)V

    :cond_5
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->h:Landroid/widget/Button;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/F;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/F;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    if-eqz p1, :cond_1

    const-string v0, "screen_time_home_intent_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->j:Ljava/lang/String;

    :cond_1
    :goto_0
    return-void
.end method

.method private a(Landroid/widget/ImageView;)V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->b:Z

    if-nez v0, :cond_0

    const v0, 0x7f08034b

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_0
    const v0, 0x7f080348

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f08017d

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->j()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Landroid/content/DialogInterface;)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/misettings/common/utils/m;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Landroidx/appcompat/app/ActionBar;->e(Z)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a:Z

    return p1
.end method

.method private static b(Landroid/content/Context;)V
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const/4 v0, 0x1

    const-string v1, "misettings_has_track_permission_event"

    invoke-static {p0, v1, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    if-eqz p1, :cond_1

    const-string v0, "misettings_from_page"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->o:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->o()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->d:Z

    return p1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->d:Z

    return p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->s()V

    return-void
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->f:Landroid/view/View;

    return-object p0
.end method

.method private f()V
    .locals 3

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Lmiuix/appcompat/app/d;->p()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/xiaomi/misettings/usagestats/G;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/G;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->b:Z

    if-eqz v2, :cond_2

    const v2, 0x7f080260

    goto :goto_0

    :cond_2
    const v2, 0x7f080261

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    const v2, 0x7f130044

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/d;->b(Landroid/view/View;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/d;->e(I)V

    const v1, 0x7f1303d4

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static synthetic f(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->j()V

    return-void
.end method

.method private g()V
    .locals 4

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->i:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->h()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->i()V

    :goto_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/g;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/g;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private h()V
    .locals 8

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v6

    if-nez v6, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/C;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/C;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->b:Z

    if-eqz v1, :cond_1

    const v1, 0x7f080260

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    const v1, 0x7f080261

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    const v1, 0x7f130044

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/misettings/common/base/BaseActivity;->isSplitStyleActivity()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v6, v0}, Lmiuix/appcompat/app/d;->b(Landroid/view/View;)V

    const/4 v0, 0x4

    invoke-virtual {v6, v0}, Landroidx/appcompat/app/ActionBar;->b(I)V

    :cond_2
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->o:Ljava/lang/String;

    const-string v1, "misettings_from_page"

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->j:Ljava/lang/String;

    const-string v1, "screen_time_home_intent_key"

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v6, p0, v0}, Lmiuix/appcompat/app/d;->a(Landroidx/fragment/app/FragmentActivity;Z)V

    invoke-virtual {v6}, Landroidx/appcompat/app/ActionBar;->l()Landroidx/appcompat/app/ActionBar$c;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->e:Landroidx/appcompat/app/ActionBar$c;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->c:[Ljava/lang/String;

    aget-object v2, v1, v0

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->e:Landroidx/appcompat/app/ActionBar$c;

    aget-object v0, v1, v0

    invoke-virtual {v3, v0}, Landroidx/appcompat/app/ActionBar$c;->a(Ljava/lang/CharSequence;)Landroidx/appcompat/app/ActionBar$c;

    const-class v5, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    const/4 v7, 0x0

    move-object v0, v6

    move-object v1, v2

    move-object v2, v3

    move-object v3, v5

    move v5, v7

    invoke-virtual/range {v0 .. v5}, Lmiuix/appcompat/app/d;->a(Ljava/lang/String;Landroidx/appcompat/app/ActionBar$c;Ljava/lang/Class;Landroid/os/Bundle;Z)I

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    const/4 v7, 0x1

    if-lt v0, v1, :cond_4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->c:[Ljava/lang/String;

    aget-object v1, v0, v7

    invoke-virtual {v6}, Landroidx/appcompat/app/ActionBar;->l()Landroidx/appcompat/app/ActionBar$c;

    move-result-object v2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->c:[Ljava/lang/String;

    aget-object v0, v0, v7

    invoke-virtual {v2, v0}, Landroidx/appcompat/app/ActionBar$c;->a(Ljava/lang/CharSequence;)Landroidx/appcompat/app/ActionBar$c;

    const-class v3, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    invoke-virtual/range {v0 .. v5}, Lmiuix/appcompat/app/d;->a(Ljava/lang/String;Landroidx/appcompat/app/ActionBar$c;Ljava/lang/Class;Landroid/os/Bundle;Z)I

    goto :goto_1

    :cond_4
    const v0, 0x7f1303d4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(I)V

    :goto_1
    new-instance v0, Lcom/xiaomi/misettings/usagestats/D;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/D;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V

    invoke-virtual {v6, v0}, Lmiuix/appcompat/app/d;->a(Lmiuix/appcompat/app/d$a;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->j:Ljava/lang/String;

    const-string v1, "focus_mode"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v6, v7}, Landroidx/appcompat/app/ActionBar;->c(I)V

    :cond_5
    return-void
.end method

.method private i()V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;-><init>()V

    const v2, 0x7f0b019d

    invoke-virtual {v0, v2, v1}, Landroidx/fragment/app/qa;->a(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {v0}, Landroidx/fragment/app/qa;->a()I

    return-void
.end method

.method private init()V
    .locals 3

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/a/p;->a()Lcom/xiaomi/misettings/usagestats/b/a/p;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/p;->b(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/widget/desktop/service/UsageStatsUpdateService;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getSourceBounds()Landroid/graphics/Rect;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "from_short_cut"

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->o:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(I)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Landroid/content/Intent;)V

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$b;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private j()V
    .locals 5

    const-string v0, "Timer-UsageStatsMainActivity"

    :try_start_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Landroid/widget/ImageView;)V

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f130250

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const v3, 0x7f130418

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    new-instance v3, Lcom/xiaomi/misettings/usagestats/i;

    invoke-direct {v3, p0, v2}, Lcom/xiaomi/misettings/usagestats/i;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;Landroid/widget/ImageView;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a:Z

    if-eqz v3, :cond_1

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/d;->a(Landroid/view/View;)V

    goto :goto_1

    :cond_1
    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->d:Z

    if-eqz v3, :cond_2

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/d;->a(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/d;->a(Landroid/view/View;)V

    :goto_1
    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->i:Z

    if-nez v3, :cond_3

    const/16 v2, 0xc

    :cond_3
    invoke-virtual {v1, v2}, Landroidx/appcompat/app/ActionBar;->b(I)V

    goto :goto_2

    :cond_4
    const-string v1, "configActionBar: null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "configActionBar error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return-void
.end method

.method private k()V
    .locals 4

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v0

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v1

    const-string v2, "default_category"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;Z)Z

    move-result v1

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->k:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->l:Z

    if-ne v1, v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->n:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x4e20

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/widget/desktop/service/UsageStatsUpdateService;->b(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->n:J

    :cond_1
    return-void
.end method

.method private l()V
    .locals 0

    return-void
.end method

.method private m()V
    .locals 0

    return-void
.end method

.method private n()V
    .locals 3

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v1, "default_category"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->k:Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->l:Z

    return-void
.end method

.method private o()V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$c;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$c;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private p()V
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v0, 0x7f1303d4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(I)V

    return-void
.end method

.method private q()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroidx/appcompat/app/ActionBar;->o()V

    const v1, 0x7f1303d4

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->a(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private r()V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/K;->a(Landroid/app/Activity;)V

    return-void
.end method

.method private s()V
    .locals 4

    const-string v0, "misettings_from_page"

    const-string v1, "Timer-UsageStatsMainActivity"

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->b(Landroid/content/Intent;)V

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->o:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v2, :cond_1

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->o:Ljava/lang/String;

    goto :goto_1

    :cond_1
    :goto_0
    const-string v0, "from_page_settings"

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->o:Ljava/lang/String;

    :cond_2
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "trackFromPage: fromPage="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "trackFromPage error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v1, "key_has_accredit"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Z)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->d()V

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v1

    const-string v3, "pref_key_accredit_time"

    invoke-virtual {v0, v3, v1, v2}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;J)V

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v1, "has_home_premission"

    invoke-virtual {v0, v1}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v3, "PREF_KEY_FIRST_USE_TIME"

    invoke-virtual {v0, v3, v1, v2}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;J)V

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->b(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/widget/desktop/service/UsageStatsUpdateService;->b(Landroid/content/Context;)V

    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(ZLandroid/os/Bundle;)V

    return-void
.end method

.method public synthetic a(Landroid/widget/ImageView;Landroid/view/View;)V
    .locals 0

    iget-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a:Z

    if-eqz p2, :cond_0

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/app/AppCompatActivity;->showImmersionMenu(Landroid/view/View;Landroid/view/ViewGroup;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/misettings/common/base/a;

    invoke-direct {p1, p0}, Lcom/misettings/common/base/a;-><init>(Landroid/content/Context;)V

    const-class p2, Lcom/xiaomi/misettings/usagestats/home/ui/NewSubSettings;

    invoke-virtual {p1, p2}, Lcom/misettings/common/base/a;->a(Ljava/lang/Class;)Lcom/misettings/common/base/a;

    const-string p2, "com.xiaomi.misettings.usagestats.focusmode.FocusRecordsFragment"

    invoke-virtual {p1, p2}, Lcom/misettings/common/base/a;->a(Ljava/lang/String;)Lcom/misettings/common/base/a;

    const p2, 0x7f130418

    invoke-virtual {p1, p2}, Lcom/misettings/common/base/a;->b(I)Lcom/misettings/common/base/a;

    invoke-virtual {p1}, Lcom/misettings/common/base/a;->b()V

    :goto_0
    return-void
.end method

.method protected a(ZLandroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v1, "key_has_accredit"

    invoke-virtual {v0, v1}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Z)V

    if-nez v0, :cond_3

    invoke-static {p0}, Lcom/misettings/common/utils/m;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->f()V

    :cond_0
    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Landroid/content/DialogInterface;)V

    return-void

    :cond_1
    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Landroid/content/DialogInterface;)V

    return-void

    :cond_2
    if-nez p2, :cond_5

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->r()V

    goto :goto_0

    :cond_3
    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->i:Z

    if-eqz p1, :cond_4

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Looper;->getQueue()Landroid/os/MessageQueue;

    move-result-object p1

    new-instance p2, Lcom/xiaomi/misettings/usagestats/E;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/usagestats/E;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V

    invoke-virtual {p1, p2}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    :cond_4
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->g()V

    :cond_5
    :goto_0
    return-void
.end method

.method public b()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.xiaomi.misettings.ILoadAppNameInterface"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->m:Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$d;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method public c()V
    .locals 3

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$a;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public d()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->init()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->c()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->g()V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->m:Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$d;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p3    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    new-instance p3, Lcom/xiaomi/misettings/usagestats/H;

    invoke-direct {p3, p0}, Lcom/xiaomi/misettings/usagestats/H;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V

    invoke-static {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/utils/K;->a(Landroid/app/Activity;IILcom/xiaomi/misettings/usagestats/utils/K$a;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x1c

    if-lt v0, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->i:Z

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$d;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->m:Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$d;

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    const v3, 0x7f1303d4

    if-nez v0, :cond_1

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_1
    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0e0022

    goto :goto_1

    :cond_2
    const v0, 0x7f0e0023

    :goto_1
    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, Lcom/misettings/common/utils/m;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/appcompat/app/d;->e(I)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroidx/appcompat/app/ActionBar;->d(Z)V

    :cond_4
    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroidx/appcompat/app/ActionBar;->b(I)V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroidx/appcompat/app/ActionBar;->a(Ljava/lang/CharSequence;)V

    :cond_5
    const v0, 0x7f0b0041

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_6

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_6
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->i:Z

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->q()V

    const v0, 0x7f0e0021

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    goto :goto_2

    :cond_7
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->p()V

    :goto_2
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v4, "key_has_accredit"

    invoke-virtual {v0, v4}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->init()V

    :cond_8
    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/misettings/f;->c(Landroid/content/Context;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->b:Z

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->c:[Ljava/lang/String;

    iget-boolean v5, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->i:Z

    if-eqz v5, :cond_9

    const v5, 0x7f13039c

    invoke-virtual {p0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_9
    const-string v5, ""

    :goto_3
    aput-object v5, v4, v2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->c:[Ljava/lang/String;

    const v4, 0x7f1303f3

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->i:Z

    if-nez v1, :cond_a

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_a
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->b()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/l;->a()Lcom/xiaomi/misettings/usagestats/utils/l;

    move-result-object p1

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/xiaomi/misettings/usagestats/utils/l;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/display/RefreshRate/f;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/display/RefreshRate/f;

    move-result-object p1

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/xiaomi/misettings/display/RefreshRate/f;->a(Landroid/content/Context;)V

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->c()V

    :cond_b
    invoke-virtual {p0}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result p1

    if-eqz p1, :cond_c

    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/xiaomi/misettings/usagestats/ExitMultiWindowActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_c
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0f0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, 0x7f0b0278

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_1
    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0b027b

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/J;->a()Lcom/xiaomi/misettings/usagestats/utils/J;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/J;->b()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->e()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4

    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    const-string v0, "Timer-UsageStatsMainActivity"

    if-eqz p1, :cond_0

    :try_start_0
    const-string v1, "screen_time_home_intent_key"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "focus_mode"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/ActionBar;->c(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const-string v1, "onNewIntent: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_5

    const-string v0, "misettings_from_page"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Landroid/content/Intent;->getSourceBounds()Landroid/graphics/Rect;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->o:Ljava/lang/String;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->o:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->s()V

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->o:Ljava/lang/String;

    const-string v0, "fromSteadyOn"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->e:Landroidx/appcompat/app/ActionBar$c;

    invoke-virtual {p1, v0}, Landroidx/appcompat/app/ActionBar;->a(Landroidx/appcompat/app/ActionBar$c;)V

    :cond_3
    invoke-static {p0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object p1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "misettings.action.FROM_STEADY_ON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, La/m/a/b;->a(Landroid/content/Intent;)Z

    :cond_4
    return-void

    :cond_5
    :goto_1
    invoke-static {p0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object p1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "misettings.action.NOTIFY_TODAY_DATA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, La/m/a/b;->a(Landroid/content/Intent;)Z

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/f/g;->a(Landroid/content/Context;Landroid/view/MenuItem;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->l()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->k()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onResume()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->m()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->n()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStop()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b()V

    :cond_0
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    return-void
.end method
