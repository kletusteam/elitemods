.class public Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;
.super Lcom/xiaomi/misettings/usagestats/a/e;


# static fields
.field private static c:Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/a/e;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;

    return-object p0
.end method

.method public static d()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Landroid/database/sqlite/SQLiteOpenHelper;
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/a/a;

    invoke-direct {v0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/a;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected a()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceapplimit"

    return-object v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "packageName"

    invoke-virtual {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/a/e;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 2

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    new-array p2, v1, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, p2, v0

    const-string p1, "packageName=?"

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/a/e;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_0
    return v1

    :cond_1
    new-instance p2, Landroid/content/ContentValues;

    invoke-direct {p2}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "isCloseLimit"

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "packageName"

    invoke-virtual {p2, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/usagestats/a/e;->a(Landroid/content/ContentValues;)Z

    move-result p1

    return p1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "packageName"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const-string v3, "isCloseLimit=?"

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v3, v2, v4}, Lcom/xiaomi/misettings/usagestats/a/e;->a([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_1
    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/a/e;->a(Landroid/database/Cursor;)V

    return-object v0
.end method
