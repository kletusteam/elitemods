.class Lcom/xiaomi/misettings/usagestats/home/ui/H;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->t()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/H;->a:Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string p1, "SubContentFragment"

    const-string v0, "onReceive: "

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, ":key:notify_channel"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    const-string v0, "misettings.action.EXCHANGE_DEVICE_LIMIT"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/H;->a:Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    iget-boolean p2, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    if-eqz p2, :cond_8

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->g()V

    goto/16 :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/H;->a:Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    iget-boolean p2, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    if-nez p2, :cond_8

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->g()V

    goto/16 :goto_0

    :cond_1
    const-string v0, "misettings.action.EXCHANGE_STEADY_ON"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/H;->a:Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    iget-boolean p2, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    if-eqz p2, :cond_8

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->h()V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/H;->a:Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    iget-boolean p2, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    if-nez p2, :cond_8

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->h()V

    goto :goto_0

    :cond_3
    const-string v0, "misettings.action.EXCHANGE_DETAIL_LIST"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz p2, :cond_4

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/H;->a:Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    iget-boolean p2, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    if-eqz p2, :cond_8

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->f()V

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/H;->a:Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    iget-boolean p2, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    if-nez p2, :cond_8

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->f()V

    goto :goto_0

    :cond_5
    const-string p2, "misettings.action.NOTIFY_TODAY_DATA"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_6

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/H;->a:Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->d(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V

    goto :goto_0

    :cond_6
    const-string p2, "misettings.action.FORCE_NOTIFY_DATA"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_7

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/H;->a:Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->l()V

    goto :goto_0

    :cond_7
    const-string p2, "miui.token.change"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/H;->a:Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    :cond_8
    :goto_0
    return-void
.end method
