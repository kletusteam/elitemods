.class public Lcom/xiaomi/misettings/usagestats/utils/F;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/xiaomi/misettings/usagestats/utils/F;


# instance fields
.field private b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/F;
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/F;->a:Lcom/xiaomi/misettings/usagestats/utils/F;

    if-nez v0, :cond_1

    const-class v0, Lcom/xiaomi/misettings/usagestats/utils/F;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/F;->a:Lcom/xiaomi/misettings/usagestats/utils/F;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/utils/F;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/utils/F;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/utils/F;->a:Lcom/xiaomi/misettings/usagestats/utils/F;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/utils/F;->a:Lcom/xiaomi/misettings/usagestats/utils/F;

    return-object p0
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/F;->b(Ljava/io/File;)V

    return-void
.end method

.method private a(Ljava/io/File;)Z
    .locals 2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result p1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method private b(Landroid/content/Context;)V
    .locals 1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private b(Ljava/io/File;)V
    .locals 5

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/io/File;)Z

    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeDir: path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",files="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ClearCacheUtils"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_4

    aget-object v3, v0, v2

    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/usagestats/utils/F;->b(Ljava/io/File;)V

    goto :goto_1

    :cond_3
    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/io/File;)Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/io/File;)Z

    :cond_5
    :goto_2
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const-string v0, "ClearCacheUtils"

    const-string v1, "clearLocalData: start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->a()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->b()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/a/d;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/a/d;->c()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/a/o;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/a/o;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/d/c;->b()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/g/a;->a(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/i/h;->d(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v1

    invoke-virtual {v1}, Lcom/misettings/common/utils/p;->a()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v1

    invoke-virtual {v1}, Lb/e/a/b/h;->a()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->k(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/U;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/U;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/U;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/utils/F;->b(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/misettings/common/utils/g;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/utils/F;->b(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->b()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/a/d;->d()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/a/o;->d()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->d()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;->c()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->d()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->o(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->i(Landroid/content/Context;)V

    const-string v1, "clearLocalData: end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/U;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/U;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->d(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->k(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/F;->b:Landroid/content/Context;

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    invoke-virtual {v0}, Lb/e/a/b/h;->a()V

    return-void
.end method
