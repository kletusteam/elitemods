.class public Lcom/xiaomi/misettings/usagestats/home/category/database/h;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/xiaomi/misettings/usagestats/home/category/database/h;


# instance fields
.field private b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/Application;->c()Landroid/content/Context;

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-class v0, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase;

    const-string v1, "users_category"

    invoke-static {p1, v0, v1}, Landroidx/room/u;->a(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;)Landroidx/room/v$a;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/room/v$a;->a()Landroidx/room/v$a;

    invoke-virtual {p1}, Landroidx/room/v$a;->b()Landroidx/room/v;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "init database error"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ClassifyManagerUtils"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase;

    :goto_0
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/home/category/database/h;
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/home/category/database/h;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->a:Lcom/xiaomi/misettings/usagestats/home/category/database/h;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/category/database/h;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/category/database/h;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->a:Lcom/xiaomi/misettings/usagestats/home/category/database/h;

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->a:Lcom/xiaomi/misettings/usagestats/home/category/database/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static a()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->a:Lcom/xiaomi/misettings/usagestats/home/category/database/h;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase;

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase;->l()Lcom/xiaomi/misettings/usagestats/home/category/database/b;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/xiaomi/misettings/usagestats/home/category/database/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const-string p1, "ClassifyManagerUtils"

    const-string v1, "getCategoryIdByPackageName error"

    invoke-static {p1, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method public varargs a([Lcom/xiaomi/misettings/usagestats/home/category/database/a;)[Ljava/lang/Long;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase;

    if-nez v1, :cond_0

    new-array p1, v0, [Ljava/lang/Long;

    return-object p1

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase;->l()Lcom/xiaomi/misettings/usagestats/home/category/database/b;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/xiaomi/misettings/usagestats/home/category/database/b;->a([Lcom/xiaomi/misettings/usagestats/home/category/database/a;)[Ljava/lang/Long;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    const-string v1, "ClassifyManagerUtils"

    const-string v2, "insertCategoryEntity error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    new-array p1, v0, [Ljava/lang/Long;

    return-object p1
.end method
