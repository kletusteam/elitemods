.class public Lcom/xiaomi/misettings/usagestats/utils/K;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/utils/K$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/app/Activity;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.SYSTEM_PERMISSION_DECLARE_NEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v1, 0x7f130369

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "all_purpose"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, "mandatory_permission"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const v3, 0x7f1303c8

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v1, "runtime_perm_desc"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0xc8

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public static a(Landroid/app/Activity;IILcom/xiaomi/misettings/usagestats/utils/K$a;)V
    .locals 1

    const/16 v0, 0xc8

    if-ne p1, v0, :cond_3

    const/4 p1, -0x3

    if-eq p2, p1, :cond_2

    const/16 p1, 0x29a

    if-eq p2, p1, :cond_1

    if-eqz p2, :cond_2

    const/4 p0, 0x1

    if-eq p2, p0, :cond_0

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_3

    invoke-interface {p3}, Lcom/xiaomi/misettings/usagestats/utils/K$a;->onSuccess()V

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_3

    invoke-interface {p3}, Lcom/xiaomi/misettings/usagestats/utils/K$a;->a()V

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/K;->a(Landroid/app/Activity;)V

    :cond_3
    :goto_0
    return-void
.end method
