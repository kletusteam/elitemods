.class public Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;
.super Ljava/lang/Object;


# instance fields
.field private addUpCount:J

.field private addUpDays:I

.field private addUpTime:I

.field private runningDays:I


# direct methods
.method public constructor <init>(IJII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->addUpTime:I

    iput-wide p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->addUpCount:J

    iput p4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->addUpDays:I

    iput p5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->runningDays:I

    return-void
.end method


# virtual methods
.method public getAddUpCount()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->addUpCount:J

    return-wide v0
.end method

.method public getAddUpDays()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->addUpDays:I

    return v0
.end method

.method public getAddUpTime()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->addUpTime:I

    return v0
.end method

.method public getRunningDays()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->runningDays:I

    return v0
.end method

.method public setAddUpCount(J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->addUpCount:J

    return-void
.end method

.method public setAddUpDays(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->addUpDays:I

    return-void
.end method

.method public setAddUpTime(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->addUpTime:I

    return-void
.end method

.method public setRunningDays(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->runningDays:I

    return-void
.end method
