.class Lcom/xiaomi/misettings/usagestats/focusmode/B;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->t()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/B;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const-string v0, "FocusSettingsFragment"

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/B;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/B;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->g(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/B;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    iget-object v2, v2, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->z:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/B;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->h(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/B;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {v4}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->i(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/B;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {v5}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->g(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v5

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/focusmode/B;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {v6}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->h(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    iget v7, v6, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v8, p0, Lcom/xiaomi/misettings/usagestats/focusmode/B;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {v8}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->i(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/TextView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout$LayoutParams;

    iget v8, v8, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v3

    sub-int/2addr v1, v4

    sub-int/2addr v1, v5

    sub-int/2addr v1, v7

    sub-int/2addr v1, v8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bottom"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-gtz v1, :cond_1

    div-int/lit8 v7, v7, 0x2

    iput v7, v6, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/B;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->g(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    div-int/lit8 v5, v5, 0x2

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v5, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "fitSmallDevice: "

    invoke-static {v0, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    :goto_0
    return-void
.end method
