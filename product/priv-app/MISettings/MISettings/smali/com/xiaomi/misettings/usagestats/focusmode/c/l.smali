.class final Lcom/xiaomi/misettings/usagestats/focusmode/c/l;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/focusmode/c/q$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/xiaomi/misettings/usagestats/focusmode/c/q$a;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/focusmode/c/q$a;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/l;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/l;->b:Lcom/xiaomi/misettings/usagestats/focusmode/c/q$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const-string v0, "FocusModeUtils"

    const-string v1, "forceStopFocusMode: finish focus mode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/l;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->D(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/l;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/l;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "misettings_key_has_finish_focus"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/l;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "settings_focus_mode_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/l;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->c(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/l;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->e(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/l;->b:Lcom/xiaomi/misettings/usagestats/focusmode/c/q$a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q$a;->call()V

    :cond_0
    return-void
.end method
