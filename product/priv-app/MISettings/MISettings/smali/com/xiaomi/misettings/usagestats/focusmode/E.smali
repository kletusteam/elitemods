.class Lcom/xiaomi/misettings/usagestats/focusmode/E;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->B()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/E;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/E;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    new-instance p2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/E;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/port/FocusModeTimingPortActivity;

    invoke-direct {p2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/E;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-virtual {v0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/land/FocusModeTimingLandActivity;

    invoke-direct {p2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/E;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->j(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)I

    move-result v0

    const-string v1, "keyFocusModeTimeIndex"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v0, 0x8

    invoke-static {p2, v0}, Lcom/misettings/common/utils/n;->a(Landroid/content/Intent;I)V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p2, v0}, Lcom/misettings/common/utils/m;->a(Landroid/content/Intent;I)V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/E;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    if-eqz p1, :cond_2

    const p2, 0x7f010014

    const v0, 0x7f010015

    invoke-virtual {p1, p2, v0}, Landroid/app/Activity;->overridePendingTransition(II)V

    :cond_2
    return-void
.end method
