.class Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->j()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->i(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->m(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->n(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Landroid/graphics/PathMeasure;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->j(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->m(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Landroid/graphics/Path;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/graphics/PathMeasure;->getSegment(FFLandroid/graphics/Path;Z)Z

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    float-to-double v0, p1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double p1, v0, v4

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iput-boolean v3, p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->G:Z

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/d;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    return-void
.end method
