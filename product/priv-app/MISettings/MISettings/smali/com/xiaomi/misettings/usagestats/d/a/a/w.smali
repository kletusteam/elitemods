.class Lcom/xiaomi/misettings/usagestats/d/a/a/w;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/d/a/a/y;->a(Ljava/util/List;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/f/d;

.field final synthetic b:Lcom/xiaomi/misettings/usagestats/d/a/a/y;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/d/a/a/y;Lcom/xiaomi/misettings/usagestats/f/d;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/w;->b:Lcom/xiaomi/misettings/usagestats/d/a/a/y;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/w;->a:Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/w;->b:Lcom/xiaomi/misettings/usagestats/d/a/a/y;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->a(Lcom/xiaomi/misettings/usagestats/d/a/a/y;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/w;->b:Lcom/xiaomi/misettings/usagestats/d/a/a/y;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->b(Lcom/xiaomi/misettings/usagestats/d/a/a/y;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/d/i;->a()Lcom/miui/greenguard/entity/DashBordBean;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean;->getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/FamilyBean;->getUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean;->getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/FamilyBean;->getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/w;->a:Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean;->getDateType()I

    move-result v5

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean;->getDate()I

    move-result v6

    invoke-static/range {v1 .. v6}, Lcom/miui/greenguard/manager/ExtraRouteManager;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    return-void

    :cond_0
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const/4 v0, 0x0

    const-string v1, "isWeek"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/w;->a:Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "packageName"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    const-string v1, "hasTime"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/w;->a:Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/w;->a:Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/f/d;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v1, "usageList"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->m:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v0

    iget-wide v0, v0, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    const-string v2, "dayBeginTime"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/w;->b:Lcom/xiaomi/misettings/usagestats/d/a/a/y;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    return-void
.end method
