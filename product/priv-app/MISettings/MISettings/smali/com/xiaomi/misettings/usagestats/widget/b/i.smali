.class public abstract Lcom/xiaomi/misettings/usagestats/widget/b/i;
.super Lcom/xiaomi/misettings/usagestats/widget/b/g;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/widget/b/n;


# instance fields
.field private ba:I

.field private ca:I

.field protected da:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;"
        }
    .end annotation
.end field

.field protected ea:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private h()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ca:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/i;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ca:I

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/i;->a(Lcom/xiaomi/misettings/usagestats/f/i;)I

    move-result v1

    add-int/2addr v2, v1

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ca:I

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/xiaomi/misettings/usagestats/f/i;)I
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ea:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ea:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ea:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ea:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ea:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ea:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ba:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ea:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ba:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v1, v0

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ba:I

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected b()I
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ea:Ljava/util/List;

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected b(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method protected c()J
    .locals 4

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/i;->a(Lcom/xiaomi/misettings/usagestats/f/i;)I

    move-result v3

    if-le v3, v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/i;->a(Lcom/xiaomi/misettings/usagestats/f/i;)I

    move-result v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ea:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ge v1, v3, :cond_2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1

    :cond_3
    int-to-long v0, v1

    return-wide v0
.end method

.method protected c(I)Ljava/lang/String;
    .locals 5

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ne p1, v1, :cond_1

    const p1, 0x7f13041c

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/i;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->b:Landroid/util/SparseIntArray;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/i;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/f/j;->b:I

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, v2

    :goto_1
    if-ne p1, v0, :cond_5

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, p1

    goto :goto_2

    :cond_4
    add-int/lit8 v0, p1, 0x1

    :goto_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v3, 0x7f110029

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-virtual {p1, v3, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_5
    rem-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, v2

    if-ne p1, v0, :cond_6

    goto :goto_3

    :cond_6
    const-string p1, ""

    return-object p1

    :cond_7
    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, p1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    :cond_8
    add-int/2addr p1, v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    :goto_4
    return-object p1
.end method

.method protected d()F
    .locals 1

    const v0, 0x7f0704da

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    return v0
.end method

.method protected d(I)F
    .locals 4

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/i;->a(Lcom/xiaomi/misettings/usagestats/f/i;)I

    move-result p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->ea:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :goto_0
    if-nez p1, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    add-int/lit8 p1, p1, 0x64

    int-to-float p1, p1

    return p1

    :cond_1
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->E:F

    sub-float/2addr v0, v1

    const/high16 v2, 0x3f800000    # 1.0f

    int-to-float p1, p1

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->N:F

    div-float/2addr p1, v3

    sub-float/2addr v2, p1

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public d(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/i;->da:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/i;->h()V

    :cond_2
    return-void
.end method

.method protected e()F
    .locals 1

    const v0, 0x7f0704f8

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    return v0
.end method

.method protected f(I)Landroid/graphics/Paint$Align;
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    sget-object p1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    return-object p1

    :cond_0
    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f(I)Landroid/graphics/Paint$Align;

    move-result-object p1

    return-object p1
.end method

.method public f()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f()V

    return-void
.end method

.method protected g(I)I
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ne p1, v0, :cond_1

    const p1, 0x7f060373

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result p1

    return p1

    :cond_1
    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g(I)I

    move-result p1

    return p1
.end method
