.class public Lcom/xiaomi/misettings/usagestats/b/a/i;
.super Lb/c/b/a/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/b/a/i$a;
    }
.end annotation


# static fields
.field private static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private volatile b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/b/a/i$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/b/a/h;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/b/a/h;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/b/a/i;->a:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lb/c/b/a/a;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->c:Ljava/util/List;

    return-void
.end method

.method private a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1, p2, p3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->c:Ljava/util/List;

    invoke-interface {p1, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private b(Lorg/json/JSONObject;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_photo"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_photo"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_game"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_game"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_reading"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_reading"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_medicine"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_medicine"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_tools"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_tools"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_life"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_life"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_sport"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_sport"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_travel"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_travel"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_education"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_education"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_financial"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_financial"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_productivity"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_productivity"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_social"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_social"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_undefined"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_undefined"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_entainment"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_entainment"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_system"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_system"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_shopping"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_shopping"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_video_etc"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_video_etc"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, "category_news"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    const-string v1, "usagestats_app_category_miui_news"

    invoke-direct {p0, v0, v1, p1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v2

    :cond_2
    return-object v1
.end method

.method public a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->d:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_1

    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/b/a/i;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/b/a/i$a;

    sget-object v3, Lcom/xiaomi/misettings/usagestats/b/a/i;->a:Landroid/util/SparseArray;

    iget v4, v2, Lcom/xiaomi/misettings/usagestats/b/a/i$a;->b:I

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ","

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v2, Lcom/xiaomi/misettings/usagestats/b/a/i$a;->a:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :try_start_0
    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "dateToJson: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CategoryNetData"

    invoke-static {v4, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    return-object v0

    :cond_2
    :goto_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized a(Lorg/json/JSONObject;)V
    .locals 0

    monitor-enter p0

    if-nez p1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/i;->b(Lorg/json/JSONObject;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
