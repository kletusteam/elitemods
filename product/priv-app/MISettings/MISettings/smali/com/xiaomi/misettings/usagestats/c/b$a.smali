.class public Lcom/xiaomi/misettings/usagestats/c/b$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/c/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/xiaomi/misettings/usagestats/c/b$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Ljava/lang/CharSequence;

.field private d:J

.field private e:J

.field private f:Z

.field private g:Ljava/lang/CharSequence;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;JJLjava/lang/CharSequence;)V
    .locals 9

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/xiaomi/misettings/usagestats/c/b$a;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;JJLjava/lang/CharSequence;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;JJLjava/lang/CharSequence;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->h:I

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->b:Landroid/graphics/drawable/Drawable;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->c:Ljava/lang/CharSequence;

    iput-wide p3, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->d:J

    iput-wide p5, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->e:J

    iput-boolean p8, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->f:Z

    iput-object p7, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->g:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;JJLjava/lang/CharSequence;ZLjava/lang/String;)V
    .locals 9

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/xiaomi/misettings/usagestats/c/b$a;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;JJLjava/lang/CharSequence;Z)V

    move-object/from16 v1, p9

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/c/b$a;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/usagestats/c/b$a;)I
    .locals 2

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/c/b$a;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result p1

    return p1
.end method

.method public a()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->h:I

    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->g:Ljava/lang/CharSequence;

    return-void
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->e:J

    return-wide v0
.end method

.method public c()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->a(Lcom/xiaomi/misettings/usagestats/c/b$a;)I

    move-result p1

    return p1
.end method

.method public d()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->h:I

    return v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/c/b$a;->f:Z

    return v0
.end method
