.class public Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;
.super Lcom/misettings/common/base/BaseActivity;


# instance fields
.field private a:Z

.field private b:[Ljava/lang/String;

.field public c:Lmiuix/viewpager/widget/ViewPager;

.field private d:Landroidx/appcompat/app/ActionBar$c;

.field private e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

.field private f:Lcom/xiaomi/misettings/display/RefreshRate/l;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->b:[Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->h:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->h:Ljava/util/List;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->i:Landroid/content/Context;

    return-object p0
.end method

.method private d()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e()V

    return-void
.end method

.method private e()V
    .locals 7

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/xiaomi/misettings/display/RefreshRate/q;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/display/RefreshRate/q;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->a:Z

    if-eqz v2, :cond_1

    sget v2, Lcom/xiaomi/misettings/display/i;->miuix_appcompat_action_bar_back_dark:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v2, Lcom/xiaomi/misettings/display/l;->back:I

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    sget v2, Lcom/xiaomi/misettings/display/i;->miuix_appcompat_action_bar_back_light:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v2, Lcom/xiaomi/misettings/display/l;->back:I

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/d;->b(Landroid/view/View;)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->b(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lmiuix/appcompat/app/d;->a(Landroidx/fragment/app/FragmentActivity;Z)V

    invoke-virtual {v0}, Landroidx/appcompat/app/ActionBar;->l()Landroidx/appcompat/app/ActionBar$c;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->d:Landroidx/appcompat/app/ActionBar$c;

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->b:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v0}, Landroidx/appcompat/app/ActionBar;->l()Landroidx/appcompat/app/ActionBar$c;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->b:[Ljava/lang/String;

    aget-object v1, v4, v1

    invoke-virtual {v3, v1}, Landroidx/appcompat/app/ActionBar$c;->a(Ljava/lang/CharSequence;)Landroidx/appcompat/app/ActionBar$c;

    const-class v4, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    invoke-virtual/range {v0 .. v5}, Lmiuix/appcompat/app/d;->a(Ljava/lang/String;Landroidx/appcompat/app/ActionBar$c;Ljava/lang/Class;Landroid/os/Bundle;Z)I

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v1, Ld/b/g;->view_pager:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lmiuix/viewpager/widget/ViewPager;

    if-eqz v1, :cond_2

    check-cast v0, Lmiuix/viewpager/widget/ViewPager;

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->c:Lmiuix/viewpager/widget/ViewPager;

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    return-void
.end method


# virtual methods
.method public a()Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;
    .locals 5

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v1

    const-string v2, "AppCateSearchFragment"

    invoke-virtual {v0, v2}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    iput-object v3, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    iget-object v3, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    if-nez v3, :cond_1

    new-instance v3, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    invoke-direct {v3}, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;-><init>()V

    iput-object v3, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    iget-object v3, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->g:Ljava/util/List;

    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    iget-object v4, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->g:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->a(Ljava/util/List;)V

    :cond_0
    const v3, 0x1020002

    iget-object v4, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    invoke-virtual {v1, v3, v4, v2}, Landroidx/fragment/app/qa;->a(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/qa;

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    invoke-virtual {v1, v2}, Landroidx/fragment/app/qa;->e(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    :cond_1
    invoke-virtual {v1}, Landroidx/fragment/app/qa;->b()I

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->p()Z

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->g:Ljava/util/List;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, " Order 1 and thread id is "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, " Split screen "

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public a(Z)V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v1

    const-string v2, "AppCateSearchFragment"

    invoke-virtual {v0, v2}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    iput-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    if-eqz v2, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {v1, v2}, Landroidx/fragment/app/qa;->e(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v2}, Landroidx/fragment/app/qa;->c(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    :goto_0
    invoke-virtual {v1}, Landroidx/fragment/app/qa;->b()I

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->p()Z

    :cond_1
    return-void
.end method

.method public b()Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    return-object v0
.end method

.method public b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->h:Ljava/util/List;

    return-void
.end method

.method public c()V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "AppCateSearchFragment"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    iput-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->e:Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    invoke-virtual {v1, v2}, Landroidx/fragment/app/qa;->d(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {v1}, Landroidx/fragment/app/qa;->b()I

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->p()Z

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/display/RefreshRate/e;->c(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->a:Z

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->b:[Ljava/lang/String;

    sget v0, Lcom/xiaomi/misettings/display/l;->new_customize_high_refresh_title:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    new-instance p1, Lcom/xiaomi/misettings/display/RefreshRate/l;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/display/RefreshRate/l;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->f:Lcom/xiaomi/misettings/display/RefreshRate/l;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->i:Landroid/content/Context;

    if-nez p1, :cond_0

    iput-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->i:Landroid/content/Context;

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->d()V

    return-void
.end method
