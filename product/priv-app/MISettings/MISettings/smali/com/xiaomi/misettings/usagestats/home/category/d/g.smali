.class public Lcom/xiaomi/misettings/usagestats/home/category/d/g;
.super Ljava/lang/Object;


# direct methods
.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/category/c/c;Lcom/xiaomi/misettings/usagestats/home/category/c/c;)I
    .locals 2

    iget-wide v0, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->e:J

    iget-wide p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->e:J

    sub-long/2addr v0, p0

    long-to-int p0, v0

    return p0
.end method

.method private static a(Lcom/xiaomi/misettings/usagestats/f/o;Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Lb/c/a/a/a;
    .locals 13

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/home/category/c/e;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->a:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/o;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/o;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/o;->e()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->h:J

    iget-wide v1, p1, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->d:J

    iget-wide v1, p1, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->e:J

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/o;->d()Ljava/util/ArrayList;

    move-result-object p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/h;

    if-nez v3, :cond_0

    new-instance v3, Lcom/xiaomi/misettings/usagestats/f/e;

    const-wide/16 v4, 0x0

    new-instance v6, Lcom/xiaomi/misettings/usagestats/f/j;

    iget-wide v7, p1, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    sget-wide v9, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    int-to-long v11, v2

    mul-long/2addr v9, v11

    add-long/2addr v7, v9

    invoke-direct {v6, v7, v8}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-direct {v3, v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/f/e;-><init>(JLcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    new-instance v4, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v5

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/h;->d()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v3

    invoke-direct {v4, v5, v6, v3}, Lcom/xiaomi/misettings/usagestats/f/e;-><init>(JLcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->g:Ljava/util/List;

    return-object v0
.end method

.method private static a(Lcom/xiaomi/misettings/usagestats/f/h;J)Lcom/xiaomi/misettings/usagestats/home/category/c/e;
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/home/category/c/e;-><init>()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->a:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/h;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/h;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/h;->d()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v1

    iget-wide v1, v1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->d:J

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/h;->b()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/category/d/g;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->f:Ljava/util/List;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->h:J

    iput-wide p1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->i:J

    return-object v0
.end method

.method private static a(Lcom/xiaomi/misettings/usagestats/f/h;)Lcom/xiaomi/misettings/usagestats/home/category/c/f;
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/c/f;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/home/category/c/f;-><init>()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/h;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/f;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/h;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/f;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/f;->c:J

    return-object v0
.end method

.method private static a(Lcom/xiaomi/misettings/usagestats/f/o;)Lcom/xiaomi/misettings/usagestats/home/category/c/f;
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/c/f;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/home/category/c/f;-><init>()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/o;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/f;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/o;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/f;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/o;->e()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/f;->c:J

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/h;J)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            "J)",
            "Ljava/util/List<",
            "Lb/c/a/a/a;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/h;->b()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/h;->d()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v1

    invoke-static {p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/home/category/d/g;->a(Lcom/xiaomi/misettings/usagestats/f/h;J)Lcom/xiaomi/misettings/usagestats/home/category/c/e;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p2, Lcom/xiaomi/misettings/usagestats/home/category/c/a;

    const/4 p3, 0x1

    invoke-direct {p2, p3}, Lcom/xiaomi/misettings/usagestats/home/category/c/a;-><init>(I)V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/h;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/d;

    new-instance v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    invoke-direct {v4}, Lcom/xiaomi/misettings/usagestats/home/category/c/c;-><init>()V

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->g:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v5

    iput-wide v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->e:J

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->h:Ljava/util/List;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/h;->e()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->b:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->c:Ljava/lang/String;

    iget-wide v5, v1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    iput-wide v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->i:J

    iget-wide v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->e:J

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-lez v3, :cond_1

    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    sget-object p0, Lcom/xiaomi/misettings/usagestats/home/category/d/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/d/e;

    invoke-static {p2, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/home/category/d/b;->a:Lcom/xiaomi/misettings/usagestats/home/category/d/b;

    invoke-static {p3, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance p0, Lcom/xiaomi/misettings/usagestats/home/category/c/d;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/c/d;-><init>()V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/home/category/d/g;->a()Z

    move-result p0

    if-eqz p0, :cond_3

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/category/d/g;->a(Lcom/xiaomi/misettings/usagestats/f/h;)Lcom/xiaomi/misettings/usagestats/home/category/c/f;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/o;Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/xiaomi/misettings/usagestats/f/o;",
            "Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;",
            ")",
            "Ljava/util/List<",
            "Lb/c/a/a/a;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/d/g;->a(Lcom/xiaomi/misettings/usagestats/f/o;Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Lb/c/a/a/a;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/category/c/a;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/c/a;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/o;->d()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2, p2}, Lcom/xiaomi/misettings/usagestats/home/category/d/g;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Ljava/util/Map;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    iget-wide v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->e:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/xiaomi/misettings/usagestats/utils/p;->a:Ljava/util/List;

    iget-object v4, v2, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->c:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    sget-object p0, Lcom/xiaomi/misettings/usagestats/home/category/d/c;->a:Lcom/xiaomi/misettings/usagestats/home/category/d/c;

    invoke-static {p2, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/home/category/d/d;->a:Lcom/xiaomi/misettings/usagestats/home/category/d/d;

    invoke-static {v1, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance p0, Lcom/xiaomi/misettings/usagestats/home/category/c/d;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/c/d;-><init>()V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/home/category/d/g;->a()Z

    move-result p0

    if-eqz p0, :cond_4

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/category/d/g;->a(Lcom/xiaomi/misettings/usagestats/f/o;)Lcom/xiaomi/misettings/usagestats/home/category/c/f;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/d;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-gt v3, v2, :cond_2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    add-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/home/category/c/c;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/h;

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/h;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    if-nez v4, :cond_2

    new-instance v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    invoke-direct {v4}, Lcom/xiaomi/misettings/usagestats/home/category/c/c;-><init>()V

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v5

    iput-wide v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->e:J

    iput-object p2, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->b:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->f:Landroid/graphics/drawable/Drawable;

    invoke-static {p0, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->g:Ljava/lang/String;

    iput-object v3, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->c:Ljava/lang/String;

    iget-wide v5, p3, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    iput-wide v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->i:J

    iget-wide v5, p3, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    iput-wide v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->j:J

    const/4 v2, 0x1

    iput-boolean v2, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->a:Z

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    iget-wide v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->e:J

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v2

    add-long/2addr v5, v2

    iput-wide v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->e:J

    goto :goto_1

    :cond_3
    return-object v0
.end method

.method private static a()Z
    .locals 1

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/category/c/c;Lcom/xiaomi/misettings/usagestats/home/category/c/c;)I
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->g:Ljava/lang/String;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->g:Ljava/lang/String;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/category/c/c;Lcom/xiaomi/misettings/usagestats/home/category/c/c;)I
    .locals 2

    iget-wide v0, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->e:J

    iget-wide p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->e:J

    sub-long/2addr v0, p0

    long-to-int p0, v0

    return p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/home/category/c/c;Lcom/xiaomi/misettings/usagestats/home/category/c/c;)I
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->g:Ljava/lang/String;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->g:Ljava/lang/String;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p0

    return p0
.end method
