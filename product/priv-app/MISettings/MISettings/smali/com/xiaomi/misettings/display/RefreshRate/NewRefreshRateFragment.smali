.class public Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;
.super Lmiuix/preference/PreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$c;
.implements Landroidx/preference/Preference$b;


# static fields
.field private static final a:Z

.field private static final b:I

.field private static c:Landroid/content/Context;

.field private static d:Ljava/lang/String;


# instance fields
.field private e:Landroid/content/Intent;

.field private f:[Ljava/lang/CharSequence;

.field private g:Landroid/content/IntentFilter;

.field private h:Landroidx/preference/Preference;

.field private i:Landroidx/preference/Preference;

.field private j:Lcom/misettings/common/widget/FooterPreference;

.field private k:Lmiuix/preference/DropDownPreference;

.field private l:Lmiuix/preference/RadioButtonPreference;

.field private m:Lmiuix/preference/RadioButtonPreference;

.field private final n:Ljava/lang/String;

.field private o:Landroid/database/ContentObserver;

.field private final p:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    const-string v1, "dc_backlight_fps_incompatible"

    invoke-static {v1, v0}, Lb/c/a/b/a/b;->a(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->a:Z

    const-string v1, "defaultFps"

    invoke-static {v1, v0}, Lb/c/a/b/a/b;->a(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->b:I

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/preference/PreferenceFragment;-><init>()V

    const-string v0, "custom_mode_switch"

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->n:Ljava/lang/String;

    new-instance v0, Lcom/xiaomi/misettings/display/RefreshRate/w;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/display/RefreshRate/w;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->p:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a([Ljava/lang/CharSequence;I)I
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    array-length v1, p1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    move v1, v0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    aget-object v2, p1, v0

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/display/b;->a(Ljava/lang/String;)I

    move-result v2

    if-ne p2, v2, :cond_1

    move v1, v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v1

    :cond_3
    :goto_1
    return v0
.end method

.method public static a(Landroid/content/Context;)Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;
    .locals 0

    sput-object p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    new-instance p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;-><init>()V

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->k()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v1, "image_preferce_category"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->a(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceCategory;

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->g()V

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->h:Landroidx/preference/Preference;

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->b(Landroidx/preference/Preference;)Z

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->h:Landroidx/preference/Preference;

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->d(Landroidx/preference/Preference;)Z

    :goto_0
    return-void
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    sput-object p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->d:Ljava/lang/String;

    return-object p0
.end method

.method private b(Z)V
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->d:Ljava/lang/String;

    const-string v1, "false"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const-string v1, "btn_preferce_category"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->a(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceCategory;

    if-nez v0, :cond_2

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->h()V

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->i:Landroidx/preference/Preference;

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->b(Landroidx/preference/Preference;)Z

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->i:Landroidx/preference/Preference;

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->d(Landroidx/preference/Preference;)Z

    :goto_0
    return-void
.end method

.method private c(I)V
    .locals 4

    sget-boolean v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x3c

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;I)V

    :cond_0
    invoke-static {}, Lcom/misettings/common/utils/j;->a()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/display/RefreshRate/y;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/y;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;I)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private d(I)V
    .locals 2

    sget-boolean v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x3c

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;I)V

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/display/b;->c(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic f()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    return-object v0
.end method

.method private g()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->h:Landroidx/preference/Preference;

    if-nez v0, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->h:Landroidx/preference/Preference;

    if-nez v0, :cond_0

    new-instance v0, Landroidx/preference/Preference;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->h:Landroidx/preference/Preference;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->h:Landroidx/preference/Preference;

    const-string v1, "fps_save_battery_tips"

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->h:Landroidx/preference/Preference;

    sget v1, Lcom/xiaomi/misettings/display/l;->fps_save_battery_tips:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/16 v3, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->h:Landroidx/preference/Preference;

    sget v1, Lcom/xiaomi/misettings/display/k;->fps_save_battery_tips_background:I

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->h:Landroidx/preference/Preference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->h:Landroidx/preference/Preference;

    invoke-virtual {v0, v4}, Landroidx/preference/Preference;->setOrder(I)V

    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method private h()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->i:Landroidx/preference/Preference;

    if-nez v0, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->i:Landroidx/preference/Preference;

    if-nez v0, :cond_0

    new-instance v0, Landroidx/preference/Preference;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->i:Landroidx/preference/Preference;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->i:Landroidx/preference/Preference;

    const-string v1, "high_refresh_options"

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->i:Landroidx/preference/Preference;

    sget v1, Lcom/xiaomi/misettings/display/l;->customize_high_refresh_title:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->i:Landroidx/preference/Preference;

    sget v1, Lcom/xiaomi/misettings/display/l;->customize_high_refresh_summary:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/16 v4, 0x78

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->i:Landroidx/preference/Preference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method private i()[Ljava/lang/CharSequence;
    .locals 7

    const-string v0, "fpsList"

    invoke-static {v0}, Lb/c/a/b/a/b;->b(Ljava/lang/String;)[I

    move-result-object v0

    array-length v1, v0

    new-array v1, v1, [Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    array-length v2, v0

    if-gtz v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->sort([I)V

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    array-length v4, v0

    if-ge v3, v4, :cond_1

    sget v4, Lcom/xiaomi/misettings/display/l;->screen_fps_unit:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aget v6, v0, v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {p0, v4, v5}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-object v1
.end method

.method private j()Z
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/b;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3c

    sget-object v1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/display/b;->b(Landroid/content/Context;)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private k()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->m:Lmiuix/preference/RadioButtonPreference;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->l:Lmiuix/preference/RadioButtonPreference;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->k:Lmiuix/preference/DropDownPreference;

    if-nez v0, :cond_0

    goto :goto_2

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/b;->h(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->m:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->l:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->k:Lmiuix/preference/DropDownPreference;

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->f:[Ljava/lang/CharSequence;

    sget-object v3, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {v3}, Lcom/xiaomi/misettings/display/b;->b(Landroid/content/Context;)I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->a([Ljava/lang/CharSequence;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lmiuix/preference/DropDownPreference;->a(I)V

    :goto_0
    const/16 v0, 0x78

    sget-object v2, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/misettings/display/b;->b(Landroid/content/Context;)I

    move-result v2

    if-eq v0, v2, :cond_2

    const/16 v0, 0x90

    sget-object v2, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/misettings/display/b;->b(Landroid/content/Context;)I

    move-result v2

    if-ne v0, v2, :cond_3

    :cond_2
    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/b;->h(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->b(Z)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->b(Z)V

    :goto_1
    return-void

    :cond_4
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "refreshFpsUI: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->m:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->l:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->k:Lmiuix/preference/DropDownPreference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NewRefreshRateFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lmiuix/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->o:Landroid/database/ContentObserver;

    if-nez p1, :cond_1

    new-instance p1, Lcom/xiaomi/misettings/display/RefreshRate/x;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/xiaomi/misettings/display/RefreshRate/x;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;Landroid/os/Handler;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->o:Landroid/database/ContentObserver;

    :cond_1
    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "custom_mode_switch"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->o:Landroid/database/ContentObserver;

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    :cond_0
    sget p1, Lcom/xiaomi/misettings/display/o;->refresh_rate:I

    invoke-virtual {p0, p1, p2}, Landroidx/preference/PreferenceFragmentCompat;->setPreferencesFromResource(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->i()[Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->f:[Ljava/lang/CharSequence;

    const-string p1, "user_tip_footer"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/misettings/common/widget/FooterPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->j:Lcom/misettings/common/widget/FooterPreference;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->j:Lcom/misettings/common/widget/FooterPreference;

    sget p2, Lcom/xiaomi/misettings/display/l;->fps_user_tip_footer:I

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v3

    invoke-virtual {p0, p2, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    const-string p1, "intelligent_recommendation"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/RadioButtonPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->m:Lmiuix/preference/RadioButtonPreference;

    const-string p1, "customize_fps"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/RadioButtonPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->l:Lmiuix/preference/RadioButtonPreference;

    const-string p1, "customize_fps_ddp"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/DropDownPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->k:Lmiuix/preference/DropDownPreference;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->k:Lmiuix/preference/DropDownPreference;

    sget-object p2, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {p2}, Lcom/xiaomi/misettings/display/b;->h(Landroid/content/Context;)Z

    move-result p2

    xor-int/2addr p2, v3

    invoke-virtual {p1, p2}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->f:[Ljava/lang/CharSequence;

    if-eqz p1, :cond_1

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->k:Lmiuix/preference/DropDownPreference;

    invoke-virtual {p2, p1}, Lmiuix/preference/DropDownPreference;->a([Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->k:Lmiuix/preference/DropDownPreference;

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->f:[Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lmiuix/preference/DropDownPreference;->b([Ljava/lang/CharSequence;)V

    :cond_1
    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    sget p2, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->b:I

    const-string v0, "last_fps_value"

    invoke-static {p1, v0, p2}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result p1

    sget-object p2, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    const-string v0, "custom_mode_switch"

    invoke-static {p2, v0}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    sput-object p2, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->d:Ljava/lang/String;

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->f:[Ljava/lang/CharSequence;

    array-length v0, p2

    sub-int/2addr v0, v3

    aget-object p2, p2, v0

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/xiaomi/misettings/display/b;->a(Ljava/lang/String;)I

    move-result p2

    if-ne p1, p2, :cond_3

    const/16 p2, 0x78

    if-eq p1, p2, :cond_2

    const/16 p2, 0x90

    if-ne p1, p2, :cond_3

    :cond_2
    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/display/b;->h(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_3

    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->d:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->b(Z)V

    :cond_3
    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->j()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->a(Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->o:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/display/b;->a(Ljava/lang/String;)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c(I)V

    sget-object p2, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    const-string v0, "last_fps_value"

    invoke-static {p2, v0, p1}, Lcom/xiaomi/misettings/display/b;->b(Landroid/content/Context;Ljava/lang/String;I)V

    sget-object p2, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->d:Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 v0, 0x1

    if-nez p2, :cond_2

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->f:[Ljava/lang/CharSequence;

    array-length v1, p2

    sub-int/2addr v1, v0

    aget-object p2, p2, v1

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/xiaomi/misettings/display/b;->a(Ljava/lang/String;)I

    move-result p2

    if-ne p1, p2, :cond_1

    const/16 p2, 0x78

    if-eq p1, p2, :cond_0

    const/16 p2, 0x90

    if-ne p1, p2, :cond_1

    :cond_0
    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->b(Z)V

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->b(Z)V

    :cond_2
    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The current FPS value is: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "NewRefreshRateFragment"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 7

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "customize_fps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    move p1, v1

    goto :goto_1

    :sswitch_1
    const-string v0, "intelligent_recommendation"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    move p1, v2

    goto :goto_1

    :sswitch_2
    const-string v0, "fps_save_battery_tips"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    move p1, v3

    goto :goto_1

    :sswitch_3
    const-string v0, "high_refresh_options"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    move p1, v4

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p1, -0x1

    :goto_1
    const-string v0, "NewRefreshRateFragment"

    if-eqz p1, :cond_8

    if-eq p1, v4, :cond_6

    const/16 v5, 0x90

    const/16 v6, 0x78

    if-eq p1, v2, :cond_4

    if-eq p1, v1, :cond_1

    goto/16 :goto_2

    :cond_1
    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/display/b;->h(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_a

    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->d(I)V

    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    sget v1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->b:I

    const-string v2, "last_fps_value"

    invoke-static {p1, v2, v1}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c(I)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The last FPS value is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq p1, v6, :cond_2

    if-ne p1, v5, :cond_3

    :cond_2
    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, v4}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->b(Z)V

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->k:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_a

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->f:[Ljava/lang/CharSequence;

    invoke-direct {p0, v1, p1}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->a([Ljava/lang/CharSequence;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lmiuix/preference/DropDownPreference;->a(I)V

    goto/16 :goto_2

    :cond_4
    invoke-direct {p0, v4}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->d(I)V

    sget p1, Lcom/xiaomi/misettings/display/b;->b:I

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c(I)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The current FPS value is: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lcom/xiaomi/misettings/display/b;->b:I

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget p1, Lcom/xiaomi/misettings/display/b;->b:I

    if-eq p1, v6, :cond_5

    if-ne p1, v5, :cond_a

    :cond_5
    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->d:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_a

    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->b(Z)V

    goto :goto_2

    :cond_6
    new-instance p1, Landroid/content/Intent;

    const-string v1, "miui.intent.action.HIGH_REFRESH"

    invoke-direct {p1, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    const-string v1, "miui.intent.action.HIGH_REFRESH_OPTIONS"

    invoke-static {p1, v1}, Lcom/xiaomi/misettings/display/RefreshRate/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    :cond_7
    const-string p1, "jump to HighRefreshOptionsActivity"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_8
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->e:Landroid/content/Intent;

    if-nez p1, :cond_9

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->e:Landroid/content/Intent;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->e:Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.securitycenter"

    const-string v3, "com.miui.powercenter.PowerMainActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->e:Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :cond_9
    :try_start_0
    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->e:Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    const-string p1, "jump to security center error!"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    :goto_2
    return v4

    :sswitch_data_0
    .sparse-switch
        -0x6be79b63 -> :sswitch_3
        0xb7e47d6 -> :sswitch_2
        0x5ecbc281 -> :sswitch_1
        0x61c56eed -> :sswitch_0
    .end sparse-switch
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->g:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->g:Landroid/content/IntentFilter;

    const-string v1, "miui.intent.action.POWER_SAVE_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->p:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->g:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->k:Lmiuix/preference/DropDownPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$b;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->m:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->l:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->j()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->a(Z)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/NewRefreshRateFragment;->k()V

    return-void
.end method
