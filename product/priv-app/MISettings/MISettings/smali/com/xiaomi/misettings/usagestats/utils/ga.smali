.class public Lcom/xiaomi/misettings/usagestats/utils/ga;
.super Landroid/os/HandlerThread;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/utils/ga$b;,
        Lcom/xiaomi/misettings/usagestats/utils/ga$a;
    }
.end annotation


# static fields
.field private static a:Landroid/os/Handler;

.field private static volatile b:Lcom/xiaomi/misettings/usagestats/utils/ga;

.field public static c:Lcom/xiaomi/misettings/usagestats/f/g;

.field private static d:Landroid/os/Handler;

.field private static e:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/xiaomi/misettings/usagestats/utils/ga$b;",
            ">;"
        }
    .end annotation
.end field

.field private static f:Ljava/lang/Runnable;

.field private static g:Ljava/lang/Runnable;


# instance fields
.field private h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->d:Landroid/os/Handler;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->e:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/ea;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/utils/ea;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->f:Ljava/lang/Runnable;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/fa;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/utils/fa;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->g:Ljava/lang/Runnable;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "Usagestats data thread..."

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/ga;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->start()V

    return-void
.end method

.method static synthetic a()Ljava/lang/Runnable;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->g:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic a(I)V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->c(I)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->c(Landroid/content/Context;)V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/utils/ga;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Landroid/content/Context;Ljava/lang/Boolean;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ljava/lang/Boolean;Z)V
    .locals 23

    move-object/from16 v0, p0

    const-class v10, Lcom/xiaomi/misettings/usagestats/utils/ga;

    monitor-enter v10

    :try_start_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v13

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v3, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-direct {v3, v2, v13, v14}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-direct {v1, v3}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    :cond_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v1

    iget-wide v3, v1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v1, v3, v13

    if-eqz v1, :cond_1

    new-instance v1, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v3, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-direct {v3, v2, v13, v14}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-direct {v1, v3}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    :cond_1
    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    invoke-static/range {p0 .. p0}, Lcom/xiaomi/misettings/usagestats/utils/u;->a(Landroid/content/Context;)J

    move-result-wide v1

    invoke-static {v13, v14, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v15

    const/4 v7, 0x0

    if-eqz v15, :cond_2

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/L;->c(J)I

    move-result v1

    move/from16 v16, v1

    goto :goto_0

    :cond_2
    move/from16 v16, v7

    :goto_0
    invoke-static {v0, v11, v12}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;J)V

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-wide v2, v11

    move-wide v4, v13

    invoke-static/range {v1 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJZ)Ljava/util/List;

    move-result-object v9

    invoke-static {v9}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v8

    const-string v1, "UsagestatsDataThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadUsageByInterval: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v5, v13

    :goto_1
    const-wide/16 v3, 0x0

    const/4 v2, 0x1

    if-ge v7, v8, :cond_5

    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    if-eqz v15, :cond_3

    add-int/lit8 v1, v16, -0x1

    if-gt v7, v1, :cond_3

    move/from16 v21, v7

    move/from16 v19, v8

    move-object/from16 v22, v9

    goto :goto_2

    :cond_3
    sget-object v19, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    const/16 v20, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    move-wide/from16 v3, v17

    move/from16 v21, v7

    move/from16 v19, v8

    move-wide v7, v13

    move-object/from16 v22, v9

    move/from16 v9, v20

    invoke-static/range {v1 .. v9}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJZ)V

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    const-wide/16 v7, 0x0

    invoke-virtual {v1, v7, v8}, Lcom/xiaomi/misettings/usagestats/f/g;->a(J)V

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    const/4 v9, 0x1

    invoke-virtual {v1, v9}, Lcom/xiaomi/misettings/usagestats/f/g;->b(Z)V

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->f:Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->c(Ljava/lang/Runnable;)V

    :cond_4
    :goto_2
    add-int/lit8 v7, v21, 0x1

    move-wide/from16 v5, v17

    move/from16 v8, v19

    move-object/from16 v9, v22

    goto :goto_1

    :cond_5
    move v9, v2

    move-wide v7, v3

    sget-object v2, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/4 v15, 0x0

    move-object/from16 v1, p0

    move-wide/from16 v16, v11

    move-wide v11, v7

    move-wide v7, v13

    move v9, v15

    invoke-static/range {v1 .. v9}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJZ)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v6

    move-object/from16 v1, p0

    move-wide v2, v13

    move/from16 v7, p2

    invoke-static/range {v1 .. v7}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJLjava/util/concurrent/ConcurrentHashMap;Z)V

    if-nez p2, :cond_6

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Ljava/util/concurrent/ConcurrentHashMap;)V

    :cond_6
    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1, v11, v12}, Lcom/xiaomi/misettings/usagestats/f/g;->a(J)V

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/xiaomi/misettings/usagestats/f/g;->b(Z)V

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1, v2}, Lcom/xiaomi/misettings/usagestats/f/g;->a(Z)V

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->e(Landroid/content/Context;J)V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->f:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->c(Ljava/lang/Runnable;)V

    const-string v0, "UsagestatsDataThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadTodayData:duration="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v1

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v10

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0
.end method

.method static synthetic a(Ljava/lang/Runnable;)V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->b:Lcom/xiaomi/misettings/usagestats/utils/ga;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->b:Lcom/xiaomi/misettings/usagestats/utils/ga;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const-class v0, Lcom/xiaomi/misettings/usagestats/utils/ga;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->b:Lcom/xiaomi/misettings/usagestats/utils/ga;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->b:Lcom/xiaomi/misettings/usagestats/utils/ga;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    new-instance v1, Lcom/xiaomi/misettings/usagestats/utils/ga;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/utils/ga;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->b:Lcom/xiaomi/misettings/usagestats/utils/ga;

    :cond_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    sget-object p0, Lcom/xiaomi/misettings/usagestats/utils/ga;->b:Lcom/xiaomi/misettings/usagestats/utils/ga;

    return-object p0

    :catchall_0
    move-exception p0

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p0
.end method

.method public static c()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->b:Lcom/xiaomi/misettings/usagestats/utils/ga;

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->a:Landroid/os/Handler;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/g;->h()V

    :cond_0
    return-void
.end method

.method private static c(I)V
    .locals 3

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/utils/ga$b;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->d:Landroid/os/Handler;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/utils/da;

    invoke-direct {v2, v0, p0}, Lcom/xiaomi/misettings/usagestats/utils/da;-><init>(Lcom/xiaomi/misettings/usagestats/utils/ga$b;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private static declared-synchronized c(Landroid/content/Context;)V
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/utils/ga;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Landroid/content/Context;Ljava/lang/Boolean;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private static c(Ljava/lang/Runnable;)V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Ljava/lang/Runnable;J)V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public b()V
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->a:Landroid/os/Handler;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public b(I)V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public quit()Z
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->a:Landroid/os/Handler;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_1
    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->e:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :cond_2
    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/ga;->h:Ljava/lang/ref/WeakReference;

    sput-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->b:Lcom/xiaomi/misettings/usagestats/utils/ga;

    invoke-super {p0}, Landroid/os/HandlerThread;->quit()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized start()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/os/HandlerThread;->start()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/ga;->h:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/ga;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;

    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/utils/ga;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/ga$a;-><init>(Landroid/os/Looper;Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/ga;->a:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
