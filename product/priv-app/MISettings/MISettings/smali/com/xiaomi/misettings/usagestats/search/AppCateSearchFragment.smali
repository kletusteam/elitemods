.class public Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;
.super Lcom/xiaomi/misettings/base/BaseFragment;


# instance fields
.field private c:Landroid/view/View;

.field private d:Lmiuix/recyclerview/widget/RecyclerView;

.field public e:Lcom/xiaomi/misettings/usagestats/search/g;

.field private f:Ljava/lang/String;

.field private g:Z

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/a;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/a;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->f:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->g:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->h:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->i:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->j:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->k:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->l:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->m:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->n:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->o:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private d(Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->k:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->l:Ljava/util/List;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->j:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e:Lcom/xiaomi/misettings/usagestats/search/g;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/search/g;->b(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e:Lcom/xiaomi/misettings/usagestats/search/g;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->k:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/search/g;->a(Ljava/util/List;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e:Lcom/xiaomi/misettings/usagestats/search/g;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->l:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/search/g;->b(Ljava/util/List;)V

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->o:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->m:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->h:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e:Lcom/xiaomi/misettings/usagestats/search/g;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/search/g;->b(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e:Lcom/xiaomi/misettings/usagestats/search/g;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->o:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/search/g;->c(Ljava/util/List;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e:Lcom/xiaomi/misettings/usagestats/search/g;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/search/g;->a(Ljava/util/ArrayList;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e002d

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->i:Ljava/util/List;

    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->n:Ljava/util/List;

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e:Lcom/xiaomi/misettings/usagestats/search/g;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/search/g;->b(Ljava/lang/String;)V

    sget-boolean v0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->d(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e:Lcom/xiaomi/misettings/usagestats/search/g;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->k()V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->l()V

    :goto_1
    return-void
.end method

.method public c(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->j:Ljava/util/List;

    return-void
.end method

.method public d(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->h:Ljava/util/List;

    return-void
.end method

.method public k()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->d:Lmiuix/recyclerview/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public l()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->d:Lmiuix/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->g:Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e:Lcom/xiaomi/misettings/usagestats/search/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/search/g;->a()V

    :goto_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const p2, 0x7f0b0069

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->c:Landroid/view/View;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->c:Landroid/view/View;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/search/a;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/search/a;-><init>(Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const p2, 0x7f0b02fc

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->d:Lmiuix/recyclerview/widget/RecyclerView;

    new-instance p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    invoke-direct {p1, p2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->k(I)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->d:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    new-instance p1, Lcom/xiaomi/misettings/usagestats/search/g;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/xiaomi/misettings/usagestats/search/g;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e:Lcom/xiaomi/misettings/usagestats/search/g;

    sget-boolean p1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    const-string p2, ""

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->f:Ljava/lang/String;

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->f:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->f:Ljava/lang/String;

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->f:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->d(Ljava/lang/String;)V

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->d:Lmiuix/recyclerview/widget/RecyclerView;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->e:Lcom/xiaomi/misettings/usagestats/search/g;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->d:Lmiuix/recyclerview/widget/RecyclerView;

    new-instance p2, Lcom/xiaomi/misettings/usagestats/search/b;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/usagestats/search/b;-><init>(Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;)V

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$k;)V

    return-void
.end method
