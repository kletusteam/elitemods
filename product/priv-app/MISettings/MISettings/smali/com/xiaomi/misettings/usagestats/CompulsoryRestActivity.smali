.class public Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;
.super Lcom/misettings/common/base/BaseActivity;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/Button;

.field private e:Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;

.field private f:I

.field private g:I

.field private h:I

.field private i:Landroid/os/CountDownTimer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->h:I

    return p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->b:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/i/h;->e(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/i/h;->d(Landroid/content/Context;Z)V

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->h:I

    return p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->g:I

    return p0
.end method

.method private c()V
    .locals 3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->e:Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;

    return-object p0
.end method

.method private d()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "miui.intent.action.STEADY_ON_TIMEOVER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private initData()V
    .locals 8

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->m(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->c(Landroid/content/Context;)V

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->a(Z)V

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->h(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->f:I

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->j(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->g:I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->i(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->h:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f110016

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->f:I

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f110014

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->g:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v7

    invoke-virtual {v2, v3, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->h:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->a(J)V

    return-void
.end method

.method private initView()V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {p0}, Lcom/xiaomi/misettings/f;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    and-int/lit16 v1, v1, -0x2001

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_0
    const v1, 0x7f0b00c0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->a:Landroid/widget/TextView;

    const v1, 0x7f0b02d5

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->b:Landroid/widget/TextView;

    const v1, 0x7f0b02d4

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->c:Landroid/widget/TextView;

    const v1, 0x7f0b007c

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->d:Landroid/widget/Button;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->d:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->d:Landroid/widget/Button;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->d:Landroid/widget/Button;

    new-array v3, v3, [Lmiuix/animation/a/a;

    invoke-interface {v1, v2, v3}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    const v1, 0x7f0b02c8

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->e:Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/n;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/n;-><init>(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->i:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->i:Landroid/os/CountDownTimer;

    return-void
.end method

.method public a(J)V
    .locals 6

    new-instance p1, Lcom/xiaomi/misettings/usagestats/o;

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->h:I

    mul-int/lit16 p2, p2, 0x3e8

    int-to-long v2, p2

    const-wide/16 v4, 0x3e8

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/misettings/usagestats/o;-><init>(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;JJ)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->i:Landroid/os/CountDownTimer;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->b()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->i:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0b007c

    if-eq p1, v0, :cond_0

    goto :goto_1

    :cond_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->h:I

    if-gtz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->a(Z)V

    :goto_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->c()V

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/16 v0, 0x400

    invoke-virtual {p1, v0, v0}, Landroid/view/Window;->setFlags(II)V

    const p1, 0x7f0e0020

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->d()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->initView()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->initData()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->a()V

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    return-void
.end method

.method protected onStop()V
    .locals 4

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStop()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/a;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/a;-><init>(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x12c

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    :goto_0
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
