.class public Lcom/xiaomi/misettings/usagestats/widget/a/c/d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/widget/a/c/d$a;,
        Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;
    }
.end annotation


# static fields
.field public static a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

.field public static b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/widget/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/widget/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field public static e:Z

.field public static f:J

.field public static g:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b:Ljava/util/List;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->e:Z

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->f:J

    const-wide/16 v0, 0x7d0

    sput-wide v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->g:J

    return-void
.end method

.method public static a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->f()Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    move-result-object v0

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;->c:Ljava/util/List;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "init"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v1, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UsageStatsWidgetDataHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->e:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b(Landroid/content/Context;)V

    const/4 p0, 0x1

    sput-boolean p0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->e:Z

    :cond_0
    return-void
.end method

.method protected static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b:Ljava/util/List;

    invoke-static {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/b/a/g;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;Ljava/util/List;)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b:Ljava/util/List;

    invoke-static {p0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Z)V
    .locals 8

    const-class v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sget-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    if-nez v3, :cond_0

    new-instance v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    invoke-direct {v3}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;-><init>()V

    sput-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    :cond_0
    sget-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    iput-object v2, v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;->c:Ljava/util/List;

    sget-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v4

    invoke-static {p0, v4}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v4

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->e()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-long v4, v4

    sget-wide v6, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v4, v6

    iput-wide v4, v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;->a:J

    sget-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;->b:J

    if-eqz p1, :cond_1

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V

    goto :goto_0

    :cond_1
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    :goto_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized a(Landroid/content/Context;ZLcom/xiaomi/misettings/usagestats/widget/a/c/d$a;)V
    .locals 8

    const-class v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;

    monitor-enter v0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeMainProcessProvider;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "com.xiaomi.misettings.usagestats.screentimemainprocessprovider"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "GET_APP_INNER_DATA"

    const-string v4, ""

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "updateUsageStatsRemoteProcess"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "call"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_3

    const-string v2, "data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/misettings/common/utils/h;->a()Lb/a/b/q;

    move-result-object v2

    const-class v3, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v2, v1, v3}, Lb/a/b/q;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/g;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    :try_start_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sget-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    if-nez v3, :cond_1

    new-instance v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    invoke-direct {v3}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;-><init>()V

    sput-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    :cond_1
    sget-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    iput-object v2, v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;->c:Ljava/util/List;

    sget-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v4

    invoke-static {p0, v4}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v4

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->e()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-long v4, v4

    sget-wide v6, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v4, v6

    iput-wide v4, v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;->a:J

    sget-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;->b:J

    if-eqz p1, :cond_2

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V

    goto :goto_0

    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    :goto_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a(Z)V

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_5

    invoke-interface {p2}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$a;->a()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p0

    if-eqz p2, :cond_4

    :try_start_3
    invoke-interface {p2}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$a;->a()V

    :cond_4
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_5
    :goto_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private static a(Z)V
    .locals 14

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c:Ljava/util/List;

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->d:Ljava/util/List;

    const/4 v1, 0x5

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v1, :cond_2

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->d:Ljava/util/List;

    move v0, v2

    :goto_0
    if-ge v0, v1, :cond_2

    sget-object v3, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->d:Ljava/util/List;

    new-instance v4, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;

    invoke-direct {v4}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const-wide/16 v3, 0x64

    const-wide/16 v5, 0x5

    if-eqz p0, :cond_5

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-lez p0, :cond_5

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b()Ljava/util/List;

    move-result-object p0

    if-eqz p0, :cond_4

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_3

    goto :goto_2

    :cond_3
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    invoke-static {v1, p0}, Ljava/lang/Math;->min(II)I

    move-result p0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v0

    long-to-int v0, v0

    move v1, v2

    :goto_1
    if-ge v1, p0, :cond_8

    sget-object v7, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->d:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/h;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->b(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v9

    mul-long/2addr v9, v3

    int-to-long v11, v0

    div-long/2addr v9, v11

    invoke-static {v5, v6, v9, v10}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v9

    long-to-int v9, v9

    invoke-virtual {v7, v9}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->a(I)V

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/h;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->a(J)V

    invoke-virtual {v7, v2}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->a(Z)V

    sget-object v8, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    return-void

    :cond_5
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a()Ljava/util/List;

    move-result-object p0

    if-eqz p0, :cond_8

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_6

    goto/16 :goto_4

    :cond_6
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    invoke-static {v1, p0}, Ljava/lang/Math;->min(II)I

    move-result p0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a()Ljava/util/List;

    move-result-object v1

    move v7, v2

    :goto_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v2, v8, :cond_8

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    sget-object v9, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->d:Ljava/util/List;

    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->b(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v10

    mul-long/2addr v10, v3

    int-to-long v12, v0

    div-long/2addr v10, v12

    invoke-static {v5, v6, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    long-to-int v10, v10

    invoke-virtual {v9, v10}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->a(I)V

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->a(Ljava/lang/String;)V

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->a(Z)V

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->a(J)V

    sget-object v8, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c:Ljava/util/List;

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    sget-object v8, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ne v8, p0, :cond_7

    goto :goto_4

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    :goto_4
    return-void
.end method

.method public static b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->b:Ljava/util/List;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->f:J

    sub-long/2addr v0, v2

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->g:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "UsageStatsWidgetDataHelper"

    const-string v1, "initCategoryLoad"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/a/p;->a()Lcom/xiaomi/misettings/usagestats/b/a/p;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/b/a/p;->c(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->f:J

    return-void
.end method

.method public static c()J
    .locals 2

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->f()Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    move-result-object v0

    iget-wide v0, v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;->a:J

    return-wide v0
.end method

.method public static d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/widget/a/a/a;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c:Ljava/util/List;

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c:Ljava/util/List;

    return-object v0
.end method

.method public static e()J
    .locals 2

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->f()Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    move-result-object v0

    iget-wide v0, v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;->b:J

    return-wide v0
.end method

.method public static f()Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->a:Lcom/xiaomi/misettings/usagestats/widget/a/c/d$b;

    return-object v0
.end method

.method public static g()Z
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
