.class public Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;
.super Lcom/misettings/common/base/BaseActivity;


# static fields
.field public static a:Z

.field public static b:Z

.field public static c:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;


# instance fields
.field private d:Z

.field private e:Landroidx/appcompat/app/ActionBar$c;

.field private f:[Ljava/lang/String;

.field public g:Lmiuix/viewpager/widget/ViewPager;

.field private h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/a;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field public m:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->f:[Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->i:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->j:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->k:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->l:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->m:I

    return-void
.end method

.method private e()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->f()V

    return-void
.end method

.method private f()V
    .locals 8

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v6

    if-nez v6, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/ui/s;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/ui/s;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->d:Z

    const v2, 0x7f130044

    if-eqz v1, :cond_1

    const v1, 0x7f080260

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const v1, 0x7f080261

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const v2, 0x7f130397

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const v2, 0x7f08016c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v2, Lcom/xiaomi/misettings/usagestats/ui/t;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/ui/t;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v6, v1}, Lmiuix/appcompat/app/d;->a(Landroid/view/View;)V

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    invoke-virtual {v6, v0}, Lmiuix/appcompat/app/d;->b(Landroid/view/View;)V

    const/4 v0, 0x4

    invoke-virtual {v6, v0}, Landroidx/appcompat/app/ActionBar;->b(I)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {v6, p0, v0}, Lmiuix/appcompat/app/d;->a(Landroidx/fragment/app/FragmentActivity;Z)V

    invoke-virtual {v6}, Landroidx/appcompat/app/ActionBar;->l()Landroidx/appcompat/app/ActionBar$c;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->e:Landroidx/appcompat/app/ActionBar$c;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->f:[Ljava/lang/String;

    aget-object v2, v1, v0

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->e:Landroidx/appcompat/app/ActionBar$c;

    aget-object v0, v1, v0

    invoke-virtual {v3, v0}, Landroidx/appcompat/app/ActionBar$c;->a(Ljava/lang/CharSequence;)Landroidx/appcompat/app/ActionBar$c;

    const-class v5, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    const/4 v7, 0x0

    move-object v0, v6

    move-object v1, v2

    move-object v2, v3

    move-object v3, v5

    move v5, v7

    invoke-virtual/range {v0 .. v5}, Lmiuix/appcompat/app/d;->a(Ljava/lang/String;Landroidx/appcompat/app/ActionBar$c;Ljava/lang/Class;Landroid/os/Bundle;Z)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->f:[Ljava/lang/String;

    const/4 v7, 0x1

    aget-object v1, v0, v7

    invoke-virtual {v6}, Landroidx/appcompat/app/ActionBar;->l()Landroidx/appcompat/app/ActionBar$c;

    move-result-object v2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->f:[Ljava/lang/String;

    aget-object v0, v0, v7

    invoke-virtual {v2, v0}, Landroidx/appcompat/app/ActionBar$c;->a(Ljava/lang/CharSequence;)Landroidx/appcompat/app/ActionBar$c;

    const-class v3, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    invoke-virtual/range {v0 .. v5}, Lmiuix/appcompat/app/d;->a(Ljava/lang/String;Landroidx/appcompat/app/ActionBar$c;Ljava/lang/Class;Landroid/os/Bundle;Z)I

    sget-boolean v0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->b:Z

    if-eqz v0, :cond_3

    invoke-virtual {v6, v7}, Landroidx/appcompat/app/ActionBar;->c(I)V

    iput v7, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->m:I

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0b03c1

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lmiuix/viewpager/widget/ViewPager;

    if-eqz v1, :cond_4

    check-cast v0, Lmiuix/viewpager/widget/ViewPager;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->g:Lmiuix/viewpager/widget/ViewPager;

    :cond_4
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->g:Lmiuix/viewpager/widget/ViewPager;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/ui/u;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/ui/u;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;)V

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/OriginalViewPager;->setOnPageChangeListener(Landroidx/viewpager/widget/OriginalViewPager$d;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->d()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    return-void
.end method


# virtual methods
.method public a()Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;
    .locals 5

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v1

    const-string v2, "AppCateSearchFragment"

    invoke-virtual {v0, v2}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    if-nez v3, :cond_4

    new-instance v3, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    invoke-direct {v3}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;-><init>()V

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->i:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->i:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->d(Ljava/util/List;)V

    :cond_0
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->j:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->j:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->a(Ljava/util/List;)V

    :cond_1
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->k:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->c(Ljava/util/List;)V

    :cond_2
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->l:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->l:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->b(Ljava/util/List;)V

    :cond_3
    const v3, 0x1020002

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    invoke-virtual {v1, v3, v4, v2}, Landroidx/fragment/app/qa;->a(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/qa;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    invoke-virtual {v1, v2}, Landroidx/fragment/app/qa;->e(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    :cond_4
    invoke-virtual {v1}, Landroidx/fragment/app/qa;->b()I

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->p()Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->j:Ljava/util/List;

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->g:Lmiuix/viewpager/widget/ViewPager;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/viewpager/widget/ViewPager;->setDraggable(Z)V

    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->m:I

    return v0
.end method

.method public b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->l:Ljava/util/List;

    return-void
.end method

.method public b(Z)V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v1

    const-string v2, "AppCateSearchFragment"

    invoke-virtual {v0, v2}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    if-eqz v2, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {v1, v2}, Landroidx/fragment/app/qa;->e(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v2}, Landroidx/fragment/app/qa;->c(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    :goto_0
    invoke-virtual {v1}, Landroidx/fragment/app/qa;->b()I

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->p()Z

    :cond_1
    return-void
.end method

.method public c()Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    return-object v0
.end method

.method public c(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->k:Ljava/util/List;

    return-void
.end method

.method public d()V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "AppCateSearchFragment"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->h:Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    invoke-virtual {v1, v2}, Landroidx/fragment/app/qa;->d(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {v1}, Landroidx/fragment/app/qa;->b()I

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->p()Z

    :cond_0
    return-void
.end method

.method public d(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->i:Ljava/util/List;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    const-string v1, "key_is_week"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    sget-boolean v1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "weekInfo"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    sput-object v1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->c:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    :cond_0
    const-string v1, "key_is_category"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    sput-boolean p1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->b:Z

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/f;->c(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->d:Z

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    const/4 v2, 0x1

    if-lt p1, v1, :cond_2

    move p1, v2

    goto :goto_0

    :cond_2
    move p1, v0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->f:[Ljava/lang/String;

    if-eqz p1, :cond_3

    const p1, 0x7f1303a4

    invoke-virtual {p0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_3
    const-string p1, ""

    :goto_1
    aput-object p1, v1, v0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->f:[Ljava/lang/String;

    const v0, 0x7f130396

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->e()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onResume()V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->m:Lcom/xiaomi/misettings/usagestats/f/g;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    :cond_0
    return-void
.end method
