.class Lcom/xiaomi/misettings/usagestats/adapter/h$a;
.super Landroidx/recyclerview/widget/RecyclerView$t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/adapter/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$t;-><init>(Landroid/view/View;)V

    const v0, 0x7f0b0241

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/h$a;->a:Landroid/widget/ImageView;

    const v0, 0x7f0b0391

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/h$a;->b:Landroid/widget/TextView;

    const v0, 0x7f0b0132

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/h$a;->c:Landroid/widget/FrameLayout;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/adapter/h$a;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/h$a;->a:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/adapter/h$a;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/h$a;->b:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/adapter/h$a;)Landroid/widget/FrameLayout;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/h$a;->c:Landroid/widget/FrameLayout;

    return-object p0
.end method
