.class public Lcom/xiaomi/misettings/usagestats/focusmode/c/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;
    }
.end annotation


# static fields
.field private static a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b;


# instance fields
.field public b:I

.field private c:I

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x28

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->b:I

    const v0, 0x7f03001b

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->c:I

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->d:Landroid/content/Context;

    return-void
.end method

.method public static a(IILandroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/c/b;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;

    invoke-direct {v0, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b;

    :cond_0
    sget-object p2, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b;

    invoke-virtual {p2, p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a(II)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b;

    return-object p0
.end method

.method public static a()V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b;

    :cond_0
    return-void
.end method

.method private a(I)[I
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->length()I

    move-result v0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->length()I

    move-result v1

    new-array v1, v1, [I

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v0, :cond_0

    invoke-virtual {p1, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    aput v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-object v1
.end method


# virtual methods
.method public a(Landroid/widget/ImageView;)Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->c:I

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a(I)[I

    move-result-object v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->b:I

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/c/b;Landroid/widget/ImageView;[II)V

    return-object v0
.end method

.method public a(II)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->c:I

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->b:I

    return-void
.end method
