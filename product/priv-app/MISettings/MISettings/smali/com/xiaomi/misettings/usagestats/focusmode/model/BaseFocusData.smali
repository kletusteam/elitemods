.class public Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;
.super Lb/c/b/a/a;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lb/c/b/a/a;",
        "Ljava/lang/Comparable<",
        "TT;>;"
    }
.end annotation


# static fields
.field public static final FLOOR_NUM:I = 0x4

.field public static final TYPE_DATE:I = 0x0

.field public static final TYPE_DETAIL:I = 0x1

.field public static final TYPE_HEADER:I = 0x3

.field public static final TYPE_ILLEGAL:I = 0x2


# instance fields
.field private date:J

.field private hasMore:Z

.field private isFirstUseDate:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lb/c/b/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    const/4 p1, 0x0

    return p1
.end method

.method public getDate()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->date:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lb/c/a/a/a;->type:I

    return v0
.end method

.method public isFirstUseDate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->isFirstUseDate:Z

    return v0
.end method

.method public isHasMore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->hasMore:Z

    return v0
.end method

.method public setDate(J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->date:J

    return-void
.end method

.method public setFirstUseDate(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->isFirstUseDate:Z

    return-void
.end method

.method public setHasMore(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->hasMore:Z

    return-void
.end method

.method public setType(I)V
    .locals 0

    iput p1, p0, Lb/c/a/a/a;->type:I

    return-void
.end method
