.class public Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/xiaomi/misettings/usagestats/statutoryholidays/c; = null

.field private static b:Z = false

.field private static c:I = 0x12


# instance fields
.field private d:I

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->a:Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->a:Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;

    const/4 p0, 0x0

    sput-boolean p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->b:Z

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->a:Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private declared-synchronized a(Ljava/lang/String;)V
    .locals 9

    monitor-enter p0

    :try_start_0
    const-string v0, "HolidayFactory"

    const-string v1, "parseHoliday"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "holiday"

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "year"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "workday"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v7, v1

    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v7, v8, :cond_0

    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getInt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_0
    const-string v5, "freeday"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v7, v1

    :goto_2
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v7, v8, :cond_1

    invoke-virtual {v3, v7}, Lorg/json/JSONArray;->getInt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_1
    new-instance v3, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;

    invoke-direct {v3}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;-><init>()V

    invoke-virtual {v3, v4}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;->a(I)V

    invoke-virtual {v3, v5}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;->a(Ljava/util/List;)V

    invoke-virtual {v3, v6}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;->b(Ljava/util/List;)V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const-string p1, "versioncode"

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->d:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    if-nez p1, :cond_3

    invoke-static {}, Lcom/xiaomi/misettings/Application;->c()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    :cond_3
    const-string p1, "HolidayFactory"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "parseHoliday: mVersion code is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->d:I

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->a(Landroid/content/Context;I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_0
    move-exception p1

    :try_start_1
    const-string v0, "HolidayFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "parseHolidayData: json exception"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_3
    monitor-exit p0

    return-void

    :goto_4
    monitor-exit p0

    throw p1
.end method

.method private a(II)Z
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->d()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->b(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-nez v0, :cond_2

    return v1

    :cond_2
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->a(I)I

    move-result p1

    if-ltz p1, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;->a()Ljava/util/List;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method private declared-synchronized b(Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "HolidayFactory"

    const-string v1, "parseHolidayWhenArrayEmpty"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private b(II)Z
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->d()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->b(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-nez v0, :cond_2

    return v1

    :cond_2
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->a(I)I

    move-result p1

    if-ltz p1, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;->b()Ljava/util/List;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "HolidayFactory"

    const-string v1, "checkInit initHolidayData"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->a()V

    :cond_1
    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 9

    const-string v0, "loadHolidayDataFromMemory() finally error is "

    const-string v1, "HolidayFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f120005

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    const/4 v4, 0x0

    :try_start_0
    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_0
    :try_start_3
    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_6

    :catch_0
    move-exception v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :catchall_0
    move-exception v2

    move-object v4, v5

    move-object v5, v3

    goto :goto_7

    :catch_1
    move-exception v4

    move-object v8, v5

    move-object v5, v3

    move-object v3, v4

    goto :goto_2

    :catchall_1
    move-exception v2

    move-object v8, v5

    move-object v5, v4

    move-object v4, v8

    goto :goto_7

    :catch_2
    move-exception v3

    move-object v8, v5

    move-object v5, v4

    :goto_2
    move-object v4, v8

    goto :goto_3

    :catchall_2
    move-exception v2

    move-object v5, v4

    goto :goto_7

    :catch_3
    move-exception v3

    move-object v5, v4

    :goto_3
    :try_start_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "loadHolidayDataFromMemory() error is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    if-eqz v4, :cond_1

    :try_start_5
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V

    goto :goto_4

    :catch_4
    move-exception v3

    goto :goto_5

    :cond_1
    :goto_4
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_6

    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    :cond_2
    :goto_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catchall_3
    move-exception v2

    :goto_7
    if-eqz v4, :cond_3

    :try_start_6
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V

    goto :goto_8

    :catch_5
    move-exception v3

    goto :goto_9

    :cond_3
    :goto_8
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_a

    :goto_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_a
    throw v2
.end method


# virtual methods
.method public a(I)I
    .locals 3

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTheYearHolidayData:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "HolidayFactory"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, -0x1

    :goto_1
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/a;->c()I

    move-result v2

    if-ne v2, p1, :cond_2

    move v0, v1

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    :goto_2
    return v0
.end method

.method public a()V
    .locals 1

    sget-boolean v0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->b:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->b()V

    :cond_0
    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/statutoryholidays/d;)V
    .locals 1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/d;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/d;->b()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->d:I

    return-void
.end method

.method public b()V
    .locals 2

    const/4 v0, 0x1

    sput-boolean v0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->b:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->d(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->d:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initHolidayData : version code is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HolidayFactory"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->d:I

    if-eqz v0, :cond_1

    sget v1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->c:I

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    :goto_1
    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/greenguard/manager/m;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/misettings/common/utils/o;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->g(Landroid/content/Context;)V

    :cond_3
    return-void
.end method

.method public c()Z
    .locals 7

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x7

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-direct {p0, v2, v3}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->a(II)Z

    move-result v5

    const/4 v6, 0x0

    if-eqz v5, :cond_0

    return v6

    :cond_0
    invoke-direct {p0, v2, v3}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->b(II)Z

    move-result v2

    if-eqz v2, :cond_1

    return v1

    :cond_1
    if-eq v0, v1, :cond_3

    if-ne v0, v4, :cond_2

    goto :goto_0

    :cond_2
    return v1

    :cond_3
    :goto_0
    return v6
.end method
