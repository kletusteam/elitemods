.class public Lcom/xiaomi/misettings/usagestats/focusmode/c/t;
.super Ljava/util/Observable;


# static fields
.field private static volatile a:Lcom/xiaomi/misettings/usagestats/focusmode/c/t;


# instance fields
.field private b:Lcom/xiaomi/misettings/usagestats/focusmode/data/j;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    return-void
.end method

.method public static b()Lcom/xiaomi/misettings/usagestats/focusmode/c/t;
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/t;

    if-nez v0, :cond_1

    const-class v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/t;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;-><init>()V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/t;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/t;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-virtual {p0}, Ljava/util/Observable;->deleteObservers()V

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/t;

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/focusmode/data/j;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->b:Lcom/xiaomi/misettings/usagestats/focusmode/data/j;

    invoke-virtual {p0}, Ljava/util/Observable;->setChanged()V

    invoke-virtual {p0, p1}, Ljava/util/Observable;->notifyObservers(Ljava/lang/Object;)V

    return-void
.end method
