.class public abstract Lcom/xiaomi/misettings/usagestats/d/e/d;
.super Lcom/xiaomi/misettings/usagestats/d/e/h;


# instance fields
.field private La:I

.field private Ma:I

.field private Na:I

.field private Oa:I

.field private Pa:I

.field private Qa:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;-><init>(Landroid/content/Context;)V

    const p1, 0x7f06035c

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Oa:I

    const/4 p1, 0x2

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Pa:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const v0, 0x7f130394

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Qa:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected b(I)I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    if-ne p1, v0, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->La:I

    if-nez p1, :cond_0

    const p1, 0x7f06035f

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->La:I

    :cond_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->La:I

    return p1

    :cond_1
    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->b(I)I

    move-result p1

    return p1
.end method

.method protected c(I)I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    if-ne p1, v0, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->La:I

    if-nez p1, :cond_0

    const p1, 0x7f06035f

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->La:I

    :cond_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->La:I

    return p1

    :cond_1
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Ma:I

    if-nez p1, :cond_2

    const p1, 0x7f06035d

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Ma:I

    :cond_2
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Ma:I

    return p1
.end method

.method protected e()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Na:I

    if-nez v0, :cond_0

    const v0, 0x7f06035e

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Na:I

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Na:I

    return v0
.end method

.method protected e(I)I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    if-ne p1, v0, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->la:I

    if-nez p1, :cond_0

    const p1, 0x7f06036a

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->la:I

    :cond_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->la:I

    return p1

    :cond_1
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ka:I

    if-nez p1, :cond_2

    const p1, 0x7f06035a

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ka:I

    :cond_2
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ka:I

    return p1
.end method

.method protected h()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Oa:I

    return v0
.end method

.method protected i()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Pa:I

    return v0
.end method

.method protected i(I)I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    if-ne p1, v0, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->la:I

    if-nez p1, :cond_0

    const p1, 0x7f06036a

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->la:I

    :cond_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->la:I

    return p1

    :cond_1
    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->i(I)I

    move-result p1

    return p1
.end method

.method protected p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected q()V
    .locals 6

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    rem-long v2, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    :cond_0
    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    div-long/2addr v0, v2

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    goto :goto_0

    :cond_1
    const-string v0, "BaseWeekUsageViewRender"

    const-string v1, "resetMaxValue: not more than one hour"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/d;->Qa:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->t:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    array-length v1, v0

    :goto_1
    if-ge v2, v1, :cond_4

    aget-object v3, v0, v2

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->q:F

    cmpg-float v4, v4, v3

    if-gez v4, :cond_3

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->q:F

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    return-void
.end method
