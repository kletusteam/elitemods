.class public abstract Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;
.super Landroid/widget/FrameLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;
    }
.end annotation


# instance fields
.field protected a:Landroid/widget/ListView;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field protected c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;"
        }
    .end annotation
.end field

.field protected d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/o;",
            ">;"
        }
    .end annotation
.end field

.field protected e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;

.field protected g:Z

.field protected h:Z

.field protected i:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->a()V

    return-void
.end method

.method private a()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0603a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0142

    invoke-static {v0, v1, p0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0b03a9

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->a:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->a:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    return-void
.end method


# virtual methods
.method protected setAdapter(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->b:Ljava/util/List;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->f:Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;-><init>(Ljava/util/List;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->f:Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->a:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->f:Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;->a(Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method public setDayAppUsage(Lcom/xiaomi/misettings/usagestats/f/g;)V
    .locals 0

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->e:Ljava/util/List;

    return-void
.end method

.method public setWeekAppUsageList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->e:Ljava/util/List;

    return-void
.end method

.method public setWeekInfo(Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->i:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    return-void
.end method
