.class Lcom/xiaomi/misettings/usagestats/controller/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/e;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/controller/e;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-virtual {v2}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->d()Z

    move-result v3

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/controller/e;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-static {v5}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Ljava/util/Map;

    move-result-object v5

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/controller/e;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-virtual {v6}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v4, v3}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/controller/e;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-static {v5}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->d(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Ljava/util/Map;

    move-result-object v5

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/controller/e;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-virtual {v6}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v4}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/controller/e;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Z)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init: duration="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppLimitService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
