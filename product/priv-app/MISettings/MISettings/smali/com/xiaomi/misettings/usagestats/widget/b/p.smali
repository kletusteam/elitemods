.class public Lcom/xiaomi/misettings/usagestats/widget/b/p;
.super Lcom/xiaomi/misettings/usagestats/widget/b/g;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/widget/b/l;


# instance fields
.field private ba:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field private ca:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;"
        }
    .end annotation
.end field

.field private da:Ljava/lang/String;

.field private ea:J

.field private fa:Landroid/graphics/Paint;

.field private ga:F

.field private ha:F

.field private ia:Ljava/lang/String;

.field private ja:I

.field private ka:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;-><init>(Landroid/content/Context;)V

    const-string p1, ""

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->da:Ljava/lang/String;

    return-void
.end method

.method private h()Ljava/lang/String;
    .locals 9

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/p;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ia:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ia:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->j(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x410e8f5c    # 8.91f

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ga:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    const v1, 0x7f060378

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41680000    # 14.5f

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ja:I

    if-gtz v0, :cond_0

    const v0, 0x7f13041b

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const v1, 0x7f1303d3

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    int-to-long v5, v0

    sget-wide v7, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v5, v7

    invoke-static {v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x418747ae    # 16.91f

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ga:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    const v1, 0x7f06036e

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ha:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ea:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    const v0, 0x7f130421

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->da:Ljava/lang/String;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ja:I

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ia:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_1
    const-wide/16 v0, 0x0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0

    :cond_2
    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ea:J

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->da:Ljava/lang/String;

    :cond_3
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    :cond_0
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    move-object/from16 v2, p1

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_1
    sget-wide v1, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-wide v9, v1

    move v6, v3

    move-wide v1, v4

    move-wide v7, v1

    :goto_0
    iget-object v11, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v3, v11, :cond_4

    iget-object v11, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {v11}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v11

    sget-wide v13, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    cmp-long v15, v11, v13

    if-lez v15, :cond_2

    rem-long v15, v11, v13

    cmp-long v15, v15, v4

    if-lez v15, :cond_2

    rem-long v13, v11, v13

    add-long/2addr v1, v13

    :cond_2
    cmp-long v13, v9, v11

    if-lez v13, :cond_3

    cmp-long v13, v11, v4

    if-eqz v13, :cond_3

    move v6, v3

    move-wide v9, v11

    :cond_3
    add-long/2addr v7, v11

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    sget-wide v3, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    const-wide/16 v9, 0x2

    div-long/2addr v3, v9

    cmp-long v3, v1, v3

    if-ltz v3, :cond_5

    iget-object v3, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/d;

    new-instance v4, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v9

    add-long/2addr v9, v1

    invoke-virtual {v4, v9, v10}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    invoke-interface {v1, v6, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_5
    iput-wide v7, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ea:J

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-static {v1, v7, v8}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->da:Ljava/lang/String;

    :cond_6
    return-void
.end method

.method protected b()I
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method protected b(I)I
    .locals 4

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ka:J

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    iget-wide v2, p1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    const p1, 0x7f060373

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result p1

    return p1

    :cond_0
    const p1, 0x7f060372

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result p1

    return p1
.end method

.method protected b(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/p;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    goto :goto_0

    :cond_0
    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    :goto_0
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    int-to-float v1, v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ga:F

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected c()J
    .locals 6

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v4

    cmp-long v4, v1, v4

    if-gez v4, :cond_0

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v4

    cmp-long v4, v1, v4

    if-gez v4, :cond_2

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v1

    goto :goto_1

    :cond_3
    return-wide v1
.end method

.method protected c(I)Ljava/lang/String;
    .locals 5

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/e;

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ka:J

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v0

    iget-wide v3, v0, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v0, v1, v3

    if-nez v0, :cond_0

    const p1, 0x7f13041c

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->b:Landroid/util/SparseIntArray;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/f/j;->b:I

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, v2

    :goto_0
    if-ne p1, v0, :cond_4

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, p1

    goto :goto_1

    :cond_3
    add-int/lit8 v0, p1, 0x1

    :goto_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v3, 0x7f110029

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-virtual {p1, v3, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_4
    rem-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, v2

    if-ne p1, v0, :cond_5

    goto :goto_2

    :cond_5
    const-string p1, ""

    return-object p1

    :cond_6
    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, p1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    :cond_7
    add-int/2addr p1, v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    :goto_3
    return-object p1
.end method

.method protected d()F
    .locals 1

    const v0, 0x7f070509

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    return v0
.end method

.method protected d(I)F
    .locals 5

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v0

    :goto_0
    long-to-float p1, v0

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    add-int/lit8 p1, p1, 0x64

    int-to-float p1, p1

    return p1

    :cond_1
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    int-to-float v1, v0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->E:F

    sub-float/2addr v1, v2

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->N:F

    div-float/2addr p1, v4

    sub-float/2addr v3, p1

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    int-to-float p1, v0

    sub-float/2addr p1, v1

    const/high16 v2, 0x40400000    # 3.0f

    cmpl-float p1, p1, v2

    if-lez p1, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, -0x3

    int-to-float v1, v0

    :goto_1
    return v1
.end method

.method protected e()F
    .locals 1

    const v0, 0x7f0704f8

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    return v0
.end method

.method protected f(I)Landroid/graphics/Paint$Align;
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    sget-object p1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    return-object p1

    :cond_0
    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f(I)Landroid/graphics/Paint$Align;

    move-result-object p1

    return-object p1
.end method

.method public f()V
    .locals 2

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ka:J

    const v0, 0x7f0704fa

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ha:F

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    const v1, 0x7f06036e

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ha:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->fa:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    return-void
.end method

.method protected g(I)I
    .locals 5

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/e;

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ka:J

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v0

    iget-wide v3, v0, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v0, v1, v3

    if-nez v0, :cond_0

    const p1, 0x7f060373

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result p1

    return p1

    :cond_0
    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g(I)I

    move-result p1

    return p1
.end method

.method protected j(I)V
    .locals 6

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    const v0, 0x7f130404

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->c:Ljava/text/SimpleDateFormat;

    iget-wide v4, p1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v1

    invoke-virtual {p0, v0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->q:Ljava/lang/String;

    goto :goto_2

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    const v3, 0x7f1303da

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v5

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v5, p1

    sub-int/2addr v5, v2

    goto :goto_0

    :cond_1
    move v5, p1

    :goto_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v1, p1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, p1, 0x1

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->q:Ljava/lang/String;

    :goto_2
    return-void
.end method

.method protected k(I)V
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ba:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/p;->ca:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v0

    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->r:Ljava/lang/String;

    return-void
.end method
