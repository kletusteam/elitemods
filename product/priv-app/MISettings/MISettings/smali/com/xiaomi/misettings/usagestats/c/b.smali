.class public Lcom/xiaomi/misettings/usagestats/c/b;
.super Lcom/xiaomi/misettings/usagestats/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/c/b$a;
    }
.end annotation


# instance fields
.field private c:Lcom/xiaomi/misettings/usagestats/f/e;

.field private d:Lcom/xiaomi/misettings/usagestats/c/b$a;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:I

.field private k:Z

.field private l:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/c/c;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->j:I

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/c/b;->k:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/c/b;->b()Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/c/b;)Landroid/widget/LinearLayout;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->l:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/c/b;)Lcom/xiaomi/misettings/usagestats/c/b$a;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/c/b;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->f:Landroid/widget/TextView;

    return-object p0
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b023e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b0389

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b038a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b0242

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b01c6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->l:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b01b0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->i:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/usagestats/c/b$a;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)V"
        }
    .end annotation

    instance-of v0, p1, Lcom/xiaomi/misettings/usagestats/f/f;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/f;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/f/f;->a:Lcom/xiaomi/misettings/usagestats/c/b$a;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/xiaomi/misettings/usagestats/c/b$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/c/b$a;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    :cond_1
    :goto_0
    return-void
.end method

.method protected b()Landroid/view/View;
    .locals 3

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->k:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    const v2, 0x7f0e0151

    invoke-static {v0, v2, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    const v2, 0x7f0e0150

    invoke-static {v0, v2, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/c/b;->e()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->l:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/c/a;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/c/a;-><init>(Lcom/xiaomi/misettings/usagestats/c/b;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->j:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->j:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/c/b$a;->f()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->f()I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/c/b$a;->d()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/c/b$a;->b()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->a(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/b;->d:Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->d()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_4
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/c/b;->c:Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/c/b;->c:Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/b;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/c/b;->c:Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
