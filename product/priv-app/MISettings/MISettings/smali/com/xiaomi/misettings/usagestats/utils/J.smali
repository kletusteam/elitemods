.class public Lcom/xiaomi/misettings/usagestats/utils/J;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/xiaomi/misettings/usagestats/utils/J;


# instance fields
.field private b:Ljava/util/Observable;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/I;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/utils/I;-><init>(Lcom/xiaomi/misettings/usagestats/utils/J;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/J;->b:Ljava/util/Observable;

    return-void
.end method

.method public static a()Lcom/xiaomi/misettings/usagestats/utils/J;
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/J;->a:Lcom/xiaomi/misettings/usagestats/utils/J;

    if-nez v0, :cond_1

    const-class v0, Lcom/xiaomi/misettings/usagestats/utils/J;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/J;->a:Lcom/xiaomi/misettings/usagestats/utils/J;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/utils/J;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/utils/J;-><init>()V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/utils/J;->a:Lcom/xiaomi/misettings/usagestats/utils/J;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/J;->a:Lcom/xiaomi/misettings/usagestats/utils/J;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/J;->b:Ljava/util/Observable;

    invoke-virtual {v0, p1}, Ljava/util/Observable;->notifyObservers(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Ljava/util/Observer;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/J;->b:Ljava/util/Observable;

    invoke-virtual {v0, p1}, Ljava/util/Observable;->addObserver(Ljava/util/Observer;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/J;->b:Ljava/util/Observable;

    invoke-virtual {v0}, Ljava/util/Observable;->deleteObservers()V

    return-void
.end method

.method public b(Ljava/util/Observer;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/J;->b:Ljava/util/Observable;

    invoke-virtual {v0, p1}, Ljava/util/Observable;->deleteObserver(Ljava/util/Observer;)V

    return-void
.end method
