.class Lcom/xiaomi/misettings/usagestats/ui/r;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->m()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    sget-boolean v0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->a(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->b(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->c(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    invoke-virtual {v0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->s:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->d(Ljava/util/List;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->i:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a(Ljava/util/List;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->j:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->c(Ljava/util/List;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->k:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->b(Ljava/util/List;)V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/r;->a:Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;->g(Lcom/xiaomi/misettings/usagestats/ui/AppUsageListActionBarFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/ui/q;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/ui/q;-><init>(Lcom/xiaomi/misettings/usagestats/ui/r;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
