.class public Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;
.super Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;


# instance fields
.field n:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

.field private o:Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;-><init>()V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/c;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/c;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->o:Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    new-instance v0, Lcom/misettings/common/base/a;

    invoke-direct {v0, p0}, Lcom/misettings/common/base/a;-><init>(Landroid/content/Context;)V

    const-class v1, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/misettings/common/base/a;->a(Ljava/lang/String;)Lcom/misettings/common/base/a;

    const v1, 0x7f13042e

    invoke-virtual {v0, v1}, Lcom/misettings/common/base/a;->b(I)Lcom/misettings/common/base/a;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v2}, Lcom/misettings/common/base/a;->a(Landroid/os/Bundle;)Lcom/misettings/common/base/a;

    const-class v2, Lcom/xiaomi/misettings/usagestats/home/ui/PreferenceSubSettings;

    invoke-virtual {v0, v2}, Lcom/misettings/common/base/a;->a(Ljava/lang/Class;)Lcom/misettings/common/base/a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/misettings/common/base/a;->a(Landroid/app/Fragment;I)Lcom/misettings/common/base/a;

    invoke-static {p0}, Lcom/misettings/common/utils/b;->a(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {v0, v1}, Lcom/misettings/common/base/a;->b(I)Lcom/misettings/common/base/a;

    :cond_0
    invoke-virtual {v0}, Lcom/misettings/common/base/a;->b()V

    return-void
.end method

.method private a(Z)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getUnit()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/16 v0, 0x14

    if-le v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;
    .locals 0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->n:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->getWorkingDay()Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->n:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->getHoliday()Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private c(I)Ljava/lang/String;
    .locals 7

    div-int/lit8 v0, p1, 0x3c

    rem-int/lit8 p1, p1, 0x3c

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lez v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f110034

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    if-lez p1, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f110035

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    invoke-virtual {v0, v4, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private g()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->e:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;

    invoke-virtual {v2, v1}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->a(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->k:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private h()Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->o:Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->a(Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;)V

    return-object v0
.end method

.method private i()V
    .locals 6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/16 v3, 0x14

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v2, v5

    const v4, 0x7f110017

    invoke-virtual {v1, v4, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lb/e/a/b/g;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method


# virtual methods
.method public synthetic a(ILcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;)V
    .locals 3

    invoke-virtual {p2}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->getDataType()I

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    if-eq p1, v0, :cond_0

    goto :goto_3

    :cond_0
    const-string p1, "DeviceRestrictionFragme"

    const-string v1, "deleteListener"

    invoke-static {p1, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->getDataType()I

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getUnit()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-gez p1, :cond_2

    return-void

    :cond_2
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->e:Ljava/util/List;

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->k:Ljava/util/List;

    :goto_1
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getUnit()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    if-eqz v0, :cond_4

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->a:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p2, v1}, Landroidx/preference/PreferenceGroup;->d(Landroidx/preference/Preference;)Z

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->e:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->f:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p2, v1}, Landroidx/preference/PreferenceGroup;->d(Landroidx/preference/Preference;)Z

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->k:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :goto_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->g()V

    :goto_3
    return-void
.end method

.method protected a(Landroidx/preference/Preference;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->d(Landroidx/preference/Preference;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->a(Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->i()V

    return-void

    :cond_0
    const/16 v0, 0x4ec

    const/16 v1, 0x1c2

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->d(Landroidx/preference/Preference;)Z

    move-result p1

    invoke-virtual {p0, v0, v1, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->b(IIZ)V

    return-void
.end method

.method public a(Lmiuix/pickerwidget/widget/TimePicker;IILmiuix/pickerwidget/widget/TimePicker;IIZ)V
    .locals 2

    invoke-super/range {p0 .. p7}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->a(Lmiuix/pickerwidget/widget/TimePicker;IILmiuix/pickerwidget/widget/TimePicker;IIZ)V

    new-instance p1, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;

    invoke-direct {p1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;-><init>()V

    xor-int/lit8 p4, p7, 0x1

    invoke-virtual {p1, p4}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->setDataType(I)V

    const/4 p4, 0x2

    new-array v0, p4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 p3, 0x1

    aput-object p2, v0, p3

    const-string p2, "%d:%02d"

    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->setBeginTime(Ljava/lang/String;)V

    new-array p4, p4, [Ljava/lang/Object;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    aput-object p5, p4, v1

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    aput-object p5, p4, p3

    invoke-static {p2, p4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->setEndTime(Ljava/lang/String;)V

    invoke-direct {p0, p7}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object p2

    invoke-virtual {p2}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getUnit()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, " UnusableTime"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const-string p4, "DeviceRestrictionFragme"

    invoke-static {p4, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->h()Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;

    move-result-object p3

    invoke-virtual {p3, p2, p1}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->a(ILcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;)V

    if-eqz p7, :cond_0

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->a:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p2, p3}, Landroidx/preference/PreferenceGroup;->b(Landroidx/preference/Preference;)Z

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->e:Ljava/util/List;

    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->f:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p2, p3}, Landroidx/preference/PreferenceGroup;->b(Landroidx/preference/Preference;)Z

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->k:Ljava/util/List;

    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-direct {p0, p7}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object p2

    invoke-virtual {p2}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getUnit()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lmiuix/pickerwidget/widget/TimePicker;IIZ)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->a(Lmiuix/pickerwidget/widget/TimePicker;IIZ)V

    const/4 p1, 0x0

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f130412

    invoke-static {p2, p3, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    invoke-direct {p0, p4}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object p4

    invoke-virtual {p4, p2, p3}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->setDurationPerDayByHourMin(II)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->c:Lmiuix/preference/TextPreference;

    const/4 p3, 0x1

    invoke-direct {p0, p3}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object p3

    invoke-virtual {p3}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getTotalMin()I

    move-result p3

    invoke-direct {p0, p3}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->c(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lmiuix/preference/TextPreference;->setText(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->h:Lmiuix/preference/TextPreference;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getTotalMin()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->c(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lmiuix/preference/TextPreference;->setText(Ljava/lang/String;)V

    return-void
.end method

.method protected b(Landroidx/preference/Preference;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->d(Landroidx/preference/Preference;)Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getHour()I

    move-result v1

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getMin()I

    move-result v0

    invoke-virtual {p0, v1, v0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->a(IIZ)V

    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroidx/appcompat/app/ActionBar;->o()V

    invoke-virtual {v0, p1}, Landroidx/appcompat/app/ActionBar;->a(Ljava/lang/CharSequence;)V

    const/16 p1, 0xc

    invoke-virtual {v0, p1}, Landroidx/appcompat/app/ActionBar;->b(I)V

    :cond_0
    return-void
.end method

.method protected c(Landroidx/preference/Preference;)V
    .locals 0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->a(Landroid/content/Context;)V

    return-void
.end method

.method protected f()V
    .locals 6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/greenguard/c/a/i;->a(Landroid/content/Context;)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    move-result-object v0

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lb/e/a/b/c;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DeviceRestrictionFragme"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->n:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->b:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->getWorkingDay()Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->isEnable()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->g:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->getHoliday()Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->isEnable()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->c:Lmiuix/preference/TextPreference;

    invoke-virtual {v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getTotalMin()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lmiuix/preference/TextPreference;->setText(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->h:Lmiuix/preference/TextPreference;

    invoke-virtual {v3}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getTotalMin()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->c(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmiuix/preference/TextPreference;->setText(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->c:Lmiuix/preference/TextPreference;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->b:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->d:Lmiuix/preference/TextPreference;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->b:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->h:Lmiuix/preference/TextPreference;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->g:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->i:Lmiuix/preference/TextPreference;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->g:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroidx/preference/Preference;->setEnabled(Z)V

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getUnit()Ljava/util/List;

    move-result-object v1

    move v3, v2

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->h()Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;

    move-result-object v4

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;

    invoke-virtual {v4, v3, v5}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->a(ILcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;)V

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;

    invoke-virtual {v5, v2}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->setDataType(I)V

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->o:Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;

    invoke-virtual {v4, v5}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->a(Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;)V

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->b:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v5

    invoke-virtual {v4, v5}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->a:Landroidx/preference/PreferenceCategory;

    invoke-virtual {v5, v4}, Landroidx/preference/PreferenceGroup;->b(Landroidx/preference/Preference;)Z

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->e:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getUnit()Ljava/util/List;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->h()Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;

    move-result-object v3

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;

    invoke-virtual {v4, v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->setDataType(I)V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->o:Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;

    invoke-virtual {v3, v4}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->a(Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;)V

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;

    invoke-virtual {v3, v2, v4}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->a(ILcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;)V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->g:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->f:Landroidx/preference/PreferenceCategory;

    invoke-virtual {v4, v3}, Landroidx/preference/PreferenceGroup;->b(Landroidx/preference/Preference;)Z

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->k:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->g()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f13042e

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->n:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->getWorkingDay()Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->b:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->setEnable(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->n:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->getHoliday()Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->g:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->setEnable(Z)V

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "save:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->n:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    invoke-static {v1}, Lb/e/a/b/c;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DeviceRestrictionFragme"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/miui/greenguard/c/a/i;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionFragment;->n:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    invoke-direct {v0, v1, v2}, Lcom/miui/greenguard/c/a/i;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;)V

    invoke-virtual {v0}, Lcom/miui/greenguard/c/a/a/d;->c()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Lmiuix/preference/PreferenceFragment;->onStop()V

    return-void
.end method
