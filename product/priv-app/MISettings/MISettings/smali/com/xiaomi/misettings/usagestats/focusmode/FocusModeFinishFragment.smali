.class public Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;
.super Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;


# instance fields
.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Lcom/airbnb/lottie/LottieAnimationView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Landroid/widget/ImageView;

.field private q:Lcom/xiaomi/misettings/usagestats/focusmode/b/e;

.field private r:Landroid/animation/ValueAnimator;

.field private s:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

.field private t:I

.field private u:Landroid/animation/ValueAnimator;

.field private v:Ljava/text/SimpleDateFormat;

.field private w:Landroid/os/Handler;

.field private x:Landroid/os/Handler;

.field private y:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->t:I

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->w:Landroid/os/Handler;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->x:Landroid/os/Handler;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/m;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/m;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->y:Ljava/lang/Runnable;

    return-void
.end method

.method private a(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->a()V

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->e()V

    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->f()V

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->g()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->r()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;)Lcom/airbnb/lottie/LottieAnimationView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->k:Lcom/airbnb/lottie/LottieAnimationView;

    return-object p0
.end method

.method private b(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->c()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->p:Landroid/widget/ImageView;

    return-object p0
.end method

.method private c(I)V
    .locals 3

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v1, 0x5

    if-le p1, v1, :cond_1

    :cond_0
    move p1, v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "images_lv"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sweep"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ".json"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->k:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->k:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0, p1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->k:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->c(Lcom/airbnb/lottie/LottieAnimationView;)V

    return-void
.end method

.method private c(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->d()V

    :cond_0
    return-void
.end method

.method private g(Landroid/view/View;)V
    .locals 1

    const v0, 0x7f0b0210

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->e:Landroid/widget/TextView;

    const v0, 0x7f0b0217

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->f:Landroid/widget/TextView;

    const v0, 0x7f0b016a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->j:Landroid/widget/TextView;

    const v0, 0x7f0b01b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->k:Lcom/airbnb/lottie/LottieAnimationView;

    const v0, 0x7f0b01b5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->l:Landroid/widget/TextView;

    const v0, 0x7f0b0214

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->m:Landroid/widget/TextView;

    const v0, 0x7f0b0185

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->g:Landroid/widget/TextView;

    const v0, 0x7f0b01f8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->h:Landroid/widget/TextView;

    const v0, 0x7f0b018e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->i:Landroid/widget/TextView;

    const v0, 0x7f0b01bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->n:Landroid/view/View;

    const v0, 0x7f0b01ea

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->o:Landroid/view/View;

    const v0, 0x7f0b0248

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->p:Landroid/widget/ImageView;

    return-void
.end method

.method private m()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v1, 0x2ee0

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->u:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->u:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/p;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/p;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->u:Landroid/animation/ValueAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->u:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private n()V
    .locals 4

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->s:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    if-nez v0, :cond_1

    const v0, 0x7f03001b

    const/16 v1, 0x18

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a(IILandroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b;->a(Landroid/widget/ImageView;)Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->s:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->s:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->a()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->w:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/o;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/o;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;)V

    const-wide/16 v2, 0x2ee0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private o()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v1, 0x7d0

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->r:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->r:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/n;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/n;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->r:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->r:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private p()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->o:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->n()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->m()V

    return-void
.end method

.method private q()V
    .locals 9

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->x:Landroid/os/Handler;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->y:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->n(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11002b

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->totalTime:I

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v6, v8

    invoke-virtual {v2, v3, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->m:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f110027

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->totalTime:I

    new-array v6, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v2, v3, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11002e

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->wakeUps:I

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->h:Landroid/widget/TextView;

    iget-wide v2, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->startTime:J

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->i:Landroid/widget/TextView;

    iget-wide v2, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->endTime:J

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->g:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->v:Ljava/text/SimpleDateFormat;

    iget-wide v3, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->date:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->q:Lcom/xiaomi/misettings/usagestats/focusmode/b/e;

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->a(Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->q:Lcom/xiaomi/misettings/usagestats/focusmode/b/e;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->d()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->r()V

    return-void
.end method

.method private r()V
    .locals 7

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->p()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->r(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->l:Landroid/widget/TextView;

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f1303fc

    goto :goto_0

    :cond_0
    const v2, 0x7f1303fb

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v2

    int-to-long v3, v0

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v3, v5

    invoke-static {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e0084

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method protected k()Ljava/lang/String;
    .locals 1

    const-string v0, "FocusModeFinishFragment"

    return-object v0
.end method

.method protected l()Lcom/xiaomi/misettings/usagestats/focusmode/b/a;
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->q:Lcom/xiaomi/misettings/usagestats/focusmode/b/e;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->q:Lcom/xiaomi/misettings/usagestats/focusmode/b/e;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusBaseNoActionbarFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object p1

    check-cast p1, Ljava/text/SimpleDateFormat;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->v:Ljava/text/SimpleDateFormat;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->v:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    const v1, 0x7f1303cd

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->x:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->k:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->a(Lcom/airbnb/lottie/LottieAnimationView;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->r:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onStart()V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->t:I

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->c(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->o()V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onStop()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->k:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->b(Lcom/airbnb/lottie/LottieAnimationView;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->r:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->pause()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->g(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishFragment;->q()V

    return-void
.end method
