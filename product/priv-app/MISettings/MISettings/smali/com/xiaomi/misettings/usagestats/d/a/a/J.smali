.class public Lcom/xiaomi/misettings/usagestats/d/a/a/J;
.super Lcom/xiaomi/misettings/usagestats/d/a/a/s;


# instance fields
.field private c:[Ljava/lang/String;

.field private d:Lcom/xiaomi/misettings/usagestats/d/a/a;

.field private e:Z

.field private f:Lmiuix/slidingwidget/widget/SlidingButton;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->n:Z

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0704e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0704e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f030022

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->c:[Ljava/lang/String;

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/miui/greenguard/manager/k;->b()[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->c:[Ljava/lang/String;

    :cond_0
    const p1, 0x7f0b01fb

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->f:Lmiuix/slidingwidget/widget/SlidingButton;

    const p1, 0x7f0b01e5

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->g:Landroid/view/View;

    const p1, 0x7f0b01e6

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->h:Landroid/view/View;

    const p1, 0x7f0b01fa

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->i:Landroid/widget/TextView;

    const p1, 0x7f0b01da

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->j:Landroid/widget/TextView;

    const p1, 0x7f0b0395

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->k:Landroid/view/View;

    const p1, 0x7f0b0203

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->l:Landroid/view/View;

    const p1, 0x7f0b0399

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/view/View;->setSelected(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->l:Landroid/view/View;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/view/View;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->g:Landroid/view/View;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/view/View;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->h:Landroid/view/View;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->g()V

    return-void
.end method

.method private a(I[Ljava/lang/String;)I
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    array-length v1, p2

    if-lez v1, :cond_1

    move v1, v0

    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aget-object v3, p2, v1

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/d/d/j$a;)Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;
    .locals 2

    new-instance v0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;

    invoke-direct {v0}, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;-><init>()V

    iget-boolean v1, p1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    iput-boolean v1, v0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->enable:Z

    iget v1, p1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->a:I

    mul-int/lit8 v1, v1, 0x3c

    iput v1, v0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->continuousDuration:I

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->b:I

    mul-int/lit8 p1, p1, 0x3c

    iput p1, v0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->restTime:I

    return-object v0
.end method

.method private a(ILjava/lang/String;)V
    .locals 0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    iput p2, p1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->a:I

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    iput p2, p1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->b:I

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->i()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->h()V

    return-void
.end method

.method private a(I[Ljava/lang/String;I)V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f0e014f

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0b01cf

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiuix/pickerwidget/widget/NumberPicker;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const-string v4, "time_picker_label_minute"

    const-string v5, "miui"

    invoke-static {v3, v4, v5}, Lcom/xiaomi/misettings/f;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->setLabel(Ljava/lang/String;)V

    const v3, 0x7f1401c7

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setScrollBarStyle(I)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->setMinValue(I)V

    array-length v4, p2

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v4}, Lmiuix/pickerwidget/widget/NumberPicker;->setMaxValue(I)V

    :goto_0
    array-length v4, p2

    if-ge v3, v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "i==>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v5, p2, v3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "NewSteadyOnItemViewHold"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2, p2}, Lmiuix/pickerwidget/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Lmiuix/pickerwidget/widget/NumberPicker;->setValue(I)V

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p3

    instance-of p3, p3, Landroid/view/ViewGroup;

    if-eqz p3, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p3

    check-cast p3, Landroid/view/ViewGroup;

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    new-instance p3, Lmiuix/appcompat/app/j$a;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v4, 0x7f140007

    invoke-direct {p3, v3, v4}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;I)V

    const v3, 0x1010355

    invoke-virtual {p3, v3}, Lmiuix/appcompat/app/j$a;->a(I)Lmiuix/appcompat/app/j$a;

    const v3, 0x7f1302f1

    invoke-virtual {p3, v3}, Lmiuix/appcompat/app/j$a;->c(I)Lmiuix/appcompat/app/j$a;

    invoke-virtual {p3, v0}, Lmiuix/appcompat/app/j$a;->b(Landroid/view/View;)Lmiuix/appcompat/app/j$a;

    const v0, 0x7f1300d8

    new-instance v3, Lcom/xiaomi/misettings/usagestats/d/a/a/m;

    invoke-direct {v3, p0, p1, p2, v2}, Lcom/xiaomi/misettings/usagestats/d/a/a/m;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/J;I[Ljava/lang/String;Lmiuix/pickerwidget/widget/NumberPicker;)V

    invoke-virtual {p3, v0, v3}, Lmiuix/appcompat/app/j$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    const p1, 0x7f13029e

    invoke-virtual {p3, p1, v1}, Lmiuix/appcompat/app/j$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {p3}, Lmiuix/appcompat/app/j$a;->b()Lmiuix/appcompat/app/j;

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iput-boolean p1, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->i()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->f()V

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->i()V

    :cond_0
    return-void
.end method

.method private c()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "misettings.action.remote.FROM_STEADY_ON"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    const-string v2, ":key:remote_notify_channel"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->d()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    const-string v2, ":key:deviceId"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {v1}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v1

    invoke-virtual {v1, v0}, La/m/a/b;->a(Landroid/content/Intent;)Z

    return-void
.end method

.method private d()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "misettings.action.EXCHANGE_STEADY_ON"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, ":key:notify_channel"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {v1}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v1

    invoke-virtual {v1, v0}, La/m/a/b;->a(Landroid/content/Intent;)Z

    return-void
.end method

.method private e()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->f:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->e:Z

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->l:Landroid/view/View;

    const v1, 0x7f0805ba

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget-boolean v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->l:Landroid/view/View;

    const v2, 0x7f0805bb

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->f:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->g:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->f:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private f()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->e()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->h()V

    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->f:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/a/a/p;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/p;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/J;)V

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->l:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/a/a/q;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/q;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/J;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->g:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/a/a/n;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/n;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/J;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->h:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/a/a/o;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/o;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/J;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private h()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget v2, v2, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->a:I

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/i/h;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget v2, v2, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->b:I

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/i/h;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private i()V
    .locals 3

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->a(Lcom/xiaomi/misettings/usagestats/d/d/j$a;)Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;

    move-result-object v0

    new-instance v1, Lcom/miui/greenguard/params/PostMandatoryRestParam;

    invoke-direct {v1}, Lcom/miui/greenguard/params/PostMandatoryRestParam;-><init>()V

    iget v2, v0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->continuousDuration:I

    invoke-virtual {v1, v2}, Lcom/miui/greenguard/params/PostMandatoryRestParam;->setContinuousDuration(I)V

    iget-boolean v2, v0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->enable:Z

    invoke-virtual {v1, v2}, Lcom/miui/greenguard/params/PostMandatoryRestParam;->setEnable(Z)V

    iget v0, v0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->restTime:I

    invoke-virtual {v1, v0}, Lcom/miui/greenguard/params/PostMandatoryRestParam;->setRestTime(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->c()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->e()Lcom/xiaomi/misettings/usagestats/d/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/b;->a(Lcom/miui/greenguard/params/PostMandatoryRestParam;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/miui/greenguard/c/a/j;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->a(Lcom/xiaomi/misettings/usagestats/d/d/j$a;)Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/miui/greenguard/c/a/j;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;)V

    invoke-virtual {v0}, Lcom/miui/greenguard/c/a/a/d;->c()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->d()V

    :goto_0
    return-void
.end method


# virtual methods
.method public synthetic a(I[Ljava/lang/String;Lmiuix/pickerwidget/widget/NumberPicker;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-virtual {p3}, Lmiuix/pickerwidget/widget/NumberPicker;->getValue()I

    move-result p3

    aget-object p2, p2, p3

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->a(ILjava/lang/String;)V

    return-void
.end method

.method public synthetic a(Landroid/view/View;)V
    .locals 1

    const-string p1, "NewSteadyOnItemViewHold"

    const-string v0, "setOnClickListener"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->e:Z

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->e:Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->f:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->e:Z

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->e:Z

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->a(Z)V

    return-void
.end method

.method public synthetic a(Landroid/widget/CompoundButton;Z)V
    .locals 0

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->e:Z

    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->a(Z)V

    const-string p1, "NewSteadyOnItemViewHold"

    const-string p2, "setOnPerformCheckedChangeListener"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/a/a;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-boolean p1, p2, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->n:Z

    if-eqz p1, :cond_0

    iget-object p1, p2, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->b()V

    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget-boolean p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->e:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->f()V

    return-void
.end method

.method protected b()V
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/d/d/j$a;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/i/h;->m(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/i/h;->h(Landroid/content/Context;)I

    move-result v1

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->a:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/i/h;->j(Landroid/content/Context;)I

    move-result v1

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->b:I

    return-void
.end method

.method public synthetic b(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->c:[Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->a:I

    invoke-direct {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->a(I[Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->a(I[Ljava/lang/String;I)V

    return-void
.end method

.method public synthetic c(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->c:[Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->m:Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->b:I

    invoke-direct {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->a(I[Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v1, p1, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;->a(I[Ljava/lang/String;I)V

    return-void
.end method
