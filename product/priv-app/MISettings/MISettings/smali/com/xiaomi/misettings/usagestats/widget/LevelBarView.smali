.class public Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;
.super Landroid/view/View;


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:I

.field private c:I

.field private d:I

.field private e:J

.field private f:J

.field private g:F

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->h:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->a()V

    return-void
.end method

.method private a()V
    .locals 3

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/H;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->i:Z

    const/16 v0, 0xa

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->d:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->a:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06038e

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41200000    # 10.0f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->g:F

    return-void
.end method

.method private b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_5

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->e:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->h:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->i:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->b:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->c:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->c:I

    :goto_0
    int-to-float v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->c:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_1
    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->e:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->i:Z

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->b:I

    int-to-float v3, v2

    int-to-float v4, v2

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->g:F

    sub-float/2addr v4, v5

    iget-wide v5, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->f:J

    long-to-float v5, v5

    mul-float/2addr v4, v5

    long-to-float v0, v0

    div-float/2addr v4, v0

    sub-float v6, v3, v4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->c:I

    sub-int v1, v2, v0

    int-to-float v1, v1

    cmpl-float v1, v6, v1

    if-ltz v1, :cond_2

    sub-int/2addr v2, v0

    int-to-float v1, v2

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_2
    div-int/lit8 v1, v0, 0x2

    int-to-float v7, v1

    sub-int/2addr v2, v0

    int-to-float v8, v2

    div-int/lit8 v0, v0, 0x2

    int-to-float v9, v0

    iget-object v10, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->a:Landroid/graphics/Paint;

    move-object v5, p1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_3
    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->b:I

    int-to-float v2, v2

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->g:F

    sub-float/2addr v2, v3

    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->f:J

    long-to-float v3, v3

    mul-float/2addr v2, v3

    long-to-float v0, v0

    div-float v6, v2, v0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->c:I

    div-int/lit8 v1, v0, 0x2

    int-to-float v1, v1

    cmpg-float v1, v6, v1

    if-gtz v1, :cond_4

    int-to-float v1, v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_4
    int-to-float v4, v0

    div-int/lit8 v1, v0, 0x2

    int-to-float v5, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v7, v0

    iget-object v8, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->a:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_5
    :goto_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, 0x0

    if-ne v0, v2, :cond_0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->b:I

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->b:I

    move p1, v3

    :goto_0
    if-ne v1, v2, :cond_1

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->c:I

    goto :goto_1

    :cond_1
    iget p2, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->d:I

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->c:I

    move p2, v3

    :goto_1
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public setCurrentLevel(J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->f:J

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setIsNoti(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->h:Z

    return-void
.end method

.method public setMaxLevel(J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/widget/LevelBarView;->e:J

    return-void
.end method
