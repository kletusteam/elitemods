.class public Lcom/xiaomi/misettings/usagestats/widget/b/r;
.super Lcom/xiaomi/misettings/usagestats/widget/b/g;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/widget/b/m;


# instance fields
.field protected ba:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation
.end field

.field protected ca:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private da:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/usagestats/f/g;)V
    .locals 20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    :cond_0
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    if-nez p1, :cond_1

    return-void

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/xiaomi/misettings/usagestats/f/g;->c()Ljava/util/List;

    move-result-object v1

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_2
    sget-wide v1, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-wide v9, v1

    move v1, v3

    move v2, v1

    move v8, v2

    move-wide v6, v4

    :goto_0
    iget-object v11, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    const-wide/32 v12, 0xc350

    if-ge v1, v11, :cond_6

    iget-object v11, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v11, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    sget-wide v16, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    rem-long v18, v14, v16

    cmp-long v11, v18, v4

    if-lez v11, :cond_4

    cmp-long v11, v18, v12

    if-lez v11, :cond_3

    rem-long v11, v14, v16

    sub-long v11, v16, v11

    sub-long/2addr v6, v11

    cmp-long v11, v18, v16

    if-gez v11, :cond_4

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_3
    add-long v6, v6, v18

    :cond_4
    :goto_1
    cmp-long v11, v9, v14

    if-lez v11, :cond_5

    cmp-long v11, v14, v4

    if-eqz v11, :cond_5

    move v2, v1

    move-wide v9, v14

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_6
    cmp-long v1, v6, v4

    if-lez v1, :cond_7

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long/2addr v3, v6

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    :cond_7
    if-gez v1, :cond_b

    move v1, v3

    :goto_2
    cmp-long v2, v6, v4

    if-gez v2, :cond_9

    iget-object v9, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ge v1, v9, :cond_9

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    sget-wide v14, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    cmp-long v2, v9, v14

    if-lez v2, :cond_8

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    sget-wide v14, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    rem-long/2addr v9, v14

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    sub-long/2addr v14, v9

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v2, v1, v11}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-long/2addr v6, v9

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_9
    if-gez v2, :cond_b

    if-lez v8, :cond_b

    int-to-long v1, v8

    div-long v1, v6, v1

    :goto_3
    cmp-long v8, v6, v4

    if-gez v8, :cond_b

    iget-object v8, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v3, v8, :cond_b

    iget-object v8, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v8, v8, v12

    if-lez v8, :cond_a

    iget-object v8, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sget-wide v10, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    cmp-long v8, v8, v10

    if-gez v8, :cond_a

    iget-object v8, v0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    add-long/2addr v9, v1

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v3, v9}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    sub-long/2addr v6, v1

    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_b
    :goto_4
    return-void
.end method

.method protected b()I
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected b(I)I
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ne p1, v0, :cond_1

    const p1, 0x7f060374

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result p1

    return p1

    :cond_1
    const p1, 0x7f060372

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result p1

    return p1
.end method

.method protected b(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method protected c()J
    .locals 6

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v4

    cmp-long v4, v1, v4

    if-gez v4, :cond_0

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v1, v4

    if-gez v4, :cond_2

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_1

    :cond_3
    return-wide v1
.end method

.method protected c(I)Ljava/lang/String;
    .locals 5

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ne p1, v1, :cond_1

    const p1, 0x7f13041c

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->b:Landroid/util/SparseIntArray;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/f/j;->b:I

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, v2

    :goto_1
    if-ne p1, v0, :cond_5

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, p1

    goto :goto_2

    :cond_4
    add-int/lit8 v0, p1, 0x1

    :goto_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v3, 0x7f110029

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-virtual {p1, v3, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_5
    rem-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, v2

    if-ne p1, v0, :cond_6

    goto :goto_3

    :cond_6
    const-string p1, ""

    return-object p1

    :cond_7
    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v0, p1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    :cond_8
    add-int/2addr p1, v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    :goto_4
    return-object p1
.end method

.method public c(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method protected d()F
    .locals 1

    const v0, 0x7f0704da

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    return v0
.end method

.method protected d(I)F
    .locals 5

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    long-to-float p1, v0

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    add-int/lit8 p1, p1, 0x64

    int-to-float p1, p1

    return p1

    :cond_1
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    int-to-float v1, v0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->E:F

    sub-float/2addr v1, v2

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->N:F

    div-float/2addr p1, v4

    sub-float/2addr v3, p1

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    int-to-float p1, v0

    sub-float/2addr p1, v1

    const/high16 v2, 0x40400000    # 3.0f

    cmpl-float p1, p1, v2

    if-lez p1, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, -0x3

    int-to-float v1, v0

    :goto_1
    return v1
.end method

.method protected e()F
    .locals 1

    const v0, 0x7f0704f8

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    return v0
.end method

.method protected f(I)Landroid/graphics/Paint$Align;
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    sget-object p1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    return-object p1

    :cond_0
    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f(I)Landroid/graphics/Paint$Align;

    move-result-object p1

    return-object p1
.end method

.method public f()V
    .locals 2

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f()V

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->da:Landroid/text/TextPaint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->da:Landroid/text/TextPaint;

    const v1, 0x7f060384

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->da:Landroid/text/TextPaint;

    const v1, 0x7f0704db

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->da:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->da:Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    return-void
.end method

.method protected g(I)I
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ne p1, v0, :cond_1

    const p1, 0x7f060373

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result p1

    return p1

    :cond_1
    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g(I)I

    move-result p1

    return p1
.end method

.method protected j(I)V
    .locals 6

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    const v0, 0x7f130404

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->c:Ljava/text/SimpleDateFormat;

    iget-wide v4, p1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v1

    invoke-virtual {p0, v0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->q:Ljava/lang/String;

    goto :goto_2

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    const v3, 0x7f1303da

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v5

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v5, p1

    sub-int/2addr v5, v2

    goto :goto_0

    :cond_1
    move v5, p1

    :goto_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v1, p1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, p1, 0x1

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->q:Ljava/lang/String;

    :goto_2
    return-void
.end method

.method protected k(I)V
    .locals 8

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ba:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/r;->ca:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    rem-long v4, v0, v2

    const-wide/32 v6, 0xc350

    cmp-long p1, v4, v6

    if-lez p1, :cond_1

    add-long/2addr v0, v2

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->r:Ljava/lang/String;

    return-void
.end method
