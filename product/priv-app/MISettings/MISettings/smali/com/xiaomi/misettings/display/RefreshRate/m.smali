.class public Lcom/xiaomi/misettings/display/RefreshRate/m;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/xiaomi/misettings/display/RefreshRate/m;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/graphics/Bitmap;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/xiaomi/misettings/display/RefreshRate/m;->e:Z

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/xiaomi/misettings/display/RefreshRate/e;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/m;->b:Landroid/graphics/Bitmap;

    iput p4, p0, Lcom/xiaomi/misettings/display/RefreshRate/m;->d:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/m;->c:Ljava/lang/String;

    iput p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/m;->d:I

    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/display/RefreshRate/m;)I
    .locals 1

    iget-object p1, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result p1

    return p1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/display/RefreshRate/m;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/m;->a(Lcom/xiaomi/misettings/display/RefreshRate/m;)I

    move-result p1

    return p1
.end method
