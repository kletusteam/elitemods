.class public Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitSettings;
.super Lcom/xiaomi/misettings/base/BaseFragment;


# instance fields
.field private c:Lcom/xiaomi/misettings/widget/CustomListView;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/xiaomi/misettings/usagestats/devicelimit/k;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    return-void
.end method

.method private l()V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitSettings;->d:Ljava/util/List;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitSettings;->d:Ljava/util/List;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/a;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitSettings;->d:Ljava/util/List;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/a;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/a;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e007e

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitSettings;->e:Lcom/xiaomi/misettings/usagestats/devicelimit/k;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->a()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Landroid/os/Message;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitSettings;->l()V

    new-instance p2, Lcom/xiaomi/misettings/usagestats/devicelimit/k;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitSettings;->d:Ljava/util/List;

    invoke-direct {p2, v0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/k;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitSettings;->e:Lcom/xiaomi/misettings/usagestats/devicelimit/k;

    const p2, 0x7f0b0188

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/widget/CustomListView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitSettings;->c:Lcom/xiaomi/misettings/widget/CustomListView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitSettings;->c:Lcom/xiaomi/misettings/widget/CustomListView;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitSettings;->e:Lcom/xiaomi/misettings/usagestats/devicelimit/k;

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
