.class public Lcom/xiaomi/misettings/widget/DelTextView;
.super Landroid/widget/TextView;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/Paint;

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/high16 p1, 0x1a000000

    iput p1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->j:I

    const/4 p1, -0x1

    iput p1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->k:I

    const/high16 p1, 0x7000000

    iput p1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->l:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 p1, 0x1a000000

    iput p1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->j:I

    const/4 p1, -0x1

    iput p1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->k:I

    const/high16 p1, 0x7000000

    iput p1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->l:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->a()V

    return-void
.end method

.method private a()V
    .locals 6

    sget-boolean v0, Lb/b/a/a;->r:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    :try_start_0
    new-array v0, v1, [Landroid/view/View;

    const/4 v3, 0x0

    aput-object p0, v0, v3

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v4

    invoke-interface {v4, v2, v2, v2, v2}, Lmiuix/animation/m;->a(FFFF)Lmiuix/animation/m;

    invoke-interface {v0}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v0

    new-array v3, v3, [Lmiuix/animation/a/a;

    invoke-interface {v0, p0, v3}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "DelTextView"

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f060067

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->j:I

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f060065

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->k:I

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f060066

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->l:I

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v0

    float-to-int v2, v2

    iput v2, p0, Lcom/xiaomi/misettings/widget/DelTextView;->d:I

    const/high16 v2, 0x40a00000    # 5.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->e:I

    iput v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->c:I

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070112

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->f:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->g:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->g:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->g:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/widget/DelTextView;->j:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->g:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/widget/DelTextView;->c:I

    int-to-float v2, v2

    iget v3, p0, Lcom/xiaomi/misettings/widget/DelTextView;->d:I

    int-to-float v3, v3

    iget v4, p0, Lcom/xiaomi/misettings/widget/DelTextView;->e:I

    int-to-float v4, v4

    iget v5, p0, Lcom/xiaomi/misettings/widget/DelTextView;->j:I

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->h:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->h:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->i:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->i:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->i:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->j:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private getShadowX()I
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->c:I

    iget v1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->d:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private getShadowY()I
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->c:I

    iget v1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->e:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowX()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/xiaomi/misettings/widget/DelTextView;->a:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowX()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowY()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->b:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowY()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/TextView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowX()I

    move-result v0

    int-to-float v2, v0

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowY()I

    move-result v0

    int-to-float v3, v0

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->a:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowX()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v4, v0

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->b:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowY()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v5, v0

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->f:I

    int-to-float v6, v0

    int-to-float v7, v0

    iget-object v8, p0, Lcom/xiaomi/misettings/widget/DelTextView;->g:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    invoke-virtual {p0}, Landroid/widget/TextView;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->h:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->l:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->h:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->k:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowX()I

    move-result v0

    int-to-float v2, v0

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowY()I

    move-result v0

    int-to-float v3, v0

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->a:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowX()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v4, v0

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->b:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowY()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v5, v0

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->f:I

    int-to-float v6, v0

    int-to-float v7, v0

    iget-object v8, p0, Lcom/xiaomi/misettings/widget/DelTextView;->h:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowX()I

    move-result v0

    int-to-float v2, v0

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowY()I

    move-result v0

    int-to-float v3, v0

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->a:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowX()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v4, v0

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->b:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowY()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v5, v0

    iget v0, p0, Lcom/xiaomi/misettings/widget/DelTextView;->f:I

    int-to-float v6, v0

    int-to-float v7, v0

    iget-object v8, p0, Lcom/xiaomi/misettings/widget/DelTextView;->i:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowX()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowY()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    invoke-virtual {p0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result p1

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowX()I

    move-result p2

    mul-int/lit8 p2, p2, 0x2

    add-int/2addr p1, p2

    iput p1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->a:I

    invoke-virtual {p0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result p1

    invoke-direct {p0}, Lcom/xiaomi/misettings/widget/DelTextView;->getShadowY()I

    move-result p2

    mul-int/lit8 p2, p2, 0x2

    add-int/2addr p1, p2

    iput p1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->b:I

    iget p1, p0, Lcom/xiaomi/misettings/widget/DelTextView;->a:I

    iget p2, p0, Lcom/xiaomi/misettings/widget/DelTextView;->b:I

    invoke-virtual {p0, p1, p2}, Landroid/widget/TextView;->setMeasuredDimension(II)V

    return-void
.end method
