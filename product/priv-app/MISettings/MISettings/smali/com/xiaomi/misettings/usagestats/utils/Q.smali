.class public Lcom/xiaomi/misettings/usagestats/utils/Q;
.super Ljava/lang/Object;


# static fields
.field private static a:I = -0x1

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/N;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/utils/N;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/Q;->b:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/O;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/utils/O;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/Q;->c:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/P;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/utils/P;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/Q;->d:Ljava/util/List;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/i;
    .locals 9

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v6

    new-instance v8, Lcom/xiaomi/misettings/usagestats/f/i;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/j;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v6, v7}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-direct {v8, v0}, Lcom/xiaomi/misettings/usagestats/f/i;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object v0, p0

    move-wide v1, v6

    move-object v5, v8

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/misettings/usagestats/utils/Q;->a(Landroid/content/Context;JJLcom/xiaomi/misettings/usagestats/f/i;)Z

    move-result p0

    if-nez p0, :cond_0

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Ops! Fail to load device usage today:"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v6, v7}, Lcom/xiaomi/misettings/usagestats/utils/L;->e(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "DeviceUsageStatsFactory"

    invoke-static {v0, p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object v8
.end method

.method private static a()V
    .locals 3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-ge v0, v1, :cond_0

    sget v0, Lcom/xiaomi/misettings/usagestats/utils/Q;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    :try_start_0
    const-class v0, Landroid/app/usage/UsageEvents$Event;

    const-string v1, "KEY_GUARD_LOCK"

    invoke-static {v0, v1}, Lb/c/a/b/b;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/xiaomi/misettings/usagestats/utils/Q;->a:I

    const-string v0, "DeviceUsageStatsFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUnLockEvent_O: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v2, Lcom/xiaomi/misettings/usagestats/utils/Q;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/16 v0, 0x66

    sput v0, Lcom/xiaomi/misettings/usagestats/utils/Q;->a:I

    :cond_0
    :goto_0
    return-void
.end method

.method public static a(Landroid/content/Context;JJLcom/xiaomi/misettings/usagestats/f/i;)Z
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/utils/ca;->a(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents;

    move-result-object p1

    invoke-static {p0, p1, p5}, Lcom/xiaomi/misettings/usagestats/utils/Q;->a(Landroid/content/Context;Landroid/app/usage/UsageEvents;Lcom/xiaomi/misettings/usagestats/f/i;)Z

    move-result p0

    return p0
.end method

.method private static a(Landroid/content/Context;Landroid/app/usage/UsageEvents;Lcom/xiaomi/misettings/usagestats/f/i;)Z
    .locals 6

    if-eqz p1, :cond_5

    if-nez p2, :cond_0

    goto :goto_2

    :cond_0
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/Q;->a()V

    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Landroid/app/usage/UsageEvents$Event;

    invoke-direct {v1}, Landroid/app/usage/UsageEvents$Event;-><init>()V

    invoke-virtual {p1, v1}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1c

    if-ge v3, v4, :cond_2

    sget v3, Lcom/xiaomi/misettings/usagestats/utils/Q;->a:I

    goto :goto_1

    :cond_2
    const/16 v3, 0x12

    :goto_1
    if-ne v2, v3, :cond_1

    invoke-virtual {v1}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J

    move-result-wide v2

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/u;->a(Landroid/content/Context;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J

    move-result-wide v1

    invoke-virtual {p2, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/i;->a(J)V

    goto :goto_0

    :cond_4
    invoke-virtual {p2, v0}, Lcom/xiaomi/misettings/usagestats/f/i;->a(Landroid/util/ArrayMap;)V

    const/4 p0, 0x1

    return p0

    :cond_5
    :goto_2
    const-string p0, "DeviceUsageStatsFactory"

    const-string p1, "analysis()...... return since events = null or ret is null."

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x0

    return p0
.end method

.method public static b(Landroid/content/Context;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x1

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(Z)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/misettings/usagestats/f/j;

    new-instance v12, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-direct {v12, v5}, Lcom/xiaomi/misettings/usagestats/f/i;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/xiaomi/misettings/usagestats/a/o;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/o;

    move-result-object v6

    invoke-virtual {v6, v5, v12}, Lcom/xiaomi/misettings/usagestats/a/o;->a(Lcom/xiaomi/misettings/usagestats/f/j;Lcom/xiaomi/misettings/usagestats/f/i;)Z

    move-result v6

    const-string v13, "DeviceUsageStatsFactory"

    if-eqz v6, :cond_0

    const-string v5, "loadDeviceUsageWeek: load device info from db"

    invoke-static {v13, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    iget-wide v7, v5, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    sget-wide v9, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    add-long/2addr v9, v7

    move-object v6, p0

    move-object v11, v12

    invoke-static/range {v6 .. v11}, Lcom/xiaomi/misettings/usagestats/utils/Q;->a(Landroid/content/Context;JJLcom/xiaomi/misettings/usagestats/f/i;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v5, "loadDeviceUsageWeek: load device info from os"

    invoke-static {v13, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Ops! Fail to load day device usage for:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v7, v5, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v7, v8}, Lcom/xiaomi/misettings/usagestats/utils/L;->e(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v13, v5}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/a/o;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/o;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/xiaomi/misettings/usagestats/a/o;->a(Ljava/util/List;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/Q;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/i;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
