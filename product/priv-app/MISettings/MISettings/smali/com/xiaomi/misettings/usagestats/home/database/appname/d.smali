.class public final Lcom/xiaomi/misettings/usagestats/home/database/appname/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/home/database/appname/b;


# instance fields
.field private final a:Landroidx/room/v;

.field private final b:Landroidx/room/c;


# direct methods
.method public constructor <init>(Landroidx/room/v;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/d;->a:Landroidx/room/v;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/database/appname/c;

    invoke-direct {v0, p0, p1}, Lcom/xiaomi/misettings/usagestats/home/database/appname/c;-><init>(Lcom/xiaomi/misettings/usagestats/home/database/appname/d;Landroidx/room/v;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/d;->b:Landroidx/room/c;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    const-string v1, "SELECT appName FROM app_name WHERE packageName = ?"

    invoke-static {v1, v0}, Landroidx/room/y;->a(Ljava/lang/String;I)Landroidx/room/y;

    move-result-object v1

    if-nez p1, :cond_0

    invoke-virtual {v1, v0}, Landroidx/room/y;->a(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v0, p1}, Landroidx/room/y;->a(ILjava/lang/String;)V

    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/d;->a:Landroidx/room/v;

    invoke-virtual {p1, v1}, Landroidx/room/v;->a(La/p/a/e;)Landroid/database/Cursor;

    move-result-object p1

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Landroidx/room/y;->b()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Landroidx/room/y;->b()V

    throw v0
.end method

.method public a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const-string v1, "SELECT packageName FROM app_name"

    invoke-static {v1, v0}, Landroidx/room/y;->a(Ljava/lang/String;I)Landroidx/room/y;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/d;->a:Landroidx/room/v;

    invoke-virtual {v2, v1}, Landroidx/room/v;->a(La/p/a/e;)Landroid/database/Cursor;

    move-result-object v2

    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Landroidx/room/y;->b()V

    return-object v3

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Landroidx/room/y;->b()V

    throw v0
.end method

.method public varargs a([Lcom/xiaomi/misettings/usagestats/home/database/appname/a;)[Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/d;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/d;->b:Landroidx/room/c;

    invoke-virtual {v0, p1}, Landroidx/room/c;->a([Ljava/lang/Object;)[Ljava/lang/Long;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/d;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/d;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    return-object p1

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/d;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    throw p1
.end method
