.class public final enum Lcom/xiaomi/misettings/usagestats/d/d/j$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/d/d/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/xiaomi/misettings/usagestats/d/d/j$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

.field public static final enum b:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

.field public static final enum c:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

.field private static final synthetic d:[Lcom/xiaomi/misettings/usagestats/d/d/j$b;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    const/4 v1, 0x0

    const-string v2, "UPDATE_BTN"

    invoke-direct {v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/d/d/j$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->a:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    const/4 v2, 0x1

    const-string v3, "CLOSE"

    invoke-direct {v0, v3, v2}, Lcom/xiaomi/misettings/usagestats/d/d/j$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->b:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    const/4 v3, 0x2

    const-string v4, "OPEN_SWITCH"

    invoke-direct {v0, v4, v3}, Lcom/xiaomi/misettings/usagestats/d/d/j$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->c:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    sget-object v4, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->a:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    aput-object v4, v0, v1

    sget-object v1, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->b:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->c:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    aput-object v1, v0, v3

    sput-object v0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->d:[Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/d/d/j$b;
    .locals 1

    const-class v0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    return-object p0
.end method

.method public static values()[Lcom/xiaomi/misettings/usagestats/d/d/j$b;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->d:[Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    invoke-virtual {v0}, [Lcom/xiaomi/misettings/usagestats/d/d/j$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    return-object v0
.end method
