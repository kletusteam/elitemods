.class public Lcom/xiaomi/misettings/usagestats/focusmode/b/b;
.super Lcom/xiaomi/misettings/usagestats/d/b/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/usagestats/d/b/b<",
        "Lb/c/a/a/a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/b/b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object p1

    check-cast p1, Ljava/text/SimpleDateFormat;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/b;->c:Ljava/text/SimpleDateFormat;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/b;->c:Ljava/text/SimpleDateFormat;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    const v1, 0x7f1303cd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    const p1, 0x7f0b0196

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/b;->b:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V
    .locals 2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/b;->b:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/b;->c:Ljava/text/SimpleDateFormat;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->getDate()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V
    .locals 0

    check-cast p2, Lb/c/a/a/a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/focusmode/b/b;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V

    return-void
.end method
