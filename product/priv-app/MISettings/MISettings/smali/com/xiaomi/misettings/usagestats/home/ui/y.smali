.class Lcom/xiaomi/misettings/usagestats/home/ui/y;
.super Ljava/lang/Object;

# interfaces
.implements Lb/e/b/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/e/b/b/a<",
        "Lcom/miui/greenguard/result/DashBordResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/greenguard/params/GetDashBordParam;

.field final synthetic b:J

.field final synthetic c:I

.field final synthetic d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;Lcom/miui/greenguard/params/GetDashBordParam;JI)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->a:Lcom/miui/greenguard/params/GetDashBordParam;

    iput-wide p3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->b:J

    iput p5, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/miui/greenguard/result/DashBordResult;)V
    .locals 4

    invoke-virtual {p1}, Lcom/miui/greenguard/result/DashBordResult;->getData()Lcom/miui/greenguard/entity/DashBordBean;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->a:Lcom/miui/greenguard/params/GetDashBordParam;

    invoke-virtual {v0}, Lcom/miui/greenguard/params/GetDashBordParam;->getDate()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/miui/greenguard/entity/DashBordBean;->setDate(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    iget-boolean v0, v0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->setDateTypeWeek()V

    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/miui/greenguard/entity/DashBordBean;->setCurrentDate(J)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/miui/greenguard/entity/DashBordBean;->setCurrentDate(J)V

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/miui/greenguard/entity/DashBordBean;->setSelectTimeStamp(J)V

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->setDateTypeDaily()V

    :goto_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->c:I

    invoke-virtual {p1, v0}, Lcom/miui/greenguard/entity/DashBordBean;->setSelectIndex(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->a(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;Lcom/miui/greenguard/entity/DashBordBean;)Lcom/miui/greenguard/entity/DashBordBean;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->a(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/miui/greenguard/entity/DashBordBean;->setFamilyBean(Lcom/miui/greenguard/entity/FamilyBean;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->b(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;Lcom/miui/greenguard/entity/DashBordBean;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dafultDataIndex"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "RemoteSubContentFragment"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    iget-object v2, v1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    iget-boolean v3, v3, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    invoke-static {v1, p1, v3}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a(Landroid/content/Context;Lcom/miui/greenguard/entity/DashBordBean;Z)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v2, p1, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->b(Ljava/util/List;I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->b(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)Lcom/miui/greenguard/result/DeviceLimitResult;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    iget-object v0, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->b(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)Lcom/miui/greenguard/result/DeviceLimitResult;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/result/DeviceLimitResult;->isEnable()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Z)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->c(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->d(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->k()V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/miui/greenguard/result/DashBordResult;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/y;->a(Lcom/miui/greenguard/result/DashBordResult;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "netError"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "RemoteSubContentFragment"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/y;->d:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->e(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V

    return-void
.end method
