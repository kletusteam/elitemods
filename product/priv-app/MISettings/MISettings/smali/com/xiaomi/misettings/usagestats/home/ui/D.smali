.class Lcom/xiaomi/misettings/usagestats/home/ui/D;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->o()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/D;->a:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "misettings.action.remote.FROM_STEADY_ON"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, ":key:deviceId"

    invoke-virtual {p2, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/D;->a:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->a(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean;->getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/D;->a:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    const-string v0, ":key:remote_notify_channel"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Lcom/xiaomi/misettings/usagestats/d/d/j$a;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string p2, "misettings.action.switch.DEVICE"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/D;->a:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a()V

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/D;->a:Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l()V

    :cond_3
    :goto_0
    return-void
.end method
