.class Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$d;
.super Lcom/xiaomi/misettings/tools/i;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/tools/i<",
        "Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;",
        "Lcom/xiaomi/misettings/usagestats/f/g;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/tools/i;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const-string v0, "SubContentFragment"

    const-string v1, "notifyTodayUsageData TaskRunnableImpl"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/tools/i;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->l(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/tools/i;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->m(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/tools/i;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;

    invoke-static {v2, v1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->a(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;Lcom/xiaomi/misettings/usagestats/f/g;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TaskRunnableImpl load data error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method
