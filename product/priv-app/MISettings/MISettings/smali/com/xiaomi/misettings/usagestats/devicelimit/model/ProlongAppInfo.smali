.class public Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:J

.field public d:I

.field public e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/c;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/c;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->a:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->b:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->c:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->a:Ljava/lang/String;

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->b:I

    iput-wide p3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->c:J

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)I
    .locals 6

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->a:Ljava/lang/String;

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->c:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;JJ)J

    move-result-wide v0

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    div-long/2addr v0, v2

    long-to-int p1, v0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->b:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->d:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->d:I

    return p1
.end method

.method public c()Z
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->b:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
