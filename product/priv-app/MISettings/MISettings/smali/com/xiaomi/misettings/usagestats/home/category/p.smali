.class public Lcom/xiaomi/misettings/usagestats/home/category/p;
.super Landroidx/recyclerview/widget/RecyclerView$a;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/home/category/b/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/xiaomi/misettings/usagestats/d/b/b<",
        "Lb/c/a/a/a;",
        ">;>;",
        "Lcom/xiaomi/misettings/usagestats/home/category/b/a;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/home/category/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

.field private d:Landroidx/recyclerview/widget/RecyclerView;

.field private e:Landroidx/recyclerview/widget/RecyclerView$t;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/home/category/c/b;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->d:Landroidx/recyclerview/widget/RecyclerView;

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    return-void
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/home/category/c/b;)I
    .locals 5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    iget-object v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    iget-object v4, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v2, v2, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->e:Z

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return v0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/category/p;Lcom/xiaomi/misettings/usagestats/home/category/c/b;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/c/b;)I

    move-result p0

    return p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/category/p;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    return-object p0
.end method

.method private a(Landroidx/recyclerview/widget/RecyclerView$t;Z)V
    .locals 6

    instance-of v0, p1, Lcom/xiaomi/misettings/usagestats/home/category/a/c;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const v0, 0x7f0b01e2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p2, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationZ(F)V

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    if-eqz p2, :cond_2

    new-instance p2, Lmiuix/animation/b/a;

    const-string v3, "itemScaleShow"

    invoke-direct {p2, v3}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v3, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    const-wide v4, 0x3ff19999a0000000L    # 1.100000023841858

    invoke-virtual {p2, v3, v4, v5}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v3, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    invoke-virtual {p2, v3, v4, v5}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-instance v3, Lmiuix/animation/b/a;

    const-string v4, "bgShow"

    invoke-direct {v3, v4}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v4, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    invoke-virtual {v3, v4, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    goto :goto_1

    :cond_2
    new-instance p2, Lmiuix/animation/b/a;

    const-string v3, "itemScaleDismiss"

    invoke-direct {p2, v3}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v3, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {p2, v3, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v3, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    invoke-virtual {p2, v3, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-instance v3, Lmiuix/animation/b/a;

    const-string v1, "bgDismiss"

    invoke-direct {v3, v1}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v1, v4, v5}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    :goto_1
    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;Lmiuix/animation/b/a;)V

    invoke-static {v0, v3}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;Lmiuix/animation/b/a;)V

    return-void
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/home/category/c/h;I)V
    .locals 5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    iget-object v3, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    iget-object v4, v2, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->e:Z

    if-nez v3, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    add-int/lit8 p2, p2, 0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {p0, p2, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemRangeRemoved(II)V

    :cond_3
    return-void
.end method

.method private a(I)Z
    .locals 1

    if-ltz p1, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/category/p;->getItemCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/category/p;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->d:Landroidx/recyclerview/widget/RecyclerView;

    return-object p0
.end method

.method private b(I)V
    .locals 0

    :try_start_0
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemRemoved(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/category/p;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->a:Landroid/content/Context;

    return-object p0
.end method

.method private c(Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 7

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object p2, p2, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    new-instance p2, Lmiuix/animation/b/a;

    const-string v1, "contentReSort"

    invoke-direct {p2, v1}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    const-wide/16 v2, 0x0

    invoke-virtual {p2, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v1, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {p2, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v1, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    const/4 v2, 0x0

    aget v3, v0, v2

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    mul-float/2addr v3, v4

    float-to-double v5, v3

    invoke-virtual {p2, v1, v5, v6}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v1, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    const/4 v3, 0x1

    aget v0, v0, v3

    int-to-float v0, v0

    mul-float/2addr v0, v4

    float-to-double v4, v0

    invoke-virtual {p2, v1, v4, v5}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-array v0, v3, [Landroid/view/View;

    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    aput-object v1, v0, v2

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/g;->cancel()V

    new-array v0, v3, [Landroid/view/View;

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    aput-object p1, v0, v2

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p1

    new-array v0, v2, [Lmiuix/animation/a/a;

    invoke-interface {p1, p2, v0}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$t;)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 8

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$t;->getAdapterPosition()I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v4, v2

    :goto_0
    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_2

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    iget-boolean v6, v5, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->e:Z

    if-eqz v6, :cond_0

    iget-object v6, v5, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    iget-object v7, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v4, v5

    goto :goto_1

    :cond_0
    iget-boolean v6, v5, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->e:Z

    if-nez v6, :cond_1

    if-eq v5, v0, :cond_1

    iget-object v6, v5, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    iget-object v7, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->b(I)V

    goto :goto_2

    :cond_3
    if-eqz v4, :cond_4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)Z

    move-result p1

    if-eqz p1, :cond_4

    return-object v0

    :cond_4
    return-object v2
.end method

.method public a()Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->d:Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/d/b/b;I)V
    .locals 1
    .param p1    # Lcom/xiaomi/misettings/usagestats/d/b/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/misettings/usagestats/d/b/b<",
            "Lb/c/a/a/a;",
            ">;I)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, p0, v0, p2}, Lcom/xiaomi/misettings/usagestats/d/b/b;->a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/home/category/c/h;)V
    .locals 3

    iget-object v0, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->j:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/c/b;)I

    move-result v1

    iget-boolean v2, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v2, v1, v0}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemRangeInserted(II)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/c/h;I)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->c:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    if-eq v0, p1, :cond_1

    iget-boolean v2, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    if-eqz v2, :cond_1

    iput-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/c/b;)I

    move-result v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->c:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    invoke-direct {p0, v2, v0}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/c/h;I)V

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :cond_1
    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->c:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget-boolean v0, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object v0

    instance-of v2, v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz v2, :cond_2

    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/c/b;)I

    move-result p1

    invoke-virtual {v0, p1, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->f(II)V

    :cond_2
    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)Z
    .locals 3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    const/4 v1, 0x0

    if-nez p2, :cond_0

    invoke-direct {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Landroidx/recyclerview/widget/RecyclerView$t;Z)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    return v1

    :cond_0
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$t;->getLayoutPosition()I

    move-result p1

    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$t;->getLayoutPosition()I

    move-result v2

    if-eq p1, v2, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-direct {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Landroidx/recyclerview/widget/RecyclerView$t;Z)V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-direct {p0, p2, v0}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Landroidx/recyclerview/widget/RecyclerView$t;Z)V

    return v0

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Landroidx/recyclerview/widget/RecyclerView$t;Z)V

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-direct {p0, p2, v0}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Landroidx/recyclerview/widget/RecyclerView$t;Z)V

    :cond_3
    :goto_0
    return v0
.end method

.method public b(Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 5

    instance-of v0, p1, Lcom/xiaomi/misettings/usagestats/home/category/a/o;

    if-eqz v0, :cond_2

    instance-of v0, p2, Lcom/xiaomi/misettings/usagestats/home/category/a/p;

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Landroidx/recyclerview/widget/RecyclerView$t;Z)V

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$t;->getAdapterPosition()I

    move-result v1

    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$t;->getAdapterPosition()I

    move-result v2

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(I)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView$t;->setIsRecyclable(Z)V

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/p;->c(Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)V

    iget-object p1, v3, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    iget-object p2, v2, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {p1, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->b(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->d:Landroidx/recyclerview/widget/RecyclerView;

    new-instance p2, Lcom/xiaomi/misettings/usagestats/home/category/m;

    invoke-direct {p2, p0, v3}, Lcom/xiaomi/misettings/usagestats/home/category/m;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/p;Lcom/xiaomi/misettings/usagestats/home/category/c/b;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {p1, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->b(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->d:Landroidx/recyclerview/widget/RecyclerView;

    new-instance p2, Lcom/xiaomi/misettings/usagestats/home/category/o;

    invoke-direct {p2, p0, v3, v2}, Lcom/xiaomi/misettings/usagestats/home/category/o;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/p;Lcom/xiaomi/misettings/usagestats/home/category/c/b;Lcom/xiaomi/misettings/usagestats/home/category/c/b;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    nop

    :cond_2
    :goto_0
    return-void
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    iget p1, p1, Lb/c/a/a/a;->type:I

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/b/b;

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/d/b/b;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/p;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/d/b/b;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/d/b/b;
    .locals 4
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/xiaomi/misettings/usagestats/d/b/b<",
            "Lb/c/a/a/a;",
            ">;"
        }
    .end annotation

    const-string v0, "ClassifyManagerAdapter"

    const-string v1, "onCreateViewHolder: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    const/4 v1, 0x1

    if-eq p2, v1, :cond_0

    new-instance p2, Lcom/xiaomi/misettings/usagestats/focusmode/b/g;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0e0155

    invoke-virtual {v2, v3, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v1, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/b/g;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/xiaomi/misettings/usagestats/home/category/a/o;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0e007b

    invoke-virtual {v2, v3, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v1, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/o;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    :cond_1
    new-instance p2, Lcom/xiaomi/misettings/usagestats/home/category/a/p;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/p;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0e007a

    invoke-virtual {v2, v3, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v1, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/p;-><init>(Landroid/content/Context;Landroid/view/View;)V

    :goto_0
    return-object p2
.end method
