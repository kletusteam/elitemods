.class final Lcom/xiaomi/misettings/usagestats/i/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/i/h;->B(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/i/e;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/i/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->l(Landroid/content/Context;)Z

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/i/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/i/h;->m(Landroid/content/Context;)Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable/isInUse"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BizSvr_steady_ctl"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/i/h;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/i/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/i/e;->a:Landroid/content/Context;

    const-class v1, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/i/e;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/i/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/i/h;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "start SteadyOn service error"

    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/i/e;->a:Landroid/content/Context;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/i/h;->b(Landroid/content/Context;J)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/i/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->u(Landroid/content/Context;)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/i/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->w(Landroid/content/Context;)V

    return-void
.end method
