.class public abstract Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;
.super Lmiuix/preference/PreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$b;
.implements Landroidx/preference/Preference$c;
.implements Lmiuix/appcompat/app/w$a;
.implements Lcom/xiaomi/misettings/usagestats/widget/d$a;


# instance fields
.field protected a:Landroidx/preference/PreferenceCategory;

.field protected b:Landroidx/preference/CheckBoxPreference;

.field protected c:Lmiuix/preference/TextPreference;

.field protected d:Lmiuix/preference/TextPreference;

.field protected e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;",
            ">;"
        }
    .end annotation
.end field

.field protected f:Landroidx/preference/PreferenceCategory;

.field protected g:Landroidx/preference/CheckBoxPreference;

.field protected h:Lmiuix/preference/TextPreference;

.field protected i:Lmiuix/preference/TextPreference;

.field protected j:Lmiuix/preference/TextPreference;

.field protected k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z

.field private m:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/preference/PreferenceFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->l:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->m:Z

    return-void
.end method


# virtual methods
.method public a(IIZ)V
    .locals 7

    new-instance v6, Lmiuix/appcompat/app/w;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/xiaomi/misettings/usagestats/home/ui/a;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/a;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;)V

    const/4 v5, 0x1

    move-object v0, v6

    move v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/app/w;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/w$a;IIZ)V

    invoke-virtual {v6}, Landroid/app/Dialog;->show()V

    iput-boolean p3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->l:Z

    return-void
.end method

.method protected abstract a(Landroidx/preference/Preference;)V
.end method

.method public a(Lmiuix/pickerwidget/widget/TimePicker;II)V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->l:Z

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->a(Lmiuix/pickerwidget/widget/TimePicker;IIZ)V

    return-void
.end method

.method public a(Lmiuix/pickerwidget/widget/TimePicker;IILmiuix/pickerwidget/widget/TimePicker;II)V
    .locals 8

    iget-boolean v7, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->m:Z

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->a(Lmiuix/pickerwidget/widget/TimePicker;IILmiuix/pickerwidget/widget/TimePicker;IIZ)V

    return-void
.end method

.method public a(Lmiuix/pickerwidget/widget/TimePicker;IILmiuix/pickerwidget/widget/TimePicker;IIZ)V
    .locals 0

    return-void
.end method

.method public a(Lmiuix/pickerwidget/widget/TimePicker;IIZ)V
    .locals 0

    return-void
.end method

.method public b(IIZ)V
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/widget/d;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2, p0}, Lcom/xiaomi/misettings/usagestats/widget/d;-><init>(Landroid/content/Context;IILcom/xiaomi/misettings/usagestats/widget/d$a;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iput-boolean p3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->m:Z

    return-void
.end method

.method protected abstract b(Landroidx/preference/Preference;)V
.end method

.method protected abstract c(Landroidx/preference/Preference;)V
.end method

.method protected d(Landroidx/preference/Preference;)Z
    .locals 2

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "weekday_usable_time"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string v0, "weekday_unusable_times"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method protected abstract f()V
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->e:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->k:Ljava/util/List;

    return-void
.end method

.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    const p1, 0x7f160005

    invoke-virtual {p0, p1, p2}, Landroidx/preference/PreferenceFragmentCompat;->setPreferencesFromResource(ILjava/lang/String;)V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0x7339ce0c

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v1, :cond_1

    const v1, 0x7cef682b

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "weekday_switch"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    move p1, v2

    goto :goto_1

    :cond_1
    const-string v0, "weekend_switch"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    move p1, v3

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_5

    if-eq p1, v3, :cond_3

    return v2

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->h:Lmiuix/preference/TextPreference;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->i:Lmiuix/preference/TextPreference;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->k:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    goto :goto_2

    :cond_4
    return v3

    :cond_5
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->c:Lmiuix/preference/TextPreference;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->d:Lmiuix/preference/TextPreference;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->e:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    goto :goto_3

    :cond_6
    return v3
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 7

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "weekend_unusable_times"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v4

    goto :goto_1

    :sswitch_1
    const-string v1, "weekday_usable_time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_1

    :sswitch_2
    const-string v1, "unlimit_apps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    goto :goto_1

    :sswitch_3
    const-string v1, "weekday_unusable_times"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v5

    goto :goto_1

    :sswitch_4
    const-string v1, "weekend_usable_time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v6

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_3

    if-eq v0, v6, :cond_3

    if-eq v0, v5, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v3, :cond_1

    return v2

    :cond_1
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->c(Landroidx/preference/Preference;)V

    return v6

    :cond_2
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->a(Landroidx/preference/Preference;)V

    return v6

    :cond_3
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->b(Landroidx/preference/Preference;)V

    return v6

    nop

    :sswitch_data_0
    .sparse-switch
        -0x77b94e24 -> :sswitch_4
        -0xc934fb1 -> :sswitch_3
        0x162bb7cf -> :sswitch_2
        0x46e7931d -> :sswitch_1
        0x4fa7a530 -> :sswitch_0
    .end sparse-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Landroidx/preference/PreferenceFragmentCompat;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const-string p1, "weekday_category"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->a:Landroidx/preference/PreferenceCategory;

    const-string p1, "weekday_switch"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->b:Landroidx/preference/CheckBoxPreference;

    const-string p1, "weekday_usable_time"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/TextPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->c:Lmiuix/preference/TextPreference;

    const-string p1, "weekday_unusable_times"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/TextPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->d:Lmiuix/preference/TextPreference;

    const-string p1, "weekend_category"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->f:Landroidx/preference/PreferenceCategory;

    const-string p1, "weekend_switch"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->g:Landroidx/preference/CheckBoxPreference;

    const-string p1, "weekend_usable_time"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/TextPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->h:Lmiuix/preference/TextPreference;

    const-string p1, "weekend_unusable_times"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/TextPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->i:Lmiuix/preference/TextPreference;

    const-string p1, "unlimit_apps"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/TextPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->j:Lmiuix/preference/TextPreference;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->b:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$b;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->c:Lmiuix/preference/TextPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->d:Lmiuix/preference/TextPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->g:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$b;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->h:Lmiuix/preference/TextPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->i:Lmiuix/preference/TextPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->j:Lmiuix/preference/TextPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/DeviceRestrictionBaseFragment;->f()V

    return-void
.end method
