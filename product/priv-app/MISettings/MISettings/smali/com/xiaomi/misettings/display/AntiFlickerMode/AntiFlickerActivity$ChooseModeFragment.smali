.class public Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;
.super Lmiuix/preference/PreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChooseModeFragment"
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;

.field private d:Lmiuix/preference/RadioButtonPreference;

.field private e:Lmiuix/preference/RadioButtonPreference;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/preference/RadioButtonPreference;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/display/AntiFlickerMode/a;

    invoke-direct {v0}, Lcom/xiaomi/misettings/display/AntiFlickerMode/a;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->a:Ljava/util/Map;

    new-instance v0, Lcom/xiaomi/misettings/display/AntiFlickerMode/b;

    invoke-direct {v0}, Lcom/xiaomi/misettings/display/AntiFlickerMode/b;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/preference/PreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->f:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;)Lmiuix/preference/RadioButtonPreference;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->d:Lmiuix/preference/RadioButtonPreference;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;)Lmiuix/preference/RadioButtonPreference;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->e:Lmiuix/preference/RadioButtonPreference;

    return-object p0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->c:Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->a(Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->c:Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->a(Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;)Landroid/widget/ImageView;

    move-result-object v0

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->b:Ljava/util/Map;

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->a:Ljava/util/Map;

    :goto_0
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "anti_flicker_mode"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;I)V

    invoke-static {}, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->a()Z

    return-void
.end method


# virtual methods
.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    sget p1, Lcom/xiaomi/misettings/display/o;->anti_flicker:I

    invoke-virtual {p0, p1, p2}, Landroidx/preference/PreferenceFragmentCompat;->setPreferencesFromResource(ILjava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->c:Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;

    const-string p1, "normal_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/RadioButtonPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->d:Lmiuix/preference/RadioButtonPreference;

    const-string p1, "anti_flicker_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/RadioButtonPreference;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->e:Lmiuix/preference/RadioButtonPreference;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->e:Lmiuix/preference/RadioButtonPreference;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/display/b;->d(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->e:Lmiuix/preference/RadioButtonPreference;

    sget p2, Lcom/xiaomi/misettings/display/l;->low_flicker_mode:I

    invoke-virtual {p1, p2}, Landroidx/preference/Preference;->setTitle(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->e:Lmiuix/preference/RadioButtonPreference;

    sget p2, Lcom/xiaomi/misettings/display/l;->low_flicker_mode_summary:I

    invoke-virtual {p1, p2}, Landroidx/preference/Preference;->setSummary(I)V

    :cond_0
    return-void
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->f:Ljava/util/List;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v2, "normal_mode"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->b(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string v0, "anti_flicker_mode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v2, 0x1

    if-eqz p1, :cond_4

    invoke-static {}, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->a()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;)I

    move-result p1

    if-eq p1, v2, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/display/b;->b(Landroid/content/Context;)I

    move-result p1

    const/16 v3, 0x3c

    if-eq p1, v3, :cond_2

    new-instance p1, Lmiuix/appcompat/app/j$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lcom/xiaomi/misettings/display/m;->AlertDialog_Theme_DayNight:I

    invoke-direct {p1, v0, v3}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;I)V

    sget v0, Lcom/xiaomi/misettings/display/l;->dc_light_hint_enable_title:I

    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/j$a;->c(I)Lmiuix/appcompat/app/j$a;

    sget v0, Lcom/xiaomi/misettings/display/l;->dc_lignt_enable_hint:I

    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/j$a;->b(I)Lmiuix/appcompat/app/j$a;

    const v0, 0x104000a

    new-instance v3, Lcom/xiaomi/misettings/display/AntiFlickerMode/d;

    invoke-direct {v3, p0}, Lcom/xiaomi/misettings/display/AntiFlickerMode/d;-><init>(Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;)V

    invoke-virtual {p1, v0, v3}, Lmiuix/appcompat/app/j$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    const/high16 v0, 0x1040000

    new-instance v3, Lcom/xiaomi/misettings/display/AntiFlickerMode/c;

    invoke-direct {v3, p0}, Lcom/xiaomi/misettings/display/AntiFlickerMode/c;-><init>(Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;)V

    invoke-virtual {p1, v0, v3}, Lmiuix/appcompat/app/j$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {p1}, Lmiuix/appcompat/app/j$a;->b()Lmiuix/appcompat/app/j;

    move-result-object p1

    invoke-virtual {p1, v1}, Lmiuix/appcompat/app/j;->setCancelable(Z)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->b(Ljava/lang/String;)V

    :cond_4
    :goto_0
    return v2
.end method

.method public onResume()V
    .locals 6

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    const-string v0, "mode_set_category"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceCategory;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "dc_back_light"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    if-eqz v3, :cond_1

    const-string v1, "anti_flicker_mode"

    goto :goto_1

    :cond_1
    const-string v1, "normal_mode"

    :goto_1
    iput-object v1, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->g:Ljava/lang/String;

    move v1, v2

    :goto_2
    invoke-virtual {v0}, Landroidx/preference/PreferenceGroup;->e()I

    move-result v3

    if-ge v1, v3, :cond_4

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->a(I)Landroidx/preference/Preference;

    move-result-object v3

    check-cast v3, Lmiuix/preference/RadioSetPreferenceCategory;

    invoke-virtual {v3, v2}, Landroidx/preference/PreferenceGroup;->a(I)Landroidx/preference/Preference;

    move-result-object v3

    if-eqz v3, :cond_3

    instance-of v4, v3, Lmiuix/preference/RadioButtonPreference;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->f:Ljava/util/List;

    move-object v5, v3

    check-cast v5, Lmiuix/preference/RadioButtonPreference;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$c;)V

    invoke-virtual {v3}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v5, v3}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v3, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->c:Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;

    invoke-static {v3}, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->a(Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;)Landroid/widget/ImageView;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->c:Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;

    invoke-static {v3}, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;->a(Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity;)Landroid/widget/ImageView;

    move-result-object v3

    sget-boolean v4, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v4, :cond_2

    sget-object v4, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->b:Ljava/util/Map;

    goto :goto_3

    :cond_2
    sget-object v4, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->a:Ljava/util/Map;

    :goto_3
    iget-object v5, p0, Lcom/xiaomi/misettings/display/AntiFlickerMode/AntiFlickerActivity$ChooseModeFragment;->g:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method
