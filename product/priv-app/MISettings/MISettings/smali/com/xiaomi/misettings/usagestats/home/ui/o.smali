.class Lcom/xiaomi/misettings/usagestats/home/ui/o;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/o;->a:Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const/4 p2, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x3ed8fd60

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v1, :cond_3

    const v1, -0x19bc86bd

    if-eq v0, v1, :cond_2

    const v1, -0x1019d1a7

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v0, "misettings.action.ACTION_REFRESH_HOME_TAB"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    move p2, v2

    goto :goto_0

    :cond_2
    const-string v0, "miui.token.change"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    move p2, v3

    goto :goto_0

    :cond_3
    const-string v0, "misettings.action.FROM_STEADY_ON"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 p2, 0x0

    :cond_4
    :goto_0
    if-eqz p2, :cond_8

    if-eq p2, v3, :cond_5

    if-eq p2, v2, :cond_5

    goto :goto_3

    :cond_5
    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/manager/a/g;->h()Z

    move-result p1

    if-nez p1, :cond_7

    :try_start_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/o;->a:Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->i(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    move-result-object p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/o;->a:Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->i(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_6
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/o;->a:Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    invoke-static {p1, v3}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->b(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;Z)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/o;->a:Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->j(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "viewCreateInit error"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "HomeContentFragment2"

    invoke-static {p2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    :goto_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/o;->a:Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->k(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    goto :goto_3

    :cond_8
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/o;->a:Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->a(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)I

    move-result p1

    if-nez p1, :cond_9

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/o;->a:Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->b(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    goto :goto_2

    :cond_9
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/o;->a:Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->h(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    :goto_2
    instance-of p2, p1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    if-eqz p2, :cond_a

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->m()V

    :cond_a
    :goto_3
    return-void
.end method
