.class Lcom/xiaomi/misettings/display/RefreshRate/u;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->a(Lcom/xiaomi/misettings/display/RefreshRate/m;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;II)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/u;->c:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    iput p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/u;->a:I

    iput p3, p0, Lcom/xiaomi/misettings/display/RefreshRate/u;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/u;->a:I

    iget v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/u;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/u;->b:I

    iget v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/u;->a:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/u;->c:Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;

    invoke-static {v2}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->d(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Lcom/xiaomi/misettings/display/RefreshRate/p;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemRangeChanged(II)V

    return-void
.end method
