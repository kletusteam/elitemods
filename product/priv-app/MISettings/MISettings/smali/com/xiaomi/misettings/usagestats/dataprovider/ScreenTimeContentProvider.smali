.class public Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;
.super Landroid/content/ContentProvider;


# static fields
.field public static a:Ljava/lang/String; = "content"


# instance fields
.field private final b:Ljava/lang/String;

.field private c:Landroid/content/ContentResolver;

.field private d:Ljava/lang/String;

.field private final e:Landroid/net/Uri;

.field private f:Landroid/content/Context;

.field private g:Landroid/graphics/Bitmap;

.field private h:Ljava/lang/String;

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    const-string v0, "last_total_hours"

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->b:Ljava/lang/String;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->d()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->e:Landroid/net/Uri;

    const-string v0, ""

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->h:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->i:I

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    return-object p0
.end method

.method private a(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 1

    invoke-static {p1, p2}, Landroidx/core/content/a;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    instance-of v0, p2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    check-cast p2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1

    :cond_0
    instance-of v0, p2, Landroid/graphics/drawable/VectorDrawable;

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0805df

    invoke-static {p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 5

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-object v0
.end method

.method private a(Landroid/content/Context;JLandroid/content/res/Resources;)Ljava/lang/String;
    .locals 6

    if-nez p1, :cond_0

    const-string p1, ""

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->h:Ljava/lang/String;

    return-object p1

    :cond_0
    const-wide/32 v0, 0xea60

    cmp-long p1, p2, v0

    const v2, 0x7f110037

    const/4 v3, 0x1

    if-gez p1, :cond_1

    invoke-virtual {p4, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->h:Ljava/lang/String;

    const-string p1, "1"

    return-object p1

    :cond_1
    const-wide/32 v4, 0x36ee80

    cmp-long p1, p2, v4

    if-gez p1, :cond_2

    div-long/2addr p2, v0

    long-to-int p1, p2

    invoke-virtual {p4, v2, p1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->h:Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    new-instance p1, Ljava/math/BigDecimal;

    invoke-direct {p1, p2, p3}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance p2, Ljava/math/BigDecimal;

    const p3, 0x36ee80

    invoke-direct {p2, p3}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 p3, 0x4

    invoke-virtual {p1, p2, v3, p3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;II)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {p1}, Ljava/math/BigDecimal;->floatValue()F

    move-result p1

    const p2, 0x7f11002a

    float-to-int p3, p1

    invoke-virtual {p4, p2, p3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->h:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p3

    sub-int/2addr p3, v3

    invoke-virtual {p2, p3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "0"

    invoke-virtual {p3, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_3

    new-instance p2, Ljava/text/DecimalFormat;

    invoke-direct {p2, p3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    float-to-double p3, p1

    invoke-virtual {p2, p3, p4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    new-instance p2, Ljava/text/DecimalFormat;

    const-string p3, "0.0"

    invoke-direct {p2, p3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    float-to-double p3, p1

    invoke-virtual {p2, p3, p4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;Landroid/content/Context;JLandroid/content/res/Resources;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Landroid/content/Context;JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->a(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v0

    const-string v1, "time"

    const-string v2, "common_category_data"

    const-string v3, "common_app_data"

    const-string v4, ""

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->c()Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v1, "app_usage_state"

    invoke-virtual {v0, v1}, Lcom/misettings/common/utils/p;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->c()Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v1, "category_usage_state"

    invoke-virtual {v0, v1}, Lcom/misettings/common/utils/p;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Landroid/content/Context;)Z

    move-result v0

    const-string v1, "unit"

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->d(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "is_support"

    if-nez v0, :cond_3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->i:I

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/delegate/UserHandlerDelegate;->getSystemUserID()I

    move-result v3

    if-eq v0, v3, :cond_0

    goto/16 :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "com.xiaomi.misettings"

    invoke-static {v0, v4, v3}, Lcom/misettings/common/utils/c;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/util/Locale;)Landroid/content/res/Resources;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->g:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->b()Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->g:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->g:Landroid/graphics/Bitmap;

    const-string v3, "bitmap"

    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const v2, 0x7f1300d0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v2, 0x7f13033a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "summary"

    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "action"

    const-string v3, "miui.action.usagestas.MAIN"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Landroid/content/Context;)Z

    move-result v2

    const-string v3, "state"

    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    const-string v3, "default_category"

    invoke-static {v2, v3, v1}, Lcom/xiaomi/misettings/usagestats/provider/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "is_category"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "send_update_signal"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Landroid/os/Bundle;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->a(Landroid/content/Context;J)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Landroid/content/res/Resources;)V

    goto :goto_1

    :cond_2
    const-string v0, "only_get_data"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Landroid/os/Bundle;)V

    goto :goto_1

    :cond_3
    :goto_0
    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string p1, "ScreenTimeContentProvider"

    const-string p2, "Not support!!!"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_1
    return-void
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    if-eqz p3, :cond_0

    const-string p2, "date"

    invoke-virtual {p3, p2, v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    :cond_0
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    new-instance p3, Lcom/xiaomi/misettings/usagestats/f/j;

    const/4 v4, 0x0

    invoke-direct {p3, v4, v0, v1}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    const/4 v0, 0x1

    invoke-static {p2, v2, v3, p3, v0}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;JLcom/xiaomi/misettings/usagestats/f/j;Z)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object p2

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p2

    const-string p3, "data"

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p2}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/d;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/misettings/common/utils/h;->a()Lb/a/b/q;

    move-result-object p2

    invoke-virtual {p2, v0}, Lb/a/b/q;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_3
    :goto_1
    const-string p2, "{}"

    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "key_has_accredit"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/provider/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method private b()Landroid/graphics/Bitmap;
    .locals 2

    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080173

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Lcom/misettings/common/utils/p;
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->c()Lcom/misettings/common/utils/p;

    move-result-object p0

    return-object p0
.end method

.method private c()Lcom/misettings/common/utils/p;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->c(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->h:Ljava/lang/String;

    return-object p0
.end method

.method private d()Landroid/net/Uri;
    .locals 2

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "com.xiaomi.misettings.usagestats.screentimecontentprovider"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->e:Landroid/net/Uri;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Landroid/content/ContentResolver;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->c:Landroid/content/ContentResolver;

    return-object p0
.end method

.method private e()V
    .locals 0

    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->e()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 4

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->c()Lcom/misettings/common/utils/p;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110037

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "unit"

    invoke-virtual {v0, v2, v1}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/res/Resources;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/dataprovider/b;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/dataprovider/b;-><init>(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 6

    const-string v0, "ScreenTimeContentProvider"

    const-string v1, "call-===========>"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Landroid/content/ContentProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->d:Ljava/lang/String;

    if-eqz p3, :cond_0

    const-string v2, "UID"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->i:I

    :cond_0
    const/4 v2, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v4, -0x59cce0ed

    const/4 v5, 0x1

    if-eq v3, v4, :cond_2

    const v4, -0x2b82ead3

    if-eq v3, v4, :cond_1

    goto :goto_0

    :cond_1
    const-string v3, "GET_FOREGROUND_COUNT_DATA"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    move v2, v5

    goto :goto_0

    :cond_2
    const-string v3, "GET_DATA"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v2, 0x0

    :cond_3
    :goto_0
    if-eqz v2, :cond_5

    if-eq v2, v5, :cond_4

    goto :goto_1

    :cond_4
    invoke-direct {p0, v1, p2, p3}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    :cond_5
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "call() arg is : "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1, p2}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "call: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f:Landroid/content/Context;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->c:Landroid/content/ContentResolver;

    goto :goto_0

    :cond_0
    const-string v0, "ScreenTimeContentProvider"

    const-string v1, "onCreate() mContext is null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
