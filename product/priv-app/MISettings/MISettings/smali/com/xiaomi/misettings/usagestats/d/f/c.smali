.class public Lcom/xiaomi/misettings/usagestats/d/f/c;
.super Ljava/lang/Object;


# direct methods
.method private static a()Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/f;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/f;-><init>(I)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/d/f$a;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/d/d/f$a;-><init>()V

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private static a(Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/j;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/j;-><init>(I)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/d/d/j$a;-><init>()V

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->isEnable()Z

    move-result v2

    iput-boolean v2, v1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->getRestTime()I

    move-result v2

    div-int/lit8 v2, v2, 0x3c

    iput v2, v1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->b:I

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->getContinuousDuration()I

    move-result p0

    div-int/lit8 p0, p0, 0x3c

    iput p0, v1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->a:I

    return-object v0
.end method

.method private static a(Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;)Lcom/xiaomi/misettings/usagestats/f/d;
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;->getPkgName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;->getUseTime()I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/f/b;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;->getIcon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/f/b;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;->getAppType()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/f/d;->d(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;J)Lcom/xiaomi/misettings/usagestats/f/e;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "week:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/misettings/common/utils/h;->a()Lb/a/b/q;

    move-result-object v1

    invoke-virtual {v1, p0}, Lb/a/b/q;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "__"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "createAppValueData"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/f/e;-><init>()V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-direct {v1, p1, p2}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/f/e;->a(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;->getPkgName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/f/e;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;->getUseTime()I

    move-result p1

    mul-int/lit16 p1, p1, 0x3e8

    int-to-long p1, p1

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/misettings/usagestats/f/e;->a(J)V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;->getIcon()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/f/e;->a(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/miui/greenguard/entity/DashBordBean;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/miui/greenguard/entity/DashBordBean;",
            ")",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/c;->b(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a()Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/c;->c(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/4 v2, 0x7

    invoke-direct {v1, v2}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/c;->d(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->n(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/i/h;->b()Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getMandatoryRest()Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;

    move-result-object p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a(Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a(Ljava/util/List;Lcom/miui/greenguard/entity/FamilyBean;)V

    new-instance p0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/16 v1, 0xb

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a(Ljava/util/List;Lcom/miui/greenguard/entity/DashBordBean;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/miui/greenguard/entity/DashBordBean;Z)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/miui/greenguard/entity/DashBordBean;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;"
        }
    .end annotation

    if-eqz p2, :cond_0

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/f/c;->b(Landroid/content/Context;Lcom/miui/greenguard/entity/DashBordBean;)Ljava/util/List;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a(Landroid/content/Context;Lcom/miui/greenguard/entity/DashBordBean;)Ljava/util/List;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static a(Lcom/miui/greenguard/entity/DashBordBean;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/greenguard/entity/DashBordBean;",
            ")",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/o;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getAppUsage()Ljava/util/List;

    move-result-object p0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;

    invoke-virtual {v3}, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;->getAppType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/o;

    if-nez v4, :cond_0

    new-instance v4, Lcom/xiaomi/misettings/usagestats/f/o;

    invoke-direct {v4}, Lcom/xiaomi/misettings/usagestats/f/o;-><init>()V

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v4, v3}, Lcom/xiaomi/misettings/usagestats/f/o;->a(Ljava/lang/String;)V

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;

    invoke-virtual {v5}, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;->getUseTime()I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    invoke-virtual {v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/f/o;->a(J)V

    sget-object v5, Lcom/xiaomi/misettings/usagestats/b/a/g;->h:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/xiaomi/misettings/usagestats/f/o;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v5

    sget-object v6, Lcom/xiaomi/misettings/usagestats/b/a/g;->d:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v5, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/xiaomi/misettings/usagestats/f/o;->c(Ljava/lang/String;)V

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private static a(Ljava/util/List;Lcom/miui/greenguard/entity/DashBordBean;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;",
            "Lcom/miui/greenguard/entity/DashBordBean;",
            ")V"
        }
    .end annotation

    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/d/d/i;

    invoke-virtual {v1, p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;->a(Lcom/miui/greenguard/entity/DashBordBean;)V

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/d/d/i;

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v2

    iput-object v2, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->f:Lcom/miui/greenguard/entity/FamilyBean;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private static a(Ljava/util/List;Lcom/miui/greenguard/entity/FamilyBean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;",
            "Lcom/miui/greenguard/entity/FamilyBean;",
            ")V"
        }
    .end annotation

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/manager/a/g;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    iput-object p1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->f:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method private static b(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 16

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/b;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/b;-><init>(I)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/miui/greenguard/entity/DashBordBean;->getDeviceUsage()Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/miui/greenguard/entity/DashBordBean;->getCurrentDate()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(J)I

    move-result v5

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    const/4 v8, 0x7

    if-ge v7, v8, :cond_6

    if-ge v7, v5, :cond_0

    const/4 v8, 0x1

    goto :goto_1

    :cond_0
    move v8, v6

    :goto_1
    new-instance v9, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v10, Lcom/xiaomi/misettings/usagestats/f/j;

    add-int/lit8 v11, v5, -0x1

    sub-int/2addr v11, v7

    int-to-long v11, v11

    sget-wide v13, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    mul-long/2addr v11, v13

    sub-long v11, v3, v11

    invoke-direct {v10, v11, v12}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-direct {v9, v10}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    if-eqz v8, :cond_1

    invoke-virtual {v2}, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;->getWeekDetail()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    mul-int/lit16 v12, v12, 0x3e8

    int-to-long v12, v12

    goto :goto_2

    :cond_1
    const-wide/16 v12, 0x0

    :goto_2
    invoke-virtual {v9, v12, v13}, Lcom/xiaomi/misettings/usagestats/f/g;->a(J)V

    invoke-virtual {v2}, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;->getDayDetail()Ljava/util/List;

    move-result-object v12

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move v14, v6

    :goto_3
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v15

    if-ge v14, v15, :cond_3

    if-eqz v8, :cond_2

    invoke-interface {v12, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    mul-int/lit16 v15, v15, 0x3e8

    int-to-long v10, v15

    goto :goto_4

    :cond_2
    const-wide/16 v10, 0x0

    :goto_4
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    :cond_3
    if-lez v7, :cond_5

    if-eqz v8, :cond_4

    invoke-virtual {v2}, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;->getWeekDetail()Ljava/util/List;

    move-result-object v8

    add-int/lit8 v10, v7, -0x1

    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    mul-int/lit16 v8, v8, 0x3e8

    int-to-long v10, v8

    goto :goto_5

    :cond_4
    const-wide/16 v10, 0x0

    :goto_5
    invoke-virtual {v9, v10, v11}, Lcom/xiaomi/misettings/usagestats/f/g;->b(J)V

    :cond_5
    invoke-virtual {v9, v13}, Lcom/xiaomi/misettings/usagestats/f/g;->a(Ljava/util/ArrayList;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_6
    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lcom/miui/greenguard/entity/DashBordBean;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/miui/greenguard/entity/DashBordBean;",
            ")",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/c;->e(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a()Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/c;->f(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/4 v2, 0x7

    invoke-direct {v1, v2}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/c;->g(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->n(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/i/h;->b()Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getMandatoryRest()Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;

    move-result-object p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a(Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a(Ljava/util/List;Lcom/miui/greenguard/entity/FamilyBean;)V

    new-instance p0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/16 v1, 0xb

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a(Ljava/util/List;Lcom/miui/greenguard/entity/DashBordBean;)V

    return-object v0
.end method

.method private static c(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 12

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/d;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/d;-><init>(I)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/d/d$a;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/d/d/d$a;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v6, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getCurrentDate()J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-direct {v5, v6}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    const/4 v8, 0x7

    if-ge v7, v8, :cond_1

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getSelectIndex()I

    move-result v8

    if-ne v7, v8, :cond_0

    new-instance v8, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v9, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getSelectTimeStamp()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-direct {v8, v9}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-interface {v4, v7, v8}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v8}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-virtual {v5, v8}, Lcom/xiaomi/misettings/usagestats/f/g;->a(Ljava/util/concurrent/ConcurrentHashMap;)V

    move v9, v6

    :goto_1
    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getAppUsage()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_2

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getAppUsage()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;

    invoke-static {v10}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a(Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;)Lcom/xiaomi/misettings/usagestats/f/d;

    move-result-object v11

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v10}, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;->getPkgName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_2
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v5, p0}, Lcom/xiaomi/misettings/usagestats/b/a/g;->b(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;Ljava/util/List;)V

    move v5, v6

    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    if-ge v5, v8, :cond_3

    invoke-interface {v3, v5, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_3
    :goto_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v6, v5, :cond_4

    invoke-interface {v2, v6, p0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_4
    iput-object v2, v1, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->c:Ljava/util/List;

    iput-object v3, v1, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->d:Ljava/util/List;

    iput-object v4, v1, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->e:Ljava/util/List;

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private static d(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 8

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getUnlock()Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/d/g;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Lcom/xiaomi/misettings/usagestats/d/d/g;-><init>(I)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x7

    if-ge v3, v4, :cond_1

    new-instance v4, Lcom/xiaomi/misettings/usagestats/f/i;

    new-instance v5, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getSelectIndex()I

    move-result v6

    if-ne v3, v6, :cond_0

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getSelectTimeStamp()J

    move-result-wide v6

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getCurrentDate()J

    move-result-wide v6

    :goto_1
    invoke-direct {v5, v6, v7}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-direct {v4, v5}, Lcom/xiaomi/misettings/usagestats/f/i;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;->getUnlockTimes()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/xiaomi/misettings/usagestats/f/i;->a(I)V

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;->getYesterday()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/xiaomi/misettings/usagestats/f/i;->b(I)V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getUnlock()Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;->getUnlocks()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/xiaomi/misettings/usagestats/f/i;->a(Ljava/util/List;)V

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;->getFirstTime()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/f/i;->b(J)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iput-object v2, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v1
.end method

.method private static e(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 18

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/c;-><init>(I)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/miui/greenguard/entity/DashBordBean;->getDeviceUsage()Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;

    move-result-object v3

    invoke-virtual {v3}, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;->getMonthDetail()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move v5, v1

    :goto_0
    if-ge v5, v4, :cond_2

    add-int/lit8 v6, v4, -0x1

    sub-int/2addr v6, v5

    invoke-virtual/range {p0 .. p0}, Lcom/miui/greenguard/entity/DashBordBean;->getToday()J

    move-result-wide v7

    invoke-static {v7, v8, v6}, Lcom/xiaomi/misettings/usagestats/weeklyreport/b/b;->a(JI)Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    move-result-object v7

    new-instance v8, Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-direct {v8}, Lcom/xiaomi/misettings/usagestats/f/m;-><init>()V

    if-nez v6, :cond_0

    const/4 v6, 0x1

    goto :goto_1

    :cond_0
    move v6, v1

    :goto_1
    invoke-virtual {v8, v6}, Lcom/xiaomi/misettings/usagestats/f/m;->a(Z)V

    iget-wide v9, v7, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    invoke-virtual {v8, v9, v10}, Lcom/xiaomi/misettings/usagestats/f/m;->c(J)V

    iget-wide v9, v7, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    invoke-virtual {v8, v9, v10}, Lcom/xiaomi/misettings/usagestats/f/m;->b(J)V

    iget v6, v7, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->a:I

    invoke-virtual {v8, v6}, Lcom/xiaomi/misettings/usagestats/f/m;->a(I)V

    invoke-virtual {v3}, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;->getMonthDetail()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v9, v6

    invoke-virtual {v8, v9, v10}, Lcom/xiaomi/misettings/usagestats/f/m;->a(J)V

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/miui/greenguard/entity/DashBordBean;->getToday()J

    move-result-wide v9

    invoke-static {v9, v10}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(J)I

    move v9, v1

    :goto_2
    invoke-virtual {v3}, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;->getWeekDetail()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_1

    new-instance v10, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v11, Lcom/xiaomi/misettings/usagestats/f/j;

    iget-wide v12, v7, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    int-to-long v14, v9

    sget-wide v16, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    mul-long v14, v14, v16

    add-long/2addr v12, v14

    invoke-direct {v11, v12, v13}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-direct {v10, v11}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-virtual {v3}, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;->getWeekDetail()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    mul-int/lit16 v11, v11, 0x3e8

    int-to-long v11, v11

    invoke-virtual {v10, v11, v12}, Lcom/xiaomi/misettings/usagestats/f/g;->a(J)V

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_1
    invoke-virtual {v8, v6}, Lcom/xiaomi/misettings/usagestats/f/m;->a(Ljava/util/List;)V

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :cond_2
    iput-object v2, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private static f(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 21

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/e;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/e;-><init>(I)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/d/d/e$a;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/miui/greenguard/entity/DashBordBean;->getDeviceUsage()Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;

    move-result-object v6

    invoke-virtual {v6}, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;->getMonthDetail()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v6, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/miui/greenguard/entity/DashBordBean;->getToday()J

    move-result-wide v9

    add-int/lit8 v11, v6, -0x1

    sub-int/2addr v11, v8

    invoke-static {v9, v10, v11}, Lcom/xiaomi/misettings/usagestats/weeklyreport/b/b;->a(JI)Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    move-result-object v9

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a(Lcom/miui/greenguard/entity/DashBordBean;)Ljava/util/List;

    move-result-object v12

    invoke-interface {v2, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v12, 0x0

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/miui/greenguard/entity/DashBordBean;->getAppUsage()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    if-ge v12, v13, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/miui/greenguard/entity/DashBordBean;->getAppUsage()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;

    iget-wide v14, v9, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    invoke-static {v13, v14, v15}, Lcom/xiaomi/misettings/usagestats/d/f/c;->a(Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;J)Lcom/xiaomi/misettings/usagestats/f/e;

    move-result-object v13

    invoke-interface {v10, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_0
    const/4 v12, 0x0

    :goto_2
    const/4 v13, 0x7

    if-ge v12, v13, :cond_1

    new-instance v13, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v14, Lcom/xiaomi/misettings/usagestats/f/j;

    move/from16 v16, v8

    iget-wide v7, v9, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    move-object/from16 v17, v0

    move-object/from16 v18, v1

    int-to-long v0, v12

    sget-wide v19, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    mul-long v0, v0, v19

    add-long/2addr v7, v0

    invoke-direct {v14, v7, v8}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-direct {v13, v14}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-interface {v11, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v12, v12, 0x1

    move/from16 v8, v16

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    goto :goto_2

    :cond_1
    move-object/from16 v17, v0

    move-object/from16 v18, v1

    move/from16 v16, v8

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v16, 0x1

    goto :goto_0

    :cond_2
    move-object/from16 v17, v0

    move-object v0, v1

    iput-object v3, v0, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->e:Ljava/util/List;

    iput-object v2, v0, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->d:Ljava/util/List;

    iput-object v4, v0, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->f:Ljava/util/List;

    iput-object v5, v0, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->c:Ljava/util/List;

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v1
.end method

.method private static g(Lcom/miui/greenguard/entity/DashBordBean;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 14

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/h;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/h;-><init>(I)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lcom/xiaomi/misettings/usagestats/d/d/h$a;

    invoke-direct {v2}, Lcom/xiaomi/misettings/usagestats/d/d/h$a;-><init>()V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getUnlock()Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;

    move-result-object v3

    invoke-virtual {v3}, Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;->getUnlockTimes()I

    move-result v3

    iput v3, v2, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a:I

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getUnlock()Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;

    move-result-object v3

    invoke-virtual {v3}, Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;->getYesterday()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a(I)V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getToday()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getSelectIndex()I

    move-result v5

    rsub-int/lit8 v5, v5, 0x3

    invoke-static {v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/weeklyreport/b/b;->a(JI)Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    move-result-object v3

    iput-object v3, v2, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->e:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getUnlock()Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->getToday()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(J)I

    const/4 p0, 0x0

    move v5, p0

    :goto_0
    invoke-virtual {v3}, Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;->getUnlocks()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_0

    new-instance v6, Lcom/xiaomi/misettings/usagestats/f/i;

    new-instance v7, Lcom/xiaomi/misettings/usagestats/f/j;

    iget-object v8, v2, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->e:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    iget-wide v8, v8, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    int-to-long v10, v5

    sget-wide v12, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    mul-long/2addr v10, v12

    add-long/2addr v8, v10

    invoke-direct {v7, v8, v9}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-direct {v6, v7}, Lcom/xiaomi/misettings/usagestats/f/i;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-virtual {v3}, Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;->getUnlocks()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/xiaomi/misettings/usagestats/f/i;->a(I)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    iput-object v4, v2, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->d:Ljava/util/List;

    :goto_1
    const/4 v3, 0x7

    if-ge p0, v3, :cond_1

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 p0, p0, 0x1

    goto :goto_1

    :cond_1
    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v0
.end method
