.class public Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;
.super Landroid/app/Service;


# instance fields
.field private a:Landroid/app/NotificationManager;

.field private b:Lmiui/process/IForegroundInfoListener$Stub;

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/service/a;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/service/a;-><init>(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->b:Lmiui/process/IForegroundInfoListener$Stub;

    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 9

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->e:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->h:I

    if-nez v1, :cond_4

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long p1, v1, v3

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c:Ljava/util/Map;

    if-eqz p1, :cond_4

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Ljava/lang/String;

    sget-object v2, Lcom/miui/greenguard/manager/j;->b:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static/range {v3 .. v8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;JJ)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/L;->f(J)I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_2
    iput v1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->h:I

    goto :goto_2

    :cond_3
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_4
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->h:I

    if-ge p1, v1, :cond_6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    sget v1, Lcom/xiaomi/misettings/usagestats/controller/i;->b:I

    if-ge p1, v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_5
    return v1

    :cond_6
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->h:I

    sub-int/2addr p1, v0

    return p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;Ljava/lang/String;I)Landroid/app/Notification;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a(Ljava/lang/String;I)Landroid/app/Notification;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;I)Landroid/app/Notification;
    .locals 4
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x1a
    .end annotation

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.miui.greenguard.appCategoryLimit"

    invoke-direct {v0, v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/xiaomi/misettings/usagestats/b/a/g;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const v1, 0x7f130074

    invoke-virtual {p0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const p1, 0x7f110009

    invoke-virtual {v1, p1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const p1, 0x7f080186

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    const/4 p1, -0x1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const-string p2, "miui.showAction"

    invoke-virtual {p1, p2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/app/NotificationManager;)V
    .locals 4
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x1a
    .end annotation

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->d(Landroid/content/Context;)Landroid/app/NotificationChannelGroup;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/NotificationManager;->createNotificationChannelGroup(Landroid/app/NotificationChannelGroup;)V

    new-instance v0, Landroid/app/NotificationChannel;

    const v1, 0x7f13035d

    invoke-virtual {p0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.miui.greenguard.appCategoryLimit"

    const/4 v3, 0x4

    invoke-direct {v0, v2, v1, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->enableLights(Z)V

    const-string v1, "app_timer"

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->setGroup(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 8
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x1a
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    const-string v1, "categotyLimit"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-wide/16 v2, 0x0

    const-string v4, "BizSvr_cate_service"

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "startCategoryAppOver:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/service/d;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/service/d;-><init>(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;Landroid/content/Intent;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-wide/16 v2, 0x5dc

    :cond_1
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_2
    const-string v1, "removeAll"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->d:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->e:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    return-void

    :cond_3
    const-string v1, "categoryId"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    const-string v1, "category_pkgNames"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c:Ljava/util/Map;

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v6, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    const-string v5, "remove"

    invoke-virtual {p1, v5, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->d:Ljava/util/Map;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->b(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v1, v0}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void

    :cond_5
    const-string v5, "limitRemindTime"

    invoke-virtual {p1, v5, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "resolveIntent:limitRemindTime"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "registerTime"

    invoke-virtual {p1, v6, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->d:Ljava/util/Map;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {p1, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->e:Ljava/util/Map;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {p1, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-direct {p0, p1, v5, v2, v3}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a(Ljava/lang/String;IJ)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/usagestats/b/a/g;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    invoke-static {p1}, Lcom/miui/greenguard/manager/j;->b(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_6

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_6

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-direct {p0, p1, v1, v0}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->i(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_7

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "resolveIntent: ==ensureForeground=="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a(Z)V

    :cond_7
    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "resolveIntent: ==mCategoryID=="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;IJ)V
    .locals 1

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/j;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p3, p4}, Lcom/xiaomi/misettings/usagestats/utils/j;->b(Landroid/content/Context;Ljava/lang/String;J)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->d()Z

    move-result p4

    invoke-static {p3, p1, p2, p4}, Lcom/xiaomi/misettings/usagestats/utils/j;->a(Landroid/content/Context;Ljava/lang/String;IZ)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x1a
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a:Landroid/app/NotificationManager;

    const-string v1, "BizSvr_cate_service"

    if-nez v0, :cond_0

    const-string p1, "updateNotification: notification is null"

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    if-nez p3, :cond_1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->b(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_1

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "updateNotification: show notification"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {v1, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a(Ljava/lang/String;)I

    move-result p3

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/service/g;->b()Lcom/xiaomi/misettings/usagestats/service/g;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/service/e;

    invoke-direct {v1, p0, p1, p3}, Lcom/xiaomi/misettings/usagestats/service/e;-><init>(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;Ljava/lang/String;I)V

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/xiaomi/misettings/usagestats/service/g;->a(Ljava/lang/String;Ljava/lang/String;ILcom/xiaomi/misettings/usagestats/service/g$b;)V

    goto :goto_0

    :cond_1
    const-string p1, "updateNotification: hide notification"

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a:Landroid/app/NotificationManager;

    const p2, 0x12087

    invoke-virtual {p1, p2}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/service/g;->b()Lcom/xiaomi/misettings/usagestats/service/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/service/g;->a()V

    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->d:Ljava/util/Map;

    return-object p0
.end method

.method private b()Z
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c:Ljava/util/Map;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_3
    return v1
.end method

.method private b(Ljava/lang/String;)Z
    .locals 7

    const-string v0, "BizSvr_cate_service"

    invoke-static {v0}, Lcom/misettings/common/utils/p;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->d:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->e:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    if-nez p1, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "shouldShowNotification: currentTime="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/utils/j;->a(Landroid/content/Context;Ljava/lang/String;J)I

    move-result v3

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->h:I

    goto :goto_0

    :cond_1
    iput v2, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->h:I

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "shouldShowNotification: limitRemindTime="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, ",registerTime="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ",usageTime="

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->h:I

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->h:I

    sub-int/2addr p1, v3

    if-gtz p1, :cond_2

    const-string p1, "shouldShowNotification: false="

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "shouldShowNotification:"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->h:I

    sub-int/2addr v3, v4

    sget v4, Lcom/xiaomi/misettings/usagestats/controller/b;->a:I

    const/4 v5, 0x1

    if-gt v3, v4, :cond_3

    move v3, v5

    goto :goto_1

    :cond_3
    move v3, v2

    :goto_1
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->h:I

    sub-int/2addr p1, v0

    sget v0, Lcom/xiaomi/misettings/usagestats/controller/b;->a:I

    if-gt p1, v0, :cond_4

    move v2, v5

    :cond_4
    return v2

    :cond_5
    :goto_2
    const-string p1, "timeEmpty"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->e:Ljava/util/Map;

    return-object p0
.end method

.method private c()V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a()V

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/service/b;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/service/b;-><init>(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->b()Z

    move-result p0

    return p0
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Landroid/app/NotificationManager;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a:Landroid/app/NotificationManager;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c:Ljava/util/Map;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->d:Ljava/util/Map;

    if-nez v0, :cond_1

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->d:Ljava/util/Map;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->e:Ljava/util/Map;

    if-nez v0, :cond_2

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->e:Ljava/util/Map;

    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 4

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/service/c;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/service/c;-><init>(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)V

    if-eqz p1, :cond_0

    const-wide/16 v2, 0x5dc

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    :goto_0
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onCreate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BizSvr_cate_service"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a:Landroid/app/NotificationManager;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    const-string v0, "[FATAL] Fail to get NotificationManager!"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a(Landroid/app/NotificationManager;)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->b:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/ProcessManagerDelegate;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c()V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "BizSvr_cate_service"

    const-string v1, "onDestory"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/j;->h(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestory mForegroundPkgName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->d:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->e:Ljava/util/Map;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onStartCommand"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, "===="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "BizSvr_cate_service"

    invoke-static {p3, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    const-string p2, "categoryId"

    invoke-virtual {p1, p2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->b()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->f:Ljava/lang/String;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->g:Ljava/lang/String;

    const/4 p3, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result p1

    return p1
.end method
