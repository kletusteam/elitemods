.class public Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;
.super Landroid/app/Service;


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:Lmiui/process/IForegroundInfoListener$Stub;

.field private e:I

.field private f:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/controller/c;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/controller/c;-><init>(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->d:Lmiui/process/IForegroundInfoListener$Stub;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;Ljava/lang/String;I)Landroid/app/Notification;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Ljava/lang/String;I)Landroid/app/Notification;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;I)Landroid/app/Notification;
    .locals 7

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.android.settings.appLimit"

    invoke-direct {v0, v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {p0}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    const v6, 0x7f11002b

    invoke-virtual {v1, v6, p2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v4

    const v1, 0x7f13035b

    invoke-virtual {p0, v1, v2}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const v1, 0x7f08018b

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Ljava/lang/String;)Landroid/graphics/drawable/Icon;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/drawable/Icon;)Landroid/app/Notification$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f130359

    invoke-virtual {p0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    const/16 v5, 0x1e

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const v1, 0x7f100004

    const v2, 0x7f130357

    invoke-virtual {p0, v2}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b(Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object p1

    invoke-virtual {v0, v1, v2, p1}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    :cond_0
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const-string p2, "miui.showAction"

    invoke-virtual {p1, p2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/drawable/Icon;
    .locals 0

    const p1, 0x7f08018f

    invoke-static {p0, p1}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->c:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->c:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/app/NotificationManager;)V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->d(Landroid/content/Context;)Landroid/app/NotificationChannelGroup;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/NotificationManager;->createNotificationChannelGroup(Landroid/app/NotificationChannelGroup;)V

    new-instance v0, Landroid/app/NotificationChannel;

    const v1, 0x7f13035d

    invoke-virtual {p0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.android.settings.appLimit"

    const/4 v3, 0x4

    invoke-direct {v0, v2, v1, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->enableLights(Z)V

    const-string v1, "app_timer"

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->setGroup(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 7

    const/4 v0, 0x0

    const-string v1, "removeAll"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->e:I

    return-void

    :cond_0
    const-string v1, "pkgName"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "limitTime"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-wide/16 v3, 0x0

    const-string v5, "registerTime"

    invoke-virtual {p1, v5, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v5, "remove"

    invoke-virtual {p1, v5, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v6, "ensureForeGround"

    invoke-virtual {p1, v6, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz v5, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a:Ljava/util/Map;

    invoke-interface {p1, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b:Ljava/util/Map;

    invoke-interface {p1, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->b(Landroid/content/Context;Ljava/lang/String;)V

    const/4 p1, 0x1

    invoke-direct {p0, v1, p1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Ljava/lang/String;IJ)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->i(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_3

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "resolveIntent: ==ensureForeground=="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->c:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "AppLimitService"

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Z)V

    :cond_3
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;IJ)V
    .locals 1

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->k(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p3, p4}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Ljava/lang/String;J)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->d()Z

    move-result p4

    invoke-static {p3, p1, p2, p4}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Ljava/lang/String;IZ)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->f:Landroid/app/NotificationManager;

    const-string v1, "AppLimitService"

    if-nez v0, :cond_0

    const-string p1, "updateNotification: notification is null"

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    if-nez p2, :cond_1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->d(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1

    const-string p2, "updateNotification: show notification"

    invoke-static {v1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->c(Ljava/lang/String;)I

    move-result p2

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/service/g;->b()Lcom/xiaomi/misettings/usagestats/service/g;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/controller/d;

    invoke-direct {v1, p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/controller/d;-><init>(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;Ljava/lang/String;I)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/xiaomi/misettings/usagestats/service/g;->a(Ljava/lang/String;ILcom/xiaomi/misettings/usagestats/service/g$b;)V

    goto :goto_0

    :cond_1
    const-string p1, "updateNotification: hide notification"

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->f:Landroid/app/NotificationManager;

    const p2, 0x10086

    invoke-virtual {p1, p2}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/service/g;->b()Lcom/xiaomi/misettings/usagestats/service/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/service/g;->a()V

    :goto_0
    return-void
.end method

.method public static b()I
    .locals 1

    const v0, 0x10086

    return v0
.end method

.method private b(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const/4 v1, 0x0

    const-string v2, "isWeek"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "packageName"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "fromNotification"

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance p1, Lcom/misettings/common/base/a;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p1, v2}, Lcom/misettings/common/base/a;-><init>(Landroid/content/Context;)V

    const-string v2, "com.xiaomi.misettings.usagestats.ui.NewAppUsageDetailFragment"

    invoke-virtual {p1, v2}, Lcom/misettings/common/base/a;->a(Ljava/lang/String;)Lcom/misettings/common/base/a;

    invoke-virtual {p1, v0}, Lcom/misettings/common/base/a;->a(Landroid/os/Bundle;)Lcom/misettings/common/base/a;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/misettings/common/base/a;->a(Landroid/app/Fragment;I)Lcom/misettings/common/base/a;

    invoke-virtual {p1}, Lcom/misettings/common/base/a;->c()Landroid/content/Intent;

    move-result-object p1

    const/high16 v0, 0x800000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x66

    const/high16 v2, 0xc000000

    invoke-static {v0, v1, p1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    return-object p1
.end method

.method private b(Ljava/lang/String;I)Landroid/app/PendingIntent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.settings.SCHEDULE_PROLONG_LIMIT_TIME"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "pkgName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "remainTime"

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    const-string v1, "showNotificationTime"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const/16 p2, 0x65

    const/high16 v1, 0xc000000

    invoke-static {p1, p2, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    return-object p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a:Ljava/util/Map;

    return-object p0
.end method

.method private c(Ljava/lang/String;)I
    .locals 9

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->e:I

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    move-object v4, p1

    invoke-static/range {v3 .. v8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;JJ)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/L;->f(J)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->e:I

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->e:I

    if-ge p1, v1, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    sget v1, Lcom/xiaomi/misettings/usagestats/controller/i;->b:I

    if-ge p1, v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_1
    return v1

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->e:I

    sub-int/2addr p1, v0

    return p1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Landroid/app/NotificationManager;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->f:Landroid/app/NotificationManager;

    return-object p0
.end method

.method private c()V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a()V

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/controller/e;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/controller/e;-><init>(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b:Ljava/util/Map;

    return-object p0
.end method

.method private d(Ljava/lang/String;)Z
    .locals 11

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    const-string v2, "AppLimitService"

    const/4 v3, 0x0

    if-eqz v0, :cond_4

    if-nez v1, :cond_0

    goto/16 :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "shouldShowNotification: currentTime="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    move-object v6, p1

    invoke-static/range {v5 .. v10}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;JJ)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/L;->f(J)I

    move-result v4

    iput v4, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->e:I

    goto :goto_0

    :cond_1
    iput v3, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->e:I

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "shouldShowNotification: limitTime="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, ",registerTime="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",usageTime="

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->e:I

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->e:I

    sub-int/2addr v1, v2

    if-gtz v1, :cond_2

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/service/MainProcessService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/app/Service;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return v3

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->e:I

    sub-int/2addr p1, v0

    sget v0, Lcom/xiaomi/misettings/usagestats/controller/i;->b:I

    if-gt p1, v0, :cond_3

    const/4 v3, 0x1

    :cond_3
    return v3

    :cond_4
    :goto_1
    const-string p1, "limitTime: registerTime empty"

    invoke-static {v2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v3
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a:Ljava/util/Map;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b:Ljava/util/Map;

    if-nez v0, :cond_1

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b:Ljava/util/Map;

    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 4

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/controller/f;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/controller/f;-><init>(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)V

    if-eqz p1, :cond_0

    const-wide/16 v2, 0x5dc

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    :goto_0
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "AppLimitService"

    const-string v1, "onCreate: ====create===="

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->f:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->f:Landroid/app/NotificationManager;

    if-nez v1, :cond_0

    const-string v1, "[FATAL] Fail to get NotificationManager!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Landroid/app/NotificationManager;)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->d:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/ProcessManagerDelegate;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->c()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b:Ljava/util/Map;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->d:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/ProcessManagerDelegate;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a:Ljava/util/Map;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b:Ljava/util/Map;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_3
    const-string v0, "AppLimitService"

    const-string v1, "onDestroy: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 0

    const-string p2, "AppLimitService"

    const-string p3, "onStartCommand: ====start===="

    invoke-static {p2, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a()V

    if-eqz p1, :cond_0

    const-string p2, "pkgName"

    invoke-virtual {p1, p2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a:Ljava/util/Map;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->c:Ljava/lang/String;

    invoke-interface {p1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->c:Ljava/lang/String;

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Ljava/lang/String;Z)V

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method
