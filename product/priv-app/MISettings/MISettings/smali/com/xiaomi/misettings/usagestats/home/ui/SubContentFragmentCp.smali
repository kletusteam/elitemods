.class public Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;
.super Lcom/xiaomi/misettings/base/BaseFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$b;,
        Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$c;,
        Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$d;,
        Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$a;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private c:Landroid/os/Handler;

.field private d:Lcom/xiaomi/misettings/usagestats/d/a/a;

.field private e:Lcom/xiaomi/misettings/usagestats/f/m;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Landroid/content/BroadcastReceiver;

.field private j:Landroid/content/BroadcastReceiver;

.field private k:Ljava/lang/Runnable;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private l:Landroidx/recyclerview/widget/RecyclerView;

.field private m:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

.field private n:Z

.field private o:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->g:I

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/I;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/I;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->k:Ljava/lang/Runnable;

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->o:Z

    return-void
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/f/g;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->c:Landroid/os/Handler;

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->c:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$c;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-direct {v1, v2, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$c;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a;Lcom/xiaomi/misettings/usagestats/f/g;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;Lcom/xiaomi/misettings/usagestats/f/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->a(Lcom/xiaomi/misettings/usagestats/f/g;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .locals 1

    instance-of v0, p1, Lcom/xiaomi/misettings/usagestats/f/m;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/m;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->e:Lcom/xiaomi/misettings/usagestats/f/m;

    goto :goto_0

    :cond_0
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->f:Ljava/util/List;

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->c:Landroid/os/Handler;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$a;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$a;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->n:Z

    return p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->p()V

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->g:I

    return p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)I
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->g:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->g:I

    return v0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->n()V

    return-void
.end method

.method static synthetic h(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->r()V

    return-void
.end method

.method static synthetic i(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)Lcom/xiaomi/misettings/usagestats/d/a/a;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    return-object p0
.end method

.method static synthetic j(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->q()V

    return-void
.end method

.method static synthetic k(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->o()V

    return-void
.end method

.method static synthetic l(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private l()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->m:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->d()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->m:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->setVisibility(I)V

    return-void
.end method

.method static synthetic m(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private m()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->o:Z

    const/4 v1, 0x1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->h:Ljava/lang/String;

    const-string v2, "steady_on"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->h:Ljava/lang/String;

    const-string v2, "disallow_limit_app"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->h:Ljava/lang/String;

    const-string v2, "device_limit"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d(I)V

    goto :goto_1

    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d(I)V

    :cond_4
    :goto_1
    return-void
.end method

.method private n()V
    .locals 6

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->l()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->e:Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/m;->h()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/k;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/k;->d()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v4

    iget-wide v4, v4, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->e:Lcom/xiaomi/misettings/usagestats/f/m;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->h:Ljava/lang/String;

    invoke-static {v0, v2, v4}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/m;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Ljava/util/List;I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->m()V

    return-void
.end method

.method private o()V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$b;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$b;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private p()V
    .locals 3

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->l()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->f:Ljava/util/List;

    invoke-static {v0, v2}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v0, v2}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Ljava/util/List;I)V

    return-void
.end method

.method private q()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$d;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp$d;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private r()V
    .locals 3

    :try_start_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->o()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->q()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SubContentFragment"

    const-string v2, "refreshUnlock error"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    return-void
.end method

.method private s()V
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/K;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/K;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->j:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/L;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/L;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->i:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "misettings.action.EXCHANGE_STEADY_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "misettings.action.EXCHANGE_DEVICE_LIMIT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "misettings.action.EXCHANGE_DETAIL_LIST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "misettings.action.NOTIFY_TODAY_DATA"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "misettings.action.FORCE_NOTIFY_DATA"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-static {v1}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "miui.android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method


# virtual methods
.method public b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const p3, 0x7f0e0061

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public synthetic c(I)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->l:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    :cond_0
    return-void
.end method

.method public d(I)V
    .locals 4

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->c:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/m;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/m;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;I)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->c:Landroid/os/Handler;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "onCreate: "

    const-string v1, "SubContentFragment"

    if-eqz p1, :cond_0

    const-string v2, "isWeek"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->n:Z

    const-string v2, "misettings_from_page"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "fromSteadyOn"

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->o:Z

    const-string v2, "screen_time_home_intent_key"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->h:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->h:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->s()V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->n:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->c:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->i:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;)V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->b()V

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->j:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_3
    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onStop()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const p2, 0x7f0b0183

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->l:Landroidx/recyclerview/widget/RecyclerView;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->l:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    new-instance p2, Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->l:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->n:Z

    if-eqz v0, :cond_0

    const/4 v2, 0x2

    :cond_0
    invoke-virtual {p2, v2}, Lcom/xiaomi/misettings/usagestats/d/a/a;->b(I)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->l:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->l:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/M;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/M;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;)V

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$k;)V

    const p2, 0x7f0b019e

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->m:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->m:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    invoke-static {p2}, Lcom/xiaomi/misettings/f;->c(Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_1

    const p2, 0x7f120009

    goto :goto_0

    :cond_1
    const p2, 0x7f120008

    :goto_0
    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->setGifResource(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->m:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->e()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragmentCp;->o()V

    return-void
.end method
