.class public Lcom/xiaomi/misettings/usagestats/utils/u;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/utils/u$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;)J
    .locals 3

    const-wide/16 v0, 0x0

    if-nez p0, :cond_0

    return-wide v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v2, "last_time_stopped_using"

    invoke-static {p0, v2, v0, v1}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;J)V
    .locals 1

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "last_time_stopped_using"

    invoke-static {p0, v0, p1, p2}, Landroid/provider/Settings$Global;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    return-void
.end method

.method static synthetic a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/utils/u$a;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/u;->c(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/utils/u$a;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/utils/u$a;)V
    .locals 3

    new-instance v0, Lmiuix/appcompat/app/j$a;

    const v1, 0x7f140007

    invoke-direct {v0, p0, v1}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f1302f3

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->b(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/j$a;

    const v1, 0x7f130362

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->a(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/j$a;

    const v1, 0x104000a

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/xiaomi/misettings/usagestats/utils/s;

    invoke-direct {v2, p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/s;-><init>(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/utils/u$a;)V

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/j$a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    const/high16 v1, 0x1040000

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/utils/r;

    invoke-direct {v1, p1}, Lcom/xiaomi/misettings/usagestats/utils/r;-><init>(Lcom/xiaomi/misettings/usagestats/utils/u$a;)V

    invoke-virtual {v0, p0, v1}, Lmiuix/appcompat/app/j$a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    new-instance p0, Lcom/xiaomi/misettings/usagestats/utils/q;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/q;-><init>(Lcom/xiaomi/misettings/usagestats/utils/u$a;)V

    invoke-virtual {v0, p0}, Lmiuix/appcompat/app/j$a;->a(Landroid/content/DialogInterface$OnCancelListener;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j$a;->a()Lmiuix/appcompat/app/j;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private static c(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/utils/u$a;)V
    .locals 3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/u;->a(Landroid/content/Context;J)V

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/T;->a(Landroid/content/Context;Z)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v2, Lcom/xiaomi/misettings/usagestats/utils/t;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/utils/t;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    const v0, 0x7f1302f5

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    invoke-interface {p1}, Lcom/xiaomi/misettings/usagestats/utils/u$a;->onSuccess()V

    return-void
.end method
