.class Lcom/xiaomi/misettings/usagestats/focusmode/c/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->a(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Ljava/lang/ref/SoftReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->b(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v0, :cond_0

    goto/16 :goto_2

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->a(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;Z)Z

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->d(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Landroid/os/Handler;

    move-result-object v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->c(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v1, p0, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->e(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)I

    move-result v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->f(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_2

    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-static {v4}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->f(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, v3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    iput-boolean v2, v3, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    iput v2, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v1, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move-object v3, v2

    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->f(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->f(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    invoke-static {v0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->a(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto :goto_1

    :cond_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->a(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;Z)Z

    return-void
.end method
