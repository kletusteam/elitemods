.class public Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;
.super Lcom/xiaomi/misettings/usagestats/d/a/a/s;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;-><init>(Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    return-object p0
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/d/d/i;Landroidx/recyclerview/widget/RecyclerView$a;)V
    .locals 8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "model.getFamilyBean():"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;->b()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HomeDatailCommonHolder"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f0b0325

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-boolean v2, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;->b()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/greenguard/entity/FamilyBean;->hasMultiDevice()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;->b()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/greenguard/entity/FamilyBean;->getDevices()Ljava/util/List;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    new-instance v3, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v5, 0x7f0e00db

    const v6, 0x1020014

    iget-object v7, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->f:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v7}, Lcom/miui/greenguard/entity/FamilyBean;->getDevicesStrList()Ljava/util/List;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    invoke-virtual {v0, v3}, Lmiuix/appcompat/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/Spinner;->setPadding(IIII)V

    const v2, 0x7f0e00d8

    invoke-virtual {v3, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;->b()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->getCurrentSelectDeviceIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/a/a/a/a;

    invoke-direct {v1, p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/a/a;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;Lcom/xiaomi/misettings/usagestats/d/d/i;Landroidx/recyclerview/widget/RecyclerView$a;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V
    .locals 0

    invoke-direct {p0, p2, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;->a(Lcom/xiaomi/misettings/usagestats/d/d/i;Landroidx/recyclerview/widget/RecyclerView$a;)V

    return-void
.end method
