.class public Lcom/xiaomi/misettings/usagestats/d/c;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<",
            "Ljava/lang/Long;",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<",
            "Ljava/lang/Long;",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->a:Ljava/util/concurrent/ConcurrentMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->b:Ljava/util/concurrent/ConcurrentMap;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/b;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/d/b;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->d:Ljava/util/List;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)J
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-static {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/f/j;)J

    move-result-wide p0

    return-wide p0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/f/j;)J
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/c;->b(Landroid/content/Context;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/f/j;)Lcom/xiaomi/misettings/usagestats/f/d;

    move-result-object p0

    if-nez p0, :cond_0

    const-wide/16 p0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide p0

    :goto_0
    return-wide p0
.end method

.method private static a(Lcom/xiaomi/misettings/usagestats/f/d;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/f/d;
    .locals 4

    if-nez p0, :cond_0

    new-instance p0, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    return-object p0

    :cond_0
    :try_start_0
    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/d;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/f/d;->b(I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/d;->g()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/d;->b(J)V

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0

    :catch_0
    new-instance p0, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    return-object p0
.end method

.method public static a(Landroid/content/Context;JLcom/xiaomi/misettings/usagestats/f/j;)Lcom/xiaomi/misettings/usagestats/f/g;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;JLcom/xiaomi/misettings/usagestats/f/j;Z)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;JLcom/xiaomi/misettings/usagestats/f/j;Z)Lcom/xiaomi/misettings/usagestats/f/g;
    .locals 9

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b()V

    iget-wide v0, p3, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_6

    if-nez p0, :cond_0

    goto/16 :goto_0

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/u;->a(Landroid/content/Context;)J

    move-result-wide v0

    iget-wide v2, p3, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v2

    if-nez v2, :cond_1

    iget-wide v2, p3, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_1

    new-instance p0, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-direct {p0, p3}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    return-object p0

    :cond_1
    iget-wide v0, p3, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long p1, v0, p1

    if-nez p1, :cond_2

    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p0, p1, p4}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Landroid/content/Context;Ljava/lang/Boolean;Z)V

    sget-object p1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-static {p0, p1, p4}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;Z)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object p0

    return-object p0

    :cond_2
    const-string p1, "loadAppUsageOfDay"

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/d/f/d;->a(Ljava/lang/String;J)V

    sget-object p2, Lcom/xiaomi/misettings/usagestats/d/c;->a:Ljava/util/concurrent/ConcurrentMap;

    iget-wide v0, p3, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/f/g;

    if-eqz p2, :cond_3

    const-string p0, "load from cache"

    invoke-static {p1, p0}, Lcom/xiaomi/misettings/usagestats/d/f/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-object p2

    :cond_3
    new-instance p2, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-direct {p2, p3}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/d;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/d;

    move-result-object v0

    iget-wide v1, p3, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/a/d;->a(JLjava/util/concurrent/ConcurrentHashMap;)V

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-gtz v0, :cond_4

    const-string v0, "currentDayLoadFrom os"

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/d/f/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, p2}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/d;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/d;

    move-result-object p1

    iget-wide v0, p3, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/a/d;->b(JLjava/util/concurrent/ConcurrentHashMap;)Z

    :cond_4
    iget-wide v4, p3, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v8

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJLjava/util/concurrent/ConcurrentHashMap;)V

    const/4 p0, 0x1

    invoke-virtual {p2, p0}, Lcom/xiaomi/misettings/usagestats/f/g;->b(Z)V

    if-nez p4, :cond_5

    sget-object p0, Lcom/xiaomi/misettings/usagestats/d/c;->a:Ljava/util/concurrent/ConcurrentMap;

    iget-wide p3, p3, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p0, p1, p2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    return-object p2

    :cond_6
    :goto_0
    new-instance p0, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-direct {p0, p3}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)Lcom/xiaomi/misettings/usagestats/f/g;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;Z)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;Z)Lcom/xiaomi/misettings/usagestats/f/g;
    .locals 13

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    if-nez p0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-static {v5, v4}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Lcom/xiaomi/misettings/usagestats/f/d;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/f/d;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-wide v7, v0, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v11

    move-object v6, p0

    move v12, p2

    invoke-static/range {v6 .. v12}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJLjava/util/concurrent/ConcurrentHashMap;Z)V

    if-nez p2, :cond_2

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Ljava/util/concurrent/ConcurrentHashMap;)V

    :cond_2
    const-wide/16 p0, 0x0

    invoke-virtual {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/f/g;->a(J)V

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Lcom/xiaomi/misettings/usagestats/f/g;->b(Z)V

    invoke-virtual {v1, p0}, Lcom/xiaomi/misettings/usagestats/f/g;->a(Z)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;J)Lcom/xiaomi/misettings/usagestats/f/k;
    .locals 8

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/c;->d(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/k;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/f/k;-><init>()V

    new-instance v6, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-direct {v6, p1, p2}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-virtual {v0, v6}, Lcom/xiaomi/misettings/usagestats/f/k;->a(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide p1

    invoke-static {p0, v0, p1, p2, v6}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/k;JLcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v4

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move-object v2, p0

    move-object v3, v0

    invoke-static/range {v2 .. v7}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/k;JLcom/xiaomi/misettings/usagestats/f/j;Ljava/util/List;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/m;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/c;->b(Landroid/content/Context;I)Lcom/xiaomi/misettings/usagestats/f/m;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/m;)V

    return-object v0
.end method

.method private static a(ILcom/xiaomi/misettings/usagestats/f/m;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ")",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/j;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/b/b;->a(I)Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    move-result-object p0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->a:I

    iput v0, p1, Lcom/xiaomi/misettings/usagestats/f/m;->a:I

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    iput-wide v0, p1, Lcom/xiaomi/misettings/usagestats/f/m;->b:J

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    iput-wide v0, p1, Lcom/xiaomi/misettings/usagestats/f/m;->c:J

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    add-int/lit8 p1, p1, -0x1

    :goto_0
    if-ltz p1, :cond_0

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/c;->b(Landroid/content/Context;I)Lcom/xiaomi/misettings/usagestats/f/m;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/m;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/j;",
            ">;Z)",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-static {p0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;JLcom/xiaomi/misettings/usagestats/f/j;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v3

    if-eqz p2, :cond_0

    invoke-static {p0, v3}, Lcom/xiaomi/misettings/usagestats/d/c;->b(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v3

    :cond_0
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static a(Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;",
            ")",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/j;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    :goto_0
    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    cmp-long v3, v1, v3

    if-gez v3, :cond_0

    new-instance v3, Lcom/xiaomi/misettings/usagestats/f/j;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-wide v3, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    add-long/2addr v1, v3

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static a()V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/k;)V
    .locals 1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/k;->a()Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/c;->b(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v0

    invoke-virtual {p1, p0, v0}, Lcom/xiaomi/misettings/usagestats/f/k;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/k;JLcom/xiaomi/misettings/usagestats/f/j;)V
    .locals 0

    invoke-virtual {p1, p4}, Lcom/xiaomi/misettings/usagestats/f/k;->a(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-static {p0, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;JLcom/xiaomi/misettings/usagestats/f/j;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object p2

    invoke-virtual {p1, p0, p2}, Lcom/xiaomi/misettings/usagestats/f/k;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/k;JLcom/xiaomi/misettings/usagestats/f/j;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/xiaomi/misettings/usagestats/f/k;",
            "J",
            "Lcom/xiaomi/misettings/usagestats/f/j;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->b:Ljava/util/concurrent/ConcurrentMap;

    iget-wide v1, p4, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/i;

    iget-wide v1, p4, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v1, v1, p2

    if-lez v1, :cond_0

    new-instance p0, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-direct {p0, p4}, Lcom/xiaomi/misettings/usagestats/f/i;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-virtual {p1, p0}, Lcom/xiaomi/misettings/usagestats/f/k;->a(Lcom/xiaomi/misettings/usagestats/f/i;)V

    return-void

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/u;->a(Landroid/content/Context;)J

    move-result-wide v1

    iget-wide v3, p4, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v1, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v3

    if-nez v3, :cond_1

    iget-wide v3, p4, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v1, v3, v1

    if-gez v1, :cond_1

    new-instance p0, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-direct {p0, p4}, Lcom/xiaomi/misettings/usagestats/f/i;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-virtual {p1, p0}, Lcom/xiaomi/misettings/usagestats/f/k;->a(Lcom/xiaomi/misettings/usagestats/f/i;)V

    return-void

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/f/k;->a(Lcom/xiaomi/misettings/usagestats/f/i;)V

    return-void

    :cond_2
    iget-wide v0, p4, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long p2, v0, p2

    if-nez p2, :cond_3

    const/4 p2, 0x1

    goto :goto_0

    :cond_3
    const/4 p2, 0x0

    :goto_0
    new-instance p3, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-direct {p3, p4}, Lcom/xiaomi/misettings/usagestats/f/i;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/o;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/o;

    move-result-object v0

    invoke-virtual {v0, p4, p3}, Lcom/xiaomi/misettings/usagestats/a/o;->a(Lcom/xiaomi/misettings/usagestats/f/j;Lcom/xiaomi/misettings/usagestats/f/i;)Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz p0, :cond_4

    iget-wide v1, p4, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    sget-wide v3, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    add-long/2addr v3, v1

    move-object v0, p0

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/misettings/usagestats/utils/Q;->a(Landroid/content/Context;JJLcom/xiaomi/misettings/usagestats/f/i;)Z

    :cond_4
    if-nez p2, :cond_6

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result p0

    if-lez p0, :cond_6

    invoke-interface {p5, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "loadDeviceUsageOfDay: from DB "

    invoke-virtual {p0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p4, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p5, "HomeDataFactory"

    invoke-static {p5, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    :goto_1
    if-nez p2, :cond_7

    sget-object p0, Lcom/xiaomi/misettings/usagestats/d/c;->b:Ljava/util/concurrent/ConcurrentMap;

    iget-wide p4, p4, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p0, p2, p3}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    invoke-virtual {p1, p3}, Lcom/xiaomi/misettings/usagestats/f/k;->a(Lcom/xiaomi/misettings/usagestats/f/i;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/m;)V
    .locals 3

    if-eqz p1, :cond_3

    if-nez p0, :cond_0

    goto :goto_1

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    const-string v0, "HomeDataFactory"

    const-string v1, "insertOtherApps: no has fetch apps"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/c;->d(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/m;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/k;

    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/k;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, p0, v0}, Lcom/xiaomi/misettings/usagestats/f/m;->a(Landroid/content/Context;Ljava/util/List;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/f/j;)Lcom/xiaomi/misettings/usagestats/f/d;
    .locals 2

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    invoke-static {p0, v0, v1, p2}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;JLcom/xiaomi/misettings/usagestats/f/j;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object p0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p0

    invoke-virtual {p0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/xiaomi/misettings/usagestats/f/d;

    return-object p0
.end method

.method public static b(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)Lcom/xiaomi/misettings/usagestats/f/g;
    .locals 6

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-gtz v0, :cond_1

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/c;->d(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object p0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    sget-object v5, Lcom/xiaomi/misettings/usagestats/d/c;->d:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    new-instance v5, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {v5, v4}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    invoke-virtual {p1, v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_5
    :goto_1
    return-object p1
.end method

.method public static b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/k;
    .locals 2

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;J)Lcom/xiaomi/misettings/usagestats/f/k;

    move-result-object p0

    return-object p0
.end method

.method public static b(Landroid/content/Context;I)Lcom/xiaomi/misettings/usagestats/f/m;
    .locals 13

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/c;->d(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/f/m;-><init>()V

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/d/c;->a(ILcom/xiaomi/misettings/usagestats/f/m;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v10

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v7, v3

    check-cast v7, Lcom/xiaomi/misettings/usagestats/f/j;

    new-instance v12, Lcom/xiaomi/misettings/usagestats/f/k;

    invoke-direct {v12}, Lcom/xiaomi/misettings/usagestats/f/k;-><init>()V

    invoke-virtual {v12, v7}, Lcom/xiaomi/misettings/usagestats/f/k;->a(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-static {p0, v12, v10, v11, v7}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/k;JLcom/xiaomi/misettings/usagestats/f/j;)V

    move-object v3, p0

    move-object v4, v12

    move-wide v5, v10

    move-object v8, v9

    invoke-static/range {v3 .. v8}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/k;JLcom/xiaomi/misettings/usagestats/f/j;Ljava/util/List;)V

    invoke-interface {v2, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Lcom/xiaomi/misettings/usagestats/f/m;->b(Ljava/util/List;)V

    if-nez p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/f/m;->a(Z)V

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_3

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/o;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/o;

    move-result-object p0

    invoke-virtual {p0, v9}, Lcom/xiaomi/misettings/usagestats/a/o;->a(Ljava/util/List;)V

    :cond_3
    return-object v0
.end method

.method public static b()V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x4

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;I)Ljava/util/List;

    return-void
.end method

.method private static d(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    return-object v2

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->l(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const-string v4, "HomeDataFactory"

    if-eqz p0, :cond_3

    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/PackageInfo;

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    sget-object v6, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->g:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v3, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {v5}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const-string p0, "getInstallAppList: wow!! packageManager is null!!!"

    invoke-static {v4, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getInstallAppList: duration="

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v0

    invoke-virtual {p0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ",packageCount="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v2
.end method
