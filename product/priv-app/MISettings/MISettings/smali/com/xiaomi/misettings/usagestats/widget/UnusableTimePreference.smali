.class public Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;
.super Landroidx/preference/Preference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;
    }
.end annotation


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ImageView;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;

.field private mContext:Landroid/content/Context;

.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p2, -0x1

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->l:I

    new-instance p2, Lcom/xiaomi/misettings/usagestats/widget/e;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/usagestats/widget/e;-><init>(Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;)V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;)Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->m:Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->mContext:Landroid/content/Context;

    const p1, 0x7f0e0075

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->f:I

    return p0
.end method

.method private b()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v1, v0, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    if-eqz v1, :cond_1

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->l:I

    if-nez v2, :cond_0

    const/high16 v2, 0x40000000    # 2.0f

    goto :goto_0

    :cond_0
    const/high16 v2, 0x41200000    # 10.0f

    :goto_0
    invoke-static {v1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->g:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;

    return-object p0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->l:I

    return-void
.end method

.method public a(ILcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;)V
    .locals 3

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->g:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->f:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->g:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->getBeginTime()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->d:Ljava/lang/String;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->d:Ljava/lang/String;

    const-string p2, ":"

    invoke-virtual {p1, p2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    aget-object v0, p1, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->h:I

    aget-object p1, p1, v2

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->i:I

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->g:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->getEndTime()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->e:Ljava/lang/String;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->e:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length p2, p1

    if-le p2, v2, :cond_1

    aget-object p2, p1, v1

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->j:I

    aget-object p1, p1, v2

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->k:I

    :cond_1
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->h:I

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->j:I

    if-gt p1, p2, :cond_2

    if-ne p1, p2, :cond_3

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->i:I

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->k:I

    if-lt p1, p2, :cond_3

    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->mContext:Landroid/content/Context;

    const v0, 0x7f13025c

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->e:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->e:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0}, Landroidx/preference/Preference;->notifyChanged()V

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->m:Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference$a;

    return-void
.end method

.method public onBindViewHolder(Landroidx/preference/B;)V
    .locals 3

    invoke-super {p0, p1}, Landroidx/preference/Preference;->onBindViewHolder(Landroidx/preference/B;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const v0, 0x7f0b0399

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->b:Landroid/widget/TextView;

    const v0, 0x7f0b0240

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->c:Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->a:Landroid/view/View;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->c:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->b:Landroid/widget/TextView;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->d:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->e:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "%s - %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/UnusableTimePreference;->b()V

    return-void
.end method
