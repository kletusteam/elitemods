.class Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;
.super Landroid/database/ContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/display/ScreenExpertSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Landroid/net/Uri;

.field final synthetic c:Lcom/xiaomi/misettings/display/ScreenExpertSettings;


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/display/ScreenExpertSettings;)V
    .locals 1

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->c:Lcom/xiaomi/misettings/display/ScreenExpertSettings;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {v0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string p1, "screen_paper_mode_enabled"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->a:Landroid/net/Uri;

    const-string p1, "screen_paper_mode"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->b:Landroid/net/Uri;

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->c:Lcom/xiaomi/misettings/display/ScreenExpertSettings;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->b(Lcom/xiaomi/misettings/display/ScreenExpertSettings;)Landroidx/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->c:Lcom/xiaomi/misettings/display/ScreenExpertSettings;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/ScreenExpertSettings;->b(Lcom/xiaomi/misettings/display/ScreenExpertSettings;)Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method private c()Z
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->c:Lcom/xiaomi/misettings/display/ScreenExpertSettings;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    const-string v2, "screen_paper_mode"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private d()Z
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->c:Lcom/xiaomi/misettings/display/ScreenExpertSettings;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_paper_mode_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/SystemSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->c:Lcom/xiaomi/misettings/display/ScreenExpertSettings;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->c:Lcom/xiaomi/misettings/display/ScreenExpertSettings;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->c:Lcom/xiaomi/misettings/display/ScreenExpertSettings;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onChange(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->d()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->a(Z)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/ScreenExpertSettings$a;->a(Z)V

    :goto_0
    return-void
.end method
