.class public Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;
.super Lcom/xiaomi/misettings/usagestats/focusmode/FocusBaseNoActionbarFragment;

# interfaces
.implements Ljava/util/Observer;
.implements Lcom/xiaomi/misettings/usagestats/focusmode/a/g$a;
.implements Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;
.implements Lcom/xiaomi/misettings/usagestats/focusmode/a/g$d;
.implements Lcom/xiaomi/misettings/usagestats/focusmode/a/g$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment$b;,
        Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment$a;
    }
.end annotation


# static fields
.field private static c:Landroid/os/Handler;


# instance fields
.field private d:I

.field private e:I

.field private f:Landroid/widget/TextView;

.field private g:Landroid/view/View;

.field private h:Z

.field private i:Landroid/os/Handler;

.field private j:Z

.field private k:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/focusmode/data/g;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field private p:Landroid/net/Uri;

.field private q:Landroid/content/ContentResolver;

.field private r:Lcom/xiaomi/misettings/usagestats/focusmode/a/g;

.field private s:Lmiuix/appcompat/app/j;

.field private t:Landroid/widget/LinearLayout;

.field private u:Landroid/widget/LinearLayout;

.field private v:Lmiuix/androidbasewidget/widget/StateEditText;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/Button;

.field private y:Ljava/lang/StringBuilder;

.field z:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusBaseNoActionbarFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d:I

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->i:Landroid/os/Handler;

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->j:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->o:Z

    const-string v0, "content://com.xiaomi.misettings.usagestats.focusmode.data.TimerContentProvider/focus_mode_timers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->p:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->y:Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->z:Landroid/widget/ImageView;

    return-void
.end method

.method private A()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->g:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/C;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/C;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->f:Landroid/widget/TextView;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/D;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/D;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->b(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->l:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    return-void
.end method

.method private B()V
    .locals 3

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lmiuix/appcompat/app/j$a;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1303f7

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->c(I)Lmiuix/appcompat/app/j$a;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d:I

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->b(I)Lmiuix/appcompat/app/j$a;

    const v1, 0x7f13041f

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/E;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/E;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/j$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    const v1, 0x7f13029e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/j$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j$a;->b()Lmiuix/appcompat/app/j;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d:I

    return p1
.end method

.method static synthetic a(Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0

    sput-object p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->c:Landroid/os/Handler;

    return-object p0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, "add_timer_show_dialog_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->e(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d(Z)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Z
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->m()Z

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->e:I

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Ljava/lang/StringBuilder;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->y:Ljava/lang/StringBuilder;

    return-object p0
.end method

.method private b(Landroid/view/View;)V
    .locals 11

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->q:Landroid/content/ContentResolver;

    const v0, 0x7f0b036d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->k:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->x(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->q:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->p:Landroid/net/Uri;

    const-string v3, "id"

    const-string v4, "duration"

    filled-new-array {v3, v4}, [Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v1, :cond_6

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    const-string v7, ""

    if-eqz v6, :cond_2

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    new-instance v8, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-direct {v8, v6, v7}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;-><init>(Ljava/lang/String;Z)V

    if-eqz v7, :cond_1

    iput-boolean v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->o:Z

    move-object v2, v8

    :cond_1
    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    if-eqz v6, :cond_5

    const-string v8, "sp_insert_time"

    invoke-virtual {v6, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v6, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/xiaomi/misettings/usagestats/focusmode/data/h;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-interface {v3, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    new-instance v10, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-direct {v10, v8, v9}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;-><init>(Ljava/lang/String;Z)V

    if-eqz v9, :cond_4

    move-object v2, v10

    :cond_4
    iget-object v8, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0, v5}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->b(Z)V

    :cond_7
    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    const-string v1, "190"

    invoke-direct {v0, v1, v5}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->o:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    :cond_8
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Landroidx/recyclerview/widget/LinearLayoutManager;->k(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->k:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    invoke-direct {v0, v1, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->r:Lcom/xiaomi/misettings/usagestats/focusmode/a/g;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->r:Lcom/xiaomi/misettings/usagestats/focusmode/a/g;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->k:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->r:Lcom/xiaomi/misettings/usagestats/focusmode/a/g;

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$a;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->r:Lcom/xiaomi/misettings/usagestats/focusmode/a/g;

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->r:Lcom/xiaomi/misettings/usagestats/focusmode/a/g;

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$d;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->r:Lcom/xiaomi/misettings/usagestats/focusmode/a/g;

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$c;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->k:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->r:Lcom/xiaomi/misettings/usagestats/focusmode/a/g;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->k:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    if-eqz v2, :cond_9

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->c(I)V

    :cond_9
    const v0, 0x7f0b0222

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/A;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/A;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->j:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080099

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060078

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08009a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060079

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->w:Landroid/widget/TextView;

    return-object p0
.end method

.method private c(I)V
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->k:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_0
    return-void
.end method

.method private c(Landroid/view/View;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v0, 0x0

    iput v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->r:Lcom/xiaomi/misettings/usagestats/focusmode/a/g;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b(Ljava/lang/String;)V

    return-void
.end method

.method private c(Z)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0704a5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    if-eqz p1, :cond_0

    div-int/lit8 v1, v1, 0x2

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    if-eqz p1, :cond_1

    div-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    return-void
.end method

.method private d(I)I
    .locals 1

    const/16 v0, -0x64

    if-ne p1, v0, :cond_3

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/misettings/common/utils/SettingsFeatures;->isWifiOnly(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object p1

    invoke-virtual {p1}, Lmiui/telephony/TelephonyManager;->isVoiceCapable()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const p1, 0x7f1303ee

    goto :goto_1

    :cond_1
    :goto_0
    const p1, 0x7f1303ed

    :goto_1
    return p1

    :cond_2
    const p1, 0x7f1303ec

    return p1

    :cond_3
    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/misettings/common/utils/SettingsFeatures;->isWifiOnly(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_5

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object p1

    invoke-virtual {p1}, Lmiui/telephony/TelephonyManager;->isVoiceCapable()Z

    move-result p1

    if-nez p1, :cond_4

    goto :goto_2

    :cond_4
    const p1, 0x7f1303f6

    goto :goto_3

    :cond_5
    :goto_2
    const p1, 0x7f1303f5

    :goto_3
    return p1

    :cond_6
    const p1, 0x7f1303f4

    return p1
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Lmiuix/androidbasewidget/widget/StateEditText;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    return-object p0
.end method

.method private d(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->q:Landroid/content/ContentResolver;

    const-string v1, "is_in_delete_mode"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private d(Ljava/lang/String;)Z
    .locals 4

    const-string v0, "FocusSettingsFragment"

    :try_start_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "language:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "language error"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Lmiuix/appcompat/app/j;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->s:Lmiuix/appcompat/app/j;

    return-object p0
.end method

.method private e(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->s:Lmiuix/appcompat/app/j;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->p()V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->s:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->i:Landroid/os/Handler;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/F;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/F;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V

    const-wide/16 v1, 0xc8

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/Button;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->x:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/LinearLayout;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->u:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic h(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->l:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic i(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->m:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic j(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d:I

    return p0
.end method

.method static synthetic k(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->e:I

    return p0
.end method

.method static synthetic k()Landroid/os/Handler;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->c:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic l(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->B()V

    return-void
.end method

.method private m()Z
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->q:Landroid/content/ContentResolver;

    const/4 v1, 0x0

    const-string v2, "is_in_delete_mode"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    return v1
.end method

.method static synthetic m(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->j:Z

    return p0
.end method

.method static synthetic n(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private n()V
    .locals 8

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ug"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    const/16 v1, 0xb4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    const v4, 0x7f130182

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/text/SpannableString;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v6

    aput-object v1, v3, v5

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Landroid/text/style/AbsoluteSizeSpan;

    const/16 v2, 0x11

    invoke-direct {v1, v2, v5}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v6

    aput-object v1, v3, v5

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private o()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/I;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/I;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private p()V
    .locals 7

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0e005b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->t:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->t:Landroid/widget/LinearLayout;

    const v1, 0x7f0b00d8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->x:Landroid/widget/Button;

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->x:Landroid/widget/Button;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v1

    new-array v2, v0, [Lmiuix/animation/m$b;

    sget-object v4, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    aput-object v4, v2, v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v1, v4, v2}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->x:Landroid/widget/Button;

    new-array v5, v3, [Lmiuix/animation/a/a;

    invoke-interface {v1, v2, v5}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->x:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->t:Landroid/widget/LinearLayout;

    const v2, 0x7f0b00d7

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-array v2, v0, [Landroid/view/View;

    aput-object v1, v2, v3

    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v2

    new-array v5, v0, [Lmiuix/animation/m$b;

    sget-object v6, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    aput-object v6, v5, v3

    invoke-interface {v2, v4, v5}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    new-array v4, v3, [Lmiuix/animation/a/a;

    invoke-interface {v2, v1, v4}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->t:Landroid/widget/LinearLayout;

    const v4, 0x7f0b022b

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->t:Landroid/widget/LinearLayout;

    const v4, 0x7f0b0221

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->w:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->w:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f130182

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/16 v6, 0x14

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    const/16 v3, 0xb4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->o()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->x:Landroid/widget/Button;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/G;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/G;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/H;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/H;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->q()V

    return-void
.end method

.method private q()V
    .locals 2

    new-instance v0, Lmiuix/appcompat/app/j$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;)V

    const v1, 0x7f130183

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->c(I)Lmiuix/appcompat/app/j$a;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->b(Landroid/view/View;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j$a;->a()Lmiuix/appcompat/app/j;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->s:Lmiuix/appcompat/app/j;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->s:Lmiuix/appcompat/app/j;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j;->setCancelable(Z)V

    return-void
.end method

.method private r()V
    .locals 4

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->h:Z

    const/16 v1, 0x8

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->g:Landroid/view/View;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v2

    const-string v3, "settings_experience_count"

    invoke-virtual {v2, v3}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private s()V
    .locals 4

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->c(Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070091

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    if-eqz v0, :cond_0

    div-int/lit8 v2, v2, 0x2

    :cond_0
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->l:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->w()Z

    move-result v3

    if-eqz v3, :cond_1

    int-to-float v0, v0

    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v0, v3

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v0, v3

    goto :goto_0

    :cond_1
    int-to-float v0, v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->x()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->z()V

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->y()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/n;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->z()V

    const-string v0, "FocusSettingsFragment"

    const-string v1, "setNoMargin"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void
.end method

.method private t()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->u:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/B;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/B;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private u()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/misettings/common/utils/n;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070179

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    :goto_0
    return-void
.end method

.method private v()Z
    .locals 3

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "force_fsg_nav_bar"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->w()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/m;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    return v1
.end method

.method private w()Z
    .locals 1

    const-string v0, "bo_CN"

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ug_CN"

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private x()Z
    .locals 1

    const-string v0, "bo_CN"

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private y()Z
    .locals 2

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/f;->h(Landroid/content/Context;)F

    move-result v0

    const v1, 0x3f028f5c    # 0.51f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private z()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->l:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->m:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->c(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->b(Z)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d:I

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->b(Z)V

    return-void
.end method

.method public b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const p3, 0x7f0e0148

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->k:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    return-void
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->s:Lmiuix/appcompat/app/j;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->e(Ljava/lang/String;)V

    return-void
.end method

.method public l()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    :cond_1
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    if-ne p1, p3, :cond_0

    const/16 p1, 0x64

    if-ne p2, p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusBaseNoActionbarFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object p1, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->c:Landroid/os/Handler;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->i:Landroid/os/Handler;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->k:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->k:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->f:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->g:Landroid/view/View;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->l:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->m:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->s:Lmiuix/appcompat/app/j;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->y:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->w:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->t:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->b()Lcom/xiaomi/misettings/usagestats/focusmode/c/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->a()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->s:Lmiuix/appcompat/app/j;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/j;->dismiss()V

    :cond_0
    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    const-string v0, "FocusSettingsFragment"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->s:Lmiuix/appcompat/app/j;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->v:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "add_timer_show_dialog_key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onStart()V

    const-string v0, "FocusSettingsFragment"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment$a;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment$a;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->b()Lcom/xiaomi/misettings/usagestats/focusmode/c/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/Observable;->addObserver(Ljava/util/Observer;)V

    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onStop()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->z:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->b()Lcom/xiaomi/misettings/usagestats/focusmode/c/t;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/c;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V

    invoke-virtual {v0, v1}, Ljava/util/Observable;->deleteObserver(Ljava/util/Observer;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusBaseNoActionbarFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const-string v0, "FocusSettingsFragment"

    const-string v1, "onViewCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->H(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->h:Z

    const v0, 0x7f0b020d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->l:Landroid/widget/TextView;

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f1303fc

    goto :goto_0

    :cond_0
    const v0, 0x7f1303fb

    :goto_0
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/misettings/common/utils/m;->e(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/misettings/common/utils/m;->b()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "\n"

    const-string v2, ","

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\uff0c"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0b0200

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->m:Landroid/widget/TextView;

    const v0, 0x7f0b0178

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->f:Landroid/widget/TextView;

    const v0, 0x7f0b0190

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->g:Landroid/view/View;

    const v0, 0x7f0b0195

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->z:Landroid/widget/ImageView;

    const v0, 0x7f0b0261

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->u:Landroid/widget/LinearLayout;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->b(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->r()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->f:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->A()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->s()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->u()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->t()V

    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0

    instance-of p1, p2, Lcom/xiaomi/misettings/usagestats/focusmode/data/j;

    if-eqz p1, :cond_0

    check-cast p2, Lcom/xiaomi/misettings/usagestats/focusmode/data/j;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/j;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d(Z)V

    :cond_0
    return-void
.end method
