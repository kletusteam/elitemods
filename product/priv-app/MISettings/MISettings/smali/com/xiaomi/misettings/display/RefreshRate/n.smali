.class Lcom/xiaomi/misettings/display/RefreshRate/n;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/display/RefreshRate/p$a;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/display/RefreshRate/m;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/xiaomi/misettings/display/RefreshRate/p$a;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/display/RefreshRate/p$a;I)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->b:Lcom/xiaomi/misettings/display/RefreshRate/p$a;

    iput p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->b:Lcom/xiaomi/misettings/display/RefreshRate/p$a;

    invoke-static {p1}, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->a(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)Lcom/xiaomi/misettings/display/RefreshRate/m;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->b:Lcom/xiaomi/misettings/display/RefreshRate/p$a;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->a(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)Lcom/xiaomi/misettings/display/RefreshRate/m;

    move-result-object v0

    iget-boolean v0, v0, Lcom/xiaomi/misettings/display/RefreshRate/m;->e:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->e:Z

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->b:Lcom/xiaomi/misettings/display/RefreshRate/p$a;

    invoke-static {p1}, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->b(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->b:Lcom/xiaomi/misettings/display/RefreshRate/p$a;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->a(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)Lcom/xiaomi/misettings/display/RefreshRate/m;

    move-result-object v0

    iget-boolean v0, v0, Lcom/xiaomi/misettings/display/RefreshRate/m;->e:Z

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, " packagename is "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->b:Lcom/xiaomi/misettings/display/RefreshRate/p$a;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->a(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)Lcom/xiaomi/misettings/display/RefreshRate/m;

    move-result-object v0

    iget-object v0, v0, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " and checked value is "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->b:Lcom/xiaomi/misettings/display/RefreshRate/p$a;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->a(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)Lcom/xiaomi/misettings/display/RefreshRate/m;

    move-result-object v0

    iget-boolean v0, v0, Lcom/xiaomi/misettings/display/RefreshRate/m;->e:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, " and position is "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->a:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "HighRefreshItemAdapter"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->b:Lcom/xiaomi/misettings/display/RefreshRate/p$a;

    iget-object p1, p1, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->g:Lcom/xiaomi/misettings/display/RefreshRate/p;

    invoke-static {p1}, Lcom/xiaomi/misettings/display/RefreshRate/p;->a(Lcom/xiaomi/misettings/display/RefreshRate/p;)Lcom/xiaomi/misettings/display/RefreshRate/k;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->b:Lcom/xiaomi/misettings/display/RefreshRate/p$a;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->a(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)Lcom/xiaomi/misettings/display/RefreshRate/m;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/n;->b:Lcom/xiaomi/misettings/display/RefreshRate/p$a;

    invoke-static {v1}, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->a(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)Lcom/xiaomi/misettings/display/RefreshRate/m;

    move-result-object v1

    iget-boolean v1, v1, Lcom/xiaomi/misettings/display/RefreshRate/m;->e:Z

    invoke-interface {p1, v0, v1}, Lcom/xiaomi/misettings/display/RefreshRate/k;->a(Lcom/xiaomi/misettings/display/RefreshRate/m;Z)V

    return-void
.end method
