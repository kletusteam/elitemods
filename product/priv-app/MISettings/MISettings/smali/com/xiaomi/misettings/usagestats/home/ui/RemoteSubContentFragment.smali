.class public Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;
.super Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/d/a/b;


# static fields
.field public static k:Z


# instance fields
.field private l:Lcom/miui/greenguard/entity/FamilyBean;

.field private m:I

.field private n:I

.field private o:Lcom/miui/greenguard/entity/DashBordBean;

.field private p:Landroid/content/BroadcastReceiver;

.field private q:Landroid/view/View;

.field private r:Lcom/miui/greenguard/result/DeviceLimitResult;

.field private final s:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->m:I

    const/4 v0, -0x5

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->n:I

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/A;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/A;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->s:Landroid/os/Handler;

    return-void
.end method

.method private a(J)I
    .locals 2

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method private a(Lcom/miui/greenguard/entity/DashBordBean;)I
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->n:I

    const/4 v1, -0x5

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->isWeekDateData()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getDeviceUsage()Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;->getMonthDetail()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->n:I

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getCurrentDate()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(J)I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->n:I

    :cond_1
    :goto_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->n:I

    return p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;Lcom/miui/greenguard/entity/DashBordBean;)Lcom/miui/greenguard/entity/DashBordBean;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->o:Lcom/miui/greenguard/entity/DashBordBean;

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)Lcom/miui/greenguard/entity/FamilyBean;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;Lcom/miui/greenguard/result/DeviceLimitResult;)Lcom/miui/greenguard/result/DeviceLimitResult;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->r:Lcom/miui/greenguard/result/DeviceLimitResult;

    return-object p1
.end method

.method public static a(ZLcom/miui/greenguard/entity/FamilyBean;)Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;
    .locals 3

    new-instance p1, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    invoke-direct {p1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "newInstance"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "RemoteSubContentFragment"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "isWeek"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p1, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;Lcom/miui/greenguard/entity/DashBordBean;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->a(Lcom/miui/greenguard/entity/DashBordBean;)I

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)Lcom/miui/greenguard/result/DeviceLimitResult;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->r:Lcom/miui/greenguard/result/DeviceLimitResult;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->v()V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->s()V

    return-void
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->u()V

    return-void
.end method

.method private q()I
    .locals 6

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->o:Lcom/miui/greenguard/entity/DashBordBean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean;->getDeviceUsage()Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;->getMonthDetail()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->n:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v1, v0

    return v1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->o:Lcom/miui/greenguard/entity/DashBordBean;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->a(J)I

    move-result v0

    return v0

    :cond_2
    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean;->getToday()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(J)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->n:I

    sub-int/2addr v3, v2

    int-to-long v2, v3

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->a(J)I

    move-result v0

    return v0
.end method

.method private r()J
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->o:Lcom/miui/greenguard/entity/DashBordBean;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/miui/greenguard/entity/DashBordBean;->getToday()J

    move-result-wide v0

    :goto_0
    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(J)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->n:I

    sub-int/2addr v3, v2

    int-to-long v2, v3

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private s()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/miui/greenguard/params/GetDeviceLimitParam;

    invoke-direct {v0}, Lcom/miui/greenguard/params/GetDeviceLimitParam;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->getUserId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/params/GetBaseParam;->setUid(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/params/GetBaseParam;->setDeviceId(Ljava/lang/String;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/C;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/C;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V

    invoke-static {v0, v1}, Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;

    return-void
.end method

.method private t()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->q:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private u()V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->k()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->q:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private v()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/miui/greenguard/params/GetMandatoryRestParam;

    invoke-direct {v0}, Lcom/miui/greenguard/params/GetMandatoryRestParam;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/params/GetBaseParam;->setDeviceId(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->getUserId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/params/GetBaseParam;->setUid(Ljava/lang/String;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/E;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/E;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V

    invoke-static {v0, v1}, Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->n:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l()V

    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->a(Landroid/view/View;)V

    const v0, 0x7f0b03bf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->q:Landroid/view/View;

    return-void
.end method

.method public a(Lcom/miui/greenguard/params/PostMandatoryRestParam;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean;->getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/miui/greenguard/params/PostMandatoryRestParam;->setDeviceId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean;->getUserId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/miui/greenguard/params/PostMandatoryRestParam;->setUid(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "postMandatoryRest:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lb/e/a/b/c;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RemoteSubContentFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/z;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/z;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V

    invoke-static {p1, v0}, Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;

    return-void
.end method

.method public b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const p3, 0x7f0e0061

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method protected k()V
    .locals 2

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->k()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected l()V
    .locals 8

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->t()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->n()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadData"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RemoteSubContentFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/miui/greenguard/params/GetDashBordParam;

    invoke-direct {v0}, Lcom/miui/greenguard/params/GetDashBordParam;-><init>()V

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    invoke-virtual {v0, v2}, Lcom/miui/greenguard/params/GetDashBordParam;->setDataType(Z)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->q()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/miui/greenguard/params/GetDashBordParam;->setDate(I)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v2}, Lcom/miui/greenguard/entity/FamilyBean;->getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/miui/greenguard/params/GetBaseParam;->setDeviceId(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v2}, Lcom/miui/greenguard/entity/FamilyBean;->getUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/miui/greenguard/params/GetBaseParam;->setUid(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GetDashBordParam:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lb/e/a/b/c;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->r()J

    move-result-wide v5

    iget v7, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->n:I

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/y;

    move-object v2, v1

    move-object v3, p0

    move-object v4, v0

    invoke-direct/range {v2 .. v7}, Lcom/xiaomi/misettings/usagestats/home/ui/y;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;Lcom/miui/greenguard/params/GetDashBordParam;JI)V

    invoke-static {v0, v1}, Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;

    return-void
.end method

.method protected n()V
    .locals 2

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->n()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method o()V
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto/32 :goto_a

    nop

    :goto_1
    invoke-virtual {v1, v2, v0}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    goto/32 :goto_4

    nop

    :goto_2
    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/D;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V

    goto/32 :goto_9

    nop

    :goto_3
    const-string v1, "misettings.action.remote.FROM_STEADY_ON"

    goto/32 :goto_0

    nop

    :goto_4
    return-void

    :goto_5
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    goto/32 :goto_3

    nop

    :goto_6
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_7
    new-instance v0, Landroid/content/IntentFilter;

    goto/32 :goto_5

    nop

    :goto_8
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->p:Landroid/content/BroadcastReceiver;

    goto/32 :goto_1

    nop

    :goto_9
    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->p:Landroid/content/BroadcastReceiver;

    goto/32 :goto_7

    nop

    :goto_a
    const-string v1, "misettings.action.switch.DEVICE"

    goto/32 :goto_d

    nop

    :goto_b
    invoke-static {v1}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_c
    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/D;

    goto/32 :goto_2

    nop

    :goto_d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto/32 :goto_6

    nop
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->o()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->u:Lcom/miui/greenguard/entity/FamilyBean;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->onDestroy()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->p()V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->s:Landroid/os/Handler;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->s:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/B;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/B;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->s()V

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->v()V

    :cond_2
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Lcom/xiaomi/misettings/usagestats/d/a/b;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->l:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Lcom/miui/greenguard/entity/FamilyBean;)V

    return-void
.end method

.method p()V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_5

    nop

    :goto_1
    invoke-virtual {v0, v1}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;)V

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_6
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->p:Landroid/content/BroadcastReceiver;

    goto/32 :goto_0

    nop

    :goto_7
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->p:Landroid/content/BroadcastReceiver;

    goto/32 :goto_1

    nop
.end method
