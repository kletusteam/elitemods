.class public Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;
.super Lcom/xiaomi/misettings/base/BaseFragment;


# instance fields
.field private c:Landroidx/fragment/app/Fragment;

.field private d:Landroidx/fragment/app/Fragment;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/view/View;

.field private h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

.field private i:Landroid/content/BroadcastReceiver;

.field private j:I

.field private k:I

.field private l:I

.field private m:Z

.field private n:Z

.field private o:I

.field private p:Lcom/xiaomi/misettings/usagestats/utils/A;

.field private q:Ljava/lang/String;

.field private r:Z

.field private s:Z

.field t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/FamilyBean;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lcom/miui/greenguard/entity/FamilyBean;

.field public v:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->m:Z

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->o:I

    const-string v1, ""

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->q:Ljava/lang/String;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->r:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->s:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->v:Z

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    return p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    return p1
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->initView(Landroid/view/View;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->e:Landroid/widget/TextView;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/r;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/r;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->f:Landroid/widget/TextView;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/s;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/s;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/FamilyBean;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    if-eqz v0, :cond_8

    if-eqz p1, :cond_8

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_4

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    :goto_0
    const/4 v3, 0x0

    if-ltz v1, :cond_3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v4}, Lcom/miui/greenguard/entity/FamilyBean;->isOwner()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0, v3, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v4}, Lcom/miui/greenguard/entity/FamilyBean;->isChild()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v4}, Lcom/miui/greenguard/entity/FamilyBean;->hasDevice()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, v2, :cond_4

    return-void

    :cond_4
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/entity/FamilyBean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->isOwner()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->isAllShowAccountList()Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_4

    :cond_5
    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->t:Ljava/util/List;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->t:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/greenguard/entity/FamilyBean;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->u:Lcom/miui/greenguard/entity/FamilyBean;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "saveUserId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HomeContentFragment2"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v3

    :goto_2
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->t:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->t:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v2}, Lcom/miui/greenguard/entity/FamilyBean;->getUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->t:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/entity/FamilyBean;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->u:Lcom/miui/greenguard/entity/FamilyBean;

    goto :goto_3

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    move v0, v3

    :goto_3
    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/miui/greenguard/manager/a/g;->a(Ljava/util/List;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v3, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    new-instance p1, Lcom/xiaomi/misettings/usagestats/adapter/h;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->t:Ljava/util/List;

    invoke-direct {p1, v1}, Lcom/xiaomi/misettings/usagestats/adapter/h;-><init>(Ljava/util/List;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/i;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/i;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    invoke-virtual {p1, v1}, Lcom/xiaomi/misettings/usagestats/adapter/h;->a(Lcom/xiaomi/misettings/usagestats/adapter/h$b;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    invoke-virtual {v1, p1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-eqz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/adapter/h;->a(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/g;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/g;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    :cond_8
    :goto_4
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->m:Z

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->j:I

    return p1
.end method

.method private b(Z)I
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz p1, :cond_0

    const p1, 0x7f060364

    goto :goto_0

    :cond_0
    const p1, 0x7f060365

    :goto_0
    invoke-virtual {v0, p1}, Landroid/content/Context;->getColor(I)I

    move-result p1

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)Landroidx/fragment/app/Fragment;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->r:Z

    return p1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->o:I

    return p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->k:I

    return p1
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->q:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->u()V

    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->p()V

    return-void
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->v()V

    return-void
.end method

.method static synthetic h(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)Landroidx/fragment/app/Fragment;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    return-object p0
.end method

.method static synthetic i(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    return-object p0
.end method

.method private initView(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f0b0204

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->e:Landroid/widget/TextView;

    const v0, 0x7f0b0205

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->f:Landroid/widget/TextView;

    const v0, 0x7f0b01e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->g:Landroid/view/View;

    const v0, 0x7f0b02e1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    const v0, 0x7f0b01e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initTabIndex:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HomeContentFragment2"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/q;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/q;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->v()V

    return-void
.end method

.method static synthetic j(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->w()V

    return-void
.end method

.method static synthetic k(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->s()V

    return-void
.end method

.method static synthetic l(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->j:I

    return p0
.end method

.method static synthetic m(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->g:Landroid/view/View;

    return-object p0
.end method

.method public static synthetic n(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->t()V

    return-void
.end method

.method private o()V
    .locals 8

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->m:Z

    const-string v1, ""

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "misettings_from_page"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "screen_time_home_intent_key"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    move-object v1, v2

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "dayFragment"

    invoke-virtual {v2, v3}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v4

    iput-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    const-string v4, "weekFragment"

    invoke-virtual {v2, v4}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v4

    iput-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    invoke-virtual {v2}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v2

    const v4, 0x7f020001

    const v5, 0x7f020002

    invoke-virtual {v2, v4, v5}, Landroidx/fragment/app/qa;->a(II)Landroidx/fragment/app/qa;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addDayFragment:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "HomeContentFragment2"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    const/4 v6, 0x0

    if-eqz v4, :cond_2

    iget-boolean v7, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->s:Z

    if-eqz v7, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v2, v4}, Landroidx/fragment/app/qa;->e(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    goto :goto_3

    :cond_2
    :goto_1
    const-string v4, "addDayFragment: day null"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->r:Z

    if-eqz v4, :cond_3

    invoke-static {v6, v1, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    move-result-object v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->u:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-static {v6, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->a(ZLcom/miui/greenguard/entity/FamilyBean;)Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    const v0, 0x7f0b019c

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    invoke-virtual {v2, v0, v1, v3}, Landroidx/fragment/app/qa;->a(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/qa;

    :goto_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    if-eqz v0, :cond_4

    invoke-virtual {v2, v0}, Landroidx/fragment/app/qa;->c(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    :cond_4
    invoke-virtual {v2}, Landroidx/fragment/app/qa;->b()I

    iput-boolean v6, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->s:Z

    return-void
.end method

.method public static synthetic o(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->r()V

    return-void
.end method

.method private p()V
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->o()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->q()V

    :goto_0
    return-void
.end method

.method private q()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "weekFragment"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    const-string v2, "dayFragment"

    invoke-virtual {v0, v2}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    const v2, 0x7f020001

    const v3, 0x7f020002

    invoke-virtual {v0, v2, v3}, Landroidx/fragment/app/qa;->a(II)Landroidx/fragment/app/qa;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->s:Z

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->o:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->c(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->e(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    goto :goto_2

    :cond_2
    :goto_0
    const-string v2, "HomeContentFragment2"

    const-string v3, "addDayFragment: week null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->r:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    const-string v2, ""

    invoke-static {v3, v2}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->a(ZLjava/lang/String;)Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    move-result-object v2

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->u:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-static {v3, v2}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->a(ZLcom/miui/greenguard/entity/FamilyBean;)Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    move-result-object v2

    :goto_1
    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    const v2, 0x7f0b019c

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0, v2, v3, v1}, Landroidx/fragment/app/qa;->a(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/qa;

    :goto_2
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    if-eqz v1, :cond_4

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->c(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    :cond_4
    invoke-virtual {v0}, Landroidx/fragment/app/qa;->b()I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->s:Z

    return-void
.end method

.method private r()V
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->o()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->q()V

    :goto_0
    return-void
.end method

.method private s()V
    .locals 2

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/manager/a/g;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/h;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/h;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/manager/a/g;->b(Lcom/miui/greenguard/manager/a/g$c;)V

    return-void
.end method

.method private t()V
    .locals 3

    :try_start_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->d(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {v0}, Landroidx/fragment/app/qa;->b()I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->d(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {v0}, Landroidx/fragment/app/qa;->b()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "HomeContentFragment2"

    const-string v2, "removeDayAndWeekFragment error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private u()V
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->m:Z

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->n()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->p()V

    :cond_0
    return-void
.end method

.method private v()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->e:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    move v1, v3

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->b(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->f:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    if-ne v1, v3, :cond_2

    move v2, v3

    :cond_2
    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->b(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private w()V
    .locals 2

    const-string v0, "HomeContentFragment2"

    const-string v1, "viewCreateInit"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->s:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->d(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {v0}, Landroidx/fragment/app/qa;->b()I

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->d(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {v0}, Landroidx/fragment/app/qa;->b()I

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->c:Landroidx/fragment/app/Fragment;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->d:Landroidx/fragment/app/Fragment;

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->n()V

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->v()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->p()V

    return-void
.end method


# virtual methods
.method protected b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e001e

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public synthetic c(I)V
    .locals 1

    if-ltz p1, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->t:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->t:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/greenguard/entity/FamilyBean;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->u:Lcom/miui/greenguard/entity/FamilyBean;

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->r:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->w()V

    :cond_2
    :goto_1
    return-void
.end method

.method public synthetic k()V
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->o:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->u()V

    :cond_0
    return-void
.end method

.method public synthetic l()V
    .locals 2

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/e;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/e;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/manager/a/g;->a(Lcom/miui/greenguard/manager/a/g$b;)V

    return-void
.end method

.method public synthetic m()V
    .locals 2

    new-instance v0, Lcom/miui/greenguard/params/GetFamilyParam;

    invoke-direct {v0}, Lcom/miui/greenguard/params/GetFamilyParam;-><init>()V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/p;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/p;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    invoke-static {v0, v1}, Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;

    return-void
.end method

.method public n()V
    .locals 7

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->n:Z

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "translationX"

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->g:Landroid/view/View;

    new-array v2, v2, [F

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    if-ne v6, v5, :cond_0

    move v6, v4

    goto :goto_0

    :cond_0
    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->k:I

    int-to-float v6, v6

    :goto_0
    aput v6, v2, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    if-ne v1, v5, :cond_1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->k:I

    int-to-float v4, v1

    :cond_1
    aput v4, v2, v5

    invoke-static {v0, v3, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->g:Landroid/view/View;

    new-array v2, v2, [F

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    if-ne v6, v5, :cond_3

    move v6, v4

    goto :goto_1

    :cond_3
    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->k:I

    neg-int v6, v6

    int-to-float v6, v6

    :goto_1
    aput v6, v2, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    if-ne v1, v5, :cond_4

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->k:I

    neg-int v1, v1

    int-to-float v4, v1

    :cond_4
    aput v4, v2, v5

    invoke-static {v0, v3, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    :goto_2
    const-wide/16 v1, 0xf0

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/appcompat/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string p1, "HomeContentFragment2"

    const-string v0, "onConfigurationChanged: "

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->p()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/H;->b()Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->n:Z

    new-instance p1, Lcom/xiaomi/misettings/usagestats/home/ui/o;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/o;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->i:Landroid/content/BroadcastReceiver;

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/miui/greenguard/manager/a/g;->a(Lcom/miui/greenguard/manager/a/g$b;)V

    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "misettings.action.FROM_STEADY_ON"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "miui.token.change"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "misettings.action.ACTION_REFRESH_HOME_TAB"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1, p1}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    new-instance p1, Lcom/xiaomi/misettings/usagestats/utils/A;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/f;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/f;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/d;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/d;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;)V

    invoke-direct {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/A;-><init>(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->p:Lcom/xiaomi/misettings/usagestats/utils/A;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->i:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->p:Lcom/xiaomi/misettings/usagestats/utils/A;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/A;->d()V

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onLowMemory()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->p:Lcom/xiaomi/misettings/usagestats/utils/A;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/A;->a()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->l:I

    const-string v1, "tabIndex"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->u:Lcom/miui/greenguard/entity/FamilyBean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean;->getUserId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "accountUserId"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onStart()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->p:Lcom/xiaomi/misettings/usagestats/utils/A;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/A;->b()V

    const-string v0, "HomeContentFragment2"

    const-string v1, "HomeContentFragment2 : onStart()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onStop()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->p:Lcom/xiaomi/misettings/usagestats/utils/A;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/A;->c()V

    const-string v0, "HomeContentFragment2"

    const-string v1, "HomeContentFragment2 : onStop()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    const-string v1, "tabIndex"

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->o:I

    const-string v0, "accountUserId"

    const-string v1, ""

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->q:Ljava/lang/String;

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->a(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->p()V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "isSupportMiuiVersion"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object p2

    invoke-virtual {p2}, Lcom/miui/greenguard/manager/a/g;->i()Z

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "HomeContentFragment2"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/manager/a/g;->i()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2;->s()V

    :cond_1
    return-void
.end method
