.class public Lcom/xiaomi/misettings/usagestats/d/a/a/y;
.super Lcom/xiaomi/misettings/usagestats/d/a/a/r;


# static fields
.field public static m:Lcom/xiaomi/misettings/usagestats/f/g;


# instance fields
.field private n:J

.field private o:Lcom/xiaomi/misettings/usagestats/d/d/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const-string p1, "DetailListViewHolder"

    const-string p2, "DetailListViewHolder create"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/d/d/d$a;IZ)V
    .locals 2

    const-string v0, "DetailListViewHolder"

    const-string v1, "ensureShow: show list"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->c:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1, p3}, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->b(Ljava/util/List;Z)V

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->d:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1, p3}, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->a(Ljava/util/List;Z)V

    :goto_0
    return-void
.end method

.method private a(Ljava/util/List;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;Z)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->c()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    const-wide v1, 0x7fffffffffffffffL

    move-wide v2, v1

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_9

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-gtz v7, :cond_0

    goto/16 :goto_5

    :cond_0
    add-int/lit8 v1, v1, 0x1

    const/4 v7, 0x5

    if-lt v0, v7, :cond_1

    goto/16 :goto_6

    :cond_1
    if-nez v0, :cond_2

    iput-wide v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->l:J

    move-wide v2, v5

    :cond_2
    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->c:Landroid/util/SparseArray;

    invoke-virtual {v7, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    const/4 v8, 0x0

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v9, 0x7f0e006a

    invoke-static {v7, v9, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    :cond_3
    new-instance v9, Lcom/xiaomi/misettings/usagestats/d/a/a/w;

    invoke-direct {v9, p0, v4}, Lcom/xiaomi/misettings/usagestats/d/a/a/w;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/y;Lcom/xiaomi/misettings/usagestats/f/d;)V

    invoke-virtual {v7, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v9, Lcom/xiaomi/misettings/usagestats/d/a/a/v;

    invoke-direct {v9, v7}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;-><init>(Landroid/view/View;)V

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->o:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-boolean v7, v7, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz v7, :cond_4

    iget-object v7, v9, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->b:Lcom/xiaomi/misettings/widget/CircleImageView;

    invoke-virtual {v7}, Lcom/xiaomi/misettings/widget/CircleImageView;->setDefaultRadius()V

    :cond_4
    new-instance v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;

    invoke-direct {v7}, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;-><init>()V

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/b;->c()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_6

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    goto :goto_1

    :cond_5
    iput-object v8, v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->a:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    :cond_6
    :goto_1
    iget-object v8, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->a:Landroid/graphics/drawable/Drawable;

    :goto_2
    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/b;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->c:Ljava/lang/String;

    goto :goto_3

    :cond_7
    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/b;->a()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->c:Ljava/lang/String;

    :goto_3
    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/b;->c()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->b:Ljava/lang/String;

    long-to-float v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float/2addr v4, v5

    long-to-float v5, v2

    div-float/2addr v4, v5

    iput v4, v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->d:F

    invoke-virtual {v9, v7}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->a(Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;)V

    if-eqz p2, :cond_8

    iget-object v4, v9, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->c()V

    goto :goto_4

    :cond_8
    iget-object v4, v9, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->b()V

    :goto_4
    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->a()Landroid/view/View;

    move-result-object v5

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    const/4 v8, -0x2

    invoke-direct {v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d:Ljava/util/List;

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_9
    :goto_6
    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d(I)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/d/a/a/y;)Z
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->d()Z

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/d/a/a/y;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->o:Lcom/xiaomi/misettings/usagestats/d/d/i;

    return-object p0
.end method

.method private b(Ljava/util/List;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;Z)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->c()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    const-wide v1, 0x7fffffffffffffffL

    move-wide v2, v1

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-gtz v5, :cond_0

    goto/16 :goto_2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    const/4 v5, 0x5

    if-lt v0, v5, :cond_1

    goto/16 :goto_3

    :cond_1
    if-nez v0, :cond_2

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->l:J

    :cond_2
    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->c:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v6, 0x7f0e006a

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    :cond_3
    new-instance v6, Lcom/xiaomi/misettings/usagestats/d/a/a/x;

    invoke-direct {v6, p0, v4}, Lcom/xiaomi/misettings/usagestats/d/a/a/x;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/y;Lcom/xiaomi/misettings/usagestats/f/h;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v6, Lcom/xiaomi/misettings/usagestats/d/a/a/v;

    invoke-direct {v6, v5}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;-><init>(Landroid/view/View;)V

    new-instance v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;

    invoke-direct {v5}, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;-><init>()V

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ic_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/h;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    iput-object v7, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/h;->c()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->c:Ljava/lang/String;

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->b:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v7

    long-to-float v4, v7

    const/high16 v7, 0x3f800000    # 1.0f

    mul-float/2addr v4, v7

    long-to-float v7, v2

    div-float/2addr v4, v7

    iput v4, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->d:F

    invoke-virtual {v6, v5}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->a(Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;)V

    if-eqz p2, :cond_4

    iget-object v4, v6, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->c()V

    goto :goto_1

    :cond_4
    iget-object v4, v6, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->b()V

    :goto_1
    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->a()Landroid/view/View;

    move-result-object v5

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, -0x2

    invoke-direct {v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_5
    :goto_3
    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d(I)V

    return-void
.end method

.method private d()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->o:Lcom/xiaomi/misettings/usagestats/d/d/i;

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private e()V
    .locals 7

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f13039f

    new-array v2, v2, [Ljava/lang/Object;

    iget-wide v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->n:J

    invoke-static {v5, v6}, Lcom/xiaomi/misettings/usagestats/d/f/a;->a(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->g:Landroid/widget/TextView;

    const v1, 0x7f130396

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f1303a2

    new-array v2, v2, [Ljava/lang/Object;

    iget-wide v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->n:J

    invoke-static {v5, v6}, Lcom/xiaomi/misettings/usagestats/d/f/a;->a(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->g:Landroid/widget/TextView;

    const v1, 0x7f1303a4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->g:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V
    .locals 9

    move-object v0, p1

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Lcom/xiaomi/misettings/usagestats/d/a/a$a;)V

    move-object v0, p2

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/d;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/d$a;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->o:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/a;->a:Z

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/a;->b:Z

    invoke-direct {p0, v0, p4, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->a(Lcom/xiaomi/misettings/usagestats/d/d/d$a;IZ)V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/a;->a:Z

    :cond_0
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->e:Ljava/util/List;

    invoke-interface {v1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v1

    iget-wide v1, v1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    iput-wide v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->n:J

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->b()Z

    move-result v1

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isCategory"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DetailListViewHolder"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->e()V

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->j:Landroid/view/View;

    new-instance v8, Lcom/xiaomi/misettings/usagestats/d/a/a/b;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p2

    move-object v4, v0

    move-object v5, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/xiaomi/misettings/usagestats/d/a/a/b;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/y;Lcom/xiaomi/misettings/usagestats/d/d/i;Lcom/xiaomi/misettings/usagestats/d/d/d$a;Landroidx/recyclerview/widget/RecyclerView$a;I)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, v0, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->e:Ljava/util/List;

    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/g;

    sput-object p1, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->m:Lcom/xiaomi/misettings/usagestats/f/g;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->i:Landroid/view/View;

    new-instance p3, Lcom/xiaomi/misettings/usagestats/d/a/a/c;

    invoke-direct {p3, p0, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/c;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/y;Lcom/xiaomi/misettings/usagestats/d/d/i;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic a(Lcom/xiaomi/misettings/usagestats/d/d/i;Landroid/view/View;)V
    .locals 6

    iget-boolean v0, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;->a()Lcom/miui/greenguard/entity/DashBordBean;

    move-result-object p1

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p2

    invoke-virtual {p2}, Lcom/miui/greenguard/entity/FamilyBean;->getUserId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p2

    invoke-virtual {p2}, Lcom/miui/greenguard/entity/FamilyBean;->getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    move-result-object p2

    invoke-virtual {p2}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getDateType()I

    move-result v3

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getDate()I

    move-result v4

    iget-boolean v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    invoke-static/range {v0 .. v5}, Lcom/miui/greenguard/manager/ExtraRouteManager;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIZ)V

    return-void

    :cond_0
    new-instance p1, Landroid/content/Intent;

    const-string p2, "miui.action.usagestas.NEW_APP_CATEGORY_LIST"

    invoke-direct {p1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    const/4 v0, 0x0

    const-string v1, "key_is_week"

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    const-string v1, "key_is_category"

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p1, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {p2, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public synthetic a(Lcom/xiaomi/misettings/usagestats/d/d/i;Lcom/xiaomi/misettings/usagestats/d/d/d$a;Landroidx/recyclerview/widget/RecyclerView$a;ILandroid/view/View;)V
    .locals 2

    iget-boolean p5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    const/4 v0, 0x1

    if-nez p5, :cond_0

    move p5, v0

    goto :goto_0

    :cond_0
    const/4 p5, 0x0

    :goto_0
    iput-boolean p5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    iget-object p5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->o:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    invoke-virtual {p5, v1}, Lcom/xiaomi/misettings/usagestats/d/d/i;->a(Z)V

    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setOnClickListener isCategory"

    invoke-virtual {p5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    invoke-virtual {p5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    const-string v1, "DetailListViewHolder"

    invoke-static {v1, p5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p1

    iget-boolean p5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    const-string v1, "default_category"

    invoke-virtual {p1, v1, p5}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Z)V

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->e()V

    iput-boolean v0, p2, Lcom/xiaomi/misettings/usagestats/d/d/a;->b:Z

    move-object p1, p3

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->c()V

    iput-boolean v0, p2, Lcom/xiaomi/misettings/usagestats/d/d/a;->a:Z

    invoke-virtual {p3, p4}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->a(Z)V

    return-void
.end method

.method protected b()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->o:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/d/i;->c()Z

    move-result v0

    return v0

    :cond_0
    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->b()Z

    move-result v0

    return v0
.end method
