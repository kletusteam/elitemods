.class public Lcom/xiaomi/misettings/usagestats/search/g;
.super Landroidx/recyclerview/widget/RecyclerView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/search/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Landroidx/recyclerview/widget/RecyclerView$t;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/content/Context;

.field private h:Lcom/xiaomi/misettings/usagestats/f/g;

.field public i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    sget-boolean v0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->a:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->b:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->c:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->d:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->e:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->f:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->i:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->g:Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->f:Ljava/util/List;

    sget-object p1, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->m:Lcom/xiaomi/misettings/usagestats/f/g;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->h:Lcom/xiaomi/misettings/usagestats/f/g;

    return-void
.end method

.method private a(I)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sget-boolean v1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    const-string v2, "isWeek"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "packageName"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "hasTime"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->h:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v1

    iget-wide v1, v1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    const-string v3, "dayBeginTime"

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->h:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Lcom/xiaomi/misettings/usagestats/f/g;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    const-string v1, "usageList"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->g:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    return-void
.end method

.method private a(ILcom/xiaomi/misettings/usagestats/search/g$a;)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/e;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->f:Ljava/util/List;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/search/g;->g:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/D;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/search/g;->d:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->b(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->g:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/search/g;->c(Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->c(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->c(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/search/g;->g:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->d(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/view/View;

    move-result-object p1

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    const/4 v2, 0x4

    :goto_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p2, v1}, Lcom/xiaomi/misettings/usagestats/search/g$a;->a(Z)V

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long p1, v1, v3

    if-eqz p1, :cond_2

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->e(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/widget/TextView;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/search/g;->g:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v0

    invoke-static {p2, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->e(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/widget/TextView;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/search/g;->g:Landroid/content/Context;

    const v0, 0x7f1303a9

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/search/g;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/search/g;->b(I)V

    return-void
.end method

.method private b(I)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/c/b$a;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    move-object v1, v2

    :goto_0
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const/4 v0, 0x1

    const-string v3, "isWeek"

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "packageName"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-object v0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->c:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    const-string v1, "weekInfo"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    new-instance v0, Lcom/misettings/common/base/a;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->g:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/misettings/common/base/a;-><init>(Landroid/content/Context;)V

    const-string v1, "com.xiaomi.misettings.usagestats.ui.NewAppUsageDetailFragment"

    invoke-virtual {v0, v1}, Lcom/misettings/common/base/a;->a(Ljava/lang/String;)Lcom/misettings/common/base/a;

    invoke-virtual {v0, p1}, Lcom/misettings/common/base/a;->a(Landroid/os/Bundle;)Lcom/misettings/common/base/a;

    const/4 p1, 0x0

    invoke-virtual {v0, v2, p1}, Lcom/misettings/common/base/a;->a(Landroid/app/Fragment;I)Lcom/misettings/common/base/a;

    invoke-virtual {v0}, Lcom/misettings/common/base/a;->b()V

    return-void
.end method

.method private b(ILcom/xiaomi/misettings/usagestats/search/g$a;)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/c/b$a;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->f:Ljava/util/List;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->b(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/search/g;->c(Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->c(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->c(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->d(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    const/4 v2, 0x4

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p2, v0}, Lcom/xiaomi/misettings/usagestats/search/g$a;->a(Z)V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->e(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/widget/TextView;

    move-result-object p2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->g:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/c/b$a;->b()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->e(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/widget/TextView;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/search/g;->g:Landroid/content/Context;

    const v0, 0x7f1303a9

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/search/g;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/search/g;->a(I)V

    return-void
.end method

.method private c(Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->b:Ljava/lang/String;

    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    :goto_0
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->end()I

    move-result v2

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const/high16 v4, -0x10000

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_0
    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->f:Ljava/util/List;

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->i:Ljava/util/ArrayList;

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->c:Ljava/util/List;

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->b:Ljava/lang/String;

    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->d:Ljava/util/List;

    return-void
.end method

.method public c(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->e:Ljava/util/List;

    return-void
.end method

.method public getItemCount()I
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->a:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    return v1

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->a:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_1

    return v0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/search/g;->e:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_1

    return v0

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    instance-of v0, p1, Lcom/xiaomi/misettings/usagestats/search/g$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/search/g$a;

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/search/g;->a:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p2, p1}, Lcom/xiaomi/misettings/usagestats/search/g;->a(ILcom/xiaomi/misettings/usagestats/search/g$a;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2, p1}, Lcom/xiaomi/misettings/usagestats/search/g;->b(ILcom/xiaomi/misettings/usagestats/search/g$a;)V

    :goto_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/search/g$a;->a(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/search/e;

    invoke-direct {v0, p0, p2}, Lcom/xiaomi/misettings/usagestats/search/e;-><init>(Lcom/xiaomi/misettings/usagestats/search/g;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const/4 v0, 0x0

    if-nez p2, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v1, 0x7f0e013d

    invoke-virtual {p2, v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/xiaomi/misettings/usagestats/search/d;

    invoke-direct {p2, p0, p1}, Lcom/xiaomi/misettings/usagestats/search/d;-><init>(Lcom/xiaomi/misettings/usagestats/search/g;Landroid/view/View;)V

    return-object p2

    :cond_0
    new-instance p2, Lcom/xiaomi/misettings/usagestats/search/g$a;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0e0151

    invoke-virtual {v1, v2, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/xiaomi/misettings/usagestats/search/g$a;-><init>(Landroid/view/View;)V

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->a(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v1, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    const/16 v2, 0x50

    invoke-virtual {p1, v2, v0, v2, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/search/g$a;->a(Lcom/xiaomi/misettings/usagestats/search/g$a;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$t;

    return-object p1
.end method
