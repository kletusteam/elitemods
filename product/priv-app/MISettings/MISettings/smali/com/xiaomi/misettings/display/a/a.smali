.class public Lcom/xiaomi/misettings/display/a/a;
.super Ljava/lang/Object;


# static fields
.field private static final A:Landroid/util/Range;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final B:Landroid/util/Range;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final C:Landroid/util/Range;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final D:Landroid/util/Range;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final E:Landroid/util/Range;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final F:Landroid/util/Range;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final G:Landroid/util/Range;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final H:[Ljava/lang/String;

.field public static final a:Z

.field public static final b:I

.field public static c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I

.field public static final h:I

.field public static final i:I

.field public static final j:I

.field public static final k:I

.field public static final l:I

.field public static final m:I

.field public static final n:I

.field public static final o:I

.field public static final p:I

.field public static final q:I

.field public static final r:I

.field public static final s:I

.field public static final t:I

.field public static final u:I

.field public static final v:I

.field public static final w:I

.field public static final x:I

.field private static final y:Landroid/util/Range;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final z:Landroid/util/Range;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:I

.field private O:I

.field private P:I

.field private Q:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v0, 0x0

    const-string v1, "support_display_expert_mode"

    invoke-static {v1, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/xiaomi/misettings/display/a/a;->a:Z

    const-string v1, "expert_gamut_default"

    invoke-static {v1, v0}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/xiaomi/misettings/display/a/a;->b:I

    sget v1, Lcom/xiaomi/misettings/display/a/a;->b:I

    sput v1, Lcom/xiaomi/misettings/display/a/a;->c:I

    const-string v1, "expert_gamut_min"

    invoke-static {v1, v0}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/xiaomi/misettings/display/a/a;->d:I

    const-string v1, "expert_gamut_max"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/xiaomi/misettings/display/a/a;->e:I

    const/16 v1, 0xff

    const-string v2, "expert_RGB_default"

    invoke-static {v2, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/xiaomi/misettings/display/a/a;->f:I

    const-string v2, "expert_RGB_min"

    invoke-static {v2, v0}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/xiaomi/misettings/display/a/a;->g:I

    const-string v2, "expert_RGB_max"

    invoke-static {v2, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/xiaomi/misettings/display/a/a;->h:I

    const-string v2, "expert_hue_default"

    invoke-static {v2, v0}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/xiaomi/misettings/display/a/a;->i:I

    const-string v2, "expert_hue_min"

    const/16 v3, -0xb4

    invoke-static {v2, v3}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/xiaomi/misettings/display/a/a;->j:I

    const-string v2, "expert_hue_max"

    const/16 v3, 0xb4

    invoke-static {v2, v3}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/xiaomi/misettings/display/a/a;->k:I

    const-string v2, "expert_saturation_default"

    invoke-static {v2, v0}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/xiaomi/misettings/display/a/a;->l:I

    const-string v2, "expert_saturation_min"

    const/16 v3, -0x32

    invoke-static {v2, v3}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/xiaomi/misettings/display/a/a;->m:I

    const/16 v2, 0x64

    const-string v3, "expert_saturation_max"

    invoke-static {v3, v2}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v3

    sput v3, Lcom/xiaomi/misettings/display/a/a;->n:I

    const-string v3, "expert_value_default"

    invoke-static {v3, v0}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v3

    sput v3, Lcom/xiaomi/misettings/display/a/a;->o:I

    const-string v3, "expert_value_min"

    const/16 v4, -0xff

    invoke-static {v3, v4}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v3

    sput v3, Lcom/xiaomi/misettings/display/a/a;->p:I

    const-string v3, "expert_value_max"

    invoke-static {v3, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/xiaomi/misettings/display/a/a;->q:I

    const-string v1, "expert_contrast_default"

    invoke-static {v1, v0}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/xiaomi/misettings/display/a/a;->r:I

    const-string v0, "expert_contrast_min"

    const/16 v1, -0x64

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/xiaomi/misettings/display/a/a;->s:I

    const-string v0, "expert_contrast_max"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/xiaomi/misettings/display/a/a;->t:I

    const-string v0, "expert_gamma_default"

    const/16 v1, 0xdc

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/xiaomi/misettings/display/a/a;->u:I

    const-string v0, "expert_gamma_min"

    const/16 v1, 0xaa

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/xiaomi/misettings/display/a/a;->v:I

    const-string v0, "expert_gamma_max"

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/xiaomi/misettings/display/a/a;->w:I

    sget v0, Lcom/xiaomi/misettings/display/a/a;->v:I

    sput v0, Lcom/xiaomi/misettings/display/a/a;->x:I

    new-instance v0, Landroid/util/Range;

    sget v1, Lcom/xiaomi/misettings/display/a/a;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/a/a;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Range;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    sput-object v0, Lcom/xiaomi/misettings/display/a/a;->y:Landroid/util/Range;

    new-instance v0, Landroid/util/Range;

    sget v1, Lcom/xiaomi/misettings/display/a/a;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/a/a;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Range;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    sput-object v0, Lcom/xiaomi/misettings/display/a/a;->z:Landroid/util/Range;

    new-instance v0, Landroid/util/Range;

    sget v1, Lcom/xiaomi/misettings/display/a/a;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/a/a;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Range;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    sput-object v0, Lcom/xiaomi/misettings/display/a/a;->A:Landroid/util/Range;

    new-instance v0, Landroid/util/Range;

    sget v1, Lcom/xiaomi/misettings/display/a/a;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/a/a;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Range;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    sput-object v0, Lcom/xiaomi/misettings/display/a/a;->B:Landroid/util/Range;

    new-instance v0, Landroid/util/Range;

    sget v1, Lcom/xiaomi/misettings/display/a/a;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/a/a;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Range;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    sput-object v0, Lcom/xiaomi/misettings/display/a/a;->C:Landroid/util/Range;

    new-instance v0, Landroid/util/Range;

    sget v1, Lcom/xiaomi/misettings/display/a/a;->m:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/a/a;->n:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Range;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    sput-object v0, Lcom/xiaomi/misettings/display/a/a;->D:Landroid/util/Range;

    new-instance v0, Landroid/util/Range;

    sget v1, Lcom/xiaomi/misettings/display/a/a;->p:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/a/a;->q:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Range;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    sput-object v0, Lcom/xiaomi/misettings/display/a/a;->E:Landroid/util/Range;

    new-instance v0, Landroid/util/Range;

    sget v1, Lcom/xiaomi/misettings/display/a/a;->s:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/a/a;->t:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Range;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    sput-object v0, Lcom/xiaomi/misettings/display/a/a;->F:Landroid/util/Range;

    new-instance v0, Landroid/util/Range;

    sget v1, Lcom/xiaomi/misettings/display/a/a;->v:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/a/a;->w:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Range;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    sput-object v0, Lcom/xiaomi/misettings/display/a/a;->G:Landroid/util/Range;

    const-string v3, "color_gamut"

    const-string v4, "color_r"

    const-string v5, "color_g"

    const-string v6, "color_b"

    const-string v7, "color_hue"

    const-string v8, "color_saturation"

    const-string v9, "color_value"

    const-string v10, "contrast_ratio"

    const-string v11, "gamma"

    filled-new-array/range {v3 .. v11}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/misettings/display/a/a;->H:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IIIIIIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/xiaomi/misettings/display/a/a;->I:I

    iput p2, p0, Lcom/xiaomi/misettings/display/a/a;->J:I

    iput p3, p0, Lcom/xiaomi/misettings/display/a/a;->K:I

    iput p4, p0, Lcom/xiaomi/misettings/display/a/a;->L:I

    iput p5, p0, Lcom/xiaomi/misettings/display/a/a;->M:I

    iput p6, p0, Lcom/xiaomi/misettings/display/a/a;->N:I

    iput p7, p0, Lcom/xiaomi/misettings/display/a/a;->O:I

    iput p8, p0, Lcom/xiaomi/misettings/display/a/a;->P:I

    iput p9, p0, Lcom/xiaomi/misettings/display/a/a;->Q:I

    return-void
.end method

.method public static a()Lcom/xiaomi/misettings/display/a/a;
    .locals 11

    new-instance v10, Lcom/xiaomi/misettings/display/a/a;

    sget v1, Lcom/xiaomi/misettings/display/a/a;->c:I

    sget v4, Lcom/xiaomi/misettings/display/a/a;->f:I

    sget v5, Lcom/xiaomi/misettings/display/a/a;->i:I

    sget v6, Lcom/xiaomi/misettings/display/a/a;->l:I

    sget v7, Lcom/xiaomi/misettings/display/a/a;->o:I

    sget v8, Lcom/xiaomi/misettings/display/a/a;->r:I

    sget v9, Lcom/xiaomi/misettings/display/a/a;->u:I

    move-object v0, v10

    move v2, v4

    move v3, v4

    invoke-direct/range {v0 .. v9}, Lcom/xiaomi/misettings/display/a/a;-><init>(IIIIIIIII)V

    return-object v10
.end method

.method public static a(Landroid/content/Context;)Lcom/xiaomi/misettings/display/a/a;
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "expert_data"

    invoke-static {p0, v0}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/xiaomi/misettings/display/a/a;->a(Lorg/json/JSONObject;)Lcom/xiaomi/misettings/display/a/a;

    move-result-object p0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    const-string v0, "ExpertData"

    const-string v2, "getFromDatabase failed"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    return-object v1
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/xiaomi/misettings/display/a/a;
    .locals 11

    :try_start_0
    new-instance v10, Lcom/xiaomi/misettings/display/a/a;

    const-string v0, "color_gamut"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v0, "color_r"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "color_g"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v0, "color_b"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v0, "color_hue"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v0, "color_saturation"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v0, "color_value"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string v0, "contrast_ratio"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v0, "gamma"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/xiaomi/misettings/display/a/a;-><init>(IIIIIIIII)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v10

    :catch_0
    move-exception p0

    const-string v0, "ExpertData"

    const-string v1, "createFromJson failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    const/4 p0, 0x0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/display/a/a;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/a/a;->b()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/a/a;->b()Lorg/json/JSONObject;

    move-result-object p1

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "expert_data"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    :cond_0
    const-string p0, "ExpertData"

    const-string p1, "saveToDatabase failed"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public static b(I)Landroid/util/Range;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    packed-switch p0, :pswitch_data_0

    const-string p0, "ExpertData"

    const-string v0, "getRangeByCookie cookie illegal"

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x0

    return-object p0

    :pswitch_0
    sget-object p0, Lcom/xiaomi/misettings/display/a/a;->G:Landroid/util/Range;

    return-object p0

    :pswitch_1
    sget-object p0, Lcom/xiaomi/misettings/display/a/a;->F:Landroid/util/Range;

    return-object p0

    :pswitch_2
    sget-object p0, Lcom/xiaomi/misettings/display/a/a;->E:Landroid/util/Range;

    return-object p0

    :pswitch_3
    sget-object p0, Lcom/xiaomi/misettings/display/a/a;->D:Landroid/util/Range;

    return-object p0

    :pswitch_4
    sget-object p0, Lcom/xiaomi/misettings/display/a/a;->C:Landroid/util/Range;

    return-object p0

    :pswitch_5
    sget-object p0, Lcom/xiaomi/misettings/display/a/a;->B:Landroid/util/Range;

    return-object p0

    :pswitch_6
    sget-object p0, Lcom/xiaomi/misettings/display/a/a;->A:Landroid/util/Range;

    return-object p0

    :pswitch_7
    sget-object p0, Lcom/xiaomi/misettings/display/a/a;->z:Landroid/util/Range;

    return-object p0

    :pswitch_8
    sget-object p0, Lcom/xiaomi/misettings/display/a/a;->y:Landroid/util/Range;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static l(I)V
    .locals 0

    sput p0, Lcom/xiaomi/misettings/display/a/a;->c:I

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    const-string p1, "ExpertData"

    const-string v0, "getByCookie cookie illegal"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    return p1

    :pswitch_0
    iget p1, p0, Lcom/xiaomi/misettings/display/a/a;->Q:I

    return p1

    :pswitch_1
    iget p1, p0, Lcom/xiaomi/misettings/display/a/a;->P:I

    return p1

    :pswitch_2
    iget p1, p0, Lcom/xiaomi/misettings/display/a/a;->O:I

    return p1

    :pswitch_3
    iget p1, p0, Lcom/xiaomi/misettings/display/a/a;->N:I

    return p1

    :pswitch_4
    iget p1, p0, Lcom/xiaomi/misettings/display/a/a;->M:I

    return p1

    :pswitch_5
    iget p1, p0, Lcom/xiaomi/misettings/display/a/a;->L:I

    return p1

    :pswitch_6
    iget p1, p0, Lcom/xiaomi/misettings/display/a/a;->K:I

    return p1

    :pswitch_7
    iget p1, p0, Lcom/xiaomi/misettings/display/a/a;->J:I

    return p1

    :pswitch_8
    iget p1, p0, Lcom/xiaomi/misettings/display/a/a;->I:I

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(II)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    const-string p1, "ExpertData"

    const-string p2, "setByCookie cookie illegal"

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/display/a/a;->k(I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/display/a/a;->j(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/display/a/a;->i(I)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/display/a/a;->h(I)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/display/a/a;->f(I)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/display/a/a;->c(I)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/display/a/a;->d(I)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/display/a/a;->g(I)V

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/display/a/a;->e(I)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()Lorg/json/JSONObject;
    .locals 3

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "color_gamut"

    iget v2, p0, Lcom/xiaomi/misettings/display/a/a;->I:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "color_r"

    iget v2, p0, Lcom/xiaomi/misettings/display/a/a;->J:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "color_g"

    iget v2, p0, Lcom/xiaomi/misettings/display/a/a;->K:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "color_b"

    iget v2, p0, Lcom/xiaomi/misettings/display/a/a;->L:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "color_hue"

    iget v2, p0, Lcom/xiaomi/misettings/display/a/a;->M:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "color_saturation"

    iget v2, p0, Lcom/xiaomi/misettings/display/a/a;->N:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "color_value"

    iget v2, p0, Lcom/xiaomi/misettings/display/a/a;->O:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "contrast_ratio"

    iget v2, p0, Lcom/xiaomi/misettings/display/a/a;->P:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "gamma"

    iget v2, p0, Lcom/xiaomi/misettings/display/a/a;->Q:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/display/a/a;->L:I

    return-void
.end method

.method public d(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/display/a/a;->K:I

    return-void
.end method

.method public e(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/display/a/a;->I:I

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lcom/xiaomi/misettings/display/a/a;

    iget v0, p0, Lcom/xiaomi/misettings/display/a/a;->I:I

    iget v1, p1, Lcom/xiaomi/misettings/display/a/a;->I:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/display/a/a;->J:I

    iget v1, p1, Lcom/xiaomi/misettings/display/a/a;->J:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/display/a/a;->K:I

    iget v1, p1, Lcom/xiaomi/misettings/display/a/a;->K:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/display/a/a;->L:I

    iget v1, p1, Lcom/xiaomi/misettings/display/a/a;->L:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/display/a/a;->M:I

    iget v1, p1, Lcom/xiaomi/misettings/display/a/a;->M:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/display/a/a;->N:I

    iget v1, p1, Lcom/xiaomi/misettings/display/a/a;->N:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/display/a/a;->O:I

    iget v1, p1, Lcom/xiaomi/misettings/display/a/a;->O:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/display/a/a;->P:I

    iget v1, p1, Lcom/xiaomi/misettings/display/a/a;->P:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/display/a/a;->Q:I

    iget p1, p1, Lcom/xiaomi/misettings/display/a/a;->Q:I

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public f(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/display/a/a;->M:I

    return-void
.end method

.method public g(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/display/a/a;->J:I

    return-void
.end method

.method public h(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/display/a/a;->N:I

    return-void
.end method

.method public i(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/display/a/a;->O:I

    return-void
.end method

.method public j(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/display/a/a;->P:I

    return-void
.end method

.method public k(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/display/a/a;->Q:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ExpertData{colorGamut="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/display/a/a;->I:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", colorR="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/display/a/a;->J:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", colorG="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/display/a/a;->K:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", colorB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/display/a/a;->L:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", colorHue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/display/a/a;->M:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", colorSaturation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/display/a/a;->N:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", colorValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/display/a/a;->O:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", contrastRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/display/a/a;->P:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", gamma="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/display/a/a;->Q:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
