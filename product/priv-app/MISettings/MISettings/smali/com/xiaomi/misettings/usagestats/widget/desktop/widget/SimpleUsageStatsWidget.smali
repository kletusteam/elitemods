.class public Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/SimpleUsageStatsWidget;
.super Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/BaseAppWidgetProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/BaseAppWidgetProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisabled(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/BaseAppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/widget/desktop/service/UsageStatsUpdateService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/BaseAppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/widget/desktop/service/UsageStatsUpdateService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/BaseAppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    invoke-static {p1, p3}, Lcom/xiaomi/misettings/usagestats/widget/a/c/c;->b(Landroid/content/Context;[I)V

    return-void
.end method
