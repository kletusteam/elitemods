.class final Lcom/xiaomi/misettings/usagestats/statutoryholidays/e;
.super Ljava/lang/Object;

# interfaces
.implements Lb/c/b/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->g(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/c/b/c/h<",
        "Lcom/xiaomi/misettings/usagestats/statutoryholidays/i;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/e;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const-string v0, "StatutoryHolidayUtils"

    const-string v1, "requestVersionCode: request failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/statutoryholidays/i;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/i;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/e;->a(Lcom/xiaomi/misettings/usagestats/statutoryholidays/i;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    if-eqz p1, :cond_1

    new-instance v0, Lb/a/b/q;

    invoke-direct {v0}, Lb/a/b/q;-><init>()V

    const-class v1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/i;

    invoke-virtual {v0, p1, v1}, Lb/a/b/q;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/i;

    iget v0, p1, Lb/c/b/a/a;->code:I

    if-nez v0, :cond_1

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/i;->a:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSuccess() : request success, the version code is : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " , local version : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->d(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StatutoryHolidayUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/e;->a:Landroid/content/Context;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->a(Landroid/content/Context;J)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->d(Landroid/content/Context;)I

    move-result v0

    if-le p1, v0, :cond_0

    const-string p1, "onSuccess: register upload service alarm"

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/e;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->e(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/e;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->f(Landroid/content/Context;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/e;->a()V

    return-void
.end method
