.class public Lcom/xiaomi/misettings/usagestats/focusmode/b/f;
.super Lcom/xiaomi/misettings/usagestats/focusmode/b/a;


# instance fields
.field private c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/ImageView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private a(I)I
    .locals 2

    const/4 v0, 0x1

    const v1, 0x7f13037b

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const v1, 0x7f13037f

    goto :goto_0

    :cond_1
    const v1, 0x7f13037e

    goto :goto_0

    :cond_2
    const v1, 0x7f13037d

    goto :goto_0

    :cond_3
    const v1, 0x7f13037c

    :cond_4
    :goto_0
    return v1
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    const v0, 0x7f0b0169

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->d:Landroid/widget/TextView;

    const v0, 0x7f0b0167

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->e:Landroid/widget/TextView;

    const v0, 0x7f0b01dc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->f:Landroid/widget/TextView;

    const v0, 0x7f0b0165

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->g:Landroid/widget/TextView;

    const v0, 0x7f0b01b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->h:Landroid/widget/TextView;

    const v0, 0x7f0b0176

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->i:Landroid/widget/TextView;

    const v0, 0x7f0b01b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->j:Landroid/widget/ImageView;

    const v0, 0x7f0b01f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->k:Landroid/widget/TextView;

    const v0, 0x7f0b01f3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->l:Landroid/widget/TextView;

    return-void
.end method

.method private b(I)I
    .locals 2

    const/4 v0, 0x1

    const v1, 0x7f130380

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const v1, 0x7f130384

    goto :goto_0

    :cond_1
    const v1, 0x7f130383

    goto :goto_0

    :cond_2
    const v1, 0x7f130382

    goto :goto_0

    :cond_3
    const v1, 0x7f130381

    :cond_4
    :goto_0
    return v1
.end method

.method private f()V
    .locals 10

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    iget-wide v3, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->totalTime:J

    invoke-static {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->e()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->usedTimes:I

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    const v6, 0x7f11002e

    invoke-virtual {v2, v6, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->e()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->consecutiveDays:I

    new-array v5, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const v6, 0x7f11001c

    invoke-virtual {v2, v6, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->e()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->totalDays:I

    new-array v5, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-virtual {v2, v6, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->e()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->levelName:Ljava/lang/String;

    aput-object v5, v3, v7

    const v5, 0x7f1303eb

    invoke-virtual {v2, v5, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->beat:I

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->e()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f130366

    new-array v5, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->e()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f13040b

    new-array v4, v4, [Ljava/lang/Object;

    iget v9, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->beat:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v7

    invoke-virtual {v6, v8, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v7

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->i:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->h:Landroid/widget/TextView;

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f1303fc

    goto :goto_0

    :cond_1
    const v2, 0x7f1303fb

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->k:Landroid/widget/TextView;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->currentLevel:I

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->b(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->l:Landroid/widget/TextView;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->currentLevel:I

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->j:Landroid/widget/ImageView;

    iget v0, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->currentLevel:I

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)V"
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/c/c;->a(Ljava/lang/Object;)V

    check-cast p1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    return-void
.end method

.method protected b()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    const v1, 0x7f0e0089

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->a(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/f;->f()V

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->d()V

    return-void
.end method
