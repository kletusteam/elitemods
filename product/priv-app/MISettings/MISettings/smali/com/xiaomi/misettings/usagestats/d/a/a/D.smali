.class public Lcom/xiaomi/misettings/usagestats/d/a/a/D;
.super Lcom/xiaomi/misettings/usagestats/d/a/a/s;


# instance fields
.field private c:Lcom/xiaomi/misettings/usagestats/d/d/i;

.field private d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/D;->b()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0704e1

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/D;->b()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0704e5

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/D;->c()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/d/a/a/D;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/D;->c:Lcom/xiaomi/misettings/usagestats/d/d/i;

    return-object p0
.end method

.method private b()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 2

    const v0, 0x7f0b0203

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0b038c

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/D;->d:Landroid/widget/TextView;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/a/a/C;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/C;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/D;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V
    .locals 0

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/D;->c:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/D;->c:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-boolean p2, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz p2, :cond_0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/d/f;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/d/f$a;

    iget-boolean p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->a:Z

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->j(Landroid/content/Context;)Z

    move-result p1

    :goto_0
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/D;->d:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    if-eqz p1, :cond_1

    const p1, 0x7f1300f1

    goto :goto_1

    :cond_1
    const p1, 0x7f1300f2

    :goto_1
    invoke-virtual {p3, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
