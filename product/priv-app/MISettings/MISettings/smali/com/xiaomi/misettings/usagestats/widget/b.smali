.class Lcom/xiaomi/misettings/usagestats/widget/b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/widget/CategoryPagerItemView;->setWeekAppUsageList(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/widget/CategoryPagerItemView;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/widget/CategoryPagerItemView;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b;->a:Lcom/xiaomi/misettings/usagestats/widget/CategoryPagerItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b;->a:Lcom/xiaomi/misettings/usagestats/widget/CategoryPagerItemView;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-gtz p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b;->a:Lcom/xiaomi/misettings/usagestats/widget/CategoryPagerItemView;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->d:Ljava/util/List;

    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/o;

    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    const-string p3, "key_category_data"

    invoke-virtual {p2, p3, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const/4 p1, 0x1

    const-string p3, "key_is_week"

    invoke-virtual {p2, p3, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b;->a:Lcom/xiaomi/misettings/usagestats/widget/CategoryPagerItemView;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;->i:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    const-string p3, "weekInfo"

    invoke-virtual {p2, p3, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    new-instance p1, Lcom/misettings/common/base/a;

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/widget/b;->a:Lcom/xiaomi/misettings/usagestats/widget/CategoryPagerItemView;

    invoke-virtual {p3}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p1, p3}, Lcom/misettings/common/base/a;-><init>(Landroid/content/Context;)V

    const-string p3, "com.xiaomi.misettings.usagestats.ui.CategoryUsageDetailFragment"

    invoke-virtual {p1, p3}, Lcom/misettings/common/base/a;->a(Ljava/lang/String;)Lcom/misettings/common/base/a;

    invoke-virtual {p1, p2}, Lcom/misettings/common/base/a;->a(Landroid/os/Bundle;)Lcom/misettings/common/base/a;

    invoke-virtual {p1}, Lcom/misettings/common/base/a;->b()V

    return-void
.end method
