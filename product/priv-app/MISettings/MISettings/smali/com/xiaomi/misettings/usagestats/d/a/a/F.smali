.class public Lcom/xiaomi/misettings/usagestats/d/a/a/F;
.super Lcom/xiaomi/misettings/usagestats/d/a/a/s;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/d/a/a$a;


# instance fields
.field private c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/LinearLayout;

.field private h:Landroid/view/View;

.field private i:I

.field private j:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;-><init>(Landroid/content/Context;Landroid/view/View;)V

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->j:Landroid/graphics/Rect;

    const p1, 0x7f0b039c

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->e:Landroid/widget/TextView;

    const p1, 0x7f0b039b

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->f:Landroid/widget/TextView;

    const p1, 0x7f0b039a

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->d:Landroid/widget/TextView;

    const p1, 0x7f0b0174

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const p1, 0x7f0b0187

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->g:Landroid/widget/LinearLayout;

    const p1, 0x7f0b01d7

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->h:Landroid/view/View;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->h:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const/16 p2, 0x10

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setBarType(I)V

    return-void
.end method

.method public static synthetic a(Lcom/xiaomi/misettings/usagestats/d/a/a/F;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->b()V

    return-void
.end method

.method private a(Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/h$a;",
            ">;I)V"
        }
    .end annotation

    if-nez p2, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->f:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->f:Landroid/widget/TextView;

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->b(Ljava/util/List;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->g:Landroid/widget/LinearLayout;

    new-instance p2, Lcom/xiaomi/misettings/usagestats/d/a/a/g;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/g;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/F;)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method private b(Ljava/util/List;I)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/h$a;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/h$a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a()I

    move-result p1

    goto :goto_0

    :cond_0
    add-int/lit8 v1, p2, -0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/d/h$a;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a:I

    :goto_0
    const/4 v1, 0x3

    if-ne p2, v1, :cond_1

    iget p2, v0, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a:I

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->f()I

    move-result v0

    div-int/2addr p2, v0

    goto :goto_1

    :cond_1
    iget p2, v0, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a:I

    sget v0, Lcom/xiaomi/misettings/usagestats/utils/L;->d:I

    div-int/2addr p2, v0

    :goto_1
    sget v0, Lcom/xiaomi/misettings/usagestats/utils/L;->d:I

    div-int/2addr p1, v0

    if-nez p1, :cond_2

    const-string p1, ""

    return-object p1

    :cond_2
    if-ne p2, p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const p2, 0x7f1303b6

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    sub-int v0, p2, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v1, v0

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v1, v2

    int-to-float v2, p1

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-le p2, p1, :cond_4

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const p2, 0x7f1303b5

    new-array v5, v4, [Ljava/lang/Object;

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v5, v3

    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_4
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const p2, 0x7f1303b4

    new-array v5, v4, [Ljava/lang/Object;

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v5, v3

    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_2
    const-string p2, "\\d+.\\d+"

    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/regex/Matcher;->find()Z

    invoke-virtual {p2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object p2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11002e

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private b()V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->f:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->f:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTextSize()F

    move-result v3

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/CharSequence;FI)I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->e:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTextSize()F

    move-result v5

    invoke-static {v2, v3, v5, v4}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/CharSequence;FI)I

    move-result v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->f:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v3}, Landroid/widget/LinearLayout$LayoutParams;->getMarginStart()I

    move-result v5

    add-int/2addr v5, v1

    add-int/2addr v5, v2

    if-ge v0, v5, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v1, 0x412e6666    # 10.9f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->h:Landroid/view/View;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->j:Landroid/graphics/Rect;

    invoke-virtual {p1, p2}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "DeviceWeekUnlockViewHolder"

    const-string p2, "onScroll: should anim show"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a()V

    :cond_0
    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V
    .locals 8

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->i:I

    const/4 v0, 0x1

    add-int/2addr p3, v0

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->i:I

    check-cast p2, Lcom/xiaomi/misettings/usagestats/d/d/h;

    iget-object p2, p2, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast p2, Ljava/util/List;

    invoke-interface {p2, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/xiaomi/misettings/usagestats/d/d/h$a;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iget-object v2, p3, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->d:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setWeekUnlockList(Ljava/util/List;)V

    const/4 v1, 0x2

    const v2, 0x7f11001f

    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, p3, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a:I

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, p3, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->e:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-static {v7}, Lcom/xiaomi/misettings/usagestats/d/f/a;->b(Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    iget v7, p3, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-virtual {v4, v2, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, p3, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v3

    iget-object v6, p3, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->e:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-static {v6}, Lcom/xiaomi/misettings/usagestats/d/f/a;->b(Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v0

    invoke-virtual {v4, v2, v5, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x3

    const/high16 v2, 0x3f800000    # 1.0f

    if-ne p4, v1, :cond_0

    iget p3, p3, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a:I

    int-to-float p3, p3

    mul-float/2addr p3, v2

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->f()I

    move-result v1

    int-to-float v1, v1

    goto :goto_1

    :cond_0
    iget p3, p3, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a:I

    int-to-float p3, p3

    mul-float/2addr p3, v2

    const/high16 v1, 0x40e00000    # 7.0f

    :goto_1
    div-float/2addr p3, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f11002f

    float-to-int v5, p3

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p3

    aput-object p3, v6, v3

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p2, p4}, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->a(Ljava/util/List;I)V

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p1, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Lcom/xiaomi/misettings/usagestats/d/a/a$a;)V

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->i:I

    if-lt p1, v0, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/F;->h:Landroid/view/View;

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method
