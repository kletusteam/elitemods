.class Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->l(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->r(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    const-wide/16 v1, 0x1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Ljava/lang/Long;)Ljava/lang/Long;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->v(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->r(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    new-instance v2, Ljava/math/BigDecimal;

    float-to-double v3, v0

    invoke-direct {v2, v3, v4}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->y(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Landroid/graphics/Paint;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->h(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->d(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->x(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->w(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->i(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->e(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->f(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->g(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->h(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/c;->a:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c()V

    return-void
.end method
