.class public Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;
.super Landroid/widget/LinearLayout;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:I

.field private d:I

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->a()V

    return-void
.end method

.method private a()V
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06038a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->d:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06038d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->e:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e005a

    invoke-static {v0, v1, p0}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0b0199

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->a:Landroid/widget/TextView;

    const v0, 0x7f0b0198

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110037

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->c:I

    return v0
.end method

.method public setIndex(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->c:I

    return-void
.end method

.method public setSelect(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->f:Z

    if-eqz p1, :cond_0

    const v0, 0x7f080158

    goto :goto_0

    :cond_0
    const v0, 0x7f080159

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->a:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->d:I

    goto :goto_1

    :cond_1
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->e:I

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->b:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->d:I

    goto :goto_2

    :cond_2
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->e:I

    :goto_2
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/TimeSelectContainer;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
