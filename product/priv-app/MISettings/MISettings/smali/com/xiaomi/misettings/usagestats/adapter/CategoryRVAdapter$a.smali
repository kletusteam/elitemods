.class Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;
.super Landroidx/recyclerview/widget/RecyclerView$t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/LinearLayout;

.field private itemView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$t;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->itemView:Landroid/view/View;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Landroid/view/View;)V

    const v0, 0x7f0b023e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->a:Landroid/widget/ImageView;

    const v0, 0x7f0b0389

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->b:Landroid/widget/TextView;

    const v0, 0x7f0b038a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->c:Landroid/widget/TextView;

    const v0, 0x7f0b0242

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->d:Landroid/view/View;

    const v0, 0x7f0b01c6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->e:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/LinearLayout;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->e:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->b:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->a:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->d:Landroid/view/View;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->c:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->itemView:Landroid/view/View;

    return-object p0
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->e:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/adapter/g;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/adapter/g;-><init>(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
