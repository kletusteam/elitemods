.class public Lcom/xiaomi/misettings/usagestats/widget/b/a;
.super Lcom/xiaomi/misettings/usagestats/widget/b/g;


# instance fields
.field private ba:Landroid/graphics/Paint;

.field private ca:Landroid/graphics/Paint;

.field private da:Landroid/graphics/Paint;

.field private ea:Landroid/graphics/Paint;

.field private fa:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation
.end field

.field private ga:F

.field private ha:Ljava/lang/String;

.field private ia:Ljava/lang/String;

.field private ja:I

.field private ka:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/f/j;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->c:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f13042b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->c:Ljava/text/SimpleDateFormat;

    iget-wide v1, p1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a()V

    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/a;->c(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected a(Landroid/graphics/Canvas;IF)V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v0

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-nez v1, :cond_0

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/f/j;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    if-eqz p2, :cond_0

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    int-to-float v3, p2

    int-to-float p2, p2

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->E:F

    sub-float v5, p2, v1

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->da:Landroid/graphics/Paint;

    move-object v1, p1

    move v2, p3

    move v4, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/a;->a(Lcom/xiaomi/misettings/usagestats/f/j;)Ljava/lang/String;

    move-result-object p2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->E:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    const v2, 0x405147ae    # 3.27f

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ea:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method protected b()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method protected b(I)I
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    sub-int/2addr v1, v2

    :goto_0
    if-ne p1, v1, :cond_1

    const p1, 0x7f060374

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result p1

    return p1

    :cond_1
    iget p1, v0, Lcom/xiaomi/misettings/usagestats/f/j;->b:I

    if-eq p1, v2, :cond_3

    const/4 v0, 0x7

    if-ne p1, v0, :cond_2

    goto :goto_1

    :cond_2
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ja:I

    return p1

    :cond_3
    :goto_1
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ka:I

    return p1
.end method

.method protected b(Landroid/graphics/Canvas;)V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ba:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ja:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    int-to-float v0, v0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ga:F

    div-float/2addr v2, v1

    sub-float/2addr v0, v2

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ga:F

    div-float/2addr v0, v1

    :goto_0
    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->v:F

    div-float/2addr v2, v1

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ga:F

    div-float/2addr v3, v1

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ba:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ba:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->v:F

    div-float/2addr v2, v1

    invoke-static {v0, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result v0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ga:F

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    const v4, 0x40975c29    # 4.73f

    invoke-static {v3, v4}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ha:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v5

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    int-to-float v5, v5

    sub-float/2addr v5, v2

    goto :goto_1

    :cond_1
    move v5, v2

    :goto_1
    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ca:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v5, v0, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ha:Ljava/lang/String;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ca:Landroid/graphics/Paint;

    invoke-virtual {p0, v3, v5}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v3

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    const/high16 v6, 0x41200000    # 10.0f

    invoke-static {v5, v6}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v2, v5

    add-float/2addr v2, v3

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ga:F

    div-float/2addr v3, v1

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ba:Landroid/graphics/Paint;

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ka:I

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    int-to-float v3, v3

    sub-float/2addr v3, v2

    goto :goto_2

    :cond_2
    move v3, v2

    :goto_2
    iget v5, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->v:F

    div-float/2addr v5, v1

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ga:F

    div-float/2addr v6, v1

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ba:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v5, v6}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ga:F

    div-float/2addr v3, v1

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-static {v5, v4}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ia:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v4

    if-eqz v4, :cond_3

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    int-to-float v1, v1

    sub-float/2addr v1, v2

    goto :goto_3

    :cond_3
    iget v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ga:F

    div-float/2addr v4, v1

    add-float v1, v2, v4

    :goto_3
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ca:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v1, v0, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected c()J
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v4

    cmp-long v4, v1, v4

    if-gez v4, :cond_0

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v1

    goto :goto_0

    :cond_1
    return-wide v1
.end method

.method protected c(I)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ne p1, v0, :cond_1

    const p1, 0x7f13041c

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-nez v0, :cond_3

    rem-int/lit8 v0, p1, 0x5

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/f/j;->c:I

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->b:Landroid/util/SparseIntArray;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/f/j;->b:I

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_4
    const-string p1, ""

    return-object p1
.end method

.method protected d()F
    .locals 1

    const v0, 0x7f0704b3

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    return v0
.end method

.method protected d(I)F
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    add-int/lit8 p1, p1, 0x64

    int-to-float p1, p1

    return p1

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->E:F

    sub-float/2addr v0, v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v3

    long-to-float p1, v3

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->N:F

    div-float/2addr p1, v3

    sub-float/2addr v2, p1

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method protected e()F
    .locals 1

    const v0, 0x7f0704f7

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    return v0
.end method

.method protected f(I)Landroid/graphics/Paint$Align;
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    sget-object p1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    return-object p1

    :cond_0
    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f(I)Landroid/graphics/Paint$Align;

    move-result-object p1

    return-object p1
.end method

.method public f()V
    .locals 6

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ba:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ba:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ca:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ca:Landroid/graphics/Paint;

    const v2, 0x7f0704d4

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ca:Landroid/graphics/Paint;

    const v2, 0x7f060381

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ca:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    goto :goto_0

    :cond_0
    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    :goto_0
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ca:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->da:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    const v2, 0x3eb851ec    # 0.36f

    invoke-static {v0, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->da:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/DashPathEffect;

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v0, v4, v5

    aput v0, v4, v1

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->da:Landroid/graphics/Paint;

    const v3, 0x7f060386

    invoke-virtual {p0, v3}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->da:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ea:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ea:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ea:Landroid/graphics/Paint;

    const v1, 0x7f0704ee

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ea:Landroid/graphics/Paint;

    const v1, 0x7f060387

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    const v1, 0x40a2e148    # 5.09f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ga:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ba:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ga:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ba:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    const v0, 0x7f13042c

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ha:Ljava/lang/String;

    const v0, 0x7f13040f

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ia:Ljava/lang/String;

    const v0, 0x7f060372

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ja:I

    const v0, 0x7f060376

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->ka:I

    return-void
.end method

.method public f(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method protected g(I)I
    .locals 1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ne p1, v0, :cond_1

    const p1, 0x7f060373

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result p1

    return p1

    :cond_1
    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g(I)I

    move-result p1

    return p1
.end method

.method protected j(I)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->c:Ljava/text/SimpleDateFormat;

    const v1, 0x7f1303e2

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->c:Ljava/text/SimpleDateFormat;

    iget-wide v2, p1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const p1, 0x7f130404

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->q:Ljava/lang/String;

    return-void
.end method

.method protected k(I)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/a;->fa:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/g;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->r:Ljava/lang/String;

    return-void
.end method
