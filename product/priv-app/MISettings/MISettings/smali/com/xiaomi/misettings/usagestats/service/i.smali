.class Lcom/xiaomi/misettings/usagestats/service/i;
.super Landroid/os/CountDownTimer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;JJ)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/i;->a:Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/i;->a:Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/i;->a:Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    return-void
.end method

.method public onTick(J)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/i;->a:Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->b(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;)Landroid/os/CountDownTimer;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    long-to-int v0, v0

    sput v0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/i;->a:Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;

    invoke-static {v0, p1, p2}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;J)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/i;->a:Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;

    invoke-virtual {p1}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->f(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/i;->a:Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;Z)V

    :cond_1
    return-void
.end method
