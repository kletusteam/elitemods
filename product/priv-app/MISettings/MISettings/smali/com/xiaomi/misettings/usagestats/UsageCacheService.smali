.class public Lcom/xiaomi/misettings/usagestats/UsageCacheService;
.super Landroid/app/Service;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;)V

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->e(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/content/Context;)V

    :cond_1
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    const/4 v0, 0x0

    const-string v1, "key_has_delete_repeat_data"

    invoke-static {p1, v1, v0}, Lcom/xiaomi/misettings/usagestats/provider/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "UsageCacheService"

    const-string v3, "ensureDeleteRepeatData: delete repeat data"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v2, 0x64

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {p1, v0, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;IIJ)Ljava/util/List;

    const/4 v0, 0x1

    invoke-static {p1, v1, v0}, Lcom/xiaomi/misettings/usagestats/provider/a;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageCacheService;->b()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageCacheService;->a()V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->j(Landroid/content/Context;)V

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/A;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/A;-><init>(Lcom/xiaomi/misettings/usagestats/UsageCacheService;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/UsageCacheService;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/UsageCacheService;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/UsageCacheService;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/UsageCacheService;->a(Landroid/content/Intent;)V

    return-void
.end method

.method private b()V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/B;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/B;-><init>(Lcom/xiaomi/misettings/usagestats/UsageCacheService;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "UsageCacheService"

    const-string v1, "onCreate: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->n(Landroid/content/Context;)V

    const-string v0, "UsageCacheService"

    const-string v1, "onDestroy: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    const-string v0, "UsageCacheService"

    const-string v1, "onStartCommand: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/z;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/z;-><init>(Lcom/xiaomi/misettings/usagestats/UsageCacheService;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result p1

    return p1
.end method
