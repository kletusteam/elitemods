.class Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(ILandroid/graphics/Path;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/graphics/PathMeasure;

.field final synthetic b:I

.field final synthetic c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Landroid/graphics/PathMeasure;I)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->a:Landroid/graphics/PathMeasure;

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Z)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->q(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F

    move-result v3

    mul-float/2addr v2, v3

    invoke-static {v0, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->d(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->a:Landroid/graphics/PathMeasure;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->p(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)F

    move-result v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iget-object v4, v3, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->j:[F

    iget-object v3, v3, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->i:[F

    invoke-virtual {v0, v2, v4, v3}, Landroid/graphics/PathMeasure;->getPosTan(F[F[F)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->j:[F

    aget v1, v2, v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->e(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->j:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->f(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;F)F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    float-to-double v0, p1

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    cmpl-double p1, v0, v3

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Z)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->d(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;Z)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->b:I

    iput v0, p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->u(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;)Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iget v0, v0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    sub-int/2addr v0, v2

    invoke-interface {p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;->a(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iget v0, p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    sub-int/2addr v0, v2

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/i;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iget v0, p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->k:I

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;I)I

    :cond_0
    return-void
.end method
