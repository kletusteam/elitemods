.class Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;->a:Ljava/util/List;

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;->a:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    new-instance p2, Lcom/xiaomi/misettings/usagestats/c/b;

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;->b:Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p2, p3, v0}, Lcom/xiaomi/misettings/usagestats/c/b;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/c/c;->a()Landroid/view/View;

    move-result-object p3

    invoke-virtual {p3, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/xiaomi/misettings/usagestats/c/b;

    move-object v1, p3

    move-object p3, p2

    move-object p2, v1

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/BasePagerItemView$a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {p2, p1}, Lcom/xiaomi/misettings/usagestats/c/b;->a(Lcom/xiaomi/misettings/usagestats/c/b$a;)V

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/c/b;->d()V

    return-object p3
.end method
