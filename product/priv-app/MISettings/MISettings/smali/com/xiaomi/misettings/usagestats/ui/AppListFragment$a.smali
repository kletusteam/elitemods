.class Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;
.super Lcom/xiaomi/misettings/usagestats/c/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private c:Ljava/lang/String;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/c/c;-><init>(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/view/View;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v0, 0x7f0b01a9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;->d:Landroid/widget/ImageView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v0, 0x7f0b01aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;->e:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v0, 0x7f0b01ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;->f:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v0, 0x7f0b01ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;->g:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)V"
        }
    .end annotation

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;->c:Ljava/lang/String;

    return-void
.end method

.method protected b()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    const v1, 0x7f0e0144

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->j(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method
