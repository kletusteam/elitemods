.class Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;I)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->a:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto/16 :goto_2

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)Z

    move-result v1

    const v2, 0x7f08008a

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->b(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->c(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)Lb/c/a/a/b;

    move-result-object v2

    iget v2, v2, Lb/c/a/a/b;->d:I

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/misettings/common/utils/f;->d(Landroid/content/Context;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)Z

    move-result v1

    const v2, 0x7f080087

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->b(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_1
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->c(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)Lb/c/a/a/b;

    move-result-object v2

    iget v2, v2, Lb/c/a/a/b;->d:I

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/misettings/common/utils/f;->d(Landroid/content/Context;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    :goto_2
    return-void
.end method
