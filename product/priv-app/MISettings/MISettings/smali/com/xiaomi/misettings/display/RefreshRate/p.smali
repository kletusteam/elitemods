.class public Lcom/xiaomi/misettings/display/RefreshRate/p;
.super Landroidx/recyclerview/widget/RecyclerView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/display/RefreshRate/p$b;,
        Lcom/xiaomi/misettings/display/RefreshRate/p$a;,
        Lcom/xiaomi/misettings/display/RefreshRate/p$d;,
        Lcom/xiaomi/misettings/display/RefreshRate/p$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/xiaomi/misettings/display/RefreshRate/p$c;",
        ">;"
    }
.end annotation


# static fields
.field public static a:I = 0x1

.field public static b:I = 0x2

.field public static c:I = 0x3


# instance fields
.field private d:Landroid/content/Context;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/xiaomi/misettings/display/RefreshRate/k;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/xiaomi/misettings/display/RefreshRate/k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;",
            "Lcom/xiaomi/misettings/display/RefreshRate/k;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->g:Ljava/lang/String;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->d:Landroid/content/Context;

    iput-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->e:Ljava/util/List;

    iput-object p3, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->f:Lcom/xiaomi/misettings/display/RefreshRate/k;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/RefreshRate/p;)Lcom/xiaomi/misettings/display/RefreshRate/k;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->f:Lcom/xiaomi/misettings/display/RefreshRate/k;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/display/RefreshRate/p;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->d:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/display/RefreshRate/p$c;I)V
    .locals 1
    .param p1    # Lcom/xiaomi/misettings/display/RefreshRate/p$c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, p0, v0, p2}, Lcom/xiaomi/misettings/display/RefreshRate/p$c;->a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->e:Ljava/util/List;

    return-void
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->e:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget v0, v0, Lcom/xiaomi/misettings/display/RefreshRate/m;->d:I

    sget v1, Lcom/xiaomi/misettings/display/RefreshRate/p;->a:I

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget p1, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->d:I

    sget v0, Lcom/xiaomi/misettings/display/RefreshRate/p;->b:I

    if-ne p1, v0, :cond_1

    return v0

    :cond_1
    sget p1, Lcom/xiaomi/misettings/display/RefreshRate/p;->c:I

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/xiaomi/misettings/display/RefreshRate/p$c;

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/display/RefreshRate/p;->a(Lcom/xiaomi/misettings/display/RefreshRate/p$c;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/display/RefreshRate/p;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/display/RefreshRate/p$c;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/display/RefreshRate/p$c;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const-string p1, "HighRefreshItemAdapter"

    const-string v0, " here is onCreateViewHolder "

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget p1, Lcom/xiaomi/misettings/display/RefreshRate/p;->a:I

    const/4 v0, 0x0

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/xiaomi/misettings/display/RefreshRate/p$d;

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->d:Landroid/content/Context;

    sget v1, Lcom/xiaomi/misettings/display/k;->layout_title_item:I

    invoke-static {p2, v1, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/xiaomi/misettings/display/RefreshRate/p$d;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/p;Landroid/view/View;)V

    return-object p1

    :cond_0
    sget p1, Lcom/xiaomi/misettings/display/RefreshRate/p;->b:I

    if-ne p2, p1, :cond_1

    new-instance p1, Lcom/xiaomi/misettings/display/RefreshRate/p$a;

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->d:Landroid/content/Context;

    sget v1, Lcom/xiaomi/misettings/display/k;->layout_app_item:I

    invoke-static {p2, v1, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/xiaomi/misettings/display/RefreshRate/p$a;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/p;Landroid/view/View;)V

    return-object p1

    :cond_1
    new-instance p1, Lcom/xiaomi/misettings/display/RefreshRate/p$b;

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p;->d:Landroid/content/Context;

    sget v1, Lcom/xiaomi/misettings/display/k;->layout_follow_app_item:I

    invoke-static {p2, v1, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/xiaomi/misettings/display/RefreshRate/p$b;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/p;Landroid/view/View;)V

    return-object p1
.end method
