.class Lcom/xiaomi/misettings/usagestats/ui/i;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/view/f$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    check-cast p1, Lmiuix/view/f;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    iget-object p2, p2, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->g:Landroid/view/View;

    invoke-interface {p1, p2}, Lmiuix/view/f;->c(Landroid/view/View;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    iget-object p2, p2, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->f:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-interface {p1, p2}, Lmiuix/view/f;->a(Landroid/view/View;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-virtual {p2}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return v0

    :cond_0
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-virtual {p2}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a()Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lmiuix/appcompat/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-interface {p1, v1}, Lmiuix/view/f;->b(Landroid/view/View;)V

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->k()V

    :cond_1
    invoke-interface {p1}, Lmiuix/view/f;->a()Landroid/widget/EditText;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->b(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;)Landroid/text/TextWatcher;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-virtual {p1}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a(Z)V

    const/4 p1, 0x1

    return p1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 1

    check-cast p1, Lmiuix/view/f;

    invoke-interface {p1}, Lmiuix/view/f;->a()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->b(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;)Landroid/text/TextWatcher;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-virtual {p1}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-virtual {p1}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->c()Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    move-result-object p1

    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->c(Ljava/lang/String;)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-virtual {p1}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->b(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-virtual {p1}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->d()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/i;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-virtual {p1}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a(Z)V

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method
