.class Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;
.super Lcom/xiaomi/misettings/usagestats/d/b/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/usagestats/d/b/b<",
        "Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Landroidx/recyclerview/widget/RecyclerView$a;

.field private c:Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Lmiuix/slidingwidget/widget/SlidingButton;

.field private mPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/b/b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const p1, 0x7f0b01a9

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->d:Landroid/widget/ImageView;

    const p1, 0x7f0b01aa

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->e:Landroid/widget/TextView;

    const p1, 0x7f0b01a7

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->f:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/view/View;)V

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/utils/H;->d(Landroid/view/View;)V

    new-instance p1, Lcom/xiaomi/misettings/usagestats/devicelimit/n;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/n;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->f:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/o;

    invoke-direct {v0, p0, p2}, Lcom/xiaomi/misettings/usagestats/devicelimit/o;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;)Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->f:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object p0
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;I)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->b:Landroidx/recyclerview/widget/RecyclerView$a;

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->mPosition:I

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->f:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    iget-boolean p2, p2, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->c:Z

    invoke-virtual {p1, p2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->b:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    iget-object p2, p2, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->a:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    iput-object p1, p2, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->b:Ljava/lang/String;

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->d:Landroid/widget/ImageView;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    iget-object p3, p3, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->a:Ljava/lang/String;

    invoke-static {p2, p3}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->e:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    iget-object p2, p2, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->b:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->f:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->c:Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    iget-boolean p2, p2, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->d:Z

    invoke-virtual {p1, p2}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    return-void
.end method

.method public bridge synthetic a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V
    .locals 0

    check-cast p2, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;I)V

    return-void
.end method
