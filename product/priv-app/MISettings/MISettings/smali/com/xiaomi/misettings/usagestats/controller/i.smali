.class public Lcom/xiaomi/misettings/usagestats/controller/i;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/lang/String; = "LR-AppUsageController"

.field public static b:I = 0xf

.field public static c:I = 0x1


# direct methods
.method private static a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "limitTime"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v1

    const-string p0, "registerTime"

    invoke-virtual {v0, p0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string p0, "pkgName"

    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private static a(Landroid/content/Context;)V
    .locals 5

    const-string v0, "com.android.settings.usagestats.controller.AppLimitService"

    :try_start_0
    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.android.settings"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "removeAll"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/misettings/common/utils/j;->a()Landroid/os/Handler;

    move-result-object v0

    new-instance v2, Lcom/xiaomi/misettings/usagestats/controller/h;

    invoke-direct {v2, p0, v1}, Lcom/xiaomi/misettings/usagestats/controller/h;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    const-wide/16 v3, 0xc8

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    const-string v0, "ensureStopSettingsLimitService: settings limit service is not running"

    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :goto_0
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/app/usage/UsageStatsManager;Ljava/lang/String;I)V
    .locals 12

    invoke-static {p0, p2}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p2

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/L;->f(J)I

    move-result v0

    sget-object v1, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "interceptor: usageTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sub-int/2addr p3, v0

    :cond_0
    invoke-static {p0, p2, p3}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    sget v1, Lcom/xiaomi/misettings/usagestats/controller/i;->b:I

    const/4 v2, 0x0

    const/high16 v3, 0xc000000

    const/4 v4, 0x1

    if-gt p3, v1, :cond_1

    invoke-static {p0, p2}, Lcom/xiaomi/misettings/usagestats/controller/i;->c(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    shr-int/lit8 v6, v1, 0x1

    invoke-static {p0, v6, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v11

    new-array v7, v4, [Ljava/lang/String;

    aput-object p2, v7, v2

    sget v1, Lcom/xiaomi/misettings/usagestats/controller/i;->b:I

    sub-int v1, p3, v1

    mul-int/lit8 v1, v1, 0x3c

    int-to-long v8, v1

    sget-object v10, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v5, p1

    invoke-static/range {v5 .. v11}, Lcom/xiaomi/misettings/usagestats/delegate/UsageManagerDelegate;->registerAppUsageObserver(Landroid/app/usage/UsageStatsManager;I[Ljava/lang/String;JLjava/util/concurrent/TimeUnit;Landroid/app/PendingIntent;)V

    :goto_0
    sget v1, Lcom/xiaomi/misettings/usagestats/controller/i;->c:I

    if-le p3, v1, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    shr-int/lit8 v6, v1, 0x2

    const-string v1, "ensureForeGround"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v6, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v11

    new-array v7, v4, [Ljava/lang/String;

    aput-object p2, v7, v2

    sget p2, Lcom/xiaomi/misettings/usagestats/controller/i;->c:I

    sub-int/2addr p3, p2

    mul-int/lit8 p3, p3, 0x3c

    int-to-long v8, p3

    sget-object v10, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v5, p1

    invoke-static/range {v5 .. v11}, Lcom/xiaomi/misettings/usagestats/delegate/UsageManagerDelegate;->registerAppUsageObserver(Landroid/app/usage/UsageStatsManager;I[Ljava/lang/String;JLjava/util/concurrent/TimeUnit;Landroid/app/PendingIntent;)V

    :cond_2
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "usagestats"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/usage/UsageStatsManager;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    shr-int/lit8 p1, p1, 0x3

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/delegate/UsageManagerDelegate;->unregisterAppUsageObserver(Landroid/app/usage/UsageStatsManager;I)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/controller/i;->d(Landroid/content/Context;Ljava/lang/String;J)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;ZZ)V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ljava/lang/String;ZZ)V
    .locals 6

    const-class v0, Lcom/xiaomi/misettings/usagestats/controller/i;

    monitor-enter v0

    const/4 v1, 0x0

    if-nez p3, :cond_1

    if-nez p2, :cond_1

    :try_start_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->f(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-static {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->b(Landroid/content/Context;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :cond_1
    :try_start_1
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/g;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/utils/j;->g(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez p2, :cond_2

    if-eqz v2, :cond_2

    invoke-static {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->b(Landroid/content/Context;Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-void

    :cond_2
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    if-eqz v2, :cond_5

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/String;

    aput-object p1, v5, v1

    aput-object v5, v3, v1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const/4 v4, 0x0

    aput-object v4, v3, v1

    const/4 v1, 0x3

    aput-object v4, v3, v1

    const/4 v1, 0x4

    const-string v4, "!miui_Suspended!"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/delegate/PackageManagerDelegate;->setPackagesSuspended(Landroid/content/pm/PackageManager;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz p2, :cond_3

    :try_start_3
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/String;)V

    sget-object v1, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    const-string v2, "suspendApp: cancel process"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_4
    sget-object v2, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    const-string v3, "suspendApp: "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    :goto_0
    if-nez p3, :cond_4

    invoke-static {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/p;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_4
    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Suspended] pkg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", suspended="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p1, ", fromDeviceLimit="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    const-string p1, "[Suspended] failed since pm is null!"

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ljava/util/List;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    const-class v0, Lcom/xiaomi/misettings/usagestats/controller/i;

    monitor-enter v0

    if-eqz p1, :cond_4

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    const-string p1, "suspendApps: packageManager is null"

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :cond_1
    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    aput-object v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v3

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x1

    aput-object v5, v4, v6

    const/4 v5, 0x2

    const/4 v6, 0x0

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "!miui_Suspended!"

    aput-object v6, v4, v5

    invoke-static {v1, v4}, Lcom/xiaomi/misettings/usagestats/delegate/PackageManagerDelegate;->setPackagesSuspended(Landroid/content/pm/PackageManager;[Ljava/lang/Object;)V

    if-eqz p2, :cond_3

    array-length v1, v2

    :goto_1
    if-ge v3, v1, :cond_3

    aget-object v4, v2, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {p0, v4}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    :try_start_3
    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "suspendApps: suspended="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p2, ",appCount="

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0

    :cond_4
    :goto_2
    monitor-exit v0

    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "pkgName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "remove"

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 10

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v1, v0}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;ZZ)V

    const-wide/32 v2, 0x7fffffff

    cmp-long v2, p2, v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    const-string v2, "usagestats"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Landroid/app/usage/UsageStatsManager;

    if-eqz v3, :cond_1

    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "packageName"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    shr-int/lit8 v4, v4, 0x3

    const/high16 v5, 0xc000000

    invoke-static {p0, v4, v2, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    new-array v5, v0, [Ljava/lang/String;

    aput-object p1, v5, v1

    const-wide/16 p0, 0x3c

    mul-long v6, p2, p0

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static/range {v3 .. v9}, Lcom/xiaomi/misettings/usagestats/delegate/UsageManagerDelegate;->registerAppUsageObserver(Landroid/app/usage/UsageStatsManager;I[Ljava/lang/String;JLjava/util/concurrent/TimeUnit;Landroid/app/PendingIntent;)V

    :cond_1
    return-void
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "usagestats"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/usage/UsageStatsManager;

    if-eqz p0, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    shr-int/lit8 v1, v0, 0x2

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/delegate/UsageManagerDelegate;->unregisterAppUsageObserver(Landroid/app/usage/UsageStatsManager;I)V

    shr-int/lit8 v1, v0, 0x1

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/delegate/UsageManagerDelegate;->unregisterAppUsageObserver(Landroid/app/usage/UsageStatsManager;I)V

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/delegate/UsageManagerDelegate;->unregisterAppUsageObserver(Landroid/app/usage/UsageStatsManager;I)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[UNRegistered] pkg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    const-string p1, "Opps! unregister manager is null!"

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/controller/g;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/controller/g;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static d(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 10

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_3

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ensureRegisterStrategy [Registered] pkg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", limitTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "min"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_2

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;

    move-result-object v3

    if-eqz v0, :cond_3

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->a(Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;)I

    move-result v0

    int-to-long v4, v0

    cmp-long v0, v4, p2

    if-gez v0, :cond_3

    iget-object p2, v3, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->a:Ljava/lang/String;

    iget p3, v3, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->b:I

    iget-wide v0, v3, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->c:J

    invoke-static {p0, p2, p3, v0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->a(Landroid/content/Context;Ljava/lang/String;IJ)V

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/i;->b(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/i;->c(Landroid/content/Context;Ljava/lang/String;)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "ensureRegisterStrategy: device limit prolong this app is monitoring, pkgName="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/g;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->g(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0, p1, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void

    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    new-array v4, v1, [Ljava/lang/String;

    aput-object p1, v4, v2

    aput-object v4, v3, v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x2

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "!miui_Suspended!"

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Lcom/xiaomi/misettings/usagestats/delegate/PackageManagerDelegate;->setPackagesSuspended(Landroid/content/pm/PackageManager;[Ljava/lang/Object;)V

    invoke-static {p0, p1, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    const-string v0, "usagestats"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/app/usage/UsageStatsManager;

    if-eqz v3, :cond_5

    long-to-int v0, p2

    invoke-static {p0, v3, p1, v0}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Landroid/app/usage/UsageStatsManager;Ljava/lang/String;I)V

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/service/MainProcessService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v5, 0xc000000

    invoke-static {p0, v4, v0, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    new-array v5, v1, [Ljava/lang/String;

    aput-object p1, v5, v2

    const-wide/16 v0, 0x3c

    mul-long/2addr p2, v0

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide v6, p2

    invoke-static/range {v3 .. v9}, Lcom/xiaomi/misettings/usagestats/delegate/UsageManagerDelegate;->registerAppUsageObserver(Landroid/app/usage/UsageStatsManager;I[Ljava/lang/String;JLjava/util/concurrent/TimeUnit;Landroid/app/PendingIntent;)V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerApp:registerAppUsageObserver"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "----"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/p;->k(Landroid/content/Context;)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    const-string p1, "ensureRegisterStrategy: app limit has registered"

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_5
    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    const-string p1, "Opps! manager is null!"

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_6
    :goto_3
    sget-object p0, Lcom/xiaomi/misettings/usagestats/controller/i;->a:Ljava/lang/String;

    const-string p1, "Opps! The limited time should >= 1 minute"

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
