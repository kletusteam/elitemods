.class public Lcom/xiaomi/misettings/usagestats/d/a/a/v;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;
    }
.end annotation


# instance fields
.field public a:Landroid/view/View;

.field public b:Lcom/xiaomi/misettings/widget/CircleImageView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->a:Landroid/view/View;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/view/View;)V

    const v0, 0x7f0b01a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/widget/CircleImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->b:Lcom/xiaomi/misettings/widget/CircleImageView;

    const v0, 0x7f0b0213

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->c:Landroid/widget/TextView;

    const v0, 0x7f0b020c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    const v0, 0x7f0b0212

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->d:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->a:Landroid/view/View;

    return-object v0
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;)V
    .locals 2

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "renderViewHolderViewBean"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p1, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->b:Lcom/xiaomi/misettings/widget/CircleImageView;

    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->b:Lcom/xiaomi/misettings/widget/CircleImageView;

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/c/b/e;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->d:F

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->setProgress(F)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->c:Landroid/widget/TextView;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;F)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->d:Landroid/widget/TextView;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;F)V

    return-void
.end method
