.class public Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;
.super Landroid/widget/FrameLayout;


# instance fields
.field public a:I

.field private b:I

.field private c:Lb/c/a/a/b;

.field private d:Z

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Lcom/airbnb/lottie/LottieAnimationView;

.field private i:Landroid/animation/ValueAnimator;

.field private j:Landroid/graphics/Bitmap;

.field private k:Landroid/graphics/Bitmap;

.field private l:Landroid/graphics/BitmapFactory$Options;

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a:I

    new-instance p1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->l:Landroid/graphics/BitmapFactory$Options;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/c/u;->a()Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->f()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a:I

    new-instance p1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->l:Landroid/graphics/BitmapFactory$Options;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/c/u;->a()Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->f()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a:I

    new-instance p1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->l:Landroid/graphics/BitmapFactory$Options;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/c/u;->a()Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->f()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->b:I

    return p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->k:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/a;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/b;

    move-result-object p0

    invoke-interface {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/b;->b(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(F)V
    .locals 5

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->b:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->e:Landroid/widget/ImageView;

    sub-float/2addr v4, p1

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->f:Landroid/widget/ImageView;

    sub-float/2addr v4, p1

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->e:Landroid/widget/ImageView;

    sub-float/2addr v4, p1

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_3
    :goto_1
    return-void
.end method

.method private a(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->a()V

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->e()V

    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->f()V

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->g()V

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(F)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    return p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)Landroid/graphics/BitmapFactory$Options;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->l:Landroid/graphics/BitmapFactory$Options;

    return-object p0
.end method

.method public static b(Landroid/content/Context;I)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/a;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/b;

    move-result-object p0

    invoke-interface {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a/b;->a(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private b(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->c()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)Lb/c/a/a/b;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->c:Lb/c/a/a/b;

    return-object p0
.end method

.method private c(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->d()V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->l()V

    return-void
.end method

.method private f()V
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->b:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/f;->m(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->d:Z

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/f;->c(Landroid/content/Context;)Lb/c/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->c:Lb/c/a/a/b;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0081

    invoke-static {v0, v1, p0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0b01c3

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    const v0, 0x7f0b01c5

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->e:Landroid/widget/ImageView;

    const v0, 0x7f0b016d

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->f:Landroid/widget/ImageView;

    const v0, 0x7f0b01c8

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->g:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->l:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x2

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    return-void
.end method

.method private g()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x5dc

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/d;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/d;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/e;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/e;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private h()V
    .locals 3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto/16 :goto_6

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->k:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    goto :goto_1

    :cond_1
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    const v1, 0x7f08008a

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->l:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->c:Lb/c/a/a/b;

    iget v1, v1, Lb/c/a/a/b;->d:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/misettings/common/utils/f;->d(Landroid/content/Context;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_6

    :cond_3
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->k:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    goto :goto_3

    :cond_4
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    const v1, 0x7f080087

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->l:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->c:Lb/c/a/a/b;

    iget v1, v1, Lb/c/a/a/b;->d:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/misettings/common/utils/f;->d(Landroid/content/Context;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    :goto_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->f:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_6

    :cond_6
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080089

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->l:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->c:Lb/c/a/a/b;

    iget v1, v1, Lb/c/a/a/b;->d:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/misettings/common/utils/f;->d(Landroid/content/Context;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    goto :goto_5

    :cond_7
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800ad

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->l:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_4

    :cond_8
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080088

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_4
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->c:Lb/c/a/a/b;

    iget v1, v1, Lb/c/a/a/b;->d:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/misettings/common/utils/f;->d(Landroid/content/Context;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    :goto_5
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    if-nez v0, :cond_9

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a:I

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a:I

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->c(Lcom/airbnb/lottie/LottieAnimationView;)V

    :cond_9
    :goto_6
    return-void
.end method

.method private i()Z
    .locals 1

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/m;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private j()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->j:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->k:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->k:Landroid/graphics/Bitmap;

    :cond_1
    return-void
.end method

.method private k()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->g()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method private l()V
    .locals 6

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_5

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/16 v5, 0x8

    if-eq v0, v3, :cond_3

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    goto :goto_1

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->b:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iput-object v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->e:Landroid/widget/ImageView;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iput-object v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->f:Landroid/widget/ImageView;

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->e()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->m:Z

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->d()V

    :cond_4
    iput-object v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_6
    :goto_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(Lcom/airbnb/lottie/LottieAnimationView;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/c;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;I)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->b(Lcom/airbnb/lottie/LottieAnimationView;)V

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->c(Lcom/airbnb/lottie/LottieAnimationView;)V

    return-void
.end method

.method public d()V
    .locals 3

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->d()V

    return-void
.end method

.method public e()V
    .locals 3

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->d()V

    return-void
.end method

.method public setCurrentLevel(I)V
    .locals 1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->h()V

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a:I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->k()V

    :cond_0
    return-void
.end method
