.class final Lcom/xiaomi/misettings/usagestats/statutoryholidays/f;
.super Ljava/lang/Object;

# interfaces
.implements Lb/c/b/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->f(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/c/b/c/h<",
        "Lcom/xiaomi/misettings/usagestats/statutoryholidays/b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/f;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const-string v0, "StatutoryHolidayUtils"

    const-string v1, "requestHolidayData() request failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/statutoryholidays/b;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/b;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/f;->a(Lcom/xiaomi/misettings/usagestats/statutoryholidays/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    if-eqz p1, :cond_0

    new-instance v0, Lb/a/b/q;

    invoke-direct {v0}, Lb/a/b/q;-><init>()V

    const-class v1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/b;

    invoke-virtual {v0, p1, v1}, Lb/a/b/q;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/b;

    iget v2, v1, Lb/c/b/a/a;->code:I

    if-nez v2, :cond_0

    const-string v2, "StatutoryHolidayUtils"

    const-string v3, "requestHolidayData() request succeeded"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "request holiday data result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, v1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/b;->a:Ljava/lang/String;

    const-class v2, Lcom/xiaomi/misettings/usagestats/statutoryholidays/d;

    invoke-virtual {v0, p1, v2}, Lb/a/b/q;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/d;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/f;->a:Landroid/content/Context;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/statutoryholidays/b;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/f;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/d;->b()I

    move-result v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/g;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/f;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/c;->a(Lcom/xiaomi/misettings/usagestats/statutoryholidays/d;)V

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/f;->a()V

    return-void
.end method
