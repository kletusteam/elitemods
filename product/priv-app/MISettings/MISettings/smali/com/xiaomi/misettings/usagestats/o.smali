.class Lcom/xiaomi/misettings/usagestats/o;
.super Landroid/os/CountDownTimer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->a(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;JJ)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/o;->a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/o;->a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->a(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;Z)V

    return-void
.end method

.method public onTick(J)V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/o;->a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->a(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/L;->b(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/o;->a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;

    const-wide/16 v1, 0x3e8

    div-long/2addr p1, v1

    long-to-int p1, p1

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->a(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;I)I

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result p1

    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    if-eqz p1, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "mRemainTime:"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/o;->a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->b(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "CompulsoryRestActivity"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRestTime:"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/o;->a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->c(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRemainTime pgr:"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/o;->a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->b(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, v0

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/o;->a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;

    invoke-static {v4}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->c(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x3c

    int-to-double v4, v4

    div-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/o;->a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->d(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/o;->a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->b(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)I

    move-result p2

    int-to-double v2, p2

    mul-double/2addr v2, v0

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/o;->a:Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;->c(Lcom/xiaomi/misettings/usagestats/CompulsoryRestActivity;)I

    move-result p2

    mul-int/lit8 p2, p2, 0x3c

    int-to-double v0, p2

    div-double/2addr v2, v0

    double-to-int p2, v2

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;->setProgress(I)V

    return-void
.end method
