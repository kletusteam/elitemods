.class public Lcom/xiaomi/misettings/usagestats/service/g;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/service/g$c;,
        Lcom/xiaomi/misettings/usagestats/service/g$b;,
        Lcom/xiaomi/misettings/usagestats/service/g$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/service/g$c;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Timer;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/g;->b:Ljava/util/Timer;

    return-void
.end method

.method synthetic constructor <init>(Lcom/xiaomi/misettings/usagestats/service/f;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/service/g;-><init>()V

    return-void
.end method

.method private declared-synchronized a(Lcom/xiaomi/misettings/usagestats/service/g$c;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/g;->b:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/g;->b:Ljava/util/Timer;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/service/f;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/service/f;-><init>(Lcom/xiaomi/misettings/usagestats/service/g;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/service/g;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/service/g;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/service/g;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/service/g;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/service/g;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/service/g$c;

    iget-object v2, v2, Lcom/xiaomi/misettings/usagestats/service/g$c;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/service/g;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/service/g$c;

    iget-object v2, v2, Lcom/xiaomi/misettings/usagestats/service/g$c;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/service/g$c;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/service/g$c;->a(Lcom/xiaomi/misettings/usagestats/service/g$c;)I

    move-result v2

    if-ne v2, p2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Z)V
    .locals 9

    const-string v0, "LimitServiceNotifyHelper"

    const-string v1, "handleWapperInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/service/g;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/service/g$c;

    if-eqz p1, :cond_1

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/service/g;->b(Lcom/xiaomi/misettings/usagestats/service/g$c;)V

    return-void

    :cond_1
    iget-object p1, v2, Lcom/xiaomi/misettings/usagestats/service/g$c;->a:Ljava/lang/String;

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_3

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/xiaomi/misettings/usagestats/service/g$c;

    iget-object v7, v6, Lcom/xiaomi/misettings/usagestats/service/g$c;->a:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    add-int/lit8 v5, v5, 0x1

    iget v7, v6, Lcom/xiaomi/misettings/usagestats/service/g$c;->b:I

    iget v8, v2, Lcom/xiaomi/misettings/usagestats/service/g$c;->b:I

    if-ge v7, v8, :cond_2

    move-object v2, v6

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "runWrapper type"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/service/g$c;->a(Lcom/xiaomi/misettings/usagestats/service/g$c;)I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " count:"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-le v5, v3, :cond_4

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "runWrapper iNotify"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/service/g$c;->b(Lcom/xiaomi/misettings/usagestats/service/g$c;)Lcom/xiaomi/misettings/usagestats/service/g$b;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/service/g;->b(Lcom/xiaomi/misettings/usagestats/service/g$c;)V

    :cond_4
    return-void
.end method

.method public static b()Lcom/xiaomi/misettings/usagestats/service/g;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/service/g$a;->a:Lcom/xiaomi/misettings/usagestats/service/g;

    return-object v0
.end method

.method private b(Lcom/xiaomi/misettings/usagestats/service/g$c;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "runWrapper call"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/service/g$c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LimitServiceNotifyHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/service/g$c;->b(Lcom/xiaomi/misettings/usagestats/service/g$c;)Lcom/xiaomi/misettings/usagestats/service/g$b;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/service/g$c;->b(Lcom/xiaomi/misettings/usagestats/service/g$c;)Lcom/xiaomi/misettings/usagestats/service/g$b;

    move-result-object v0

    invoke-interface {v0}, Lcom/xiaomi/misettings/usagestats/service/g$b;->call()V

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/service/g$c;->a:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/service/g;->a(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/g;->b:Ljava/util/Timer;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/Timer;->cancel()V

    new-instance p1, Ljava/util/Timer;

    invoke-direct {p1}, Ljava/util/Timer;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/g;->b:Ljava/util/Timer;

    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/service/g;->a(Z)V

    return-void
.end method

.method public a(Ljava/lang/String;ILcom/xiaomi/misettings/usagestats/service/g$b;)V
    .locals 2

    const-string v0, "com.miui.home"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "addAppLimit"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " remind:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LimitServiceNotifyHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/service/g;->a(Ljava/lang/String;I)V

    invoke-static {p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/service/g$c;->a(Ljava/lang/String;ILcom/xiaomi/misettings/usagestats/service/g$b;)Lcom/xiaomi/misettings/usagestats/service/g$c;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/service/g;->a(Lcom/xiaomi/misettings/usagestats/service/g$c;)V

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_2

    invoke-interface {p3}, Lcom/xiaomi/misettings/usagestats/service/g$b;->call()V

    :cond_2
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ILcom/xiaomi/misettings/usagestats/service/g$b;)V
    .locals 1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "addCategory"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " remind:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "LimitServiceNotifyHelper"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string p1, "com.miui.home"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object p1

    const-class v0, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/p;->j(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    invoke-direct {p0, p2, p1}, Lcom/xiaomi/misettings/usagestats/service/g;->a(Ljava/lang/String;I)V

    invoke-static {p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/service/g$c;->b(Ljava/lang/String;ILcom/xiaomi/misettings/usagestats/service/g$b;)Lcom/xiaomi/misettings/usagestats/service/g$c;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/service/g;->a(Lcom/xiaomi/misettings/usagestats/service/g$c;)V

    goto :goto_1

    :cond_2
    :goto_0
    if-eqz p4, :cond_3

    invoke-interface {p4}, Lcom/xiaomi/misettings/usagestats/service/g$b;->call()V

    :cond_3
    :goto_1
    return-void
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/service/g$c;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/g;->a:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/g;->a:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/g;->a:Ljava/util/List;

    return-object v0
.end method
