.class Lcom/xiaomi/misettings/usagestats/utils/Z;
.super Ljava/lang/Object;

# interfaces
.implements Lb/c/b/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;->n()Landroidx/work/ListenableWorker$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/c/b/c/h<",
        "Lb/c/b/a/a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/Z;->a:Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const-string v0, "UploadWorker"

    const-string v1, "onFail"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/Z;->a:Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;

    invoke-virtual {v0}, Landroidx/work/ListenableWorker;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/a/q;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/q;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "request fail"

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/a/q;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public a(Lb/c/b/a/a;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lb/c/b/a/a;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/Z;->a(Lb/c/b/a/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSuccess : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UploadWorker"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-class v0, Lb/c/b/a/a;

    invoke-static {p1, v0}, Lb/e/a/b/c;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lb/c/b/a/a;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/Z;->a:Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;

    invoke-virtual {p1}, Landroidx/work/ListenableWorker;->a()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/a/q;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/q;

    move-result-object p1

    const-string v1, "no net result"

    invoke-virtual {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/a/q;->a(ZLjava/lang/String;)V

    return-void

    :cond_0
    iget v1, p1, Lb/c/b/a/a;->code:I

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/Z;->a:Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;

    invoke-virtual {v1}, Landroidx/work/ListenableWorker;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/a/q;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/q;

    move-result-object v1

    iget-object p1, p1, Lb/c/b/a/a;->description:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/xiaomi/misettings/usagestats/a/q;->a(ZLjava/lang/String;)V

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/Z;->a:Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/Z;->a:Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;

    invoke-virtual {v0}, Landroidx/work/ListenableWorker;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;->a(Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    return-void
.end method
