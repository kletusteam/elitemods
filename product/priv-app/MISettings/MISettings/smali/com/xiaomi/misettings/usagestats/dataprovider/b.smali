.class Lcom/xiaomi/misettings/usagestats/dataprovider/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Landroid/content/res/Resources;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/res/Resources;

.field final synthetic b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;Landroid/content/res/Resources;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->a:Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const-string v0, "ScreenTimeContentProvider"

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Landroid/content/Context;

    move-result-object v1

    sget-object v3, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-static {v1, v3}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v3, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v4, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-direct {v3, v4}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendLoadDataMessage()--run() e : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    :goto_0
    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "total time is :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-virtual {v5}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->a:Landroid/content/res/Resources;

    invoke-static {v5, v6, v3, v4, v7}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;Landroid/content/Context;JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v6}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v6, v1, v7, v7}, Lcom/xiaomi/misettings/usagestats/h/a;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;ZZ)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v8}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->a(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v1, v2, v7}, Lcom/xiaomi/misettings/usagestats/h/a;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;ZZ)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->b(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Lcom/misettings/common/utils/p;

    move-result-object v2

    const-string v7, "total_usage_time"

    invoke-virtual {v2, v7, v3, v4}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;J)V

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const-string v3, "run: "

    const-string v4, ""

    const-string v7, "category_usage_state"

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->b(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Lcom/misettings/common/utils/p;

    move-result-object v2

    invoke-virtual {v2, v7, v6}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->b(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Lcom/misettings/common/utils/p;

    move-result-object v2

    invoke-virtual {v2, v7, v4}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const-string v6, "app_usage_state"

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->b(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Lcom/misettings/common/utils/p;

    move-result-object v2

    invoke-virtual {v2, v6, v1}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->b(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Lcom/misettings/common/utils/p;

    move-result-object v1

    invoke-virtual {v1, v6, v4}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->b(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Lcom/misettings/common/utils/p;

    move-result-object v1

    const-string v2, "last_total_hours"

    invoke-virtual {v1, v2, v5}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->b(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Lcom/misettings/common/utils/p;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->c(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "unit"

    invoke-virtual {v1, v3, v2}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->e(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->d(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    const-string v1, "notify to get data"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/dataprovider/b;->b:Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;->f(Lcom/xiaomi/misettings/usagestats/dataprovider/ScreenTimeContentProvider;)V

    return-void
.end method
