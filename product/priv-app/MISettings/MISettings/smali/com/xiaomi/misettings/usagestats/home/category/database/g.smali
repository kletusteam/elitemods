.class Lcom/xiaomi/misettings/usagestats/home/category/database/g;
.super Landroidx/room/x$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;->a(Landroidx/room/a;)La/p/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;I)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/g;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;

    invoke-direct {p0, p2}, Landroidx/room/x$a;-><init>(I)V

    return-void
.end method


# virtual methods
.method public a(La/p/a/b;)V
    .locals 1

    const-string v0, "CREATE TABLE IF NOT EXISTS `users_category` (`id` INTEGER NOT NULL, `packageName` TEXT NOT NULL, `categoryId` TEXT, PRIMARY KEY(`id`, `packageName`))"

    invoke-interface {p1, v0}, La/p/a/b;->b(Ljava/lang/String;)V

    const-string v0, "CREATE UNIQUE INDEX `index_users_category_packageName` ON `users_category` (`packageName`)"

    invoke-interface {p1, v0}, La/p/a/b;->b(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)"

    invoke-interface {p1, v0}, La/p/a/b;->b(Ljava/lang/String;)V

    const-string v0, "INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"8d0eabd50b2ec0a0563a8d4527e587f6\")"

    invoke-interface {p1, v0}, La/p/a/b;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b(La/p/a/b;)V
    .locals 1

    const-string v0, "DROP TABLE IF EXISTS `users_category`"

    invoke-interface {p1, v0}, La/p/a/b;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected c(La/p/a/b;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/g;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;->a(Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/g;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;->b(Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/g;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;->c(Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/room/v$b;

    invoke-virtual {v2, p1}, Landroidx/room/v$b;->a(La/p/a/b;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public d(La/p/a/b;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/g;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;->a(Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;La/p/a/b;)La/p/a/b;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/g;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;->b(Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;La/p/a/b;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/g;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;->d(Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/g;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;->e(Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/database/g;->b:Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;->f(Lcom/xiaomi/misettings/usagestats/home/category/database/ClassifyManagerDatabase_Impl;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/room/v$b;

    invoke-virtual {v2, p1}, Landroidx/room/v$b;->c(La/p/a/b;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected h(La/p/a/b;)V
    .locals 7

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    new-instance v1, Landroidx/room/b/f$a;

    const-string v2, "id"

    const/4 v3, 0x1

    const-string v4, "INTEGER"

    invoke-direct {v1, v2, v4, v3, v3}, Landroidx/room/b/f$a;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Landroidx/room/b/f$a;

    const-string v2, "TEXT"

    const-string v4, "packageName"

    const/4 v5, 0x2

    invoke-direct {v1, v4, v2, v3, v5}, Landroidx/room/b/f$a;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Landroidx/room/b/f$a;

    const-string v5, "categoryId"

    const/4 v6, 0x0

    invoke-direct {v1, v5, v2, v6, v6}, Landroidx/room/b/f$a;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    invoke-virtual {v0, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v6}, Ljava/util/HashSet;-><init>(I)V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(I)V

    new-instance v5, Landroidx/room/b/f$d;

    filled-new-array {v4}, [Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    const-string v6, "index_users_category_packageName"

    invoke-direct {v5, v6, v3, v4}, Landroidx/room/b/f$d;-><init>(Ljava/lang/String;ZLjava/util/List;)V

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v3, Landroidx/room/b/f;

    const-string v4, "users_category"

    invoke-direct {v3, v4, v0, v1, v2}, Landroidx/room/b/f;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;)V

    invoke-static {p1, v4}, Landroidx/room/b/f;->a(La/p/a/b;Ljava/lang/String;)Landroidx/room/b/f;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroidx/room/b/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Migration didn\'t properly handle users_category(com.xiaomi.misettings.usagestats.home.category.database.CategoryEntity).\n Expected:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, "\n Found:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
