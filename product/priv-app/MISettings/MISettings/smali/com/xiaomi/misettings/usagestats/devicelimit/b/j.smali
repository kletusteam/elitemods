.class public Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;
.super Lcom/xiaomi/misettings/usagestats/c/c;

# interfaces
.implements Lmiuix/appcompat/app/w$a;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Ljava/util/Observer;


# instance fields
.field private c:Lmiuix/slidingwidget/widget/SlidingButton;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Lmiuix/miuixbasewidget/widget/MessageView;

.field private m:Landroid/view/View;

.field private n:Lmiuix/appcompat/app/w;

.field private o:Lcom/xiaomi/misettings/usagestats/devicelimit/k;

.field private p:I

.field private q:I

.field private r:Z

.field private s:Z

.field private t:Lcom/xiaomi/misettings/usagestats/f/g;

.field private u:Ljava/lang/Object;

.field private v:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/devicelimit/k;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/c/c;-><init>(Landroid/content/Context;)V

    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->u:Ljava/lang/Object;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->v:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->i()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/J;->a()Lcom/xiaomi/misettings/usagestats/utils/J;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/xiaomi/misettings/usagestats/utils/J;->a(Ljava/util/Observer;)V

    :cond_0
    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->o:Lcom/xiaomi/misettings/usagestats/devicelimit/k;

    return-void
.end method

.method private a(II)Ljava/lang/String;
    .locals 1

    mul-int/lit8 p1, p1, 0x2

    const/16 v0, 0x1e

    if-lt p2, v0, :cond_0

    add-int/lit8 p1, p1, 0x1

    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;)Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->c:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object p0
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    invoke-virtual {p1, v0, v1, p2}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;->a(Landroid/content/Context;IZ)Z

    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 7

    const v0, 0x7f0b030b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->c:Lmiuix/slidingwidget/widget/SlidingButton;

    const v0, 0x7f0b039e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->f:Landroid/widget/TextView;

    const v0, 0x7f0b0392

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->g:Landroid/widget/TextView;

    const v0, 0x7f0b0263

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->d:Landroid/view/View;

    const v0, 0x7f0b025c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->e:Landroid/view/View;

    const v0, 0x7f0b0393

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->j:Landroid/widget/TextView;

    const v0, 0x7f0b039f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->i:Landroid/widget/TextView;

    const v0, 0x7f0b0260

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->h:Landroid/view/View;

    const v0, 0x7f0b01cc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->k:Landroid/widget/TextView;

    const v0, 0x7f0b01cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->m:Landroid/view/View;

    const v0, 0x7f0b0219

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/miuixbasewidget/widget/MessageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->l:Lmiuix/miuixbasewidget/widget/MessageView;

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1c

    if-lt p1, v0, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->m:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->m:Landroid/view/View;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/f;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/f;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->m:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    new-instance p1, Lmiuix/appcompat/app/w;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p1

    move-object v3, p0

    invoke-direct/range {v1 .. v6}, Lmiuix/appcompat/app/w;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/w$a;IIZ)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->n:Lmiuix/appcompat/app/w;

    return-void
.end method

.method private a(Landroid/widget/TextView;I)V
    .locals 6

    if-nez p2, :cond_0

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    div-int/lit8 v1, p2, 0x3c

    rem-int/lit8 p2, p2, 0x3c

    const/4 v2, 0x1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->e()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f11002b

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-virtual {v1, v3, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->e()Landroid/content/res/Resources;

    move-result-object p2

    const v3, 0x7f110028

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-virtual {p2, v3, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->e()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f130401

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private a(Lmiuix/miuixbasewidget/widget/MessageView;)V
    .locals 6

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_1

    return-void

    :cond_1
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_4

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v4, v3, Landroid/widget/ImageView;

    if-eqz v4, :cond_2

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    instance-of v4, v3, Landroid/widget/TextView;

    if-eqz v4, :cond_3

    move-object v4, v3

    check-cast v4, Landroid/widget/TextView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setGravity(I)V

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v5, 0x11

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    iput v1, v4, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "DeviceLimitTimeSetHolder"

    const-string v1, "resetMessageView: "

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->r:Z

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->s:Z

    return p1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->q:I

    return p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;)Lmiuix/appcompat/app/w;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->n:Lmiuix/appcompat/app/w;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    return p0
.end method

.method private e()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 8

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/b/b;->d(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/l;->a()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->t:Lcom/xiaomi/misettings/usagestats/f/g;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->t:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const-wide/16 v3, 0x0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v5

    add-long/2addr v3, v5

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    const v1, 0x7f13036a

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget v6, Lcom/xiaomi/misettings/usagestats/utils/L;->d:I

    int-to-long v6, v6

    div-long/2addr v3, v6

    invoke-static {v0, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->l:Lmiuix/miuixbasewidget/widget/MessageView;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->l:Lmiuix/miuixbasewidget/widget/MessageView;

    invoke-virtual {v1, v0}, Lmiuix/miuixbasewidget/widget/MessageView;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->s:Z

    return p0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->q:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    if-nez v0, :cond_0

    const/16 v0, 0x12c

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->q:I

    if-nez v0, :cond_1

    const/16 v0, 0x1e0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->q:I

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->g:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->a(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->f:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->q:I

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->a(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->s:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->j()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->c:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->s:Z

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->c:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0, p0}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->l:Lmiuix/miuixbasewidget/widget/MessageView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->a(Lmiuix/miuixbasewidget/widget/MessageView;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->f()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->l:Lmiuix/miuixbasewidget/widget/MessageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->d:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/devicelimit/b/g;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/g;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->e:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/devicelimit/b/h;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/h;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->h:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/devicelimit/b/i;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/i;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private i()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private j()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->g:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->s:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->f:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->s:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->j:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->s:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->i:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->s:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->k:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->s:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method private k()V
    .locals 0

    return-void
.end method


# virtual methods
.method public a(Lmiuix/pickerwidget/widget/TimePicker;II)V
    .locals 6

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    const p2, 0x7f130412

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    mul-int/lit8 v1, p2, 0x3c

    add-int/2addr v1, p3

    invoke-direct {p0, p2, p3}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->a(II)Ljava/lang/String;

    iget-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->r:Z

    const-wide/16 v2, 0x0

    const/4 p3, 0x1

    if-eqz p2, :cond_3

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    if-ge p2, v1, :cond_1

    move p2, p3

    goto :goto_0

    :cond_1
    move p2, v0

    :goto_0
    iget v4, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    if-ne v4, v1, :cond_2

    move v4, p3

    goto :goto_1

    :cond_2
    move v4, v0

    :goto_1
    iput v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    invoke-static {p1, v1, p3}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;IZ)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->g:Landroid/widget/TextView;

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    invoke-direct {p0, v1, v5}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->a(Landroid/widget/TextView;I)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {p1, v2, v3}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;J)V

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;I)V

    invoke-direct {p0, p1, p3}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->a(Landroid/content/Context;Z)V

    goto :goto_4

    :cond_3
    iget p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->q:I

    if-ge p2, v1, :cond_4

    move p2, p3

    goto :goto_2

    :cond_4
    move p2, v0

    :goto_2
    iget v4, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->q:I

    if-ne v4, v1, :cond_5

    move v4, p3

    goto :goto_3

    :cond_5
    move v4, v0

    :goto_3
    iput v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->q:I

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->q:I

    invoke-static {p1, p3, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;IZ)V

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->f:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->q:I

    invoke-direct {p0, p3, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->a(Landroid/widget/TextView;I)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result p3

    if-nez p3, :cond_6

    invoke-static {p1, v2, v3}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;J)V

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;I)V

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->a(Landroid/content/Context;Z)V

    :cond_6
    :goto_4
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result p3

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->r:Z

    if-ne p3, v0, :cond_8

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->i()Z

    move-result p3

    if-eqz p3, :cond_7

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;)Z

    move-result p3

    if-eqz p3, :cond_7

    if-nez p2, :cond_7

    if-eqz v4, :cond_7

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;

    move-result-object p3

    invoke-virtual {p3, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->c(Landroid/content/Context;)V

    :cond_7
    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/controller/l;->f(Landroid/content/Context;Z)V

    :cond_8
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/J;->a()Lcom/xiaomi/misettings/usagestats/utils/J;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->u:Ljava/lang/Object;

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/J;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->k()V

    return-void
.end method

.method public a(Z)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    const/16 v2, 0x12c

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->p:I

    invoke-static {v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;IZ)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->q:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;IZ)V

    :cond_0
    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;J)V

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->e(Landroid/content/Context;Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;->a(Landroid/content/Context;Z)Z

    if-eqz p1, :cond_1

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->l(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->m(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;J)V

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->j()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->o:Lcom/xiaomi/misettings/usagestats/devicelimit/k;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/k;->a(Z)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/J;->a()Lcom/xiaomi/misettings/usagestats/utils/J;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->u:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/J;->a(Ljava/lang/Object;)V

    return-void
.end method

.method protected b()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    const v1, 0x7f0e0143

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/J;->a()Lcom/xiaomi/misettings/usagestats/utils/J;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/utils/J;->b(Ljava/util/Observer;)V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->v:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->a(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->g()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->h()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->v:Z

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->s:Z

    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->a(Z)V

    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0

    instance-of p1, p2, Lcom/xiaomi/misettings/usagestats/f/g;

    if-eqz p1, :cond_0

    check-cast p2, Lcom/xiaomi/misettings/usagestats/f/g;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->t:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/j;->f()V

    :cond_0
    return-void
.end method
