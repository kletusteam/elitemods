.class public abstract Lcom/xiaomi/misettings/usagestats/home/category/k$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/home/category/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation


# static fields
.field private static final a:Landroid/view/animation/Interpolator;

.field private static final b:Landroid/view/animation/Interpolator;


# instance fields
.field private c:I

.field public d:Z

.field public e:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/i;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/home/category/i;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a:Landroid/view/animation/Interpolator;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/j;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/home/category/j;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->c:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->d:Z

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->e:Landroid/graphics/Point;

    return-void
.end method

.method private a(Landroidx/recyclerview/widget/RecyclerView;)I
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0701c5

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->c:I

    :cond_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->c:I

    return p1
.end method

.method public static b(II)I
    .locals 3

    const v0, 0xc0c0c

    and-int v1, p0, v0

    if-nez v1, :cond_0

    return p0

    :cond_0
    not-int v2, v1

    and-int/2addr p0, v2

    if-nez p1, :cond_1

    shl-int/lit8 p1, v1, 0x2

    :goto_0
    or-int/2addr p0, p1

    return p0

    :cond_1
    shl-int/lit8 p1, v1, 0x1

    const v1, -0xc0c0d

    and-int/2addr v1, p1

    or-int/2addr p0, v1

    and-int/2addr p1, v0

    shl-int/lit8 p1, p1, 0x2

    goto :goto_0
.end method

.method public static c(II)I
    .locals 0

    mul-int/lit8 p0, p0, 0x8

    shl-int p0, p1, p0

    return p0
.end method

.method public static d(II)I
    .locals 2

    or-int v0, p1, p0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->c(II)I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v1, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->c(II)I

    move-result p1

    or-int/2addr p1, v0

    const/4 v0, 0x2

    invoke-static {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->c(II)I

    move-result p0

    or-int/2addr p0, p1

    return p0
.end method


# virtual methods
.method public a(F)F
    .locals 0

    return p1
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$t;)F
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/high16 p1, 0x3f000000    # 0.5f

    return p1
.end method

.method public a()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(II)I
    .locals 3

    const v0, 0x303030

    and-int v1, p1, v0

    if-nez v1, :cond_0

    return p1

    :cond_0
    not-int v2, v1

    and-int/2addr p1, v2

    if-nez p2, :cond_1

    shr-int/lit8 p2, v1, 0x2

    :goto_0
    or-int/2addr p1, p2

    return p1

    :cond_1
    shr-int/lit8 p2, v1, 0x1

    const v1, -0x303031

    and-int/2addr v1, p2

    or-int/2addr p1, v1

    and-int/2addr p2, v0

    shr-int/lit8 p2, p2, 0x2

    goto :goto_0
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;IIIJ)I
    .locals 4
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView;)I

    move-result p1

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result p4

    int-to-float v0, p3

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    float-to-int v0, v0

    int-to-float p4, p4

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr p4, v1

    int-to-float p2, p2

    div-float/2addr p4, p2

    invoke-static {v1, p4}, Ljava/lang/Math;->min(FF)F

    move-result p2

    mul-int/2addr v0, p1

    int-to-float p1, v0

    sget-object p4, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b:Landroid/view/animation/Interpolator;

    invoke-interface {p4, p2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p2

    mul-float/2addr p1, p2

    float-to-int p1, p1

    const-wide/16 v2, 0x7d0

    cmp-long p2, p5, v2

    if-lez p2, :cond_0

    goto :goto_0

    :cond_0
    long-to-float p2, p5

    const/high16 p4, 0x44fa0000    # 2000.0f

    div-float v1, p2, p4

    :goto_0
    int-to-float p1, p1

    sget-object p2, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a:Landroid/view/animation/Interpolator;

    invoke-interface {p2, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p2

    mul-float/2addr p1, p2

    float-to-int p1, p1

    if-nez p1, :cond_2

    if-lez p3, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, -0x1

    :cond_2
    :goto_1
    return p1
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;IFF)J
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    move-result-object p1

    const/16 p3, 0x8

    if-nez p1, :cond_1

    if-ne p2, p3, :cond_0

    const/16 p1, 0xc8

    goto :goto_0

    :cond_0
    const/16 p1, 0xfa

    :goto_0
    int-to-long p1, p1

    goto :goto_1

    :cond_1
    if-ne p2, p3, :cond_2

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;->e()J

    move-result-wide p1

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;->f()J

    move-result-wide p1

    :goto_1
    return-wide p1
.end method

.method public abstract a(Landroidx/recyclerview/widget/RecyclerView$t;Ljava/util/List;II)Landroidx/recyclerview/widget/RecyclerView$t;
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView$t;",
            "Ljava/util/List<",
            "Landroidx/recyclerview/widget/RecyclerView$t;",
            ">;II)",
            "Landroidx/recyclerview/widget/RecyclerView$t;"
        }
    .end annotation
.end method

.method public a(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;FFIZ)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lcom/xiaomi/misettings/usagestats/home/category/r;->a:Landroidx/recyclerview/widget/v;

    iget-object v3, p3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Landroidx/recyclerview/widget/v;->b(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;FFIZ)V

    return-void
.end method

.method a(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;Ljava/util/List;IFF)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Landroidx/recyclerview/widget/RecyclerView$t;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/home/category/k$c;",
            ">;IFF)V"
        }
    .end annotation

    goto/32 :goto_20

    nop

    :goto_0
    move-object/from16 v3, p3

    goto/32 :goto_11

    nop

    :goto_1
    move/from16 v6, p5

    goto/32 :goto_8

    nop

    :goto_2
    move-object/from16 v2, p2

    goto/32 :goto_0

    nop

    :goto_3
    move-object v0, p0

    goto/32 :goto_17

    nop

    :goto_4
    move-object v1, p1

    goto/32 :goto_18

    nop

    :goto_5
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v9

    goto/32 :goto_15

    nop

    :goto_6
    move-object/from16 v11, p4

    goto/32 :goto_16

    nop

    :goto_7
    if-lt v10, v9, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_6

    nop

    :goto_8
    invoke-virtual/range {v0 .. v7}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;FFIZ)V

    goto/32 :goto_12

    nop

    :goto_9
    goto :goto_d

    :goto_a
    goto/32 :goto_1b

    nop

    :goto_b
    return-void

    :goto_c
    move v10, v0

    :goto_d
    goto/32 :goto_7

    nop

    :goto_e
    const/4 v7, 0x0

    goto/32 :goto_2a

    nop

    :goto_f
    invoke-virtual/range {v0 .. v7}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;FFIZ)V

    :goto_10
    goto/32 :goto_19

    nop

    :goto_11
    move/from16 v4, p6

    goto/32 :goto_24

    nop

    :goto_12
    invoke-virtual {p1, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :goto_13
    goto/32 :goto_b

    nop

    :goto_14
    check-cast v0, Lcom/xiaomi/misettings/usagestats/home/category/k$c;

    goto/32 :goto_1a

    nop

    :goto_15
    const/4 v0, 0x0

    goto/32 :goto_c

    nop

    :goto_16
    invoke-interface {v11, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_17
    move-object v1, p1

    goto/32 :goto_2

    nop

    :goto_18
    move-object/from16 v2, p2

    goto/32 :goto_f

    nop

    :goto_19
    invoke-virtual {p1, v12}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/32 :goto_27

    nop

    :goto_1a
    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->d()V

    goto/32 :goto_1e

    nop

    :goto_1b
    move-object v13, p0

    goto/32 :goto_23

    nop

    :goto_1c
    if-eqz v1, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_29

    nop

    :goto_1d
    iget v5, v0, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->k:F

    goto/32 :goto_25

    nop

    :goto_1e
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v12

    goto/32 :goto_1f

    nop

    :goto_1f
    move-object v13, p0

    goto/32 :goto_21

    nop

    :goto_20
    move-object v8, p1

    goto/32 :goto_5

    nop

    :goto_21
    iget-boolean v1, v13, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->d:Z

    goto/32 :goto_1c

    nop

    :goto_22
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v9

    goto/32 :goto_26

    nop

    :goto_23
    if-nez p3, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_22

    nop

    :goto_24
    move/from16 v5, p7

    goto/32 :goto_1

    nop

    :goto_25
    iget v6, v0, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->f:I

    goto/32 :goto_e

    nop

    :goto_26
    const/4 v7, 0x1

    goto/32 :goto_3

    nop

    :goto_27
    add-int/lit8 v10, v10, 0x1

    goto/32 :goto_9

    nop

    :goto_28
    iget v4, v0, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->j:F

    goto/32 :goto_1d

    nop

    :goto_29
    iget-object v3, v0, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_28

    nop

    :goto_2a
    move-object v0, p0

    goto/32 :goto_4

    nop
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object p1, Lcom/xiaomi/misettings/usagestats/home/category/r;->a:Landroidx/recyclerview/widget/v;

    iget-object p2, p2, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-interface {p1, p2}, Landroidx/recyclerview/widget/v;->a(Landroid/view/View;)V

    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;ILandroidx/recyclerview/widget/RecyclerView$t;III)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object p3

    instance-of v0, p3, Lcom/xiaomi/misettings/usagestats/home/category/k$d;

    if-eqz v0, :cond_0

    check-cast p3, Lcom/xiaomi/misettings/usagestats/home/category/k$d;

    iget-object p1, p2, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    iget-object p2, p4, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-interface {p3, p1, p2, p6, p7}, Lcom/xiaomi/misettings/usagestats/home/category/k$d;->a(Landroid/view/View;Landroid/view/View;II)V

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView$g;->a()Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, p4, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView$g;->f(Landroid/view/View;)I

    move-result p2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result p6

    if-gt p2, p6, :cond_1

    invoke-virtual {p1, p5}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_1
    iget-object p2, p4, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView$g;->i(Landroid/view/View;)I

    move-result p2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result p6

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result p7

    sub-int/2addr p6, p7

    if-lt p2, p6, :cond_2

    invoke-virtual {p1, p5}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_2
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView$g;->b()Z

    move-result p2

    if-eqz p2, :cond_4

    iget-object p2, p4, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView$g;->j(Landroid/view/View;)I

    move-result p2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result p6

    if-gt p2, p6, :cond_3

    invoke-virtual {p1, p5}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_3
    iget-object p2, p4, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView$g;->e(Landroid/view/View;)I

    move-result p2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result p3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result p4

    sub-int/2addr p3, p4

    if-lt p2, p3, :cond_4

    invoke-virtual {p1, p5}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_4
    :goto_0
    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)Z
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 p1, 0x1

    return p1
.end method

.method public b(F)F
    .locals 0

    return p1
.end method

.method public b(Landroidx/recyclerview/widget/RecyclerView$t;)F
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/high16 p1, 0x3f000000    # 0.5f

    return p1
.end method

.method final b(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)I
    .locals 0

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {p0, p2, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(II)I

    move-result p1

    goto/32 :goto_1

    nop

    :goto_1
    return p1

    :goto_2
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->l(Landroid/view/View;)I

    move-result p1

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->c(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)I

    move-result p2

    goto/32 :goto_2

    nop
.end method

.method public b(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;FFIZ)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lcom/xiaomi/misettings/usagestats/home/category/r;->a:Landroidx/recyclerview/widget/v;

    iget-object v3, p3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Landroidx/recyclerview/widget/v;->a(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;FFIZ)V

    return-void
.end method

.method b(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;Ljava/util/List;IFF)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Landroidx/recyclerview/widget/RecyclerView$t;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/home/category/k$c;",
            ">;IFF)V"
        }
    .end annotation

    goto/32 :goto_1f

    nop

    :goto_0
    iget-boolean v2, v1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->i:Z

    goto/32 :goto_3f

    nop

    :goto_1
    move-object v14, p0

    goto/32 :goto_f

    nop

    :goto_2
    iget-boolean v2, v1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->m:Z

    goto/32 :goto_2e

    nop

    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto/32 :goto_38

    nop

    :goto_4
    sub-int/2addr v10, v0

    :goto_5
    goto/32 :goto_7

    nop

    :goto_6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v12

    goto/32 :goto_30

    nop

    :goto_7
    if-gez v10, :cond_0

    goto/32 :goto_21

    :cond_0
    goto/32 :goto_17

    nop

    :goto_8
    move/from16 v4, p6

    goto/32 :goto_2d

    nop

    :goto_9
    move/from16 v6, p5

    goto/32 :goto_32

    nop

    :goto_a
    move-object/from16 v1, p1

    goto/32 :goto_40

    nop

    :goto_b
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v10

    goto/32 :goto_28

    nop

    :goto_c
    add-int/lit8 v10, v10, -0x1

    goto/32 :goto_20

    nop

    :goto_d
    goto/16 :goto_3c

    :goto_e
    goto/32 :goto_2c

    nop

    :goto_f
    if-nez p3, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_6

    nop

    :goto_10
    move-object v0, p0

    goto/32 :goto_15

    nop

    :goto_11
    const/4 v7, 0x0

    goto/32 :goto_10

    nop

    :goto_12
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_13
    invoke-virtual {v8, v12}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :goto_14
    goto/32 :goto_12

    nop

    :goto_15
    move-object/from16 v1, p1

    goto/32 :goto_2b

    nop

    :goto_16
    if-nez v11, :cond_2

    goto/32 :goto_23

    :cond_2
    goto/32 :goto_22

    nop

    :goto_17
    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_36

    nop

    :goto_18
    invoke-interface {v9, v10}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_19
    invoke-virtual/range {v0 .. v7}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;FFIZ)V

    :goto_1a
    goto/32 :goto_24

    nop

    :goto_1b
    move-object/from16 v3, p3

    goto/32 :goto_8

    nop

    :goto_1c
    if-lt v12, v10, :cond_3

    goto/32 :goto_39

    :cond_3
    goto/32 :goto_1e

    nop

    :goto_1d
    iget v4, v0, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->j:F

    goto/32 :goto_35

    nop

    :goto_1e
    invoke-interface {v9, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2f

    nop

    :goto_1f
    move-object/from16 v8, p1

    goto/32 :goto_29

    nop

    :goto_20
    goto/16 :goto_5

    :goto_21
    goto/32 :goto_16

    nop

    :goto_22
    invoke-virtual/range {p2 .. p2}, Landroid/view/ViewGroup;->invalidate()V

    :goto_23
    goto/32 :goto_37

    nop

    :goto_24
    invoke-virtual {v8, v13}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/32 :goto_3

    nop

    :goto_25
    iget v6, v0, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->f:I

    goto/32 :goto_11

    nop

    :goto_26
    if-eqz v1, :cond_4

    goto/32 :goto_3c

    :cond_4
    goto/32 :goto_3b

    nop

    :goto_27
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v13

    goto/32 :goto_33

    nop

    :goto_28
    const/4 v11, 0x0

    goto/32 :goto_3d

    nop

    :goto_29
    move-object/from16 v9, p4

    goto/32 :goto_b

    nop

    :goto_2a
    if-eqz v1, :cond_5

    goto/32 :goto_1a

    :cond_5
    goto/32 :goto_31

    nop

    :goto_2b
    move-object/from16 v2, p2

    goto/32 :goto_19

    nop

    :goto_2c
    iget-boolean v1, v1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->m:Z

    goto/32 :goto_26

    nop

    :goto_2d
    move/from16 v5, p7

    goto/32 :goto_9

    nop

    :goto_2e
    if-nez v2, :cond_6

    goto/32 :goto_e

    :cond_6
    goto/32 :goto_0

    nop

    :goto_2f
    check-cast v0, Lcom/xiaomi/misettings/usagestats/home/category/k$c;

    goto/32 :goto_27

    nop

    :goto_30
    const/4 v7, 0x1

    goto/32 :goto_3a

    nop

    :goto_31
    iget-object v3, v0, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_1d

    nop

    :goto_32
    invoke-virtual/range {v0 .. v7}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;FFIZ)V

    goto/32 :goto_13

    nop

    :goto_33
    move-object v14, p0

    goto/32 :goto_34

    nop

    :goto_34
    iget-boolean v1, v14, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->d:Z

    goto/32 :goto_2a

    nop

    :goto_35
    iget v5, v0, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->k:F

    goto/32 :goto_25

    nop

    :goto_36
    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;

    goto/32 :goto_2

    nop

    :goto_37
    return-void

    :goto_38
    goto :goto_3e

    :goto_39
    goto/32 :goto_1

    nop

    :goto_3a
    move-object v0, p0

    goto/32 :goto_a

    nop

    :goto_3b
    move v11, v0

    :goto_3c
    goto/32 :goto_c

    nop

    :goto_3d
    move v12, v11

    :goto_3e
    goto/32 :goto_1c

    nop

    :goto_3f
    if-eqz v2, :cond_7

    goto/32 :goto_e

    :cond_7
    goto/32 :goto_18

    nop

    :goto_40
    move-object/from16 v2, p2

    goto/32 :goto_1b

    nop
.end method

.method public b(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_0

    sget-object p2, Lcom/xiaomi/misettings/usagestats/home/category/r;->a:Landroidx/recyclerview/widget/v;

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-interface {p2, p1}, Landroidx/recyclerview/widget/v;->b(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract b(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)Z
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract c(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)I
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract c(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method d(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)Z
    .locals 0

    goto/32 :goto_5

    nop

    :goto_0
    return p1

    :goto_1
    const/4 p1, 0x1

    goto/32 :goto_6

    nop

    :goto_2
    const/high16 p2, 0xff0000

    goto/32 :goto_9

    nop

    :goto_3
    const/4 p1, 0x0

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)I

    move-result p1

    goto/32 :goto_2

    nop

    :goto_6
    goto :goto_4

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    if-nez p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_1

    nop

    :goto_9
    and-int/2addr p1, p2

    goto/32 :goto_8

    nop
.end method
