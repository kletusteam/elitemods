.class public Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;


# instance fields
.field private b:Landroid/content/SharedPreferences;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "misettings_device_limit"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->b:Landroid/content/SharedPreferences;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    const-string v0, "is_low_accuracy"

    invoke-virtual {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method public a()Z
    .locals 2

    const-string v0, "is_low_accuracy"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public b(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public b(Z)V
    .locals 1

    const-string v0, "open_accuracy"

    invoke-virtual {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method public b()Z
    .locals 2

    const-string v0, "open_accuracy"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
