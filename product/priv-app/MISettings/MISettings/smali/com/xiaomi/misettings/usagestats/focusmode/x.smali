.class Lcom/xiaomi/misettings/usagestats/focusmode/x;
.super Ljava/lang/Object;

# interfaces
.implements Lb/c/b/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->o()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/c/b/c/h<",
        "Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "FocusRecord"

    const-string v1, "fail: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->g(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->f(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;Z)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->d(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->e(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData;)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->g(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->f(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData$Data;

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData$Data;->records:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;Z)Z

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData$Data;->records:Ljava/util/List;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->i(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)I

    move-result v3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;I)I

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->c(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)I

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/e;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->h(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Lcom/xiaomi/misettings/usagestats/focusmode/a/b;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/b;->a(Ljava/util/List;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->h(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Lcom/xiaomi/misettings/usagestats/focusmode/a/b;

    move-result-object v1

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData$Data;->totalCount:I

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->i(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)I

    move-result v2

    if-le p1, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/b;->a(Z)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;Z)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->d(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->e(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Landroid/os/Handler;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/x;->a(Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method
