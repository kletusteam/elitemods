.class Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;
.super Landroidx/recyclerview/widget/RecyclerView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/xiaomi/misettings/usagestats/d/b/b<",
        "Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/usagestats/d/b/b;I)V
    .locals 1
    .param p1    # Lcom/xiaomi/misettings/usagestats/d/b/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/misettings/usagestats/d/b/b<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;",
            ">;I)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, p0, v0, p2}, Lcom/xiaomi/misettings/usagestats/d/b/b;->a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V

    return-void
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;->b:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/b/b;

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;->a(Lcom/xiaomi/misettings/usagestats/d/b/b;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/d/b/b;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/d/b/b;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/xiaomi/misettings/usagestats/d/b/b<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;",
            ">;"
        }
    .end annotation

    new-instance p1, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;->a:Landroid/content/Context;

    const v0, 0x7f0e0091

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    return-object p1
.end method
