.class public Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;
.super Lcom/xiaomi/misettings/base/BaseFragment;

# interfaces
.implements Lmiuix/appcompat/app/w$a;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private c:Lmiuix/slidingwidget/widget/SlidingButton;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Lmiuix/appcompat/app/w;

.field private m:I

.field private n:I

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Landroid/view/View;

.field private s:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->s:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;)Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->c:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object p0
.end method

.method private a(Landroid/widget/TextView;I)V
    .locals 5

    if-nez p2, :cond_0

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    div-int/lit8 v1, p2, 0x3c

    rem-int/lit8 p2, p2, 0x3c

    const/4 v2, 0x1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f11002b

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-virtual {v1, v3, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v3, 0x7f110028

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-virtual {p2, v3, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const v3, 0x7f130401

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v4, v2

    invoke-virtual {p0, v3, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->p:Z

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->l()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->q:Z

    return p1
.end method

.method private c(I)I
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getColor(I)I

    move-result p1

    return p1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->n:I

    return p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;)Lmiuix/appcompat/app/w;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->l:Lmiuix/appcompat/app/w;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->m:I

    return p0
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->q:Z

    return p0
.end method

.method private initView(Landroid/view/View;)V
    .locals 1

    const v0, 0x7f0b030b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->c:Lmiuix/slidingwidget/widget/SlidingButton;

    const v0, 0x7f0b025b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->d:Landroid/view/View;

    const v0, 0x7f0b0262

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->e:Landroid/view/View;

    const v0, 0x7f0b039e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->h:Landroid/widget/TextView;

    const v0, 0x7f0b0392

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->i:Landroid/widget/TextView;

    const v0, 0x7f0b0263

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->f:Landroid/view/View;

    const v0, 0x7f0b025c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->g:Landroid/view/View;

    const v0, 0x7f0b0393

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->k:Landroid/widget/TextView;

    const v0, 0x7f0b039f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->j:Landroid/widget/TextView;

    const v0, 0x7f0b0260

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->r:Landroid/view/View;

    return-void
.end method

.method private l()V
    .locals 7

    new-instance v6, Lmiuix/appcompat/app/w;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->o:I

    div-int/lit8 v3, v0, 0x3c

    rem-int/lit8 v4, v0, 0x3c

    const/4 v5, 0x1

    move-object v0, v6

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/app/w;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/w$a;IIZ)V

    iput-object v6, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->l:Lmiuix/appcompat/app/w;

    return-void
.end method

.method private m()V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->m:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->n:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->m:I

    if-nez v0, :cond_0

    const/16 v0, 0x12c

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->m:I

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->n:I

    if-nez v0, :cond_1

    const/16 v0, 0x1e0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->n:I

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->i:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->m:I

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->a(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->h:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->n:I

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->a(Landroid/widget/TextView;I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->q:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->o()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->c:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->q:Z

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->c:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0, p0}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method private n()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->f:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/I;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/I;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->g:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/J;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/J;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->r:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/K;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/K;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private o()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->i:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->q:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->h:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->q:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->k:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->q:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->j:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->q:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->h:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->q:Z

    const v2, 0x7f060381

    const v3, 0x7f06037d

    if-eqz v1, :cond_0

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->c(I)I

    move-result v1

    goto :goto_0

    :cond_0
    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->c(I)I

    move-result v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->i:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->q:Z

    if-eqz v1, :cond_1

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->c(I)I

    move-result v1

    goto :goto_1

    :cond_1
    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->c(I)I

    move-result v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private p()V
    .locals 0

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e0143

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public a(Lmiuix/pickerwidget/widget/TimePicker;II)V
    .locals 3

    const/4 p1, 0x0

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f130412

    invoke-static {p2, p3, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->p:Z

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_1

    mul-int/lit8 p2, p2, 0x3c

    add-int/2addr p2, p3

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->m:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p2

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->m:I

    const/4 v0, 0x1

    invoke-static {p2, p3, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;IZ)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->i:Landroid/widget/TextView;

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->m:I

    invoke-direct {p0, p2, p3}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->a(Landroid/widget/TextView;I)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v1, v2}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;J)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;I)V

    goto :goto_0

    :cond_1
    mul-int/lit8 p2, p2, 0x3c

    add-int/2addr p2, p3

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->n:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p2

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->n:I

    invoke-static {p2, p3, p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;IZ)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->h:Landroid/widget/TextView;

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->n:I

    invoke-direct {p0, p2, p3}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->a(Landroid/widget/TextView;I)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result p2

    if-nez p2, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v1, v2}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;J)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;I)V

    :cond_2
    :goto_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result p1

    iget-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->p:Z

    if-ne p1, p2, :cond_3

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->l(Landroid/content/Context;)V

    :cond_3
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/J;->a()Lcom/xiaomi/misettings/usagestats/utils/J;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->s:Ljava/lang/Object;

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/J;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->p()V

    return-void
.end method

.method public b(Z)V
    .locals 3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->m:I

    const/16 v1, 0x12c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->m:I

    invoke-static {v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;IZ)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->n:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;J)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->e(Landroid/content/Context;Z)V

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->l(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->m(Landroid/content/Context;)V

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->o()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/J;->a()Lcom/xiaomi/misettings/usagestats/utils/J;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->s:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/J;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->q:Z

    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->b(Z)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->initView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->m()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsTimeSetFragment;->n()V

    return-void
.end method
