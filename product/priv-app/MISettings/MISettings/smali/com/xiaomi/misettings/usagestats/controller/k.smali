.class final Lcom/xiaomi/misettings/usagestats/controller/k;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/k;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/k;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/k;->a:Landroid/content/Context;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/controller/l;->b()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/16 v5, 0x1c

    if-lt v2, v5, :cond_3

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/k;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->l(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/k;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->m(Landroid/content/Context;)V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/k;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "misettings_device_limit_status"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/k;->a:Landroid/content/Context;

    invoke-static {v0, v4}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;Z)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    new-instance v2, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v5, Lcom/xiaomi/misettings/usagestats/f/j;

    const/4 v6, 0x0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v7

    invoke-direct {v5, v6, v7, v8}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-direct {v2, v5}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/controller/k;->a:Landroid/content/Context;

    invoke-static {v5, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/controller/k;->a:Landroid/content/Context;

    invoke-static {v5}, Lcom/xiaomi/misettings/usagestats/controller/l;->f(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/g;->e()I

    move-result v2

    sub-int/2addr v5, v2

    if-gtz v5, :cond_4

    goto :goto_1

    :cond_4
    move v3, v4

    :goto_1
    if-nez v0, :cond_5

    if-nez v1, :cond_6

    :cond_5
    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    if-eqz v3, :cond_7

    :cond_6
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/controller/l;->a()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IMPORTANT: Stop Monitor....."

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/controller/k;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/controller/l;->m(Landroid/content/Context;)V

    :cond_7
    if-eqz v0, :cond_8

    if-nez v1, :cond_8

    if-nez v3, :cond_8

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/controller/l;->a()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IMPORTANT: Start Monitor....."

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/controller/k;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/controller/l;->l(Landroid/content/Context;)V

    :cond_8
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/controller/l;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IMPORTANT: isSet="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, ",isRunning="

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, ",isFinished="

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
