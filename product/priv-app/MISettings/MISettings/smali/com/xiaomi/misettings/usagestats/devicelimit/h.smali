.class Lcom/xiaomi/misettings/usagestats/devicelimit/h;
.super Lmiui/process/IForegroundInfoListener$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/h;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 2

    iget v0, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/UserHandlerDelegate;->getUserId(Ljava/lang/Integer;)I

    move-result v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/delegate/UserHandlerDelegate;->getSystemUserID()I

    move-result v1

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    iget-object p1, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onForegroundInfoChanged: foregroundPackageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DeviceLimitProlongAppService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/h;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/h;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;)Landroid/os/Handler;

    move-result-object p1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/g;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/g;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/h;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
