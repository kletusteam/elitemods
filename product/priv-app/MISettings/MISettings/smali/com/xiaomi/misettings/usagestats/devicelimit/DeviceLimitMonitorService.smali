.class public Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;
.super Landroid/app/Service;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    api = 0x1a
.end annotation


# static fields
.field private static final a:J

.field private static final b:J


# instance fields
.field private c:I

.field private d:Z

.field private e:Landroid/app/PendingIntent;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Landroid/os/HandlerThread;

.field private i:Landroid/os/Handler;

.field private j:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    const-wide/16 v2, 0x1e

    mul-long/2addr v2, v0

    sput-wide v2, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->a:J

    sput-wide v0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->b:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->f:Z

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/e;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/e;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->j:Ljava/lang/Runnable;

    return-void
.end method

.method private a(I)Landroid/app/Notification;
    .locals 3

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.android.settings.usagestats_devicelimit"

    invoke-direct {v0, v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->c(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->c()Landroid/app/PendingIntent;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    const p1, 0x7f08018b

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x7f08018f

    invoke-static {p1, v1}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/drawable/Icon;)Landroid/app/Notification$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    return-object p1
.end method

.method private a()V
    .locals 5

    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/app/Service;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->d(Landroid/content/Context;)Landroid/app/NotificationChannelGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannelGroup(Landroid/app/NotificationChannelGroup;)V

    new-instance v1, Landroid/app/NotificationChannel;

    const v2, 0x7f1303d4

    invoke-virtual {p0, v2}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.android.settings.usagestats_devicelimit"

    const/4 v4, 0x2

    invoke-direct {v1, v3, v2, v4}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v2}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->enableLights(Z)V

    const-string v2, "app_timer"

    invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->setGroup(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->b(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->c(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;J)V
    .locals 4

    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->b(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object p1

    new-instance v1, Landroid/app/AlarmManager$AlarmClockInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p2

    invoke-direct {v1, v2, v3, p1}, Landroid/app/AlarmManager$AlarmClockInfo;-><init>(JLandroid/app/PendingIntent;)V

    invoke-virtual {v0, v1, p1}, Landroid/app/AlarmManager;->setAlarmClock(Landroid/app/AlarmManager$AlarmClockInfo;Landroid/app/PendingIntent;)V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p2

    const-string v0, "ACTION_RESET"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_1

    :cond_0
    iget-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->d:Z

    if-eqz p2, :cond_2

    :cond_1
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->a(Landroid/content/Context;)V

    :cond_2
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "isProlong"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->f:Z

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ensureIsProlong: isProlong="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->f:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "DeviceLimitMonitorService"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->g()V

    return-void
.end method

.method private b(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/miui/greenguard/a/b;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ACTION_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x4000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    return-object p1
.end method

.method private b()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;Z)V

    return-void
.end method

.method private b(I)V
    .locals 6

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->d(I)V

    goto :goto_0

    :cond_0
    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    int-to-long v2, p1

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v4, v2

    cmp-long v4, v4, v0

    if-gtz v4, :cond_1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->d(I)V

    goto :goto_0

    :cond_1
    const v4, 0x1aefa

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->a(I)Landroid/app/Notification;

    move-result-object p1

    invoke-virtual {p0, v4, p1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v2, v4

    sub-long/2addr v2, v0

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->a:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->a(Landroid/content/Context;J)V

    :goto_0
    return-void
.end method

.method private b(Landroid/content/Intent;)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ACTION_UPDATE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private c()Landroid/app/PendingIntent;
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->e:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.action.usagestas.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    const/high16 v2, 0x4000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->e:Landroid/app/PendingIntent;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->e:Landroid/app/PendingIntent;

    return-object v0
.end method

.method private c(I)Ljava/lang/CharSequence;
    .locals 9

    int-to-long v0, p1

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v0, v2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez p1, :cond_0

    const p1, 0x7f130375

    new-array v0, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->c:I

    int-to-long v3, v3

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v3, v5

    invoke-static {v1, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, p1, v0}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->d:Z

    const v4, 0x7f130372

    if-eqz p1, :cond_2

    const-wide/16 v5, 0x2

    sget-wide v7, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    mul-long/2addr v7, v5

    cmp-long p1, v0, v7

    if-gtz p1, :cond_1

    new-array p1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v2

    invoke-virtual {p0, v4, p1}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-array p1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v2

    invoke-virtual {p0, v4, p1}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    new-array p1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v2

    invoke-virtual {p0, v4, p1}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private c(Landroid/content/Context;)V
    .locals 0

    return-void
.end method

.method private d()I
    .locals 4

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDayUsageStats:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DeviceLimitMonitorService"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->e()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "totalUsageTimeInMinute:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v0
.end method

.method private d(I)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->a(I)Landroid/app/Notification;

    move-result-object p1

    const v0, 0x1aefa

    invoke-virtual {p0, v0, p1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->j:Ljava/lang/Runnable;

    sget-wide v1, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->b:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private e()V
    .locals 5

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->c(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/misettings/common/utils/f;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "jumpToTimeOver: currentTopPkg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DeviceLimitMonitorService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    const/4 v4, 0x1

    if-nez v2, :cond_1

    sget-object v2, Lcom/xiaomi/misettings/usagestats/utils/p;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "jumpToTimeOver: startTimeOverActivity"

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "miui.intent.action.USAGE_STATS_TIMEOVER"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "pkgName"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x800000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v2, "deviceLimit"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Service;->startActivity(Landroid/content/Intent;)V

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->c(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1303c2

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->c(Landroid/content/Context;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method private f()V
    .locals 4

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->d()I

    move-result v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->c:I

    sub-int/2addr v1, v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remainTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "min,totalUsageTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "min"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "DeviceLimitMonitorService"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-gtz v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->h()V

    goto :goto_1

    :cond_0
    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->b(I)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->f:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->f:Z

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;J)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->b()V

    :cond_2
    :goto_0
    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->f:Z

    :goto_1
    return-void
.end method

.method private g()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->f()V

    return-void
.end method

.method private h()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->a(I)Landroid/app/Notification;

    move-result-object v0

    const v1, 0x1aefa

    invoke-virtual {p0, v1, v0}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->e()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->i:Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "device limit monitor"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->h:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->h:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->i:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->a()V

    const v0, 0x7f130373

    invoke-virtual {p0, v0}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->g:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->h:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Service;->stopForeground(Z)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->b(Landroid/content/Context;)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->a(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->d()Z

    move-result p3

    invoke-static {p2, p3}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result p3

    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->c(Landroid/content/Context;)V

    const/4 v0, 0x1

    if-eqz p3, :cond_3

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    iput p3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->c:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStartCommand: commonLimitTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const-string v1, "DeviceLimitMonitorService"

    invoke-static {v1, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p3, 0x0

    iput-boolean p3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->d:Z

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->c:I

    if-lez p3, :cond_2

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->j:Ljava/lang/Runnable;

    invoke-virtual {p3, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->b(Landroid/content/Intent;)Z

    move-result p3

    if-eqz p3, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->j:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return v0

    :cond_1
    invoke-direct {p0, p2, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->a(Landroid/content/Context;Landroid/content/Intent;)V

    const p1, 0x1aefa

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->c:I

    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->a(I)Landroid/app/Notification;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->j:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "onStartCommand: limit time is not available "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitMonitorService;->c:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    :goto_0
    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    :goto_1
    return v0
.end method
