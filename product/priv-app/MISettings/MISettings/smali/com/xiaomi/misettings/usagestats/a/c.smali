.class public Lcom/xiaomi/misettings/usagestats/a/c;
.super Landroid/database/sqlite/SQLiteOpenHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string v0, "appusagestats.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "CREATE TABLE appusagestats(_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,date LONG ,packageName VARCHAR(255),totalForeGroundTime LONG ,lastUsageTime LONG,upload INTEGER,subUpLoad INTEGER,firstForeGroundTime LONG,lastBackGroundTime LONG,subTime0 LONG, subTime1 LONG, subTime2 LONG, subTime3 LONG, subTime4 LONG, subTime5 LONG, subTime6 LONG, subTime7 LONG, subTime8 LONG, subTime9 LONG, subTime10 LONG, subTime11 LONG, subTime12 LONG, subTime13 LONG,subTime14 LONG, subTime15 LONG, subTime16 LONG, subTime17 LONG, subTime18 LONG, subTime19 LONG, subTime20 LONG, subTime21 LONG, subTime22 LONG, subTime23 LONG)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    const-string p2, "DROP TABLE IF EXISTS appusagestats"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/a/c;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    return-void
.end method
