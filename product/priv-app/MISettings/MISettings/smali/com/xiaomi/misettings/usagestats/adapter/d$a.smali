.class Lcom/xiaomi/misettings/usagestats/adapter/d$a;
.super Landroidx/recyclerview/widget/RecyclerView$t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/adapter/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/LinearLayout;

.field private itemView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$t;-><init>(Landroid/view/View;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Landroid/view/View;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->itemView:Landroid/view/View;

    const v0, 0x7f0b023e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->a:Landroid/widget/ImageView;

    const v0, 0x7f0b0389

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->b:Landroid/widget/TextView;

    const v0, 0x7f0b038a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->c:Landroid/widget/TextView;

    const v0, 0x7f0b0242

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->d:Landroid/view/View;

    const v0, 0x7f0b01c6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->e:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->itemView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->a:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->b:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->d:Landroid/view/View;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->c:Landroid/widget/TextView;

    return-object p0
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->e:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x41ab3333    # 21.4f

    invoke-static {p1, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    sub-int/2addr v0, p1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getMaxWidth()I

    move-result p1

    if-ne p1, v0, :cond_1

    return-void

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->b:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    return-void
.end method

.method public b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->e:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/adapter/c;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/adapter/c;-><init>(Lcom/xiaomi/misettings/usagestats/adapter/d$a;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
