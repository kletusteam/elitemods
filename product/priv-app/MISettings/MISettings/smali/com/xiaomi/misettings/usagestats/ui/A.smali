.class Lcom/xiaomi/misettings/usagestats/ui/A;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;Z)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/A;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/ui/A;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "onClick: negative "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/ui/A;->a:Z

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "showDialog"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/ui/A;->a:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/A;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->j(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/A;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->g(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)I

    move-result p2

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/A;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->i(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)I

    move-result p2

    :goto_0
    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;I)I

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/A;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :goto_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/A;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->b(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V

    return-void
.end method
