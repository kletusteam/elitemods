.class public Lcom/xiaomi/misettings/tools/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/Button;

.field private f:Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;

.field private g:I

.field private h:I

.field private i:I

.field private j:Landroid/os/CountDownTimer;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/tools/e;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/tools/e;->i:I

    return p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/tools/e;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/tools/e;->c:Landroid/widget/TextView;

    return-object p0
.end method

.method private a(J)V
    .locals 6

    new-instance p1, Lcom/xiaomi/misettings/tools/d;

    iget p2, p0, Lcom/xiaomi/misettings/tools/e;->i:I

    mul-int/lit16 p2, p2, 0x3e8

    int-to-long v2, p2

    const-wide/16 v4, 0x3e8

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/misettings/tools/d;-><init>(Lcom/xiaomi/misettings/tools/e;JJ)V

    iput-object p1, p0, Lcom/xiaomi/misettings/tools/e;->j:Landroid/os/CountDownTimer;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/tools/e;->f()V

    return-void
.end method

.method static synthetic a(Landroid/app/Activity;)V
    .locals 0

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/a/a;->a(Landroid/view/Window;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/tools/e;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/tools/e;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->e(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/i/h;->d(Landroid/content/Context;Z)V

    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/tools/e;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/tools/e;->i:I

    return p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/tools/e;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/tools/e;->h:I

    return p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/tools/e;)Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/tools/e;->f:Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;

    return-object p0
.end method

.method private g()V
    .locals 8

    iget-object v0, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->m(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->c(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/tools/e;->a(Z)V

    :cond_0
    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->h(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/xiaomi/misettings/tools/e;->g:I

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->j(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/xiaomi/misettings/tools/e;->h:I

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->i(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/tools/e;->i:I

    iget-object v0, p0, Lcom/xiaomi/misettings/tools/e;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f110016

    iget v4, p0, Lcom/xiaomi/misettings/tools/e;->g:I

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/tools/e;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f110014

    iget v4, p0, Lcom/xiaomi/misettings/tools/e;->h:I

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v7

    invoke-virtual {v1, v3, v4, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/xiaomi/misettings/tools/e;->i:I

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/tools/e;->a(J)V

    return-void
.end method

.method private h()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setFlags(II)V

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/f;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    and-int/lit16 v2, v2, -0x2001

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_0
    const v2, 0x7f0b00c0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/xiaomi/misettings/tools/e;->b:Landroid/widget/TextView;

    const v2, 0x7f0b02d5

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/xiaomi/misettings/tools/e;->c:Landroid/widget/TextView;

    const v2, 0x7f0b02d4

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/xiaomi/misettings/tools/e;->d:Landroid/widget/TextView;

    const v2, 0x7f0b007c

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/xiaomi/misettings/tools/e;->e:Landroid/widget/Button;

    iget-object v2, p0, Lcom/xiaomi/misettings/tools/e;->e:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/view/View;

    iget-object v3, p0, Lcom/xiaomi/misettings/tools/e;->e:Landroid/widget/Button;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/misettings/tools/e;->e:Landroid/widget/Button;

    new-array v4, v4, [Lmiuix/animation/a/a;

    invoke-interface {v2, v3, v4}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    const v2, 0x7f0b02c8

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;

    iput-object v2, p0, Lcom/xiaomi/misettings/tools/e;->f:Lcom/xiaomi/misettings/usagestats/home/widget/CircularProgressView;

    new-instance v2, Lcom/xiaomi/misettings/tools/a;

    invoke-direct {v2, v0}, Lcom/xiaomi/misettings/tools/a;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private i()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/tools/e;->j:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/tools/e;->j:Landroid/os/CountDownTimer;

    return-void
.end method


# virtual methods
.method public synthetic a()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f0b0130

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070439

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    return-void
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    iget-object v0, p0, Lcom/xiaomi/misettings/tools/e;->a:Landroid/app/Activity;

    const v1, 0x7f0e0020

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/tools/e;->h()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/tools/e;->g()V

    return-void
.end method

.method public d()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/tools/e;->i()V

    return-void
.end method

.method public e()V
    .locals 4

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/xiaomi/misettings/tools/b;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/tools/b;-><init>(Lcom/xiaomi/misettings/tools/e;)V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x12c

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    :goto_0
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/tools/e;->j:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0b007c

    if-ne p1, v0, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/tools/e;->i:I

    if-gtz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/tools/e;->a(Z)V

    :cond_1
    return-void
.end method
