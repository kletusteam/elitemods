.class Lcom/xiaomi/misettings/usagestats/controller/c;
.super Lmiui/process/IForegroundInfoListener$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onForegroundInfoChanged: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppLimitService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/UserHandlerDelegate;->getUserId(Ljava/lang/Integer;)I

    move-result v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/delegate/UserHandlerDelegate;->getSystemUserID()I

    move-result v1

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;Ljava/lang/String;)Ljava/lang/String;

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/p;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-virtual {v1}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-virtual {v1}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->b(Landroid/content/Context;Ljava/lang/String;)V

    iget-object p1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundPackageName:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-static {v0, p1, v2}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;Ljava/lang/String;Z)V

    :cond_1
    return-void

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->b(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/c;->a:Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;->a(Lcom/xiaomi/misettings/usagestats/controller/AppLimitService;Ljava/lang/String;Z)V

    :goto_0
    return-void
.end method
