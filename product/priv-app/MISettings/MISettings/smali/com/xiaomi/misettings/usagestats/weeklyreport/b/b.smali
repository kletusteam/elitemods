.class public Lcom/xiaomi/misettings/usagestats/weeklyreport/b/b;
.super Ljava/lang/Object;


# direct methods
.method public static a(J)I
    .locals 2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 p0, 0x3

    invoke-virtual {v0, p0}, Ljava/util/Calendar;->get(I)I

    move-result p0

    return p0
.end method

.method private static a()J
    .locals 6

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-nez v2, :cond_0

    move v2, v3

    :cond_0
    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    mul-long/2addr v2, v4

    sub-long/2addr v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCurrentWeekDayStartTime: currentWeekStartTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "WeeklyReportDateUtils"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-wide v0
.end method

.method public static a(I)Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;
    .locals 7

    new-instance v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;-><init>()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/weeklyreport/b/b;->a()J

    move-result-wide v1

    int-to-long v3, p0

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/L;->h:J

    mul-long/2addr v3, v5

    sub-long/2addr v1, v3

    add-long/2addr v5, v1

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/weeklyreport/b/b;->a(J)I

    move-result p0

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    const-wide/16 v1, 0x1

    sub-long/2addr v5, v1

    iput-wide v5, v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    iput p0, v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->a:I

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getRollBackWeekInfo: "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v1, "WeeklyReportDateUtils"

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method public static a(JI)Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;
    .locals 5

    new-instance v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;-><init>()V

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/weeklyreport/b/b;->b(J)J

    move-result-wide p0

    int-to-long v1, p2

    sget-wide v3, Lcom/xiaomi/misettings/usagestats/utils/L;->h:J

    mul-long/2addr v1, v3

    sub-long/2addr p0, v1

    add-long/2addr v3, p0

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/weeklyreport/b/b;->a(J)I

    move-result p2

    iput-wide p0, v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    const-wide/16 p0, 0x1

    sub-long/2addr v3, p0

    iput-wide v3, v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    iput p2, v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->a:I

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "getRollBackWeekInfo: "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "WeeklyReportDateUtils"

    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method private static b(J)J
    .locals 4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_0

    move v0, v1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    int-to-long v0, v0

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    mul-long/2addr v0, v2

    sub-long/2addr p0, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getCurrentWeekDayStartTime: currentWeekStartTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WeeklyReportDateUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-wide p0
.end method
