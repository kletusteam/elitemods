.class public Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;
.super Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;


# instance fields
.field f:Lcom/xiaomi/misettings/usagestats/f/h;

.field g:Lcom/xiaomi/misettings/usagestats/f/o;

.field private h:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

.field private i:Ljava/text/SimpleDateFormat;

.field private j:Z

.field private k:Ljava/io/Serializable;

.field private l:Lcom/xiaomi/misettings/usagestats/home/category/c;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mArguments:Landroid/os/Bundle;

.field private n:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->n:Z

    return-void
.end method

.method private a(J)J
    .locals 5

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v1

    new-instance v3, Lcom/xiaomi/misettings/usagestats/f/j;

    const/4 v4, 0x0

    invoke-direct {v3, v4, p1, p2}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;JLcom/xiaomi/misettings/usagestats/f/j;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object p1

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/xiaomi/misettings/usagestats/b/a/g;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;Ljava/util/List;)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/f/h;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->f:Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/h;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide p1

    return-wide p1

    :cond_1
    const-wide/16 p1, 0x0

    return-wide p1
.end method

.method public static a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/misettings/common/base/a;

    invoke-direct {v0, p0}, Lcom/misettings/common/base/a;-><init>(Landroid/content/Context;)V

    const-class v1, Lcom/xiaomi/misettings/usagestats/home/ui/NewSubSettings;

    invoke-virtual {v0, v1}, Lcom/misettings/common/base/a;->a(Ljava/lang/Class;)Lcom/misettings/common/base/a;

    const-string v1, "com.xiaomi.misettings.usagestats.ui.CategoryUsageDetailFragment"

    invoke-virtual {v0, v1}, Lcom/misettings/common/base/a;->a(Ljava/lang/String;)Lcom/misettings/common/base/a;

    invoke-virtual {v0, p1}, Lcom/misettings/common/base/a;->a(Landroid/os/Bundle;)Lcom/misettings/common/base/a;

    invoke-static {p0}, Lcom/misettings/common/utils/b;->a(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const-class p0, Lcom/xiaomi/misettings/usagestats/home/ui/NewSubSettings;

    invoke-virtual {v0, p0}, Lcom/misettings/common/base/a;->a(Ljava/lang/Class;)Lcom/misettings/common/base/a;

    invoke-virtual {v0, p2}, Lcom/misettings/common/base/a;->a(Ljava/lang/CharSequence;)Lcom/misettings/common/base/a;

    :cond_0
    invoke-virtual {v0}, Lcom/misettings/common/base/a;->b()V

    return-void
.end method

.method private p()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->mArguments:Landroid/os/Bundle;

    const-string v1, "key_category_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->k:Ljava/io/Serializable;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->k:Ljava/io/Serializable;

    const-string v1, ""

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-object v1

    :cond_0
    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->j:Z

    if-nez v2, :cond_1

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/h;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->f:Lcom/xiaomi/misettings/usagestats/f/h;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->i:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->f:Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/h;->d()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v1

    iget-wide v1, v1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->mArguments:Landroid/os/Bundle;

    const-string v2, "weekInfo"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v2, v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    if-eqz v2, :cond_2

    check-cast v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->h:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->i:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->h:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    iget-wide v2, v2, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->i:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->h:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    iget-wide v2, v2, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    return-object v1
.end method

.method private q()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->f:Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/h;->d()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v0

    iget-wide v0, v0, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    sub-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->a(J)J

    move-result-wide v0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->f:Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-static {v2, v3, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/d/g;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/h;J)Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/misettings/common/utils/j;->a()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/xiaomi/misettings/usagestats/ui/c;

    invoke-direct {v2, p0, v0}, Lcom/xiaomi/misettings/usagestats/ui/c;-><init>(Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private r()V
    .locals 3

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->g:Lcom/xiaomi/misettings/usagestats/f/o;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->h:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/d/g;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/o;Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/misettings/common/utils/j;->a()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/xiaomi/misettings/usagestats/ui/b;

    invoke-direct {v2, p0, v0}, Lcom/xiaomi/misettings/usagestats/ui/b;-><init>(Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/util/List;)V
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/c;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/xiaomi/misettings/usagestats/home/category/c;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/home/category/c;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/home/category/c;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->m:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/c;->a(Ljava/util/List;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/home/category/c;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->l()V

    return-void
.end method

.method public synthetic b(Ljava/util/List;)V
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/c;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/xiaomi/misettings/usagestats/home/category/c;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/home/category/c;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/home/category/c;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->m:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/c;->a(Ljava/util/List;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/home/category/c;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->l()V

    return-void
.end method

.method protected n()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->k:Ljava/io/Serializable;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/ui/a;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/ui/a;-><init>(Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic o()V
    .locals 1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->m:Ljava/util/List;

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->j:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->q()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->k:Ljava/io/Serializable;

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/o;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->g:Lcom/xiaomi/misettings/usagestats/f/o;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->r()V

    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->mArguments:Landroid/os/Bundle;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->mArguments:Landroid/os/Bundle;

    const-string v0, "key_is_week"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->j:Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/misettings/common/utils/n;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object p1

    check-cast p1, Ljava/text/SimpleDateFormat;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->i:Ljava/text/SimpleDateFormat;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->i:Ljava/text/SimpleDateFormat;

    const v0, 0x7f1303e2

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->p()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/base/BaseFragment;->b(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/home/category/c;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->m:Ljava/util/List;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/home/category/c;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->m:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/c;->a(Ljava/util/List;Z)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->n:Z

    return-void
.end method
