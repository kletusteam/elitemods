.class public Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;
.super Lcom/xiaomi/misettings/base/BaseFragment;

# interfaces
.implements Lmiuix/appcompat/app/w$a;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Ljava/lang/String;

.field private E:Z

.field private F:J

.field private G:J

.field private H:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

.field private I:Z

.field private J:Lcom/xiaomi/misettings/usagestats/f/g;

.field private K:Lcom/xiaomi/misettings/usagestats/f/g;

.field private L:J

.field private M:I

.field private N:J

.field private O:Z

.field private P:Z

.field private Q:Z

.field private R:I

.field private S:I

.field private T:I

.field private U:Z

.field private V:I

.field private W:Lmiuix/appcompat/app/j;

.field private X:Z

.field private Y:I

.field private Z:I

.field private aa:Ljava/text/SimpleDateFormat;

.field private ba:Lmiui/process/IForegroundInfoListener$Stub;

.field private c:Landroid/content/Context;

.field private ca:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lmiuix/springback/view/SpringBackLayout;

.field private da:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Lmiuix/slidingwidget/widget/SlidingButton;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/view/View;

.field private y:Lmiuix/appcompat/app/w;

.field private z:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->F:J

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->F:J

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->G:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->I:Z

    new-instance v0, Lcom/xiaomi/misettings/usagestats/ui/v;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/ui/v;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->ba:Lmiui/process/IForegroundInfoListener$Stub;

    return-void
.end method

.method private A()V
    .locals 4

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Q:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->V:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Y:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->V:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Z:I

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->x()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->V:I

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Q:Z

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Ljava/lang/String;IZ)V

    new-instance v0, Lcom/miui/greenguard/c/a/f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/greenguard/c/a/f;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/miui/greenguard/c/a/a/b;->e()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Q:Z

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->X:Z

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->V:I

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->M:I

    sub-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Ljava/lang/String;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->U:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->w()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->V:I

    return p1
.end method

.method private a(Ljava/util/List;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, -0x1

    move v2, v0

    move v3, v1

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    if-ne v3, v1, :cond_0

    move v2, v0

    move v3, v2

    goto :goto_1

    :cond_0
    move v2, v0

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    return v2
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/f/g;)J
    .locals 3

    const-wide/16 v0, 0x0

    if-nez p1, :cond_0

    return-wide v0

    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/d;

    if-nez p1, :cond_1

    return-wide v0

    :cond_1
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->D:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object p0
.end method

.method private a(J)V
    .locals 5

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->t()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->L:J

    invoke-static {v0, v1, v2, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/16 v0, 0x8

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->j:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->j:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result p2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070403

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {p1, p2, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setBarType(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->J:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/d;

    new-instance p2, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/j;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->G:J

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-direct {p2, v0}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/xiaomi/misettings/usagestats/f/g;->a(Ljava/util/ArrayList;)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setDayUsageStat(Lcom/xiaomi/misettings/usagestats/f/g;Z)V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 3

    new-instance v0, Lcom/misettings/common/base/a;

    invoke-direct {v0, p0}, Lcom/misettings/common/base/a;-><init>(Landroid/content/Context;)V

    const-string v1, "com.xiaomi.misettings.usagestats.ui.NewAppUsageDetailFragment"

    invoke-virtual {v0, v1}, Lcom/misettings/common/base/a;->a(Ljava/lang/String;)Lcom/misettings/common/base/a;

    const v1, 0x7f1303d5

    invoke-virtual {v0, v1}, Lcom/misettings/common/base/a;->b(I)Lcom/misettings/common/base/a;

    invoke-virtual {v0, p1}, Lcom/misettings/common/base/a;->a(Landroid/os/Bundle;)Lcom/misettings/common/base/a;

    const-class p1, Lcom/xiaomi/misettings/usagestats/home/ui/NewSubSettings;

    invoke-virtual {v0, p1}, Lcom/misettings/common/base/a;->a(Ljava/lang/Class;)Lcom/misettings/common/base/a;

    const/4 p1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Lcom/misettings/common/base/a;->a(Landroid/app/Fragment;I)Lcom/misettings/common/base/a;

    invoke-static {p0}, Lcom/misettings/common/utils/b;->a(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {v0, v1}, Lcom/misettings/common/base/a;->b(I)Lcom/misettings/common/base/a;

    :cond_0
    invoke-virtual {v0}, Lcom/misettings/common/base/a;->b()V

    return-void
.end method

.method private a(Landroid/widget/TextView;I)V
    .locals 6

    if-nez p2, :cond_0

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    div-int/lit8 v1, p2, 0x3c

    rem-int/lit8 p2, p2, 0x3c

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_1

    return-void

    :cond_1
    const/4 v2, 0x1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f11002b

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-virtual {v1, v3, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    if-nez p2, :cond_3

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v3, 0x7f110028

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-virtual {p2, v3, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    const v4, 0x7f130401

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->d(Z)V

    return-void
.end method

.method private a(ZI)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->k()V

    new-instance v0, Lmiuix/appcompat/app/j$a;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;)V

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->a(I)Lmiuix/appcompat/app/j$a;

    const v1, 0x7f130356

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->c(I)Lmiuix/appcompat/app/j$a;

    const v1, 0x7f130355

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->b(I)Lmiuix/appcompat/app/j$a;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/ui/B;

    invoke-direct {v1, p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/ui/B;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;ZI)V

    const p2, 0x7f13029f

    invoke-virtual {v0, p2, v1}, Lmiuix/appcompat/app/j$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    new-instance p2, Lcom/xiaomi/misettings/usagestats/ui/A;

    invoke-direct {p2, p0, p1}, Lcom/xiaomi/misettings/usagestats/ui/A;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;Z)V

    const v1, 0x7f13029e

    invoke-virtual {v0, v1, p2}, Lmiuix/appcompat/app/j$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    new-instance p2, Lcom/xiaomi/misettings/usagestats/ui/z;

    invoke-direct {p2, p0, p1}, Lcom/xiaomi/misettings/usagestats/ui/z;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;Z)V

    invoke-virtual {v0, p2}, Lmiuix/appcompat/app/j$a;->a(Landroid/content/DialogInterface$OnCancelListener;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j$a;->a()Lmiuix/appcompat/app/j;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->W:Lmiuix/appcompat/app/j;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->W:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 6

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->X:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->X:Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Y:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Z:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/L;->f(J)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->M:I

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    return-object p0
.end method

.method private c(Z)V
    .locals 2

    if-eqz p1, :cond_2

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Y:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->V:I

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Z:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->V:I

    :goto_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->V:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->M:I

    const/4 v1, 0x1

    if-gt p1, v0, :cond_1

    invoke-direct {p0, v1, p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(ZI)V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->d(Z)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o:Lmiuix/slidingwidget/widget/SlidingButton;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->d(Z)V

    :goto_1
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Q:Z

    return p1
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    return-object p0
.end method

.method private d(Z)V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->U:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z()V

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->k(Landroid/content/Context;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->V:I

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->M:I

    sub-int/2addr v1, v2

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->c(Landroid/content/Context;Ljava/lang/String;I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->j(Landroid/content/Context;)V

    new-instance p1, Lcom/miui/greenguard/c/a/f;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lcom/miui/greenguard/c/a/f;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/miui/greenguard/c/a/a/b;->e()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/controller/i;->b(Landroid/content/Context;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/controller/i;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->w()V

    return-void
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->A()V

    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m()V

    return-void
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Y:I

    return p0
.end method

.method static synthetic h(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Lmiuix/appcompat/app/w;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->y:Lmiuix/appcompat/app/w;

    return-object p0
.end method

.method static synthetic i(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Z:I

    return p0
.end method

.method private initView(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f0b01f7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/springback/view/SpringBackLayout;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->d:Lmiuix/springback/view/SpringBackLayout;

    const v0, 0x7f0b01bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->e:Landroid/view/View;

    const v0, 0x7f0b01a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->f:Landroid/widget/ImageView;

    const v0, 0x7f0b01be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->g:Landroid/widget/TextView;

    const v0, 0x7f0b0200

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->h:Landroid/widget/TextView;

    const v0, 0x7f0b0213

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->i:Landroid/widget/TextView;

    const v0, 0x7f0b0214

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->j:Landroid/widget/TextView;

    const v0, 0x7f0b0172

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const v0, 0x7f0b01d8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->k:Landroid/widget/TextView;

    const v0, 0x7f0b01b7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b018a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->n:Landroid/view/View;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b02e0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->x:Landroid/view/View;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b006a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b0264

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->p:Landroid/view/View;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b0265

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->q:Landroid/view/View;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b03a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->r:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b03a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->s:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b03a3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->t:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b03a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->u:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b0390

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->v:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b0385

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->w:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    const v0, 0x7f0b0396

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->da:Landroid/view/View;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->n:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->d:Lmiuix/springback/view/SpringBackLayout;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lmiuix/springback/view/SpringBackLayout;->setEnabled(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->h:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->k:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->E:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07050d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->X:Z

    return p0
.end method

.method public static synthetic k(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->q()V

    return-void
.end method

.method private m()V
    .locals 7

    new-instance v6, Lmiuix/appcompat/app/w;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, v6

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/app/w;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/w$a;IIZ)V

    iput-object v6, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->y:Lmiuix/appcompat/app/w;

    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    const-string v2, "isWeek"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->E:Z

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->E:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z:Landroid/os/Bundle;

    const-string v1, "weekInfo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->H:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->H:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->aa:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->H:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    iget-wide v2, v2, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->aa:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->H:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    iget-wide v2, v2, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z:Landroid/os/Bundle;

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->G:J

    const-string v3, "dayBeginTime"

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->G:J

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->aa:Ljava/text/SimpleDateFormat;

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->G:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const v0, 0x7f1303d5

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 4

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->c(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o:Lmiuix/slidingwidget/widget/SlidingButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->x:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->p:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->P:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->da:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Lcom/xiaomi/misettings/f;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->da:Landroid/view/View;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    const v2, 0x7f060397

    invoke-virtual {v1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->c(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method private p()V
    .locals 3

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    const v1, 0x7f06037d

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;I)I

    move-result v1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->R:I

    const v1, 0x7f060351

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;I)I

    move-result v1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->S:I

    const v1, 0x7f0603a3

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->T:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z:Landroid/os/Bundle;

    const-string v1, "packageName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/p;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->O:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/misettings/common/utils/c;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->P:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z:Landroid/os/Bundle;

    const-string v1, "fromPager"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->A:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z:Landroid/os/Bundle;

    const-string v1, "fromNotification"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->C:Z

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->C:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->F:J

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->G:J

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->ba:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/ProcessManagerDelegate;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    :cond_0
    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/ui/d;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/ui/d;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private q()V
    .locals 4

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/ga;->c:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->K:Lcom/xiaomi/misettings/usagestats/f/g;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->K:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Lcom/xiaomi/misettings/usagestats/f/g;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->N:J

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->N:J

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    div-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->M:I

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->E:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->s()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->r()V

    :goto_0
    return-void
.end method

.method private r()V
    .locals 8

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->F:J

    new-instance v3, Lcom/xiaomi/misettings/usagestats/f/j;

    iget-wide v4, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->G:J

    sget-wide v6, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    sub-long/2addr v4, v6

    invoke-direct {v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;JLcom/xiaomi/misettings/usagestats/f/j;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Lcom/xiaomi/misettings/usagestats/f/g;)J

    move-result-wide v1

    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->G:J

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->K:Lcom/xiaomi/misettings/usagestats/f/g;

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->J:Lcom/xiaomi/misettings/usagestats/f/g;

    goto :goto_0

    :cond_0
    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->F:J

    new-instance v5, Lcom/xiaomi/misettings/usagestats/f/j;

    iget-wide v6, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->G:J

    invoke-direct {v5, v6, v7}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(J)V

    invoke-static {v0, v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;JLcom/xiaomi/misettings/usagestats/f/j;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v3

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->J:Lcom/xiaomi/misettings/usagestats/f/g;

    :goto_0
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->J:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-static {v0, v3}, Lcom/xiaomi/misettings/usagestats/d/c;->b(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v3

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->J:Lcom/xiaomi/misettings/usagestats/f/g;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->J:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Lcom/xiaomi/misettings/usagestats/f/g;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->L:J

    new-instance v3, Lcom/xiaomi/misettings/usagestats/ui/f;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/ui/f;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;Landroid/app/Activity;J)V

    invoke-static {v3}, Lcom/misettings/common/utils/j;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private s()V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->H:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->ca:Ljava/util/List;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->L:J

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/d;

    if-nez v2, :cond_0

    new-instance v2, Lcom/xiaomi/misettings/usagestats/f/d;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    :cond_0
    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->L:J

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v5

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->L:J

    new-instance v3, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a(Ljava/util/ArrayList;)V

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v1

    invoke-virtual {v3, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/g;->a(J)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->ca:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/xiaomi/misettings/usagestats/ui/e;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/ui/e;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V

    invoke-static {v0}, Lcom/misettings/common/utils/j;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private t()V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->d:Lmiuix/springback/view/SpringBackLayout;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmiuix/springback/view/SpringBackLayout;->setEnabled(Z)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->f:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->n:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->h:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/xiaomi/misettings/usagestats/b/a/g;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->i:Landroid/widget/TextView;

    iget-wide v4, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->L:J

    invoke-static {v0, v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/misettings/common/utils/q;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->O:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->P:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->w()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->v()V

    goto :goto_1

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o()V

    :goto_1
    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method private u()V
    .locals 3

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->t()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->y()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setBarType(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->l:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->ca:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setWeekUsageStat(Ljava/util/List;Z)V

    return-void
.end method

.method private v()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->x:Landroid/view/View;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->p:Landroid/view/View;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->q:Landroid/view/View;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->x:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/ui/w;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/ui/w;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->p:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/ui/x;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/ui/x;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->q:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/ui/y;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/ui/y;-><init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private w()V
    .locals 9

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->M:I

    sub-int/2addr v0, v2

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->N:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    const v0, 0x7f130421

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    if-gtz v0, :cond_1

    const v0, 0x7f13041b

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v2, 0x7f1303d3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    int-to-long v5, v0

    sget-wide v7, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v5, v7

    invoke-static {v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-virtual {p0, v2, v3}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->k:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private x()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->s:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Y:I

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->u:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Z:I

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Landroid/widget/TextView;I)V

    return-void
.end method

.method private y()V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->ca:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Ljava/util/List;)I

    move-result v0

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->L:J

    int-to-long v3, v0

    cmp-long v0, v1, v3

    if-gez v0, :cond_0

    goto :goto_0

    :cond_0
    div-long/2addr v1, v3

    :goto_0
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->j:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    const v4, 0x7f130395

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v3, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->j:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

.method private z()V
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->O:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->v:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->R:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->w:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->R:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->r:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->O:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->S:I

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->R:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->t:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->O:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->S:I

    goto :goto_1

    :cond_2
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->R:I

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->u:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->O:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->T:I

    goto :goto_2

    :cond_3
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->R:I

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->s:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->O:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->T:I

    goto :goto_3

    :cond_4
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->R:I

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Landroid/app/Activity;J)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0, p2, p3}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(J)V

    :cond_0
    return-void
.end method

.method public a(Lmiuix/pickerwidget/widget/TimePicker;II)V
    .locals 1

    const/4 p1, 0x0

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f130412

    invoke-static {p2, p3, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v0

    mul-int/lit8 p2, p2, 0x3c

    add-int/2addr p2, p3

    iget-boolean p3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Q:Z

    if-ne p3, v0, :cond_2

    if-eqz v0, :cond_1

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Y:I

    goto :goto_0

    :cond_1
    iget p3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Z:I

    :goto_0
    if-ne p2, p3, :cond_2

    return-void

    :cond_2
    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->b(Z)V

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->V:I

    iget-boolean p3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Q:Z

    if-ne p3, v0, :cond_3

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->M:I

    if-gt p2, p3, :cond_3

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(ZI)V

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->A()V

    :goto_1
    return-void
.end method

.method protected b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e014d

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->W:Lmiuix/appcompat/app/j;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->W:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j;->dismiss()V

    :cond_0
    return-void
.end method

.method public synthetic l()V
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->u()V

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/content/Context;)V

    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object p1

    check-cast p1, Ljava/text/SimpleDateFormat;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->aa:Ljava/text/SimpleDateFormat;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->aa:Ljava/text/SimpleDateFormat;

    const v0, 0x7f1303e2

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z:Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->n()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/base/BaseFragment;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/misettings/common/utils/n;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->C:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->ba:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/ProcessManagerDelegate;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->O:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->P:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->X:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->o:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->O:Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/xiaomi/misettings/usagestats/utils/p;->j(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Y:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Z:I

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->X:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Y:I

    goto :goto_1

    :cond_1
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->Z:I

    :goto_1
    iput v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->V:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->x()V

    :cond_2
    return-void
.end method

.method public onStop()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onStop()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->C:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->D:Ljava/lang/String;

    const-string v1, "com.miui.home"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->i()V

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->U:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->A:Ljava/lang/String;

    const-class v1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->B:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c:Landroid/content/Context;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->z:Landroid/os/Bundle;

    if-nez p2, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->initView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->p()V

    return-void
.end method
