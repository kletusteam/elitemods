.class Lcom/xiaomi/misettings/usagestats/controller/n;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->b(Landroid/content/Context;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:I

.field final synthetic c:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;Landroid/content/Context;I)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->c:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->a:Landroid/content/Context;

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-direct {v1, v4, v2, v3}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/g;->e()I

    move-result v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->b:I

    sub-int/2addr v2, v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;J)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "limitedTime="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->b:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "min,usedTime="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "min"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LR-DeviceUsageMonitorService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-gtz v2, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->c:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v3, 0xde

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->c:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->a:Landroid/content/Context;

    invoke-static {v0, v3, v2}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;Landroid/content/Context;I)V

    :goto_0
    const/16 v0, 0x1e

    if-gt v2, v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->c:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->a:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-static {v0, v3, v2, v4}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;Landroid/content/Context;IZ)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "monitor().....updateNotification since reset="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "mins"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->c:Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/controller/n;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/controller/l;->f(Landroid/content/Context;)I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;->a(Lcom/xiaomi/misettings/usagestats/controller/DeviceUsageMonitorService;Landroid/content/Context;IZ)V

    const-string v0, "monitor().....updateNotification since not reach the left."

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method
