.class Lcom/xiaomi/misettings/usagestats/TimeoverActivity$b;
.super Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/TimeoverActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;-><init>(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;->a()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;->b()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->f(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;->b()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->g(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)Lmiui/process/IForegroundInfoListener$Stub;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/ProcessManagerDelegate;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;->b()Ljava/lang/ref/WeakReference;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;->b()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;->b()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->j(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V

    :cond_1
    new-instance v0, Lcom/xiaomi/misettings/usagestats/f;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/f;-><init>(Lcom/xiaomi/misettings/usagestats/TimeoverActivity$b;)V

    invoke-static {v0}, Lcom/misettings/common/utils/j;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic c()V
    .locals 1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;->b()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->h(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V

    return-void
.end method
