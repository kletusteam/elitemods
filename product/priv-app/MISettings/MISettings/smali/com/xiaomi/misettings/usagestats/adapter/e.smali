.class public abstract Lcom/xiaomi/misettings/usagestats/adapter/e;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/adapter/e;->b:Ljava/util/List;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/e;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected abstract a(I)Lcom/xiaomi/misettings/usagestats/c/c;
.end method

.method protected a(ILandroid/view/View;)V
    .locals 0

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/e;->b:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/e;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/adapter/e;->a(I)Lcom/xiaomi/misettings/usagestats/c/c;

    move-result-object p2

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/c/c;->a()Landroid/view/View;

    move-result-object p3

    invoke-virtual {p3, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/xiaomi/misettings/usagestats/c/c;

    move-object v1, p3

    move-object p3, p2

    move-object p2, v1

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/e;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/xiaomi/misettings/usagestats/c/c;->a(Ljava/lang/Object;)V

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/c/c;->d()V

    invoke-virtual {p0, p1, p3}, Lcom/xiaomi/misettings/usagestats/adapter/e;->a(ILandroid/view/View;)V

    return-object p3
.end method
