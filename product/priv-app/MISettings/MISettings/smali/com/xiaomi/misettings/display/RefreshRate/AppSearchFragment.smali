.class public Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;
.super Lmiuix/appcompat/app/Fragment;

# interfaces
.implements Lcom/xiaomi/misettings/display/RefreshRate/k;


# static fields
.field private static b:Landroid/content/Context;

.field private static c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static d:I

.field private static e:I


# instance fields
.field private f:Landroid/view/View;

.field private g:Lmiuix/recyclerview/widget/RecyclerView;

.field public h:Lcom/xiaomi/misettings/display/RefreshRate/p;

.field private i:Ljava/lang/String;

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/xiaomi/misettings/display/RefreshRate/l;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->c:Ljava/util/List;

    const/4 v0, 0x2

    sput v0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->d:I

    const/4 v0, 0x3

    sput v0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->e:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/Fragment;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->i:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->j:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->k:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->n:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->o:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->p:Ljava/util/List;

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->c(Ljava/lang/String;)V

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 7

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->m:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, " Split screen "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Order 3 and thread id is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->m:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget v3, v2, Lcom/xiaomi/misettings/display/RefreshRate/m;->d:I

    sget v4, Lcom/xiaomi/misettings/display/RefreshRate/p;->a:I

    if-eq v3, v4, :cond_1

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v3

    invoke-virtual {v2}, Lcom/xiaomi/misettings/display/RefreshRate/m;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/xiaomi/misettings/display/RefreshRate/e;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/xiaomi/misettings/display/RefreshRate/e;->f(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/xiaomi/misettings/display/RefreshRate/e;->g(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->j:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->h:Lcom/xiaomi/misettings/display/RefreshRate/p;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->j:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/display/RefreshRate/p;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const-string v0, "AppCateSearchFragment"

    const-string v1, "handleSearchData error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    sget p3, Lcom/xiaomi/misettings/display/k;->app_search_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/xiaomi/misettings/display/RefreshRate/m;Z)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->l:Lcom/xiaomi/misettings/display/RefreshRate/l;

    iget-object v1, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    iget-boolean v2, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->e:Z

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/misettings/display/RefreshRate/l;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->l:Lcom/xiaomi/misettings/display/RefreshRate/l;

    iget-object v1, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/xiaomi/misettings/display/RefreshRate/l;->a(Ljava/lang/String;I)V

    sget-object p2, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->b:Landroid/content/Context;

    iget-object v0, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    iget-boolean p1, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->e:Z

    const-string v1, "miui.intent.action.HIGH_REFRESH_SWITCH"

    invoke-static {p2, v1, v0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->m:Ljava/util/List;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, " Order 2 and thread id is "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, " Split screen "

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->i:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->i()V

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->c(Ljava/lang/String;)V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->e(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->g:Lmiuix/recyclerview/widget/RecyclerView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->h:Lcom/xiaomi/misettings/display/RefreshRate/p;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    return-void
.end method

.method public i()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->g:Lmiuix/recyclerview/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/appcompat/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->b:Landroid/content/Context;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->b:Landroid/content/Context;

    :cond_0
    new-instance p1, Lcom/xiaomi/misettings/display/RefreshRate/l;

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->b:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/xiaomi/misettings/display/RefreshRate/l;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->l:Lcom/xiaomi/misettings/display/RefreshRate/l;

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    sget p2, Lcom/xiaomi/misettings/display/j;->app_cate_search_fragment_bg_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->f:Landroid/view/View;

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->f:Landroid/view/View;

    new-instance v0, Lcom/xiaomi/misettings/display/RefreshRate/g;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/display/RefreshRate/g;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    sget p2, Lcom/xiaomi/misettings/display/j;->search_view_list:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->g:Lmiuix/recyclerview/widget/RecyclerView;

    new-instance p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    invoke-direct {p1, p2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->k(I)V

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->g:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    new-instance p1, Lcom/xiaomi/misettings/display/RefreshRate/p;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->m:Ljava/util/List;

    invoke-direct {p1, p2, v0, p0}, Lcom/xiaomi/misettings/display/RefreshRate/p;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/xiaomi/misettings/display/RefreshRate/k;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->h:Lcom/xiaomi/misettings/display/RefreshRate/p;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->i:Ljava/lang/String;

    const-string p2, ""

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->i:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->d(Ljava/lang/String;)V

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->g:Lmiuix/recyclerview/widget/RecyclerView;

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->h:Lcom/xiaomi/misettings/display/RefreshRate/p;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->g:Lmiuix/recyclerview/widget/RecyclerView;

    new-instance p2, Lcom/xiaomi/misettings/display/RefreshRate/h;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/display/RefreshRate/h;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;)V

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$k;)V

    return-void
.end method
