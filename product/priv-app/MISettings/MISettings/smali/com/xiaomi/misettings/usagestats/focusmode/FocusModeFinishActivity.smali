.class public Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishActivity;
.super Lcom/misettings/common/base/SubSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/misettings/common/base/SubSettings;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/app/AppCompatActivity;->onBackPressed()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/l;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/l;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/PersistableBundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;Landroid/os/PersistableBundle;)V

    const p1, 0x7f130413

    invoke-virtual {p0, p1}, Lcom/misettings/common/base/SubSettings;->setTitle(I)V

    return-void
.end method
