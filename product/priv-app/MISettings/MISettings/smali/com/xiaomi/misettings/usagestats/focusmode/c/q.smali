.class public Lcom/xiaomi/misettings/usagestats/focusmode/c/q;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/focusmode/c/q$a;
    }
.end annotation


# static fields
.field private static a:[Ljava/lang/String;

.field private static b:I

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const-string v0, "20"

    const-string v1, "30"

    const-string v2, "45"

    const-string v3, "60"

    const-string v4, "90"

    filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b:I

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/m;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/m;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->c:Ljava/util/List;

    return-void
.end method

.method public static A(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const/4 v0, 0x0

    const-string v1, "misettings_camera_show"

    invoke-static {p0, v1, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    move v0, v1

    :cond_0
    return v0
.end method

.method public static B(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->t(Landroid/content/Context;)I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static C(Landroid/content/Context;)V
    .locals 3

    const-class v0, Landroid/media/AudioManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/media/AudioManager;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x3

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v1, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    goto :goto_0

    :cond_0
    const-string p0, "FocusModeUtils"

    const-string v0, "requestAudioFocus: music is not active"

    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public static D(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/f;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/f;->b(Landroid/content/Context;)V

    return-void
.end method

.method public static E(Landroid/content/Context;)V
    .locals 3

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;Z)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->D(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->K(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "settings_focus_mode_status"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->c(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public static F(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "misettings_camera_show"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method public static G(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/i;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/i;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static H(Landroid/content/Context;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object p0

    const-string v1, "_id"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const-string v3, "total_time!=?"

    const-string v4, "start_time DESC limit 0,1"

    invoke-virtual {p0, v1, v3, v2, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/database/Cursor;)V

    return v0
.end method

.method public static I(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/f;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/f;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static J(Landroid/content/Context;)V
    .locals 2

    const-string v0, "FocusModeUtils"

    const-string v1, "ensureEnterFocusMode: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->i(Landroid/content/Context;)V

    return-void
.end method

.method private static K(Landroid/content/Context;)V
    .locals 4

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->w(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const-string p0, "start_time=?"

    invoke-virtual {v0, p0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    return-void
.end method

.method private static L(Landroid/content/Context;)V
    .locals 4

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/p;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/p;-><init>(Landroid/content/Context;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private static M(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static N(Landroid/content/Context;)V
    .locals 4

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->w(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-gtz v0, :cond_0

    const-string v0, "FocusModeUtils"

    const-string v1, "hasFinishFocusMode: WOW time rollback....."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->K(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->E(Landroid/content/Context;)V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/h;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/h;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/port/FocusModePortActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/land/FocusModeLandActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :cond_1
    const/4 v1, 0x0

    const-string v2, "keyCanWrite"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    instance-of v1, p0, Landroid/app/Activity;

    if-nez v1, :cond_2

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_2
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static O(Landroid/content/Context;)V
    .locals 5

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->w(Landroid/content/Context;)J

    move-result-wide v0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->y(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "wakeup_count"

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->t(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "total_time"

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->p(Landroid/content/Context;)J

    move-result-wide v3

    add-long/2addr v3, v0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "end_time"

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, v4, v1

    const-string v0, "start_time=?"

    invoke-virtual {v3, v2, v0, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateCurrentFocusModeData: updateResult = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, ",ScreenOnCount="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->y(Landroid/content/Context;)I

    move-result p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "FocusModeUtils"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static P(Landroid/content/Context;)V
    .locals 5

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->B(Landroid/content/Context;)Z

    move-result v0

    const-string v1, "FocusModeUtils"

    if-eqz v0, :cond_0

    const-string p0, "writeDataToDB: is experience state"

    invoke-static {v1, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->w(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->c(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "writeDataToDB: hasExist startTime="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "start_time"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "end_time"

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "total_time"

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "_date"

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "wakeup_count"

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "has_upload"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object p0

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/ContentValues;)Z

    move-result p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "writeDataToDB: insertFinish="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static a(I)I
    .locals 2

    const/4 v0, 0x1

    const v1, 0x7f080258

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    return v1

    :cond_0
    const p0, 0x7f08025c

    return p0

    :cond_1
    const p0, 0x7f08025b

    return p0

    :cond_2
    const p0, 0x7f08025a

    return p0

    :cond_3
    const p0, 0x7f080259

    return p0

    :cond_4
    return v1
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 0

    const/16 p0, -0x64

    if-ne p1, p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    return p1
.end method

.method public static a(J)Ljava/lang/String;
    .locals 4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 p0, 0xb

    invoke-virtual {v0, p0}, Ljava/util/Calendar;->get(I)I

    move-result p0

    const/16 p1, 0xc

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0"

    const/16 v2, 0xa

    if-ge p0, v2, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    :goto_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, ":"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ge p1, v2, :cond_1

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    :goto_1
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;IIJ)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IIJ)",
            "Ljava/util/List<",
            "Lb/c/b/a/a;",
            ">;"
        }
    .end annotation

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start_time DESC limit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2

    move-object v0, p0

    move-object v1, p2

    move-object v2, v6

    move-wide v3, p3

    move v5, p1

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/List;JI)V

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/database/Cursor;)V

    return-object v6
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    const-class v0, Landroid/media/AudioManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/media/AudioManager;

    if-eqz p0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    goto :goto_0

    :cond_0
    const-string p0, "FocusModeUtils"

    const-string v0, "killPlayAudioProcess: music is not active"

    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public static a(Landroid/content/Context;J)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "misettings_focus_limit_milli"

    invoke-static {p0, v0, p1, p2}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/List;JI)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            "Ljava/util/List<",
            "Lb/c/b/a/a;",
            ">;JI)V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    if-eqz v0, :cond_a

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_0

    goto/16 :goto_5

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getFocusHistoryData: count="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "FocusModeUtils"

    invoke-static {v6, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v8, 0x0

    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_9

    const-string v10, "_id"

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v11, "_date"

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    const-string v13, "start_time"

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    const-string v15, "end_time"

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-string v15, "total_time"

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/16 v18, 0x0

    move-wide/from16 v19, v2

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-static/range {p0 .. p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ""

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v18

    const-string v3, "_id=?"

    invoke-virtual {v0, v3, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-wide/from16 v6, v19

    const-wide/16 v16, 0x0

    goto/16 :goto_4

    :cond_1
    const-wide/16 v16, 0x0

    cmp-long v0, v6, v16

    if-eqz v0, :cond_7

    if-nez v15, :cond_2

    goto :goto_3

    :cond_2
    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;-><init>()V

    invoke-virtual {v0, v13, v14}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;->setStartTime(J)V

    invoke-virtual {v0, v6, v7}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;->setEndTime(J)V

    invoke-virtual {v0, v15}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;->setTotalTime(I)V

    invoke-virtual {v0, v11, v12}, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->setDate(J)V

    cmp-long v3, v11, p3

    if-nez v3, :cond_3

    move v6, v2

    goto :goto_1

    :cond_3
    move/from16 v6, v18

    :goto_1
    invoke-virtual {v0, v6}, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->setFirstUseDate(Z)V

    cmp-long v6, v8, v11

    if-eqz v6, :cond_6

    invoke-static {v1, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Ljava/util/List;Ljava/util/List;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;-><init>()V

    if-nez v3, :cond_4

    move v3, v2

    goto :goto_2

    :cond_4
    move/from16 v3, v18

    :goto_2
    invoke-virtual {v0, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->setFirstUseDate(Z)V

    move-wide/from16 v6, v19

    invoke-static {v6, v7, v11, v12}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;->setToday(Z)V

    invoke-virtual {v0, v11, v12}, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->setDate(J)V

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez p5, :cond_5

    invoke-virtual {v0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;->setFirstData(Z)V

    :cond_5
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-wide v8, v11

    goto :goto_4

    :cond_6
    move-wide/from16 v6, v19

    if-eqz v4, :cond_8

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    :goto_3
    move-wide/from16 v6, v19

    :cond_8
    :goto_4
    move-object/from16 v0, p1

    move-wide v2, v6

    goto/16 :goto_0

    :cond_9
    invoke-interface {v5}, Ljava/util/List;->clear()V

    invoke-static {v1, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Ljava/util/List;Ljava/util/List;)V

    :cond_a
    :goto_5
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/focusmode/c/q$a;)V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/l;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/l;-><init>(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/focusmode/c/q$a;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const-string v0, "touch_assistant_enabled"

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Landroid/content/Intent;

    const-string v0, "com.miui.touchassistant.SHOW_FLOATING_WINDOW"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "com.miui.touchassistant"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.miui.touchassistant.CoreService"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/focusmode/data/b;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;->c:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;

    sget-object v1, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;

    const-string v2, "0"

    invoke-direct {p0, p1, v2, v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;)V

    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/focusmode/data/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "long_press_back_key"

    invoke-static {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "long_press_home_key"

    invoke-static {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "long_press_menu_key"

    invoke-static {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "settings_focus_mode_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    const-string p0, "FocusModeUtils"

    const-string p1, "ensureFinishFocusMode: hasWriteFocusStatus"

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/k;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static a(Landroid/database/Cursor;)V
    .locals 1

    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    :cond_0
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb/c/b/a/a;",
            ">;",
            "Ljava/util/List<",
            "Lb/c/b/a/a;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method public static a()Z
    .locals 2

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string v1, "yunluo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string v1, "nabu"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic b(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->P(Landroid/content/Context;)V

    return-void
.end method

.method public static b(Landroid/content/Context;I)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "misettings_focus_limit_time"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method public static b(Landroid/content/Context;J)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "misettings_key_enter_focus_time"

    invoke-static {p0, v0, p1, p2}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/focusmode/data/b;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "none"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {p0, p1, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    new-instance p0, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;

    sget-object v1, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;->b:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;

    sget-object v2, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;->c:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;)V

    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/focusmode/data/b;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;

    invoke-virtual {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, v1, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;->e:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b(Landroid/content/Context;Ljava/util/List;)V

    :cond_4
    :goto_1
    return-void
.end method

.method static synthetic c(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->O(Landroid/content/Context;)V

    return-void
.end method

.method public static c(Landroid/content/Context;I)V
    .locals 1

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "last_focus_mode_limit_time"

    invoke-virtual {p0, v0, p1}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;I)V

    return-void
.end method

.method private static c(Landroid/content/Context;J)Z
    .locals 4

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object p0

    const-string v0, "start_time"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    aput-object p1, v2, p2

    const-string p1, "start_time=?"

    const/4 v3, 0x0

    invoke-virtual {p0, v0, p1, v2, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result p0

    if-lez p0, :cond_0

    move p2, v1

    :cond_0
    return p2
.end method

.method static synthetic d(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->M(Landroid/content/Context;)V

    return-void
.end method

.method private static d(Landroid/content/Context;I)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sendToHandyMode: send mode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FocusModeUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.action.handymode.changemode"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "mode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic e(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->K(Landroid/content/Context;)V

    return-void
.end method

.method public static f(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->d(Landroid/content/Context;I)V

    return-void
.end method

.method public static g(Landroid/content/Context;)V
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->y(Landroid/content/Context;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const-string v2, "focus_mode_screen_on_count"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "collectScreenOnCount: screenOnCount = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->y(Landroid/content/Context;)I

    move-result p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "FocusModeUtils"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static h(Landroid/content/Context;)V
    .locals 10

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->f(Landroid/content/Context;)V

    const-string v0, "com.android.settings"

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->d(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "force_fsg_nav_bar"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v4, 0x1

    const-string v5, ""

    if-ne v1, v4, :cond_1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/util/Timer;

    invoke-direct {v6}, Ljava/util/Timer;-><init>()V

    new-instance v7, Lcom/xiaomi/misettings/usagestats/focusmode/c/n;

    invoke-direct {v7, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/n;-><init>(Landroid/content/Context;)V

    const-wide/16 v8, 0x1f4

    invoke-virtual {v6, v7, v8, v9}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :goto_0
    new-instance v6, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v7, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;

    sget-object v8, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;

    invoke-direct {v6, v2, v1, v7, v8}, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "touch_assistant_enabled"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v4, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v6, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v7, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;->b:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;

    sget-object v8, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;

    invoke-direct {v6, v2, v1, v7, v8}, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "double_click_power_key"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "none"

    invoke-static {v6, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {v7, v2, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    new-instance v6, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;

    sget-object v7, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;->b:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;

    sget-object v8, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;->c:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;

    invoke-direct {v6, v2, v1, v7, v8}, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    sget-object v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->c:Ljava/util/List;

    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "miui_slider_tool_choice"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v6, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v7, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;->b:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;

    sget-object v8, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;

    invoke-direct {v6, v2, v1, v7, v8}, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    invoke-static {p0}, Landroid/provider/MiuiSettings$SoundMode;->isSilenceModeOn(Landroid/content/Context;)Z

    move-result v2

    xor-int/2addr v2, v4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "zen_mode"

    invoke-static {v6, v7, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eq v6, v4, :cond_6

    const/4 v6, 0x2

    if-ne v1, v6, :cond_6

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "screentime_turn_off_ringer"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x20

    if-le v4, v6, :cond_5

    invoke-static {p0, v2}, Landroid/provider/MiuiSettings$SoundMode;->setSilenceModeOn(Landroid/content/Context;Z)V

    goto :goto_1

    :cond_5
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->I(Landroid/content/Context;)V

    :goto_1
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v4, Lcom/xiaomi/misettings/usagestats/focusmode/c/o;

    invoke-direct {v4, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/o;-><init>(Landroid/content/Context;)V

    const-wide/16 v6, 0x1388

    invoke-virtual {v2, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;->a:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;

    sget-object v6, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;

    const-string v7, "mode_ringer"

    invoke-direct {v2, v7, v1, v4, v6}, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "lock_screen_allow_private_notifications"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v4, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v6, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;->c:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;

    sget-object v7, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;

    invoke-direct {v4, v2, v1, v6, v7}, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "lock_screen_show_notifications"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v3, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;->c:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;

    sget-object v5, Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;->a:Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;

    invoke-direct {v3, v2, v1, v4, v5}, Lcom/xiaomi/misettings/usagestats/focusmode/data/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$a;Lcom/xiaomi/misettings/usagestats/focusmode/data/f$b;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    invoke-static {p0}, Lcom/misettings/common/utils/f;->o(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "FocusModeUtils"

    const-string v2, "disableEffects: disable_key_menu"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "disallow_key_menu"

    invoke-static {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    const-string v1, "disallow_key_home"

    invoke-static {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    const-string v1, "disallow_key_back"

    invoke-static {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;Ljava/util/List;)V

    :cond_9
    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b(Landroid/content/Context;Ljava/util/List;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->L(Landroid/content/Context;)V

    return-void
.end method

.method public static i(Landroid/content/Context;)V
    .locals 5

    const-string v0, "FocusModeUtils"

    const-string v1, "ensureEnterFocusMode: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->z(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_1

    :try_start_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->N(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    sget v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b:I

    add-int/2addr v1, v2

    sput v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b:I

    sget v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b:I

    const/4 v3, 0x5

    if-ge v1, v3, :cond_0

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/c/g;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/g;-><init>(Landroid/content/Context;)V

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_0
    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;Z)V

    :goto_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ensureEnterFocusMode: retryCount="

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b:I

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;Z)V

    :goto_1
    return-void
.end method

.method public static j(Landroid/content/Context;)V
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/focusmode/service/FocusModeForeBackGroundMonitorService;

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/service/FocusModeForeBackGroundMonitorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method

.method public static k(Landroid/content/Context;)V
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/focusmode/service/FocusModeForeBackGroundMonitorService;

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/service/FocusModeForeBackGroundMonitorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    :cond_0
    return-void
.end method

.method public static l(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result p0

    invoke-static {p0}, Landroid/os/Process;->killProcess(I)V

    const/4 p0, 0x0

    invoke-static {p0}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method public static m(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "content://com.xiaomi.misettings.usagestats.focusmode.data.TimerContentProvider/focus_mode_timers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "id"

    const-string v4, "duration"

    filled-new-array {v3, v4}, [Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/h;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    return-object v0
.end method

.method public static n(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;
    .locals 6

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object v0

    const-string v1, "total_time"

    const-string v2, "wakeup_count"

    const-string v3, "start_time"

    const-string v4, "end_time"

    const-string v5, "_date"

    filled-new-array {v1, v2, v3, v4, v5}, [Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->w(Landroid/content/Context;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const-string p0, "start_time=?"

    const/4 v5, 0x0

    invoke-virtual {v0, v1, p0, v3, v5}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;-><init>()V

    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->totalTime:I

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->wakeUps:I

    const/4 v1, 0x2

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->startTime:J

    const/4 v1, 0x3

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->endTime:J

    const/4 v1, 0x4

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->date:J

    :cond_0
    return-object v0
.end method

.method public static o(Landroid/content/Context;)J
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object p0

    const-string v0, "_date"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "_date asc limit 0, 1"

    invoke-virtual {p0, v0, v1, v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFirstDate: firstDate="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "FocusModeUtils"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/database/Cursor;)V

    return-wide v0
.end method

.method public static p(Landroid/content/Context;)J
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    const-wide/16 v2, 0x14

    mul-long/2addr v0, v2

    const-string v2, "misettings_focus_limit_milli"

    invoke-static {p0, v2, v0, v1}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static q(Landroid/content/Context;)I
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object p0

    const-string v0, "SELECT COUNT(_id) FROM focusmode GROUP BY _date"

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/database/Cursor;)V

    return v0
.end method

.method public static r(Landroid/content/Context;)I
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object p0

    const-string v1, "SELECT SUM(total_time) FROM focusmode WHERE end_time != 0"

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/database/Cursor;)V

    :cond_1
    return v0
.end method

.method public static s(Landroid/content/Context;)J
    .locals 4

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object v0

    const-string v1, "0"

    filled-new-array {v1, v1}, [Ljava/lang/String;

    move-result-object v2

    const-string v3, "total_time=? OR end_time = ? OR start_time == end_time"

    invoke-virtual {v0, v3, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object p0

    const-string v0, "_id"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    const-string v2, "total_time!=?"

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v1, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    return-wide v0

    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/database/Cursor;)V

    int-to-long v0, v0

    return-wide v0
.end method

.method public static t(Landroid/content/Context;)I
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "misettings_focus_limit_time"

    const/16 v1, 0x14

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static u(Landroid/content/Context;)I
    .locals 8

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object p0

    const-string v0, "SELECT _date FROM focusmode GROUP BY _date ORDER BY _date DESC"

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    const-string v0, "_date"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v3, 0x1

    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    sub-long/2addr v1, v4

    sget-wide v6, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    cmp-long v1, v1, v6

    if-nez v1, :cond_0

    add-int/lit8 v3, v3, 0x1

    move-wide v1, v4

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/database/Cursor;)V

    return v3

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method public static v(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;
    .locals 7

    new-instance v6, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->r(Landroid/content/Context;)I

    move-result v1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->s(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->q(Landroid/content/Context;)I

    move-result v4

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->u(Landroid/content/Context;)I

    move-result v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;-><init>(IJII)V

    return-object v6
.end method

.method public static w(Landroid/content/Context;)J
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "misettings_key_enter_focus_time"

    const-wide/16 v1, 0x0

    invoke-static {p0, v0, v1, v2}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static x(Landroid/content/Context;)I
    .locals 1

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "last_focus_mode_limit_time"

    invoke-virtual {p0, v0}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method public static y(Landroid/content/Context;)I
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "focus_mode_screen_on_count"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static z(Landroid/content/Context;)Z
    .locals 9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "time is"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->w(Landroid/content/Context;)J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->t(Landroid/content/Context;)I

    move-result v3

    int-to-long v3, v3

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v3, v5

    cmp-long v1, v1, v3

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ltz v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " FOCUS_MODE_STATUS:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "settings_focus_mode_status"

    invoke-static {v1, v4, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ensureEnterFocusMode"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->w(Landroid/content/Context;)J

    move-result-wide v5

    sub-long/2addr v0, v5

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->t(Landroid/content/Context;)I

    move-result v5

    int-to-long v5, v5

    sget-wide v7, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v5, v7

    cmp-long v0, v0, v5

    if-gez v0, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {p0, v4, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-eq p0, v2, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :cond_2
    :goto_1
    return v2
.end method
