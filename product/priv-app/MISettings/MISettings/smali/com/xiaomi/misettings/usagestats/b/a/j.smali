.class public final enum Lcom/xiaomi/misettings/usagestats/b/a/j;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/xiaomi/misettings/usagestats/b/a/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/xiaomi/misettings/usagestats/b/a/j;

.field private static final synthetic b:[Lcom/xiaomi/misettings/usagestats/b/a/j;


# instance fields
.field private volatile c:Lcom/xiaomi/misettings/usagestats/b/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/b/a/j;

    const/4 v1, 0x0

    const-string v2, "INSTANCE"

    invoke-direct {v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/b/a/j;->a:Lcom/xiaomi/misettings/usagestats/b/a/j;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/xiaomi/misettings/usagestats/b/a/j;

    sget-object v2, Lcom/xiaomi/misettings/usagestats/b/a/j;->a:Lcom/xiaomi/misettings/usagestats/b/a/j;

    aput-object v2, v0, v1

    sput-object v0, Lcom/xiaomi/misettings/usagestats/b/a/j;->b:[Lcom/xiaomi/misettings/usagestats/b/a/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private a(Lorg/json/JSONObject;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/xiaomi/misettings/usagestats/b/a/a;

    invoke-direct {v0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/a;-><init>(Lorg/json/JSONObject;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/j;->c:Lcom/xiaomi/misettings/usagestats/b/a/a;

    return-void
.end method

.method private a(I)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/j;->c:Lcom/xiaomi/misettings/usagestats/b/a/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/j;->c:Lcom/xiaomi/misettings/usagestats/b/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a()I

    move-result v0

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private b(Landroid/content/Context;)Z
    .locals 6

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "misettings_key_update_time"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    invoke-static {p1, v1, v4, v5}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    const/4 p1, 0x1

    return p1
.end method

.method private c(Landroid/content/Context;)V
    .locals 5

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "MiSettings"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "version"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/b/a/j;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/j;->a(Lorg/json/JSONObject;)V

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/U;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    const-string v4, "category_sort.json"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/lang/String;Ljava/io/File;)V

    goto :goto_0

    :cond_2
    const-string p1, "CloudCategoryUtils"

    const-string v0, "readFromCloudProvider: no misettings cloud data"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method private d(Landroid/content/Context;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/j;->c:Lcom/xiaomi/misettings/usagestats/b/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/j;->c:Lcom/xiaomi/misettings/usagestats/b/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a()I

    move-result v0

    if-lez v0, :cond_0

    return-void

    :cond_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/U;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object p1

    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "category_sort.json"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/b/a/j;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "CloudCategoryUtils"

    const-string v1, "readFromFile: "

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    :goto_0
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/b/a/j;
    .locals 1

    const-class v0, Lcom/xiaomi/misettings/usagestats/b/a/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/xiaomi/misettings/usagestats/b/a/j;

    return-object p0
.end method

.method public static values()[Lcom/xiaomi/misettings/usagestats/b/a/j;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/b/a/j;->b:[Lcom/xiaomi/misettings/usagestats/b/a/j;

    invoke-virtual {v0}, [Lcom/xiaomi/misettings/usagestats/b/a/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/misettings/usagestats/b/a/j;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/j;->c:Lcom/xiaomi/misettings/usagestats/b/a/a;

    if-nez v0, :cond_0

    const-string p1, ""

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/j;->c:Lcom/xiaomi/misettings/usagestats/b/a/a;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/j;->d(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/j;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/j;->c(Landroid/content/Context;)V

    :cond_0
    return-void
.end method
