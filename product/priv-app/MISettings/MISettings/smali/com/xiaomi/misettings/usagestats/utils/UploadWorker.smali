.class public Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;
.super Landroidx/work/Worker;


# instance fields
.field private g:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroidx/work/WorkerParameters;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/work/WorkerParameters;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Landroidx/work/Worker;-><init>(Landroid/content/Context;Landroidx/work/WorkerParameters;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;)J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;->g:J

    return-wide v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/utils/aa;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/aa;-><init>(Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;->a(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public n()Landroidx/work/ListenableWorker$a;
    .locals 6
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0}, Landroidx/work/ListenableWorker;->e()Landroidx/work/f;

    move-result-object v0

    const-wide/16 v1, 0x0

    const-string v3, "input_date"

    invoke-virtual {v0, v3, v1, v2}, Landroidx/work/f;->a(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;->g:J

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doWork: date = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;->g:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "UploadWorker"

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v4, p0, Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;->g:J

    cmp-long v0, v4, v1

    if-eqz v0, :cond_3

    :try_start_0
    invoke-virtual {p0}, Landroidx/work/ListenableWorker;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/a/d;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/d;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/work/ListenableWorker;->a()Landroid/content/Context;

    move-result-object v1

    iget-wide v4, p0, Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;->g:J

    invoke-virtual {v0, v1, v4, v5}, Lcom/xiaomi/misettings/usagestats/a/d;->a(Landroid/content/Context;J)Lorg/json/JSONArray;

    move-result-object v0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-gtz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_2

    const-string v0, "run: has no upload data"

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroidx/work/ListenableWorker;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/a/q;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/a/q;->d()V

    invoke-static {}, Landroidx/work/ListenableWorker$a;->a()Landroidx/work/ListenableWorker$a;

    move-result-object v0

    return-object v0

    :cond_2
    const-string v2, "records"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lb/c/b/c/g;

    const-string v2, "/weeklyReport/upload"

    sget-object v3, Lb/c/b/c/a$a;->b:Lb/c/b/c/a$a;

    new-instance v4, Lcom/xiaomi/misettings/usagestats/utils/Z;

    invoke-direct {v4, p0}, Lcom/xiaomi/misettings/usagestats/utils/Z;-><init>(Lcom/xiaomi/misettings/usagestats/utils/UploadWorker;)V

    invoke-direct {v0, v2, v1, v3, v4}, Lb/c/b/c/g;-><init>(Ljava/lang/String;Ljava/util/Map;Lb/c/b/c/a$a;Lb/c/b/c/h;)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lb/c/b/c/g;->a(I)V

    invoke-virtual {v0}, Lb/c/b/c/g;->a()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_3
    :goto_2
    invoke-static {}, Landroidx/work/ListenableWorker$a;->c()Landroidx/work/ListenableWorker$a;

    move-result-object v0

    return-object v0
.end method
