.class public Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;
.super Landroid/app/Service;


# static fields
.field public static a:Ljava/lang/String; = ""


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lmiui/process/IForegroundInfoListener$Stub;

.field private e:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/b;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->c:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/c;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->d:Lmiui/process/IForegroundInfoListener$Stub;

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    const-class v0, Landroid/app/ActivityManager;

    invoke-virtual {p0, v0}, Landroid/app/Service;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->b:Ljava/lang/String;

    return-object p0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "keyMonitorPackageName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->a()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private b()V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Service;->getApplication()Landroid/app/Application;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->b:Ljava/lang/String;

    invoke-static {v0, v2, v1, v1}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;ZZ)V

    :cond_1
    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->b:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->c:Ljava/util/List;

    return-object p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->b()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "CurrentNoLimitAppMonitorService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->d:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/ProcessManagerDelegate;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/d;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/d;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->e:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/app/Service;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "CurrentNoLimitAppMonitorService"

    const-string v1, "onDestory"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ""

    sput-object v0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->d:Lmiui/process/IForegroundInfoListener$Stub;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/ProcessManagerDelegate;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->e:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Landroid/app/Service;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    const-string p2, "CurrentNoLimitAppMonitorService"

    const-string p3, "onStartCommand:"

    invoke-static {p2, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    const-string p3, "keyMonitorPackageName"

    invoke-virtual {p1, p3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->b:Ljava/lang/String;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->b:Ljava/lang/String;

    sput-object p1, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->a:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "monitorPackageName:"

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p3, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->a:Ljava/lang/String;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    :goto_0
    const/4 p1, 0x2

    return p1
.end method
