.class public Lcom/xiaomi/misettings/usagestats/utils/T;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Z)Landroid/app/Dialog;
    .locals 5

    new-instance v0, Lmiuix/appcompat/app/j$a;

    const v1, 0x7f140007

    invoke-direct {v0, p0, v1}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;I)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0e0044

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const v3, 0x7f0b0216

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f06032e

    invoke-virtual {p0, v4}, Landroid/content/Context;->getColor(I)I

    move-result p0

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->b(Landroid/view/View;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j$a;->a()Lmiuix/appcompat/app/j;

    move-result-object p0

    invoke-virtual {p0, v2}, Lmiuix/appcompat/app/j;->setCanceledOnTouchOutside(Z)V

    new-instance p1, Lcom/xiaomi/misettings/usagestats/utils/S;

    invoke-direct {p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/S;-><init>(Z)V

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    return-object p0
.end method

.method public static a(Landroid/content/Context;Z)Landroid/app/Dialog;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/T;->a(Landroid/content/Context;Ljava/lang/CharSequence;Z)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method
