.class Lcom/xiaomi/misettings/usagestats/b/a/n;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/b/a/o;->a(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/b/a/i;

.field final synthetic b:Lcom/xiaomi/misettings/usagestats/b/a/o;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/b/a/o;Lcom/xiaomi/misettings/usagestats/b/a/i;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/b/a/n;->b:Lcom/xiaomi/misettings/usagestats/b/a/o;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/b/a/n;->a:Lcom/xiaomi/misettings/usagestats/b/a/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/n;->b:Lcom/xiaomi/misettings/usagestats/b/a/o;

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/b/a/o;->d:Lcom/xiaomi/misettings/usagestats/b/a/p;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/b/a/o;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/b/a/n;->a:Lcom/xiaomi/misettings/usagestats/b/a/i;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/xiaomi/misettings/usagestats/b/a/p;->a(Lcom/xiaomi/misettings/usagestats/b/a/p;Landroid/content/Context;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSuccess: merge result:="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetCategoryUtils"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/b/a/n;->b:Lcom/xiaomi/misettings/usagestats/b/a/o;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/b/a/o;->d:Lcom/xiaomi/misettings/usagestats/b/a/p;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/b/a/p;->b(Lcom/xiaomi/misettings/usagestats/b/a/p;)Lcom/xiaomi/misettings/usagestats/b/a/i;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/b/a/i;->a(Lorg/json/JSONObject;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/b/a/n;->b:Lcom/xiaomi/misettings/usagestats/b/a/o;

    iget-object v2, v1, Lcom/xiaomi/misettings/usagestats/b/a/o;->d:Lcom/xiaomi/misettings/usagestats/b/a/p;

    iget-object v3, v1, Lcom/xiaomi/misettings/usagestats/b/a/o;->b:Landroid/content/Context;

    iget-boolean v1, v1, Lcom/xiaomi/misettings/usagestats/b/a/o;->c:Z

    invoke-static {v2, v3, v0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/p;->a(Lcom/xiaomi/misettings/usagestats/b/a/p;Landroid/content/Context;Lorg/json/JSONObject;Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/n;->b:Lcom/xiaomi/misettings/usagestats/b/a/o;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/b/a/o;->d:Lcom/xiaomi/misettings/usagestats/b/a/p;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/b/a/p;->c()V

    return-void
.end method
