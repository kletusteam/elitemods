.class Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/media/SoundPool;

.field private c:I

.field private d:J


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->a:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/f;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;-><init>()V

    return-void
.end method


# virtual methods
.method a()V
    .locals 9

    goto/32 :goto_6

    nop

    :goto_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->b:Landroid/media/SoundPool;

    goto/32 :goto_f

    nop

    :goto_1
    const/high16 v5, 0x3f800000    # 1.0f

    goto/32 :goto_4

    nop

    :goto_2
    const/4 v7, 0x0

    goto/32 :goto_5

    nop

    :goto_3
    iget v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->c:I

    goto/32 :goto_10

    nop

    :goto_4
    const/4 v6, 0x0

    goto/32 :goto_2

    nop

    :goto_5
    const/high16 v8, 0x3f800000    # 1.0f

    goto/32 :goto_a

    nop

    :goto_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto/32 :goto_0

    nop

    :goto_7
    sub-long v3, v0, v3

    goto/32 :goto_e

    nop

    :goto_8
    return-void

    :goto_9
    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->d:J

    goto/32 :goto_7

    nop

    :goto_a
    invoke-virtual/range {v2 .. v8}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto/32 :goto_c

    nop

    :goto_b
    cmp-long v3, v3, v5

    goto/32 :goto_11

    nop

    :goto_c
    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->d:J

    :goto_d
    goto/32 :goto_8

    nop

    :goto_e
    const-wide/16 v5, 0x32

    goto/32 :goto_b

    nop

    :goto_f
    if-nez v2, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_9

    nop

    :goto_10
    const/high16 v4, 0x3f800000    # 1.0f

    goto/32 :goto_1

    nop

    :goto_11
    if-gtz v3, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_3

    nop
.end method

.method a(I)V
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, v1, v3, v2}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->b:Landroid/media/SoundPool;

    goto/32 :goto_d

    nop

    :goto_2
    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->c:I

    :goto_3
    goto/32 :goto_c

    nop

    :goto_4
    invoke-static {}, Lcom/xiaomi/misettings/Application;->c()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_5
    const/4 v2, 0x1

    goto/32 :goto_e

    nop

    :goto_6
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_7
    new-instance v0, Landroid/media/SoundPool;

    goto/32 :goto_9

    nop

    :goto_8
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_a

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_a
    return-void

    :goto_b
    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->b:Landroid/media/SoundPool;

    goto/32 :goto_10

    nop

    :goto_c
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->a:Ljava/util/Set;

    goto/32 :goto_6

    nop

    :goto_d
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_7

    nop

    :goto_e
    invoke-direct {v0, v2, v2, v1}, Landroid/media/SoundPool;-><init>(III)V

    goto/32 :goto_b

    nop

    :goto_f
    const v3, 0x7f12000b

    goto/32 :goto_0

    nop

    :goto_10
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->b:Landroid/media/SoundPool;

    goto/32 :goto_4

    nop
.end method

.method b(I)V
    .locals 1

    goto/32 :goto_a

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->a:Ljava/util/Set;

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {p1}, Landroid/media/SoundPool;->release()V

    goto/32 :goto_d

    nop

    :goto_3
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->b:Landroid/media/SoundPool;

    goto/32 :goto_6

    nop

    :goto_4
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    goto/32 :goto_5

    nop

    :goto_5
    if-nez p1, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_3

    nop

    :goto_6
    if-nez p1, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_2

    nop

    :goto_7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto/32 :goto_b

    nop

    :goto_8
    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->b:Landroid/media/SoundPool;

    :goto_9
    goto/32 :goto_c

    nop

    :goto_a
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->a:Ljava/util/Set;

    goto/32 :goto_7

    nop

    :goto_b
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result p1

    goto/32 :goto_0

    nop

    :goto_c
    return-void

    :goto_d
    const/4 p1, 0x0

    goto/32 :goto_8

    nop
.end method
