.class public Lcom/xiaomi/misettings/usagestats/f/d;
.super Lcom/xiaomi/misettings/usagestats/f/b;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/usagestats/f/b;",
        "Ljava/lang/Comparable<",
        "Lcom/xiaomi/misettings/usagestats/f/d;",
        ">;"
    }
.end annotation


# instance fields
.field private f:J

.field private g:J

.field private h:I

.field public i:J

.field public j:J

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/f/b;-><init>(Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->f:J

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->g:J

    const/4 p1, 0x0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->h:I

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    new-instance p1, Lcom/xiaomi/misettings/usagestats/f/c;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/usagestats/f/c;-><init>(Lcom/xiaomi/misettings/usagestats/f/d;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->k:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/usagestats/f/d;)I
    .locals 4

    iget-wide v0, p1, Lcom/xiaomi/misettings/usagestats/f/d;->f:J

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/f/d;->f:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Long;->compare(JJ)I

    move-result p1

    return p1
.end method

.method public a(I)V
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->h:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->h:I

    return-void
.end method

.method public a(J)V
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->f:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->f:J

    return-void
.end method

.method public a(JJ)V
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->g:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->g:J

    iput-wide p3, p0, Lcom/xiaomi/misettings/usagestats/f/d;->f:J

    :cond_0
    return-void
.end method

.method public a(JJJJ)V
    .locals 4

    cmp-long v0, p1, p7

    if-gez v0, :cond_0

    return-void

    :cond_0
    sub-long v0, p1, p7

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    div-long/2addr v0, v2

    long-to-int v0, v0

    sub-long p1, p3, p1

    cmp-long p1, p5, p1

    if-lez p1, :cond_1

    sub-long p5, p3, p7

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->k:Ljava/util/ArrayList;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->h:I

    return-void
.end method

.method public b(J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->g:J

    return-void
.end method

.method public c(J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->f:J

    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/f/d;->a(Lcom/xiaomi/misettings/usagestats/f/d;)I

    move-result p1

    return p1
.end method

.method public d(J)V
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->g:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->g:J

    :cond_0
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->l:Ljava/lang/String;

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->l:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    const-class v0, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/d;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/b;->a:Ljava/lang/String;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/f/b;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->h:I

    return v0
.end method

.method public g()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->g:J

    return-wide v0
.end method

.method public h()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->k:Ljava/util/ArrayList;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->f:J

    return-wide v0
.end method

.method public j()V
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->h:I

    return-void
.end method

.method public k()Z
    .locals 4

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppUsageStats{pkgName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/f/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", totalForegroundTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->f:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", lastUsageTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", foregroundCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/f/d;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
