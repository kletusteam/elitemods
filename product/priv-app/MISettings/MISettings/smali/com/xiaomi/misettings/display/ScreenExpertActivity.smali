.class public Lcom/xiaomi/misettings/display/ScreenExpertActivity;
.super Lcom/misettings/common/base/BaseFragmentActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/misettings/common/base/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroidx/fragment/app/Fragment;
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/display/ScreenExpertSettings;

    invoke-direct {v0}, Lcom/xiaomi/misettings/display/ScreenExpertSettings;-><init>()V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/xiaomi/misettings/display/b;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "ScreenExpertActivity"

    const-string v0, "The Current device does not support advanced color mode"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/d;->e(I)V

    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/d;->g(Z)V

    :cond_1
    return-void
.end method
