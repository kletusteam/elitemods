.class public Lcom/xiaomi/misettings/usagestats/b/b;
.super Ljava/lang/Object;


# static fields
.field private static volatile a:Lcom/xiaomi/misettings/usagestats/f/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/l;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/f/l;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/b/b;->a:Lcom/xiaomi/misettings/usagestats/f/l;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/l;
    .locals 1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/b;->b()V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/b/b;->a:Lcom/xiaomi/misettings/usagestats/f/l;

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/f/l;->b(Landroid/content/Context;)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/b/b;->a:Lcom/xiaomi/misettings/usagestats/f/l;

    return-object p0
.end method

.method public static a()V
    .locals 1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/f/l;->b()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/b;->b()V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/b/b;->a:Lcom/xiaomi/misettings/usagestats/f/l;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/l;->c()V

    return-void
.end method

.method public static b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/l;
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/Q;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/i;

    move-result-object p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/f/l;->a(Lcom/xiaomi/misettings/usagestats/f/i;)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/b/b;->a:Lcom/xiaomi/misettings/usagestats/f/l;

    return-object p0
.end method

.method private static declared-synchronized b()V
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/b/b;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/b/b;->a:Lcom/xiaomi/misettings/usagestats/f/l;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/f/l;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/f/l;-><init>()V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/b/b;->a:Lcom/xiaomi/misettings/usagestats/f/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static c(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/l;
    .locals 1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/b;->b()V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/b/b;->a:Lcom/xiaomi/misettings/usagestats/f/l;

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/f/l;->c(Landroid/content/Context;)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/b/b;->a:Lcom/xiaomi/misettings/usagestats/f/l;

    return-object p0
.end method

.method public static d(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/l;
    .locals 1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/b;->b()V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/b/b;->a:Lcom/xiaomi/misettings/usagestats/f/l;

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/f/l;->a(Landroid/content/Context;)V

    sget-object p0, Lcom/xiaomi/misettings/usagestats/b/b;->a:Lcom/xiaomi/misettings/usagestats/f/l;

    return-object p0
.end method
