.class public abstract Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;
.super Lcom/xiaomi/misettings/base/BaseFragment;


# instance fields
.field protected c:Landroid/os/Handler;

.field protected d:Lcom/xiaomi/misettings/usagestats/d/a/a;

.field protected e:Landroidx/recyclerview/widget/RecyclerView;

.field protected f:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

.field protected g:I

.field protected h:Ljava/lang/String;

.field protected i:Z

.field protected j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->g:I

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->j:Z

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public synthetic c(I)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    :cond_0
    return-void
.end method

.method public d(I)V
    .locals 4

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->c:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/b;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/b;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;I)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method protected k()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->f:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->d()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->f:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->setVisibility(I)V

    return-void
.end method

.method protected abstract l()V
.end method

.method public m()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d(I)V

    :cond_0
    return-void
.end method

.method protected n()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->f:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->f:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->f:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->e()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->c:Landroid/os/Handler;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "onCreate: "

    const-string v1, "BaseSubContentFragment"

    if-eqz p1, :cond_0

    const-string v2, "isWeek"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    const-string v2, "misettings_from_page"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "fromSteadyOn"

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->j:Z

    const-string v2, "screen_time_home_intent_key"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->h:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->h:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->c:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->b()V

    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->a(Landroid/view/View;)V

    const p2, 0x7f0b0183

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    new-instance p2, Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    if-eqz v0, :cond_0

    const/4 v2, 0x2

    :cond_0
    invoke-virtual {p2, v2}, Lcom/xiaomi/misettings/usagestats/d/a/a;->b(I)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->d:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->e:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/n;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/n;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;)V

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$k;)V

    const p2, 0x7f0b019e

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->f:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->f:Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    invoke-static {p2}, Lcom/xiaomi/misettings/f;->c(Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_1

    const p2, 0x7f120009

    goto :goto_0

    :cond_1
    const p2, 0x7f120008

    :goto_0
    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/home/widget/GifImageView;->setGifResource(I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->n()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->l()V

    return-void
.end method
