.class Lcom/xiaomi/misettings/usagestats/utils/ba$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/utils/ba;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field a:Landroid/app/usage/UsageEvents$Event;

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/app/usage/UsageEvents$Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a(I)Landroid/app/usage/UsageEvents$Event;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getValidResumeEvent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LR-UsageEventUtil"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->a:Landroid/app/usage/UsageEvents$Event;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->b:Ljava/util/List;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_2

    :cond_0
    const/4 p1, 0x0

    move v2, p1

    :goto_0
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->b:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/usage/UsageEvents$Event;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/app/usage/UsageEvents$Event;)I

    move-result v3

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->a:Landroid/app/usage/UsageEvents$Event;

    invoke-static {v4}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/app/usage/UsageEvents$Event;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(II)Z

    move-result v3

    if-eqz v3, :cond_1

    move p1, v1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->a:Landroid/app/usage/UsageEvents$Event;

    return-object p1

    :cond_3
    :goto_2
    return-object v0
.end method

.method public a(Landroid/app/usage/UsageEvents$Event;)V
    .locals 2

    invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->a:Landroid/app/usage/UsageEvents$Event;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method
