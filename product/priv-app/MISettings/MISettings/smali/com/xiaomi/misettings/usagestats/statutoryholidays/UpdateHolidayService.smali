.class public Lcom/xiaomi/misettings/usagestats/statutoryholidays/UpdateHolidayService;
.super Landroid/app/IntentService;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "UpdateHolidayService"

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/UpdateHolidayService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const-string p1, "UpdateHolidayService"

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/UpdateHolidayService;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const-string p1, "UpdateHolidayService"

    const-string v0, "onHandleIntent()"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object p1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/statutoryholidays/h;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/statutoryholidays/h;-><init>(Lcom/xiaomi/misettings/usagestats/statutoryholidays/UpdateHolidayService;)V

    invoke-virtual {p1, v0}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method
