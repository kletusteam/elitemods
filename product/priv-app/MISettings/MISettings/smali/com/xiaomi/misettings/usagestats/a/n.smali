.class Lcom/xiaomi/misettings/usagestats/a/n;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/a/o;->a(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/xiaomi/misettings/usagestats/a/o;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/a/o;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/a/n;->b:Lcom/xiaomi/misettings/usagestats/a/o;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/a/n;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/a/n;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/i;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/a/n;->b:Lcom/xiaomi/misettings/usagestats/a/o;

    iget-object v3, v1, Lcom/xiaomi/misettings/usagestats/f/i;->d:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/a/o;->a(Lcom/xiaomi/misettings/usagestats/a/o;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/i;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v2

    iget-object v3, v1, Lcom/xiaomi/misettings/usagestats/f/i;->d:Ljava/util/List;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result v1

    iget-wide v4, v2, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "saveToUnlockDB: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v6, "UnlockDatabaseUtils"

    invoke-static {v6, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/a/n;->b:Lcom/xiaomi/misettings/usagestats/a/o;

    invoke-static {v7, v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/a/o;->a(Lcom/xiaomi/misettings/usagestats/a/o;Ljava/lang/Long;J)Z

    move-result v7

    if-eqz v7, :cond_2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "saveToUnlockDB: has exist current time stamp, timeStamp="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    const-string v9, "date"

    invoke-virtual {v7, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "timeStamp"

    invoke-virtual {v7, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v8, "hasUpload"

    invoke-virtual {v7, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v8, "totalCount"

    invoke-virtual {v7, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/a/n;->b:Lcom/xiaomi/misettings/usagestats/a/o;

    invoke-virtual {v3, v7}, Lcom/xiaomi/misettings/usagestats/a/e;->a(Landroid/content/ContentValues;)Z

    goto :goto_1

    :cond_3
    return-void
.end method
