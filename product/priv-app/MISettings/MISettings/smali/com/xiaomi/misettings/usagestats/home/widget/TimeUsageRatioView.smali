.class public Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;
.super Landroid/view/View;


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:I

.field private c:I

.field private d:I

.field private e:Z

.field private f:F

.field private g:F

.field private h:Landroid/graphics/RectF;

.field private i:Landroid/graphics/RectF;

.field private j:I

.field private k:I

.field private l:I

.field private m:Landroid/animation/ValueAnimator;

.field private n:F

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 p1, 0xa

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->d:I

    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->n:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->o:Z

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->p:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->e()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;)F
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->g:F

    return p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;F)F
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->f:F

    return p1
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->i:Landroid/graphics/RectF;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->i:Landroid/graphics/RectF;

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->g:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->f:F

    sub-float/2addr v0, v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->n:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->e:Z

    const/4 v2, 0x0

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->i:Landroid/graphics/RectF;

    iput v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->b:I

    int-to-float v3, v3

    mul-float/2addr v3, v0

    iput v3, v1, Landroid/graphics/RectF;->right:F

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->i:Landroid/graphics/RectF;

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v3, v0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->b:I

    int-to-float v4, v0

    mul-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->left:F

    int-to-float v0, v0

    iput v0, v1, Landroid/graphics/RectF;->right:F

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->i:Landroid/graphics/RectF;

    iput v2, v0, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->c:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->j:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->i:Landroid/graphics/RectF;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->l:I

    int-to-float v2, v1

    int-to-float v1, v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->o:Z

    return p1
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->h:Landroid/graphics/RectF;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->h:Landroid/graphics/RectF;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->h:Landroid/graphics/RectF;

    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->b:I

    int-to-float v2, v2

    iput v2, v0, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->c:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->k:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->h:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->h:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->p:Z

    return p1
.end method

.method private e()V
    .locals 2

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/H;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->e:Z

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->a:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060368

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->j:I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060369

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->k:I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x44268000    # 666.0f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->l:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->p:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->d()V

    :goto_0
    return-void
.end method

.method public synthetic a(Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->n:F

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->p:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->p:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->a()V

    return-void
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->m:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->m:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->m:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->m:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->m:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/widget/b;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/widget/b;-><init>(Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->m:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/widget/d;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/widget/d;-><init>(Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->m:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/widget/e;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/widget/e;-><init>(Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->o:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->p:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->o:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_1
    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->b(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->a(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, 0x0

    if-ne v0, v2, :cond_0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->b:I

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->b:I

    move p1, v3

    :goto_0
    if-ne v1, v2, :cond_1

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->c:I

    goto :goto_1

    :cond_1
    iget p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->d:I

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->c:I

    move p2, v3

    :goto_1
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public setProgress(F)V
    .locals 1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->g:F

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->g:F

    const v0, 0x3d4ccccd    # 0.05f

    cmpg-float p1, p1, v0

    if-gez p1, :cond_0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->g:F

    :cond_0
    return-void
.end method
