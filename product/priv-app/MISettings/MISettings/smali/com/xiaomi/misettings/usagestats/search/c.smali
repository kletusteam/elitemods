.class public Lcom/xiaomi/misettings/usagestats/search/c;
.super Ljava/util/Observable;


# static fields
.field private static volatile a:Lcom/xiaomi/misettings/usagestats/search/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    return-void
.end method

.method public static a()Lcom/xiaomi/misettings/usagestats/search/c;
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/search/c;->a:Lcom/xiaomi/misettings/usagestats/search/c;

    if-nez v0, :cond_1

    const-class v0, Lcom/xiaomi/misettings/usagestats/search/c;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/search/c;->a:Lcom/xiaomi/misettings/usagestats/search/c;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/search/c;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/search/c;-><init>()V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/search/c;->a:Lcom/xiaomi/misettings/usagestats/search/c;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/search/c;->a:Lcom/xiaomi/misettings/usagestats/search/c;

    return-object v0
.end method


# virtual methods
.method public b()V
    .locals 1

    invoke-virtual {p0}, Ljava/util/Observable;->setChanged()V

    const-string v0, "exit_search_mode"

    invoke-virtual {p0, v0}, Ljava/util/Observable;->notifyObservers(Ljava/lang/Object;)V

    return-void
.end method
