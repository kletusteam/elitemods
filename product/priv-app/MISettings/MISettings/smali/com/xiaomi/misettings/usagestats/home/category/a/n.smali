.class public Lcom/xiaomi/misettings/usagestats/home/category/a/n;
.super Lcom/xiaomi/misettings/usagestats/d/b/b;

# interfaces
.implements Lmiuix/appcompat/app/w$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/usagestats/d/b/b<",
        "Lb/c/a/a/a;",
        ">;",
        "Lmiuix/appcompat/app/w$a;"
    }
.end annotation


# instance fields
.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Lmiuix/slidingwidget/widget/SlidingButton;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Z

.field private m:Lmiuix/appcompat/app/w;

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:Lcom/xiaomi/misettings/usagestats/home/category/c/f;

.field private t:Lcom/xiaomi/misettings/usagestats/home/category/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/b/b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const p1, 0x7f0b0390

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->g:Landroid/widget/TextView;

    const p1, 0x7f0b0385

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->h:Landroid/widget/TextView;

    const p1, 0x7f0b02e0

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->b:Landroid/view/View;

    const p1, 0x7f0b006a

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->i:Lmiuix/slidingwidget/widget/SlidingButton;

    const p1, 0x7f0b0264

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->j:Landroid/view/View;

    const p1, 0x7f0b0265

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->k:Landroid/view/View;

    const p1, 0x7f0b03a1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->c:Landroid/widget/TextView;

    const p1, 0x7f0b03a0

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->d:Landroid/widget/TextView;

    const p1, 0x7f0b03a3

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->e:Landroid/widget/TextView;

    const p1, 0x7f0b03a2

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->f:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->f()V

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f06037d

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->r:I

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f060351

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->p:I

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0603a3

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->q:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->g:Landroid/widget/TextView;

    const v0, 0x7f130391

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->h:Landroid/widget/TextView;

    const v0, 0x7f130390

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->h()V

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/category/a/n;)Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->i:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object p0
.end method

.method private a(IZ)V
    .locals 2

    new-instance v0, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;

    invoke-direct {v0}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;-><init>()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->setCategoryId(Ljava/lang/String;)V

    mul-int/lit8 p1, p1, 0x3c

    invoke-virtual {v0, p1}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->setDurationPerDay(I)V

    xor-int/lit8 p1, p2, 0x1

    invoke-virtual {v0, p1}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->setPolicyType(I)V

    new-instance p1, Lcom/miui/greenguard/c/a/g;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-direct {p1, p2, v0}, Lcom/miui/greenguard/c/a/g;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;)V

    invoke-virtual {p1}, Lcom/miui/greenguard/c/a/a/d;->c()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->t:Lcom/xiaomi/misettings/usagestats/home/category/c;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/category/c;->a()V

    :cond_0
    return-void
.end method

.method private a(Landroid/widget/TextView;I)V
    .locals 6

    if-nez p2, :cond_0

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    div-int/lit8 v1, p2, 0x3c

    rem-int/lit8 p2, p2, 0x3c

    const/4 v2, 0x1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f11002b

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-virtual {v1, v3, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v3, 0x7f110028

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-virtual {p2, v3, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    const v4, 0x7f130401

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/category/a/n;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->c(I)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/category/a/n;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->b(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    new-instance v0, Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;

    invoke-direct {v0}, Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;-><init>()V

    invoke-virtual {v0, p1}, Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;->setStatus(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;->setCategoryId(Ljava/lang/String;)V

    new-instance v1, Lcom/miui/greenguard/c/a/h;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/miui/greenguard/c/a/h;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/miui/greenguard/c/a/h;->f()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->l:Z

    invoke-static {v0, v2, v3, p1}, Lcom/xiaomi/misettings/usagestats/utils/j;->a(Landroid/content/Context;Ljava/lang/String;ZI)V

    invoke-virtual {v1}, Lcom/miui/greenguard/c/a/a/d;->c()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->t:Lcom/xiaomi/misettings/usagestats/home/category/c;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/category/c;->a()V

    :cond_0
    return-void
.end method

.method private a(ZI)V
    .locals 2

    new-instance v0, Lmiuix/appcompat/app/j$a;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;)V

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->a(I)Lmiuix/appcompat/app/j$a;

    const v1, 0x7f130356

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->c(I)Lmiuix/appcompat/app/j$a;

    const v1, 0x7f130355

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j$a;->b(I)Lmiuix/appcompat/app/j$a;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/category/a/m;

    invoke-direct {v1, p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/a/m;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/a/n;ZI)V

    const p1, 0x7f13029f

    invoke-virtual {v0, p1, v1}, Lmiuix/appcompat/app/j$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    new-instance p1, Lcom/xiaomi/misettings/usagestats/home/category/a/l;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/l;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/a/n;)V

    const p2, 0x7f13029e

    invoke-virtual {v0, p2, p1}, Lmiuix/appcompat/app/j$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j$a;->a()Lmiuix/appcompat/app/j;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/category/a/n;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->c()V

    return-void
.end method

.method private b(Z)V
    .locals 4

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->i:Lmiuix/slidingwidget/widget/SlidingButton;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->c(Z)V

    return-void

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result p1

    if-eqz p1, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->n:I

    goto :goto_0

    :cond_1
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->o:I

    :goto_0
    int-to-long v0, p1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->s:Lcom/xiaomi/misettings/usagestats/home/category/c/f;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/home/category/c/f;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    const/4 v1, 0x1

    if-gtz v0, :cond_2

    invoke-direct {p0, v1, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->a(ZI)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->i:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->c(Z)V

    :goto_1
    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/category/a/n;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->l:Z

    return p1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/category/a/n;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->n:I

    return p0
.end method

.method private c()V
    .locals 7

    new-instance v6, Lmiuix/appcompat/app/w;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, v6

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/app/w;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/w$a;IIZ)V

    iput-object v6, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->m:Lmiuix/appcompat/app/w;

    return-void
.end method

.method private c(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->l:Z

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->n:I

    goto :goto_0

    :cond_0
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->o:I

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->e()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->l:Z

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->a(IZ)V

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/category/a/n;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->c(Z)V

    return-void
.end method

.method private c(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->h()V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->a(Z)V

    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->s:Lcom/xiaomi/misettings/usagestats/home/category/c/f;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/home/category/a/n;)Lmiuix/appcompat/app/w;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->m:Lmiuix/appcompat/app/w;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/home/category/a/n;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->o:I

    return p0
.end method

.method private e()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->h()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->g()V

    return-void
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->b:Landroid/view/View;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->j:Landroid/view/View;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->k:Landroid/view/View;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->b:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/category/a/i;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/i;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/a/n;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->j:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/category/a/j;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/j;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/a/n;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->k:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/category/a/k;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/k;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/a/n;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->d:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->n:I

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->a(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->f:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->o:I

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->a(Landroid/widget/TextView;I)V

    return-void
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->i:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->p:I

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->r:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->i:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->p:I

    goto :goto_1

    :cond_1
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->r:I

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->i:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->q:I

    goto :goto_2

    :cond_2
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->r:I

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->i:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->q:I

    goto :goto_3

    :cond_3
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->r:I

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V
    .locals 1

    const-string p3, "CategoryLimitHolder"

    const-string v0, "CategoryLimitHolder bindView"

    invoke-static {p3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    check-cast p2, Lcom/xiaomi/misettings/usagestats/home/category/c/f;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->s:Lcom/xiaomi/misettings/usagestats/home/category/c/f;

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/category/c;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->t:Lcom/xiaomi/misettings/usagestats/home/category/c;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->d()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x1

    invoke-static {p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->n:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->d()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->o:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->d()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/j;->b(Ljava/lang/String;)Z

    move-result p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->i:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p2, p1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->e()V

    return-void
.end method

.method public bridge synthetic a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V
    .locals 0

    check-cast p2, Lb/c/a/a/a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V

    return-void
.end method

.method public a(Lmiuix/pickerwidget/widget/TimePicker;II)V
    .locals 4

    const/4 p1, 0x0

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f130412

    invoke-static {p2, p3, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v0

    mul-int/lit8 p2, p2, 0x3c

    add-int/2addr p2, p3

    iget-boolean p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->l:Z

    if-ne p3, v0, :cond_2

    if-eqz v0, :cond_1

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->n:I

    goto :goto_0

    :cond_1
    iget p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->o:I

    :goto_0
    if-ne p2, p3, :cond_2

    return-void

    :cond_2
    iget-boolean p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->l:Z

    if-ne p3, v0, :cond_3

    int-to-long v0, p2

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->s:Lcom/xiaomi/misettings/usagestats/home/category/c/f;

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/home/category/c/f;->a()J

    move-result-wide v2

    cmp-long p3, v0, v2

    if-gtz p3, :cond_3

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->a(ZI)V

    goto :goto_1

    :cond_3
    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/usagestats/home/category/a/n;->c(I)V

    :goto_1
    return-void
.end method
