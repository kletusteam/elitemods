.class public Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;
.super Landroidx/fragment/app/Fragment;

# interfaces
.implements Lcom/xiaomi/misettings/display/RefreshRate/A;


# static fields
.field private static final a:Z

.field private static b:Landroid/content/Context;


# instance fields
.field private c:Lcom/xiaomi/misettings/display/RefreshRate/j;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;",
            ">;"
        }
    .end annotation
.end field

.field private e:[I

.field private f:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "dc_backlight_fps_incompatible"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lb/c/a/b/a/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;
    .locals 0

    sput-object p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->b:Landroid/content/Context;

    new-instance p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;-><init>()V

    return-object p0
.end method

.method private f()V
    .locals 4

    const-string v0, "fpsList"

    invoke-static {v0}, Lb/c/a/b/a/b;->b(Ljava/lang/String;)[I

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v1, v0

    if-gtz v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->sort([I)V

    array-length v1, v0

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->e:[I

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->e:[I

    array-length v3, v0

    sub-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x1

    aget v3, v0, v3

    aput v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private g()V
    .locals 8

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->f:Landroid/view/View;

    sget v1, Lcom/xiaomi/misettings/display/j;->fps_choose_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->e:[I

    if-nez v1, :cond_0

    return-void

    :cond_0
    array-length v1, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_4

    iget-object v3, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->e:[I

    aget v3, v3, v2

    new-instance v4, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;

    sget-object v5, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->b:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;-><init>(Landroid/content/Context;)V

    iget-object v5, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->c:Lcom/xiaomi/misettings/display/RefreshRate/j;

    invoke-virtual {v5, v3}, Lcom/xiaomi/misettings/display/RefreshRate/j;->a(I)Lcom/xiaomi/misettings/display/RefreshRate/j$a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->d()I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v5}, Lcom/xiaomi/misettings/display/RefreshRate/j$a;->d()I

    move-result v6

    const/16 v7, 0x78

    if-ne v6, v7, :cond_2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v4, v5}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->setFpsData(Lcom/xiaomi/misettings/display/RefreshRate/j$a;)V

    invoke-virtual {v4, p0}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->setOnSelectedChangedListener(Lcom/xiaomi/misettings/display/RefreshRate/A;)V

    sget-object v5, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->b:Landroid/content/Context;

    invoke-static {v5}, Lcom/xiaomi/misettings/display/b;->b(Landroid/content/Context;)I

    move-result v5

    if-ne v3, v5, :cond_3

    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->setSelected(Z)V

    :cond_3
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->d:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->getValue()I

    move-result v2

    invoke-virtual {v1}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->getValue()I

    move-result v3

    const/4 v4, 0x0

    if-ne v2, v3, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->setSelected(Z)V

    sget-boolean v1, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->a:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x3c

    if-eq v2, v1, :cond_0

    sget-object v1, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v3, :cond_0

    sget-object v1, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->b:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;I)V

    :cond_0
    sget-object v1, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->b:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/display/b;->b(Landroid/content/Context;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v4}, Lcom/xiaomi/misettings/display/RefreshRate/SelectedFpsView;->setSelected(Z)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1    # Landroid/content/res/Configuration;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->f:Landroid/view/View;

    sget v0, Lcom/xiaomi/misettings/display/j;->fps_choose_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/h;->refresh_card_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->f:Landroid/view/View;

    sget v0, Lcom/xiaomi/misettings/display/j;->textView_one:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/h;->refresh_user_tip_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/xiaomi/misettings/display/h;->refresh_user_tip_margin_end:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->f:Landroid/view/View;

    sget v0, Lcom/xiaomi/misettings/display/j;->textView_two:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/h;->refresh_user_tip_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/xiaomi/misettings/display/h;->refresh_user_tip_margin_end:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->f:Landroid/view/View;

    sget v0, Lcom/xiaomi/misettings/display/j;->fps_view_summary:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/xiaomi/misettings/display/h;->fps_view_summary_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    sget-object p1, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->b:Landroid/content/Context;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->b:Landroid/content/Context;

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    sget p3, Lcom/xiaomi/misettings/display/k;->refresh_rate_settings:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->f:Landroid/view/View;

    new-instance p1, Lcom/xiaomi/misettings/display/RefreshRate/j;

    sget-object p2, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->b:Landroid/content/Context;

    invoke-direct {p1, p2}, Lcom/xiaomi/misettings/display/RefreshRate/j;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->c:Lcom/xiaomi/misettings/display/RefreshRate/j;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->d:Ljava/util/List;

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->f()V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->f:Landroid/view/View;

    return-object p1
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/OldRefreshRateFragment;->g()V

    return-void
.end method
