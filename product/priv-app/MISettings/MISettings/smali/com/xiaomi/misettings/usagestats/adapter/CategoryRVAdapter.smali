.class public Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$a;

# interfaces
.implements Landroidx/lifecycle/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;",
        ">;",
        "Landroidx/lifecycle/k;"
    }
.end annotation


# instance fields
.field private a:Lcom/xiaomi/misettings/usagestats/f/g;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/o;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/content/Context;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->a:Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->b:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->c:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->d:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->e:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->f:Ljava/util/List;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->g:Landroid/content/Context;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->m:Lcom/xiaomi/misettings/usagestats/f/g;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->a:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/j;->g(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->h:Ljava/util/List;

    return-void
.end method

.method private a(I)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/h;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "key_category_data"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const/4 v1, 0x0

    const-string v2, "key_is_week"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->a:Lcom/xiaomi/misettings/usagestats/f/g;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v1

    iget-wide v1, v1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    const-string v3, "dayBeginTime"

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->g:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/h;->c()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, v0, p1}, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->b(I)V

    return-void
.end method

.method private b(I)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/o;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "key_category_data"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const/4 v1, 0x1

    const-string v2, "key_is_week"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    sget-object v1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->c:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    const-string v2, "weekInfo"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->g:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/o;->c()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, v0, p1}, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->a(I)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->g(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->h:Ljava/util/List;

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;I)V
    .locals 9
    .param p1    # Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->h:Ljava/util/List;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/util/Collection;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->h:Ljava/util/List;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->b:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/h;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    sget-boolean v2, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    const v3, 0x7f1303a9

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    return-void

    :cond_1
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->f:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->c(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->b(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->c()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->d(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/view/View;

    move-result-object v7

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    move v1, v6

    :goto_1
    invoke-virtual {v7, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->a(Z)V

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->b()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->e(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->g:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->b()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_3
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->e(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->g:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_4
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_5

    return-void

    :cond_5
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->c:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->c(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->b(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->c()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->d(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/view/View;

    move-result-object v7

    if-eqz v0, :cond_6

    goto :goto_2

    :cond_6
    move v1, v6

    :goto_2
    invoke-virtual {v7, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->b()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->e(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->g:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->b()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_7
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->e(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->g:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;->f(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/adapter/f;

    invoke-direct {v0, p0, p2}, Lcom/xiaomi/misettings/usagestats/adapter/f;-><init>(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->c:Ljava/util/List;

    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->f:Ljava/util/List;

    return-void
.end method

.method public c(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->e:Ljava/util/List;

    return-void
.end method

.method public d(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->b:Ljava/util/List;

    return-void
.end method

.method public e(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/o;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->d:Ljava/util/List;

    return-void
.end method

.method public getItemCount()I
    .locals 2

    sget-boolean v0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->f:Ljava/util/List;

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->c:Ljava/util/List;

    if-nez v0, :cond_2

    return v1

    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemViewType(I)I

    move-result p1

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->a(Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance p2, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0e002e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;-><init>(Landroid/view/View;)V

    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter$a;

    return-object p1
.end method
