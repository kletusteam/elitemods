.class public Lcom/xiaomi/misettings/usagestats/d/f/b;
.super Ljava/lang/Object;


# direct methods
.method private static a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/d/d/j$a;-><init>()V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->m(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->h(Landroid/content/Context;)I

    move-result p0

    iput p0, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->a:I

    if-eqz v1, :cond_0

    sget-object p0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->a:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    iput-object p0, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->d:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->b:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    iput-object p0, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->d:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    :goto_0
    new-instance p0, Lcom/xiaomi/misettings/usagestats/d/d/j;

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/j;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object p0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 4

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/d/d/f$a;-><init>()V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->a:Z

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v2

    iput v2, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->d:I

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result p0

    iput p0, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->c:I

    if-eqz v1, :cond_1

    const-string p0, "disallow_limit_app"

    invoke-static {p1, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    sget-object p0, Lcom/xiaomi/misettings/usagestats/d/d/f$b;->a:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    iput-object p0, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->b:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/d/d/f$b;->b:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    iput-object p0, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->b:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    goto :goto_0

    :cond_1
    sget-object p0, Lcom/xiaomi/misettings/usagestats/d/d/f$b;->c:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    iput-object p0, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->b:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    :goto_0
    new-instance p0, Lcom/xiaomi/misettings/usagestats/d/d/f;

    invoke-direct {p0, v3}, Lcom/xiaomi/misettings/usagestats/d/d/f;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object p0
.end method

.method private static a(Lcom/xiaomi/misettings/usagestats/f/m;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/b;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/b;-><init>(I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/m;->h()Ljava/util/List;

    move-result-object p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/k;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/k;->a()Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public static a(Ljava/util/List;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ">;)",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;"
        }
    .end annotation

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/e;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/e;-><init>(I)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/d/d/e$a;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/f/m;->i()Ljava/util/List;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/f/m;->a()Ljava/util/List;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/f/m;->g()Ljava/util/List;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-direct {v7}, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;-><init>()V

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/f/m;->f()J

    move-result-wide v8

    iput-wide v8, v7, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/f/m;->d()J

    move-result-wide v8

    iput-wide v8, v7, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/f/m;->e()I

    move-result v6

    iput v6, v7, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->a:I

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iput-object v3, v1, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->e:Ljava/util/List;

    iput-object v2, v1, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->d:Ljava/util/List;

    iput-object v4, v1, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->f:Ljava/util/List;

    iput-object v5, v1, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->c:Ljava/util/List;

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public static a(Lcom/xiaomi/misettings/usagestats/f/g;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p0

    invoke-virtual {p0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {v2, p1}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/xiaomi/misettings/usagestats/f/d;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_0

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/m;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Lcom/xiaomi/misettings/usagestats/f/m;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0, p2}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/b;->b(Lcom/xiaomi/misettings/usagestats/f/m;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p2, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/4 v1, 0x7

    invoke-direct {p2, v1}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/b;->c(Lcom/xiaomi/misettings/usagestats/f/m;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/f/b;->b(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/4 p1, 0x5

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/f/b;->d(Ljava/util/List;)V

    new-instance p0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/16 p1, 0xb

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/b;->b(Ljava/util/List;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Ljava/util/List;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/4 v2, 0x7

    invoke-direct {v1, v2}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/b;->c(Ljava/util/List;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/f/b;->b(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/4 p1, 0x5

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/f/b;->d(Ljava/util/List;)V

    new-instance p0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    const/16 p1, 0xb

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;-><init>(I)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private static a()Z
    .locals 1

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private static b(Lcom/xiaomi/misettings/usagestats/f/m;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ")",
            "Lcom/xiaomi/misettings/usagestats/d/d/i<",
            "Lcom/xiaomi/misettings/usagestats/d/d/d$a;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/d;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/d;-><init>(I)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/d/d$a;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/d/d/d$a;-><init>()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/m;->h()Ljava/util/List;

    move-result-object p0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/misettings/usagestats/f/k;

    invoke-virtual {v5}, Lcom/xiaomi/misettings/usagestats/f/k;->a()Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Lcom/xiaomi/misettings/usagestats/f/k;->b()Ljava/util/List;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Lcom/xiaomi/misettings/usagestats/f/k;->a()Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v5}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iput-object v2, v1, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->c:Ljava/util/List;

    iput-object v3, v1, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->d:Ljava/util/List;

    iput-object v4, v1, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->e:Ljava/util/List;

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private static b(Ljava/util/List;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ">;)",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;"
        }
    .end annotation

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/c;-><init>(I)V

    iput-object p0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->n(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/i/h;->b()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object p0

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static c(Lcom/xiaomi/misettings/usagestats/f/m;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/g;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/g;-><init>(I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/m;->h()Ljava/util/List;

    move-result-object p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/k;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/k;->c()Lcom/xiaomi/misettings/usagestats/f/i;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private static c(Ljava/util/List;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ">;)",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;"
        }
    .end annotation

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/h;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/d/d/h;-><init>(I)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    new-instance v4, Lcom/xiaomi/misettings/usagestats/d/d/h$a;

    invoke-direct {v4}, Lcom/xiaomi/misettings/usagestats/d/d/h$a;-><init>()V

    new-instance v5, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-direct {v5}, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;-><init>()V

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/f/m;->h()Ljava/util/List;

    move-result-object v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move v9, v2

    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_0

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/xiaomi/misettings/usagestats/f/k;

    invoke-virtual {v10}, Lcom/xiaomi/misettings/usagestats/f/k;->c()Lcom/xiaomi/misettings/usagestats/f/i;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/f/m;->f()J

    move-result-wide v9

    iput-wide v9, v5, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/f/m;->d()J

    move-result-wide v9

    iput-wide v9, v5, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/f/m;->e()I

    move-result v7

    iput v7, v5, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->a:I

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/f/m;->b()I

    move-result v6

    iput v6, v4, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->a:I

    iput-object v8, v4, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->d:Ljava/util/List;

    iput-object v5, v4, Lcom/xiaomi/misettings/usagestats/d/d/h$a;->e:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private static d(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
