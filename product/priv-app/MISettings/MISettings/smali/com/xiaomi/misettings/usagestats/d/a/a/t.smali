.class public Lcom/xiaomi/misettings/usagestats/d/a/a/t;
.super Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/d/c/a;


# instance fields
.field private c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

.field private d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

.field private e:Lcom/xiaomi/misettings/usagestats/d/a/a;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/LinearLayout;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const p1, 0x7f0b0173

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const p1, 0x7f0b0172

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const p1, 0x7f0b020d

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->f:Landroid/widget/TextView;

    const p1, 0x7f0b0200

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->g:Landroid/widget/TextView;

    const p1, 0x7f0b0202

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->h:Landroid/widget/TextView;

    const p1, 0x7f0b0201

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->i:Landroid/widget/LinearLayout;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const/4 p2, 0x7

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setBarType(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setBarType(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {p1, p0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setOnItemClickListener(Lcom/xiaomi/misettings/usagestats/d/c/a;)V

    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object p1

    check-cast p1, Ljava/text/SimpleDateFormat;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->l:Ljava/text/SimpleDateFormat;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->l:Ljava/text/SimpleDateFormat;

    const-string p2, "M.d"

    invoke-virtual {p1, p2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/f/g;I)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->l:Ljava/text/SimpleDateFormat;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v2

    iget-wide v2, v2, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-lez p2, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->f()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->j:Ljava/util/List;

    add-int/lit8 p2, p2, -0x1

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v0

    :goto_0
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide p1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->h:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {v3, p1, p2, v0, v1}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->h:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private b(Lcom/xiaomi/misettings/usagestats/f/g;I)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setDayUsageStat(Lcom/xiaomi/misettings/usagestats/f/g;Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/misettings/common/utils/q;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->a(Lcom/xiaomi/misettings/usagestats/f/g;I)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    if-gez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->e:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->e()Lcom/xiaomi/misettings/usagestats/d/a/b;

    move-result-object v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->k:I

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->e:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->e()Lcom/xiaomi/misettings/usagestats/d/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/b;->a(I)V

    :cond_1
    return-void

    :cond_2
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->k:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->j:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt p1, v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-direct {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->b(Lcom/xiaomi/misettings/usagestats/f/g;I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->b()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->e:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(I)V

    :cond_3
    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/a/a;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->e:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iput p4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->k:I

    move-object p1, p2

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/d/b;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->j:Ljava/util/List;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->j:Ljava/util/List;

    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/g;

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->j:Ljava/util/List;

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setWeekUsageStat(Ljava/util/List;Z)V

    iget-boolean p2, p2, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->c:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setInterceptSwitchIndex()V

    :cond_0
    invoke-direct {p0, p1, p4}, Lcom/xiaomi/misettings/usagestats/d/a/a/t;->b(Lcom/xiaomi/misettings/usagestats/f/g;I)V

    return-void
.end method
