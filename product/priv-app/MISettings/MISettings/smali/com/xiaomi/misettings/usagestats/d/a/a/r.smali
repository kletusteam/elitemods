.class public abstract Lcom/xiaomi/misettings/usagestats/d/a/a/r;
.super Lcom/xiaomi/misettings/usagestats/d/a/a/s;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/d/a/a$a;


# instance fields
.field protected c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field protected d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/a/a/v;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/graphics/Rect;

.field protected f:Landroid/widget/TextView;

.field protected g:Landroid/widget/TextView;

.field protected h:Landroid/widget/LinearLayout;

.field protected i:Landroid/view/View;

.field protected j:Landroid/view/View;

.field protected k:Z

.field protected l:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;-><init>(Landroid/content/Context;Landroid/view/View;)V

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->e:Landroid/graphics/Rect;

    const p1, 0x7f0b01ba

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->h:Landroid/widget/LinearLayout;

    const p1, 0x7f0b020d

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->f:Landroid/widget/TextView;

    const p1, 0x7f0b0180

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->g:Landroid/widget/TextView;

    const p1, 0x7f0b01f5

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->i:Landroid/view/View;

    const p1, 0x7f0b017e

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->j:Landroid/view/View;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->j:Landroid/view/View;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/view/View;)V

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x1c

    if-ge p1, p2, :cond_0

    const p1, 0x7f0b0395

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->i:Landroid/view/View;

    const p2, 0x7f0805ba

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    new-instance p1, Landroid/util/SparseArray;

    const/4 p2, 0x5

    invoke-direct {p1, p2}, Landroid/util/SparseArray;-><init>(I)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->c:Landroid/util/SparseArray;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d:Ljava/util/List;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/d/a/a/v;

    iget-object v0, p2, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p2, p2, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->d()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected a(Z)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "misettings.action.EXCHANGE_DETAIL_LIST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, ":key:notify_channel"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {p1}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object p1

    invoke-virtual {p1, v0}, La/m/a/b;->a(Landroid/content/Intent;)Z

    return-void
.end method

.method protected b()Z
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v1, "default_category"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected c()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0b0213

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    const v3, 0x7f0b0212

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->c:Landroid/util/SparseArray;

    invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected d(I)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->i:Landroid/view/View;

    const/4 v1, 0x5

    if-le p1, v1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
