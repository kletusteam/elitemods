.class public Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;
.super Lcom/misettings/common/base/BaseActivity;


# instance fields
.field private a:Lcom/airbnb/lottie/LottieAnimationView;

.field private b:Landroid/os/Handler;

.field private c:Landroid/os/Handler;

.field private d:I

.field private e:Z

.field private f:Landroid/view/View;

.field private g:F

.field private h:Landroid/content/BroadcastReceiver;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->d:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->e:Z

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/c/u;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->i:Z

    return-void
.end method

.method private a()V
    .locals 4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->g:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v0

    const v0, 0x459c4000    # 5000.0f

    mul-float/2addr v1, v0

    float-to-long v0, v1

    const-wide/16 v2, 0x3e8

    rem-long v2, v0, v2

    sub-long/2addr v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dismissCancelBtn: delayTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "FocusModeTimingActivity"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->b:Landroid/os/Handler;

    new-instance v3, Lcom/xiaomi/misettings/usagestats/focusmode/u;

    invoke-direct {v3, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/u;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;)V

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->k()V

    return-void
.end method

.method private b()V
    .locals 3

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ensureStartFocusMode: topActivity="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "FocusModeTimingActivity"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/land/FocusModeTimingLandActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/port/FocusModeTimingPortActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->i()V

    :cond_2
    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->d()V

    return-void
.end method

.method private c()V
    .locals 2

    const v0, 0x7f0b018d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    const v0, 0x7f0b0177

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->f:Landroid/view/View;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/view/View;)V

    invoke-static {p0}, Lcom/misettings/common/utils/m;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0x348

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    const/16 v1, 0x8c

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->i()V

    return-void
.end method

.method private d()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    const v0, 0x7f010012

    const v1, 0x7f010013

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->g()V

    return-void
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->f:Landroid/view/View;

    return-object p0
.end method

.method private e()Ljava/lang/String;
    .locals 2

    const-class v0, Landroid/app/ActivityManager;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->h:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/v;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/v;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->h:Landroid/content/BroadcastReceiver;

    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->b()V

    return-void
.end method

.method private g()V
    .locals 5

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080089

    goto :goto_0

    :cond_0
    const v0, 0x7f080088

    :goto_0
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v2, 0x2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-static {p0}, Lcom/misettings/common/utils/f;->c(Landroid/content/Context;)Lb/c/a/a/b;

    move-result-object v2

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget v2, v2, Lb/c/a/a/b;->d:I

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/misettings/common/utils/f;->d(Landroid/content/Context;)I

    move-result v4

    invoke-static {v1, v2, v4}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "FocusModeTimingActivity"

    const-string v3, "setFocusBg: exception"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_1
    return-void
.end method

.method private h()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->f()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->f:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/q;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/q;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/r;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/r;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;)V

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->a(Landroid/animation/Animator$AnimatorListener;)V

    :cond_0
    return-void
.end method

.method private i()V
    .locals 7

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->j(Landroid/content/Context;)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->d:I

    const/4 v1, 0x1

    const/16 v2, -0x64

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v2, "settings_experience_count"

    invoke-virtual {v0, v2}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v3

    add-int/2addr v0, v1

    invoke-virtual {v3, v2, v0}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;I)V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->d:I

    invoke-static {v0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->c(Landroid/content/Context;I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b(Landroid/content/Context;I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    int-to-long v3, v0

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v3, v5

    invoke-static {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;J)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->b(Landroid/content/Context;J)V

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "settings_focus_mode_status"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->c(Landroid/content/Context;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/port/FocusModePortActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0}, Lcom/misettings/common/utils/n;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/land/FocusModeLandActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :cond_1
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method

.method private j()V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "startTiming: current progress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->g:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FocusModeTimingActivity"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->g:F

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setProgress(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->d()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->i()V

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->e:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->c:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/s;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/s;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void
.end method

.method private k()V
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->i:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->g:F

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v1}, Lcom/airbnb/lottie/LottieAnimationView;->getProgress()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->getProgress()F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->g:F

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->c()V

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "stopTiming: current progress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->g:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FocusModeTimingActivity"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->b:Landroid/os/Handler;

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method private l()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->h:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/Window;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x1302

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :try_start_0
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p0}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->d()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0e0147

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a(Landroid/view/Window;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->b:Landroid/os/Handler;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->c:Landroid/os/Handler;

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->g:F

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    const-string v1, "keyFocusModeTimeIndex"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->d:I

    :cond_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->d:I

    const/16 v0, 0xb4

    if-le p1, v0, :cond_1

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->c()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->h()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->k()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->i:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->a()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->e()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->f()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->g()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->c:Landroid/os/Handler;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->l()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz p1, :cond_0

    const v0, 0x7f1300f6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->a:Lcom/airbnb/lottie/LottieAnimationView;

    const v0, 0x7f1300f5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStart()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->j()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStop()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeTimingActivityBase;->k()V

    return-void
.end method
