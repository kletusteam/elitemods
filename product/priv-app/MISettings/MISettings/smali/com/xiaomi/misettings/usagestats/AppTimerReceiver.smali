.class public Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;->a:Z

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    const-string v0, "AppTimerReceiver"

    if-nez p1, :cond_0

    const-string p1, "context is null"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "renoir"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;I)V

    const-string p1, "Anti has been closed on screen change"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 6

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "time_set_by_settings"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/b;->a()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/f/l;->b()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/d/c;->b()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    const-wide/16 v4, 0x1d

    mul-long/2addr v2, v4

    sub-long/2addr v0, v2

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Landroid/content/Context;J)V

    :cond_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->l(Landroid/content/Context;)V

    return-void
.end method

.method private b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive: action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AppTimerReceiver"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const-string v3, "miui.android.intent.action.SCREEN_ON"

    if-nez v1, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a;->a(Landroid/content/Context;)V

    :cond_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1a

    if-ge v1, v4, :cond_2

    return-void

    :cond_2
    const-string v1, "miui.intent.action.settings.SCHEDULE_DEVICE_USAGE_MONITOR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v1, :cond_4

    const-string v0, "Receive ACTION_DEVICE_USAGE_MONITOR!!!"

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "key_modify_notification_text"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    sub-long/2addr v0, v2

    invoke-static {p1, v0, v1, v4}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->a(Landroid/content/Context;JZ)V

    goto/16 :goto_1

    :cond_3
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->e(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_4
    const-string v1, "miui.intent.action.settings.SCHEDULE_APP_LIMIT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string p2, "Receive ACTION_APP_LIMIT_INIT!!!"

    invoke-static {v2, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, v4}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Z)V

    goto/16 :goto_1

    :cond_5
    const-string v1, "miui.intent.action.settings.SCHEDULE_PROLONG_LIMIT_TIME"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "pkgName"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "remainTime"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-wide/16 v2, 0x0

    const-string v4, "showNotificationTime"

    invoke-virtual {p2, v4, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {p1, v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Ljava/lang/String;IJ)V

    goto/16 :goto_1

    :cond_6
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;->b(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_7
    const-string v1, "miui.settings.action.PRELOAD_YESTERDAY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/content/Context;)V

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object p2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/l;

    invoke-direct {v0, p0, p1}, Lcom/xiaomi/misettings/usagestats/l;-><init>(Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    :cond_8
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;->d(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object p2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/m;

    invoke-direct {v0, p0, p1}, Lcom/xiaomi/misettings/usagestats/m;-><init>(Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    :cond_9
    const-string v1, "android.intent.action.LOCKED_BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;->d(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_a
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    goto/16 :goto_0

    :cond_b
    const-string p2, "misettings.action.FOCUS_MODE_SHARE"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_c

    invoke-static {}, Lcom/misettings/common/utils/n;->b()Z

    move-result p2

    if-eqz p2, :cond_12

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;->c(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_c
    const-string p2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_d

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object p2

    sput-object p2, Lb/e/b/e;->i:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/xiaomi/misettings/display/RefreshRate/f;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/display/RefreshRate/f;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/xiaomi/misettings/display/RefreshRate/f;->g(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/l;->a()Lcom/xiaomi/misettings/usagestats/utils/l;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/xiaomi/misettings/usagestats/utils/l;->f(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/misettings/display/RefreshRate/e;->a()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/m;->a()V

    const p2, 0x7f1303d4

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/g/a;->c(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_12

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->m(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->l(Landroid/content/Context;)V

    goto :goto_1

    :cond_d
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_e

    const-string p2, "handleIntent: SCREEN_ON"

    invoke-static {v2, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->e(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;->a(Landroid/content/Context;)V

    goto :goto_1

    :cond_e
    const-string p2, "miui.android.intent.action.SCREEN_OFF"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_f

    const-string p2, "handleIntent: SCREEN_OFF"

    invoke-static {v2, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;->a(Landroid/content/Context;)V

    goto :goto_1

    :cond_f
    const-string p2, "miui.settings.action.NOTIFY"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_10

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/i/h;->t(Landroid/content/Context;)V

    goto :goto_1

    :cond_10
    const-string p1, "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_12

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/greenguard/manager/a/g;->a()V

    goto :goto_1

    :cond_11
    :goto_0
    invoke-static {p1, p2}, Lcom/miui/greenguard/receiver/f;->a(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_12
    :goto_1
    return-void
.end method

.method private c(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeFinishActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private d(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tryRecoverFocusMode"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppTimerReceiver"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;->a:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->i(Landroid/content/Context;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;->b(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d;

    invoke-direct {v1, p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d;-><init>(Lcom/xiaomi/misettings/usagestats/AppTimerReceiver;Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method
