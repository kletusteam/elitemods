.class public Lcom/xiaomi/misettings/usagestats/focusmode/c/e;
.super Ljava/lang/Object;


# direct methods
.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData$RecordDetail;",
            ">;)",
            "Ljava/util/List<",
            "Lb/c/b/a/a;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy:MM:dd"

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const-string v4, ""

    const/4 v5, 0x0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData$RecordDetail;

    iget-wide v7, v6, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData$RecordDetail;->starttime:J

    iget-wide v9, v6, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData$RecordDetail;->duration:J

    add-long v11, v7, v9

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v13, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;

    invoke-direct {v13}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;-><init>()V

    invoke-virtual {v13, v7, v8}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;->setStartTime(J)V

    invoke-virtual {v13, v11, v12}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;->setEndTime(J)V

    sget-wide v11, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    div-long/2addr v9, v11

    long-to-int v9, v9

    invoke-virtual {v13, v9}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;->setTotalTime(I)V

    invoke-virtual {v13, v7, v8}, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->setDate(J)V

    invoke-static {v6, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-static {v0, v5}, Lcom/xiaomi/misettings/usagestats/focusmode/c/e;->a(Ljava/util/List;Ljava/util/List;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;

    invoke-direct {v5}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;-><init>()V

    invoke-static {v2, v3, v7, v8}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v9

    invoke-virtual {v5, v9}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDateData;->setToday(Z)V

    invoke-virtual {v5, v7, v8}, Lcom/xiaomi/misettings/usagestats/focusmode/model/BaseFocusData;->setDate(J)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v5, v4

    move-object v4, v6

    goto :goto_0

    :cond_1
    if-eqz v5, :cond_0

    invoke-interface {v5, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-static {v0, v5}, Lcom/xiaomi/misettings/usagestats/focusmode/c/e;->a(Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method private static a(IILb/c/b/c/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lb/c/b/c/h<",
            "Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const-string v1, "pageNum"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const-string p1, "pageSize"

    invoke-interface {v0, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p0, Lb/c/b/c/g;

    sget-object p1, Lb/c/b/c/a$a;->a:Lb/c/b/c/a$a;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/c/d;

    invoke-direct {v1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/d;-><init>(Lb/c/b/c/h;)V

    const-string p2, "/focus/records"

    invoke-direct {p0, p2, v0, p1, v1}, Lb/c/b/c/g;-><init>(Ljava/lang/String;Ljava/util/Map;Lb/c/b/c/a$a;Lb/c/b/c/h;)V

    invoke-virtual {p0}, Lb/c/b/c/g;->a()V

    return-void
.end method

.method public static a(Landroid/content/Context;IILb/c/b/c/h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Lb/c/b/c/h<",
            "Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHistoryData;",
            ">;)V"
        }
    .end annotation

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/e;->a(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/focusmode/c/e;->a(IILb/c/b/c/h;)V

    goto :goto_0

    :cond_0
    invoke-static {p3}, Lcom/xiaomi/misettings/usagestats/focusmode/c/e;->b(Lb/c/b/c/h;)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lb/c/b/c/h;)V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/e;->b(Lb/c/b/c/h;)V

    return-void
.end method

.method private static a(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb/c/b/a/a;",
            ">;",
            "Ljava/util/List<",
            "Lb/c/b/a/a;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 4

    invoke-static {p0}, Lcom/misettings/common/utils/f;->e(Landroid/content/Context;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/misettings/common/utils/o;->a(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static b(Lb/c/b/c/h;)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lb/c/b/c/h;->a()V

    :cond_0
    return-void
.end method
