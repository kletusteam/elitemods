.class public Lcom/xiaomi/misettings/usagestats/PadAuthorizationActivity;
.super Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a()V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Landroid/app/Activity;->overridePendingTransition(II)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/PadAuthorizationActivity;->finish()V

    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 4

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/v;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/v;-><init>(Lcom/xiaomi/misettings/usagestats/PadAuthorizationActivity;Landroid/os/Bundle;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public finish()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method
