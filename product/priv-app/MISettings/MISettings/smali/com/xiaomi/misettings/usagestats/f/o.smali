.class public Lcom/xiaomi/misettings/usagestats/f/o;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable<",
        "Lcom/xiaomi/misettings/usagestats/f/o;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/Long;

.field private b:I

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->a:Ljava/lang/Long;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/n;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/f/n;-><init>(Lcom/xiaomi/misettings/usagestats/f/o;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/usagestats/f/o;)I
    .locals 1

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/f/o;->a:Ljava/lang/Long;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->a:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result p1

    return p1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->f:Ljava/lang/String;

    return-object v0
.end method

.method public a(J)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->a:Ljava/lang/Long;

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->b:I

    int-to-long v0, v0

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    div-long/2addr p1, v2

    add-long/2addr v0, p1

    long-to-int p1, v0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/f/o;->b:I

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/f/h;I)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/o;->f:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->e:Ljava/lang/String;

    return-object v0
.end method

.method public b(J)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->a:Ljava/lang/Long;

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->b:I

    int-to-long v0, v0

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    div-long/2addr p1, v2

    add-long/2addr v0, p1

    long-to-int p1, v0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/f/o;->b:I

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/o;->e:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/o;->d:Ljava/lang/String;

    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/o;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/f/o;->a(Lcom/xiaomi/misettings/usagestats/f/o;)I

    move-result p1

    return p1
.end method

.method public d()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public e()J
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/o;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method
