.class public Lcom/xiaomi/misettings/usagestats/a/i;
.super Ljava/lang/Object;


# static fields
.field public static a:Lcom/xiaomi/misettings/usagestats/a/i;

.field private static b:Lcom/xiaomi/misettings/usagestats/a/h;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/U;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/a/i;->b(Landroid/content/Context;)I

    move-result p1

    const/4 v1, 0x1

    const-wide/32 v2, 0xa00000

    invoke-static {v0, p1, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/a/h;->a(Ljava/io/File;IIJ)Lcom/xiaomi/misettings/usagestats/a/h;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "DiskLruCacheUtils"

    const-string v1, "DiskLruCacheUtils: openLruCacheError"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/i;
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/a/i;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/i;->a:Lcom/xiaomi/misettings/usagestats/a/i;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/a/i;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/a/i;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/a/i;->a:Lcom/xiaomi/misettings/usagestats/a/i;

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/a/i;->a:Lcom/xiaomi/misettings/usagestats/a/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private b(Landroid/content/Context;)I
    .locals 2

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p1

    iget p1, p1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    const-string v0, "DiskLruCacheUtils"

    const-string v1, "Cannot find package: "

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 p1, 0x1

    return p1
.end method

.method public static b()Z
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/a/i;->a:Lcom/xiaomi/misettings/usagestats/a/i;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/a/h$a;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/a/h;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    invoke-virtual {v1, p1}, Lcom/xiaomi/misettings/usagestats/a/h;->e(Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/a/h$a;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v2, "DiskLruCacheUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "the entry spcified key:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is editing by other . "

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-object v1

    :cond_2
    :goto_0
    return-object v0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    return-object v0
.end method

.method public a()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/xiaomi/misettings/usagestats/a/i;->a:Lcom/xiaomi/misettings/usagestats/a/i;

    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/a/h;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/a/h;->close()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/a/i;->a(Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/a/h$a;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez p1, :cond_0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/io/Closeable;)V

    return-void

    :cond_0
    :try_start_1
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/xiaomi/misettings/usagestats/a/h$a;->a(I)Ljava/io/OutputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v1, p2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/a/h$a;->b()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/io/Closeable;)V

    goto :goto_4

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_5

    :catch_0
    move-exception p2

    goto :goto_0

    :catch_1
    move-exception p2

    :goto_0
    move-object v0, v1

    goto :goto_2

    :catch_2
    move-exception p2

    goto :goto_2

    :catch_3
    move-exception p2

    goto :goto_2

    :catchall_1
    move-exception p1

    goto :goto_5

    :catch_4
    move-exception p2

    goto :goto_1

    :catch_5
    move-exception p2

    :goto_1
    move-object p1, v0

    :goto_2
    :try_start_3
    const-string v1, "DiskLruCacheUtils"

    const-string v2, "putString: putStringError"

    invoke-static {v1, v2, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz p1, :cond_1

    :try_start_4
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/a/h$a;->a()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    :catch_6
    move-exception p1

    :try_start_5
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_1
    :goto_3
    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/io/Closeable;)V

    :goto_4
    return-void

    :goto_5
    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/io/Closeable;)V

    throw p1
.end method

.method public b(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/a/h;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    invoke-virtual {v1, p1}, Lcom/xiaomi/misettings/usagestats/a/h;->f(Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/a/h$c;

    move-result-object p1

    if-nez p1, :cond_1

    const-string p1, "DiskLruCacheUtils"

    const-string v1, "not find entry , or entry.readable = false"

    invoke-static {p1, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/xiaomi/misettings/usagestats/a/h$c;->b(I)Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_2
    :goto_0
    return-object v0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    return-object v0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, "DiskLruCacheUtils"

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/a/i;->b(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    return-object v1

    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/InputStreamReader;

    sget-object v3, Lcom/xiaomi/misettings/usagestats/a/r;->b:Ljava/nio/charset/Charset;

    invoke-direct {v2, p1, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/a/r;->a(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString: readFroDiskSuccess"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v2

    :try_start_1
    const-string v3, "getString: readFroDiskFail"

    invoke-static {v0, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/io/Closeable;)V

    return-object v1

    :goto_1
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/a/h;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/a/i;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/a/h;->g(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method
