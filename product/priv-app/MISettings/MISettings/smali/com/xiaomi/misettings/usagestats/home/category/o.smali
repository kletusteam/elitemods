.class Lcom/xiaomi/misettings/usagestats/home/category/o;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/home/category/p;->b(Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

.field final synthetic b:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

.field final synthetic c:Lcom/xiaomi/misettings/usagestats/home/category/p;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/category/p;Lcom/xiaomi/misettings/usagestats/home/category/c/b;Lcom/xiaomi/misettings/usagestats/home/category/c/b;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->c:Lcom/xiaomi/misettings/usagestats/home/category/p;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->b:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->c:Lcom/xiaomi/misettings/usagestats/home/category/p;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/p;Lcom/xiaomi/misettings/usagestats/home/category/c/b;)I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->c:Lcom/xiaomi/misettings/usagestats/home/category/p;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/p;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget v2, v1, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->g:I

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    iput v2, v1, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->g:I

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->j:Ljava/util/List;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    check-cast v2, Lcom/xiaomi/misettings/usagestats/home/category/c/g;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->c:Lcom/xiaomi/misettings/usagestats/home/category/p;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->b:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    check-cast v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    iput-object v2, v1, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->j:Ljava/util/List;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->j:Ljava/util/List;

    :cond_0
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->j:Ljava/util/List;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    check-cast v2, Lcom/xiaomi/misettings/usagestats/home/category/c/g;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->g:I

    add-int/2addr v1, v3

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->g:I

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->j:Ljava/util/List;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/home/category/n;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/home/category/n;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/o;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->c:Lcom/xiaomi/misettings/usagestats/home/category/p;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->b:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/p;Lcom/xiaomi/misettings/usagestats/home/category/c/b;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    iput-boolean v3, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->c:Lcom/xiaomi/misettings/usagestats/home/category/p;

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/c/h;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->c:Lcom/xiaomi/misettings/usagestats/home/category/p;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->b:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/p;Lcom/xiaomi/misettings/usagestats/home/category/c/b;)I

    move-result v1

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->j:Ljava/util/List;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    invoke-interface {v2, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v3

    if-ltz v1, :cond_1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->c:Lcom/xiaomi/misettings/usagestats/home/category/p;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/p;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->g:I

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->c:Lcom/xiaomi/misettings/usagestats/home/category/p;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/home/category/p;->b(Lcom/xiaomi/misettings/usagestats/home/category/p;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "run: scrolled position:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ClassifyManagerAdapter"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    instance-of v1, v1, Lcom/xiaomi/misettings/usagestats/home/category/c/g;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->c:Lcom/xiaomi/misettings/usagestats/home/category/p;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->c(Lcom/xiaomi/misettings/usagestats/home/category/p;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/o;->a:Lcom/xiaomi/misettings/usagestats/home/category/c/b;

    check-cast v2, Lcom/xiaomi/misettings/usagestats/home/category/c/g;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/xiaomi/misettings/usagestats/home/category/d/f;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/home/category/c/g;Ljava/lang/String;)V

    :cond_2
    return-void
.end method
