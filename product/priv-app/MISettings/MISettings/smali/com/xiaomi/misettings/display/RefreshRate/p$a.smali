.class public Lcom/xiaomi/misettings/display/RefreshRate/p$a;
.super Lcom/xiaomi/misettings/display/RefreshRate/p$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/display/RefreshRate/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/display/RefreshRate/p$c<",
        "Lcom/xiaomi/misettings/display/RefreshRate/m;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Lcom/xiaomi/misettings/display/RefreshRate/m;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/TextView;

.field private e:Lmiuix/slidingwidget/widget/SlidingButton;

.field private f:Landroid/view/View;

.field final synthetic g:Lcom/xiaomi/misettings/display/RefreshRate/p;


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/display/RefreshRate/p;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->g:Lcom/xiaomi/misettings/display/RefreshRate/p;

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/display/RefreshRate/p$c;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/p;Landroid/view/View;)V

    iput-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->f:Landroid/view/View;

    sget p1, Lcom/xiaomi/misettings/display/j;->id_item_icon:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->c:Landroid/widget/ImageView;

    sget p1, Lcom/xiaomi/misettings/display/j;->id_item_name:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->d:Landroid/widget/TextView;

    sget p1, Lcom/xiaomi/misettings/display/j;->id_is_open_no_limit:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->e:Lmiuix/slidingwidget/widget/SlidingButton;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)Lcom/xiaomi/misettings/display/RefreshRate/m;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->b:Lcom/xiaomi/misettings/display/RefreshRate/m;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->e:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->f:Landroid/view/View;

    return-object p0
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/display/RefreshRate/m;I)V
    .locals 0

    iput-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->b:Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->f:Landroid/view/View;

    new-instance p2, Lcom/xiaomi/misettings/display/RefreshRate/n;

    invoke-direct {p2, p0, p3}, Lcom/xiaomi/misettings/display/RefreshRate/n;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/p$a;I)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->e:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance p2, Lcom/xiaomi/misettings/display/RefreshRate/o;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/display/RefreshRate/o;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/p$a;)V

    invoke-virtual {p1, p2}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->b:Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object p1, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->c:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->g:Lcom/xiaomi/misettings/display/RefreshRate/p;

    invoke-static {p1}, Lcom/xiaomi/misettings/display/RefreshRate/p;->b(Lcom/xiaomi/misettings/display/RefreshRate/p;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->b:Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object p2, p2, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/xiaomi/misettings/display/RefreshRate/e;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->b:Lcom/xiaomi/misettings/display/RefreshRate/m;

    iput-object p1, p2, Lcom/xiaomi/misettings/display/RefreshRate/m;->c:Ljava/lang/String;

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->b:Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object p2, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->b:Landroid/graphics/Bitmap;

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->g:Lcom/xiaomi/misettings/display/RefreshRate/p;

    invoke-static {p2}, Lcom/xiaomi/misettings/display/RefreshRate/p;->b(Lcom/xiaomi/misettings/display/RefreshRate/p;)Landroid/content/Context;

    move-result-object p2

    iget-object p3, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->b:Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object p3, p3, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    invoke-static {p2, p3}, Lcom/xiaomi/misettings/display/RefreshRate/e;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p2

    iput-object p2, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->b:Landroid/graphics/Bitmap;

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->c:Landroid/widget/ImageView;

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->b:Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object p2, p2, Lcom/xiaomi/misettings/display/RefreshRate/m;->b:Landroid/graphics/Bitmap;

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->d:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->b:Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object p2, p2, Lcom/xiaomi/misettings/display/RefreshRate/m;->c:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->e:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->b:Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-boolean p2, p2, Lcom/xiaomi/misettings/display/RefreshRate/m;->e:Z

    invoke-virtual {p1, p2}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    return-void
.end method

.method public bridge synthetic a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V
    .locals 0

    check-cast p2, Lcom/xiaomi/misettings/display/RefreshRate/m;

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/display/RefreshRate/p$a;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/display/RefreshRate/m;I)V

    return-void
.end method
