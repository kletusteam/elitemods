.class Lcom/xiaomi/misettings/usagestats/b/a/o;
.super Ljava/lang/Object;

# interfaces
.implements Lb/c/b/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/b/a/p;->a(Landroid/content/Context;Ljava/util/List;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/c/b/c/h<",
        "Lb/c/b/a/a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Z

.field final synthetic d:Lcom/xiaomi/misettings/usagestats/b/a/p;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/b/a/p;JLandroid/content/Context;Z)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/b/a/o;->d:Lcom/xiaomi/misettings/usagestats/b/a/p;

    iput-wide p2, p0, Lcom/xiaomi/misettings/usagestats/b/a/o;->a:J

    iput-object p4, p0, Lcom/xiaomi/misettings/usagestats/b/a/o;->b:Landroid/content/Context;

    iput-boolean p5, p0, Lcom/xiaomi/misettings/usagestats/b/a/o;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const-string v0, "NetCategoryUtils"

    const-string v1, "fail: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public a(Lb/c/b/a/a;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lb/c/b/a/a;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/o;->a(Lb/c/b/a/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSuccess: duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/b/a/o;->a:J

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetCategoryUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSuccess: result:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-class v0, Lcom/xiaomi/misettings/usagestats/b/a/i;

    invoke-static {p1, v0}, Lcom/misettings/common/utils/h;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/b/a/i;

    if-eqz p1, :cond_0

    iget v0, p1, Lb/c/b/a/a;->code:I

    if-nez v0, :cond_0

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/b/a/n;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/n;-><init>(Lcom/xiaomi/misettings/usagestats/b/a/o;Lcom/xiaomi/misettings/usagestats/b/a/i;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
