.class public Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;
.super Landroidx/preference/PreferenceCategory;

# interfaces
.implements Lcom/xiaomi/misettings/display/s;


# instance fields
.field private j:I

.field private k:Lcom/xiaomi/misettings/display/s;

.field l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/xiaomi/misettings/display/ExpertRadioPreference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroidx/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->j:I

    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->l:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroidx/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->j:I

    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->l:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroidx/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->j:I

    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->l:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroidx/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->j:I

    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->l:Ljava/util/Map;

    return-void
.end method

.method private a(Lcom/xiaomi/misettings/display/ExpertRadioPreference;)V
    .locals 5

    invoke-virtual {p1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/display/ExpertRadioPreference;

    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->j:I

    invoke-static {v3, v4, v1}, Lcom/xiaomi/misettings/display/a/c;->a(Landroid/content/Context;II)V

    invoke-virtual {v2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "primary_color"

    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "color_space_mode"

    invoke-static {v2, v3, v1}, Landroid/provider/MiuiSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private i()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/display/ExpertRadioPreference;

    invoke-virtual {v2}, Landroidx/preference/Preference;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/preference/Preference;

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->e(Landroidx/preference/Preference;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a(ILcom/xiaomi/misettings/display/s;)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->j:I

    iput-object p2, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->k:Lcom/xiaomi/misettings/display/s;

    return-void
.end method

.method public c()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/display/a/b;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/display/a/a;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->j:I

    invoke-virtual {v2, v4}, Lcom/xiaomi/misettings/display/a/a;->a(I)I

    move-result v2

    if-ne v3, v2, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/preference/Preference;

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->e(Landroidx/preference/Preference;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public e(Landroidx/preference/Preference;)V
    .locals 1

    instance-of v0, p1, Lcom/xiaomi/misettings/display/ExpertRadioPreference;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/xiaomi/misettings/display/ExpertRadioPreference;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->a(Lcom/xiaomi/misettings/display/ExpertRadioPreference;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->k:Lcom/xiaomi/misettings/display/s;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/xiaomi/misettings/display/s;->c()V

    :cond_0
    return-void
.end method

.method public h()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->l:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/xiaomi/misettings/display/d;->a(Ljava/util/Map;)V

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Landroidx/preference/PreferenceGroup;->e()I

    move-result v2

    if-ge v1, v2, :cond_2

    invoke-virtual {p0, v1}, Landroidx/preference/PreferenceGroup;->a(I)Landroidx/preference/Preference;

    move-result-object v2

    check-cast v2, Lmiuix/preference/RadioSetPreferenceCategory;

    invoke-virtual {v2, v0}, Landroidx/preference/PreferenceGroup;->a(I)Landroidx/preference/Preference;

    move-result-object v2

    instance-of v3, v2, Lcom/xiaomi/misettings/display/ExpertRadioPreference;

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Landroidx/preference/Preference;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_1

    check-cast v2, Lcom/xiaomi/misettings/display/ExpertRadioPreference;

    invoke-virtual {v2}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->i()V

    return-void
.end method

.method public onAttached()V
    .locals 11

    invoke-super {p0}, Landroidx/preference/PreferenceGroup;->onAttached()V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/display/a/b;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/display/a/a;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "support_nature_mode"

    invoke-static {v2, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/xiaomi/misettings/display/l;->primary_color_region:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/xiaomi/misettings/display/l;->primary_color_region_chinese_only:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->l:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->clear()V

    move v6, v1

    :goto_0
    invoke-virtual {p0}, Landroidx/preference/PreferenceGroup;->e()I

    move-result v7

    if-ge v6, v7, :cond_3

    invoke-virtual {p0, v6}, Landroidx/preference/PreferenceGroup;->a(I)Landroidx/preference/Preference;

    move-result-object v7

    check-cast v7, Lmiuix/preference/RadioSetPreferenceCategory;

    invoke-virtual {v7, v1}, Landroidx/preference/PreferenceGroup;->a(I)Landroidx/preference/Preference;

    move-result-object v7

    if-eqz v7, :cond_2

    instance-of v8, v7, Lcom/xiaomi/misettings/display/ExpertRadioPreference;

    if-eqz v8, :cond_2

    check-cast v7, Lcom/xiaomi/misettings/display/ExpertRadioPreference;

    iget-object v8, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->l:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v8, p0, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->j:I

    invoke-virtual {v0, v8}, Lcom/xiaomi/misettings/display/a/a;->a(I)I

    move-result v8

    if-ne v6, v8, :cond_0

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-virtual {p0, v7}, Lcom/xiaomi/misettings/display/ExpertPreferenceCategory;->e(Landroidx/preference/Preference;)V

    :cond_0
    const-string v8, "ExpertPreferenceCategory"

    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    invoke-virtual {v7}, Landroidx/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "set title from "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, " to "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v7, v5}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Title not change. The device feature is "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v9, ", language is "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    :cond_3
    return-void
.end method

.method public onBindViewHolder(Landroidx/preference/B;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/preference/PreferenceCategory;->onBindViewHolder(Landroidx/preference/B;)V

    return-void
.end method
