.class public Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;
.super Lcom/xiaomi/misettings/base/BaseFragment;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/focusmode/a/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;
    }
.end annotation


# instance fields
.field private c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

.field private d:Z

.field private e:Landroid/os/HandlerThread;

.field private f:Landroid/os/Handler;

.field private g:Landroid/view/View;

.field private h:Landroidx/recyclerview/widget/RecyclerView;

.field private i:Lcom/xiaomi/misettings/usagestats/focusmode/a/b;

.field private volatile j:Z

.field private volatile k:Z

.field private volatile l:Z

.field private m:I

.field private n:Landroid/os/Handler;

.field private o:I

.field private p:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->j:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->k:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->l:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->m:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->n:Landroid/os/Handler;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/w;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/w;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->p:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->c(I)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->a(Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;)V
    .locals 5

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    const/4 v1, 0x3

    iput v1, v0, Lb/c/a/a/a;->type:I

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;-><init>()V

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->getAddUpDays()I

    move-result v1

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->totalDays:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->getRunningDays()I

    move-result v1

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->consecutiveDays:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->getAddUpTime()I

    move-result v1

    int-to-long v1, v1

    sget-wide v3, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v1, v3

    iput-wide v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->totalTime:J

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->getAddUpCount()J

    move-result-wide v1

    long-to-int p1, v1

    iput p1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->usedTimes:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iget-object v0, p1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    const/4 v1, -0x1

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->beat:I

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->currentLevel:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->i:Lcom/xiaomi/misettings/usagestats/focusmode/a/b;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/b;->a(Lb/c/b/a/a;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->m()V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb/c/b/a/a;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->i:Lcom/xiaomi/misettings/usagestats/focusmode/a/b;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->m:I

    mul-int/lit8 v1, v1, 0x14

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->o:I

    if-ge v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/b;->a(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->i:Lcom/xiaomi/misettings/usagestats/focusmode/a/b;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/b;->a(Ljava/util/List;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->g:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->l:Z

    return p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->l:Z

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->o:I

    return p1
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/a/b;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/b;-><init>(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/focusmode/a/b$a;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->i:Lcom/xiaomi/misettings/usagestats/focusmode/a/b;

    const v0, 0x7f0b01bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->g:Landroid/view/View;

    const v0, 0x7f0b01b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    new-instance p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->k(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->i:Lcom/xiaomi/misettings/usagestats/focusmode/a/b;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->m()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->k:Z

    return p1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)I
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->m:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->m:I

    return v0
.end method

.method private c(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->o:I

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->n()V

    return-void
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->f:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->p:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->n:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic h(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)Lcom/xiaomi/misettings/usagestats/focusmode/a/b;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->i:Lcom/xiaomi/misettings/usagestats/focusmode/a/b;

    return-object p0
.end method

.method static synthetic i(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->o:I

    return p0
.end method

.method private k()V
    .locals 3

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    instance-of v0, v0, Lmiuix/appcompat/app/AppCompatActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    invoke-virtual {v1}, Landroidx/appcompat/app/ActionBar;->o()V

    const v2, 0x7f130418

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/ActionBar;->d(I)V

    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/xiaomi/misettings/f;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0802fb

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    const v0, 0x7f080301

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/d;->a(Landroid/view/View;)V

    :cond_3
    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/b;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)V

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    const/16 v0, 0xc

    invoke-virtual {v1, v0}, Landroidx/appcompat/app/ActionBar;->b(I)V

    return-void
.end method

.method private l()V
    .locals 7

    :try_start_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/data/c;

    move-result-object v1

    const-string v3, "start_time<= ?"

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {v1, v3, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/c;->a(Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deleteOverDateData:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "deleteOverDateData"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private m()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private declared-synchronized n()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->j:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->e:Landroid/os/HandlerThread;

    if-nez v0, :cond_1

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Focus records..."

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->e:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->f:Landroid/os/Handler;

    if-nez v0, :cond_2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->e:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->f:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private o()V
    .locals 4

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->m:I

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/x;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/x;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;)V

    const/16 v3, 0x14

    invoke-static {v0, v1, v3, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/e;->a(Landroid/content/Context;IILb/c/b/c/h;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e0085

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public synthetic a(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    const-string v1, "levelData"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    new-instance v0, Lcom/misettings/common/base/a;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/misettings/common/base/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lcom/misettings/common/base/a;->a(Landroid/os/Bundle;)Lcom/misettings/common/base/a;

    invoke-virtual {v0}, Lcom/misettings/common/base/a;->a()Lcom/misettings/common/base/a;

    const/high16 p1, 0x800000

    invoke-virtual {v0, p1}, Lcom/misettings/common/base/a;->a(I)Lcom/misettings/common/base/a;

    const-string p1, "com.xiaomi.misettings.usagestats.FocusHistoryShareFragment"

    invoke-virtual {v0, p1}, Lcom/misettings/common/base/a;->a(Ljava/lang/String;)Lcom/misettings/common/base/a;

    invoke-virtual {v0}, Lcom/misettings/common/base/a;->b()V

    :cond_1
    :goto_0
    return-void
.end method

.method public e()V
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->l:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->o()V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->k:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->f:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->l()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->k()V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->n:Landroid/os/Handler;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->f:Landroid/os/Handler;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->f:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->e:Landroid/os/HandlerThread;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    :cond_2
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->b(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->n()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->f:Landroid/os/Handler;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->f:Landroid/os/Handler;

    const/4 p2, 0x2

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
