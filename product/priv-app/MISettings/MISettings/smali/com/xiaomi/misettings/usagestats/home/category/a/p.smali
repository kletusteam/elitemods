.class public Lcom/xiaomi/misettings/usagestats/home/category/a/p;
.super Lcom/xiaomi/misettings/usagestats/home/category/a/c;


# instance fields
.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/view/View;

.field private g:Lcom/xiaomi/misettings/usagestats/home/category/p;

.field private h:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/a/c;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->i:Z

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/c;->b:Z

    const p1, 0x7f0b01be

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->c:Landroid/widget/TextView;

    const p1, 0x7f0b0184

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->d:Landroid/widget/TextView;

    const p1, 0x7f0b016f

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->e:Landroid/widget/ImageView;

    const p1, 0x7f0b0181

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->f:Landroid/view/View;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/view/View;)V

    new-instance p1, Lcom/xiaomi/misettings/usagestats/home/category/a/b;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/b;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/a/p;)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/misettings/common/utils/f;->c(Landroid/content/Context;)Lb/c/a/a/b;

    move-result-object v0

    iget v0, v0, Lb/c/a/a/b;->d:I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingStart()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingEnd()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    const/high16 v2, 0x41e80000    # 29.0f

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    const v3, 0x419170a4    # 18.18f

    invoke-static {v2, p1, v3}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/CharSequence;F)I

    move-result p1

    add-int/2addr v1, p1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result p1

    add-int/2addr v1, p1

    sub-int/2addr v0, v1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->c:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    return-void
.end method

.method private c()I
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->i:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->h:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget-boolean v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0xb4

    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->h:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget-boolean v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    if-eqz v0, :cond_2

    const/16 v1, -0xb4

    :cond_2
    return v1
.end method

.method private d()I
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->i:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->h:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget-boolean v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    if-eqz v0, :cond_0

    const/16 v1, 0xb4

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->h:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget-boolean v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/16 v1, -0xb4

    :goto_0
    return v1
.end method

.method private e()V
    .locals 8

    new-instance v7, Landroid/view/animation/RotateAnimation;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->c()I

    move-result v0

    int-to-float v1, v0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->d()I

    move-result v0

    int-to-float v2, v0

    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    const-wide/16 v0, 0x1f4

    invoke-virtual {v7, v0, v1}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v7, v0}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method


# virtual methods
.method public synthetic a(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->h:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->g:I

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->e()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->h:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget-boolean v0, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->g:Lcom/xiaomi/misettings/usagestats/home/category/p;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a(Lcom/xiaomi/misettings/usagestats/home/category/c/h;)V

    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V
    .locals 2

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/category/p;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->g:Lcom/xiaomi/misettings/usagestats/home/category/p;

    check-cast p2, Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->h:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    const/4 p2, 0x1

    new-array p3, p2, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->h:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p3, v1

    const-string v0, "(%d)"

    invoke-static {p1, v0, p3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->a(Ljava/lang/String;)V

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->h:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->c:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->d:Landroid/widget/TextView;

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->h:Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    iget-boolean p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->h:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->e:Landroid/widget/ImageView;

    const p3, 0x7f08016e

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->i:Z

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->e:Landroid/widget/ImageView;

    const p2, 0x7f08016d

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->i:Z

    :goto_0
    return-void
.end method

.method public bridge synthetic a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V
    .locals 0

    check-cast p2, Lb/c/a/a/a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/home/category/a/p;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V

    return-void
.end method

.method protected b()I
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    const v1, 0x4268b852    # 58.18f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    return v0
.end method
