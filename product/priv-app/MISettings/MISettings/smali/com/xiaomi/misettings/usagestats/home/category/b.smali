.class public Lcom/xiaomi/misettings/usagestats/home/category/b;
.super Lcom/xiaomi/misettings/usagestats/home/category/k$a;


# instance fields
.field private f:Lcom/xiaomi/misettings/usagestats/home/category/b/a;

.field private g:Landroidx/recyclerview/widget/RecyclerView$t;

.field private h:Z

.field private i:Landroidx/recyclerview/widget/RecyclerView$t;

.field private j:Landroidx/recyclerview/widget/RecyclerView$t;


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/usagestats/home/category/b/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->f:Lcom/xiaomi/misettings/usagestats/home/category/b/a;

    return-void
.end method

.method private a(Ljava/util/List;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroidx/recyclerview/widget/RecyclerView$t;",
            ">;I)",
            "Landroidx/recyclerview/widget/RecyclerView$t;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ensureHasCoverHolder: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "BaseClassifyItemTouchHelperCallback"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->f:Lcom/xiaomi/misettings/usagestats/home/category/b/a;

    instance-of v1, p1, Lcom/xiaomi/misettings/usagestats/home/category/p;

    if-eqz v1, :cond_4

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/category/p;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->a()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-virtual {v2}, Landroidx/recyclerview/widget/RecyclerView$t;->getAdapterPosition()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ensureHasCoverHolder: lastCoverPosition="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    if-le p2, v0, :cond_1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/category/p;->getItemCount()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    if-lt v2, p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    return-object p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object p1

    return-object p1

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    if-ge p2, p1, :cond_3

    if-gtz v2, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    return-object p1

    :cond_2
    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object p1

    return-object p1

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    return-object p1

    :cond_4
    const/4 p1, 0x0

    return-object p1
.end method

.method private a(Ljava/lang/String;Landroidx/recyclerview/widget/RecyclerView$t;F)V
    .locals 0

    iget-object p2, p2, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-static {p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Ljava/lang/String;Landroid/view/View;F)V

    return-void
.end method

.method private c(Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->h:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->f:Lcom/xiaomi/misettings/usagestats/home/category/b/a;

    if-eqz v1, :cond_0

    invoke-interface {v1, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/b/a;->a(Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$t;Ljava/util/List;II)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 5
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView$t;",
            "Ljava/util/List<",
            "Landroidx/recyclerview/widget/RecyclerView$t;",
            ">;II)",
            "Landroidx/recyclerview/widget/RecyclerView$t;"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, "BaseClassifyItemTouchHelperCallback"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const-string p2, "chooseDropTarget: dropTargets size is 0"

    invoke-static {v1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/b;->c(Landroidx/recyclerview/widget/RecyclerView$t;)V

    return-object v2

    :cond_0
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr p3, v0

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr p4, v0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v4, v3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    if-ge p3, v4, :cond_1

    iget-object v4, v3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    if-le p3, v4, :cond_1

    iget-object v4, v3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    if-le p4, v4, :cond_1

    iget-object v4, v3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    if-ge p4, v4, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move-object v3, v2

    :goto_1
    if-nez v3, :cond_4

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    if-eqz p3, :cond_3

    invoke-direct {p0, p2, p4}, Lcom/xiaomi/misettings/usagestats/home/category/b;->a(Ljava/util/List;I)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object p2

    if-eqz p2, :cond_3

    return-object p2

    :cond_3
    const-string p2, "chooseDropTarget:coverhlder is null"

    invoke-static {v1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/b;->c(Landroidx/recyclerview/widget/RecyclerView$t;)V

    return-object v2

    :cond_4
    return-object v3
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 7
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPreSelectedChanged: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BaseClassifyItemTouchHelperCallback"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_3

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->h:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->d:Z

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->f:Lcom/xiaomi/misettings/usagestats/home/category/b/a;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    if-eqz v2, :cond_0

    instance-of v2, p1, Lcom/xiaomi/misettings/usagestats/home/category/a/c;

    if-eqz v2, :cond_0

    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setTranslationZ(F)V

    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    new-instance v2, Lmiuix/animation/b/a;

    const-string v4, "contentRestore"

    invoke-direct {v2, v4}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v4, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v2, v4, v5, v6}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v4, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {v2, v4, v5, v6}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-array v4, v0, [Landroid/view/View;

    iget-object v5, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    aput-object v5, v4, v1

    invoke-static {v4}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v4

    invoke-interface {v4}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v4

    invoke-interface {v4}, Lmiuix/animation/g;->cancel()V

    new-array v0, v0, [Landroid/view/View;

    iget-object v4, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    aput-object v4, v0, v1

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v0

    new-array v4, v1, [Lmiuix/animation/a/a;

    invoke-interface {v0, v2, v4}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->f:Lcom/xiaomi/misettings/usagestats/home/category/b/a;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-interface {v0, p1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/b/a;->b(Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)V

    iput-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    :cond_0
    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->d:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->f:Lcom/xiaomi/misettings/usagestats/home/category/b/a;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->j:Landroidx/recyclerview/widget/RecyclerView$t;

    if-eqz v2, :cond_2

    invoke-interface {v0, p1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/b/a;->b(Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)V

    :cond_2
    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->d:Z

    :cond_3
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView$t;I)V

    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)V

    return-void
.end method

.method public b(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 8
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSelectedChanged: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BaseClassifyItemTouchHelperCallback"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p2, :cond_2

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->i:Landroidx/recyclerview/widget/RecyclerView$t;

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->h:Z

    instance-of v3, p1, Lcom/xiaomi/misettings/usagestats/home/category/b/b;

    if-eqz v3, :cond_1

    move-object v3, p1

    check-cast v3, Lcom/xiaomi/misettings/usagestats/home/category/b/b;

    invoke-interface {v3}, Lcom/xiaomi/misettings/usagestats/home/category/b/b;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const/high16 v3, 0x41f00000    # 30.0f

    invoke-virtual {v0, v3}, Landroid/view/View;->setTranslationZ(F)V

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const v3, 0x7f0800d5

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    const v0, 0x3f733333    # 0.95f

    const-string v3, "contentSelect"

    invoke-direct {p0, v3, p1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/b;->a(Ljava/lang/String;Landroidx/recyclerview/widget/RecyclerView$t;F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->f:Lcom/xiaomi/misettings/usagestats/home/category/b/a;

    if-eqz v0, :cond_4

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/b/a;->a(Landroidx/recyclerview/widget/RecyclerView$t;)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->j:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->h:Z

    goto :goto_1

    :cond_1
    return-void

    :cond_2
    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->h:Z

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->i:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v3, v3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setTranslationZ(F)V

    new-instance v4, Lmiuix/animation/b/a;

    const-string v5, "contentUnSelect"

    invoke-direct {v4, v5}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v5, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v4, v5, v6, v7}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v5, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {v4, v5, v6, v7}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-array v5, v1, [Landroid/view/View;

    aput-object v3, v5, v2

    invoke-static {v5}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v5

    invoke-interface {v5}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v5

    invoke-interface {v5}, Lmiuix/animation/g;->cancel()V

    new-array v1, v1, [Landroid/view/View;

    aput-object v3, v1, v2

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v1

    new-array v3, v2, [Lmiuix/animation/a/a;

    invoke-interface {v1, v4, v3}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    :cond_3
    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->i:Landroidx/recyclerview/widget/RecyclerView$t;

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->h:Z

    :cond_4
    :goto_1
    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(Landroidx/recyclerview/widget/RecyclerView$t;I)V

    return-void
.end method

.method public b(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)Z
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string p1, "contentMove"

    const v0, 0x3f4ccccd    # 0.8f

    invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/home/category/b;->a(Ljava/lang/String;Landroidx/recyclerview/widget/RecyclerView$t;F)V

    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$t;->getAdapterPosition()I

    move-result p1

    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView$t;->getAdapterPosition()I

    move-result v0

    if-eq p1, v0, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->f:Lcom/xiaomi/misettings/usagestats/home/category/b/a;

    if-eqz p1, :cond_2

    instance-of p1, p3, Lcom/xiaomi/misettings/usagestats/home/category/b/b;

    if-eqz p1, :cond_2

    move-object p1, p3

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/category/b/b;

    invoke-interface {p1}, Lcom/xiaomi/misettings/usagestats/home/category/b/b;->a()Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->f:Lcom/xiaomi/misettings/usagestats/home/category/b/a;

    invoke-interface {p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/home/category/b/a;->a(Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->h:Z

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->h:Z

    if-eqz p1, :cond_0

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/b;->g:Landroidx/recyclerview/widget/RecyclerView$t;

    goto :goto_0

    :cond_1
    const-string p1, "BaseClassifyItemTouchHelperCallback"

    const-string p2, "onMove: is same position"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public c(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)I
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    instance-of p1, p2, Lcom/xiaomi/misettings/usagestats/home/category/b/b;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    check-cast p2, Lcom/xiaomi/misettings/usagestats/home/category/b/b;

    invoke-interface {p2}, Lcom/xiaomi/misettings/usagestats/home/category/b/b;->a()Z

    move-result p1

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 p1, 0x3

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->d(II)I

    move-result p1

    return p1
.end method

.method public c(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method
