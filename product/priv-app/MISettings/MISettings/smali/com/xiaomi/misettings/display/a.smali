.class public Lcom/xiaomi/misettings/display/a;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/display/a;->c(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const-string p0, "support_dc_backlight_sec"

    return-object p0

    :cond_0
    const-string p0, "support_dc_backlight"

    return-object p0
.end method

.method public static a()Z
    .locals 2

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v1, "zizhan"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/display/a;->c(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const-string p0, "support_low_flicker_backlight_sec"

    return-object p0

    :cond_0
    const-string p0, "support_low_flicker_backlight"

    return-object p0
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 1

    invoke-static {}, Lcom/xiaomi/misettings/display/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/misettings/common/utils/m;->e(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
