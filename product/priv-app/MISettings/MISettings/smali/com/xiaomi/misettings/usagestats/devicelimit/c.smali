.class Lcom/xiaomi/misettings/usagestats/devicelimit/c;
.super Lmiui/process/IForegroundInfoListener$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/c;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/c;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->a(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/c;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->c(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/c;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->b(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/c;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->a(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/c;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;

    invoke-virtual {p1}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_3

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/c;->a:Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->d(Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;)V

    :cond_3
    return-void
.end method
