.class public Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;
.super Landroid/widget/FrameLayout;


# instance fields
.field private a:I

.field private b:Lb/c/a/a/b;

.field private c:Landroid/os/Handler;

.field private d:Z

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Lcom/airbnb/lottie/LottieAnimationView;

.field private i:Lcom/airbnb/lottie/LottieAnimationView;

.field private j:Lcom/airbnb/lottie/LottieAnimationView;

.field private k:Landroid/view/ViewStub;

.field private l:Landroid/view/ViewStub;

.field private m:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->d()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->d()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->d()V

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->f:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->k:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->f:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->i:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->f:Landroid/view/View;

    const v1, 0x7f0b016c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->i:Lcom/airbnb/lottie/LottieAnimationView;

    :cond_1
    return-void
.end method

.method private a(F)V
    .locals 4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->c()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->f:Landroid/view/View;

    sub-float/2addr v2, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->g:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->c()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->e:Landroid/view/View;

    sub-float/2addr v2, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->f:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    :goto_0
    return-void
.end method

.method private a(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->a()V

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->e()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->h()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a(F)V

    return-void
.end method

.method private b()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->e:Landroid/view/View;

    if-nez v0, :cond_0

    const v0, 0x7f0b01c5

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->e:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_1

    const v0, 0x7f0b01c4

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    :cond_1
    return-void
.end method

.method private b(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->d()V

    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->g:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->l:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->g:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->j:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->g:Landroid/view/View;

    const v1, 0x7f0b01c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->j:Lcom/airbnb/lottie/LottieAnimationView;

    :cond_1
    return-void
.end method

.method private d()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/f;->n(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->d:Z

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/f;->c(Landroid/content/Context;)Lb/c/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b:Lb/c/a/a/b;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->c:Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0082

    invoke-static {v0, v1, p0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0b016e

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->k:Landroid/view/ViewStub;

    const v0, 0x7f0b01c9

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->l:Landroid/view/ViewStub;

    return-void
.end method

.method private e()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->m:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->m:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->m:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x5dc

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->m:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->m:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/a;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->m:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/b;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/b;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private f()V
    .locals 6

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x2

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a:I

    const/4 v3, 0x1

    const v4, 0x7f080088

    const/4 v5, -0x1

    if-eq v2, v3, :cond_2

    if-eq v2, v1, :cond_1

    const/4 v1, 0x3

    if-eq v2, v1, :cond_0

    goto/16 :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->c()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->g:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08008a

    invoke-static {v3, v4, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b:Lb/c/a/a/b;

    iget v3, v3, Lb/c/a/a/b;->d:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/misettings/common/utils/f;->d(Landroid/content/Context;)I

    move-result v4

    invoke-static {v0, v3, v4}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->j:Lcom/airbnb/lottie/LottieAnimationView;

    sget-object v1, Lcom/airbnb/lottie/Q;->b:Lcom/airbnb/lottie/Q;

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setRenderMode(Lcom/airbnb/lottie/Q;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->j:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0, v5}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->f:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v4, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b:Lb/c/a/a/b;

    iget v3, v3, Lb/c/a/a/b;->d:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/misettings/common/utils/f;->d(Landroid/content/Context;)I

    move-result v4

    invoke-static {v0, v3, v4}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->i:Lcom/airbnb/lottie/LottieAnimationView;

    sget-object v1, Lcom/airbnb/lottie/Q;->b:Lcom/airbnb/lottie/Q;

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setRenderMode(Lcom/airbnb/lottie/Q;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->i:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0, v5}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->e:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v4, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b:Lb/c/a/a/b;

    iget v3, v3, Lb/c/a/a/b;->d:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/misettings/common/utils/f;->d(Landroid/content/Context;)I

    move-result v4

    invoke-static {v0, v3, v4}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    sget-object v1, Lcom/airbnb/lottie/Q;->b:Lcom/airbnb/lottie/Q;

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setRenderMode(Lcom/airbnb/lottie/Q;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0, v5}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    :goto_0
    return-void
.end method

.method private g()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->e()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method private h()V
    .locals 4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_6

    const/4 v1, 0x2

    const/16 v3, 0x8

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto/16 :goto_6

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->c()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->i:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a(Lcom/airbnb/lottie/LottieAnimationView;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->j:Lcom/airbnb/lottie/LottieAnimationView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->d:Z

    if-nez v1, :cond_1

    const-string v1, "images_night"

    goto :goto_0

    :cond_1
    const-string v1, "images_night_small"

    :goto_0
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->j:Lcom/airbnb/lottie/LottieAnimationView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->d:Z

    if-nez v1, :cond_2

    const-string v1, "night.json"

    goto :goto_1

    :cond_2
    const-string v1, "night_s.json"

    :goto_1
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->j:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b(Lcom/airbnb/lottie/LottieAnimationView;)V

    goto :goto_6

    :cond_3
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a(Lcom/airbnb/lottie/LottieAnimationView;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->i:Lcom/airbnb/lottie/LottieAnimationView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->d:Z

    if-nez v1, :cond_4

    const-string v1, "images_afternoon"

    goto :goto_2

    :cond_4
    const-string v1, "images_afternoon_small"

    :goto_2
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->i:Lcom/airbnb/lottie/LottieAnimationView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->d:Z

    if-nez v1, :cond_5

    const-string v1, "afternoon.json"

    goto :goto_3

    :cond_5
    const-string v1, "afternoon_s.json"

    :goto_3
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->i:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b(Lcom/airbnb/lottie/LottieAnimationView;)V

    goto :goto_6

    :cond_6
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->d:Z

    if-nez v1, :cond_7

    const-string v1, "images_morning"

    goto :goto_4

    :cond_7
    const-string v1, "images_morning_small"

    :goto_4
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->d:Z

    if-nez v1, :cond_8

    const-string v1, "morning.json"

    goto :goto_5

    :cond_8
    const-string v1, "morning_s.json"

    :goto_5
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->h:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->b(Lcom/airbnb/lottie/LottieAnimationView;)V

    :goto_6
    return-void
.end method


# virtual methods
.method public setCurrentLevel(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->a:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->f()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusModeBackgroundView;->g()V

    return-void
.end method
