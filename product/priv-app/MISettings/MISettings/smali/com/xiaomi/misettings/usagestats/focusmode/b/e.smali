.class public Lcom/xiaomi/misettings/usagestats/focusmode/b/e;
.super Lcom/xiaomi/misettings/usagestats/focusmode/b/a;


# instance fields
.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;

.field private m:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    const v0, 0x7f0b0211

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->c:Landroid/widget/TextView;

    const v0, 0x7f0b0218

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->d:Landroid/widget/TextView;

    const v0, 0x7f0b016b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->h:Landroid/widget/TextView;

    const v0, 0x7f0b01b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->i:Landroid/widget/ImageView;

    const v0, 0x7f0b01b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->j:Landroid/widget/TextView;

    const v0, 0x7f0b0215

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->k:Landroid/widget/TextView;

    const v0, 0x7f0b0186

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->e:Landroid/widget/TextView;

    const v0, 0x7f0b01f9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->f:Landroid/widget/TextView;

    const v0, 0x7f0b018f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->g:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->l:Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;

    return-void
.end method

.method protected b()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    const v1, 0x7f0e0083

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 9

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->l:Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    const v2, 0x7f1303cd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->e()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11002b

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->l:Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;

    iget v4, v4, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->totalTime:I

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v6, v8

    invoke-virtual {v2, v3, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->k:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->e()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f110027

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->l:Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;

    iget v4, v4, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->totalTime:I

    new-array v6, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v2, v3, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->e()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11002e

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->l:Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;

    iget v4, v4, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->wakeUps:I

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->f:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->l:Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;

    iget-wide v2, v2, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->startTime:J

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->g:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->l:Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;

    iget-wide v2, v2, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->endTime:J

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->l:Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;

    iget-wide v2, v2, Lcom/xiaomi/misettings/usagestats/focusmode/model/CurrentUsageState;->date:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->m:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->levelDescription:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v2, "\\"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->levelDescription:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->m:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->levelDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->m:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iget-object v2, v2, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    iget-wide v2, v2, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->totalTime:J

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->m:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;->data:Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;

    iget v1, v1, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData$LevelDetail;->currentLevel:I

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->r(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->j:Landroid/widget/TextView;

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f1303fc

    goto :goto_0

    :cond_3
    const v2, 0x7f1303fb

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/e;->h:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    int-to-long v3, v0

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v3, v5

    invoke-static {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->d()V

    return-void
.end method
