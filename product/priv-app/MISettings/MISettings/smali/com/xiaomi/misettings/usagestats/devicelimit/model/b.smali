.class public Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->c:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->c:Z

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->a:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->d:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;)I
    .locals 1

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;->a(Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;)I

    move-result p1

    return p1
.end method
