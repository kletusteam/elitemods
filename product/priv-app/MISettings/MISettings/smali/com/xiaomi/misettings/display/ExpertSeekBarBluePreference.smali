.class public Lcom/xiaomi/misettings/display/ExpertSeekBarBluePreference;
.super Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;


# instance fields
.field private f:Lmiuix/androidbasewidget/widget/SeekBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarBluePreference;->f()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarBluePreference;->f()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarBluePreference;->f()V

    return-void
.end method

.method private f()V
    .locals 1

    sget v0, Lcom/xiaomi/misettings/display/k;->miuix_preference_widget_seekbar:I

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroidx/preference/B;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->onBindViewHolder(Landroidx/preference/B;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/xiaomi/misettings/display/j;->seekbar:I

    invoke-virtual {p1, v1}, Landroidx/preference/B;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/androidbasewidget/widget/SeekBar;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarBluePreference;->f:Lmiuix/androidbasewidget/widget/SeekBar;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarBluePreference;->f:Lmiuix/androidbasewidget/widget/SeekBar;

    if-eqz p1, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/xiaomi/misettings/display/g;->foreground_primary_color_b:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/xiaomi/misettings/display/g;->foreground_primary_disable_color_b:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lmiuix/androidbasewidget/widget/SeekBar;->setForegroundPrimaryColor(II)V

    :cond_0
    return-void
.end method
