.class public abstract Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;
.super Lcom/xiaomi/misettings/usagestats/focusmode/FocusBaseNoActionbarFragment;


# instance fields
.field private c:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusBaseNoActionbarFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0704ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {p0}, Lcom/misettings/common/utils/m;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/misettings/common/utils/m;->g(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    int-to-double v0, v0

    const-wide/high16 v2, 0x400c000000000000L    # 3.5

    mul-double/2addr v0, v2

    double-to-int v0, v0

    :cond_0
    return v0
.end method

.method public static b(Landroid/content/Context;)F
    .locals 3

    invoke-static {p0}, Lcom/misettings/common/utils/f;->n(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x3fc2d82e

    goto :goto_0

    :cond_0
    const v0, 0x3fe1e1e2

    :goto_0
    invoke-static {p0}, Lcom/misettings/common/utils/m;->g(Landroid/content/Context;)Z

    move-result v1

    const v2, 0x3f934d35

    if-eqz v1, :cond_3

    invoke-static {p0}, Lcom/misettings/common/utils/m;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v2, 0x401c71c7

    goto :goto_1

    :cond_1
    invoke-static {p0}, Lcom/misettings/common/utils/m;->f(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_2

    const v2, 0x3f325137

    :cond_2
    :goto_1
    return v2

    :cond_3
    invoke-static {p0}, Lcom/misettings/common/utils/m;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p0}, Lcom/misettings/common/utils/m;->f(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_4

    const v0, 0x3f5e7486

    goto :goto_2

    :cond_4
    move v0, v2

    :cond_5
    :goto_2
    return v0
.end method

.method private b(Landroid/view/View;)V
    .locals 3

    const v0, 0x7f0b01ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07009b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    div-int/lit8 v2, v1, 0x2

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/misettings/common/utils/m;->f(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    div-int/lit8 v1, v1, 0x4

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->setPageName(Ljava/lang/String;)V

    const v0, 0x7f0b01eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->f(Landroid/view/View;)V

    const v1, 0x7f0b01ed

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->f(Landroid/view/View;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f070528

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->m()Z

    move-result v1

    if-eqz v1, :cond_2

    div-int/lit8 p1, p1, 0x2

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iput p1, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/misettings/common/utils/m;->d(Landroid/content/Context;)Z

    move-result p1

    const/4 v2, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/misettings/common/utils/a;->b(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_3

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :cond_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/misettings/common/utils/m;->g(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/misettings/common/utils/m;->f(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_4

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method private c(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f0b0170

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    const v1, 0x7f130044

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/d;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/d;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f08016b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/m;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0b036e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method private d(Landroid/view/View;)V
    .locals 5

    const v0, 0x7f0b01ea

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v2

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v3

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/misettings/usagestats/utils/H;->h(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/high16 v4, 0x41200000    # 10.0f

    goto :goto_0

    :cond_0
    const/high16 v4, 0x41a00000    # 20.0f

    :goto_0
    invoke-static {v2, v3, v4}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;Landroid/graphics/drawable/BitmapDrawable;F)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->d:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->e(Landroid/view/View;)V

    return-void
.end method

.method private e(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->b(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->l()Lcom/xiaomi/misettings/usagestats/focusmode/b/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/b/a;->d()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->c:Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/c/c;->a()Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->setView(Landroid/view/View;)V

    return-void
.end method

.method private f(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/misettings/common/utils/f;->j(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->a(Landroid/content/Context;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v1, v1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->b(Landroid/content/Context;)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private m()Z
    .locals 3

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "force_fsg_nav_bar"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    return v1
.end method


# virtual methods
.method protected abstract k()Ljava/lang/String;
.end method

.method protected abstract l()Lcom/xiaomi/misettings/usagestats/focusmode/b/a;
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->d:Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->b(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusBaseNoActionbarFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->c(Landroid/view/View;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->d(Landroid/view/View;)V

    return-void
.end method
