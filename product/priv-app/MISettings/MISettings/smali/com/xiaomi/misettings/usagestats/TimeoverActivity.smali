.class public Lcom/xiaomi/misettings/usagestats/TimeoverActivity;
.super Lcom/misettings/common/base/BaseActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/TimeoverActivity$b;,
        Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;,
        Lcom/xiaomi/misettings/usagestats/TimeoverActivity$a;
    }
.end annotation


# static fields
.field private static a:Ljava/lang/String; = "LR-TimeOverActivity"

.field static final b:Ljava/lang/Object;


# instance fields
.field private c:Ljava/lang/String;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/view/View;

.field private j:Z

.field private k:J

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:J

.field private q:Ljava/lang/String;

.field private r:Z

.field s:Z

.field t:Z

.field private u:I

.field private v:Lcom/xiaomi/misettings/usagestats/f/h;

.field private w:Lmiui/process/IForegroundInfoListener$Stub;

.field private x:J

.field private y:Z

.field private z:Lcom/xiaomi/misettings/tools/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->m:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->n:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->o:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->u:I

    return p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;J)J
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->p:J

    return-wide p1
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 p0, 0x800000

    invoke-virtual {v0, p0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 p0, 0x10000000

    invoke-virtual {v0, p0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string p0, "page_from"

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;Lcom/xiaomi/misettings/usagestats/f/h;)Lcom/xiaomi/misettings/usagestats/f/h;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->v:Lcom/xiaomi/misettings/usagestats/f/h;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->l:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->l:Ljava/lang/String;

    return-object p1
.end method

.method private a(J)V
    .locals 4

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->r:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->v:Lcom/xiaomi/misettings/usagestats/f/h;

    const-string v0, "key_category_data"

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string p2, "key_is_week"

    invoke-virtual {p1, p2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {p0, p2}, Lcom/xiaomi/misettings/usagestats/b/a/g;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/ui/CategoryUsageDetailFragment;->a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "isWeek"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    const-string v3, "packageName"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "usageTime"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string p1, "hasTime"

    invoke-virtual {v0, p1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string p1, "fromNotification"

    invoke-virtual {v0, p1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->j:Z

    if-eqz p1, :cond_1

    const-class p1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    const-string p2, "fromPager"

    invoke-virtual {v0, p2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    :goto_0
    invoke-static {p0}, Lcom/xiaomi/misettings/f;->a(Landroid/app/Activity;)Landroid/util/Pair;

    move-result-object p1

    iget-object p2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p2, p1}, Landroid/app/Activity;->overridePendingTransition(II)V

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->m:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->finish()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a(Landroid/content/Context;Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 3

    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "jumpTimeOverActivity from"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "miui.intent.action.USAGE_STATS_TIMEOVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "limitType"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p2, "pkgName"

    invoke-virtual {v0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 p1, 0x800000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 p1, 0x10000000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "miui.intent.action.USAGE_STATS_TIMEOVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "pkgName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 p1, 0x800000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 p1, 0x10000000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string p1, "deviceLimit"

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "deviceLimitPeriod"

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->n:Z

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->o:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a(J)V

    return-void
.end method

.method private b()Z
    .locals 6

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/p;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/i;->b(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->l()V

    return v1

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->j:Z

    const/4 v2, 0x1

    if-nez v0, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v3, "deviceLimit"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->y:Z

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->s:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->t:Z

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->y:Z

    :goto_1
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->y:Z

    if-eqz v0, :cond_3

    return v2

    :cond_3
    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    const-string v3, "deviceLimitStatus check end"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->r:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->q:Ljava/lang/String;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v3

    invoke-static {p0, v0, v3}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v0

    sget-object v3, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "categoryLimitTime"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "---mTotalCategoryUsageTime:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->u:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->u:I

    if-le v0, v3, :cond_6

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->q:Ljava/lang/String;

    invoke-static {p0, v2, v1}, Lcom/xiaomi/misettings/usagestats/controller/b;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->q:Ljava/lang/String;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->u:I

    sub-int/2addr v0, v3

    invoke-static {p0, v2, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->b(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->finish()V

    return v1

    :cond_4
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->j(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ensureShow: is not in limit list "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/i;->b(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->l()V

    return v1

    :cond_5
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v3

    invoke-static {p0, v0, v3}, Lcom/xiaomi/misettings/usagestats/utils/p;->e(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v0

    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->x:J

    invoke-static {v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/L;->f(J)I

    move-result v3

    if-le v0, v3, :cond_6

    sget-object v2, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ensureShow: totalTime="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ",limitTime="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ",pkgName"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {p0, v2, v1}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    sub-int/2addr v0, v3

    invoke-static {p0, v2, v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->finish()V

    return v1

    :cond_6
    return v2
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 1

    const-class v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/tools/c;->c(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->f(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->r:Z

    return p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;J)J
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->x:J

    return-wide p1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->q:Ljava/lang/String;

    return-object p0
.end method

.method private c()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->k()V

    :cond_0
    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->o:Ljava/lang/String;

    return-object p0
.end method

.method private d()V
    .locals 2

    const v0, 0x7f0b023e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->d:Landroid/widget/ImageView;

    const v0, 0x7f0b0389

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->e:Landroid/widget/TextView;

    const v0, 0x7f0b0390

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->f:Landroid/widget/TextView;

    const v0, 0x7f0b0394

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->g:Landroid/widget/TextView;

    const v0, 0x7f0b038f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->h:Landroid/widget/TextView;

    const v0, 0x7f0b038d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->i:Landroid/view/View;

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->p:J

    return-wide v0
.end method

.method private e()V
    .locals 3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    :cond_0
    return-void
.end method

.method private f()V
    .locals 7

    const-string v0, "limitType"

    const-string v1, "__"

    :try_start_0
    sget-object v2, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    const-string v3, "initCategoryCheck start"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/b/a/g;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->q:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/p;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->s:Z

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->q:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/utils/j;->g(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->t:Z

    sget-object v2, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hasSuspendBy"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->s:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->t:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->s:Z

    const/4 v3, 0x1

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->t:Z

    if-eqz v2, :cond_0

    iput-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->r:Z

    goto :goto_0

    :cond_0
    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->t:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->i(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iput-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->r:Z

    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sget-object v2, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LIMIT_TYPE:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v0, v3, :cond_2

    move v4, v3

    :cond_2
    iput-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->r:Z

    iput-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->t:Z

    :cond_3
    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsStartFromCategory:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->r:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hasSuspendByChange"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->s:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->t:Z

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initCategoryCheck error"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)Z
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->b()Z

    move-result p0

    return p0
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)Lmiui/process/IForegroundInfoListener$Stub;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->w:Lmiui/process/IForegroundInfoListener$Stub;

    return-object p0
.end method

.method private g()Z
    .locals 3

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    const-string v2, "page_from"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->f(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private h()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    and-int/lit16 v1, v1, -0x2001

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->i()V

    return-void
.end method

.method private i()V
    .locals 3

    const v0, 0x7f0e0141

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    const v0, 0x7f0b0251

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/M;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    goto :goto_0

    :cond_0
    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {p0}, Lcom/xiaomi/misettings/f;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    and-int/lit16 v1, v1, -0x2001

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->d()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->initData()V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x43160000    # 150.0f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/a/a;->a(Landroid/view/Window;F)V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Build.VERSION.SDK_INT:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic i(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->n:Z

    return p0
.end method

.method private initData()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->e:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->f:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->g:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->i:Landroid/view/View;

    if-nez v1, :cond_0

    goto/16 :goto_1

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->y:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->g:Landroid/widget/TextView;

    const v1, 0x7f13036b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->g:Landroid/widget/TextView;

    const v1, 0x7f130361

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->r:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f130331

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->x:J

    invoke-static {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    const v0, 0x7f130330

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/M;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->x:J

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->g:Landroid/widget/TextView;

    new-instance v3, Lcom/xiaomi/misettings/usagestats/w;

    invoke-direct {v3, p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/w;-><init>(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;J)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->i:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/x;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/x;-><init>(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    :goto_1
    return-void
.end method

.method private j()V
    .locals 14

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const v2, 0x7f130370

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v1, v4

    const v3, 0x7f13036c

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    aput-object v5, v1, v6

    const v5, 0x7f13036d

    invoke-virtual {p0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    aput-object v7, v1, v8

    const v7, 0x7f13036e

    invoke-virtual {p0, v7}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    aput-object v9, v1, v10

    const v9, 0x7f13036f

    invoke-virtual {p0, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x4

    aput-object v11, v1, v12

    new-instance v11, Lmiuix/appcompat/app/j$a;

    invoke-direct {v11, p0}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result v13

    if-eqz v13, :cond_0

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {p0, v7}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-virtual {p0, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v12

    const-string v2, "1\u5206\u949f"

    aput-object v2, v1, v0

    :cond_0
    const/4 v0, -0x1

    new-instance v2, Lcom/xiaomi/misettings/usagestats/y;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/y;-><init>(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V

    invoke-virtual {v11, v1, v0, v2}, Lmiuix/appcompat/app/j$a;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    const v0, 0x7f13029e

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Lmiuix/appcompat/app/j$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v11}, Lmiuix/appcompat/app/j$a;->b()Lmiuix/appcompat/app/j;

    return-void
.end method

.method static synthetic j(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c()V

    return-void
.end method

.method private k()V
    .locals 4

    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    const-string v1, "suspendApp"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->k:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->r:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    const-string v2, "suspendApp mIsStartFromCategory"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->q:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/controller/b;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    const-string v2, "suspendApp app"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->y:Z

    invoke-static {v0, v2, v1, v3}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;ZZ)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->finish()V

    :cond_2
    return-void
.end method

.method static synthetic k(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->y:Z

    return p0
.end method

.method private l()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/controller/i;->c(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->finish()V

    return-void
.end method

.method static synthetic l(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->j()V

    return-void
.end method

.method static synthetic m(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->k()V

    return-void
.end method

.method static synthetic n(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic o(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->x:J

    return-wide v0
.end method


# virtual methods
.method public finish()V
    .locals 0

    invoke-super {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->z:Lcom/xiaomi/misettings/tools/e;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/tools/e;->b()V

    return-void

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    const-string v0, "onConfigurationChanged"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const p1, 0x7f0b023e

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout$LayoutParams;

    iget v0, p1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07046c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget v2, p1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iget v3, p1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->e()V

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    sget-object p1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TimeoverActivity create:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->g()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/xiaomi/misettings/tools/e;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/tools/e;-><init>(Landroid/app/Activity;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->z:Lcom/xiaomi/misettings/tools/e;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->z:Lcom/xiaomi/misettings/tools/e;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/tools/e;->c()V

    return-void

    :cond_0
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->z:Lcom/xiaomi/misettings/tools/e;

    new-instance p1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$a;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$a;-><init>(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->w:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->h()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->k:J

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "pkgName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "theEnd"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->j:Z

    sget-object p1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onCreateVal:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->j:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->f()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    const-string v0, "onCreate: invalid packageName"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->finish()V

    return-void

    :cond_1
    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object p1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$b;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$b;-><init>(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V

    invoke-virtual {p1, v0}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->z:Lcom/xiaomi/misettings/tools/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/tools/e;->d()V

    return-void

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->w:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/ProcessManagerDelegate;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyDown: keycode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->y:Z

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;ZZ)V

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onMultiWindowModeChanged(ZLandroid/content/res/Configuration;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMultiWindowModeChanged(ZLandroid/content/res/Configuration;)V

    sget-object p2, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onMultiWindowModeChanged: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c()V

    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 6

    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNewIntent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->x:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "pkgName"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "theEnd"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    sget-object p1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNewIntent: val is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    const-string v1, "onNewIntent: empty "

    invoke-static {p1, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ""

    :goto_0
    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->x:J

    const-wide/16 v4, 0x0

    cmp-long p1, v2, v4

    if-eqz p1, :cond_3

    if-eqz v1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object p1

    new-instance v2, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity$c;-><init>(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V

    invoke-virtual {p1, v2}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->b()Z

    move-result p1

    if-nez p1, :cond_2

    return-void

    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->j:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->initData()V

    :cond_3
    :goto_1
    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 4

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStop()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->z:Lcom/xiaomi/misettings/tools/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/tools/e;->e()V

    return-void

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStop: ==stop=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->l:Ljava/lang/String;

    const-string v1, "com.miui.home"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->l:Ljava/lang/String;

    const-string v1, "com.miui.systemui"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->l:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStop: deviceLimitStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->y:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->y:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->j:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->m:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->c()V

    :cond_2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/j;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/j;-><init>(Lcom/xiaomi/misettings/usagestats/TimeoverActivity;)V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    const-wide/16 v2, 0x12c

    goto :goto_0

    :cond_3
    const-wide/16 v2, 0x0

    :goto_0
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
