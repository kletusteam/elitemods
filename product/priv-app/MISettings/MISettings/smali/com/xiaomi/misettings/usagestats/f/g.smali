.class public Lcom/xiaomi/misettings/usagestats/f/g;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private a:Lcom/xiaomi/misettings/usagestats/f/j;

.field private b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private d:J

.field private e:Z

.field private f:I

.field private g:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:J


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/usagestats/f/j;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/g;->a:Lcom/xiaomi/misettings/usagestats/f/j;

    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/g;->b:Ljava/util/concurrent/ConcurrentHashMap;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->d:J

    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/g;->g:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/g;->c:Ljava/util/ArrayList;

    return-void
.end method

.method private a(IJ)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr p2, v0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->b:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/f/g;->d:J

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/g;->c:Ljava/util/ArrayList;

    return-void
.end method

.method public a(Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/g;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/g;->i()V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/f/g;->e:Z

    return-void
.end method

.method public a(ZJ)V
    .locals 14

    move-object v1, p0

    const/4 v0, 0x0

    iput v0, v1, Lcom/xiaomi/misettings/usagestats/f/g;->f:I

    iget-object v2, v1, Lcom/xiaomi/misettings/usagestats/f/g;->c:Ljava/util/ArrayList;

    monitor-enter v2

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v3, v1, Lcom/xiaomi/misettings/usagestats/f/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-object v3, v1, Lcom/xiaomi/misettings/usagestats/f/g;->b:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v3, :cond_6

    iget-object v3, v1, Lcom/xiaomi/misettings/usagestats/f/g;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, v1, Lcom/xiaomi/misettings/usagestats/f/g;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/d;

    if-nez v4, :cond_1

    goto :goto_0

    :cond_1
    iget-wide v5, v1, Lcom/xiaomi/misettings/usagestats/f/g;->d:J

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v7

    add-long/2addr v5, v7

    iput-wide v5, v1, Lcom/xiaomi/misettings/usagestats/f/g;->d:J

    iget v5, v1, Lcom/xiaomi/misettings/usagestats/f/g;->f:I

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/d;->f()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v1, Lcom/xiaomi/misettings/usagestats/f/g;->f:I

    if-eqz p1, :cond_5

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v6, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    const-wide/16 v8, 0x0

    if-nez v7, :cond_2

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    :cond_2
    iget-object v10, v1, Lcom/xiaomi/misettings/usagestats/f/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v6, v10, :cond_4

    iget-object v10, v1, Lcom/xiaomi/misettings/usagestats/f/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    add-long/2addr v10, v12

    cmp-long v7, p2, v8

    if-lez v7, :cond_3

    cmp-long v7, v10, p2

    if-ltz v7, :cond_3

    move-wide/from16 v10, p2

    :cond_3
    iget-object v7, v1, Lcom/xiaomi/misettings/usagestats/f/g;->c:Ljava/util/ArrayList;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v6, v8}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    iget-object v8, v1, Lcom/xiaomi/misettings/usagestats/f/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_5
    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/b;->b()I

    move-result v5

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v6

    invoke-direct {p0, v5, v6, v7}, Lcom/xiaomi/misettings/usagestats/f/g;->a(IJ)V

    goto/16 :goto_0

    :cond_6
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Lcom/xiaomi/misettings/usagestats/f/j;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->a:Lcom/xiaomi/misettings/usagestats/f/j;

    return-object v0
.end method

.method public b(J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/f/g;->i:J

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/f/g;->h:Z

    return-void
.end method

.method public b(Z)V
    .locals 2

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    invoke-virtual {p0, p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a(ZJ)V

    return-void
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/g;->i()V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->d:J

    return-wide v0
.end method

.method public e()I
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->d:J

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/L;->f(J)I

    move-result v0

    return v0
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->i:J

    return-wide v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->h:Z

    return v0
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/g;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    return-void
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/f/g;->b(Z)V

    return-void
.end method
