.class Lcom/xiaomi/misettings/usagestats/service/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-virtual {v2}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/utils/j;->g(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    const-string v3, "BizSvr_cate_service"

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->d()Z

    move-result v4

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/a/p;->a()Lcom/xiaomi/misettings/usagestats/b/a/p;

    move-result-object v5

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-virtual {v6}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/xiaomi/misettings/usagestats/b/a/p;->b(Landroid/content/Context;)V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-static {v6}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->b(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Ljava/util/Map;

    move-result-object v6

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-virtual {v7}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v5, v4}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-static {v6}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Ljava/util/Map;

    move-result-object v6

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-virtual {v7}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v5}, Lcom/xiaomi/misettings/usagestats/utils/j;->e(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "init: mCategoryRegisterTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "init: val="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-static {v7}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->b(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v7, "__"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-static {v7}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->c(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-virtual {v6}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v5}, Lcom/xiaomi/misettings/usagestats/b/a/g;->e(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    invoke-static {v7}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->d(Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_1
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/service/b;->a:Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/xiaomi/misettings/usagestats/service/AppCategoryLimitService;->a(Z)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init: duration="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
