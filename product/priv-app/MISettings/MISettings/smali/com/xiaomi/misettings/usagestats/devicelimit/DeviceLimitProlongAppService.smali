.class public Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;
.super Landroid/app/Service;


# static fields
.field private static a:Landroid/app/PendingIntent;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/app/NotificationManager;

.field private d:Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;

.field private e:Landroid/os/Handler;

.field private f:Landroid/os/HandlerThread;

.field private g:I

.field private h:Ljava/lang/Runnable;

.field private i:Lmiui/process/IForegroundInfoListener$Stub;

.field private j:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->c:Landroid/app/NotificationManager;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->g:I

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/f;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/f;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->h:Ljava/lang/Runnable;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/h;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/h;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->i:Lmiui/process/IForegroundInfoListener$Stub;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/i;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/i;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->j:Ljava/lang/Runnable;

    return-void
.end method

.method private a()I
    .locals 1

    const v0, 0x15180

    return v0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->g:I

    return p1
.end method

.method private a(Ljava/lang/String;I)Landroid/app/Notification;
    .locals 6
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x1a
    .end annotation

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.android.settings.prolong"

    invoke-direct {v0, v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const p1, 0x7f13035a

    invoke-virtual {p0, p1, v2}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-array p1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    const v5, 0x7f11002b

    invoke-virtual {v2, v5, p2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v3

    const p2, 0x7f130358

    invoke-virtual {p0, p2, p1}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const p1, 0x7f08018b

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    invoke-direct {p0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    const p1, 0x7f08018f

    invoke-static {p0, p1}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/drawable/Icon;)Landroid/app/Notification$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    const/4 p1, -0x1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 2

    sget-object p1, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a:Landroid/app/PendingIntent;

    if-nez p1, :cond_0

    new-instance p1, Landroid/content/Intent;

    const-string v0, "miui.action.usagestas.MAIN"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "com.xiaomi.misettings"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x1

    const/high16 v1, 0x4000000

    invoke-static {p0, v0, p1, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a:Landroid/app/PendingIntent;

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a:Landroid/app/PendingIntent;

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->d:Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->d:Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a(Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b:Ljava/util/List;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;

    iget-object v3, v2, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->a:Ljava/lang/String;

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v2

    :cond_2
    return-object v1
.end method

.method private a(Landroid/app/NotificationManager;)V
    .locals 4
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x1a
    .end annotation

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->d(Landroid/content/Context;)Landroid/app/NotificationChannelGroup;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/NotificationManager;->createNotificationChannelGroup(Landroid/app/NotificationChannelGroup;)V

    new-instance v0, Landroid/app/NotificationChannel;

    const v1, 0x7f13035d

    invoke-virtual {p0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.android.settings.prolong"

    const/4 v3, 0x2

    invoke-direct {v0, v2, v1, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->enableLights(Z)V

    const-string v1, "app_timer"

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->setGroup(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 6

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "packageName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    const/4 v1, 0x0

    const-string v2, "prolongTime"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    if-nez p1, :cond_2

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;

    move-result-object p1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->c(JLjava/lang/String;)I

    move-result p1

    :cond_2
    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;

    move-result-object v1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->d(JLjava/lang/String;)J

    move-result-wide v1

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a(Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initStart: packageName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ",prolongTime="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ",startProlongTimeStamp="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "DeviceLimitProlongAppService"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->c()V

    const v4, 0x7fffffff

    if-eq p1, v4, :cond_6

    const/4 v4, -0x2

    if-eq p1, v4, :cond_6

    if-nez p1, :cond_3

    goto :goto_0

    :cond_3
    if-eqz v3, :cond_4

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_4
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b:Ljava/util/List;

    new-instance v4, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;

    invoke-direct {v4, v0, p1, v1, v2}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;-><init>(Ljava/lang/String;IJ)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p0}, Lcom/misettings/common/utils/f;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initStart: topAppPkg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->e:Landroid/os/Handler;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->h:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_5
    return-void

    :cond_6
    :goto_0
    if-eqz v3, :cond_7

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b:Ljava/util/List;

    invoke-interface {p1, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_7
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a(Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;Z)V

    return-void
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;Z)V
    .locals 3

    const v0, 0x10087

    const-string v1, "DeviceLimitProlongAppService"

    if-nez p2, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a(Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;)Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startMonitorAndShowNotification: showNotification remainTime ="

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->d:I

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->c:Landroid/app/NotificationManager;

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->a:Ljava/lang/String;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->d:I

    invoke-direct {p0, v1, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a(Ljava/lang/String;I)Landroid/app/Notification;

    move-result-object p1

    invoke-virtual {p2, v0, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->e:Landroid/os/Handler;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->h:Ljava/lang/Runnable;

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_0
    const-string p1, "startMonitorAndShowNotification: hid notification"

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->c:Landroid/app/NotificationManager;

    invoke-virtual {p1, v0}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->e:Landroid/os/Handler;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->h:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->a:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->j(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    const v3, 0x7fffffff

    if-eqz v2, :cond_1

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->g(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/L;->f(J)I

    move-result v3

    sub-int v3, v2, v3

    :cond_1
    invoke-virtual {p1, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->a(Landroid/content/Context;)I

    move-result v2

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->g:I

    if-ne v2, v4, :cond_2

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Landroid/content/Context;)V

    :cond_2
    iput v2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->g:I

    const-string v4, "DeviceLimitProlongAppService"

    if-le v2, v3, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "shouldShowNotification: app limit is running limitRemainTime="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "prolongLimitTime="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->c()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return v0

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "remainTime"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-gtz v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "shouldShowNotification: time over ,packageName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;

    move-result-object v2

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/ProlongAppInfo;->a:Ljava/lang/String;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v3

    invoke-virtual {v2, p1, v3, v4}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->a(Ljava/lang/String;J)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/i/h;->b(Landroid/content/Context;)V

    invoke-static {p0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/app/Service;->startActivity(Landroid/content/Intent;)V

    return v0

    :cond_4
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a()I

    move-result p1

    if-le v2, p1, :cond_5

    return v0

    :cond_5
    const/4 p1, 0x1

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->e:Landroid/os/Handler;

    return-object p0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b:Ljava/util/List;

    return-object p0
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b:Ljava/util/List;

    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 2
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x1a
    .end annotation

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/app/Service;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->c:Landroid/app/NotificationManager;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->c:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->a(Landroid/app/NotificationManager;)V

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->c()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "prolong app monitor..."

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->f:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->f:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->f:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->e:Landroid/os/Handler;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->i:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/ProcessManagerDelegate;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->b()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->e:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->f:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->i:Lmiui/process/IForegroundInfoListener$Stub;

    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/delegate/ProcessManagerDelegate;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    :cond_2
    return-void
.end method

.method public onLowMemory()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onLowMemory()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 0

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;->e:Landroid/os/Handler;

    new-instance p3, Lcom/xiaomi/misettings/usagestats/devicelimit/j;

    invoke-direct {p3, p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/j;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/DeviceLimitProlongAppService;Landroid/content/Intent;)V

    invoke-virtual {p2, p3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 p1, 0x1

    return p1
.end method
