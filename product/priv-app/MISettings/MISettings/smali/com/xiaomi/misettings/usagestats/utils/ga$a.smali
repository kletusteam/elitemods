.class public Lcom/xiaomi/misettings/usagestats/utils/ga$a;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/utils/ga;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/os/Looper;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleMessage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UsagestatsDataThread"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/E;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/b/b;->d(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/l;

    iget p1, p1, Landroid/os/Message;->what:I

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(I)V

    goto :goto_0

    :pswitch_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/b/b;->c(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/l;

    goto :goto_0

    :pswitch_3
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/a;->a()Lcom/xiaomi/misettings/usagestats/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/b/a;->a(Landroid/content/Context;)V

    iget p1, p1, Landroid/os/Message;->what:I

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(I)V

    goto :goto_0

    :pswitch_4
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/b/b;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/l;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a()Ljava/lang/Runnable;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/b/b;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/l;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/b/b;->d(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/l;

    iget p1, p1, Landroid/os/Message;->what:I

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(I)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    return-void

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/ga$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Landroid/content/Context;)V

    :cond_2
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
