.class public Lcom/xiaomi/misettings/usagestats/home/category/a/h;
.super Lcom/xiaomi/misettings/usagestats/d/b/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/usagestats/d/b/b<",
        "Lb/c/a/a/a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

.field private g:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/b/b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const p1, 0x7f0b01a2

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->b:Landroid/widget/ImageView;

    const p1, 0x7f0b01be

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->c:Landroid/widget/TextView;

    const p1, 0x7f0b0213

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->d:Landroid/widget/TextView;

    const p1, 0x7f0b0214

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->e:Landroid/widget/TextView;

    const p1, 0x7f0b0172

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->f:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const p1, 0x7f0b01d8

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->g:Landroid/widget/TextView;

    return-void
.end method

.method private a(Ljava/util/List;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, -0x1

    move v2, v0

    move v3, v1

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    if-ne v3, v1, :cond_0

    move v2, v0

    move v3, v2

    goto :goto_1

    :cond_0
    move v2, v0

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    return v2
.end method

.method private a(JJ)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/home/category/c/e;)V
    .locals 8

    iget-wide v0, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->h:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const-wide/16 v2, 0x3c

    div-long/2addr v0, v2

    long-to-int v0, v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    iget-object v2, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->b:Ljava/lang/String;

    iget-boolean v3, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->a:Z

    const/4 v4, 0x1

    xor-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->b:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    const/4 v2, 0x0

    if-ne p1, v4, :cond_0

    move p1, v4

    goto :goto_0

    :cond_0
    move p1, v2

    :goto_0
    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->g:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    sub-int/2addr v1, v0

    if-gtz v0, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    const v0, 0x7f130421

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    if-gtz v1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    const v0, 0x7f13041b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    const v0, 0x7f1303d3

    new-array v3, v4, [Ljava/lang/Object;

    int-to-long v4, v1

    sget-wide v6, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v4, v6

    invoke-static {p1, v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->g:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V
    .locals 4

    check-cast p2, Lcom/xiaomi/misettings/usagestats/home/category/c/e;

    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->a(Lcom/xiaomi/misettings/usagestats/home/category/c/e;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->b:Landroid/widget/ImageView;

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    iget-object v0, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->b:Ljava/lang/String;

    invoke-static {p3, v0}, Lcom/xiaomi/misettings/usagestats/b/a/g;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->c:Landroid/widget/TextView;

    iget-object p3, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->c:Ljava/lang/String;

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-wide v0, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->h:J

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->d:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-static {p3, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean p1, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->a:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->f:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const/16 p3, 0x11

    invoke-virtual {p1, p3}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setBarType(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->f:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iget-object p3, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->f:Ljava/util/List;

    invoke-virtual {p1, p3}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setCategoryDayUsageList(Ljava/util/List;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->e:Landroid/widget/TextView;

    iget-wide p2, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->i:J

    invoke-direct {p0, v0, v1, p2, p3}, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->a(JJ)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->f:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const/16 p3, 0x12

    invoke-virtual {p1, p3}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setBarType(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->f:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iget-object p3, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->g:Ljava/util/List;

    invoke-virtual {p1, p3}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setCategoryWeekUsageList(Ljava/util/List;)V

    iget-object p1, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/e;->g:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->a(Ljava/util/List;)I

    move-result p1

    int-to-long p2, p1

    cmp-long v2, v0, p2

    if-gez v2, :cond_1

    move-wide p2, v0

    goto :goto_0

    :cond_1
    div-long p2, v0, p2

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bindView: totalUsageTime = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ",exactUsageDays="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "CategoryHeaderViewHolder"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, 0x0

    cmp-long p1, p2, v0

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    const v1, 0x7f130395

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0, p2, p3}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->e:Landroid/widget/TextView;

    const-string p2, ""

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

.method public bridge synthetic a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V
    .locals 0

    check-cast p2, Lb/c/a/a/a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/home/category/a/h;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V

    return-void
.end method
