.class Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field private static a:I = 0x14


# instance fields
.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/os/Handler;

.field private d:I

.field private e:J


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;Landroid/os/Looper;)V
    .locals 1

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->c:Landroid/os/Handler;

    const/4 p2, 0x0

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->d:I

    new-instance p2, Ljava/lang/ref/WeakReference;

    invoke-direct {p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->b:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic a()I
    .locals 1

    sget v0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->a:I

    return v0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->d:I

    return p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->d:I

    return p1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;

    invoke-virtual {v0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    iget p1, p1, Landroid/os/Message;->what:I

    if-eqz p1, :cond_5

    const/4 v2, 0x1

    if-eq p1, v2, :cond_4

    const/4 v2, 0x2

    if-eq p1, v2, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->o(Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->e:J

    goto :goto_0

    :cond_3
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->d:I

    sget v2, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->a:I

    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->e:J

    invoke-static {v1, p1, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;IIJ)Ljava/util/List;

    move-result-object p1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->c:Landroid/os/Handler;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/y;

    invoke-direct {v2, p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/y;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_4
    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->v(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;->getAddUpCount()J

    move-result-wide v1

    long-to-int v1, v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;->a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->c:Landroid/os/Handler;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/z;

    invoke-direct {v2, p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/z;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment;Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusHeaderData;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_5
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusRecordsFragment$a;->c:Landroid/os/Handler;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_6
    :goto_0
    return-void
.end method
