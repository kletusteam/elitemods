.class public Lcom/xiaomi/misettings/usagestats/b/a/a;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/a;->a:I

    const-string v0, "version"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/a;->a:I

    const-string v0, "category"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Lorg/json/JSONObject;)Landroid/util/ArrayMap;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/a;->b:Landroid/util/ArrayMap;

    const-string v0, "specialApps"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/b/a/a;->c:Ljava/util/List;

    return-void
.end method

.method private a(Lorg/json/JSONObject;)Landroid/util/ArrayMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    if-eqz p1, :cond_0

    const-string v1, "category_photo"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_photo"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_game"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_game"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_reading"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_reading"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_medicine"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_medicine"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_tools"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_tools"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_life"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_life"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_sport"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_sport"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_travel"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_travel"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_education"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_education"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_financial"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_financial"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_productivity"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_productivity"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_social"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_social"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_undefined"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_undefined"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_entainment"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_entainment"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_system"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_system"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_shopping"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_shopping"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_video_etc"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "usagestats_app_category_miui_video_etc"

    invoke-direct {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "category_news"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    const-string v1, "usagestats_app_category_miui_news"

    invoke-direct {p0, v0, v1, p1}, Lcom/xiaomi/misettings/usagestats/b/a/a;->a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_0
    return-object v0
.end method

.method private a(Landroid/util/ArrayMap;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1, p2, p3}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private c(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/a;->a:I

    return v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/b/a/a;->b:Landroid/util/ArrayMap;

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/b/a/a;->b:Landroid/util/ArrayMap;

    invoke-virtual {v3, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v2

    :cond_2
    return-object v1
.end method
