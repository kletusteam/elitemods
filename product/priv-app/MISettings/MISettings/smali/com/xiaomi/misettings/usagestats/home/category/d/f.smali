.class public Lcom/xiaomi/misettings/usagestats/home/category/d/f;
.super Ljava/lang/Object;


# direct methods
.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/category/c/g;Lcom/xiaomi/misettings/usagestats/home/category/c/g;)I
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->c:Ljava/lang/String;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->c:Ljava/lang/String;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/b/a$a;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/xiaomi/misettings/usagestats/d/c;->d:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/b/a/g;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/b/a$a;

    if-nez v4, :cond_2

    new-instance v4, Lcom/xiaomi/misettings/usagestats/b/a$a;

    invoke-direct {v4}, Lcom/xiaomi/misettings/usagestats/b/a$a;-><init>()V

    invoke-virtual {v4, v3}, Lcom/xiaomi/misettings/usagestats/b/a$a;->b(Ljava/lang/String;)V

    invoke-static {p0, v3}, Lcom/xiaomi/misettings/usagestats/b/a/g;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/xiaomi/misettings/usagestats/b/a$a;->c(Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {v4, v2}, Lcom/xiaomi/misettings/usagestats/b/a$a;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object v0
.end method

.method private static a()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "misettings.action.FORCE_NOTIFY_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/xiaomi/misettings/Application;->c()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v1

    invoke-virtual {v1, v0}, La/m/a/b;->a(Landroid/content/Intent;)Z

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/home/category/c/g;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/database/a;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/home/category/database/a;-><init>()V

    iput-object p2, v0, Lcom/xiaomi/misettings/usagestats/home/category/database/a;->c:Ljava/lang/String;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/c/g;->g:Ljava/lang/String;

    iput-object p1, v0, Lcom/xiaomi/misettings/usagestats/home/category/database/a;->b:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/category/d/f;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/home/category/database/a;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/home/category/database/a;)V
    .locals 3

    const-string v0, "CategoryClassifyAdapterUtils"

    :try_start_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/home/category/database/h;

    move-result-object p0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/xiaomi/misettings/usagestats/home/category/database/a;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->a([Lcom/xiaomi/misettings/usagestats/home/category/database/a;)[Ljava/lang/Long;

    move-result-object p0

    array-length p1, p0

    if-lez p1, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/home/category/d/f;->a()V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertCategoryEntity: insertCount="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p0, p0, v2

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const-string p0, "insertCategoryEntity: insert fail"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string p1, "insertCategoryEntity: "

    invoke-static {v0, p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/b/a$a;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/home/category/c/b;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_0

    goto/16 :goto_3

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/b/a$a;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/b/a$a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    invoke-direct {v4}, Lcom/xiaomi/misettings/usagestats/home/category/c/h;-><init>()V

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/b/a$a;->a()I

    move-result v5

    iput v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->g:I

    iput-object v3, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/xiaomi/misettings/usagestats/b/a/g;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->c:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/xiaomi/misettings/usagestats/b/a/g;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->b:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/b/a$a;->b()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-instance v6, Lcom/xiaomi/misettings/usagestats/home/category/c/g;

    invoke-direct {v6}, Lcom/xiaomi/misettings/usagestats/home/category/c/g;-><init>()V

    iput-object v3, v6, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    invoke-static {p0, v5}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->c:Ljava/lang/String;

    invoke-static {p0, v5}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    iput-object v7, v6, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->b:Landroid/graphics/drawable/Drawable;

    iput-object v5, v6, Lcom/xiaomi/misettings/usagestats/home/category/c/g;->g:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->a(Lcom/xiaomi/misettings/usagestats/home/category/c/g;)V

    goto :goto_1

    :cond_2
    iget-object v2, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->j:Ljava/util/List;

    if-eqz v2, :cond_1

    iget v3, v4, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->g:I

    if-lez v3, :cond_1

    sget-object v3, Lcom/xiaomi/misettings/usagestats/home/category/d/a;->a:Lcom/xiaomi/misettings/usagestats/home/category/d/a;

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    :cond_3
    sget-object p1, Lcom/xiaomi/misettings/usagestats/b/a/g;->c:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    new-instance v3, Lcom/xiaomi/misettings/usagestats/home/category/c/h;

    invoke-direct {v3}, Lcom/xiaomi/misettings/usagestats/home/category/c/h;-><init>()V

    iput-object v2, v3, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->a:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/b/a/g;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->c:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/b/a/g;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v3, Lcom/xiaomi/misettings/usagestats/home/category/c/b;->b:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    iput v2, v3, Lcom/xiaomi/misettings/usagestats/home/category/c/h;->g:I

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    :goto_3
    return-object v0
.end method
