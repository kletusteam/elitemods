.class public Lcom/xiaomi/misettings/usagestats/focusmode/b/c;
.super Lcom/xiaomi/misettings/usagestats/d/b/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/usagestats/d/b/b<",
        "Lb/c/a/a/a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0
    .param p2    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/b/b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const p1, 0x7f0b019b

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/c;->b:Landroid/widget/TextView;

    const p1, 0x7f0b01f8

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/c;->c:Landroid/widget/TextView;

    const p1, 0x7f0b018e

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/c;->d:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V
    .locals 4

    check-cast p2, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/c;->b:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;->getTotalTime()I

    move-result v0

    int-to-long v0, v0

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v0, v2

    invoke-static {p3, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/c;->c:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;->getStartTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(J)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/c;->d:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusDetailData;->getEndTime()J

    move-result-wide p2

    invoke-static {p2, p3}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(J)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V
    .locals 0

    check-cast p2, Lb/c/a/a/a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/focusmode/b/c;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V

    return-void
.end method
