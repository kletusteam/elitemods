.class public Lcom/xiaomi/misettings/usagestats/d/e/j;
.super Lcom/xiaomi/misettings/usagestats/d/e/d;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/d/e/a/e;


# instance fields
.field private Ra:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field private Sa:Z

.field private Ta:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/d;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Sa:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->F:Z

    return-void
.end method

.method private e(Ljava/util/List;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, -0x1

    move v2, v0

    move v3, v1

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    if-ne v3, v1, :cond_0

    move v2, v0

    move v3, v2

    goto :goto_1

    :cond_0
    move v2, v0

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    return v2
.end method

.method private r()V
    .locals 12

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    move-wide v6, v3

    move v3, v2

    move-wide v4, v6

    :goto_0
    iget-object v8, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v3, v8, :cond_3

    iget-object v8, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/xiaomi/misettings/usagestats/f/e;

    if-nez v8, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v9

    add-long/2addr v6, v9

    cmp-long v11, v4, v9

    if-gez v11, :cond_1

    move-wide v4, v9

    :cond_1
    iget v9, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_2

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/e;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/e;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v8

    iget-wide v8, v8, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v8, v8, v0

    if-nez v8, :cond_2

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iput-wide v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/j;->e(Ljava/util/List;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAxisYText: exactUsageDays="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "NewCategoryWeekViewRender"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    long-to-float v1, v6

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v1, v3

    int-to-float v0, v0

    div-float/2addr v1, v0

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ta:F

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ta:F

    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    long-to-float v1, v3

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    const/4 v2, 0x1

    :cond_4
    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Sa:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/d;->q()V

    return-void
.end method


# virtual methods
.method public c(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_2
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->C:F

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->z:F

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/j;->r()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->c()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a()V

    :cond_3
    :goto_0
    return-void
.end method

.method protected d(I)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    iget-wide v0, p1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->i:J

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const v0, 0x7f13041c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    sget-object v1, Lcom/xiaomi/misettings/usagestats/d/e/h;->a:Landroid/util/SparseIntArray;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/f/j;->b:I

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected f()F
    .locals 3

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Sa:Z

    if-eqz v0, :cond_0

    const/high16 v0, -0x3d380000    # -100.0f

    return v0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ta:F

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    long-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Z:F

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    sub-float v2, v1, v2

    mul-float/2addr v0, v2

    sub-float/2addr v1, v0

    return v1
.end method

.method protected f(I)F
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    if-nez p1, :cond_0

    const/high16 p1, -0x3d380000    # -100.0f

    return p1

    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v0

    long-to-float p1, v0

    const/high16 v0, 0x3f800000    # 1.0f

    mul-float/2addr p1, v0

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    long-to-float v0, v0

    div-float/2addr p1, v0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Z:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    sub-float v1, v0, v1

    mul-float/2addr p1, v1

    sub-float/2addr v0, p1

    return v0
.end method

.method protected g()F
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ca:F

    return v0
.end method

.method protected j()F
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ba:F

    return v0
.end method

.method protected j(I)V
    .locals 8

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ta:F

    float-to-long v0, v0

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->R:Ljava/lang/String;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v0

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    rem-long v4, v0, v2

    const-wide/32 v6, 0xc350

    cmp-long p1, v4, v6

    if-lez p1, :cond_1

    add-long/2addr v0, v2

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->R:Ljava/lang/String;

    return-void
.end method

.method protected k(I)V
    .locals 7

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const v0, 0x7f130440

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Q:Ljava/lang/String;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/j;->Ra:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const v1, 0x7f130404

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->za:Ljava/text/SimpleDateFormat;

    iget-wide v5, p1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Q:Ljava/lang/String;

    return-void
.end method
