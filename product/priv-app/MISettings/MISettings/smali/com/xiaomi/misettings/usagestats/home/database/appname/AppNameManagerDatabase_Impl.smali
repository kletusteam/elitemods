.class public final Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;
.super Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;


# instance fields
.field private volatile l:Lcom/xiaomi/misettings/usagestats/home/database/appname/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;La/p/a/b;)La/p/a/b;
    .locals 0

    iput-object p1, p0, Landroidx/room/v;->a:La/p/a/b;

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Landroidx/room/v;->h:Ljava/util/List;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Landroidx/room/v;->h:Ljava/util/List;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;La/p/a/b;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroidx/room/v;->a(La/p/a/b;)V

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Landroidx/room/v;->h:Ljava/util/List;

    return-object p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Landroidx/room/v;->h:Ljava/util/List;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Landroidx/room/v;->h:Ljava/util/List;

    return-object p0
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Landroidx/room/v;->h:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method protected a(Landroidx/room/a;)La/p/a/c;
    .locals 4

    new-instance v0, Landroidx/room/x;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/database/appname/e;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/xiaomi/misettings/usagestats/home/database/appname/e;-><init>(Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;I)V

    const-string v2, "aab4cae5a6aaf0306409054de73615ec"

    const-string v3, "1f9d32ad5229e5ef138d14dedd03933d"

    invoke-direct {v0, p1, v1, v2, v3}, Landroidx/room/x;-><init>(Landroidx/room/a;Landroidx/room/x$a;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p1, Landroidx/room/a;->b:Landroid/content/Context;

    invoke-static {v1}, La/p/a/c$b;->a(Landroid/content/Context;)La/p/a/c$b$a;

    move-result-object v1

    iget-object v2, p1, Landroidx/room/a;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, La/p/a/c$b$a;->a(Ljava/lang/String;)La/p/a/c$b$a;

    invoke-virtual {v1, v0}, La/p/a/c$b$a;->a(La/p/a/c$a;)La/p/a/c$b$a;

    invoke-virtual {v1}, La/p/a/c$b$a;->a()La/p/a/c$b;

    move-result-object v0

    iget-object p1, p1, Landroidx/room/a;->a:La/p/a/c$c;

    invoke-interface {p1, v0}, La/p/a/c$c;->a(La/p/a/c$b;)La/p/a/c;

    move-result-object p1

    return-object p1
.end method

.method protected d()Landroidx/room/j;
    .locals 2

    new-instance v0, Landroidx/room/j;

    const-string v1, "app_name"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroidx/room/j;-><init>(Landroidx/room/v;[Ljava/lang/String;)V

    return-object v0
.end method

.method public l()Lcom/xiaomi/misettings/usagestats/home/database/appname/b;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;->l:Lcom/xiaomi/misettings/usagestats/home/database/appname/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;->l:Lcom/xiaomi/misettings/usagestats/home/database/appname/b;

    return-object v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;->l:Lcom/xiaomi/misettings/usagestats/home/database/appname/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/database/appname/d;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/database/appname/d;-><init>(Landroidx/room/v;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;->l:Lcom/xiaomi/misettings/usagestats/home/database/appname/b;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase_Impl;->l:Lcom/xiaomi/misettings/usagestats/home/database/appname/b;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
