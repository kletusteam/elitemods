.class public Lcom/xiaomi/misettings/usagestats/d/e/m;
.super Lcom/xiaomi/misettings/usagestats/d/e/h;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/d/e/a/f;


# instance fields
.field private La:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->F:Z

    return-void
.end method

.method private q()V
    .locals 9

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :goto_0
    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_2

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-virtual {v5}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result v6

    if-ge v4, v6, :cond_0

    invoke-virtual {v5}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result v4

    :cond_0
    invoke-virtual {v5}, Lcom/xiaomi/misettings/usagestats/f/i;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v5

    iget-wide v5, v5, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v5, v6, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v5

    if-eqz v5, :cond_1

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-nez v4, :cond_3

    move v4, v0

    :cond_3
    rem-int/lit8 v1, v4, 0x2

    if-eqz v1, :cond_4

    add-int/lit8 v4, v4, 0x1

    :cond_4
    int-to-long v5, v4

    iput-wide v5, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    const v7, 0x7f11002e

    invoke-virtual {v3, v7, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    div-int/2addr v4, v0

    new-array v6, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v2

    invoke-virtual {v3, v7, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v5

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v3, v7, v2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->t:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    array-length v1, v0

    :goto_1
    if-ge v2, v1, :cond_6

    aget-object v3, v0, v2

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->q:F

    cmpg-float v4, v4, v3

    if-gez v4, :cond_5

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->q:F

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    return-void
.end method


# virtual methods
.method public b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->C:F

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->z:F

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/m;->q()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->c()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->m()V

    :cond_2
    return-void
.end method

.method protected d(I)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/i;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    iget-wide v0, p1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->i:J

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const v0, 0x7f13041c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    sget-object v1, Lcom/xiaomi/misettings/usagestats/d/e/h;->a:Landroid/util/SparseIntArray;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/f/j;->b:I

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected e(I)I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    if-ne v0, p1, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->na:I

    if-nez p1, :cond_0

    const p1, 0x7f06036a

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->na:I

    :cond_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->na:I

    return p1

    :cond_1
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ma:I

    if-nez p1, :cond_2

    const p1, 0x7f06036b

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ma:I

    :cond_2
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ma:I

    return p1
.end method

.method protected f(I)F
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result p1

    int-to-float p1, p1

    const/high16 v0, 0x3f800000    # 1.0f

    mul-float/2addr p1, v0

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    long-to-float v0, v0

    div-float/2addr p1, v0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Z:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    sub-float v1, v0, v1

    mul-float/2addr p1, v1

    sub-float/2addr v0, p1

    return v0
.end method

.method protected g()F
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ca:F

    return v0
.end method

.method protected i(I)I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    if-ne v0, p1, :cond_1

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->na:I

    if-nez p1, :cond_0

    const p1, 0x7f06036a

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->na:I

    :cond_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->na:I

    return p1

    :cond_1
    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->i(I)I

    move-result p1

    return p1
.end method

.method protected j()F
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ba:F

    return v0
.end method

.method protected j(I)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f11002e

    invoke-virtual {v0, v2, p1, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->R:Ljava/lang/String;

    return-void
.end method

.method protected k(I)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/m;->La:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/i;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/i;->a()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->za:Ljava/text/SimpleDateFormat;

    iget-wide v3, p1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const p1, 0x7f130420

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Q:Ljava/lang/String;

    return-void
.end method
