.class public Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;
.super Landroid/view/View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String; = "NewBarChartView"


# instance fields
.field protected b:I

.field protected c:I

.field protected d:F

.field protected e:F

.field protected f:I

.field protected g:I

.field protected h:I

.field protected i:I

.field protected j:I

.field protected k:I

.field protected l:I

.field private m:Ljava/lang/String;

.field private n:I

.field private o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

.field private p:Lcom/xiaomi/misettings/usagestats/d/e/h;

.field private q:I

.field private r:Lcom/xiaomi/misettings/usagestats/d/c/a;

.field private s:I

.field private t:I

.field public u:I

.field private v:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, -0x1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->u:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->v:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->c()V

    return-void
.end method

.method private c()V
    .locals 0

    return-void
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;-><init>()V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->b:I

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->a:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->c:I

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->b:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->i:I

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->e:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->h:I

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->f:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->f:I

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->c:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->g:I

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->d:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->j:I

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->g:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->k:I

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->h:I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->c:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->m(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->b:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->t(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->i:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->k:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->h:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->s(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->j:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->r(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->k:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->g:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->q(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->f:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->p(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a()V

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->r:Lcom/xiaomi/misettings/usagestats/d/c/a;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->v:I

    if-eq v1, p1, :cond_0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->v:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->v:I

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/c/a;->a(I)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->d()V

    :cond_0
    return-void
.end method

.method public canScrollHorizontally(I)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(I)Z

    move-result p1

    return p1

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->canScrollHorizontally(I)Z

    move-result p1

    return p1
.end method

.method public canScrollVertically(I)Z
    .locals 0

    invoke-super {p0, p1}, Landroid/view/View;->canScrollVertically(I)Z

    move-result p1

    return p1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v2, "dispatchTouchEvent: move"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->s:I

    sub-int/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->t:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v0, v2, :cond_1

    const/16 v2, 0x19

    if-ge v0, v2, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v2, "dispatchTouchEvent: down"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->s:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->t:I

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :goto_0
    invoke-super {p0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Landroid/graphics/Canvas;)V

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, 0x0

    if-ne v0, v2, :cond_0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->b:I

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->b:I

    move p1, v3

    :goto_0
    if-ne v1, v2, :cond_1

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->c:I

    goto :goto_1

    :cond_1
    iget p2, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->q:I

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->c:I

    move p2, v3

    :goto_1
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->b:I

    int-to-float p1, p1

    const/high16 p2, 0x40000000    # 2.0f

    div-float/2addr p1, p2

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->d:F

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->c:I

    int-to-float p1, p1

    div-float/2addr p1, p2

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->e:F

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->i:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->f:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->g:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->h:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingStart()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->j:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingEnd()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->k:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->c:I

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->i:I

    sub-int/2addr p1, p2

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->c:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->h:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->l:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->d()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a()V

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    if-eqz p1, :cond_3

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->d()V

    :cond_3
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1

    :cond_1
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public setAppUsageList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/a;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/widget/b/a;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/a;->f(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setAppUsageList: WOW ! Set week list fail !!!"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setBarType(I)V
    .locals 3

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->u:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->u:I

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    goto :goto_0

    :pswitch_0
    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/e/j;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/e/j;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    goto :goto_0

    :pswitch_1
    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/e/i;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/e/i;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    goto :goto_0

    :pswitch_2
    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/e/m;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/e/m;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    goto :goto_0

    :pswitch_3
    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/e/k;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/e/k;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    goto :goto_0

    :pswitch_4
    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/e/l;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/e/l;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    goto :goto_0

    :pswitch_5
    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/e/n;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/e/n;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    goto :goto_0

    :pswitch_6
    new-instance v1, Lcom/xiaomi/misettings/usagestats/widget/b/r;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/r;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    goto :goto_0

    :pswitch_7
    new-instance v1, Lcom/xiaomi/misettings/usagestats/widget/b/h;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/h;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    goto :goto_0

    :pswitch_8
    new-instance v1, Lcom/xiaomi/misettings/usagestats/widget/b/p;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/p;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    goto :goto_0

    :pswitch_9
    new-instance v1, Lcom/xiaomi/misettings/usagestats/widget/b/s;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/s;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    goto :goto_0

    :pswitch_a
    new-instance v1, Lcom/xiaomi/misettings/usagestats/widget/b/o;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/o;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    goto :goto_0

    :pswitch_b
    new-instance v1, Lcom/xiaomi/misettings/usagestats/widget/b/a;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/a;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->d()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->l(I)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->l()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->d()V

    :cond_2
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x10
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setCategoryDataList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/j;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/widget/b/j;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/j;->b(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setCategoryDataList: WOW ! Set one day list fail !!!"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setCategoryDayUsageList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/d/e/a/a;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/e/a/a;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/a/a;->d(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setCategoryDayUsageList: WOW ! ensure init render correctly"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setCategoryWeekUsageList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/d/e/a/e;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/e/a/e;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/a/e;->c(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setCategoryWeekUsageList: WOW ! ensure init render correctly"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setDayUnlockList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/d/e/a/b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/e/a/b;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/a/b;->a(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setDayUnlockList: WOW ! ensure init render correctly"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setDayUsageStat(Lcom/xiaomi/misettings/usagestats/f/g;Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/d/e/a/c;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/e/a/c;

    invoke-interface {v0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/e/a/c;->a(Lcom/xiaomi/misettings/usagestats/f/g;Z)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string p2, "setDayUsageStat: WOW ! ensure init render correctly"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setDeviceUsageList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/n;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/widget/b/n;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/n;->d(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setDeviceUsageList: WOW ! Set week list fail !!!"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setInterceptSwitchIndex()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->o()V

    return-void
.end method

.method public setOnItemClickListener(Lcom/xiaomi/misettings/usagestats/d/c/a;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->r:Lcom/xiaomi/misettings/usagestats/d/c/a;

    return-void
.end method

.method public setOneAppOneDayList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/l;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/widget/b/l;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/l;->a(Ljava/lang/String;)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->n:I

    invoke-interface {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/l;->a(I)V

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/l;->a(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setOneAppOneDayList: WOW ! Set week list fail !!!"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setOneAppWeekList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/l;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/widget/b/l;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/l;->a(Ljava/lang/String;)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->n:I

    invoke-interface {v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/l;->a(I)V

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/l;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setOneAppWeekList: WOW ! Set week list fail !!!"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setOneDayDataList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/n;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/widget/b/n;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/n;->a(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setDeviceUsageList: WOW ! Set week list fail !!!"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setOneDayTimeUsage(Lcom/xiaomi/misettings/usagestats/f/g;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/m;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/widget/b/m;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/m;->a(Lcom/xiaomi/misettings/usagestats/f/g;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setOneAppOneDayList: WOW ! Set week list fail !!!"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setOneWeekTimeUsage(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/widget/b/m;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/widget/b/m;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/m;->c(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setOneAppOneDayList: WOW ! Set week list fail !!!"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->m:Ljava/lang/String;

    return-void
.end method

.method public setRemainTime(I)V
    .locals 1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->n:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    instance-of v0, p1, Lcom/xiaomi/misettings/usagestats/widget/b/l;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/widget/b/l;

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->n:I

    invoke-interface {p1, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/l;->a(I)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setOneAppWeekList: WOW ! Set week list fail !!!"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setWeekData(Z)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->o:Lcom/xiaomi/misettings/usagestats/widget/b/g;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Z)V

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setWeekUnlockList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/i;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/d/e/a/f;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/e/a/f;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/a/f;->b(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string v0, "setDayUnlockList: WOW ! ensure init render correctly"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setWeekUsageStat(Ljava/util/List;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;Z)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->p:Lcom/xiaomi/misettings/usagestats/d/e/h;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/d/e/a/d;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/e/a/d;

    invoke-interface {v0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/e/a/d;->a(Ljava/util/List;Z)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a:Ljava/lang/String;

    const-string p2, "setWeekUsageStat: WOW ! ensure init render correctly"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method
