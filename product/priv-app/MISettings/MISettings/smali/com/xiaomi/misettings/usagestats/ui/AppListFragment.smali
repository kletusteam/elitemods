.class public Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;
.super Lcom/xiaomi/misettings/base/BaseFragment;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/b/a$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$a;,
        Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$b;
    }
.end annotation


# instance fields
.field private c:Lcom/xiaomi/misettings/widget/CustomListView;

.field private d:Ljava/lang/String;

.field private e:Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$b;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;->f:Ljava/util/ArrayList;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e014a

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/misettings/common/utils/n;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/a;->a()Lcom/xiaomi/misettings/usagestats/b/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/b/a;->b(Lcom/xiaomi/misettings/usagestats/b/a$b;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;->e:Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    const-string v0, "key_pkg_list"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v1, v0, Ljava/util/ArrayList;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void

    :cond_0
    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;->f:Ljava/util/ArrayList;

    const-string v0, "key_category_name"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;->d:Ljava/lang/String;

    const p2, 0x7f0b03aa

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/widget/CustomListView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;->c:Lcom/xiaomi/misettings/widget/CustomListView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;->e:Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$b;

    if-nez p1, :cond_1

    new-instance p1, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$b;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;->f:Ljava/util/ArrayList;

    invoke-direct {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$b;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;->e:Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$b;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;->c:Lcom/xiaomi/misettings/widget/CustomListView;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;->e:Lcom/xiaomi/misettings/usagestats/ui/AppListFragment$b;

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;->c:Lcom/xiaomi/misettings/widget/CustomListView;

    new-instance p2, Lcom/xiaomi/misettings/usagestats/ui/n;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/usagestats/ui/n;-><init>(Lcom/xiaomi/misettings/usagestats/ui/AppListFragment;)V

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/a;->a()Lcom/xiaomi/misettings/usagestats/b/a;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/xiaomi/misettings/usagestats/b/a;->a(Lcom/xiaomi/misettings/usagestats/b/a$b;)V

    return-void
.end method
