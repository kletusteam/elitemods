.class Lcom/xiaomi/misettings/d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/Application;->d()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/Application;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/Application;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/d;->a:Lcom/xiaomi/misettings/Application;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/a/p;->a()Lcom/xiaomi/misettings/usagestats/b/a/p;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/d;->a:Lcom/xiaomi/misettings/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/b/a/p;->a(Landroid/content/Context;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/d;->a:Lcom/xiaomi/misettings/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->e(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/d;->a:Lcom/xiaomi/misettings/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/d;->a:Lcom/xiaomi/misettings/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/content/Context;)V

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/tools/f;->a()Lcom/xiaomi/misettings/tools/f;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/d;->a:Lcom/xiaomi/misettings/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/tools/f;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/d;->a:Lcom/xiaomi/misettings/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/h/f;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/d;->a:Lcom/xiaomi/misettings/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->x(Landroid/content/Context;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/d;->a:Lcom/xiaomi/misettings/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "MiSettingsApplication"

    const-string v2, "init: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    :goto_0
    return-void
.end method
