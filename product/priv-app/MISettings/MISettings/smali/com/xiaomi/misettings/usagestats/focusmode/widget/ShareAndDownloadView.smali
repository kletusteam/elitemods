.class public Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;
.super Landroid/widget/LinearLayout;


# static fields
.field private static a:I

.field private static b:I


# instance fields
.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->b()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->f:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->f:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->e:Landroid/view/View;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->c()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->e:Landroid/view/View;

    instance-of v1, v0, Landroidx/cardview/widget/CardView;

    const/high16 v2, 0x40000000    # 2.0f

    if-eqz v1, :cond_1

    check-cast v0, Landroidx/cardview/widget/CardView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/cardview/widget/CardView;->setRadius(F)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->f:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/misettings/common/utils/d;->a(Landroid/view/View;Ljava/lang/String;F)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07009c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroidx/cardview/widget/CardView;->setRadius(F)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->f:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/misettings/common/utils/d;->a(Landroid/view/View;Ljava/lang/String;F)V

    :goto_0
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    sget v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sget v2, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->b:I

    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    sget v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a:I

    sget v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->b:I

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->g:Ljava/lang/String;

    return-object p0
.end method

.method private b()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FocusMode_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->g:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0094

    invoke-static {v0, v1, p0}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0b0179

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->d:Landroid/view/View;

    const v0, 0x7f0b017a

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->d:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->setClickState(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->c:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->setClickState(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->c:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/g;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/g;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->d:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/h;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private c()V
    .locals 2

    sget v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a:I

    if-eqz v0, :cond_0

    sget v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->b:I

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->getWH()V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->e:Landroid/view/View;

    const v1, 0x7f0b01ee

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->setLayoutHeight(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->e:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->d()V

    return-void
.end method

.method private d()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a()V

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->f:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f1303d4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "123"

    const-string v4, "789"

    invoke-static {v0, v1, v3, v2, v4}, Lcom/misettings/common/utils/f;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a()V

    return-void
.end method

.method private e()V
    .locals 3

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f13037a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->e()V

    return-void
.end method

.method private getWH()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->getWHNew()V

    return-void
.end method

.method private getWHNew()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/f;->j(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->a(Landroid/content/Context;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    sput v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a:I

    sget v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a:I

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/BaseShareFragment;->b(Landroid/content/Context;)F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->b:I

    return-void
.end method

.method private setClickState(Landroid/view/View;)V
    .locals 5

    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v1

    const v3, 0x3f4ccccd    # 0.8f

    new-array v0, v0, [Lmiuix/animation/m$b;

    sget-object v4, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    aput-object v4, v0, v2

    invoke-interface {v1, v3, v0}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    const v0, 0x3e99999a    # 0.3f

    const/4 v3, 0x0

    invoke-interface {v1, v0, v3, v3, v3}, Lmiuix/animation/m;->a(FFFF)Lmiuix/animation/m;

    new-array v0, v2, [Lmiuix/animation/a/a;

    invoke-interface {v1, p1, v0}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private setLayoutHeight(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    sget v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->a:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    sget v1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->b:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public setPageName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->h:Ljava/lang/String;

    return-void
.end method

.method public setView(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/ShareAndDownloadView;->e:Landroid/view/View;

    return-void
.end method
