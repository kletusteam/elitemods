.class public Lcom/xiaomi/misettings/usagestats/d/a/a/u;
.super Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;


# instance fields
.field private c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

.field private d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/LinearLayout;

.field private i:Lcom/xiaomi/misettings/usagestats/d/a/a;

.field private j:I

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/xiaomi/misettings/usagestats/d/d/i;

.field private m:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object p1

    check-cast p1, Ljava/text/SimpleDateFormat;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->m:Ljava/text/SimpleDateFormat;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->m:Ljava/text/SimpleDateFormat;

    const-string p2, "M.d"

    invoke-virtual {p1, p2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    const p1, 0x7f0b0173

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    const p1, 0x7f0b0172

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const p1, 0x7f0b020d

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->e:Landroid/widget/TextView;

    const p1, 0x7f0b0200

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->f:Landroid/widget/TextView;

    const p1, 0x7f0b0202

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->g:Landroid/widget/TextView;

    const p1, 0x7f0b0201

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->h:Landroid/widget/LinearLayout;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const/4 p2, 0x7

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setBarType(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    new-instance p2, Lcom/xiaomi/misettings/usagestats/d/a/a/a;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/a;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/u;)V

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a(Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart$a;)V

    return-void
.end method

.method private a(Ljava/util/List;I)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ">;I)V"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/m;->j()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v5, 0x7f13039b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v6, 0x7f1303b8

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/m;->e()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/m;->c()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/a;->b(Ljava/util/List;)[J

    move-result-object p1

    aget-wide v5, p1, v4

    sget-wide v7, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    rem-long v9, v5, v7

    const-wide/16 v11, 0x0

    cmp-long v3, v9, v11

    if-eqz v3, :cond_2

    div-long/2addr v5, v7

    const-wide/16 v9, 0x1

    add-long/2addr v5, v9

    mul-long/2addr v5, v7

    aput-wide v5, p1, v4

    :cond_2
    const-string v3, "0"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v5, 0x7f130394

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    aget-wide v5, p1, v4

    invoke-static {v3, v5, v6}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->setXLabel(Ljava/util/List;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {p1, v1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->setYData(Ljava/util/List;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {p1, v2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->setYLabel(Ljava/util/List;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->a()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->l:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-boolean p2, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->d:Z

    if-nez p2, :cond_3

    iget-boolean p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz p1, :cond_4

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    iput-boolean v4, p1, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->G:Z

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->c()V

    :cond_4
    return-void
.end method

.method private e(I)V
    .locals 10

    const/16 v0, 0x8

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->g:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->k:Ljava/util/List;

    add-int/lit8 v2, p1, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/m;->c()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-nez v3, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->g:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x3

    const-wide/16 v4, 0x7

    if-ne p1, v3, :cond_2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->k:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/m;->c()J

    move-result-wide v6

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->f()I

    move-result p1

    int-to-long v8, p1

    div-long/2addr v6, v8

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->k:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/m;->c()J

    move-result-wide v6

    div-long/2addr v6, v4

    :goto_0
    div-long/2addr v1, v4

    sub-long v3, v6, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    long-to-float p1, v3

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr p1, v3

    long-to-float v3, v1

    div-float/2addr p1, v3

    new-instance v3, Ljava/text/DecimalFormat;

    const-string v4, "0.0"

    invoke-direct {v3, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr p1, v4

    float-to-double v4, p1

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    cmp-long v1, v6, v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f1303af

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v2

    invoke-virtual {v1, v4, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f1303b1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v2

    invoke-virtual {v1, v4, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->g:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method private f(I)V
    .locals 10

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/m;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/m;->g()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->setWeekUsageStat(Ljava/util/List;Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->l:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-boolean v1, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->d:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->b()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->l:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iput-boolean v2, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->d:Z

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->e:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/m;->c()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/misettings/common/utils/q;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->f()I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v3, :cond_2

    const-string v4, " "

    const-string v6, "-"

    if-ne p1, v5, :cond_1

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->m:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/m;->f()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->m:Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->m:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/m;->f()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->m:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/m;->d()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    :goto_0
    if-ne p1, v5, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/m;->c()J

    move-result-wide v4

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->f()I

    move-result v0

    int-to-long v6, v0

    div-long/2addr v4, v6

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/m;->c()J

    move-result-wide v4

    const-wide/16 v6, 0x7

    div-long/2addr v4, v6

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {v0, v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v5, 0x7f1303ac

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->e(I)V

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/d/a/a/a/b;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/a/a;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->i:Lcom/xiaomi/misettings/usagestats/d/a/a;

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->j:I

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->l:Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-object p1, p2

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/d/c;

    iget-object p3, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast p3, Ljava/util/List;

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->k:Ljava/util/List;

    invoke-direct {p0, p4}, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->f(I)V

    iget-boolean p3, p1, Lcom/xiaomi/misettings/usagestats/d/d/c;->h:Z

    if-nez p3, :cond_1

    iget-boolean p2, p2, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->c:Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/home/widget/linechart/LineChart;->b()V

    :cond_0
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->k:Ljava/util/List;

    invoke-direct {p0, p2, p4}, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->a(Ljava/util/List;I)V

    const/4 p2, 0x1

    iput-boolean p2, p1, Lcom/xiaomi/misettings/usagestats/d/d/c;->h:Z

    :cond_1
    return-void
.end method

.method public synthetic d(I)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->i:Lcom/xiaomi/misettings/usagestats/d/a/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->e()Lcom/xiaomi/misettings/usagestats/d/a/b;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->i:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->e()Lcom/xiaomi/misettings/usagestats/d/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/b;->a(I)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "callBack: currentIndex"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DetailBarWeekViewHolder"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->f(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->b()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/u;->i:Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(I)V

    return-void
.end method
