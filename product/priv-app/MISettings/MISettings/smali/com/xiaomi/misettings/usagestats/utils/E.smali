.class public Lcom/xiaomi/misettings/usagestats/utils/E;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/lang/String; = "settings_cache_time"


# direct methods
.method public static a(Landroid/content/Context;)V
    .locals 7

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/E;->a:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v0

    const-string v1, "Timer_CacheUtils"

    if-nez v0, :cond_0

    const-string v0, "clearIllegalData: clear"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/xiaomi/misettings/usagestats/utils/E;->a:Ljava/lang/String;

    invoke-static {v4, v5, v2, v3}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    const-wide/16 v4, 0x28

    mul-long/2addr v2, v4

    sub-long v2, v0, v2

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/d;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/d;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v0, v1}, Lcom/xiaomi/misettings/usagestats/a/e;->a(JJ)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/o;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/o;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v0, v1}, Lcom/xiaomi/misettings/usagestats/a/e;->a(JJ)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/q;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/q;

    move-result-object v4

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    add-long/2addr v5, v0

    invoke-virtual {v4, v2, v3, v5, v6}, Lcom/xiaomi/misettings/usagestats/a/e;->a(JJ)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/c;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v0, v1}, Lcom/xiaomi/misettings/usagestats/a/e;->a(JJ)Z

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v0, v1}, Lcom/xiaomi/misettings/usagestats/a/e;->a(JJ)Z

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    const-wide/16 v4, 0x1d

    mul-long/2addr v2, v4

    sub-long/2addr v0, v2

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Landroid/content/Context;J)V

    goto :goto_0

    :cond_0
    const-string p0, "clearIllegalData: finish clear today"

    invoke-static {v1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentHashMap;J)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;J)Z"
        }
    .end annotation

    invoke-static {p0, p2, p3}, Lcom/xiaomi/misettings/usagestats/utils/U;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Ljava/lang/String;Ljava/util/concurrent/ConcurrentHashMap;)Z

    move-result p0

    return p0
.end method

.method public static a(Ljava/util/List;)Z
    .locals 0

    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static a(Ljava/util/Map;)Z
    .locals 0

    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static a(Lorg/json/JSONArray;)Z
    .locals 0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result p0

    if-gtz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 5

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/E;->a(Ljava/util/List;)Z

    move-result v1

    const-string v2, "Timer_CacheUtils"

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/U;->g(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "moveCache: start move cache"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v2

    const/4 v3, 0x1

    const-string v4, "settings_move_success"

    invoke-virtual {v2, v4, v3}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Z)V

    sub-int/2addr v1, v3

    :goto_0
    if-ltz v1, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/j;

    iget-wide v2, v2, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {p0, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/U;->c(Landroid/content/Context;J)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    const-string p0, "moveCache: has move finish "

    invoke-static {v2, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/util/concurrent/ConcurrentHashMap;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;J)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/E;->a(Ljava/util/Map;)Z

    move-result v0

    const-string v1, "Timer_CacheUtils"

    if-eqz v0, :cond_0

    const-string p0, "serializeResult: result is empty"

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/d;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/d;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p1}, Lcom/xiaomi/misettings/usagestats/a/d;->b(JLjava/util/concurrent/ConcurrentHashMap;)Z

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveUsageState: time="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, " result="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_3

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Ljava/util/concurrent/ConcurrentHashMap;)Lorg/json/JSONArray;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/E;->a(Lorg/json/JSONArray;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, ""

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "settings_move_success"

    invoke-virtual {v0, v2, v1}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Z)V

    :cond_2
    invoke-static {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Landroid/content/Context;Ljava/lang/String;J)V

    :cond_3
    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 3

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/j;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/j;

    move-result-object v0

    const-string v1, "app_limit_list"

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/a/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "serializeAppLimitData: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Timer_CacheUtils"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->n(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/j;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/j;

    move-result-object v0

    const-string v1, "app_limit_details"

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/a/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->m(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method
