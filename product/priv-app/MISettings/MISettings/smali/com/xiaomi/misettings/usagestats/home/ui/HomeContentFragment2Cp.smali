.class public Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;
.super Lcom/xiaomi/misettings/base/BaseFragment;


# instance fields
.field private c:Landroidx/fragment/app/Fragment;

.field private d:Landroidx/fragment/app/Fragment;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/view/View;

.field private h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

.field private i:Landroid/content/BroadcastReceiver;

.field private j:I

.field private k:I

.field private l:I

.field private m:Z

.field private n:Z

.field private o:I

.field private p:Lcom/xiaomi/misettings/usagestats/utils/A;

.field private q:Z

.field private r:Z

.field s:Lcom/miui/greenguard/entity/FamilyBean;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->m:Z

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->o:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->q:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->r:Z

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    return p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    return p1
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->initView(Landroid/view/View;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->e:Landroid/widget/TextView;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/v;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/v;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->f:Landroid/widget/TextView;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/w;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/w;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->m:Z

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->j:I

    return p1
.end method

.method private b(Z)I
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz p1, :cond_0

    const p1, 0x7f060364

    goto :goto_0

    :cond_0
    const p1, 0x7f060365

    :goto_0
    invoke-virtual {v0, p1}, Landroid/content/Context;->getColor(I)I

    move-result p1

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)Landroidx/fragment/app/Fragment;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->c:Landroidx/fragment/app/Fragment;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->k:I

    return p1
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->r()V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)Landroidx/fragment/app/Fragment;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->d:Landroidx/fragment/app/Fragment;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->j:I

    return p0
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->g:Landroid/view/View;

    return-object p0
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->o:I

    return p0
.end method

.method static synthetic h(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->q()V

    return-void
.end method

.method static synthetic i(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->m()V

    return-void
.end method

.method private initView(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f0b0204

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->e:Landroid/widget/TextView;

    const v0, 0x7f0b0205

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->f:Landroid/widget/TextView;

    const v0, 0x7f0b01e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->g:Landroid/view/View;

    const v0, 0x7f0b02e1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->h:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    const v0, 0x7f0b01e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/u;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/u;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->r()V

    return-void
.end method

.method public static synthetic j(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->p()V

    return-void
.end method

.method public static synthetic k(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->o()V

    return-void
.end method

.method private l()V
    .locals 8

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->m:Z

    const-string v1, ""

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "misettings_from_page"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "screen_time_home_intent_key"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    move-object v1, v2

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "dayFragment"

    invoke-virtual {v2, v3}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v4

    iput-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->c:Landroidx/fragment/app/Fragment;

    const-string v4, "weekFragment"

    invoke-virtual {v2, v4}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v4

    iput-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->d:Landroidx/fragment/app/Fragment;

    invoke-virtual {v2}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v2

    const v4, 0x7f020001

    const v5, 0x7f020002

    invoke-virtual {v2, v4, v5}, Landroidx/fragment/app/qa;->a(II)Landroidx/fragment/app/qa;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addDayFragment:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->c:Landroidx/fragment/app/Fragment;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "HomeContentFragment2"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->c:Landroidx/fragment/app/Fragment;

    const/4 v6, 0x0

    if-eqz v4, :cond_2

    iget-boolean v7, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->r:Z

    if-eqz v7, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v2, v4}, Landroidx/fragment/app/qa;->e(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    goto :goto_3

    :cond_2
    :goto_1
    const-string v4, "addDayFragment: day null"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->q:Z

    if-eqz v4, :cond_3

    invoke-static {v6, v1, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    move-result-object v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->s:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-static {v6, v0}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->a(ZLcom/miui/greenguard/entity/FamilyBean;)Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->c:Landroidx/fragment/app/Fragment;

    const v0, 0x7f0b019c

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->c:Landroidx/fragment/app/Fragment;

    invoke-virtual {v2, v0, v1, v3}, Landroidx/fragment/app/qa;->a(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/qa;

    :goto_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->d:Landroidx/fragment/app/Fragment;

    if-eqz v0, :cond_4

    invoke-virtual {v2, v0}, Landroidx/fragment/app/qa;->c(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    :cond_4
    invoke-virtual {v2}, Landroidx/fragment/app/qa;->b()I

    iput-boolean v6, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->r:Z

    return-void
.end method

.method private m()V
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->n()V

    :goto_0
    return-void
.end method

.method private n()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "weekFragment"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->d:Landroidx/fragment/app/Fragment;

    const-string v2, "dayFragment"

    invoke-virtual {v0, v2}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->c:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    const v2, 0x7f020001

    const v3, 0x7f020002

    invoke-virtual {v0, v2, v3}, Landroidx/fragment/app/qa;->a(II)Landroidx/fragment/app/qa;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->d:Landroidx/fragment/app/Fragment;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->r:Z

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->o:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->c:Landroidx/fragment/app/Fragment;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->c(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->d:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->e(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    goto :goto_2

    :cond_2
    :goto_0
    const-string v2, "HomeContentFragment2"

    const-string v3, "addDayFragment: week null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->q:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    const-string v2, ""

    invoke-static {v3, v2}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->a(ZLjava/lang/String;)Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    move-result-object v2

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->s:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-static {v3, v2}, Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;->a(ZLcom/miui/greenguard/entity/FamilyBean;)Lcom/xiaomi/misettings/usagestats/home/ui/RemoteSubContentFragment;

    move-result-object v2

    :goto_1
    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->d:Landroidx/fragment/app/Fragment;

    const v2, 0x7f0b019c

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->d:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0, v2, v3, v1}, Landroidx/fragment/app/qa;->a(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/qa;

    :goto_2
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->c:Landroidx/fragment/app/Fragment;

    if-eqz v1, :cond_4

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->c(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    :cond_4
    invoke-virtual {v0}, Landroidx/fragment/app/qa;->b()I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->r:Z

    return-void
.end method

.method private o()V
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->n()V

    :goto_0
    return-void
.end method

.method private p()V
    .locals 3

    :try_start_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->c:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->c:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->d(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {v0}, Landroidx/fragment/app/qa;->b()I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->d:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->d:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/qa;->d(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {v0}, Landroidx/fragment/app/qa;->b()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "HomeContentFragment2"

    const-string v2, "removeDayAndWeekFragment error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private q()V
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->m:Z

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->k()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->m()V

    :cond_0
    return-void
.end method

.method private r()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->e:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    move v1, v3

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->b(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->f:Landroid/widget/TextView;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    if-ne v1, v3, :cond_2

    move v2, v3

    :cond_2
    invoke-direct {p0, v2}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->b(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method


# virtual methods
.method protected b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e001e

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public k()V
    .locals 7

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->n:Z

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "translationX"

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->g:Landroid/view/View;

    new-array v2, v2, [F

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    if-ne v6, v5, :cond_0

    move v6, v4

    goto :goto_0

    :cond_0
    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->k:I

    int-to-float v6, v6

    :goto_0
    aput v6, v2, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    if-ne v1, v5, :cond_1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->k:I

    int-to-float v4, v1

    :cond_1
    aput v4, v2, v5

    invoke-static {v0, v3, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->g:Landroid/view/View;

    new-array v2, v2, [F

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    if-ne v6, v5, :cond_3

    move v6, v4

    goto :goto_1

    :cond_3
    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->k:I

    neg-int v6, v6

    int-to-float v6, v6

    :goto_1
    aput v6, v2, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    if-ne v1, v5, :cond_4

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->k:I

    neg-int v1, v1

    int-to-float v4, v1

    :cond_4
    aput v4, v2, v5

    invoke-static {v0, v3, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    :goto_2
    const-wide/16 v1, 0xf0

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/appcompat/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string p1, "HomeContentFragment2"

    const-string v0, "onConfigurationChanged: "

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->m()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/H;->b()Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->n:Z

    new-instance p1, Lcom/xiaomi/misettings/usagestats/home/ui/t;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/t;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->i:Landroid/content/BroadcastReceiver;

    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "misettings.action.FROM_STEADY_ON"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1, p1}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    new-instance p1, Lcom/xiaomi/misettings/usagestats/utils/A;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/ui/k;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/k;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/j;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/j;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;)V

    invoke-direct {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/A;-><init>(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->p:Lcom/xiaomi/misettings/usagestats/utils/A;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->i:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->p:Lcom/xiaomi/misettings/usagestats/utils/A;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/A;->d()V

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onLowMemory()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->p:Lcom/xiaomi/misettings/usagestats/utils/A;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/A;->a()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->l:I

    const-string v1, "tabIndex"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onStart()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->p:Lcom/xiaomi/misettings/usagestats/utils/A;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/A;->b()V

    const-string v0, "HomeContentFragment2"

    const-string v1, "HomeContentFragment2 : onStart()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onStop()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->p:Lcom/xiaomi/misettings/usagestats/utils/A;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/A;->c()V

    const-string v0, "HomeContentFragment2"

    const-string v1, "HomeContentFragment2 : onStop()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const-string v1, "tabIndex"

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p2

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->o:I

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->a(Landroid/view/View;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "family"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/miui/greenguard/entity/FamilyBean;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->s:Lcom/miui/greenguard/entity/FamilyBean;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->s:Lcom/miui/greenguard/entity/FamilyBean;

    if-eqz p1, :cond_1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->q:Z

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;->m()V

    return-void
.end method
