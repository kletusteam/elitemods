.class public Lcom/xiaomi/misettings/usagestats/dataprovider/a;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/Context;)J
    .locals 3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->c(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v2, "last_request_time"

    invoke-virtual {p0, v2, v0, v1}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Lcom/xiaomi/misettings/usagestats/f/d;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/f/d;
    .locals 3

    if-nez p0, :cond_0

    new-instance p0, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    return-object p0

    :cond_0
    new-instance p1, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/d;->g()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/f/d;->b(J)V

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    iput-wide v0, p1, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    iput-wide v0, p1, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object p1
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)Lcom/xiaomi/misettings/usagestats/f/g;
    .locals 12

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-static {v5, v4}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->a(Lcom/xiaomi/misettings/usagestats/f/d;Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/f/d;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-wide v7, v0, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v11

    move-object v6, p0

    invoke-static/range {v6 .. v11}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJLjava/util/concurrent/ConcurrentHashMap;)V

    const-wide/16 p0, 0x0

    invoke-virtual {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/f/g;->a(J)V

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Lcom/xiaomi/misettings/usagestats/f/g;->b(Z)V

    invoke-virtual {v1, p0}, Lcom/xiaomi/misettings/usagestats/f/g;->a(Z)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;J)V
    .locals 1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->c(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "last_request_time"

    invoke-virtual {p0, v0, p1, p2}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;J)V

    return-void
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->c(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p0

    const-string v0, "last_total_hours"

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static c(Landroid/content/Context;)Lcom/misettings/common/utils/p;
    .locals 1

    const-string v0, "_remote"

    invoke-static {p0, v0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/misettings/common/utils/p;

    move-result-object p0

    return-object p0
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method
