.class Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment$b;
.super Lcom/xiaomi/misettings/tools/i;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/tools/i<",
        "Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/tools/i;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const-string v0, "SubContentFragment"

    invoke-super {p0}, Lcom/xiaomi/misettings/tools/i;->run()V

    :try_start_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/tools/i;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    iget-boolean v1, v1, Lcom/xiaomi/misettings/usagestats/home/ui/BaseSubContentFragment;->i:Z

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/tools/i;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->g(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "queryWeekDataList: duration="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/tools/i;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    invoke-static {v1, v3}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->a(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/tools/i;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->h(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/m;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loadCurrentWeekData: duration="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/tools/i;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;

    invoke-static {v1, v3}, Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;->a(Lcom/xiaomi/misettings/usagestats/home/ui/SubContentFragment;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "LoadDataTaskRunnable run error"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "res:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/tools/i;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
