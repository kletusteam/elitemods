.class public Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;
.super Lcom/xiaomi/misettings/base/BaseFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment$a;
    }
.end annotation


# instance fields
.field private c:Landroidx/viewpager/widget/ViewPager;

.field private d:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/FamilyBean;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/miui/greenguard/entity/FamilyBean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/FamilyBean;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->e:Ljava/util/List;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->d:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/adapter/h;

    invoke-direct {v0, p1}, Lcom/xiaomi/misettings/usagestats/adapter/h;-><init>(Ljava/util/List;)V

    new-instance p1, Lcom/xiaomi/misettings/usagestats/home/ui/l;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/l;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;)V

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/adapter/h;->a(Lcom/xiaomi/misettings/usagestats/adapter/h$b;)V

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/adapter/h;->a()Ljava/util/List;

    move-result-object p1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->d:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->d:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->b(Ljava/util/List;)V

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/FamilyBean;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment$a;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;Landroidx/fragment/app/FragmentManager;)V

    new-instance v3, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;

    invoke-direct {v3}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;-><init>()V

    invoke-virtual {v3, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const/4 v1, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment$a;->a(ILandroidx/fragment/app/Fragment;)V

    if-eqz p1, :cond_0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-virtual {v0}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/io/Serializable;

    const-string v5, "family"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    new-instance v4, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;

    invoke-direct {v4}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentFragment2Cp;-><init>()V

    invoke-virtual {v4, v3}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment$a;->a(ILandroidx/fragment/app/Fragment;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->c:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {p1, v2}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/f;)V

    return-void
.end method

.method private l()V
    .locals 2

    new-instance v0, Lcom/miui/greenguard/params/GetFamilyParam;

    invoke-direct {v0}, Lcom/miui/greenguard/params/GetFamilyParam;-><init>()V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/ui/x;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/ui/x;-><init>(Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;)V

    invoke-static {v0, v1}, Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;

    return-void
.end method


# virtual methods
.method protected b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e001f

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public synthetic c(I)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/greenguard/entity/FamilyBean;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->f:Lcom/miui/greenguard/entity/FamilyBean;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->c:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const p2, 0x7f0b03cb

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroidx/viewpager/widget/ViewPager;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->c:Landroidx/viewpager/widget/ViewPager;

    const p2, 0x7f0b02e1

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->d:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->d:Lcom/xiaomi/misettings/usagestats/home/widget/CustomRecycleView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->b(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/HomeContentWrapFragment;->l()V

    return-void
.end method
