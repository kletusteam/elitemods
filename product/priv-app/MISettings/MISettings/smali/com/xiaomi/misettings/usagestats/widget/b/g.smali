.class public abstract Lcom/xiaomi/misettings/usagestats/widget/b/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/widget/b/k;
.implements Ljava/lang/Runnable;


# static fields
.field public static final a:Ljava/lang/String; = "g"

.field public static final b:Landroid/util/SparseIntArray;


# instance fields
.field protected A:Landroid/graphics/Paint;

.field protected B:F

.field protected C:F

.field protected D:F

.field protected E:F

.field protected F:F

.field protected G:Landroid/graphics/Paint;

.field protected H:F

.field private I:Landroid/graphics/Paint;

.field protected J:I

.field private K:F

.field protected L:Landroid/graphics/RectF;

.field protected M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field protected N:F

.field protected O:Landroid/view/View;

.field private P:F

.field private Q:F

.field private R:F

.field private S:F

.field private T:Z

.field private U:Landroid/animation/ValueAnimator;

.field private V:Landroid/animation/ValueAnimator;

.field private W:Z

.field private X:Landroid/graphics/PointF;

.field private Y:F

.field private Z:Z

.field private aa:Z

.field protected c:Ljava/text/SimpleDateFormat;

.field private d:Landroid/os/Handler;

.field protected e:I

.field protected f:I

.field protected g:I

.field protected h:I

.field protected i:I

.field protected j:I

.field protected k:I

.field protected l:I

.field protected m:I

.field protected n:Landroid/content/Context;

.field protected o:Z

.field protected p:Landroid/graphics/Paint;

.field protected q:Ljava/lang/String;

.field protected r:Ljava/lang/String;

.field protected s:F

.field protected t:F

.field protected u:F

.field protected v:F

.field protected w:Landroid/graphics/Paint;

.field protected x:Landroid/graphics/RectF;

.field protected y:Landroid/graphics/Paint;

.field protected z:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/widget/b/b;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/widget/b/b;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->b:Landroid/util/SparseIntArray;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->d:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->D:F

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/widget/b/g;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->d:Landroid/os/Handler;

    return-object p0
.end method

.method private a(Landroid/animation/ValueAnimator;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    return-void
.end method

.method private a(Landroid/graphics/RectF;)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->X:Landroid/graphics/PointF;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->X:Landroid/graphics/PointF;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->X:Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/RectF;->right:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->X:Landroid/graphics/PointF;

    iget p1, p1, Landroid/graphics/RectF;->top:F

    iput p1, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->l()V

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->u:F

    div-float v0, p1, v3

    add-float/2addr v0, v1

    div-float v2, p1, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    int-to-float v3, v2

    cmpl-float v3, v0, v3

    if-lez v3, :cond_1

    int-to-float v0, v2

    sub-float v1, v0, p1

    :cond_1
    const/4 p1, 0x0

    cmpg-float v2, v1, p1

    if-gez v2, :cond_2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->u:F

    add-float/2addr v0, p1

    move v1, p1

    :cond_2
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->x:Landroid/graphics/RectF;

    if-nez v2, :cond_3

    new-instance v2, Landroid/graphics/RectF;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->v:F

    invoke-direct {v2, p1, p1, p1, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->x:Landroid/graphics/RectF;

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->x:Landroid/graphics/RectF;

    iput v1, p1, Landroid/graphics/RectF;->left:F

    iput v0, p1, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result p1

    const v2, 0x7f0704fb

    if-eqz p1, :cond_4

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result p1

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->Y:F

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result p1

    add-float/2addr v1, p1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->Y:F

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/widget/b/g;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->u(I)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/widget/b/g;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->Z:Z

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/widget/b/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i()V

    return-void
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->G:Landroid/graphics/Paint;

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f(I)Landroid/graphics/Paint$Align;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->p:Landroid/graphics/Paint;

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->b(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->d(I)F

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->C:F

    add-float/2addr v4, v0

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    add-int/lit8 v5, v5, 0x64

    int-to-float v5, v5

    invoke-direct {v2, v0, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->L:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->d(I)F

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->C:F

    add-float/2addr v4, v0

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    int-to-float v5, v5

    invoke-direct {v2, v0, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->L:Landroid/graphics/RectF;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->C:F

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->G:Landroid/graphics/Paint;

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(IF)F

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->K:F

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->G:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g()Z

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->L:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, v3

    add-float/2addr v2, v0

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->B:F

    div-float/2addr v4, v3

    add-float/2addr v2, v4

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->L:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, v3

    add-float/2addr v2, v0

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->B:F

    div-float/2addr v4, v3

    sub-float/2addr v2, v4

    :goto_1
    invoke-virtual {p0, p1, v1, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Landroid/graphics/Canvas;IF)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->M:Ljava/util/List;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->L:Landroid/graphics/RectF;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->B:F

    add-float/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_1
    return-void
.end method

.method private d(Landroid/graphics/Canvas;)V
    .locals 8

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    int-to-float v5, v0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    int-to-float v4, v0

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->I:Landroid/graphics/Paint;

    const/4 v2, 0x0

    move-object v1, p1

    move v3, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->E:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float v6, v0, v1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    int-to-float v5, v0

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->I:Landroid/graphics/Paint;

    const/4 v3, 0x0

    move-object v2, p1

    move v4, v6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->E:F

    sub-float v6, v0, v1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    int-to-float v5, v0

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->I:Landroid/graphics/Paint;

    move v4, v6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private e(Landroid/graphics/Canvas;)V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->x:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->y:Landroid/graphics/Paint;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->X:Landroid/graphics/PointF;

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget v3, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->x:Landroid/graphics/RectF;

    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->y:Landroid/graphics/Paint;

    move-object v1, p1

    move v2, v4

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    const v1, 0x7f06039a

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->s:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    const v3, 0x7f0704fd

    invoke-virtual {p0, v3}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v3

    add-float/2addr v1, v3

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->q:Ljava/lang/String;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->Y:F

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v0, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    const v3, 0x7f06039b

    invoke-virtual {p0, v3}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->t:F

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x3f8b851f    # 1.09f

    invoke-static {v3, v4}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v0, v3

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;)F

    move-result v3

    div-float/2addr v3, v2

    add-float/2addr v0, v3

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->r:Ljava/lang/String;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->Y:F

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->V:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->V:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->V:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->V:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->V:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/widget/b/e;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/widget/b/e;-><init>(Lcom/xiaomi/misettings/usagestats/widget/b/g;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->V:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/widget/b/f;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/widget/b/f;-><init>(Lcom/xiaomi/misettings/usagestats/widget/b/g;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->V:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0xfe
        0x0
    .end array-data
.end method

.method private i()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->W:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e()F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->u:F

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->k()V

    return-void
.end method

.method private j()V
    .locals 5

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->M:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->k()V

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->P:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->C:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float v1, v0, v1

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->B:F

    div-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v4, v1

    mul-float/2addr v4, v3

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->B:F

    div-float/2addr v3, v2

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    add-int/lit8 v1, v1, 0x1

    :cond_1
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v2, v0, -0x1

    const/4 v3, 0x1

    if-lt v1, v2, :cond_2

    add-int/lit8 v1, v0, -0x1

    :cond_2
    if-gtz v1, :cond_3

    const/4 v1, 0x0

    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->M:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_4

    iput-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->W:Z

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->j(I)V

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->k(I)V

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Landroid/graphics/RectF;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->m()V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a:Ljava/lang/String;

    const-string v2, "doClickAction: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    :goto_0
    return-void
.end method

.method private k()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->O:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :cond_0
    return-void
.end method

.method private l()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->s:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->q:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->t:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    invoke-virtual {p0, v1, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->u:F

    const v2, 0x7f0704fb

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v3

    sub-float/2addr v1, v3

    cmpg-float v1, v1, v0

    if-gez v1, :cond_0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v2

    mul-float/2addr v2, v1

    add-float/2addr v0, v2

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->u:F

    :cond_0
    return-void
.end method

.method private m()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->U:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->U:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->U:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->U:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->U:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/widget/b/c;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/widget/b/c;-><init>(Lcom/xiaomi/misettings/usagestats/widget/b/g;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->U:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/widget/b/d;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/widget/b/d;-><init>(Lcom/xiaomi/misettings/usagestats/widget/b/g;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->U:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method private u(I)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->y:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->k()V

    return-void
.end method


# virtual methods
.method protected a(IF)F
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    int-to-float p1, p1

    return p1

    :cond_0
    if-nez p1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->C:F

    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr p1, v0

    add-float/2addr p1, p2

    :goto_0
    return p1
.end method

.method protected a(Ljava/lang/String;Landroid/graphics/Paint;)F
    .locals 0

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result p1

    return p1
.end method

.method protected varargs a(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a()V
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0704ff

    goto :goto_0

    :cond_0
    const v0, 0x7f0704d5

    :goto_0
    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->C:F

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->b()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->c()J

    move-result-wide v0

    long-to-float v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->N:F

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->d()F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->E:F

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    int-to-float v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->C:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->D:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->J:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->B:F

    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->b(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->d(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->c(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->W:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;IF)V
    .locals 0

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->O:Landroid/view/View;

    return-void
.end method

.method protected a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->O:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    if-eq v0, p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->W:Z

    :cond_0
    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->o:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a()V

    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    const/high16 v3, 0x41c80000    # 25.0f

    if-eq v0, v2, :cond_2

    const/4 v4, 0x2

    if-eq v0, v4, :cond_0

    goto/16 :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->R:F

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v3

    if-gtz v0, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->S:F

    sub-float/2addr p1, v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    cmpl-float p1, p1, v3

    if-lez p1, :cond_5

    :cond_1
    sget-object p1, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a:Ljava/lang/String;

    const-string v0, "onTouchEvent: move cancel"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->T:Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->P:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v3

    if-lez v0, :cond_3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->Q:F

    sub-float/2addr p1, v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    cmpg-float p1, p1, v3

    if-gtz p1, :cond_5

    :cond_3
    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->T:Z

    if-eqz p1, :cond_5

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->j()V

    goto :goto_0

    :cond_4
    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->T:Z

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->Z:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->U:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Landroid/animation/ValueAnimator;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->V:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->a(Landroid/animation/ValueAnimator;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->P:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->R:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->Q:F

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->S:F

    :cond_5
    :goto_0
    return v2
.end method

.method protected abstract b()I
.end method

.method protected abstract b(I)I
.end method

.method protected abstract b(Landroid/graphics/Canvas;)V
.end method

.method protected abstract c()J
.end method

.method protected abstract c(I)Ljava/lang/String;
.end method

.method protected abstract d()F
.end method

.method protected abstract d(I)F
.end method

.method protected abstract e()F
.end method

.method protected e(I)I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    return p1
.end method

.method protected e(Ljava/util/List;)Z
    .locals 0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected f(I)Landroid/graphics/Paint$Align;
    .locals 0

    if-nez p1, :cond_0

    sget-object p1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    goto :goto_0

    :cond_0
    sget-object p1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    :goto_0
    return-object p1
.end method

.method public f()V
    .locals 3

    invoke-static {}, Ljava/text/SimpleDateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->c:Ljava/text/SimpleDateFormat;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->c:Ljava/text/SimpleDateFormat;

    const v1, 0x7f1303e2

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->aa:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->M:Ljava/util/List;

    const v0, 0x7f07048c

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->H:F

    const v0, 0x7f0704d9

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->F:F

    const v0, 0x7f0704f5

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->s:F

    const v0, 0x7f0704f6

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->t:F

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e()F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->u:F

    const v0, 0x7f0704f4

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->v:F

    const v0, 0x7f0704fc

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->z:F

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->A:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->p:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->p:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->G:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->G:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->F:F

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->I:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->I:Landroid/graphics/Paint;

    const v2, 0x7f06037b

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->I:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->H:F

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->y:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->y:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->y:Landroid/graphics/Paint;

    const v2, 0x7f060399

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->w:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    return-void
.end method

.method protected g(I)I
    .locals 0

    const p1, 0x7f06037e

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e(I)I

    move-result p1

    return p1
.end method

.method protected g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->aa:Z

    return v0
.end method

.method protected h(I)F
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    return p1
.end method

.method protected i(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected abstract j(I)V
.end method

.method protected abstract k(I)V
.end method

.method public l(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->e:I

    return-void
.end method

.method public m(I)V
    .locals 4

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    const v0, 0x7f0704d9

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h(I)F

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->G:Landroid/graphics/Paint;

    int-to-float p1, p1

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(F)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr p1, v2

    invoke-static {v1, p1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->K:F

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    int-to-float p1, p1

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(F)F

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->n:Landroid/content/Context;

    const v2, 0x40ae6666    # 5.45f

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    sub-float/2addr p1, v0

    float-to-int p1, p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->g:I

    return-void
.end method

.method public n(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->k:I

    return-void
.end method

.method public o(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->m:I

    return-void
.end method

.method public p(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h:I

    return-void
.end method

.method public q(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->i:I

    return-void
.end method

.method public r(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->l:I

    return-void
.end method

.method public run()V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->Z:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/b/g;->h()V

    :cond_0
    return-void
.end method

.method public s(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->j:I

    return-void
.end method

.method public t(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/widget/b/g;->f:I

    return-void
.end method
