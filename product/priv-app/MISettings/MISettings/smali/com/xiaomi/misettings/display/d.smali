.class public Lcom/xiaomi/misettings/display/d;
.super Ljava/lang/Object;


# static fields
.field private static a:[I

.field public static final b:Z

.field public static final c:Z

.field public static d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    const-string v1, "need_remove_expert_primary"

    invoke-static {v1, v0}, Lb/c/a/b/a/b;->a(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/xiaomi/misettings/display/d;->b:Z

    const-string v1, "need_remove_expert_bright"

    invoke-static {v1, v0}, Lb/c/a/b/a/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/xiaomi/misettings/display/d;->c:Z

    const/4 v0, 0x4

    sput v0, Lcom/xiaomi/misettings/display/d;->d:I

    return-void
.end method

.method public static a(I)I
    .locals 5

    sget-object v0, Lcom/xiaomi/misettings/display/d;->a:[I

    if-eqz v0, :cond_3

    array-length v1, v0

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_2

    aget v4, v0, v3

    if-ne v4, p0, :cond_1

    return p0

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    sget-object p0, Lcom/xiaomi/misettings/display/d;->a:[I

    aget p0, p0, v2

    :cond_3
    :goto_1
    return p0
.end method

.method public static a()V
    .locals 6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    sget v3, Lcom/xiaomi/misettings/display/d;->d:I

    if-ge v2, v3, :cond_2

    if-nez v2, :cond_0

    sget-boolean v3, Lcom/xiaomi/misettings/display/d;->c:Z

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    sget-boolean v3, Lcom/xiaomi/misettings/display/d;->b:Z

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [I

    sput-object v2, Lcom/xiaomi/misettings/display/d;->a:[I

    move v2, v1

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    sget-object v3, Lcom/xiaomi/misettings/display/d;->a:[I

    add-int/lit8 v4, v2, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v3, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v4

    goto :goto_2

    :cond_3
    sget v0, Lcom/xiaomi/misettings/display/a/a;->b:I

    invoke-static {v0}, Lcom/xiaomi/misettings/display/d;->a(I)I

    move-result v0

    invoke-static {v0}, Lcom/xiaomi/misettings/display/a/a;->l(I)V

    invoke-static {}, Lcom/xiaomi/misettings/display/a/a;->a()Lcom/xiaomi/misettings/display/a/a;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/xiaomi/misettings/display/a/a;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_4
    const-string v0, "is null"

    :goto_3
    const-string v1, "GamutValueHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/xiaomi/misettings/display/ExpertRadioPreference;",
            ">;)V"
        }
    .end annotation

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v0, v0, [I

    sput-object v0, Lcom/xiaomi/misettings/display/d;->a:[I

    sget-object v0, Lcom/xiaomi/misettings/display/d;->a:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/display/ExpertRadioPreference;

    invoke-virtual {v2}, Landroidx/preference/Preference;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/xiaomi/misettings/display/d;->a:[I

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, v2, v0

    move v0, v3

    goto :goto_0

    :cond_2
    sget p0, Lcom/xiaomi/misettings/display/a/a;->b:I

    invoke-static {p0}, Lcom/xiaomi/misettings/display/d;->a(I)I

    move-result p0

    invoke-static {p0}, Lcom/xiaomi/misettings/display/a/a;->l(I)V

    return-void
.end method
