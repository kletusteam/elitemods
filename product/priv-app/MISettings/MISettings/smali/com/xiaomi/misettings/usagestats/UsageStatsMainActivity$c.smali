.class Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$c;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public synthetic a()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OtherTask runOnUiThread error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Timer-UsageStatsMainActivity"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public run()V
    .locals 7

    const-string v0, "PREF_KEY_FIRST_USE_TIME"

    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->H(Landroid/content/Context;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->b(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;Z)Z

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->c(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;

    new-instance v3, Lcom/xiaomi/misettings/usagestats/h;

    invoke-direct {v3, p0}, Lcom/xiaomi/misettings/usagestats/h;-><init>(Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity$c;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    invoke-static {v1}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v2

    const-wide/16 v3, -0x1

    invoke-virtual {v2, v0, v3, v4}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    invoke-static {v1}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    sub-long/2addr v3, v5

    invoke-virtual {v2, v0, v3, v4}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;J)V

    :cond_1
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "misettings_has_track_permission_event"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OtherTask run error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Timer-UsageStatsMainActivity"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    return-void
.end method
