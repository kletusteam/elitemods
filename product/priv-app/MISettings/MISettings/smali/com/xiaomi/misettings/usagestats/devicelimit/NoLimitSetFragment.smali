.class public Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;
.super Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$b;,
        Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;
    }
.end annotation


# instance fields
.field private f:Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;-><init>()V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->h:Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/misettings/common/base/a;

    invoke-direct {v0, p0}, Lcom/misettings/common/base/a;-><init>(Landroid/content/Context;)V

    const p0, 0x7f13043b

    invoke-virtual {v0, p0}, Lcom/misettings/common/base/a;->b(I)Lcom/misettings/common/base/a;

    const-class p0, Lcom/xiaomi/misettings/usagestats/home/ui/NewSubSettings;

    invoke-virtual {v0, p0}, Lcom/misettings/common/base/a;->a(Ljava/lang/Class;)Lcom/misettings/common/base/a;

    const-string p0, "com.xiaomi.misettings.usagestats.devicelimit.NoLimitSetFragment"

    invoke-virtual {v0, p0}, Lcom/misettings/common/base/a;->a(Ljava/lang/String;)Lcom/misettings/common/base/a;

    invoke-virtual {v0}, Lcom/misettings/common/base/a;->b()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->q()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->r()V

    return-void
.end method

.method private o()V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/devicelimit/l;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/l;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private p()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    instance-of v0, v0, Lmiuix/appcompat/app/AppCompatActivity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroidx/appcompat/app/ActionBar;->o()V

    const v1, 0x7f13043b

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->d(I)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->b(I)V

    :cond_1
    return-void
.end method

.method private q()V
    .locals 8

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/AppCompatActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->c(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->g:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sget-object v5, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->b:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_0

    :cond_1
    sget-object v5, Lcom/xiaomi/misettings/usagestats/utils/p;->a:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_0

    :cond_2
    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v5, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v6

    const/4 v7, 0x1

    invoke-direct {v5, v6, v4, v7}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    new-instance v5, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v5, v6, v4, v7}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/b;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/m;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/m;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;)V

    invoke-static {v0}, Lcom/misettings/common/utils/j;->a(Ljava/lang/Runnable;)V

    :cond_5
    :goto_1
    return-void
.end method

.method private r()V
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->l()V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->g:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->f:Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->f:Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment$a;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemViewCacheSize(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getRecycledViewPool()Landroidx/recyclerview/widget/RecyclerView$l;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroidx/recyclerview/widget/RecyclerView$l;->a(II)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->h:Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;->a()V

    return-void
.end method


# virtual methods
.method protected n()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->o()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->p()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/NoLimitSetFragment;->h:Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;->c()V

    return-void
.end method
