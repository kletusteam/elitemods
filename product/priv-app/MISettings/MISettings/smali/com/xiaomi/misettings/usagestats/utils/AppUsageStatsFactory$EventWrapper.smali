.class Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory$EventWrapper;
.super Ljava/lang/Object;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EventWrapper"
.end annotation


# instance fields
.field public lastEvent:I

.field public lastEventPkg:Ljava/lang/String;

.field public lastEventTimeStamp:J


# direct methods
.method public constructor <init>(ILjava/lang/String;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory$EventWrapper;->lastEvent:I

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory$EventWrapper;->lastEventPkg:Ljava/lang/String;

    iput-wide p3, p0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory$EventWrapper;->lastEventTimeStamp:J

    return-void
.end method


# virtual methods
.method public saveCache(Landroid/content/Context;JJ)V
    .locals 10

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "lastEvent"

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory$EventWrapper;->lastEvent:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "lastEventPkg"

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory$EventWrapper;->lastEventPkg:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "lastEventTimeStamp"

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory$EventWrapper;->lastEventTimeStamp:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v4, p1

    move-wide v5, p2

    move-wide v8, p4

    invoke-static/range {v4 .. v9}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;JLjava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "LR-AppUsageStatsFactory"

    const-string p3, "EventWrapper saveCache error"

    invoke-static {p2, p3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method
