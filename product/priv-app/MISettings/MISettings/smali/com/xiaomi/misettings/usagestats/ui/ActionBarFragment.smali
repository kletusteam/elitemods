.class public Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;
.super Lcom/xiaomi/misettings/base/BaseFragment;

# interfaces
.implements Ljava/util/Observer;


# instance fields
.field protected c:Landroid/view/View;

.field private d:J

.field protected e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected f:Lmiuix/recyclerview/widget/RecyclerView;

.field protected g:Landroid/view/View;

.field protected h:Lmiuix/view/f;

.field protected i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field protected j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/a;",
            ">;"
        }
    .end annotation
.end field

.field protected k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;

.field private m:Lmiuix/view/f$a;

.field private n:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/base/BaseFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->i:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->j:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->k:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->l:Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/ui/i;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/ui/i;-><init>(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->m:Lmiuix/view/f$a;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/ui/j;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/ui/j;-><init>(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->n:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;)Lmiuix/view/f$a;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->m:Lmiuix/view/f$a;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;)Landroid/text/TextWatcher;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->n:Landroid/text/TextWatcher;

    return-object p0
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->c()Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;->c(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Lmiuix/view/f$a;)V
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    check-cast p1, Lmiuix/view/f;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->h:Lmiuix/view/f;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->e:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public k()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected l()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->d:J

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onCreate: time="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->d:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "duration_time"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/misettings/common/utils/n;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/search/c;->a()Lcom/xiaomi/misettings/usagestats/search/c;

    move-result-object p1

    invoke-virtual {p1, p0}, Ljava/util/Observable;->addObserver(Ljava/util/Observer;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/search/c;->a()Lcom/xiaomi/misettings/usagestats/search/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/Observable;->deleteObserver(Ljava/util/Observer;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->l:Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;->c()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->onResume()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onResume: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->d:J

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "duration_time"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const p2, 0x7f0b01bd

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->c:Landroid/view/View;

    const p2, 0x7f0b0037

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->f:Lmiuix/recyclerview/widget/RecyclerView;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->f:Lmiuix/recyclerview/widget/RecyclerView;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->f:Lmiuix/recyclerview/widget/RecyclerView;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    if-eqz p2, :cond_0

    new-instance p2, Lcom/xiaomi/misettings/usagestats/ui/g;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-direct {p2, p0, v1}, Lcom/xiaomi/misettings/usagestats/ui/g;-><init>(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->k(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->f:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    :cond_0
    const p2, 0x7f0b014e

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->g:Landroid/view/View;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->g:Landroid/view/View;

    const p2, 0x1020009

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object p2

    const v0, 0x7f1302ad

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->g:Landroid/view/View;

    new-instance p2, Lcom/xiaomi/misettings/usagestats/ui/h;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/usagestats/ui/h;-><init>(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->g:Landroid/view/View;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->l:Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/h;->a()V

    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0

    instance-of p1, p2, Ljava/lang/String;

    if-eqz p1, :cond_2

    check-cast p2, Ljava/lang/String;

    const-string p1, "exit_search_mode"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    instance-of p2, p1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;

    if-nez p2, :cond_1

    return-void

    :cond_1
    check-cast p1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->b()I

    move-result p1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->k()I

    move-result p2

    if-ne p1, p2, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->h:Lmiuix/view/f;

    if-eqz p1, :cond_2

    check-cast p1, Landroid/view/ActionMode;

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    :cond_2
    return-void
.end method
