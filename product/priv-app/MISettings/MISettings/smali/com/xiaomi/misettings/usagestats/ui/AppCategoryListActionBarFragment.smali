.class public Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;
.super Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;


# instance fields
.field private o:Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;

.field private p:Landroid/os/Handler;

.field private q:Z

.field public r:Lcom/xiaomi/misettings/usagestats/f/g;

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;"
        }
    .end annotation
.end field

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/o;",
            ">;"
        }
    .end annotation
.end field

.field public v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation
.end field

.field public w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private x:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->p:Landroid/os/Handler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->q:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->r:Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->s:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->t:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->u:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->v:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->w:Ljava/util/List;

    return-void
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v2

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_2

    :cond_0
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v13

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/h;

    if-eqz v2, :cond_1

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v15

    new-instance v12, Lcom/xiaomi/misettings/usagestats/c/b$a;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ic_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/h;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/h;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v10

    move-object v5, v12

    move-wide v8, v13

    move-object v4, v12

    move-object v12, v15

    invoke-direct/range {v5 .. v12}, Lcom/xiaomi/misettings/usagestats/c/b$a;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;JJLjava/lang/CharSequence;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/high16 v4, 0x41580000    # 13.5f

    invoke-static {v2, v15, v4}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;Ljava/lang/CharSequence;F)I

    move-result v4

    if-ge v3, v4, :cond_1

    move v3, v4

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {v2, v3}, Lcom/xiaomi/misettings/usagestats/c/b$a;->a(I)V

    goto :goto_1

    :cond_3
    :goto_2
    return-object v1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->o()V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->n()V

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;)Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->o:Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;

    return-object p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->p:Landroid/os/Handler;

    return-object p0
.end method

.method private n()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->r:Lcom/xiaomi/misettings/usagestats/f/g;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->s:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/b/a/g;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;Ljava/util/List;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->s:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->s:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->t:Ljava/util/List;

    :cond_0
    return-void
.end method

.method private o()V
    .locals 14

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v1, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->m:Ljava/util/List;

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->v:Ljava/util/List;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->v:Ljava/util/List;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->u:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/b/a/g;->a(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->u:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->u:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/o;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/o;->e()J

    move-result-wide v10

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->w:Ljava/util/List;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/o;

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/o;->e()J

    move-result-wide v3

    invoke-static {v0, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v9

    iget-object v12, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->w:Ljava/util/List;

    new-instance v13, Lcom/xiaomi/misettings/usagestats/c/b$a;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ic_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/o;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/o;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/o;->e()J

    move-result-wide v7

    move-object v2, v13

    move-wide v5, v10

    invoke-direct/range {v2 .. v9}, Lcom/xiaomi/misettings/usagestats/c/b$a;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;JJLjava/lang/CharSequence;)V

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method protected b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const p3, 0x7f0e001c

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public k()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public m()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/ui/m;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/ui/m;-><init>(Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->onCreate(Landroid/os/Bundle;)V

    sget-object p1, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->m:Lcom/xiaomi/misettings/usagestats/f/g;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->r:Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance p1, Lcom/xiaomi/misettings/usagestats/ui/k;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/usagestats/ui/k;-><init>(Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->x:Landroid/content/BroadcastReceiver;

    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "misettings.action.FORCE_NOTIFY_DATA"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->x:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1, p1}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->p:Landroid/os/Handler;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/base/BaseFragment;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->x:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->x:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;)V

    :cond_1
    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->r:Lcom/xiaomi/misettings/usagestats/f/g;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->f:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->o:Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroyView()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->r:Lcom/xiaomi/misettings/usagestats/f/g;

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->onResume()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->o:Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;->a()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->q:Z

    :goto_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    new-instance p1, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->o:Lcom/xiaomi/misettings/usagestats/adapter/CategoryRVAdapter;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/ui/AppCategoryListActionBarFragment;->m()V

    return-void
.end method
