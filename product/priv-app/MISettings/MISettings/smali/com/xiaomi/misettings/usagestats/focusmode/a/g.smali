.class public Lcom/xiaomi/misettings/usagestats/focusmode/a/g;
.super Landroidx/recyclerview/widget/RecyclerView$a;

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/focusmode/a/g$c;,
        Lcom/xiaomi/misettings/usagestats/focusmode/a/g$d;,
        Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;,
        Lcom/xiaomi/misettings/usagestats/focusmode/a/g$a;,
        Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;",
        ">;",
        "Ljava/util/Observer;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/focusmode/data/g;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:Landroidx/recyclerview/widget/RecyclerView;

.field private d:Landroid/net/Uri;

.field private e:Lcom/xiaomi/misettings/widget/DelTextView;

.field private f:Landroid/view/View;

.field private g:I

.field private h:I

.field private i:Z

.field private j:Landroid/content/ContentResolver;

.field private k:Landroid/widget/Toast;

.field private l:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$a;

.field private m:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;

.field private n:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$d;

.field private o:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/focusmode/data/g;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    const-string v0, "content://com.xiaomi.misettings.usagestats.focusmode.data.TimerContentProvider/focus_mode_timers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->d:Landroid/net/Uri;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->i:Z

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    move v2, v1

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    if-ne p2, v0, :cond_2

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->j:Landroid/content/ContentResolver;

    return-void
.end method

.method private a(I)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->j:Landroid/content/ContentResolver;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->d()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->j:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->d:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    const-string p1, "duration=?"

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b()V

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/widget/DelTextView;

    invoke-direct {v1, v0}, Lcom/xiaomi/misettings/widget/DelTextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    const v2, 0x7f130180

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    const/4 v2, 0x0

    const-string v3, "mipro-medium"

    invoke-static {v3, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f07010f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070110

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    invoke-virtual {v4, v1, v3, v1, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070111

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->f:Landroid/view/View;

    if-nez v1, :cond_1

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->f:Landroid/view/View;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/a/e;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/e;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_1
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/app/Activity;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const v2, 0x800003

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/a/f;

    invoke-direct {v1, p0, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/a/f;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getVisibility()I

    move-result p2

    if-nez p2, :cond_3

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/a/a;

    invoke-direct {v0, p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/a;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    :cond_4
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->c()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e(I)V

    return-void
.end method

.method private a(ILcom/xiaomi/misettings/usagestats/focusmode/a/g$e;)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b(ILcom/xiaomi/misettings/usagestats/focusmode/a/g$e;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;ILcom/xiaomi/misettings/usagestats/focusmode/a/g$e;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a(ILcom/xiaomi/misettings/usagestats/focusmode/a/g$e;)Z

    move-result p0

    return p0
.end method

.method private b()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->n:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$d;

    invoke-interface {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$d;->a()V

    return-void
.end method

.method private b(I)V
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    if-ne p1, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->c(I)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->d(I)V

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e()V

    return-void
.end method

.method private b(ILcom/xiaomi/misettings/usagestats/focusmode/a/g$e;)V
    .locals 4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b(Z)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    if-eq v0, v2, :cond_1

    if-eq v0, p1, :cond_1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a(Z)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :cond_1
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b(Z)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->b()Lcom/xiaomi/misettings/usagestats/focusmode/c/t;

    move-result-object v0

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/data/j;

    invoke-direct {v2, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/j;-><init>(Z)V

    invoke-virtual {v0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/c/t;->a(Lcom/xiaomi/misettings/usagestats/focusmode/data/j;)V

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->i:Z

    iget-object p2, p2, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->f:Landroid/view/View;

    invoke-direct {p0, p2, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a(Landroid/view/View;I)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    add-int/2addr v2, p1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getWidth()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    sub-int/2addr v2, p1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout$LayoutParams;

    add-int/2addr v2, v1

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMarginStart(I)V

    iget v1, p1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    iget v2, p1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget v3, p1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private b(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;I)V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->f:Landroid/view/View;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->c()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f080158

    goto :goto_0

    :cond_0
    const v2, 0x7f080159

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->m:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;->a(Ljava/lang/String;)V

    :cond_1
    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b:Landroid/content/Context;

    const v3, 0x7f06007d

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b:Landroid/content/Context;

    const v3, 0x7f06038d

    :goto_1
    invoke-virtual {v2, v3}, Landroid/content/Context;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b:Landroid/content/Context;

    const v2, 0x7f06007c

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b:Landroid/content/Context;

    const v2, 0x7f06038c

    :goto_2
    invoke-virtual {v0, v2}, Landroid/content/Context;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110037

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v5, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b(I)V

    return-void
.end method

.method private c()V
    .locals 3

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->i:Z

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->c:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b(Z)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a(Z)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e()V

    return-void
.end method

.method private c(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a(I)V

    const/4 p1, -0x1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->i:Z

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->k:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->k:Landroid/widget/Toast;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->k:Landroid/widget/Toast;

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private d()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->d:Landroid/net/Uri;

    const-string v1, "id"

    const-string v4, "duration"

    filled-new-array {v1, v4}, [Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v0
.end method

.method private d(I)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->c:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->h:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a(Z)V

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->i:Z

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->m:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;->a(Ljava/lang/String;)V

    return-void
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->e:Lcom/xiaomi/misettings/widget/DelTextView;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private e(I)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b()I

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->i:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->f(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->l:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$a;

    invoke-interface {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$a;->d()V

    return-void
.end method

.method private f(I)V
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    if-eq v0, p1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a(Z)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a(Z)V

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->c:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->m:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->c()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f13017f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/16 v3, 0x14

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->f()V

    :goto_0
    return-void
.end method

.method public synthetic a(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b(Landroid/view/View;)V

    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->c:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$a;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->l:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$a;

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->m:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$c;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->o:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$c;

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$d;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->n:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$d;

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;I)V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    iget-object v2, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->f:Landroid/view/View;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v1

    new-array v2, v0, [Lmiuix/animation/m$b;

    sget-object v4, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    aput-object v4, v2, v3

    const v4, 0x3f666666    # 0.9f

    invoke-interface {v1, v4, v2}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    iget-object v2, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->f:Landroid/view/View;

    new-array v3, v3, [Lmiuix/animation/a/a;

    invoke-interface {v1, v2, v3}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->f:Landroid/view/View;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/a/c;

    invoke-direct {v2, p0, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/a/c;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->f:Landroid/view/View;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/a/d;

    invoke-direct {v2, p0, p2, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/d;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;ILcom/xiaomi/misettings/usagestats/focusmode/a/g$e;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->b(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;I)V

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    iget-object v2, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0704df

    const v4, 0x7f070184

    if-nez p2, :cond_0

    move v5, v3

    goto :goto_0

    :cond_0
    move v5, v4

    :goto_0
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    iget-object v2, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->getItemCount()I

    move-result v5

    sub-int/2addr v5, v0

    if-ne p2, v5, :cond_1

    goto :goto_1

    :cond_1
    move v3, v4

    :goto_1
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p2

    invoke-virtual {v1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;->f:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->a(Z)V

    :cond_0
    iput v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;-><init>(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    sub-int/2addr v3, v1

    invoke-interface {v2, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->m:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$b;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "duration"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->j:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->d:Landroid/net/Uri;

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->g:I

    const/4 v0, 0x2

    if-lt p1, v0, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->o:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$c;

    sub-int/2addr p1, v0

    invoke-interface {v1, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$c;->b(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->o:Lcom/xiaomi/misettings/usagestats/focusmode/a/g$c;

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$c;->b(I)V

    :goto_0
    return-void
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b()I

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/data/g;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    return v1

    :cond_1
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemViewType(I)I

    move-result p1

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->a(Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const v0, 0x7f0e0074

    const/4 v1, 0x0

    if-nez p2, :cond_0

    new-instance p2, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;Landroid/view/View;)V

    return-object p2

    :cond_0
    const/4 v2, 0x1

    if-ne p2, v2, :cond_1

    new-instance p2, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0e0026

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;Landroid/view/View;)V

    return-object p2

    :cond_1
    new-instance p2, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g$e;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/a/g;Landroid/view/View;)V

    return-object p2
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0

    instance-of p1, p2, Lcom/xiaomi/misettings/usagestats/focusmode/data/j;

    if-eqz p1, :cond_0

    check-cast p2, Lcom/xiaomi/misettings/usagestats/focusmode/data/j;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/focusmode/data/j;->a()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/a/g;->c()V

    :cond_0
    return-void
.end method
