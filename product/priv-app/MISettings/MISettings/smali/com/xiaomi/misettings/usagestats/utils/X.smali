.class final Lcom/xiaomi/misettings/usagestats/utils/X;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/utils/Y;->e(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/X;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/X;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/a/d;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/d;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/X;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/a/d;->c(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    new-instance v1, Landroidx/work/d$a;

    invoke-direct {v1}, Landroidx/work/d$a;-><init>()V

    sget-object v2, Landroidx/work/o;->b:Landroidx/work/o;

    invoke-virtual {v1, v2}, Landroidx/work/d$a;->a(Landroidx/work/o;)Landroidx/work/d$a;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroidx/work/d$a;->a(Z)Landroidx/work/d$a;

    invoke-virtual {v1}, Landroidx/work/d$a;->a()Landroidx/work/d;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4, v1}, Lcom/xiaomi/misettings/usagestats/utils/Y;->a(JLandroidx/work/d;)Landroidx/work/p;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/utils/X;->a:Landroid/content/Context;

    invoke-static {v4}, Landroidx/work/y;->a(Landroid/content/Context;)Landroidx/work/y;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroidx/work/y;->a(Landroidx/work/p;)Landroidx/work/w;

    move-result-object v3

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5, v1}, Lcom/xiaomi/misettings/usagestats/utils/Y;->a(JLandroidx/work/d;)Landroidx/work/p;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroidx/work/w;->a(Landroidx/work/p;)Landroidx/work/w;

    move-result-object v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroidx/work/w;->a()Landroidx/work/r;

    :cond_2
    :goto_1
    return-void
.end method
