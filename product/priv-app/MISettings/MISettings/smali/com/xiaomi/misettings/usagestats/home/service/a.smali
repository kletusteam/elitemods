.class Lcom/xiaomi/misettings/usagestats/home/service/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/home/service/FetchAppNameService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/home/service/FetchAppNameService;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/service/FetchAppNameService;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/service/a;->a:Lcom/xiaomi/misettings/usagestats/home/service/FetchAppNameService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/service/a;->a:Lcom/xiaomi/misettings/usagestats/home/service/FetchAppNameService;

    invoke-virtual {v0}, Landroid/app/job/JobService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/service/FetchAppNameService;->a(Lcom/xiaomi/misettings/usagestats/home/service/FetchAppNameService;Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_2

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/xiaomi/misettings/usagestats/home/database/appname/a;

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/service/a;->a:Lcom/xiaomi/misettings/usagestats/home/service/FetchAppNameService;

    invoke-virtual {v4}, Landroid/app/job/JobService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/home/database/appname/f;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_1

    :cond_1
    new-instance v4, Lcom/xiaomi/misettings/usagestats/home/database/appname/a;

    invoke-direct {v4}, Lcom/xiaomi/misettings/usagestats/home/database/appname/a;-><init>()V

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/service/a;->a:Lcom/xiaomi/misettings/usagestats/home/service/FetchAppNameService;

    invoke-virtual {v5}, Landroid/app/job/JobService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/xiaomi/misettings/usagestats/home/database/appname/a;->c:Ljava/lang/String;

    iput-object v3, v4, Lcom/xiaomi/misettings/usagestats/home/database/appname/a;->b:Ljava/lang/String;

    aput-object v4, v1, v2

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/service/a;->a:Lcom/xiaomi/misettings/usagestats/home/service/FetchAppNameService;

    invoke-virtual {v0}, Landroid/app/job/JobService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/home/database/appname/f;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->a([Lcom/xiaomi/misettings/usagestats/home/database/appname/a;)[Ljava/lang/Long;

    return-void

    :cond_3
    :goto_2
    const-string v0, "FetchAppNameService"

    const-string v1, "run: all apps has saved or no app to save"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
