.class public Lcom/xiaomi/misettings/usagestats/home/category/a/f;
.super Lcom/xiaomi/misettings/usagestats/d/b/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/usagestats/d/b/b<",
        "Lb/c/a/a/a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/LinearLayout;

.field private f:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/b/b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const p1, 0x7f0b01a9

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->b:Landroid/widget/ImageView;

    const p1, 0x7f0b01aa

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->c:Landroid/widget/TextView;

    const p1, 0x7f0b01ae

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->d:Landroid/widget/TextView;

    const p1, 0x7f0b01bb

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->e:Landroid/widget/LinearLayout;

    const p1, 0x7f0b0242

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/b/b;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->f:Landroid/widget/ImageView;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/category/a/f;)Landroid/widget/LinearLayout;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->e:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/home/category/a/f;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->c:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/home/category/a/f;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V
    .locals 2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/home/category/c/c;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->b:Landroid/widget/ImageView;

    iget-object p3, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->c:Landroid/widget/TextView;

    iget-object p3, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->g:Ljava/lang/String;

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->d:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/d/b/b;->a:Landroid/content/Context;

    iget-wide v0, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->e:J

    invoke-static {p3, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->f:Landroid/widget/ImageView;

    iget-boolean p3, p2, Lcom/xiaomi/misettings/usagestats/home/category/c/c;->d:Z

    if-eqz p3, :cond_0

    const/4 p3, 0x0

    goto :goto_0

    :cond_0
    const/16 p3, 0x8

    :goto_0
    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->e:Landroid/widget/LinearLayout;

    new-instance p3, Lcom/xiaomi/misettings/usagestats/home/category/a/d;

    invoke-direct {p3, p0, p2}, Lcom/xiaomi/misettings/usagestats/home/category/a/d;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/a/f;Lcom/xiaomi/misettings/usagestats/home/category/c/c;)V

    invoke-virtual {p1, p3}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    new-instance p3, Lcom/xiaomi/misettings/usagestats/home/category/a/e;

    invoke-direct {p3, p0, p2}, Lcom/xiaomi/misettings/usagestats/home/category/a/e;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/a/f;Lcom/xiaomi/misettings/usagestats/home/category/c/c;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V
    .locals 0

    check-cast p2, Lb/c/a/a/a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/home/category/a/f;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V

    return-void
.end method
