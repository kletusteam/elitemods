.class public Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;
.super Lcom/misettings/common/base/BaseActivity;


# static fields
.field private static final a:Landroid/content/Intent;


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Lcom/airbnb/lottie/LottieAnimationView;

.field private h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

.field private i:Landroid/content/BroadcastReceiver;

.field private j:Landroid/content/BroadcastReceiver;

.field private k:Landroid/os/Handler;

.field private l:Landroid/os/HandlerThread;

.field private m:Landroid/os/Handler;

.field private n:J

.field private o:J

.field private p:I

.field private q:I

.field private r:Z

.field private s:Z

.field t:I

.field u:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.phone.EmergencyDialer.DIAL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.android.phone"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x14800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->a:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_1
    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->q:I

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->r:Z

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/c/u;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->s:Z

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->t:I

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->u:I

    return-void
.end method

.method static synthetic a()Landroid/content/Intent;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->a:Landroid/content/Intent;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {p0}, Lcom/misettings/common/utils/f;->c(Landroid/content/Context;)Lb/c/a/a/b;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-eq p1, v2, :cond_2

    if-eq p1, v3, :cond_1

    const/4 v2, 0x3

    if-eq p1, v2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const p1, 0x7f08008a

    goto :goto_0

    :cond_1
    const p1, 0x7f080087

    goto :goto_0

    :cond_2
    const p1, 0x7f080088

    :goto_0
    if-eqz p1, :cond_3

    :try_start_0
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, p1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    iget v1, v1, Lb/c/a/a/b;->d:I

    invoke-static {p0}, Lcom/misettings/common/utils/f;->d(Landroid/content/Context;)I

    move-result v3

    invoke-static {v2, v1, v3}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_3
    :goto_1
    return-void
.end method

.method private a(J)V
    .locals 6

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    div-long v2, p1, v0

    rem-long/2addr p1, v0

    const-wide/16 v0, 0x3e8

    div-long/2addr p1, v0

    const-wide/16 v0, 0xa

    cmp-long v4, v2, v0

    const-string v5, "0"

    if-gez v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    :goto_0
    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    :goto_1
    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->b:Landroid/widget/TextView;

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->c:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->e()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->c(J)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;)J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->n:J

    return-wide v0
.end method

.method public static b()Landroid/content/Intent;
    .locals 5

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string v1, "vela"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.mlab.cam"

    goto :goto_0

    :cond_0
    const-string v0, "com.android.camera"

    :goto_0
    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "com.mtlab.camera.CameraActivity"

    goto :goto_1

    :cond_1
    const-string v1, "com.android.camera.Camera"

    :goto_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const v3, 0x10808000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v3, 0x1

    const-string v4, "ShowCameraWhenLocked"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v4, "StartActivityWhenLocked"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v3, "android.media.action.STILL_IMAGE_CAMERA"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    return-object v2
.end method

.method private b(J)V
    .locals 13

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->n:J

    const-wide/16 v2, 0x3

    div-long v4, v0, v2

    cmp-long v4, p1, v4

    const-wide/16 v5, 0x2

    const/4 v7, 0x2

    const/4 v8, 0x3

    if-gtz v4, :cond_0

    iput v8, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->q:I

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->d:Landroid/widget/TextView;

    const v1, 0x7f1303f2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {v0, v8}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->setCurrentLevel(I)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->q:I

    goto :goto_0

    :cond_0
    mul-long/2addr v0, v5

    div-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-gtz v0, :cond_1

    iput v7, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->q:I

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->d:Landroid/widget/TextView;

    const v1, 0x7f1303f1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {v0, v7}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->setCurrentLevel(I)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->q:I

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->q:I

    if-eq v1, v4, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->d:Landroid/widget/TextView;

    const v4, 0x7f1303f0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {v1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->setCurrentLevel(I)V

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->q:I

    :cond_2
    :goto_0
    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->n:J

    div-long v9, v0, v2

    const-wide/16 v11, 0x1388

    add-long/2addr v9, v11

    cmp-long v4, p1, v9

    if-gtz v4, :cond_3

    iput v8, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->u:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->u:I

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->t:I

    if-eq p1, p2, :cond_4

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {p1, v8}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(I)V

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->u:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->t:I

    goto :goto_1

    :cond_3
    mul-long/2addr v0, v5

    div-long/2addr v0, v2

    add-long/2addr v0, v11

    cmp-long p1, p1, v0

    if-gtz p1, :cond_4

    iput v7, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->u:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->u:I

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->t:I

    if-eq p1, p2, :cond_4

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {p1, v7}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a(I)V

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->u:I

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->t:I

    :cond_4
    :goto_1
    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->b(J)V

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;)J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->o:J

    return-wide v0
.end method

.method private c(J)V
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->e()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->k:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/h;

    invoke-direct {v1, p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/h;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;J)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->a(J)V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->m:Landroid/os/Handler;

    return-object p0
.end method

.method private d()V
    .locals 2

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->F(Landroid/content/Context;)V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->f:Landroid/view/View;

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private e()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->k(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->a(Landroid/content/Context;Z)V

    return-void
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->g()V

    return-void
.end method

.method private f()V
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->g:Lcom/airbnb/lottie/LottieAnimationView;

    const-string v1, "hourglass.json"

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->g:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->d()V

    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Test-FocusModeActivity"

    const-string v2, "jumpToCamera: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method private h()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/s;->a(Landroid/content/Context;Z)V

    return-void
.end method

.method private i()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->j:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/e;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/e;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->j:Landroid/content/BroadcastReceiver;

    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "misettings.action.FORCE_STOP_FOCUS_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method private j()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h()V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/s;->a(Landroid/content/Context;Z)V

    return-void
.end method

.method private k()V
    .locals 2

    const v0, 0x7f0b017c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/misettings/common/utils/SettingsFeatures;->isWifiOnly(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/telephony/TelephonyManager;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/view/View;)V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/i;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/i;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->f:Landroid/view/View;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->f:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/j;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/j;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private l()V
    .locals 0

    return-void
.end method

.method private m()V
    .locals 5

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x80001

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x1202

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->a(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->k:Landroid/os/Handler;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/focusmode/k;

    invoke-direct {v2, p0, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/k;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;Landroid/view/Window;)V

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private n()V
    .locals 4

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->o()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->m:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/focusmode/g;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/g;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private o()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->m:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public c()V
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/f;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/f;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->i:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "misettings.action.FOCUS_MODE_FINISH"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public isSupportMemoryOptimized()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    const-string v0, "FocusModeActivity"

    const-string v1, "onBackPressed: "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0e0146

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->j(Landroid/content/Context;)V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Focus count down"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->l:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->l:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->l:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->m:Landroid/os/Handler;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->k:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->m()V

    const v0, 0x7f0b0208

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->f:Landroid/view/View;

    const v0, 0x7f0b0194

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    const v0, 0x7f0b020b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->e:Landroid/view/View;

    const v0, 0x7f0b0209

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->b:Landroid/widget/TextView;

    const v0, 0x7f0b020a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->c:Landroid/widget/TextView;

    const v0, 0x7f0b0197

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->d:Landroid/widget/TextView;

    const v0, 0x7f0b01a1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->g:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->d:Landroid/widget/TextView;

    const v0, 0x7f1303f0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {p1, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->setCurrentLevel(I)V

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->p(Landroid/content/Context;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->n:J

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->k()V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/W;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->w(Landroid/content/Context;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->o:J

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->d()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->l()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->f()V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->B(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->c()V

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->i()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "keyCanWrite"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "Test-FocusModeActivity"

    const-string v0, "onCreate: start write data"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->G(Landroid/content/Context;)V

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result p1

    if-eqz p1, :cond_4

    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/xiaomi/misettings/usagestats/ExitMultiWindowActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_4
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object p1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->quit()Z

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/ga;->c()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/home/category/database/h;->a()V

    return-void
.end method

.method protected onDestroy()V
    .locals 8

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->a()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    :cond_0
    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->n:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->o:J

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x4e20

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->j()V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->i:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->j:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, La/m/a/b;->a(Landroid/content/BroadcastReceiver;)V

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->k:Landroid/os/Handler;

    if-eqz v0, :cond_4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->l:Landroid/os/HandlerThread;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    :cond_5
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->m:Landroid/os/Handler;

    if-eqz v0, :cond_6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_6
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->s:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->g:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->a()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->g:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->e()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->g:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->g:Lcom/airbnb/lottie/LottieAnimationView;

    :cond_7
    const-string v0, "Test-FocusModeActivity"

    const-string v1, "onDestroy: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const-string v0, "Test-FocusModeActivity"

    const-string v1, "onKeyDown: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x7a

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    const-string p1, "Test-FocusModeActivity"

    const-string v0, "onNewIntent: "

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h()V

    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "level"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->setCurrentLevel(I)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onResume()V

    const-string v0, "Test-FocusModeActivity"

    const-string v1, "onResume: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h()V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->r:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/s;->a(Landroid/content/Context;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->r:Z

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->p:I

    const-string v1, "level"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 6

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStart()V

    const-string v0, "Test-FocusModeActivity"

    const-string v1, "onStart: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->n:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->o:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->b(J)V

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->a(J)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->n()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->c()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->e()V

    :goto_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lcom/misettings/common/base/BaseActivity;->onStop()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->o()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/FocusModeActivityBase;->h:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NewFocusModeBackgroundView;->b()V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/s;->b(Landroid/content/Context;)V

    return-void
.end method
