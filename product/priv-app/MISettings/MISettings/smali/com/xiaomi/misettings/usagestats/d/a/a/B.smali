.class public Lcom/xiaomi/misettings/usagestats/d/a/a/B;
.super Lcom/xiaomi/misettings/usagestats/d/a/a/r;


# static fields
.field public static m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private n:Lcom/xiaomi/misettings/usagestats/d/d/e$a;

.field private o:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

.field private p:Lcom/xiaomi/misettings/usagestats/d/d/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;-><init>(Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/d/a/a/B;Ljava/util/List;Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->a(Ljava/util/List;Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/util/List;Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->n:Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->c:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget v1, Lcom/xiaomi/misettings/usagestats/utils/L;->d:I

    new-array v1, v1, [J

    iget-wide v2, p3, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    :goto_0
    iget-wide v4, p3, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->c:J

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    iget-wide v4, p3, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;->b:J

    sub-long v4, v2, v4

    sget-wide v6, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    div-long/2addr v4, v6

    long-to-int v4, v4

    aput-wide v2, v1, v4

    add-long/2addr v2, v6

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v3, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-direct {v3}, Lcom/xiaomi/misettings/usagestats/f/e;-><init>()V

    invoke-virtual {v3, p2}, Lcom/xiaomi/misettings/usagestats/f/e;->b(Ljava/lang/String;)V

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/xiaomi/misettings/usagestats/f/e;->a(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/f/e;->a(J)V

    goto :goto_2

    :cond_1
    new-instance v2, Lcom/xiaomi/misettings/usagestats/f/j;

    const/4 v4, 0x0

    aget-wide v5, v1, p3

    invoke-direct {v2, v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-virtual {v3, v2}, Lcom/xiaomi/misettings/usagestats/f/e;->a(Lcom/xiaomi/misettings/usagestats/f/j;)V

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/f/e;->a(J)V

    :goto_2
    add-int/lit8 p3, p3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/d/d/e$a;I)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-boolean p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/a;->b:Z

    invoke-direct {p0, v0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->a(Ljava/util/List;ZI)V

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p1, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->f:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-boolean p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/a;->b:Z

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->a(Ljava/util/List;Ljava/util/List;ZI)V

    :goto_0
    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;ZI)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;ZI)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->c()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    const/4 v0, 0x0

    const-wide v1, 0x7fffffffffffffffL

    move-wide v2, v1

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_8

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-gtz v7, :cond_0

    goto/16 :goto_4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    const/4 v7, 0x5

    if-lt v0, v7, :cond_1

    goto/16 :goto_5

    :cond_1
    if-nez v0, :cond_2

    iput-wide v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->l:J

    move-wide v2, v5

    :cond_2
    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->c:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    const/4 v6, 0x0

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v7, 0x7f0e006a

    invoke-static {v5, v7, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    :cond_3
    new-instance v7, Lcom/xiaomi/misettings/usagestats/d/a/a/z;

    invoke-direct {v7, p0, v4, p2, p4}, Lcom/xiaomi/misettings/usagestats/d/a/a/z;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/B;Lcom/xiaomi/misettings/usagestats/f/e;Ljava/util/List;I)V

    invoke-virtual {v5, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v;

    invoke-direct {v7, v5}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;-><init>(Landroid/view/View;)V

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->p:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-boolean v5, v5, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz v5, :cond_4

    iget-object v5, v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->b:Lcom/xiaomi/misettings/widget/CircleImageView;

    invoke-virtual {v5}, Lcom/xiaomi/misettings/widget/CircleImageView;->setDefaultRadius()V

    :cond_4
    new-instance v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;

    invoke-direct {v5}, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;-><init>()V

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/e;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    goto :goto_1

    :cond_5
    iput-object v6, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->a:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    :cond_6
    :goto_1
    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->a:Landroid/graphics/drawable/Drawable;

    :goto_2
    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->c:Ljava/lang/String;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v8

    invoke-static {v6, v8, v9}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->b:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v8

    long-to-float v6, v8

    const/high16 v8, 0x3f800000    # 1.0f

    mul-float/2addr v6, v8

    long-to-float v8, v2

    div-float/2addr v6, v8

    iput v6, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->d:F

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/e;->b()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->e:Ljava/lang/String;

    invoke-virtual {v7, v5}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->a(Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;)V

    if-eqz p3, :cond_7

    iget-object v4, v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->c()V

    goto :goto_3

    :cond_7
    iget-object v4, v7, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->b()V

    :goto_3
    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->a()Landroid/view/View;

    move-result-object v5

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, -0x2

    invoke-direct {v6, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_8
    :goto_5
    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d(I)V

    return-void
.end method

.method private a(Ljava/util/List;ZI)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/o;",
            ">;ZI)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->c()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    const/4 v0, 0x0

    const-wide v1, 0x7fffffffffffffffL

    move-wide v2, v1

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/o;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/o;->e()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-gtz v7, :cond_0

    goto/16 :goto_2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    const/4 v7, 0x5

    if-lt v0, v7, :cond_1

    goto/16 :goto_3

    :cond_1
    if-nez v0, :cond_2

    iput-wide v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->l:J

    move-wide v2, v5

    :cond_2
    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->c:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    const v6, 0x7f0e006a

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    :cond_3
    new-instance v6, Lcom/xiaomi/misettings/usagestats/d/a/a/A;

    invoke-direct {v6, p0, v4, p3}, Lcom/xiaomi/misettings/usagestats/d/a/a/A;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/B;Lcom/xiaomi/misettings/usagestats/f/o;I)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v6, Lcom/xiaomi/misettings/usagestats/d/a/a/v;

    invoke-direct {v6, v5}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;-><init>(Landroid/view/View;)V

    new-instance v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;

    invoke-direct {v5}, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;-><init>()V

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ic_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/o;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    iput-object v7, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/o;->c()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->c:Ljava/lang/String;

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/o;->e()J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->b:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/f/o;->e()J

    move-result-wide v7

    long-to-float v4, v7

    const/high16 v7, 0x3f800000    # 1.0f

    mul-float/2addr v4, v7

    long-to-float v7, v2

    div-float/2addr v4, v7

    iput v4, v5, Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;->d:F

    invoke-virtual {v6, v5}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->a(Lcom/xiaomi/misettings/usagestats/d/a/a/v$a;)V

    if-eqz p2, :cond_4

    iget-object v4, v6, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->c()V

    goto :goto_1

    :cond_4
    iget-object v4, v6, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->e:Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/home/widget/TimeUsageRatioView;->b()V

    :goto_1
    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Lcom/xiaomi/misettings/usagestats/d/a/a/v;->a()Landroid/view/View;

    move-result-object v5

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, -0x2

    invoke-direct {v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_5
    :goto_3
    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->d(I)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/d/a/a/B;)Z
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->d()Z

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/d/a/a/B;)Lcom/xiaomi/misettings/usagestats/d/d/i;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->p:Lcom/xiaomi/misettings/usagestats/d/d/i;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/d/a/a/B;)Lcom/xiaomi/misettings/usagestats/d/d/e$a;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->n:Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    return-object p0
.end method

.method private d()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->p:Lcom/xiaomi/misettings/usagestats/d/d/i;

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private e()V
    .locals 6

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f13039f

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->o:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-static {v5}, Lcom/xiaomi/misettings/usagestats/d/f/a;->a(Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->g:Landroid/widget/TextView;

    const v1, 0x7f130396

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f1303a2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->o:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-static {v5}, Lcom/xiaomi/misettings/usagestats/d/f/a;->a(Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->g:Landroid/widget/TextView;

    const v1, 0x7f1303a4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->g:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V
    .locals 2

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->p:Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-object v0, p1

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {v0, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Lcom/xiaomi/misettings/usagestats/d/a/a$a;)V

    move-object v0, p2

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/e;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->n:Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->n:Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/a;->a:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, v0, p4}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->a(Lcom/xiaomi/misettings/usagestats/d/d/e$a;I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->n:Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/a;->a:Z

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->n:Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->c:Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->o:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->e()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->j:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/a/a/d;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/d/a/a/d;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/B;Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->i:Landroid/view/View;

    new-instance p3, Lcom/xiaomi/misettings/usagestats/d/a/a/e;

    invoke-direct {p3, p0, p2, p4}, Lcom/xiaomi/misettings/usagestats/d/a/a/e;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/B;Lcom/xiaomi/misettings/usagestats/d/d/i;I)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;ILandroid/view/View;)V
    .locals 2

    iget-object p4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->n:Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    const/4 v0, 0x1

    iput-boolean v0, p4, Lcom/xiaomi/misettings/usagestats/d/d/a;->b:Z

    move-object p4, p1

    check-cast p4, Lcom/xiaomi/misettings/usagestats/d/a/a;

    invoke-virtual {p4}, Lcom/xiaomi/misettings/usagestats/d/a/a;->c()V

    iget-object p4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->n:Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    iput-boolean v0, p4, Lcom/xiaomi/misettings/usagestats/d/d/a;->a:Z

    iget-boolean p4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    const/4 v1, 0x0

    if-nez p4, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    iget-object p4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->p:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    invoke-virtual {p4, v0}, Lcom/xiaomi/misettings/usagestats/d/d/i;->a(Z)V

    iget-boolean p2, p2, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object p2

    iget-boolean p4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    const-string v0, "default_category"

    invoke-virtual {p2, v0, p4}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Z)V

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->e()V

    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->a(Z)V

    return-void
.end method

.method public synthetic a(Lcom/xiaomi/misettings/usagestats/d/d/i;ILandroid/view/View;)V
    .locals 6

    iget-boolean v0, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/d/d/i;->a()Lcom/miui/greenguard/entity/DashBordBean;

    move-result-object p1

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p2

    invoke-virtual {p2}, Lcom/miui/greenguard/entity/FamilyBean;->getUserId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object p2

    invoke-virtual {p2}, Lcom/miui/greenguard/entity/FamilyBean;->getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    move-result-object p2

    invoke-virtual {p2}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getDateType()I

    move-result v3

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/DashBordBean;->getDate()I

    move-result v4

    iget-boolean v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    invoke-static/range {v0 .. v5}, Lcom/miui/greenguard/manager/ExtraRouteManager;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIZ)V

    return-void

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->n:Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->f:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    sput-object p1, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->m:Ljava/util/List;

    new-instance p1, Landroid/content/Intent;

    const-string p3, "miui.action.usagestas.NEW_APP_CATEGORY_LIST"

    invoke-direct {p1, p3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    const/4 v0, 0x1

    const-string v1, "key_is_week"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->k:Z

    const-string v1, "key_is_category"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->n:Lcom/xiaomi/misettings/usagestats/d/d/e$a;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/e$a;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/io/Serializable;

    const-string v0, "weekInfo"

    invoke-virtual {p3, v0, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {p1, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a:Landroid/content/Context;

    invoke-virtual {p2, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected b()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/B;->p:Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->g:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/d/i;->c()Z

    move-result v0

    return v0

    :cond_0
    invoke-super {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/r;->b()Z

    move-result v0

    return v0
.end method
