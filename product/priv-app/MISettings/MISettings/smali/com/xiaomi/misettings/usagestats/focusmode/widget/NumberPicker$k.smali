.class Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "k"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;
    }
.end annotation


# static fields
.field private static final a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/widget/f;)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;

    return-void
.end method

.method constructor <init>(Landroid/os/Looper;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/32 :goto_1

    nop

    :goto_3
    const/4 v0, 0x1

    goto/32 :goto_0

    nop
.end method

.method a(I)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, v0, p1, v0}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_1

    nop
.end method

.method b()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Handler;->removeMessages(I)V

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method b(I)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x2

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {p0, v0, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_2

    nop

    :goto_4
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->b(I)V

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->a()V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k;->a:Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/NumberPicker$k$a;->a(I)V

    :goto_0
    return-void
.end method
