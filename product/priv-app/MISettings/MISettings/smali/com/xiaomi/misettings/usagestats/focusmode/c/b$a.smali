.class public Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/c/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field private a:[I

.field private b:I

.field private c:Z

.field private d:Z

.field private e:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/os/Handler;

.field private g:I

.field private h:Landroid/graphics/Bitmap;

.field final synthetic i:Lcom/xiaomi/misettings/usagestats/focusmode/c/b;


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/usagestats/focusmode/c/b;Landroid/widget/ImageView;[II)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->i:Lcom/xiaomi/misettings/usagestats/focusmode/c/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->h:Landroid/graphics/Bitmap;

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->f:Landroid/os/Handler;

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->a:[I

    const/4 p1, -0x1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->b:I

    new-instance p1, Ljava/lang/ref/SoftReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->e:Ljava/lang/ref/SoftReference;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->c:Z

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->d:Z

    const/16 p3, 0x3e8

    div-int/2addr p3, p4

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->g:I

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->a:[I

    aget p1, p3, p1

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object p1

    invoke-static {p2, p3, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->h:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->h:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Ljava/lang/ref/SoftReference;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->e:Ljava/lang/ref/SoftReference;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->c:Z

    return p0
.end method

.method private c()I
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->b:I

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->b:I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->a:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->b:I

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->a:[I

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->b:I

    aget v0, v0, v1

    return v0
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->g:I

    return p0
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->f:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)I
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->c()I

    move-result p0

    return p0
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)Landroid/graphics/Bitmap;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->h:Landroid/graphics/Bitmap;

    return-object p0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->c:Z

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/a;-><init>(Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->f:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/focusmode/c/b$a;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
