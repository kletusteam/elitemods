.class public Lcom/xiaomi/misettings/usagestats/d/e/i;
.super Lcom/xiaomi/misettings/usagestats/d/e/h;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/d/e/a/a;


# instance fields
.field private La:Ljava/text/SimpleDateFormat;

.field private Ma:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/text/DateFormat;->getTimeInstance()Ljava/text/DateFormat;

    move-result-object p1

    check-cast p1, Ljava/text/SimpleDateFormat;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->La:Ljava/text/SimpleDateFormat;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->La:Ljava/text/SimpleDateFormat;

    const-string v0, "HH:mm"

    invoke-virtual {p1, v0}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->F:Z

    return-void
.end method

.method private q()V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    iget-wide v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    iget-wide v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    const-wide/16 v5, 0x2

    div-long/2addr v3, v5

    invoke-static {v1, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    const/4 v1, 0x2

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->t:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->q:F

    cmpg-float v4, v4, v3

    if-gez v4, :cond_0

    iput v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->q:F

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private r()V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->Ma:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v1, v4

    if-gez v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1, v2}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    return-void
.end method


# virtual methods
.method protected d(I)Ljava/lang/String;
    .locals 7

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    add-int/lit8 v0, v0, -0x1

    sub-int p1, v0, p1

    :cond_0
    rem-int/lit8 v0, p1, 0x6

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const-string p1, ""

    return-object p1

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->La:Ljava/text/SimpleDateFormat;

    iget-wide v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->i:J

    int-to-long v3, p1

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    mul-long/2addr v3, v5

    add-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public d(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->Ma:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->Ma:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->Ma:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const v1, 0x4039999a    # 2.9f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->E:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->Ma:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->Ma:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/i;->r()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->Ma:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->B:F

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->z:F

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/i;->q()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->c()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a()V

    :cond_3
    :goto_0
    return-void
.end method

.method protected e(I)I
    .locals 0

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->la:I

    if-nez p1, :cond_0

    const p1, 0x7f06036a

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->la:I

    :cond_0
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->la:I

    return p1
.end method

.method protected f(I)F
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->Ma:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-float p1, v0

    const/high16 v0, 0x3f800000    # 1.0f

    mul-float/2addr p1, v0

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    long-to-float v0, v0

    div-float/2addr p1, v0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Z:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    sub-float v1, v0, v1

    mul-float/2addr p1, v1

    sub-float/2addr v0, p1

    return v0
.end method

.method protected g()F
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ca:F

    const/high16 v1, 0x41e00000    # 28.0f

    sub-float/2addr v0, v1

    return v0
.end method

.method protected j()F
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ba:F

    const/high16 v1, 0x41e00000    # 28.0f

    add-float/2addr v0, v1

    return v0
.end method

.method protected j(I)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/i;->Ma:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(J)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->R:Ljava/lang/String;

    return-void
.end method

.method protected k(I)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    sub-int/2addr v2, p1

    sub-int/2addr v2, v3

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x0

    aput-object v2, v1, v4

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    sub-int/2addr v2, p1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, p1, 0x1

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v3

    const p1, 0x7f1303da

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Q:Ljava/lang/String;

    return-void
.end method
