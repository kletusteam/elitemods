.class Lcom/xiaomi/misettings/usagestats/ui/j;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/j;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/j;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-virtual {v0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/j;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-virtual {v0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->c()Lcom/xiaomi/misettings/usagestats/search/AppCateSearchFragment;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/j;->a:Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;->a(Lcom/xiaomi/misettings/usagestats/ui/ActionBarFragment;Ljava/lang/String;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
