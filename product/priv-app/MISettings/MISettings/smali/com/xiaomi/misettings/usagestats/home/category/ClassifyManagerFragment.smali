.class public Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;
.super Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;


# instance fields
.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/home/category/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/xiaomi/misettings/usagestats/home/category/p;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;->r()V

    return-void
.end method

.method private p()V
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/k;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/category/b;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;->g:Lcom/xiaomi/misettings/usagestats/home/category/p;

    invoke-direct {v1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/b;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/b/a;)V

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/k$a;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    return-void
.end method

.method private q()V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/category/a;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/category/a;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private r()V
    .locals 4

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->l()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v2

    const/high16 v3, 0x41000000    # 8.0f

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v2

    invoke-virtual {v0, v1, v2, v1, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/m;

    invoke-direct {v1}, Landroidx/recyclerview/widget/m;-><init>()V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/RecyclerView$l;

    invoke-direct {v1}, Landroidx/recyclerview/widget/RecyclerView$l;-><init>()V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setRecycledViewPool(Landroidx/recyclerview/widget/RecyclerView$l;)V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/p;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;->f:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/home/category/p;-><init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Ljava/util/List;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;->g:Lcom/xiaomi/misettings/usagestats/home/category/p;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/ui/BaseRecycleViewFragment;->c:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;->g:Lcom/xiaomi/misettings/usagestats/home/category/p;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;->p()V

    return-void
.end method


# virtual methods
.method protected n()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;->q()V

    return-void
.end method

.method public synthetic o()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/d/f;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/d/f;->b(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;->f:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/q;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/category/q;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/ClassifyManagerFragment;)V

    invoke-static {v0}, Lcom/misettings/common/utils/j;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
