.class public Lcom/xiaomi/misettings/usagestats/widget/d;
.super Lmiuix/appcompat/app/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/widget/d$a;
    }
.end annotation


# instance fields
.field private final e:Lmiuix/pickerwidget/widget/TimePicker;

.field private final f:Lmiuix/pickerwidget/widget/TimePicker;

.field private g:Lcom/xiaomi/misettings/usagestats/widget/d$a;

.field h:I

.field i:I

.field j:I

.field k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IILcom/xiaomi/misettings/usagestats/widget/d$a;)V
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/j;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const/4 v0, 0x0

    const v1, 0x7f0e0097

    invoke-virtual {p1, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/j;->a(Landroid/view/View;)V

    iput-object p4, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->g:Lcom/xiaomi/misettings/usagestats/widget/d$a;

    const p4, 0x7f0b0369

    invoke-virtual {p1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    check-cast p4, Lmiuix/pickerwidget/widget/TimePicker;

    iput-object p4, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    iget-object p4, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p4, v1}, Lmiuix/pickerwidget/widget/TimePicker;->set24HourView(Ljava/lang/Boolean;)V

    div-int/lit8 p4, p2, 0x3c

    iput p4, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->h:I

    rem-int/lit8 p2, p2, 0x3c

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->i:I

    div-int/lit8 p2, p3, 0x3c

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->j:I

    rem-int/lit8 p3, p3, 0x3c

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->k:I

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->h:I

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p2, p3}, Lmiuix/pickerwidget/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    iget p3, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->i:I

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p2, p3}, Lmiuix/pickerwidget/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    const p2, 0x7f0b036c

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/pickerwidget/widget/TimePicker;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->f:Lmiuix/pickerwidget/widget/TimePicker;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->f:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {p1, v1}, Lmiuix/pickerwidget/widget/TimePicker;->set24HourView(Ljava/lang/Boolean;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->f:Lmiuix/pickerwidget/widget/TimePicker;

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->j:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lmiuix/pickerwidget/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->f:Lmiuix/pickerwidget/widget/TimePicker;

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->k:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lmiuix/pickerwidget/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    const/4 p2, 0x0

    invoke-virtual {p1, p2, p2}, Lmiuix/pickerwidget/widget/TimePicker;->setFixedContentHorizontalPadding(II)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->f:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {p1, p2, p2}, Lmiuix/pickerwidget/widget/TimePicker;->setFixedContentHorizontalPadding(II)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f130209

    invoke-virtual {p1, p2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    new-instance p2, Lcom/xiaomi/misettings/usagestats/widget/c;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/usagestats/widget/c;-><init>(Lcom/xiaomi/misettings/usagestats/widget/d;)V

    const/4 p3, -0x1

    invoke-virtual {p0, p3, p1, p2}, Lmiuix/appcompat/app/j;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f130208

    invoke-virtual {p1, p2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    const/4 p2, -0x2

    invoke-virtual {p0, p2, p1, v0}, Lmiuix/appcompat/app/j;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/widget/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/d;->h()V

    return-void
.end method

.method private g()V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->g:Lcom/xiaomi/misettings/usagestats/widget/d$a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v3}, Lmiuix/pickerwidget/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->f:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v4}, Lmiuix/pickerwidget/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->f:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v6}, Lmiuix/pickerwidget/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface/range {v0 .. v6}, Lcom/xiaomi/misettings/usagestats/widget/d$a;->a(Lmiuix/pickerwidget/widget/TimePicker;IILmiuix/pickerwidget/widget/TimePicker;II)V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->g:Lcom/xiaomi/misettings/usagestats/widget/d$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearFocus()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->f:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearFocus()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/d;->g()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/app/Dialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "miuix:hour"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "miuix:minute"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    const-string v3, "miuix:is24hour"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiuix/pickerwidget/widget/TimePicker;->set24HourView(Ljava/lang/Boolean;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lmiuix/pickerwidget/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    const-string v0, "miuix2:hour"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "miuix2:minute"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    const-string v3, "miuix2:is24hour"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v2, p1}, Lmiuix/pickerwidget/widget/TimePicker;->set24HourView(Ljava/lang/Boolean;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiuix/pickerwidget/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiuix/pickerwidget/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .locals 3

    invoke-super {p0}, Landroid/app/Dialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v2, "miuix:hour"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v2, "miuix:minute"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->e:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/TimePicker;->a()Z

    move-result v1

    const-string v2, "miuix:is24hour"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->f:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v2, "miuix2:hour"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->f:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v2, "miuix2:minute"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/widget/d;->f:Lmiuix/pickerwidget/widget/TimePicker;

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/TimePicker;->a()Z

    move-result v1

    const-string v2, "miuix2:is24hour"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v0
.end method
