.class public abstract Lcom/xiaomi/misettings/usagestats/d/e/h;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/xiaomi/misettings/usagestats/widget/b/k;


# static fields
.field public static final a:Landroid/util/SparseIntArray;

.field private static b:I


# instance fields
.field protected A:I

.field private Aa:Landroid/animation/ValueAnimator;

.field protected B:F

.field private Ba:Landroid/graphics/DashPathEffect;

.field protected C:F

.field private Ca:Z

.field private D:F

.field private Da:Z

.field protected E:I

.field private Ea:Landroid/graphics/RectF;

.field protected F:Z

.field private Fa:F

.field private G:Landroid/graphics/RectF;

.field private Ga:F

.field private H:F

.field private Ha:Landroid/graphics/RectF;

.field private I:F

.field private Ia:Landroid/graphics/PointF;

.field private J:F

.field private Ja:Landroid/graphics/PointF;

.field private K:F

.field protected Ka:Z

.field private L:F

.field private M:F

.field private N:Landroid/graphics/Paint;

.field private O:Landroid/graphics/Paint;

.field private P:I

.field protected Q:Ljava/lang/String;

.field protected R:Ljava/lang/String;

.field private S:F

.field private T:F

.field private U:Landroid/graphics/PointF;

.field protected V:F

.field private W:F

.field private X:F

.field private Y:Z

.field protected Z:F

.field protected aa:F

.field protected ba:F

.field protected c:Landroid/content/Context;

.field protected ca:F

.field private d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

.field private da:I

.field private e:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;

.field private ea:I

.field protected f:Landroid/graphics/RectF;

.field private fa:I

.field protected g:I

.field private ga:I

.field protected h:Z

.field private ha:Landroid/graphics/Paint;

.field protected i:J

.field private ia:I

.field private j:Z

.field private ja:Landroid/graphics/Paint;

.field private k:Landroid/os/Handler;

.field protected ka:I

.field private l:Z

.field protected la:I

.field private m:F

.field protected ma:I

.field private n:F

.field protected na:I

.field private o:Z

.field private oa:I

.field private p:Z

.field private pa:I

.field protected q:F

.field private qa:F

.field private r:F

.field private ra:F

.field private s:F

.field private sa:F

.field t:F

.field private ta:Landroid/animation/ValueAnimator;

.field protected u:Landroid/graphics/Paint;

.field private ua:F

.field protected v:J

.field protected va:Z

.field protected w:[Ljava/lang/String;

.field private wa:Z

.field private x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private xa:Z

.field private y:Landroid/graphics/Paint;

.field private ya:Landroid/graphics/Rect;

.field protected z:F

.field protected za:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/e/e;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/d/e/e;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/d/e/h;->a:Landroid/util/SparseIntArray;

    const/16 v0, 0x1e

    sput v0, Lcom/xiaomi/misettings/usagestats/d/e/h;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->j:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->l:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->p:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->q:F

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->v:J

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->x:Ljava/util/List;

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->W:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Y:Z

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ua:F

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->va:Z

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->wa:Z

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->xa:Z

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ca:Z

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Da:Z

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    return-void
.end method

.method private A()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->L:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Q:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->M:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->R:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    invoke-virtual {p0, v1, v2}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->V:F

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->J:F

    sub-float/2addr v1, v2

    cmpg-float v1, v1, v0

    if-gez v1, :cond_0

    const/high16 v1, 0x40000000    # 2.0f

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->T:F

    mul-float/2addr v2, v1

    add-float/2addr v0, v2

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->V:F

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/d/e/h;F)F
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->W:F

    return p1
.end method

.method private a(F)V
    .locals 4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->x:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->c()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->m()V

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->b(F)I

    move-result p1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Da:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->l(I)V

    return-void

    :cond_1
    const/4 v0, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq p1, v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->o:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->o:Z

    xor-int/2addr v3, v2

    invoke-static {v0, v3}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;Z)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->o:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->l:Z

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Z)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->l(I)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->F:Z

    if-eqz v0, :cond_3

    invoke-direct {p0, p1, v2}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(IZ)V

    goto :goto_1

    :cond_3
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    if-eqz v0, :cond_4

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->m()V

    :cond_4
    :goto_1
    if-gez p1, :cond_5

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->p()Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->v()V

    :cond_5
    return-void
.end method

.method private a(FF)V
    .locals 4

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ya:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ya:Landroid/graphics/Rect;

    :cond_0
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->qa:F

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v2, v0, v1

    const/4 v3, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ya:Landroid/graphics/Rect;

    invoke-virtual {p1, v2, v3, v0, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ya:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v0, :cond_1

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Fa:F

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Fa:F

    int-to-float p1, p1

    add-float/2addr p2, p1

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ga:F

    goto :goto_0

    :cond_1
    iput p2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ga:F

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ga:F

    int-to-float p1, p1

    sub-float/2addr p2, p1

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Fa:F

    :cond_2
    :goto_0
    return-void
.end method

.method private a(IZ)V
    .locals 4

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_3

    if-gez p1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->x:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Y:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->k(I)V

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->j(I)V

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Landroid/graphics/RectF;)V

    if-eqz p2, :cond_1

    invoke-direct {p0, v1, v2}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(ZZ)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->m()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->s()V

    :goto_0
    return-void

    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->s()V

    return-void
.end method

.method private a(Landroid/graphics/Canvas;I)V
    .locals 5

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->oa:I

    if-nez v0, :cond_0

    const v0, 0x7f06039a

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->oa:I

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->oa:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->L:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->K:F

    add-float/2addr v1, v3

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->sa:F

    add-float/2addr v1, v3

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Q:Ljava/lang/String;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->S:F

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v0, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->pa:I

    if-nez v1, :cond_1

    const v1, 0x7f06039b

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result v1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->pa:I

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->pa:I

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    invoke-virtual {v1, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->M:F

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x3f8b851f    # 1.09f

    invoke-static {v1, v3}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;)F

    move-result v1

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-static {p2, v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result p2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->R:Ljava/lang/String;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->S:F

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, p2, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method private a(Landroid/graphics/RectF;)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->U:Landroid/graphics/PointF;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->U:Landroid/graphics/PointF;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->U:Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/RectF;->right:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->U:Landroid/graphics/PointF;

    iget p1, p1, Landroid/graphics/RectF;->top:F

    iput p1, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->A()V

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->V:F

    div-float v0, p1, v3

    add-float/2addr v0, v1

    div-float v2, p1, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ca:F

    cmpl-float v3, v0, v2

    if-lez v3, :cond_1

    sub-float v1, v2, p1

    move v0, v2

    :cond_1
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ba:F

    cmpg-float v2, v1, p1

    if-gez v2, :cond_2

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->V:F

    add-float/2addr v0, p1

    goto :goto_0

    :cond_2
    move p1, v1

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->G:Landroid/graphics/RectF;

    const/4 v2, 0x0

    if-nez v1, :cond_3

    new-instance v1, Landroid/graphics/RectF;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->H:F

    invoke-direct {v1, v2, v2, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->G:Landroid/graphics/RectF;

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->G:Landroid/graphics/RectF;

    iput p1, v1, Landroid/graphics/RectF;->left:F

    iput v0, v1, Landroid/graphics/RectF;->right:F

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->sa:F

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v1, :cond_4

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->T:F

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->S:F

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->T:F

    add-float/2addr p1, v0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->S:F

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    :goto_1
    return-void
.end method

.method private a(ZZ)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Aa:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Aa:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Aa:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Aa:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Aa:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Aa:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Aa:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/e/b;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/b;-><init>(Lcom/xiaomi/misettings/usagestats/d/e/h;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Aa:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/e/g;

    invoke-direct {v1, p0, p2, p1}, Lcom/xiaomi/misettings/usagestats/d/e/g;-><init>(Lcom/xiaomi/misettings/usagestats/d/e/h;ZZ)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Aa:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/d/e/h;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->xa:Z

    return p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/d/e/h;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->wa:Z

    return p1
.end method

.method private b(F)I
    .locals 6

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->x:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->x:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->D:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v5, v3, v4

    sub-float/2addr v2, v5

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_0

    iget v1, v1, Landroid/graphics/RectF;->right:F

    div-float/2addr v3, v4

    add-float/2addr v1, v3

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 9

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ha:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->e(I)I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    const/high16 v2, 0x41200000    # 10.0f

    const/4 v3, 0x0

    const/high16 v4, 0x40c00000    # 6.0f

    const/high16 v5, 0x42c80000    # 100.0f

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    mul-float/2addr v6, v5

    float-to-int v5, v6

    const/4 v6, 0x0

    const/16 v7, 0x96

    const/16 v8, 0xdf

    invoke-static {v5, v6, v7, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ha:Landroid/graphics/RectF;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->P:I

    int-to-float v3, v2

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->W:F

    mul-float/2addr v3, v4

    int-to-float v2, v2

    mul-float/2addr v2, v4

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v2, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->clearShadowLayer()V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ja:Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ia:Landroid/graphics/PointF;

    iget v5, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Landroid/graphics/Canvas;I)V

    :cond_0
    return-void
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->m:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->n:F

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->n:F

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    cmpg-float p1, p1, v0

    const/4 v0, 0x1

    if-gez p1, :cond_2

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->p:Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->l:Z

    const-string v1, "NewBaseViewRender"

    const-string v2, "doActionMove: downY less than top line"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;Z)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    if-eqz v0, :cond_0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->m()V

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Y:Z

    if-eqz v0, :cond_1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Y:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->m()V

    :cond_1
    return-void

    :cond_2
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->p:Z

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->m:F

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(F)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/d/e/h;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->xa:Z

    return p1
.end method

.method private c(F)F
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ra:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v1, v2

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Z:F

    sub-float/2addr v3, v2

    cmpl-float v3, v0, v3

    if-ltz v3, :cond_0

    sub-float v0, p1, v2

    goto :goto_0

    :cond_0
    sub-float p1, v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    add-float/2addr v1, v2

    cmpg-float p1, p1, v1

    if-gez p1, :cond_1

    add-float/2addr v0, v2

    :cond_1
    :goto_0
    return v0
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 7

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->r:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->x:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->x:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ca:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->y:Landroid/graphics/Paint;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->e(I)I

    move-result v4

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->c(I)I

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(FII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->y:Landroid/graphics/Paint;

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->e(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    :goto_1
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ea:Landroid/graphics/RectF;

    if-nez v2, :cond_1

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ea:Landroid/graphics/RectF;

    :cond_1
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ea:Landroid/graphics/RectF;

    iget v3, v1, Landroid/graphics/RectF;->left:F

    iput v3, v2, Landroid/graphics/RectF;->left:F

    iget v3, v1, Landroid/graphics/RectF;->right:F

    iput v3, v2, Landroid/graphics/RectF;->right:F

    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    iget v3, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ua:F

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ea:Landroid/graphics/RectF;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->E:I

    int-to-float v4, v3

    int-to-float v3, v3

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->y:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4, v3, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->i(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ca:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->i(I)I

    move-result v4

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->b(I)I

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(FII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_2

    :cond_2
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->i(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    :goto_2
    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Landroid/graphics/RectF;I)F

    move-result v1

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const/high16 v5, 0x40400000    # 3.0f

    invoke-static {v4, v5}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v1, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private c(Landroid/view/MotionEvent;)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->m:F

    sub-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->n:F

    sub-float v3, v1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    cmpg-float v4, v1, v4

    const/4 v5, 0x0

    if-gez v4, :cond_1

    iput-boolean v5, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->p:Z

    const-string v0, "NewBaseViewRender"

    const-string v1, "doActionMove: moveY less than top line"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    cmpl-float v0, v2, v3

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    :cond_0
    invoke-virtual {p0, v5}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Z)V

    return-void

    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->p:Z

    iget-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->l:Z

    if-nez v4, :cond_2

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-static {v2, p1}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    :cond_2
    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->m:F

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->n:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->b(F)I

    move-result p1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->o:Z

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Z)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Da:Z

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->l(I)V

    :cond_3
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->F:Z

    if-eqz v0, :cond_4

    invoke-direct {p0, p1, v5}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(IZ)V

    :cond_4
    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/d/e/h;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Y:Z

    return p1
.end method

.method private d(Landroid/graphics/Canvas;)V
    .locals 9

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->t()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ca:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->da:I

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ea:I

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(FII)I

    move-result v0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->da:I

    :goto_0
    move v6, v0

    iget v7, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    :goto_1
    move v8, v0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ia:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ba:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ba:F

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ca:F

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v7

    move v4, v7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    invoke-static {v1, v7}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v8, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->f()F

    move-result v7

    invoke-direct {p0, v7, v8}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(FF)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->i()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ca:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h()I

    move-result v2

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->e()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(FII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    :goto_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ba:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ba:F

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ca:F

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v7

    move v4, v7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->fa:I

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    invoke-virtual {p0, v3}, Lcom/xiaomi/misettings/usagestats/d/e/h;->e(I)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(FII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->fa:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    :goto_3
    invoke-direct {p0, v7}, Lcom/xiaomi/misettings/usagestats/d/e/h;->c(F)F

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v8, v0, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v7, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Z:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ia:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ba:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ba:F

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ca:F

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v7

    move v4, v7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->fa:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->w:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    invoke-static {v1, v7}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;F)F

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v8, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private e(Landroid/graphics/Canvas;)V
    .locals 13

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->G:Landroid/graphics/RectF;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/d/e/h;->e(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    const/high16 v2, 0x41200000    # 10.0f

    const/4 v3, 0x0

    const/high16 v4, 0x40c00000    # 6.0f

    const/high16 v5, 0x42c80000    # 100.0f

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    mul-float/2addr v6, v5

    float-to-int v5, v6

    const/4 v6, 0x0

    const/16 v7, 0x96

    const/16 v8, 0xdf

    invoke-static {v5, v6, v7, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->U:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->G:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v1

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->W:F

    mul-float/2addr v3, v4

    add-float v6, v1, v3

    iget v9, v2, Landroid/graphics/RectF;->bottom:F

    iget v3, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v9

    mul-float/2addr v3, v4

    add-float v7, v9, v3

    iget v2, v2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v1

    mul-float/2addr v2, v4

    add-float v8, v1, v2

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->P:I

    int-to-float v2, v1

    mul-float v10, v2, v4

    int-to-float v1, v1

    mul-float v11, v1, v4

    iget-object v12, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    move-object v5, p1

    invoke-virtual/range {v5 .. v12}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->clearShadowLayer()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->k()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->k()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->U:Landroid/graphics/PointF;

    iget v5, v1, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->G:Landroid/graphics/RectF;

    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    move-object v2, p1

    move v3, v5

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Landroid/graphics/Canvas;I)V

    :cond_1
    return-void
.end method

.method private l(I)V
    .locals 1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->g:I

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;->a(I)V

    :cond_2
    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->F:Z

    if-nez p1, :cond_3

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->m()V

    :cond_3
    return-void
.end method

.method private q()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->r:F

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(F)F

    move-result v1

    sub-float/2addr v0, v1

    const v1, 0x7f0704c8

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h(I)F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Z:F

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->H:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->I:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->t:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    :goto_0
    return-void
.end method

.method private r()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->e:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    if-nez v0, :cond_1

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->e:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->g:I

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v3, :cond_2

    iget v0, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->h:I

    int-to-float v0, v0

    goto :goto_0

    :cond_2
    int-to-float v0, v1

    :goto_0
    iput v0, v2, Landroid/graphics/RectF;->left:F

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget v0, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->c:I

    int-to-float v0, v0

    iput v0, v1, Landroid/graphics/RectF;->left:F

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->e:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->h:I

    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->a:I

    iget-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v4, :cond_4

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->g:I

    :cond_4
    sub-int/2addr v3, v1

    int-to-float v0, v3

    iput v0, v2, Landroid/graphics/RectF;->right:F

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->a:I

    iget v0, v0, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->d:I

    sub-int/2addr v2, v0

    int-to-float v0, v2

    iput v0, v1, Landroid/graphics/RectF;->right:F

    :goto_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->e:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;

    iget v2, v1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->f:I

    int-to-float v2, v2

    iput v2, v0, Landroid/graphics/RectF;->top:F

    iget v2, v1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->b:I

    iget v1, v1, Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;->e:I

    sub-int/2addr v2, v1

    int-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method private s()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Y:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->m()V

    return-void
.end method

.method private t()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->da:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ia:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ia:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ba:Landroid/graphics/DashPathEffect;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ha:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ba:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x40c00000    # 6.0f
        0x40800000    # 4.0f
    .end array-data
.end method

.method private u()V
    .locals 3

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->F:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->p()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const v1, 0x410bae14    # 8.73f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->P:I

    const v0, 0x7f0704fb

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->T:F

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->H:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    const v0, 0x7f0704c4

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->H:F

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const/high16 v2, 0x41000000    # 8.0f

    invoke-static {v0, v2}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->I:F

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->J:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    const v0, 0x7f0704c5

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->J:F

    :cond_2
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->L:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    const v0, 0x7f0704f5

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->L:F

    :cond_3
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->M:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    const v0, 0x7f0704f6

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->M:F

    :cond_4
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    const/4 v2, 0x1

    if-nez v0, :cond_5

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->N:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :cond_5
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    if-nez v0, :cond_6

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :cond_6
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->K:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_7

    const v0, 0x7f0704c6

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->K:F

    :cond_7
    return-void
.end method

.method private v()V
    .locals 6

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->m:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Fa:F

    cmpl-float v1, v0, v1

    const/4 v2, 0x0

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ga:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->n:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->qa:F

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ra:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v5, v3, v4

    sub-float v5, v1, v5

    cmpl-float v5, v0, v5

    if-ltz v5, :cond_0

    div-float/2addr v3, v4

    add-float/2addr v1, v3

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/d/e/h;->j(I)V

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/d/e/h;->k(I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->w()V

    invoke-direct {p0, v0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(ZZ)V

    goto :goto_0

    :cond_0
    iput-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->m()V

    :goto_0
    return-void
.end method

.method private w()V
    .locals 8

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const/high16 v1, 0x41500000    # 13.0f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    sput v0, Lcom/xiaomi/misettings/usagestats/d/e/h;->b:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->y()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->A()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ia:Landroid/graphics/PointF;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    const/high16 v2, 0x40c00000    # 6.0f

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ga:F

    add-float/2addr v1, v2

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Fa:F

    sub-float/2addr v1, v2

    :goto_0
    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ia:Landroid/graphics/PointF;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->qa:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ja:Landroid/graphics/PointF;

    iput v1, v2, Landroid/graphics/PointF;->y:F

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sget v1, Lcom/xiaomi/misettings/usagestats/d/e/h;->b:I

    iget-boolean v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, -0x1

    :goto_1
    mul-int/2addr v1, v3

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, v2, Landroid/graphics/PointF;->x:F

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mLineStartX"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ba:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "---mLineEnd"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ca:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "-----"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ja:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ia:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getAvgTipRect"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ia:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ba:F

    goto :goto_2

    :cond_2
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ca:F

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ia:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    :goto_2
    sub-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ia:Landroid/graphics/PointF;

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v2, :cond_3

    iget v2, v1, Landroid/graphics/PointF;->x:F

    int-to-float v3, v0

    sub-float/2addr v2, v3

    goto :goto_3

    :cond_3
    iget v2, v1, Landroid/graphics/PointF;->x:F

    int-to-float v3, v0

    add-float/2addr v2, v3

    :goto_3
    iput v2, v1, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ja:Landroid/graphics/PointF;

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v2, :cond_4

    iget v2, v1, Landroid/graphics/PointF;->x:F

    int-to-float v3, v0

    sub-float/2addr v2, v3

    goto :goto_4

    :cond_4
    iget v2, v1, Landroid/graphics/PointF;->x:F

    int-to-float v3, v0

    add-float/2addr v2, v3

    :goto_4
    iput v2, v1, Landroid/graphics/PointF;->x:F

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAvgTipRect: avgStart="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ia:Landroid/graphics/PointF;

    invoke-virtual {v2}, Landroid/graphics/PointF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",avgEnd="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ja:Landroid/graphics/PointF;

    invoke-virtual {v2}, Landroid/graphics/PointF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NewBaseViewRender"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ga:F

    sget v2, Lcom/xiaomi/misettings/usagestats/d/e/h;->b:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->V:F

    add-float/2addr v2, v1

    goto :goto_5

    :cond_5
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Fa:F

    sget v2, Lcom/xiaomi/misettings/usagestats/d/e/h;->b:I

    int-to-float v2, v2

    sub-float v2, v1, v2

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->V:F

    sub-float v1, v2, v1

    :goto_5
    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->qa:F

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->H:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    cmpg-float v5, v3, v4

    if-gez v5, :cond_6

    move v3, v4

    :cond_6
    iget v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->H:F

    add-float v5, v3, v4

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Z:F

    cmpl-float v7, v5, v6

    if-lez v7, :cond_7

    sub-float v3, v6, v4

    move v5, v6

    :cond_7
    iput v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->sa:F

    iget-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v4, :cond_8

    int-to-float v4, v0

    sub-float/2addr v1, v4

    goto :goto_6

    :cond_8
    int-to-float v4, v0

    add-float/2addr v1, v4

    :goto_6
    iget-boolean v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v4, :cond_9

    int-to-float v0, v0

    sub-float/2addr v2, v0

    goto :goto_7

    :cond_9
    int-to-float v0, v0

    add-float/2addr v2, v0

    :goto_7
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->T:F

    sub-float v0, v2, v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->S:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_8

    :cond_a
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->T:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->S:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->O:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    :goto_8
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ha:Landroid/graphics/RectF;

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iput v2, v0, Landroid/graphics/RectF;->right:F

    iput v3, v0, Landroid/graphics/RectF;->top:F

    iput v5, v0, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method private x()V
    .locals 7

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g()F

    move-result v0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->j()F

    move-result v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    int-to-float v2, v1

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->z:F

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->D:F

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->j()F

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->x:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->A:I

    if-ge v1, v2, :cond_0

    int-to-float v2, v1

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->z:F

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->D:F

    add-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->x:Ljava/util/List;

    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->f(I)F

    move-result v4

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->z:F

    add-float/2addr v5, v0

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Z:F

    invoke-direct {v3, v0, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->j()F

    move-result v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private y()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ha:Landroid/graphics/RectF;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ha:Landroid/graphics/RectF;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ia:Landroid/graphics/PointF;

    if-nez v0, :cond_1

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ia:Landroid/graphics/PointF;

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ja:Landroid/graphics/PointF;

    if-nez v0, :cond_2

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ja:Landroid/graphics/PointF;

    :cond_2
    return-void
.end method

.method private z()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->t:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->fa:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    if-eqz v1, :cond_0

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    goto :goto_0

    :cond_0
    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    :goto_0
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ja:Landroid/graphics/Paint;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/graphics/Paint;)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ra:F

    return-void
.end method


# virtual methods
.method protected a(Landroid/graphics/RectF;I)F
    .locals 1

    iget p1, p1, Landroid/graphics/RectF;->left:F

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->z:F

    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr p2, v0

    add-float/2addr p1, p2

    return p1
.end method

.method protected a(Ljava/lang/String;Landroid/graphics/Paint;)F
    .locals 0

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result p1

    return p1
.end method

.method protected a(J)J
    .locals 6

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    rem-long v2, p1, v0

    const-wide/32 v4, 0xc350

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    add-long/2addr p1, v0

    :cond_0
    return-wide p1
.end method

.method public a()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Y:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->va:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->wa:Z

    if-eqz v0, :cond_1

    const-string v0, "NewBaseViewRender"

    const-string v1, "animShow: isAniming"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->xa:Z

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->d()V

    return-void
.end method

.method public synthetic a(Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ua:F

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->m()V

    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ca:Z

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->d(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->c(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Y:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->e(Landroid/graphics/Canvas;)V

    :cond_1
    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->b(Landroid/graphics/Canvas;)V

    :cond_2
    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 0

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->e:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView$a;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->c()V

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;)V
    .locals 2

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-static {}, Lcom/xiaomi/misettings/f;->c()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_1
    return-void
.end method

.method public synthetic a(ZLandroid/animation/ValueAnimator;)V
    .locals 0

    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p2

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    if-nez p1, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    sub-float/2addr p1, p2

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->X:F

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->m()V

    return-void
.end method

.method public a(I)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    const/4 p1, 0x3

    if-eq v0, p1, :cond_1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->c(Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroid/view/View;Z)V

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(Z)V

    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->l:Z

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->F:Z

    if-eqz p1, :cond_2

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Y:Z

    if-nez p1, :cond_3

    :cond_2
    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    if-eqz p1, :cond_5

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->p()Z

    move-result p1

    if-eqz p1, :cond_5

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->k:Landroid/os/Handler;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/e/a;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/d/e/a;-><init>(Lcom/xiaomi/misettings/usagestats/d/e/h;)V

    const-wide/16 v1, 0x640

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_4
    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->l:Z

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->k:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/e/h;->b(Landroid/view/MotionEvent;)V

    :cond_5
    :goto_0
    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->p:Z

    return p1
.end method

.method protected b(I)I
    .locals 0

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ga:I

    return p1
.end method

.method protected b()V
    .locals 3

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    const/high16 v1, 0x41800000    # 16.0f

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->q:F

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ba:F

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ca:F

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->f:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ba:F

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->q:F

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ca:F

    :goto_0
    return-void
.end method

.method protected c(I)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public c()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->u()V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->i:J

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->r()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->q()V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->b()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->x()V

    return-void
.end method

.method protected abstract d(I)Ljava/lang/String;
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ta:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ta:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ta:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ta:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ta:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/e/c;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/d/e/c;-><init>(Lcom/xiaomi/misettings/usagestats/d/e/h;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ta:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/e/f;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/d/e/f;-><init>(Lcom/xiaomi/misettings/usagestats/d/e/h;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ta:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ta:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected e()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract e(I)I
.end method

.method protected f()F
    .locals 3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->aa:F

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Z:F

    sub-float/2addr v1, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method protected abstract f(I)F
.end method

.method protected abstract g()F
.end method

.method protected g(I)I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    return p1
.end method

.method protected h(I)F
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    return p1
.end method

.method protected h()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->da:I

    return v0
.end method

.method protected i()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ia:I

    return v0
.end method

.method protected i(I)I
    .locals 0

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->fa:I

    return p1
.end method

.method protected abstract j()F
.end method

.method protected abstract j(I)V
.end method

.method protected k()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract k(I)V
.end method

.method public l()V
    .locals 3

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->k:Landroid/os/Handler;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->h:Z

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->r:F

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(F)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->s:F

    const v0, 0x7f0704b6

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->r:F

    const v0, 0x7f0704b7

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->t:F

    const v0, 0x7f060361

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->da:I

    const v0, 0x7f06037a

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->fa:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f060379

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ga:I

    const v0, 0x7f060360

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->g(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->ea:I

    :cond_1
    const v0, 0x7f0704c7

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->C:F

    const v0, 0x7f0704ba

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->h(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->B:F

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->u:Landroid/graphics/Paint;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->fa:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->y:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->y:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->y:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->z()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->E:I

    invoke-static {}, Ljava/text/SimpleDateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->za:Ljava/text/SimpleDateFormat;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->za:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->c:Landroid/content/Context;

    const v2, 0x7f1303e2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    return-void
.end method

.method protected m()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->d:Lcom/xiaomi/misettings/usagestats/widget/NewBarChartView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    :cond_0
    return-void
.end method

.method public synthetic n()V
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Ka:Z

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/xiaomi/misettings/usagestats/d/e/h;->a(ZZ)V

    return-void
.end method

.method public o()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/d/e/h;->Da:Z

    return-void
.end method

.method protected p()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
