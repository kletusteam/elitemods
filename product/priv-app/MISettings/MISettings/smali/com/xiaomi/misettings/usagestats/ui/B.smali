.class Lcom/xiaomi/misettings/usagestats/ui/B;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:I

.field final synthetic c:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;ZI)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->c:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->a:Z

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "onClick: positive "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->a:Z

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "NewAppUsageDetailFragment"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->c:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    iget p2, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->b:I

    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;I)I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->c:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->c(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->c:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->d(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x1

    invoke-static {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->a:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->c:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object p1

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->c:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;Z)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->c:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object p1

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/B;->c:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->e(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V

    :goto_0
    return-void
.end method
