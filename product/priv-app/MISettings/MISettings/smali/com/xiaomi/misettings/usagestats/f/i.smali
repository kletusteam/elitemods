.class public Lcom/xiaomi/misettings/usagestats/f/i;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/f/i$a;
    }
.end annotation


# instance fields
.field private a:Lcom/xiaomi/misettings/usagestats/f/j;

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/i$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:J

.field private j:I

.field private k:Z


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/usagestats/f/j;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->j:I

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->k:Z

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->a:Lcom/xiaomi/misettings/usagestats/f/j;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->c:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->d:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->e:Ljava/util/List;

    new-instance p1, Landroid/util/ArrayMap;

    invoke-direct {p1}, Landroid/util/ArrayMap;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->f:Landroid/util/ArrayMap;

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->b:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->g:I

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->h:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->i:J

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/f/i;->i()V

    return-void
.end method

.method private c(J)I
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->a:Lcom/xiaomi/misettings/usagestats/f/j;

    iget-wide v0, v0, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v2, p1, v0

    const/4 v3, -0x1

    if-ltz v2, :cond_2

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    add-long/2addr v4, v0

    cmp-long v2, p1, v4

    if-lez v2, :cond_0

    goto :goto_1

    :cond_0
    sub-long/2addr p1, v0

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    div-long/2addr p1, v0

    long-to-int p1, p1

    if-ltz p1, :cond_1

    sget p2, Lcom/xiaomi/misettings/usagestats/utils/L;->c:I

    if-ge p1, p2, :cond_1

    goto :goto_0

    :cond_1
    move p1, v3

    :goto_0
    return p1

    :cond_2
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "addUnlock()... incorrect time:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "LR-DayDeviceUsageStats"

    invoke-static {p2, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return v3
.end method

.method private h()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->f:Landroid/util/ArrayMap;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move v2, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/f/i;->f:Landroid/util/ArrayMap;

    invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/i$a;

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/i$a;->e()I

    move-result v4

    add-int/2addr v1, v4

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/i$a;->f()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    move v2, v1

    :cond_1
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->g:I

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "This stat has incorrect data. totalNotification="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/f/i;->g:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ",total="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LR-DayDeviceUsageStats"

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iput v2, p0, Lcom/xiaomi/misettings/usagestats/f/i;->h:I

    return-void
.end method

.method private i()V
    .locals 3

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    sget v2, Lcom/xiaomi/misettings/usagestats/utils/L;->c:I

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/f/i;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/f/i;->e:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/xiaomi/misettings/usagestats/f/j;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->a:Lcom/xiaomi/misettings/usagestats/f/j;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->b:I

    return-void
.end method

.method public a(J)V
    .locals 4

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->i:J

    cmp-long v2, v0, p1

    if-gtz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->i:J

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/f/i;->c(J)I

    move-result v0

    if-ltz v0, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->d:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->c:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    add-int/lit8 p2, p2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->b:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->b:I

    :cond_2
    return-void
.end method

.method public a(Landroid/util/ArrayMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/i$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->f:Landroid/util/ArrayMap;

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/f/i;->h()V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->c:Ljava/util/List;

    return-void
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->i:J

    return-wide v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->j:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->k:Z

    return-void
.end method

.method public b(J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->i:J

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->g:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->b:I

    return v0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->c:Ljava/util/List;

    return-object v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->j:I

    return v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/f/i;->k:Z

    return v0
.end method
