.class public Lcom/xiaomi/misettings/usagestats/home/category/k;
.super Landroidx/recyclerview/widget/RecyclerView$f;

# interfaces
.implements Landroidx/recyclerview/widget/RecyclerView$h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/home/category/k$d;,
        Lcom/xiaomi/misettings/usagestats/home/category/k$a;,
        Lcom/xiaomi/misettings/usagestats/home/category/k$b;,
        Lcom/xiaomi/misettings/usagestats/home/category/k$c;
    }
.end annotation


# instance fields
.field private A:Lcom/xiaomi/misettings/usagestats/home/category/k$b;

.field private final B:Landroidx/recyclerview/widget/RecyclerView$j;

.field private C:Landroid/graphics/Rect;

.field private D:J

.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final b:[F

.field c:Landroidx/recyclerview/widget/RecyclerView$t;

.field d:F

.field e:F

.field private f:F

.field private g:F

.field h:F

.field i:F

.field private j:F

.field private k:F

.field l:I

.field m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private n:I

.field o:I

.field p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/home/category/k$c;",
            ">;"
        }
    .end annotation
.end field

.field private q:I

.field r:Landroidx/recyclerview/widget/RecyclerView;

.field final s:Ljava/lang/Runnable;

.field t:Landroid/view/VelocityTracker;

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroidx/recyclerview/widget/RecyclerView$t;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private w:Landroidx/recyclerview/widget/RecyclerView$d;

.field x:Landroid/view/View;

.field y:I

.field z:Landroidx/core/view/e;


# direct methods
.method public constructor <init>(Lcom/xiaomi/misettings/usagestats/home/category/k$a;)V
    .locals 3
    .param p1    # Lcom/xiaomi/misettings/usagestats/home/category/k$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$f;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->a:Ljava/util/List;

    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->b:[F

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    const/4 v1, -0x1

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->n:I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    new-instance v2, Lcom/xiaomi/misettings/usagestats/home/category/d;

    invoke-direct {v2, p0}, Lcom/xiaomi/misettings/usagestats/home/category/d;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/k;)V

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->s:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->w:Landroidx/recyclerview/widget/RecyclerView$d;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->x:Landroid/view/View;

    iput v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->y:I

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/e;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/category/e;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/k;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->B:Landroidx/recyclerview/widget/RecyclerView$j;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    return-void
.end method

.method private a([F)V
    .locals 3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->o:I

    and-int/lit8 v0, v0, 0xc

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->j:F

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v2, v2, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    aput v0, p1, v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    aput v0, p1, v1

    :goto_0
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->o:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->k:F

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v2, v2, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    aput v0, p1, v1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    aput v0, p1, v1

    :goto_1
    return-void
.end method

.method private static a(Landroid/view/View;FFFF)Z
    .locals 1

    cmpl-float v0, p1, p3

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr p3, v0

    cmpg-float p1, p1, p3

    if-gtz p1, :cond_0

    cmpl-float p1, p2, p4

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p0

    int-to-float p0, p0

    add-float/2addr p4, p0

    cmpg-float p0, p2, p4

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private b(Landroidx/recyclerview/widget/RecyclerView$t;I)I
    .locals 8

    and-int/lit8 v0, p2, 0xc

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    const/16 v2, 0x8

    const/4 v3, 0x4

    if-lez v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    if-eqz v4, :cond_2

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    const/4 v6, -0x1

    if-le v5, v6, :cond_2

    const/16 v5, 0x3e8

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget v7, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->g:F

    invoke-virtual {v6, v7}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(F)F

    invoke-virtual {v4, v5, v7}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    invoke-virtual {v4, v5}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v4

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    invoke-virtual {v5, v6}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v5

    cmpl-float v1, v4, v1

    if-lez v1, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    and-int v3, v2, p2

    if-eqz v3, :cond_2

    if-ne v0, v2, :cond_2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->f:F

    invoke-virtual {v3, v4}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(F)F

    cmpl-float v3, v1, v4

    if-ltz v3, :cond_2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    return v2

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    invoke-virtual {v2, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(Landroidx/recyclerview/widget/RecyclerView$t;)F

    move-result p1

    mul-float/2addr v1, p1

    and-int p1, p2, v0

    if-eqz p1, :cond_3

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    cmpl-float p1, p1, v1

    if-lez p1, :cond_3

    return v0

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method private b(Landroidx/recyclerview/widget/RecyclerView$t;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView$t;",
            ")",
            "Ljava/util/List<",
            "Landroidx/recyclerview/widget/RecyclerView$t;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->u:Ljava/util/List;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->u:Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->v:Ljava/util/List;

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->v:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    :goto_0
    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a()I

    move-result v2

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->j:F

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    add-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    sub-int/2addr v3, v2

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->k:F

    iget v5, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    add-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int/2addr v4, v2

    iget-object v5, v1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v5, v3

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v5, v2

    iget-object v6, v1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v4

    add-int/2addr v6, v2

    add-int v2, v3, v5

    div-int/lit8 v2, v2, 0x2

    add-int v7, v4, v6

    div-int/lit8 v7, v7, 0x2

    iget-object v8, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v8}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object v8

    if-nez v8, :cond_1

    const/4 v10, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v8}, Landroidx/recyclerview/widget/RecyclerView$g;->e()I

    move-result v10

    :goto_1
    const/4 v11, 0x0

    :goto_2
    if-ge v11, v10, :cond_5

    invoke-virtual {v8, v11}, Landroidx/recyclerview/widget/RecyclerView$g;->d(I)Landroid/view/View;

    move-result-object v12

    if-nez v12, :cond_2

    goto/16 :goto_4

    :cond_2
    iget-object v13, v1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    if-eq v12, v13, :cond_4

    invoke-virtual {v12}, Landroid/view/View;->getBottom()I

    move-result v13

    if-lt v13, v4, :cond_4

    invoke-virtual {v12}, Landroid/view/View;->getTop()I

    move-result v13

    if-gt v13, v6, :cond_4

    invoke-virtual {v12}, Landroid/view/View;->getRight()I

    move-result v13

    if-lt v13, v3, :cond_4

    invoke-virtual {v12}, Landroid/view/View;->getLeft()I

    move-result v13

    if-gt v13, v5, :cond_4

    iget-object v13, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v13, v12}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object v13

    iget-object v14, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v15, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-virtual {v14, v15, v9, v13}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v12}, Landroid/view/View;->getLeft()I

    move-result v9

    invoke-virtual {v12}, Landroid/view/View;->getRight()I

    move-result v14

    add-int/2addr v9, v14

    div-int/lit8 v9, v9, 0x2

    sub-int v9, v2, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    invoke-virtual {v12}, Landroid/view/View;->getTop()I

    move-result v14

    invoke-virtual {v12}, Landroid/view/View;->getBottom()I

    move-result v12

    add-int/2addr v14, v12

    div-int/lit8 v14, v14, 0x2

    sub-int v12, v7, v14

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v12

    mul-int/2addr v9, v9

    mul-int/2addr v12, v12

    add-int/2addr v9, v12

    iget-object v12, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->u:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    const/4 v14, 0x0

    const/4 v15, 0x0

    :goto_3
    if-ge v14, v12, :cond_3

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->v:Ljava/util/List;

    invoke-interface {v1, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v9, v1, :cond_3

    add-int/lit8 v15, v15, 0x1

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v1, p1

    goto :goto_3

    :cond_3
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->u:Ljava/util/List;

    invoke-interface {v1, v15, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->v:Ljava/util/List;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v1, v15, v9}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_4
    :goto_4
    add-int/lit8 v11, v11, 0x1

    move-object/from16 v1, p1

    goto/16 :goto_2

    :cond_5
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->u:Ljava/util/List;

    return-object v1
.end method

.method private c(Landroidx/recyclerview/widget/RecyclerView$t;)I
    .locals 5

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->n:I

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v2, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->c(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)I

    move-result v0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v3}, Landroidx/core/view/ViewCompat;->l(Landroid/view/View;)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(II)I

    move-result v2

    const v3, 0xff00

    and-int/2addr v2, v3

    shr-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_1

    return v1

    :cond_1
    and-int/2addr v0, v3

    shr-int/lit8 v0, v0, 0x8

    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    invoke-direct {p0, p1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/k;->b(Landroidx/recyclerview/widget/RecyclerView$t;I)I

    move-result v3

    if-lez v3, :cond_3

    and-int p1, v0, v3

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->l(Landroid/view/View;)I

    move-result p1

    invoke-static {v3, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(II)I

    move-result p1

    return p1

    :cond_2
    return v3

    :cond_3
    invoke-direct {p0, p1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/k;->c(Landroidx/recyclerview/widget/RecyclerView$t;I)I

    move-result p1

    if-lez p1, :cond_7

    return p1

    :cond_4
    invoke-direct {p0, p1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/k;->c(Landroidx/recyclerview/widget/RecyclerView$t;I)I

    move-result v3

    if-lez v3, :cond_5

    return v3

    :cond_5
    invoke-direct {p0, p1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/k;->b(Landroidx/recyclerview/widget/RecyclerView$t;I)I

    move-result p1

    if-lez p1, :cond_7

    and-int/2addr v0, p1

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->l(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(II)I

    move-result p1

    :cond_6
    return p1

    :cond_7
    return v1
.end method

.method private c(Landroidx/recyclerview/widget/RecyclerView$t;I)I
    .locals 8

    and-int/lit8 v0, p2, 0x3

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-lez v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    if-eqz v4, :cond_2

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    const/4 v6, -0x1

    if-le v5, v6, :cond_2

    const/16 v5, 0x3e8

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget v7, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->g:F

    invoke-virtual {v6, v7}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(F)F

    invoke-virtual {v4, v5, v7}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    invoke-virtual {v4, v5}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v4

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    invoke-virtual {v5, v6}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v5

    cmpl-float v1, v5, v1

    if-lez v1, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v1

    and-int v3, v2, p2

    if-eqz v3, :cond_2

    if-ne v2, v0, :cond_2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->f:F

    invoke-virtual {v3, v5}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(F)F

    cmpl-float v3, v1, v5

    if-ltz v3, :cond_2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    return v2

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    invoke-virtual {v2, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(Landroidx/recyclerview/widget/RecyclerView$t;)F

    move-result p1

    mul-float/2addr v1, p1

    and-int p1, p2, v0

    if-eqz p1, :cond_3

    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    cmpl-float p1, p1, v1

    if-lez p1, :cond_3

    return v0

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method private d(Landroid/view/MotionEvent;)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    return-object v2

    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->d:F

    sub-float/2addr v3, v4

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->e:F

    sub-float/2addr v1, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->q:I

    int-to-float v5, v4

    cmpg-float v5, v3, v5

    if-gez v5, :cond_1

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_1

    return-object v2

    :cond_1
    cmpl-float v4, v3, v1

    if-lez v4, :cond_2

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$g;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    return-object v2

    :cond_2
    cmpl-float v1, v1, v3

    if-lez v1, :cond_3

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$g;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    return-object v2

    :cond_3
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->b(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object p1

    if-nez p1, :cond_4

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object v2

    :goto_0
    return-object v2
.end method

.method private d()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->w:Landroidx/recyclerview/widget/RecyclerView$d;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/h;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/category/h;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/k;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->w:Landroidx/recyclerview/widget/RecyclerView$d;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->w:Landroidx/recyclerview/widget/RecyclerView$d;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setChildDrawingOrderCallback(Landroidx/recyclerview/widget/RecyclerView$d;)V

    :cond_1
    return-void
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/RecyclerView;->removeItemDecoration(Landroidx/recyclerview/widget/RecyclerView$f;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->B:Landroidx/recyclerview/widget/RecyclerView$j;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->removeOnItemTouchListener(Landroidx/recyclerview/widget/RecyclerView$j;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/RecyclerView;->removeOnChildAttachStateChangeListener(Landroidx/recyclerview/widget/RecyclerView$h;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-virtual {v2, v3, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->x:Landroid/view/View;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->y:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/k;->f()V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/k;->i()V

    return-void
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->q:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$f;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->B:Landroidx/recyclerview/widget/RecyclerView$j;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnItemTouchListener(Landroidx/recyclerview/widget/RecyclerView$j;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/RecyclerView;->addOnChildAttachStateChangeListener(Landroidx/recyclerview/widget/RecyclerView$h;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/k;->h()V

    return-void
.end method

.method private h()V
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/home/category/k$b;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/home/category/k$b;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/k;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->A:Lcom/xiaomi/misettings/usagestats/home/category/k$b;

    new-instance v0, Landroidx/core/view/e;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->A:Lcom/xiaomi/misettings/usagestats/home/category/k$b;

    invoke-direct {v0, v1, v2}, Landroidx/core/view/e;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->z:Landroidx/core/view/e;

    return-void
.end method

.method private i()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->A:Lcom/xiaomi/misettings/usagestats/home/category/k$b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/category/k$b;->a()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->A:Lcom/xiaomi/misettings/usagestats/home/category/k$b;

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->z:Landroidx/core/view/e;

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->z:Landroidx/core/view/e;

    :cond_1
    return-void
.end method


# virtual methods
.method a(Landroid/view/MotionEvent;)Lcom/xiaomi/misettings/usagestats/home/category/k$c;
    .locals 4

    goto/32 :goto_6

    nop

    :goto_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    goto/32 :goto_7

    nop

    :goto_2
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_e

    nop

    :goto_3
    iget-object v3, v3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_12

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_f

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_6
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    goto/32 :goto_5

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_c

    nop

    :goto_8
    return-object v1

    :goto_9
    return-object v2

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    const/4 v1, 0x0

    goto/32 :goto_4

    nop

    :goto_c
    add-int/lit8 v0, v0, -0x1

    :goto_d
    goto/32 :goto_13

    nop

    :goto_e
    check-cast v2, Lcom/xiaomi/misettings/usagestats/home/category/k$c;

    goto/32 :goto_15

    nop

    :goto_f
    return-object v1

    :goto_10
    goto/32 :goto_14

    nop

    :goto_11
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_16

    nop

    :goto_12
    if-eq v3, p1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_13
    if-gez v0, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_0

    nop

    :goto_14
    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->b(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_15
    iget-object v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_3

    nop

    :goto_16
    goto :goto_d

    :goto_17
    goto/32 :goto_8

    nop
.end method

.method a(ILandroid/view/MotionEvent;I)V
    .locals 8

    goto/32 :goto_2f

    nop

    :goto_0
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b()Z

    move-result p1

    goto/32 :goto_22

    nop

    :goto_1
    cmpl-float p3, p3, v5

    goto/32 :goto_18

    nop

    :goto_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_31

    nop

    :goto_3
    invoke-virtual {v2, v3, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)I

    move-result v2

    goto/32 :goto_46

    nop

    :goto_4
    const/4 v0, 0x2

    goto/32 :goto_12

    nop

    :goto_5
    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->d:F

    goto/32 :goto_37

    nop

    :goto_6
    if-gez v6, :cond_0

    goto/32 :goto_2b

    :cond_0
    :goto_7
    goto/32 :goto_32

    nop

    :goto_8
    invoke-virtual {p2, p3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result p2

    goto/32 :goto_40

    nop

    :goto_9
    and-int/lit8 p3, v2, 0x8

    goto/32 :goto_1b

    nop

    :goto_a
    if-nez p1, :cond_1

    goto/32 :goto_2b

    :cond_1
    goto/32 :goto_36

    nop

    :goto_b
    sub-float/2addr p3, v4

    goto/32 :goto_19

    nop

    :goto_c
    if-ltz v3, :cond_2

    goto/32 :goto_3c

    :cond_2
    goto/32 :goto_30

    nop

    :goto_d
    const/4 v5, 0x0

    goto/32 :goto_e

    nop

    :goto_e
    if-gtz v4, :cond_3

    goto/32 :goto_11

    :cond_3
    goto/32 :goto_14

    nop

    :goto_f
    const/4 p3, 0x0

    goto/32 :goto_8

    nop

    :goto_10
    return-void

    :goto_11
    goto/32 :goto_1f

    nop

    :goto_12
    if-eq p1, v0, :cond_4

    goto/32 :goto_2b

    :cond_4
    goto/32 :goto_28

    nop

    :goto_13
    if-gtz p3, :cond_5

    goto/32 :goto_26

    :cond_5
    goto/32 :goto_9

    nop

    :goto_14
    cmpg-float p3, v3, v5

    goto/32 :goto_24

    nop

    :goto_15
    int-to-float v7, v6

    goto/32 :goto_3e

    nop

    :goto_16
    if-ne p1, v1, :cond_6

    goto/32 :goto_2b

    :cond_6
    goto/32 :goto_44

    nop

    :goto_17
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    goto/32 :goto_43

    nop

    :goto_18
    if-gtz p3, :cond_7

    goto/32 :goto_26

    :cond_7
    goto/32 :goto_29

    nop

    :goto_19
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    goto/32 :goto_17

    nop

    :goto_1a
    if-ltz v7, :cond_8

    goto/32 :goto_7

    :cond_8
    goto/32 :goto_45

    nop

    :goto_1b
    if-eqz p3, :cond_9

    goto/32 :goto_26

    :cond_9
    goto/32 :goto_10

    nop

    :goto_1c
    iput v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    goto/32 :goto_f

    nop

    :goto_1d
    return-void

    :goto_1e
    goto/32 :goto_39

    nop

    :goto_1f
    cmpg-float v3, p3, v5

    goto/32 :goto_c

    nop

    :goto_20
    invoke-virtual {p2, p3}, Landroid/view/MotionEvent;->getY(I)F

    move-result p3

    goto/32 :goto_5

    nop

    :goto_21
    invoke-virtual {p2, p3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    goto/32 :goto_20

    nop

    :goto_22
    if-nez p1, :cond_a

    goto/32 :goto_2b

    :cond_a
    goto/32 :goto_2

    nop

    :goto_23
    if-nez v2, :cond_b

    goto/32 :goto_2b

    :cond_b
    goto/32 :goto_21

    nop

    :goto_24
    if-ltz p3, :cond_c

    goto/32 :goto_1e

    :cond_c
    goto/32 :goto_41

    nop

    :goto_25
    return-void

    :goto_26
    goto/32 :goto_34

    nop

    :goto_27
    and-int/2addr v2, v3

    goto/32 :goto_2d

    nop

    :goto_28
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->n:I

    goto/32 :goto_42

    nop

    :goto_29
    and-int/lit8 p3, v2, 0x2

    goto/32 :goto_47

    nop

    :goto_2a
    invoke-virtual {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView$t;I)V

    :goto_2b
    goto/32 :goto_33

    nop

    :goto_2c
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_3

    nop

    :goto_2d
    shr-int/lit8 v2, v2, 0x8

    goto/32 :goto_23

    nop

    :goto_2e
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    goto/32 :goto_0

    nop

    :goto_2f
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_3f

    nop

    :goto_30
    and-int/lit8 v3, v2, 0x1

    goto/32 :goto_48

    nop

    :goto_31
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getScrollState()I

    move-result p1

    goto/32 :goto_3d

    nop

    :goto_32
    cmpl-float v4, v4, v5

    goto/32 :goto_d

    nop

    :goto_33
    return-void

    :goto_34
    iput v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    goto/32 :goto_1c

    nop

    :goto_35
    if-eqz p3, :cond_d

    goto/32 :goto_1e

    :cond_d
    goto/32 :goto_1d

    nop

    :goto_36
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    goto/32 :goto_2c

    nop

    :goto_37
    sub-float/2addr v3, v4

    goto/32 :goto_38

    nop

    :goto_38
    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->e:F

    goto/32 :goto_b

    nop

    :goto_39
    cmpl-float p3, v3, v5

    goto/32 :goto_13

    nop

    :goto_3a
    cmpl-float v6, v5, v6

    goto/32 :goto_6

    nop

    :goto_3b
    return-void

    :goto_3c
    goto/32 :goto_1

    nop

    :goto_3d
    const/4 v1, 0x1

    goto/32 :goto_16

    nop

    :goto_3e
    cmpl-float v7, v4, v7

    goto/32 :goto_1a

    nop

    :goto_3f
    if-eqz v0, :cond_e

    goto/32 :goto_2b

    :cond_e
    goto/32 :goto_4

    nop

    :goto_40
    iput p2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    goto/32 :goto_2a

    nop

    :goto_41
    and-int/lit8 p3, v2, 0x4

    goto/32 :goto_35

    nop

    :goto_42
    if-ne p1, v0, :cond_f

    goto/32 :goto_2b

    :cond_f
    goto/32 :goto_2e

    nop

    :goto_43
    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->q:I

    goto/32 :goto_15

    nop

    :goto_44
    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/usagestats/home/category/k;->d(Landroid/view/MotionEvent;)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object p1

    goto/32 :goto_a

    nop

    :goto_45
    int-to-float v6, v6

    goto/32 :goto_3a

    nop

    :goto_46
    const v3, 0xff00

    goto/32 :goto_27

    nop

    :goto_47
    if-eqz p3, :cond_10

    goto/32 :goto_26

    :cond_10
    goto/32 :goto_25

    nop

    :goto_48
    if-eqz v3, :cond_11

    goto/32 :goto_3c

    :cond_11
    goto/32 :goto_3b

    nop
.end method

.method a(Landroid/view/MotionEvent;II)V
    .locals 1

    goto/32 :goto_17

    nop

    :goto_0
    invoke-static {p3, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    goto/32 :goto_11

    nop

    :goto_1
    and-int/lit8 p1, p2, 0x8

    goto/32 :goto_21

    nop

    :goto_2
    invoke-virtual {p1, p3}, Landroid/view/MotionEvent;->getY(I)F

    move-result p1

    goto/32 :goto_1a

    nop

    :goto_3
    if-eqz p1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_19

    nop

    :goto_4
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    goto/32 :goto_1c

    nop

    :goto_5
    invoke-static {p3, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    goto/32 :goto_c

    nop

    :goto_6
    return-void

    :goto_7
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    goto/32 :goto_15

    nop

    :goto_8
    and-int/lit8 p1, p2, 0x2

    goto/32 :goto_3

    nop

    :goto_9
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    :goto_a
    goto/32 :goto_1

    nop

    :goto_b
    sub-float/2addr p1, p3

    goto/32 :goto_10

    nop

    :goto_c
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    :goto_d
    goto/32 :goto_6

    nop

    :goto_e
    and-int/lit8 p1, p2, 0x1

    goto/32 :goto_20

    nop

    :goto_f
    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    goto/32 :goto_1f

    nop

    :goto_10
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    goto/32 :goto_14

    nop

    :goto_11
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    :goto_12
    goto/32 :goto_e

    nop

    :goto_13
    if-eqz p1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_4

    nop

    :goto_14
    and-int/lit8 p1, p2, 0x4

    goto/32 :goto_16

    nop

    :goto_15
    invoke-static {p3, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    goto/32 :goto_1d

    nop

    :goto_16
    const/4 p3, 0x0

    goto/32 :goto_13

    nop

    :goto_17
    invoke-virtual {p1, p3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    goto/32 :goto_2

    nop

    :goto_18
    sub-float/2addr v0, p3

    goto/32 :goto_f

    nop

    :goto_19
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    goto/32 :goto_5

    nop

    :goto_1a
    iget p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->d:F

    goto/32 :goto_18

    nop

    :goto_1b
    iget p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    goto/32 :goto_0

    nop

    :goto_1c
    invoke-static {p3, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    goto/32 :goto_9

    nop

    :goto_1d
    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    :goto_1e
    goto/32 :goto_8

    nop

    :goto_1f
    iget p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->e:F

    goto/32 :goto_b

    nop

    :goto_20
    if-eqz p1, :cond_2

    goto/32 :goto_1e

    :cond_2
    goto/32 :goto_7

    nop

    :goto_21
    if-eqz p1, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_1b

    nop
.end method

.method public a(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView$t;I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView$t;Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->a:Ljava/util/List;

    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)V

    :cond_1
    :goto_0
    return-void
.end method

.method a(Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 10

    goto/32 :goto_19

    nop

    :goto_0
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_3f

    nop

    :goto_1
    cmpl-float v1, v1, v2

    goto/32 :goto_27

    nop

    :goto_2
    int-to-float v1, v1

    goto/32 :goto_0

    nop

    :goto_3
    mul-float/2addr v2, v0

    goto/32 :goto_1

    nop

    :goto_4
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->u:Ljava/util/List;

    goto/32 :goto_25

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_2b

    :cond_0
    goto/32 :goto_33

    nop

    :goto_6
    goto/16 :goto_2b

    :goto_7
    goto/32 :goto_13

    nop

    :goto_8
    invoke-interface {p1}, Ljava/util/List;->clear()V

    goto/32 :goto_6

    nop

    :goto_9
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_a
    sub-int v1, v9, v1

    goto/32 :goto_3d

    nop

    :goto_b
    if-eqz v6, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_4

    nop

    :goto_c
    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_2e

    nop

    :goto_d
    cmpl-float v0, v1, v2

    goto/32 :goto_31

    nop

    :goto_e
    goto/16 :goto_2b

    :goto_f
    goto/32 :goto_1b

    nop

    :goto_10
    if-eqz v0, :cond_2

    goto/32 :goto_2b

    :cond_2
    goto/32 :goto_40

    nop

    :goto_11
    float-to-int v8, v1

    goto/32 :goto_12

    nop

    :goto_12
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->k:F

    goto/32 :goto_39

    nop

    :goto_13
    invoke-virtual {v6}, Landroidx/recyclerview/widget/RecyclerView$t;->getAdapterPosition()I

    move-result v7

    goto/32 :goto_15

    nop

    :goto_14
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->v:Ljava/util/List;

    goto/32 :goto_8

    nop

    :goto_15
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$t;->getAdapterPosition()I

    move-result v5

    goto/32 :goto_28

    nop

    :goto_16
    invoke-virtual {v0}, Landroid/view/ViewGroup;->isLayoutRequested()Z

    move-result v0

    goto/32 :goto_10

    nop

    :goto_17
    add-float/2addr v1, v2

    goto/32 :goto_11

    nop

    :goto_18
    invoke-virtual {v1, p1, v0, v8, v9}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView$t;Ljava/util/List;II)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object v6

    goto/32 :goto_b

    nop

    :goto_19
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_16

    nop

    :goto_1a
    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_9

    nop

    :goto_1b
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    goto/32 :goto_2a

    nop

    :goto_1c
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_3e

    nop

    :goto_1d
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    goto/32 :goto_2

    nop

    :goto_1e
    return-void

    :goto_1f
    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView$t;)F

    move-result v0

    goto/32 :goto_23

    nop

    :goto_20
    int-to-float v2, v2

    goto/32 :goto_3b

    nop

    :goto_21
    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    goto/32 :goto_17

    nop

    :goto_22
    if-nez v1, :cond_3

    goto/32 :goto_f

    :cond_3
    goto/32 :goto_34

    nop

    :goto_23
    iget v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->j:F

    goto/32 :goto_21

    nop

    :goto_24
    add-float/2addr v1, v2

    goto/32 :goto_37

    nop

    :goto_25
    invoke-interface {p1}, Ljava/util/List;->clear()V

    goto/32 :goto_14

    nop

    :goto_26
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_3a

    nop

    :goto_27
    if-ltz v1, :cond_4

    goto/32 :goto_32

    :cond_4
    goto/32 :goto_c

    nop

    :goto_28
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    goto/32 :goto_1c

    nop

    :goto_29
    move-object v4, p1

    goto/32 :goto_35

    nop

    :goto_2a
    invoke-virtual {v1, p1, v0, v8, v9}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView$t;Ljava/util/List;II)Landroidx/recyclerview/widget/RecyclerView$t;

    :goto_2b
    goto/32 :goto_1e

    nop

    :goto_2c
    sub-int v1, v8, v1

    goto/32 :goto_1d

    nop

    :goto_2d
    if-eq v0, v1, :cond_5

    goto/32 :goto_2b

    :cond_5
    goto/32 :goto_42

    nop

    :goto_2e
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    goto/32 :goto_2c

    nop

    :goto_2f
    int-to-float v2, v2

    goto/32 :goto_3

    nop

    :goto_30
    int-to-float v1, v1

    goto/32 :goto_26

    nop

    :goto_31
    if-gez v0, :cond_6

    goto/32 :goto_2b

    :cond_6
    :goto_32
    goto/32 :goto_41

    nop

    :goto_33
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    goto/32 :goto_3c

    nop

    :goto_34
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    goto/32 :goto_18

    nop

    :goto_35
    invoke-virtual/range {v2 .. v9}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;ILandroidx/recyclerview/widget/RecyclerView$t;III)V

    goto/32 :goto_e

    nop

    :goto_36
    const/4 v1, 0x2

    goto/32 :goto_2d

    nop

    :goto_37
    float-to-int v9, v1

    goto/32 :goto_1a

    nop

    :goto_38
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_22

    nop

    :goto_39
    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    goto/32 :goto_24

    nop

    :goto_3a
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    goto/32 :goto_2f

    nop

    :goto_3b
    mul-float/2addr v2, v0

    goto/32 :goto_d

    nop

    :goto_3c
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_29

    nop

    :goto_3d
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    goto/32 :goto_30

    nop

    :goto_3e
    invoke-virtual {v0, v1, p1, v6}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;Landroidx/recyclerview/widget/RecyclerView$t;)Z

    move-result v0

    goto/32 :goto_5

    nop

    :goto_3f
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    goto/32 :goto_20

    nop

    :goto_40
    iget v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->n:I

    goto/32 :goto_36

    nop

    :goto_41
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->b(Landroidx/recyclerview/widget/RecyclerView$t;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_38

    nop

    :goto_42
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    goto/32 :goto_1f

    nop
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 23
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move/from16 v13, p2

    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    if-ne v12, v0, :cond_0

    iget v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->n:I

    if-eq v13, v0, :cond_f

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->D:J

    iget v4, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->n:I

    const/4 v14, 0x1

    invoke-virtual {v11, v12, v14}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView$t;Z)V

    iput v13, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->n:I

    const/4 v15, 0x2

    if-ne v13, v15, :cond_2

    if-eqz v12, :cond_1

    iget-object v0, v12, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    iput-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->x:Landroid/view/View;

    invoke-direct/range {p0 .. p0}, Lcom/xiaomi/misettings/usagestats/home/category/k;->d()V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must pass a ViewHolder when dragging"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    mul-int/lit8 v0, v13, 0x8

    const/16 v10, 0x8

    add-int/2addr v0, v10

    shl-int v0, v14, v0

    add-int/lit8 v16, v0, -0x1

    iget-object v9, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    const/4 v8, 0x0

    if-eqz v9, :cond_a

    iget-object v0, v9, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_9

    if-ne v4, v15, :cond_3

    move v7, v8

    goto :goto_1

    :cond_3
    invoke-direct {v11, v9}, Lcom/xiaomi/misettings/usagestats/home/category/k;->c(Landroidx/recyclerview/widget/RecyclerView$t;)I

    move-result v0

    move v7, v0

    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/xiaomi/misettings/usagestats/home/category/k;->f()V

    const/4 v0, 0x4

    const/4 v1, 0x0

    if-eq v7, v14, :cond_5

    if-eq v7, v15, :cond_5

    if-eq v7, v0, :cond_4

    if-eq v7, v10, :cond_4

    const/16 v2, 0x10

    if-eq v7, v2, :cond_4

    const/16 v2, 0x20

    if-eq v7, v2, :cond_4

    move/from16 v17, v1

    move/from16 v18, v17

    goto :goto_2

    :cond_4
    iget v2, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    iget-object v3, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    move/from16 v18, v1

    move/from16 v17, v2

    goto :goto_2

    :cond_5
    iget v2, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    iget-object v3, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    move/from16 v17, v1

    move/from16 v18, v2

    :goto_2
    if-ne v4, v15, :cond_6

    move v6, v10

    goto :goto_3

    :cond_6
    if-lez v7, :cond_7

    move v6, v15

    goto :goto_3

    :cond_7
    move v6, v0

    :goto_3
    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->b:[F

    invoke-direct {v11, v0}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a([F)V

    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->b:[F

    aget v19, v0, v8

    aget v20, v0, v14

    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v1, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-virtual {v0, v1, v13}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView$t;I)V

    new-instance v5, Lcom/xiaomi/misettings/usagestats/home/category/f;

    move-object v0, v5

    move-object/from16 v1, p0

    move-object v2, v9

    move v3, v6

    move-object v14, v5

    move/from16 v5, v19

    move v15, v6

    move/from16 v6, v20

    move/from16 v21, v7

    move/from16 v7, v17

    move/from16 v8, v18

    move-object/from16 v22, v9

    move/from16 v9, v21

    move/from16 v21, v10

    move-object/from16 v10, v22

    invoke-direct/range {v0 .. v10}, Lcom/xiaomi/misettings/usagestats/home/category/f;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/k;Landroidx/recyclerview/widget/RecyclerView$t;IIFFFFILandroidx/recyclerview/widget/RecyclerView$t;)V

    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v1, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    sub-float v2, v17, v19

    sub-float v3, v18, v20

    invoke-virtual {v0, v1, v15, v2, v3}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView;IFF)J

    move-result-wide v0

    invoke-virtual {v14, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->a(J)V

    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-boolean v0, v0, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->d:Z

    if-eqz v0, :cond_8

    invoke-virtual {v14}, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->b()V

    :cond_8
    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v14}, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->c()V

    const/4 v8, 0x1

    goto :goto_4

    :cond_9
    move-object v0, v9

    move/from16 v21, v10

    iget-object v1, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v11, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->c(Landroid/view/View;)V

    iget-object v1, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v2, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v2, v0}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)V

    const/4 v8, 0x0

    :goto_4
    const/4 v0, 0x0

    iput-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    goto :goto_5

    :cond_a
    move/from16 v21, v10

    const/4 v8, 0x0

    :goto_5
    if-eqz v12, :cond_b

    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v1, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1, v12}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)I

    move-result v0

    and-int v0, v0, v16

    iget v1, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->n:I

    mul-int/lit8 v1, v1, 0x8

    shr-int/2addr v0, v1

    iput v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->o:I

    iget-object v0, v12, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    iput v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->j:F

    iget-object v0, v12, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    iput v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->k:F

    iput-object v12, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    const/4 v0, 0x2

    if-ne v13, v0, :cond_b

    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    goto :goto_6

    :cond_b
    const/4 v1, 0x0

    :goto_6
    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v2, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    if-eqz v2, :cond_c

    const/4 v1, 0x1

    :cond_c
    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_d
    if-nez v8, :cond_e

    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$g;->A()V

    :cond_e
    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v1, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    iget v2, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->n:I

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(Landroidx/recyclerview/widget/RecyclerView$t;I)V

    iget-object v0, v11, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    :cond_f
    return-void
.end method

.method a(Landroidx/recyclerview/widget/RecyclerView$t;Z)V
    .locals 3

    goto/32 :goto_17

    nop

    :goto_0
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_18

    nop

    :goto_1
    iget-object v2, v1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_16

    nop

    :goto_2
    iput-boolean p1, v1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->l:Z

    goto/32 :goto_15

    nop

    :goto_3
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_4

    nop

    :goto_4
    goto :goto_a

    :goto_5
    goto/32 :goto_12

    nop

    :goto_6
    iget-boolean p1, v1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->l:Z

    goto/32 :goto_13

    nop

    :goto_7
    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->a()V

    :goto_8
    goto/32 :goto_11

    nop

    :goto_9
    add-int/lit8 v0, v0, -0x1

    :goto_a
    goto/32 :goto_b

    nop

    :goto_b
    if-gez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_10

    nop

    :goto_c
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_9

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_3

    nop

    :goto_f
    if-eqz p1, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_7

    nop

    :goto_10
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    goto/32 :goto_0

    nop

    :goto_11
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    goto/32 :goto_14

    nop

    :goto_12
    return-void

    :goto_13
    or-int/2addr p1, p2

    goto/32 :goto_2

    nop

    :goto_14
    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_15
    iget-boolean p1, v1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->m:Z

    goto/32 :goto_f

    nop

    :goto_16
    if-eq v2, p1, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_6

    nop

    :goto_17
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    goto/32 :goto_c

    nop

    :goto_18
    check-cast v1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;

    goto/32 :goto_1

    nop
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    if-eq v0, p1, :cond_1

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/k;->e()V

    :cond_0
    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0701c7

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->f:F

    const v0, 0x7f0701c6

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->g:F

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/category/k;->g()V

    :cond_1
    return-void
.end method

.method a(Lcom/xiaomi/misettings/usagestats/home/category/k$c;I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_4

    nop

    :goto_1
    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/category/g;

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_1

    nop

    :goto_3
    invoke-direct {v1, p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/g;-><init>(Lcom/xiaomi/misettings/usagestats/home/category/k;Lcom/xiaomi/misettings/usagestats/home/category/k$c;I)V

    goto/32 :goto_0

    nop

    :goto_4
    return-void
.end method

.method a()Z
    .locals 4

    goto/32 :goto_5

    nop

    :goto_0
    goto :goto_3

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    move v2, v1

    :goto_3
    goto/32 :goto_b

    nop

    :goto_4
    check-cast v3, Lcom/xiaomi/misettings/usagestats/home/category/k$c;

    goto/32 :goto_f

    nop

    :goto_5
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    goto/32 :goto_a

    nop

    :goto_6
    return v0

    :goto_7
    goto/32 :goto_c

    nop

    :goto_8
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_9
    return v1

    :goto_a
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_d

    nop

    :goto_b
    if-lt v2, v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_10

    nop

    :goto_c
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_0

    nop

    :goto_d
    const/4 v1, 0x0

    goto/32 :goto_2

    nop

    :goto_e
    if-eqz v3, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_8

    nop

    :goto_f
    iget-boolean v3, v3, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->m:Z

    goto/32 :goto_e

    nop

    :goto_10
    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    goto/32 :goto_11

    nop

    :goto_11
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_4

    nop
.end method

.method b(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 5

    goto/32 :goto_c

    nop

    :goto_0
    if-nez v2, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_17

    nop

    :goto_1
    if-gez v1, :cond_1

    goto/32 :goto_1e

    :cond_1
    goto/32 :goto_f

    nop

    :goto_2
    check-cast v2, Lcom/xiaomi/misettings/usagestats/home/category/k$c;

    goto/32 :goto_11

    nop

    :goto_3
    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    goto/32 :goto_10

    nop

    :goto_4
    invoke-static {v3, v0, p1, v4, v2}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroid/view/View;FFFF)Z

    move-result v2

    goto/32 :goto_0

    nop

    :goto_5
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_1b

    nop

    :goto_6
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_19

    nop

    :goto_7
    if-nez v1, :cond_2

    goto/32 :goto_15

    :cond_2
    goto/32 :goto_e

    nop

    :goto_8
    iget v4, v2, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->j:F

    goto/32 :goto_22

    nop

    :goto_9
    invoke-static {v1, v0, p1, v2, v3}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroid/view/View;FFFF)Z

    move-result v2

    goto/32 :goto_1c

    nop

    :goto_a
    add-float/2addr v3, v4

    goto/32 :goto_9

    nop

    :goto_b
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_2

    nop

    :goto_c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    goto/32 :goto_d

    nop

    :goto_d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    goto/32 :goto_20

    nop

    :goto_e
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_21

    nop

    :goto_f
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    goto/32 :goto_b

    nop

    :goto_10
    add-float/2addr v2, v3

    goto/32 :goto_24

    nop

    :goto_11
    iget-object v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_13

    nop

    :goto_12
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_1d

    nop

    :goto_13
    iget-object v3, v3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_14
    return-object v1

    :goto_15
    goto/32 :goto_16

    nop

    :goto_16
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    goto/32 :goto_6

    nop

    :goto_17
    return-object v3

    :goto_18
    goto/32 :goto_12

    nop

    :goto_19
    add-int/lit8 v1, v1, -0x1

    :goto_1a
    goto/32 :goto_1

    nop

    :goto_1b
    invoke-virtual {v1, v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->findChildViewUnder(FF)Landroid/view/View;

    move-result-object p1

    goto/32 :goto_1f

    nop

    :goto_1c
    if-nez v2, :cond_3

    goto/32 :goto_15

    :cond_3
    goto/32 :goto_14

    nop

    :goto_1d
    goto :goto_1a

    :goto_1e
    goto/32 :goto_5

    nop

    :goto_1f
    return-object p1

    :goto_20
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_7

    nop

    :goto_21
    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->j:F

    goto/32 :goto_3

    nop

    :goto_22
    iget v2, v2, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->k:F

    goto/32 :goto_4

    nop

    :goto_23
    iget v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    goto/32 :goto_a

    nop

    :goto_24
    iget v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->k:F

    goto/32 :goto_23

    nop
.end method

.method b()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    goto/32 :goto_4

    nop

    :goto_1
    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    goto/32 :goto_3

    nop

    :goto_2
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_3
    return-void

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    :goto_6
    goto/32 :goto_2

    nop
.end method

.method public b(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public c(Landroid/view/MotionEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->A:Lcom/xiaomi/misettings/usagestats/home/category/k$b;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/home/category/k$b;->a(Lcom/xiaomi/misettings/usagestats/home/category/k$b;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->b(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$t;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v2, v0}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->d(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object p1, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->performLongClick()Z

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    if-ne v1, v2, :cond_1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result p1

    iput v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->d:F

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->e:F

    const/4 p1, 0x0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->c()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView$t;I)V

    :cond_1
    iget-object p1, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->performLongClick()Z

    :cond_2
    return-void
.end method

.method c(Landroid/view/View;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_4

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->x:Landroid/view/View;

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setChildDrawingOrderCallback(Landroidx/recyclerview/widget/RecyclerView$d;)V

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->x:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_7
    if-eq p1, v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_9

    nop

    :goto_8
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->w:Landroidx/recyclerview/widget/RecyclerView$d;

    goto/32 :goto_2

    nop

    :goto_9
    const/4 p1, 0x0

    goto/32 :goto_6

    nop
.end method

.method c()Z
    .locals 16

    goto/32 :goto_e

    nop

    :goto_0
    if-gtz v9, :cond_0

    goto/32 :goto_71

    :cond_0
    goto/32 :goto_91

    nop

    :goto_1
    add-int/2addr v1, v9

    goto/32 :goto_63

    nop

    :goto_2
    move v1, v2

    :goto_3
    goto/32 :goto_59

    nop

    :goto_4
    iget-object v10, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_7a

    nop

    :goto_5
    if-ltz v12, :cond_1

    goto/32 :goto_17

    :cond_1
    goto/32 :goto_4a

    nop

    :goto_6
    if-nez v9, :cond_2

    goto/32 :goto_4d

    :cond_2
    goto/32 :goto_5d

    nop

    :goto_7
    iget-object v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    goto/32 :goto_24

    nop

    :goto_8
    move-wide v14, v7

    goto/32 :goto_58

    nop

    :goto_9
    sub-int/2addr v9, v11

    goto/32 :goto_1e

    nop

    :goto_a
    sub-int v9, v1, v9

    goto/32 :goto_1d

    nop

    :goto_b
    iget-object v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->C:Landroid/graphics/Rect;

    goto/32 :goto_f

    nop

    :goto_c
    cmpg-float v12, v12, v10

    goto/32 :goto_5

    nop

    :goto_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    goto/32 :goto_88

    nop

    :goto_e
    move-object/from16 v0, p0

    goto/32 :goto_7f

    nop

    :goto_f
    if-eqz v9, :cond_3

    goto/32 :goto_3c

    :cond_3
    goto/32 :goto_78

    nop

    :goto_10
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$g;->b()Z

    move-result v1

    goto/32 :goto_52

    nop

    :goto_11
    iget-object v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->C:Landroid/graphics/Rect;

    goto/32 :goto_51

    nop

    :goto_12
    iget-wide v7, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->D:J

    goto/32 :goto_60

    nop

    :goto_13
    if-eqz v12, :cond_4

    goto/32 :goto_33

    :cond_4
    goto/32 :goto_6f

    nop

    :goto_14
    iget-object v11, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->C:Landroid/graphics/Rect;

    goto/32 :goto_2e

    nop

    :goto_15
    const/4 v10, 0x0

    goto/32 :goto_6

    nop

    :goto_16
    goto/16 :goto_7e

    :goto_17
    goto/32 :goto_26

    nop

    :goto_18
    const-wide/high16 v3, -0x8000000000000000L

    goto/32 :goto_47

    nop

    :goto_19
    move v1, v7

    goto/32 :goto_89

    nop

    :goto_1a
    iget-object v9, v9, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_85

    nop

    :goto_1b
    if-eqz v2, :cond_5

    goto/32 :goto_93

    :cond_5
    goto/32 :goto_92

    nop

    :goto_1c
    invoke-virtual {v1, v9, v10}, Landroidx/recyclerview/widget/RecyclerView$g;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    goto/32 :goto_77

    nop

    :goto_1d
    iget-object v11, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_40

    nop

    :goto_1e
    iget v11, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    goto/32 :goto_5a

    nop

    :goto_1f
    goto/16 :goto_76

    :goto_20
    goto/32 :goto_75

    nop

    :goto_21
    invoke-virtual {v9}, Landroid/view/ViewGroup;->getHeight()I

    move-result v9

    goto/32 :goto_4

    nop

    :goto_22
    iget v9, v9, Landroid/graphics/Rect;->top:I

    goto/32 :goto_a

    nop

    :goto_23
    add-int/2addr v9, v11

    goto/32 :goto_5b

    nop

    :goto_24
    iget-object v10, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_64

    nop

    :goto_25
    float-to-int v9, v9

    goto/32 :goto_14

    nop

    :goto_26
    iget v11, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    goto/32 :goto_8c

    nop

    :goto_27
    iget v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    goto/32 :goto_7b

    nop

    :goto_28
    move v14, v12

    goto/32 :goto_62

    nop

    :goto_29
    add-int/2addr v9, v11

    goto/32 :goto_84

    nop

    :goto_2a
    if-ltz v9, :cond_6

    goto/32 :goto_74

    :cond_6
    goto/32 :goto_4b

    nop

    :goto_2b
    iget-object v10, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_45

    nop

    :goto_2c
    if-gtz v9, :cond_7

    goto/32 :goto_4d

    :cond_7
    goto/32 :goto_57

    nop

    :goto_2d
    iput-wide v3, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->D:J

    goto/32 :goto_32

    nop

    :goto_2e
    iget v11, v11, Landroid/graphics/Rect;->left:I

    goto/32 :goto_50

    nop

    :goto_2f
    iget-object v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->C:Landroid/graphics/Rect;

    goto/32 :goto_22

    nop

    :goto_30
    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->k:F

    goto/32 :goto_3d

    nop

    :goto_31
    invoke-virtual {v11}, Landroid/view/View;->getWidth()I

    move-result v11

    goto/32 :goto_3e

    nop

    :goto_32
    return v2

    :goto_33
    goto/32 :goto_12

    nop

    :goto_34
    sub-int/2addr v9, v10

    goto/32 :goto_3a

    nop

    :goto_35
    iget-object v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    goto/32 :goto_2b

    nop

    :goto_36
    sub-int/2addr v11, v12

    goto/32 :goto_8b

    nop

    :goto_37
    move v12, v11

    goto/32 :goto_16

    nop

    :goto_38
    add-int/2addr v1, v9

    goto/32 :goto_11

    nop

    :goto_39
    iput-wide v3, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->D:J

    goto/32 :goto_69

    nop

    :goto_3a
    sub-int/2addr v1, v9

    goto/32 :goto_46

    nop

    :goto_3b
    iput-object v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->C:Landroid/graphics/Rect;

    :goto_3c
    goto/32 :goto_5c

    nop

    :goto_3d
    iget v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->i:F

    goto/32 :goto_79

    nop

    :goto_3e
    iget-object v13, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_41

    nop

    :goto_3f
    const/4 v1, 0x1

    goto/32 :goto_6b

    nop

    :goto_40
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v11

    goto/32 :goto_9

    nop

    :goto_41
    invoke-virtual {v13}, Landroid/view/ViewGroup;->getWidth()I

    move-result v13

    goto/32 :goto_54

    nop

    :goto_42
    iget-object v12, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_6e

    nop

    :goto_43
    iget-object v2, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_44

    nop

    :goto_44
    invoke-virtual {v2, v12, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollBy(II)V

    goto/32 :goto_3f

    nop

    :goto_45
    iget-object v11, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_61

    nop

    :goto_46
    if-gtz v1, :cond_8

    goto/32 :goto_71

    :cond_8
    goto/32 :goto_70

    nop

    :goto_47
    if-eqz v1, :cond_9

    goto/32 :goto_6a

    :cond_9
    goto/32 :goto_39

    nop

    :goto_48
    move v12, v14

    :goto_49
    goto/32 :goto_13

    nop

    :goto_4a
    if-ltz v11, :cond_a

    goto/32 :goto_17

    :cond_a
    goto/32 :goto_37

    nop

    :goto_4b
    move v1, v9

    goto/32 :goto_73

    nop

    :goto_4c
    goto/16 :goto_7e

    :goto_4d
    goto/32 :goto_7d

    nop

    :goto_4e
    invoke-virtual/range {v9 .. v15}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView;IIIJ)I

    move-result v12

    :goto_4f
    goto/32 :goto_28

    nop

    :goto_50
    sub-int v11, v9, v11

    goto/32 :goto_81

    nop

    :goto_51
    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_1

    nop

    :goto_52
    if-nez v1, :cond_b

    goto/32 :goto_71

    :cond_b
    goto/32 :goto_30

    nop

    :goto_53
    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v11

    goto/32 :goto_98

    nop

    :goto_54
    move-wide v14, v7

    goto/32 :goto_4e

    nop

    :goto_55
    iget-object v9, v9, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_67

    nop

    :goto_56
    add-float/2addr v9, v11

    goto/32 :goto_25

    nop

    :goto_57
    move v12, v9

    goto/32 :goto_4c

    nop

    :goto_58
    invoke-virtual/range {v9 .. v15}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView;IIIJ)I

    move-result v7

    goto/32 :goto_66

    nop

    :goto_59
    if-nez v12, :cond_c

    goto/32 :goto_4f

    :cond_c
    goto/32 :goto_7

    nop

    :goto_5a
    cmpg-float v11, v11, v10

    goto/32 :goto_83

    nop

    :goto_5b
    iget-object v11, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_6d

    nop

    :goto_5c
    iget-object v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_55

    nop

    :goto_5d
    iget v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->j:F

    goto/32 :goto_8e

    nop

    :goto_5e
    const-wide/16 v7, 0x0

    goto/32 :goto_1f

    nop

    :goto_5f
    iget v12, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    goto/32 :goto_c

    nop

    :goto_60
    cmp-long v2, v7, v3

    goto/32 :goto_1b

    nop

    :goto_61
    iget-object v11, v11, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_53

    nop

    :goto_62
    if-nez v1, :cond_d

    goto/32 :goto_8a

    :cond_d
    goto/32 :goto_35

    nop

    :goto_63
    iget-object v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_21

    nop

    :goto_64
    iget-object v11, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_86

    nop

    :goto_65
    if-eqz v1, :cond_e

    goto/32 :goto_20

    :cond_e
    goto/32 :goto_5e

    nop

    :goto_66
    move v12, v1

    goto/32 :goto_19

    nop

    :goto_67
    iget-object v10, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->C:Landroid/graphics/Rect;

    goto/32 :goto_1c

    nop

    :goto_68
    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    goto/32 :goto_3b

    nop

    :goto_69
    return v2

    :goto_6a
    goto/32 :goto_d

    nop

    :goto_6b
    return v1

    :goto_6c
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_6d
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v11

    goto/32 :goto_42

    nop

    :goto_6e
    invoke-virtual {v12}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v12

    goto/32 :goto_36

    nop

    :goto_6f
    if-eqz v1, :cond_f

    goto/32 :goto_33

    :cond_f
    goto/32 :goto_2d

    nop

    :goto_70
    goto/16 :goto_3

    :goto_71
    goto/32 :goto_2

    nop

    :goto_72
    sub-int/2addr v11, v12

    goto/32 :goto_5f

    nop

    :goto_73
    goto/16 :goto_3

    :goto_74
    goto/32 :goto_27

    nop

    :goto_75
    sub-long v7, v5, v7

    :goto_76
    goto/32 :goto_9a

    nop

    :goto_77
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$g;->a()Z

    move-result v9

    goto/32 :goto_15

    nop

    :goto_78
    new-instance v9, Landroid/graphics/Rect;

    goto/32 :goto_68

    nop

    :goto_79
    add-float/2addr v1, v9

    goto/32 :goto_94

    nop

    :goto_7a
    invoke-virtual {v10}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v10

    goto/32 :goto_34

    nop

    :goto_7b
    cmpl-float v9, v9, v10

    goto/32 :goto_0

    nop

    :goto_7c
    invoke-virtual {v12}, Landroid/view/ViewGroup;->getHeight()I

    move-result v13

    goto/32 :goto_97

    nop

    :goto_7d
    move v12, v2

    :goto_7e
    goto/32 :goto_10

    nop

    :goto_7f
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_96

    nop

    :goto_80
    iget-object v11, v11, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_90

    nop

    :goto_81
    iget-object v12, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_87

    nop

    :goto_82
    iget-object v11, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_80

    nop

    :goto_83
    if-ltz v11, :cond_10

    goto/32 :goto_74

    :cond_10
    goto/32 :goto_2a

    nop

    :goto_84
    iget-object v11, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->C:Landroid/graphics/Rect;

    goto/32 :goto_99

    nop

    :goto_85
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    goto/32 :goto_38

    nop

    :goto_86
    iget-object v11, v11, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_31

    nop

    :goto_87
    invoke-virtual {v12}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v12

    goto/32 :goto_72

    nop

    :goto_88
    iget-wide v7, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->D:J

    goto/32 :goto_95

    nop

    :goto_89
    goto/16 :goto_49

    :goto_8a
    goto/32 :goto_48

    nop

    :goto_8b
    sub-int/2addr v9, v11

    goto/32 :goto_2c

    nop

    :goto_8c
    cmpl-float v11, v11, v10

    goto/32 :goto_8f

    nop

    :goto_8d
    move v1, v14

    goto/32 :goto_8

    nop

    :goto_8e
    iget v11, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->h:F

    goto/32 :goto_56

    nop

    :goto_8f
    if-gtz v11, :cond_11

    goto/32 :goto_4d

    :cond_11
    goto/32 :goto_82

    nop

    :goto_90
    invoke-virtual {v11}, Landroid/view/View;->getWidth()I

    move-result v11

    goto/32 :goto_29

    nop

    :goto_91
    iget-object v9, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_1a

    nop

    :goto_92
    iput-wide v5, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->D:J

    :goto_93
    goto/32 :goto_43

    nop

    :goto_94
    float-to-int v1, v1

    goto/32 :goto_2f

    nop

    :goto_95
    cmp-long v1, v7, v3

    goto/32 :goto_65

    nop

    :goto_96
    const/4 v2, 0x0

    goto/32 :goto_18

    nop

    :goto_97
    move v12, v1

    goto/32 :goto_8d

    nop

    :goto_98
    iget-object v12, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_7c

    nop

    :goto_99
    iget v11, v11, Landroid/graphics/Rect;->right:I

    goto/32 :goto_23

    nop

    :goto_9a
    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    goto/32 :goto_6c

    nop
.end method

.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$q;)V
    .locals 0

    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$q;)V
    .locals 9

    const/4 p3, -0x1

    iput p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->y:I

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->b:[F

    invoke-direct {p0, p3}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a([F)V

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->b:[F

    const/4 v0, 0x0

    aget v0, p3, v0

    const/4 v1, 0x1

    aget p3, p3, v1

    move v8, p3

    move v7, v0

    goto :goto_0

    :cond_0
    move v7, v0

    move v8, v7

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->n:I

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v1 .. v8}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;Ljava/util/List;IFF)V

    return-void
.end method

.method public onDrawOver(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$q;)V
    .locals 9

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->b:[F

    invoke-direct {p0, p3}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a([F)V

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->b:[F

    const/4 v0, 0x0

    aget v0, p3, v0

    const/4 v1, 0x1

    aget p3, p3, v1

    move v8, p3

    move v7, v0

    goto :goto_0

    :cond_0
    move v7, v0

    move v8, v7

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->p:Ljava/util/List;

    iget v6, p0, Lcom/xiaomi/misettings/usagestats/home/category/k;->n:I

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v1 .. v8}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->b(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;Ljava/util/List;IFF)V

    return-void
.end method
