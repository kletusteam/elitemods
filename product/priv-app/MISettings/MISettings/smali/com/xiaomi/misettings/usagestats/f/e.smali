.class public Lcom/xiaomi/misettings/usagestats/f/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable<",
        "Lcom/xiaomi/misettings/usagestats/f/e;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private d:Lcom/xiaomi/misettings/usagestats/f/j;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/e;->b:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(JLcom/xiaomi/misettings/usagestats/f/j;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/e;->b:Ljava/lang/Long;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/e;->b:Ljava/lang/Long;

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/f/e;->d:Lcom/xiaomi/misettings/usagestats/f/j;

    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/misettings/usagestats/f/e;)I
    .locals 1

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/f/e;->b:Ljava/lang/Long;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/e;->b:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result p1

    return p1
.end method

.method public a()Lcom/xiaomi/misettings/usagestats/f/j;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/e;->d:Lcom/xiaomi/misettings/usagestats/f/j;

    return-object v0
.end method

.method public a(J)V
    .locals 0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/e;->b:Ljava/lang/Long;

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/f/j;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/e;->d:Lcom/xiaomi/misettings/usagestats/f/j;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/e;->c:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/f/e;->a:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/e;->a:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/f/e;->a(Lcom/xiaomi/misettings/usagestats/f/e;)I

    move-result p1

    return p1
.end method

.method public d()J
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/f/e;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method
