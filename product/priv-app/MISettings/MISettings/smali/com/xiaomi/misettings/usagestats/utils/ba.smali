.class public Lcom/xiaomi/misettings/usagestats/utils/ba;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/utils/ba$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/app/usage/UsageEvents;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/usage/UsageEvents;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "LR-UsageEventUtil"

    if-eqz p0, :cond_5

    if-nez p1, :cond_0

    goto/16 :goto_1

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->clear()V

    new-instance v1, Lcom/xiaomi/misettings/usagestats/utils/ba$a;

    invoke-direct {v1}, Lcom/xiaomi/misettings/usagestats/utils/ba$a;-><init>()V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Landroid/app/usage/UsageEvents$Event;

    invoke-direct {v2}, Landroid/app/usage/UsageEvents$Event;-><init>()V

    invoke-virtual {p0, v2}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/utils/ba;->b(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v4

    sput v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    invoke-virtual {v2}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J

    move-result-wide v4

    sput-wide v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    if-nez v3, :cond_2

    invoke-virtual {v2}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->a(Landroid/app/usage/UsageEvents$Event;)V

    goto :goto_0

    :cond_3
    sget p0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    invoke-virtual {v1, p0}, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->a(I)Landroid/app/usage/UsageEvents$Event;

    move-result-object p0

    if-eqz p0, :cond_4

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result p1

    sput p1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J

    move-result-wide p0

    sput-wide p0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    :cond_4
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "getAllPackages: lastEvent="

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget p1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ",lastPkg="

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-wide v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    invoke-virtual {p0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_5
    :goto_1
    const-string p0, "getAllPackages()......return since invalid params."

    invoke-static {v0, p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/app/usage/UsageEvents$Event;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v0

    const/16 v1, 0x17

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result p0

    const/16 v0, 0x18

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static a(Landroid/app/usage/UsageEvents;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Landroid/app/usage/UsageEvents$Event;

    invoke-direct {v1}, Landroid/app/usage/UsageEvents$Event;-><init>()V

    invoke-virtual {p0, v1}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/ba;->a(Landroid/app/usage/UsageEvents$Event;)Z

    move-result p0

    return p0

    :cond_1
    return v0

    :cond_2
    :goto_0
    const-string p0, "LR-UsageEventUtil"

    const-string p1, "checkStopped()......return since invalid params."

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method private static a(Ljava/lang/String;Landroid/app/usage/UsageEvents$Event;)Z
    .locals 2

    const-string v0, "com.android.settings"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    sget-object p0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->f:Ljava/util/List;

    invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0

    :cond_2
    invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p0

    return p0
.end method

.method public static a(Landroid/app/usage/UsageEvents;)[Z
    .locals 10

    const-string v0, "LR-UsageEventUtil"

    const/4 v1, 0x2

    if-nez p0, :cond_0

    const-string p0, "getAllPackages()......return since invalid params."

    invoke-static {v0, p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-array p0, v1, [Z

    fill-array-data p0, :array_0

    return-object p0

    :cond_0
    invoke-virtual {p0}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v2

    if-nez v2, :cond_1

    new-array p0, v1, [Z

    fill-array-data p0, :array_1

    return-object p0

    :cond_1
    const/4 v2, 0x0

    const/4 v3, -0x1

    new-instance v4, Lcom/xiaomi/misettings/usagestats/utils/ba$a;

    invoke-direct {v4}, Lcom/xiaomi/misettings/usagestats/utils/ba$a;-><init>()V

    const/4 v5, 0x0

    move v6, v5

    :cond_2
    :goto_0
    invoke-virtual {p0}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v7

    const/4 v8, 0x1

    if-eqz v7, :cond_3

    new-instance v7, Landroid/app/usage/UsageEvents$Event;

    invoke-direct {v7}, Landroid/app/usage/UsageEvents$Event;-><init>()V

    invoke-virtual {p0, v7}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-static {v7}, Lcom/xiaomi/misettings/usagestats/utils/ba;->c(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v7}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v3

    invoke-virtual {v4, v7}, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->a(Landroid/app/usage/UsageEvents$Event;)V

    move v6, v8

    goto :goto_0

    :cond_3
    if-ne v3, v8, :cond_4

    move p0, v8

    goto :goto_1

    :cond_4
    move p0, v5

    :goto_1
    invoke-virtual {v4, v3}, Lcom/xiaomi/misettings/usagestats/utils/ba$a;->a(I)Landroid/app/usage/UsageEvents$Event;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move p0, v8

    :cond_5
    if-eqz p0, :cond_6

    sput-object v2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "findPackages: lastEvent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v3, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ",lastPkg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "isFind"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, v1, [Z

    aput-boolean v6, v0, v5

    aput-boolean p0, v0, v8

    return-object v0

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x0t
        0x0t
    .end array-data
.end method

.method public static b(Landroid/app/usage/UsageEvents;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/usage/UsageEvents;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Landroid/app/usage/UsageEvents$Event;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_2

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Landroid/app/usage/UsageEvents$Event;

    invoke-direct {v1}, Landroid/app/usage/UsageEvents$Event;-><init>()V

    invoke-virtual {p0, v1}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/ba;->b(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1, v1}, Lcom/xiaomi/misettings/usagestats/utils/ba;->a(Ljava/lang/String;Landroid/app/usage/UsageEvents$Event;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0

    :cond_2
    :goto_1
    const-string p0, "LR-UsageEventUtil"

    const-string p1, "filterEventByPackage()....invalid params!"

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Landroid/app/usage/UsageEvents$Event;)Z
    .locals 3

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result p0

    const/16 v0, 0x17

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public static c(Landroid/app/usage/UsageEvents$Event;)Z
    .locals 3

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result p0

    const/16 v0, 0x17

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public static d(Landroid/app/usage/UsageEvents$Event;)Z
    .locals 1

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
