.class public Lcom/xiaomi/misettings/usagestats/adapter/d;
.super Landroidx/recyclerview/widget/RecyclerView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/adapter/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/xiaomi/misettings/usagestats/adapter/d$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field public b:Lcom/xiaomi/misettings/usagestats/f/g;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/content/Context;

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    const-string v0, "AppRVAdapter"

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->a:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->c:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->d:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->e:Ljava/util/List;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->f:Ljava/util/HashMap;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->g:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->i:Ljava/util/List;

    const-string v1, "AppRVAdapter: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/a/a/y;->m:Lcom/xiaomi/misettings/usagestats/f/g;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->b:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->i:Ljava/util/List;

    return-void
.end method

.method private a(I)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sget-boolean v1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    const-string v2, "isWeek"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "packageName"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    const-string v2, "hasTime"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->b:Lcom/xiaomi/misettings/usagestats/f/g;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v1

    iget-wide v1, v1, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    const-string v3, "dayBeginTime"

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->b:Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Lcom/xiaomi/misettings/usagestats/f/g;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    const-string v1, "usageList"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/adapter/d;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/adapter/d;->b(I)V

    return-void
.end method

.method private b(I)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/f/e;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "isWeek"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object p1

    const-string v1, "packageName"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object p1, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->c:Lcom/xiaomi/misettings/usagestats/weeklyreport/a/a;

    const-string v1, "weekInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/adapter/d;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/adapter/d;->a(I)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->i:Ljava/util/List;

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/adapter/d$a;I)V
    .locals 9
    .param p1    # Lcom/xiaomi/misettings/usagestats/adapter/d$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onBindViewHolder"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->a(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppRVAdapter"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    const v1, 0x7f1303a9

    const-wide/16 v2, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->e:Ljava/util/List;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/c/b$a;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->i:Ljava/util/List;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->i:Ljava/util/List;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->e()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v5

    :goto_0
    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-static {v6}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/D;

    move-result-object v6

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->b(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->c(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->c()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->d(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/view/View;

    move-result-object v6

    if-eqz v0, :cond_2

    move v4, v5

    :cond_2
    invoke-virtual {v6, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->b(Z)V

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->b()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->e(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->b()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_3
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->e(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/TextView;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_4
    :goto_1
    return-void

    :cond_5
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_6

    goto/16 :goto_3

    :cond_6
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/e;

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->i:Ljava/util/List;

    invoke-static {v6}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/util/Collection;)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->i:Ljava/util/List;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    goto :goto_2

    :cond_7
    move v6, v5

    :goto_2
    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-static {v7}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/D;

    move-result-object v7

    iget-object v8, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->g:Ljava/util/List;

    invoke-interface {v8, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/f/a;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/f/a;->a()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->b(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v7, p2, v8}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->c(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/TextView;

    move-result-object p2

    iget-object v7, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->d(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/view/View;

    move-result-object p2

    if-eqz v6, :cond_8

    move v4, v5

    :cond_8
    invoke-virtual {p2, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v6}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->b(Z)V

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v4

    cmp-long p2, v4, v2

    if-eqz p2, :cond_9

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->e(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/TextView;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v0

    invoke-static {p2, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_9
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->e(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/TextView;

    move-result-object p1

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    :goto_3
    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/adapter/d$a;ILjava/util/List;)V
    .locals 1
    .param p1    # Lcom/xiaomi/misettings/usagestats/adapter/d$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/misettings/usagestats/adapter/d$a;",
            "I",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/adapter/d;->a(Lcom/xiaomi/misettings/usagestats/adapter/d$a;I)V

    goto :goto_1

    :cond_0
    sget-boolean p3, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    if-eqz p3, :cond_3

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->e:Ljava/util/List;

    invoke-static {p3}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/util/Collection;)Z

    move-result p3

    if-nez p3, :cond_2

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->e:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    if-lt p2, p3, :cond_1

    goto :goto_0

    :cond_1
    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-static {p3}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/D;

    move-result-object p3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/c/b$a;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/c/b$a;->e()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->b(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p3, p2, p1}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    goto :goto_1

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->c:Ljava/util/List;

    invoke-static {p3}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/util/Collection;)Z

    move-result p3

    if-nez p3, :cond_5

    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->c:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    if-lt p2, p3, :cond_4

    goto :goto_1

    :cond_4
    iget-object p3, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->h:Landroid/content/Context;

    invoke-static {p3}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/D;

    move-result-object p3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->g:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/misettings/usagestats/f/a;

    invoke-virtual {p2}, Lcom/xiaomi/misettings/usagestats/f/a;->a()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->b(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p3, p2, p1}, Lcom/xiaomi/misettings/usagestats/utils/D;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    :cond_5
    :goto_1
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->f:Ljava/util/HashMap;

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->c:Ljava/util/List;

    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/c/b$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->e:Ljava/util/List;

    return-void
.end method

.method public c(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->g:Ljava/util/List;

    return-void
.end method

.method public d(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->d:Ljava/util/List;

    return-void
.end method

.method public getItemCount()I
    .locals 2

    sget-boolean v0, Lcom/xiaomi/misettings/usagestats/ui/NewAppCategoryListActivity;->a:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->e:Ljava/util/List;

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/adapter/d;->c:Ljava/util/List;

    if-nez v0, :cond_2

    return v1

    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->getItemViewType(I)I

    move-result p1

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/xiaomi/misettings/usagestats/adapter/d$a;

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/adapter/d;->a(Lcom/xiaomi/misettings/usagestats/adapter/d$a;I)V

    return-void
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;ILjava/util/List;)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/xiaomi/misettings/usagestats/adapter/d$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/adapter/d;->a(Lcom/xiaomi/misettings/usagestats/adapter/d$a;ILjava/util/List;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/adapter/d;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/adapter/d$a;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/adapter/d$a;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance p2, Lcom/xiaomi/misettings/usagestats/adapter/d$a;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0e002e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;-><init>(Landroid/view/View;)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onCreateViewHolder"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->a(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "AppRVAdapter"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->a(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/xiaomi/misettings/usagestats/adapter/d$a;->a(Lcom/xiaomi/misettings/usagestats/adapter/d$a;)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/adapter/b;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/adapter/b;-><init>(Lcom/xiaomi/misettings/usagestats/adapter/d;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2
.end method

.method public setHasStableIds(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->setHasStableIds(Z)V

    return-void
.end method
