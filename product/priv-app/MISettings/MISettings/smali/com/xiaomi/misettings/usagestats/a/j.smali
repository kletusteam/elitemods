.class public Lcom/xiaomi/misettings/usagestats/a/j;
.super Ljava/lang/Object;


# static fields
.field public static a:Lcom/xiaomi/misettings/usagestats/a/j;

.field private static b:Lcom/xiaomi/misettings/usagestats/a/h;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/U;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-wide/32 v1, 0xa00000

    const/4 p1, 0x1

    invoke-static {v0, p1, p1, v1, v2}, Lcom/xiaomi/misettings/usagestats/a/h;->a(Ljava/io/File;IIJ)Lcom/xiaomi/misettings/usagestats/a/h;

    move-result-object p1

    sput-object p1, Lcom/xiaomi/misettings/usagestats/a/j;->b:Lcom/xiaomi/misettings/usagestats/a/h;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "DiskLruCacheUtils"

    const-string v1, "DiskLruCacheUtils: openLruCacheError"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/j;
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/a/j;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/j;->a:Lcom/xiaomi/misettings/usagestats/a/j;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/a/j;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/a/j;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/a/j;->a:Lcom/xiaomi/misettings/usagestats/a/j;

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/a/j;->a:Lcom/xiaomi/misettings/usagestats/a/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/j;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/j;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/a/h;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/a/j;->b:Lcom/xiaomi/misettings/usagestats/a/h;

    invoke-virtual {v1, p1}, Lcom/xiaomi/misettings/usagestats/a/h;->f(Ljava/lang/String;)Lcom/xiaomi/misettings/usagestats/a/h$c;

    move-result-object p1

    if-nez p1, :cond_1

    const-string p1, "DiskLruCacheUtils"

    const-string v1, "not find entry , or entry.readable = false"

    invoke-static {p1, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/xiaomi/misettings/usagestats/a/h$c;->b(I)Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_2
    :goto_0
    return-object v0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, "DiskLruCacheUtils"

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/a/j;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    return-object v1

    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/InputStreamReader;

    sget-object v3, Lcom/xiaomi/misettings/usagestats/a/r;->b:Ljava/nio/charset/Charset;

    invoke-direct {v2, p1, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/a/r;->a(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString: readFroDiskSuccess"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v2

    :try_start_1
    const-string v3, "getString: readFroDiskFail"

    invoke-static {v0, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/io/Closeable;)V

    return-object v1

    :goto_1
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/U;->a(Ljava/io/Closeable;)V

    throw v0
.end method
