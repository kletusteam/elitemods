.class public Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory$EventWrapper;
    }
.end annotation


# static fields
.field public static volatile a:I

.field public static volatile b:Ljava/lang/String;

.field public static volatile c:J

.field private static final d:Ljava/lang/Object;

.field private static e:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static j:Ljava/text/SimpleDateFormat;

.field private static k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->d:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->e:Ljava/util/concurrent/ConcurrentMap;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/v;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/utils/v;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->f:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/w;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/utils/w;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->g:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/x;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/utils/x;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->h:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/y;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/utils/y;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->i:Ljava/util/List;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->j:Ljava/text/SimpleDateFormat;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->k:Z

    return-void
.end method

.method public static a(Landroid/app/usage/UsageEvents$Event;)I
    .locals 3

    const/4 v0, -0x1

    if-nez p0, :cond_0

    return v0

    :cond_0
    :try_start_0
    const-string v1, "mInstanceId"

    invoke-static {p0, v1}, Lb/c/a/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getObjectField error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v1, "LR-AppUsageStatsFactory"

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;JJ)J
    .locals 8

    new-instance v7, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {v7, p1}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, v7

    invoke-static/range {v0 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;JJLcom/xiaomi/misettings/usagestats/f/d;)V

    invoke-virtual {v7}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide p0

    return-wide p0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;JJI)J
    .locals 8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "aggregate()...start <= 0, This may because a cross usage! This can only occur once:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LR-AppUsageStatsFactory"

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    sub-long v0, p4, p2

    const-wide/16 v2, 0x0

    if-eqz p6, :cond_1

    const/4 v4, 0x1

    if-eq p6, v4, :cond_0

    move-wide v4, v2

    goto :goto_0

    :cond_0
    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    goto :goto_0

    :cond_1
    const-wide/16 v4, 0x4

    sget-wide v6, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    mul-long/2addr v4, v6

    :goto_0
    sget-wide v6, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    cmp-long p6, v0, v6

    if-gez p6, :cond_2

    const-wide/16 v6, 0x1

    add-long/2addr p2, v6

    invoke-static {p0, p2, p3, p4, p5}, Lcom/xiaomi/misettings/usagestats/utils/ca;->a(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/app/usage/UsageEvents;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    return-wide v2

    :cond_2
    cmp-long p0, v0, v4

    if-lez p0, :cond_3

    move-wide v0, v2

    :cond_3
    return-wide v0
.end method

.method public static a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/g;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c(Landroid/content/Context;Z)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;JJZ)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJZ)",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p5, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide p3

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;)J

    move-result-wide v1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;)Z

    move-result p5

    if-nez p5, :cond_1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;)V

    :cond_1
    cmp-long p5, v1, p1

    if-lez p5, :cond_2

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    invoke-static {v1, v2, p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result p5

    if-eqz p5, :cond_3

    move-wide p3, v1

    :cond_3
    if-nez p5, :cond_4

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;)V

    :cond_4
    :goto_0
    const/4 p0, 0x0

    :goto_1
    sget p5, Lcom/xiaomi/misettings/usagestats/utils/L;->c:I

    if-ge p0, p5, :cond_6

    add-int/lit8 p0, p0, 0x1

    int-to-long v1, p0

    sget-wide v3, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    mul-long/2addr v1, v3

    add-long/2addr v1, p3

    cmp-long p5, v1, p1

    if-lez p5, :cond_5

    goto :goto_2

    :cond_5
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p5

    invoke-interface {v0, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    :goto_2
    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;Z)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/j;",
            ">;Z)",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation

    const-string v0, "LR-AppUsageStatsFactory"

    const-string v1, "loadDayUsages()......"

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    const-string p0, "No days info provided!"

    invoke-static {v0, p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v3, v0, -0x1

    if-ge v2, v3, :cond_1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/j;

    new-instance v4, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-direct {v4, v3}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    iget-wide v6, v3, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    sget-wide v8, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    add-long/2addr v8, v6

    move-object v5, p0

    move v10, p2

    invoke-static/range {v5 .. v10}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b(Landroid/content/Context;JJZ)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/xiaomi/misettings/usagestats/f/g;->a(Ljava/util/concurrent/ConcurrentHashMap;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p0, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance p1, Lcom/xiaomi/misettings/usagestats/f/j;

    const/4 p2, 0x0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    invoke-direct {p1, p2, v2, v3}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v1
.end method

.method public static a(Landroid/content/Context;Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation

    const-string v0, "LR-AppUsageStatsFactory"

    const-string v1, "loadUsageMonth()......"

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/util/List;Z)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private static a(J)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c()V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->e:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->e:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Map;

    invoke-direct {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(Ljava/util/Map;)V

    return-object v0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static a()V
    .locals 1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c()V

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->e:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    return-void
.end method

.method private static a(Landroid/app/usage/UsageEvents;Landroid/util/ArrayMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/usage/UsageEvents;",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Landroid/app/usage/UsageEvents$Event;",
            ">;>;)V"
        }
    .end annotation

    if-eqz p0, :cond_6

    if-nez p1, :cond_0

    goto :goto_2

    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/app/usage/UsageEvents$Event;

    invoke-direct {v0}, Landroid/app/usage/UsageEvents$Event;-><init>()V

    invoke-virtual {p0, v0}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ba;->b(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->f:Ljava/util/List;

    invoke-virtual {v0}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string p0, "com.android.settings"

    invoke-virtual {p1, p0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1, p0, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    goto :goto_1

    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    if-nez v1, :cond_4

    invoke-virtual {v0}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    return-void

    :cond_6
    :goto_2
    const-string p0, "LR-AppUsageStatsFactory"

    const-string p1, "aggregateEventByPackage()......return since invalid params."

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Landroid/content/Context;JJ)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cacheEventRestore"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LR-AppUsageStatsFactory"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-string v0, "before"

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c(Ljava/lang/String;)V

    invoke-static {p0, p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object p0

    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "lastEvent"

    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    const-string p2, "lastEventPkg"

    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    const-string p3, "lastEventTimeStamp"

    invoke-virtual {p1, p3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p3

    sput p0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    sput-object p2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    sput-wide p3, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    const-string p0, "end"

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string p1, "EventWrapper cacheEventRestore error"

    invoke-static {v1, p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method private static a(Landroid/content/Context;JJJLjava/util/concurrent/ConcurrentHashMap;Ljava/util/List;Z)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJJ",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    move-wide/from16 v9, p1

    move-wide/from16 v11, p3

    move-object/from16 v0, p7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dealCrossUseState:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LR-AppUsageStatsFactory"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_2

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    goto/16 :goto_1

    :cond_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    move-object/from16 v2, p8

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/d;

    sub-long v13, v9, v11

    if-nez v1, :cond_1

    new-instance v1, Lcom/xiaomi/misettings/usagestats/f/d;

    sget-object v2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v13, v14}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    sget-object v2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v13, v14}, Lcom/xiaomi/misettings/usagestats/f/d;->a(J)V

    :goto_0
    move-object v0, v1

    invoke-virtual {v0, v9, v10}, Lcom/xiaomi/misettings/usagestats/f/d;->b(J)V

    move-wide/from16 v1, p3

    move-wide/from16 v3, p1

    move-wide v5, v13

    move-wide/from16 v7, p5

    invoke-virtual/range {v0 .. v8}, Lcom/xiaomi/misettings/usagestats/f/d;->a(JJJJ)V

    if-eqz p9, :cond_2

    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "pkgName"

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {v8, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "time"

    invoke-virtual {v8, v0, v13, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    new-instance v13, Lcom/xiaomi/misettings/usagestats/f/d;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-direct {v13, v0}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    move-object v0, p0

    move-wide/from16 v2, p3

    move-wide/from16 v4, p1

    move-object v6, v13

    invoke-static/range {v0 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;JJLcom/xiaomi/misettings/usagestats/f/d;)V

    const-string v0, "foregroundCount"

    invoke-virtual {v13}, Lcom/xiaomi/misettings/usagestats/f/d;->f()I

    move-result v1

    invoke-virtual {v8, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    invoke-virtual {v7}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-wide/from16 v1, p3

    move-wide/from16 v4, p5

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;JLjava/lang/String;J)V

    move-wide/from16 v1, p5

    invoke-static {p0, v11, v12, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->d(Landroid/content/Context;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    :goto_1
    return-void
.end method

.method private static a(Landroid/content/Context;JJLcom/xiaomi/misettings/usagestats/f/d;Landroid/app/usage/UsageEvents$Event;)V
    .locals 24

    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    move-object/from16 v3, p5

    if-eqz v0, :cond_c

    if-nez v3, :cond_0

    goto/16 :goto_5

    :cond_0
    invoke-virtual/range {p5 .. p5}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "guess()...Last event is MOVE_TO_FOREGROUND, we guess it is still been used. pkgName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "LR-AppUsageStatsFactory"

    invoke-static {v6, v5}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v1, v2, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v9

    const-wide/16 v11, 0x1

    add-long v11, p1, v11

    invoke-static {v0, v11, v12, v9, v10}, Lcom/xiaomi/misettings/usagestats/utils/ca;->a(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents;

    move-result-object v5

    const/4 v14, -0x1

    move v15, v14

    const/16 v16, 0x0

    const-wide/16 v17, 0x0

    :goto_0
    invoke-virtual {v5}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v19

    const/16 v20, 0x1

    if-eqz v19, :cond_6

    new-instance v13, Landroid/app/usage/UsageEvents$Event;

    invoke-direct {v13}, Landroid/app/usage/UsageEvents$Event;-><init>()V

    invoke-virtual {v5, v13}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v21

    if-eqz v21, :cond_5

    invoke-virtual {v13}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-static {v13}, Lcom/xiaomi/misettings/usagestats/utils/ba;->a(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v11

    if-eqz v11, :cond_2

    if-ne v15, v14, :cond_1

    invoke-static/range {p6 .. p6}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/app/usage/UsageEvents$Event;)I

    move-result v15

    :cond_1
    invoke-static {v13}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/app/usage/UsageEvents$Event;)I

    move-result v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "guessInstanceId:"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v14, "/"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v6, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, -0x1

    if-eq v15, v12, :cond_3

    if-ne v15, v11, :cond_3

    invoke-virtual {v13}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J

    move-result-wide v9

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "guess()...selfStopped! eventType="

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v19, v20

    goto :goto_2

    :cond_2
    move v12, v14

    :cond_3
    if-nez v16, :cond_4

    invoke-static {v13}, Lcom/xiaomi/misettings/usagestats/utils/ba;->d(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v11

    if-eqz v11, :cond_4

    move-wide/from16 v22, v9

    invoke-virtual {v13}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J

    move-result-wide v9

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "guess()...otherStarted! other="

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, ",timestamp="

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v6, v11}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-wide/from16 v17, v9

    move/from16 v16, v20

    goto :goto_1

    :cond_4
    move-wide/from16 v22, v9

    goto :goto_1

    :cond_5
    move-wide/from16 v22, v9

    move v12, v14

    :goto_1
    move v14, v12

    move-wide/from16 v9, v22

    goto/16 :goto_0

    :cond_6
    move-wide/from16 v22, v9

    const/16 v19, 0x0

    :goto_2
    if-nez v19, :cond_8

    if-eqz v16, :cond_8

    sget-wide v11, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    const-wide/16 v13, 0x2

    mul-long/2addr v11, v13

    add-long/2addr v11, v9

    invoke-static {v0, v9, v10, v11, v12}, Lcom/xiaomi/misettings/usagestats/utils/ca;->a(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/xiaomi/misettings/usagestats/utils/ba;->a(Landroid/app/usage/UsageEvents;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "guess()... This is a mis-event, and we treat it end while other started"

    invoke-static {v6, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-wide/from16 v9, v17

    goto :goto_3

    :cond_7
    const-string v0, "guess()... This is a cross usage, because we find it is stopped in the future."

    invoke-static {v6, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    :goto_3
    sub-long v4, v9, p1

    iput-wide v9, v3, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    cmp-long v0, v7, v1

    if-gtz v0, :cond_9

    const-string v0, "guess()... Should not go here!"

    invoke-static {v6, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    const-wide/16 v7, 0x3

    mul-long/2addr v0, v7

    cmp-long v0, v4, v0

    if-gtz v0, :cond_b

    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-gez v0, :cond_a

    goto :goto_4

    :cond_a
    invoke-virtual {v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/f/d;->a(J)V

    invoke-virtual {v3, v9, v10}, Lcom/xiaomi/misettings/usagestats/f/d;->b(J)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "guess()...gap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/32 v1, 0xea60

    div-long v1, v4, v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "min("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "ms)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_b
    :goto_4
    const-string v0, "guess()... the gap is invalid and we treat it as a mis-event"

    invoke-static {v6, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    :goto_5
    return-void
.end method

.method public static a(Landroid/content/Context;JJLjava/util/concurrent/ConcurrentHashMap;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJLjava/util/concurrent/ConcurrentHashMap;Z)V

    return-void
.end method

.method public static a(Landroid/content/Context;JJLjava/util/concurrent/ConcurrentHashMap;Z)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;Z)V"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p5

    if-eqz v0, :cond_a

    if-nez v1, :cond_0

    goto/16 :goto_3

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->l(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual/range {p5 .. p5}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual/range {p5 .. p5}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    array-length v4, v3

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_a

    aget-object v6, v3, v5

    invoke-virtual {v1, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/xiaomi/misettings/usagestats/f/d;

    if-nez v7, :cond_1

    goto/16 :goto_2

    :cond_1
    invoke-virtual {v7}, Lcom/xiaomi/misettings/usagestats/f/d;->g()J

    move-result-wide v8

    cmp-long v8, v8, p1

    const-string v9, "LR-AppUsageStatsFactory"

    if-ltz v8, :cond_8

    invoke-virtual {v7}, Lcom/xiaomi/misettings/usagestats/f/d;->g()J

    move-result-wide v10

    cmp-long v8, v10, p3

    if-lez v8, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v7}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Lcom/xiaomi/misettings/usagestats/f/d;->k()Z

    move-result v7

    if-nez v7, :cond_3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "filterUsageEventResult()......Skip, invalid stats. pkgName="

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v9, v7}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    if-eqz p6, :cond_4

    sget-object v7, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->i:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    goto :goto_2

    :cond_4
    invoke-interface {v2, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    sget-object v7, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->g:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    :cond_5
    invoke-static {v8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    :cond_6
    invoke-static {p0, v8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v1, v6}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "filterUsageEventResult()......Skip, filter out the stat for no icon on launcher / special, pkgName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v6}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    :goto_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Wow! We filter out it again? pkgName="

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ", lastTimeUsed="

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Lcom/xiaomi/misettings/usagestats/f/d;->g()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/xiaomi/misettings/usagestats/utils/L;->e(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v9, v7}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :cond_a
    :goto_3
    return-void
.end method

.method private static a(Landroid/content/Context;JJZLandroid/content/pm/PackageManager;JLjava/util/concurrent/ConcurrentHashMap;Ljava/util/List;)V
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJZ",
            "Landroid/content/pm/PackageManager;",
            "J",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v8, p0

    move-object/from16 v9, p9

    const-string v10, "LR-AppUsageStatsFactory"

    const-string v0, "aggregateUsageStat start"

    invoke-static {v10, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface/range {p10 .. p10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Ljava/lang/String;

    move-object/from16 v13, p6

    invoke-static {v13, v12}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v14

    if-nez v14, :cond_0

    invoke-static {v8, v12}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v7, p10

    goto :goto_0

    :cond_0
    const-string v15, "com.android.settings"

    move-object/from16 v7, p10

    invoke-interface {v7, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->f:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    :cond_2
    move-object v5, v0

    if-nez v1, :cond_3

    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {v0, v12}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    move-object v6, v0

    goto :goto_1

    :cond_3
    move-object v6, v1

    :goto_1
    invoke-virtual {v6, v12}, Lcom/xiaomi/misettings/usagestats/f/b;->c(Ljava/lang/String;)V

    const-wide/16 v3, 0x0

    invoke-virtual {v6, v3, v4}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Lcom/xiaomi/misettings/usagestats/f/d;->b(I)V

    iput-wide v3, v6, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    iput-wide v3, v6, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    move-object/from16 v1, p0

    move-object/from16 v16, v11

    move-object v11, v2

    move-object v2, v12

    move-wide/from16 v17, v3

    move-wide/from16 v3, p3

    move-object v13, v5

    move-object/from16 v19, v6

    move-wide/from16 v5, p1

    move-object/from16 v7, v19

    invoke-static/range {v1 .. v7}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;JJLcom/xiaomi/misettings/usagestats/f/d;)V

    invoke-virtual/range {v19 .. v19}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v1

    invoke-virtual/range {v19 .. v19}, Lcom/xiaomi/misettings/usagestats/f/d;->f()I

    move-result v3

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->f:Ljava/util/List;

    invoke-interface {v0, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v12, v15

    :cond_4
    invoke-virtual {v9, v12}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/d;

    if-eqz v0, :cond_7

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/d;->a(J)V

    iget-wide v4, v0, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    cmp-long v6, v4, v17

    if-eqz v6, :cond_5

    move-object/from16 v6, v19

    iget-wide v14, v6, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    cmp-long v4, v4, v14

    if-gez v4, :cond_6

    goto :goto_2

    :cond_5
    move-object/from16 v6, v19

    :goto_2
    iget-wide v4, v6, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    iput-wide v4, v0, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    :cond_6
    move-object v5, v10

    move-object v15, v11

    goto :goto_5

    :cond_7
    move-object/from16 v6, v19

    new-instance v4, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {v4, v12}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    iget-wide v7, v4, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    cmp-long v0, v7, v17

    if-eqz v0, :cond_8

    move-object v5, v10

    move-object v15, v11

    iget-wide v10, v6, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    cmp-long v0, v7, v10

    if-lez v0, :cond_9

    goto :goto_3

    :cond_8
    move-object v5, v10

    move-object v15, v11

    :goto_3
    iget-wide v7, v6, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    iput-wide v7, v4, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    :cond_9
    iget-wide v7, v4, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    cmp-long v0, v7, v17

    if-eqz v0, :cond_a

    iget-wide v10, v6, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    cmp-long v0, v7, v10

    if-gez v0, :cond_b

    :cond_a
    iget-wide v7, v6, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    iput-wide v7, v4, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    :cond_b
    invoke-virtual {v4, v14}, Lcom/xiaomi/misettings/usagestats/f/b;->a(Landroid/content/pm/PackageInfo;)V

    invoke-virtual {v4, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    :try_start_0
    invoke-virtual {v9, v12, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    move-object v7, v0

    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    :goto_4
    move-object v0, v4

    :goto_5
    invoke-virtual {v0, v3}, Lcom/xiaomi/misettings/usagestats/f/d;->a(I)V

    move-wide/from16 v7, p1

    invoke-virtual {v0, v7, v8}, Lcom/xiaomi/misettings/usagestats/f/d;->b(J)V

    move-object/from16 v20, v0

    move-wide/from16 v21, p3

    move-wide/from16 v23, p1

    move-wide/from16 v25, v1

    move-wide/from16 v27, p7

    invoke-virtual/range {v20 .. v28}, Lcom/xiaomi/misettings/usagestats/f/d;->a(JJJJ)V

    if-eqz p5, :cond_c

    :try_start_1
    const-string v0, "pkgName"

    move-object v4, v15

    invoke-virtual {v4, v0, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "time"

    invoke-virtual {v4, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "foregroundCount"

    invoke-virtual {v4, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    invoke-virtual {v13, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_6

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_c
    :goto_6
    move-object/from16 v8, p0

    move-object v10, v5

    move-object v1, v6

    move-object v0, v13

    move-object/from16 v11, v16

    goto/16 :goto_0

    :cond_d
    move-object v5, v10

    if-eqz p5, :cond_f

    if-nez v0, :cond_e

    const-string v0, "[]"

    goto :goto_7

    :cond_e
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_7
    move-object v4, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "saveCache:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v5

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v1, p0

    move-wide/from16 v2, p3

    move-wide/from16 v5, p7

    invoke-static/range {v1 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;JLjava/lang/String;J)V

    move-wide/from16 v4, p7

    invoke-static {v1, v2, v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->d(Landroid/content/Context;JJ)V

    :cond_f
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/app/usage/UsageEvents;JJLjava/util/concurrent/ConcurrentHashMap;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/usage/UsageEvents;",
            "JJ",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p6

    const-string v2, "LR-AppUsageStatsFactory"

    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    if-nez p0, :cond_0

    goto/16 :goto_3

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "aggregateUsageStatsByEvent().......start="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p3}, Lcom/xiaomi/misettings/usagestats/utils/L;->e(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v10, p2

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v5, ")\uff0c end="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {p4 .. p5}, Lcom/xiaomi/misettings/usagestats/utils/L;->e(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v12, p4

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    new-instance v15, Landroid/util/ArrayMap;

    invoke-direct {v15}, Landroid/util/ArrayMap;-><init>()V

    invoke-static {v0, v15}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/app/usage/UsageEvents;Landroid/util/ArrayMap;)V

    invoke-virtual {v15}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Ljava/lang/String;

    new-instance v7, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {v7, v9}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v9}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Ljava/util/List;

    move-object/from16 v3, p0

    move-wide/from16 v5, p2

    move-object/from16 p1, v7

    move-wide/from16 v7, p4

    move-object/from16 v16, v0

    move-object v0, v9

    move-object/from16 v9, p1

    invoke-static/range {v3 .. v9}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/util/List;JJLcom/xiaomi/misettings/usagestats/f/d;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v14, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to load package info for pkg:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v14, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v4, p1

    goto :goto_1

    :cond_2
    move-object/from16 v4, p1

    move-object v9, v0

    :goto_1
    invoke-virtual {v4, v9}, Lcom/xiaomi/misettings/usagestats/f/b;->a(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Lcom/xiaomi/misettings/usagestats/f/b;->a(Landroid/content/pm/PackageInfo;)V

    invoke-virtual {v1, v0, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_2
    move-object/from16 v0, v16

    goto :goto_0

    :cond_4
    return-void

    :cond_5
    :goto_3
    const-string v0, "aggregateUsageStatsByEvent()......return since invalid params."

    invoke-static {v2, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/app/usage/UsageEvents;Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/usage/UsageEvents;",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    if-nez p0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/app/usage/UsageEvents$Event;

    invoke-direct {v0}, Landroid/app/usage/UsageEvents$Event;-><init>()V

    invoke-virtual {p1, v0}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0, v0, p2}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/pm/PackageManager;Landroid/app/usage/UsageEvents$Event;Ljava/util/concurrent/ConcurrentHashMap;)Z

    goto :goto_0

    :cond_2
    return-void

    :cond_3
    :goto_1
    const-string p0, "LR-AppUsageStatsFactory"

    const-string p1, "aggregateUsageEvent()......return since invalid params."

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V
    .locals 13

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v0

    iget-wide v10, v0, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    add-long v2, v10, v0

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v4, v10

    invoke-static/range {v1 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJZ)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move-wide v5, v10

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-wide v7, v10

    invoke-static/range {v1 .. v9}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJZ)V

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJ)V
    .locals 9

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v6

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-static/range {v0 .. v8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJZ)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJ)V
    .locals 9

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-wide v6, p6

    invoke-static/range {v0 .. v8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJZ)V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJZ)V
    .locals 1

    const-class v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;

    monitor-enter v0

    :try_start_0
    invoke-static/range {p0 .. p8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;JJLcom/xiaomi/misettings/usagestats/f/d;)V
    .locals 23

    move-object/from16 v0, p1

    move-wide/from16 v1, p2

    move-wide/from16 v3, p4

    move-object/from16 v5, p6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "loadCurrentSubTimeUsageStatForPackage start:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "LR-AppUsageStatsFactory"

    invoke-static {v7, v6}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    sub-long v8, v3, v1

    sget-wide v10, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    div-long/2addr v8, v10

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    long-to-int v6, v8

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v10, v9

    move v9, v8

    :goto_0
    if-ge v9, v6, :cond_5

    int-to-long v11, v9

    sget-wide v13, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    mul-long/2addr v11, v13

    add-long/2addr v11, v1

    add-int/lit8 v9, v9, 0x1

    move/from16 v22, v6

    int-to-long v5, v9

    mul-long/2addr v5, v13

    add-long/2addr v5, v1

    cmp-long v13, v5, v3

    if-lez v13, :cond_0

    move-object/from16 v5, p0

    move-wide v13, v3

    goto :goto_1

    :cond_0
    move-wide v13, v5

    move-object/from16 v5, p0

    :goto_1
    invoke-static {v5, v11, v12, v13, v14}, Lcom/xiaomi/misettings/usagestats/utils/ca;->a(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents;

    move-result-object v6

    if-nez v10, :cond_1

    new-instance v10, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {v10, v0}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v10, v0}, Lcom/xiaomi/misettings/usagestats/f/b;->c(Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    invoke-virtual {v10, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    invoke-virtual {v10, v8}, Lcom/xiaomi/misettings/usagestats/f/d;->b(I)V

    invoke-static {v6, v0}, Lcom/xiaomi/misettings/usagestats/utils/ba;->b(Landroid/app/usage/UsageEvents;Ljava/lang/String;)Ljava/util/List;

    move-result-object v16

    move-object/from16 v15, p0

    move-wide/from16 v17, v11

    move-wide/from16 v19, v13

    move-object/from16 v21, v10

    invoke-static/range {v15 .. v21}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/util/List;JJLcom/xiaomi/misettings/usagestats/f/d;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "Ops! Fail to aggregate~"

    invoke-static {v7, v6}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object/from16 v6, p6

    iget-wide v11, v6, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    cmp-long v1, v11, v1

    if-eqz v1, :cond_3

    iget-wide v1, v10, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    cmp-long v1, v11, v1

    if-lez v1, :cond_4

    :cond_3
    iget-wide v1, v10, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    iput-wide v1, v6, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    :cond_4
    iget-wide v1, v10, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    iput-wide v1, v6, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    invoke-virtual {v10}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v1

    invoke-virtual {v6, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/d;->a(J)V

    invoke-virtual {v10}, Lcom/xiaomi/misettings/usagestats/f/d;->f()I

    move-result v1

    invoke-virtual {v6, v1}, Lcom/xiaomi/misettings/usagestats/f/d;->a(I)V

    move-wide/from16 v1, p2

    move-object v5, v6

    move/from16 v6, v22

    goto :goto_0

    :cond_5
    move-object v6, v5

    if-eqz v6, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadCurrentSubTimeUsageStatForPackage end:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p6 .. p6}, Lcom/xiaomi/misettings/usagestats/f/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;Landroid/app/usage/UsageEvents;JJLjava/util/concurrent/ConcurrentHashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Landroid/app/usage/UsageStats;",
            ">;",
            "Landroid/app/usage/UsageEvents;",
            "JJ",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;)V"
        }
    .end annotation

    const-string v0, "LR-AppUsageStatsFactory"

    if-eqz p1, :cond_5

    if-eqz p2, :cond_5

    if-nez p7, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-static {p0, p2, p7}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Landroid/app/usage/UsageEvents;Ljava/util/concurrent/ConcurrentHashMap;)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/usage/UsageStats;

    invoke-virtual {p1}, Landroid/app/usage/UsageStats;->getPackageName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Landroid/app/usage/UsageStats;->getLastTimeUsed()J

    move-result-wide v1

    cmp-long v1, v1, p3

    if-ltz v1, :cond_3

    invoke-virtual {p1}, Landroid/app/usage/UsageStats;->getLastTimeUsed()J

    move-result-wide v1

    cmp-long v1, v1, p5

    if-lez v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p7, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/d;

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/app/usage/UsageStats;->getLastTimeUsed()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/app/usage/UsageStats;->getTotalTimeInForeground()J

    move-result-wide p1

    invoke-virtual {v1, v2, v3, p1, p2}, Lcom/xiaomi/misettings/usagestats/f/d;->a(JJ)V

    goto :goto_0

    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "aggregateUsageStats()......Skip "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ", no move to foreground event found!"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    :goto_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Wow! We filter out it since out of the range. pkgName="

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/usage/UsageStats;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", lastTimeUsed="

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/usage/UsageStats;->getLastTimeUsed()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/L;->e(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    return-void

    :cond_5
    :goto_2
    const-string p0, "aggregateUsageStats()......return since invalid params."

    invoke-static {v0, p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;JJJ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;",
            "Ljava/lang/String;",
            "JJJ)V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-wide/from16 v10, p3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "obtainFromCache:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v12, p5

    invoke-virtual {v1, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LR-AppUsageStatsFactory"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    new-instance v14, Lorg/json/JSONArray;

    move-object/from16 v1, p2

    invoke-direct {v14, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    move v15, v1

    :goto_0
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v15, v1, :cond_2

    invoke-virtual {v14, v15}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "pkgName"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "time"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    const-string v3, "foregroundCount"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v8, p0

    invoke-static {v8, v2}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/Application;->c()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_2

    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/f/d;

    if-nez v4, :cond_1

    new-instance v4, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {v4, v2}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6, v7}, Lcom/xiaomi/misettings/usagestats/f/d;->c(J)V

    invoke-virtual {v4, v3}, Lcom/xiaomi/misettings/usagestats/f/b;->a(Landroid/content/pm/PackageInfo;)V

    invoke-virtual {v4, v1}, Lcom/xiaomi/misettings/usagestats/f/d;->b(I)V

    invoke-virtual {v0, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-virtual {v4, v6, v7}, Lcom/xiaomi/misettings/usagestats/f/d;->a(J)V

    invoke-virtual {v4, v1}, Lcom/xiaomi/misettings/usagestats/f/d;->a(I)V

    :goto_1
    move-object v1, v4

    invoke-virtual {v1, v10, v11}, Lcom/xiaomi/misettings/usagestats/f/d;->b(J)V

    move-wide/from16 v2, p3

    move-wide/from16 v4, p5

    move-wide/from16 v8, p7

    invoke-virtual/range {v1 .. v9}, Lcom/xiaomi/misettings/usagestats/f/d;->a(JJJJ)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_2
    return-void
.end method

.method public static a(Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;)V"
        }
    .end annotation

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->i:Ljava/util/List;

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public static a(II)Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-eq p0, v1, :cond_1

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0
.end method

.method private static a(Landroid/app/usage/UsageEvents;Ljava/lang/String;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    move v1, v0

    :cond_1
    invoke-virtual {p0}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v2

    const-string v3, "LR-AppUsageStatsFactory"

    if-eqz v2, :cond_2

    new-instance v2, Landroid/app/usage/UsageEvents$Event;

    invoke-direct {v2}, Landroid/app/usage/UsageEvents$Event;-><init>()V

    invoke-virtual {p0, v2}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    invoke-virtual {v2}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "isHasOtherPackageResume pkg"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v5

    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "isHasOtherPackageResume count"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method private static a(Landroid/content/Context;J)Z
    .locals 9

    const-string v0, "LR-AppUsageStatsFactory"

    const-string v1, "findYesterDayEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    move v2, v1

    :goto_0
    sget v3, Lcom/xiaomi/misettings/usagestats/utils/L;->c:I

    const/4 v4, 0x0

    if-gt v2, v3, :cond_2

    int-to-long v5, v2

    sget-wide v7, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    mul-long/2addr v5, v7

    sub-long v5, p1, v5

    add-long/2addr v7, v5

    invoke-static {p0, v5, v6, v7, v8}, Lcom/xiaomi/misettings/usagestats/utils/ca;->a(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/utils/ba;->a(Landroid/app/usage/UsageEvents;)[Z

    move-result-object v3

    aget-boolean v5, v3, v1

    if-eqz v5, :cond_0

    sput v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    sput-wide p1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "_timeRange find:"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget p2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-wide p1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    invoke-virtual {p0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    aget-boolean v3, v3, v4

    if-eqz v3, :cond_1

    const-string p0, "has other stop event"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v4
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0, p1}, Lcom/misettings/common/utils/c;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;JJLcom/xiaomi/misettings/usagestats/f/d;)Z
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Landroid/app/usage/UsageEvents$Event;",
            ">;JJ",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ")Z"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v5, p6

    const/4 v1, 0x0

    const-string v2, "LR-AppUsageStatsFactory"

    if-eqz v0, :cond_10

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_10

    if-nez v5, :cond_0

    goto/16 :goto_6

    :cond_0
    sub-long v3, p4, p2

    sget-wide v6, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    cmp-long v3, v3, v6

    const/4 v13, 0x1

    if-lez v3, :cond_1

    move v3, v1

    goto :goto_0

    :cond_1
    move v3, v13

    :goto_0
    invoke-virtual/range {p6 .. p6}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "aggregate pkgName:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "--"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v0, :cond_2

    move v8, v1

    goto :goto_1

    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v8

    :goto_1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v14, 0x0

    move-object/from16 v16, v6

    move-wide v6, v14

    :goto_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v1, v8, :cond_e

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/usage/UsageEvents$Event;

    invoke-virtual {v8}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ops! Fail to aggregate event.pkgName="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-static {v8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b(Landroid/app/usage/UsageEvents$Event;)Z

    move-result v9

    if-eqz v9, :cond_4

    goto/16 :goto_5

    :cond_4
    invoke-virtual {v8}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v9

    if-eq v9, v13, :cond_b

    const/4 v10, 0x2

    if-eq v9, v10, :cond_5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ops! Invalid eventType for aggregate. pkgName="

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p6 .. p6}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ", eventType="

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, ",start="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_5
    cmp-long v9, v6, v14

    if-gtz v9, :cond_6

    if-lez v1, :cond_6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "aggregate()...start <= 0, This is not the first MOVE_TO_BACKGROUND."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_6
    invoke-virtual {v8}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J

    move-result-wide v10

    if-gtz v9, :cond_7

    move-object/from16 v6, p0

    move-object v7, v4

    move-wide/from16 v8, p2

    move-wide/from16 v17, v10

    move v12, v3

    invoke-static/range {v6 .. v12}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;JJI)J

    move-result-wide v6

    move-wide/from16 v8, v17

    goto :goto_3

    :cond_7
    move-wide v8, v10

    sub-long v6, v8, v6

    :goto_3
    cmp-long v10, v6, v14

    if-gtz v10, :cond_8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "aggregate()...Skip this aggregate, diff is invalid! diff="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v5, v6, v7}, Lcom/xiaomi/misettings/usagestats/f/d;->a(J)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "aggregate()...diff="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, "ms"

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v8, v9}, Lcom/xiaomi/misettings/usagestats/f/d;->d(J)V

    iget-wide v6, v5, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    cmp-long v10, v6, v14

    if-eqz v10, :cond_9

    cmp-long v6, v6, v8

    if-gez v6, :cond_a

    :cond_9
    iput-wide v8, v5, Lcom/xiaomi/misettings/usagestats/f/d;->j:J

    :cond_a
    :goto_4
    move-wide v6, v14

    goto :goto_5

    :cond_b
    invoke-virtual {v8}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J

    move-result-wide v6

    invoke-virtual/range {p6 .. p6}, Lcom/xiaomi/misettings/usagestats/f/d;->j()V

    iget-wide v9, v5, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    cmp-long v11, v9, v14

    if-eqz v11, :cond_c

    cmp-long v9, v9, v6

    if-lez v9, :cond_d

    :cond_c
    iput-wide v6, v5, Lcom/xiaomi/misettings/usagestats/f/d;->i:J

    :cond_d
    move-object/from16 v16, v8

    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    :cond_e
    cmp-long v0, v6, v14

    if-lez v0, :cond_f

    move-object/from16 v0, p0

    move-wide v1, v6

    move-wide/from16 v3, p4

    move-object/from16 v5, p6

    move-object/from16 v6, v16

    invoke-static/range {v0 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJLcom/xiaomi/misettings/usagestats/f/d;Landroid/app/usage/UsageEvents$Event;)V

    :cond_f
    return v13

    :cond_10
    :goto_6
    const-string v0, "aggregate()......Fail since invalid params."

    invoke-static {v2, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method private static a(Landroid/content/pm/PackageManager;Landroid/app/usage/UsageEvents$Event;Ljava/util/concurrent/ConcurrentHashMap;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Landroid/app/usage/UsageEvents$Event;",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_6

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getEventType()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    goto :goto_2

    :cond_0
    invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/d;

    goto :goto_0

    :cond_1
    new-instance v3, Lcom/xiaomi/misettings/usagestats/f/d;

    invoke-direct {v3, p1}, Lcom/xiaomi/misettings/usagestats/f/d;-><init>(Ljava/lang/String;)V

    :goto_0
    const-string v4, "LR-AppUsageStatsFactory"

    if-nez v1, :cond_4

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Fail to load package info for pkg:"

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    :cond_2
    iget-object v5, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p0, v5}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object p0

    if-eqz p0, :cond_3

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_3
    move-object p0, p1

    :goto_1
    invoke-virtual {v3, p0}, Lcom/xiaomi/misettings/usagestats/f/b;->a(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Lcom/xiaomi/misettings/usagestats/f/b;->a(Landroid/content/pm/PackageInfo;)V

    invoke-virtual {p2, p1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    if-nez v3, :cond_5

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Fail to set app usage status for pkg:"

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    :cond_5
    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/d;->j()V

    return v2

    :cond_6
    :goto_2
    return v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->h:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static b(Landroid/content/Context;Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            ">;"
        }
    .end annotation

    const-string v0, "LR-AppUsageStatsFactory"

    const-string v1, "loadUsageWeek()......"

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(Z)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/util/List;Z)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private static b(Landroid/content/Context;JJZ)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJZ)",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadUsage()......start="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", end="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LR-AppUsageStatsFactory"

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p5, :cond_0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c(Landroid/content/Context;JJ)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance p5, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p5}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {p0, p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/utils/ca;->b(Landroid/content/Context;JJ)Ljava/util/List;

    move-result-object v1

    invoke-static {p0, p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/utils/ca;->a(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents;

    move-result-object v2

    move-object v0, p0

    move-wide v3, p1

    move-wide v5, p3

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/util/List;Landroid/app/usage/UsageEvents;JJLjava/util/concurrent/ConcurrentHashMap;)V

    return-object p5
.end method

.method public static b()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJZ)V
    .locals 18

    move-object/from16 v0, p0

    move-wide/from16 v11, p2

    move-wide/from16 v13, p4

    move-wide/from16 v8, p6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadUsageByEndTimeImpl:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v15, "LR-AppUsageStatsFactory"

    invoke-static {v15, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    cmp-long v1, v8, v13

    if-nez v1, :cond_0

    const-string v2, "loadUsageByEndTimeImpl:Current Day Start---------------------------------"

    invoke-static {v15, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    cmp-long v2, v11, v13

    if-eqz v2, :cond_10

    if-nez v0, :cond_1

    goto/16 :goto_5

    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v10

    if-eqz p8, :cond_2

    invoke-static {v0, v13, v14, v8, v9}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b(Ljava/lang/String;)Z

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "subTimeDataValid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v15, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "obtainFromCache:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v15, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v7

    move-object v2, v10

    move-wide/from16 v4, p4

    move-wide/from16 v6, p2

    move-wide v10, v8

    move-wide/from16 v8, p6

    invoke-static/range {v1 .. v9}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/pm/PackageManager;Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;JJJ)V

    invoke-static {v0, v13, v14, v10, v11}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJ)V

    return-void

    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v13, v14, v11, v12}, Lcom/xiaomi/misettings/usagestats/utils/ca;->a(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/xiaomi/misettings/usagestats/utils/ba;->a(Landroid/app/usage/UsageEvents;Ljava/util/List;)V

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_f

    const-string v2, "pkgList isEmpty:"

    invoke-static {v15, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move v1, v3

    :goto_0
    if-eqz v1, :cond_4

    sput-boolean v2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->k:Z

    :cond_4
    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    const-wide/16 v16, 0x0

    cmp-long v4, v4, v16

    if-eqz v4, :cond_7

    sget-object v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    goto/16 :goto_1

    :cond_5
    sget v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    if-ne v4, v2, :cond_6

    sget-object v2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    invoke-static {v13, v14, v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result v2

    if-eqz v2, :cond_6

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    cmp-long v2, v13, v4

    if-ltz v2, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadUsageByEndTime: has app cross hour pkgName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",startTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v15, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x13a21

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "has app cross hour pkgName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ",lastEvent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ",lastTimeStamp="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    move-object/from16 v1, p0

    move-wide/from16 v2, p2

    move-wide/from16 v4, p4

    move-object/from16 v16, v6

    move-wide/from16 v6, p6

    move-object v8, v10

    move-object/from16 v9, v16

    move/from16 v10, p8

    invoke-static/range {v1 .. v10}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJJLjava/util/concurrent/ConcurrentHashMap;Ljava/util/List;Z)V

    sput-wide v11, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    goto/16 :goto_5

    :cond_6
    if-eqz v1, :cond_10

    sput-boolean v3, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->k:Z

    goto/16 :goto_5

    :cond_7
    :goto_1
    move-object/from16 v16, v6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initTimeStamp and pkg:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v5, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v15, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->k:Z

    if-nez v4, :cond_8

    return-void

    :cond_8
    invoke-static {v0, v8, v9, v13, v14}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b(Landroid/content/Context;JJ)[Z

    move-result-object v4

    aget-boolean v5, v4, v3

    aget-boolean v2, v4, v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isFindEvent,isFindLastTimeRangeEvent:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v15, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v2, :cond_a

    if-nez v5, :cond_9

    invoke-static {v0, v8, v9}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;J)Z

    move-result v2

    if-eqz v2, :cond_9

    goto :goto_2

    :cond_9
    if-eqz v1, :cond_c

    sput-boolean v3, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->k:Z

    goto :goto_3

    :cond_a
    :goto_2
    if-eqz v10, :cond_d

    sget-object v2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    if-nez v2, :cond_b

    goto :goto_4

    :cond_b
    const-string v1, "handler empty cross"

    invoke-static {v15, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v1, p0

    move-wide/from16 v2, p2

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move-object v8, v10

    move-object/from16 v9, v16

    move/from16 v10, p8

    invoke-static/range {v1 .. v10}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJJLjava/util/concurrent/ConcurrentHashMap;Ljava/util/List;Z)V

    sput-wide v11, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    :cond_c
    :goto_3
    return-void

    :cond_d
    :goto_4
    if-eqz v1, :cond_e

    sput-boolean v3, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->k:Z

    :cond_e
    return-void

    :cond_f
    move-object/from16 v16, v6

    move-object/from16 v1, p0

    move-wide/from16 v2, p2

    move-wide/from16 v4, p4

    move/from16 v6, p8

    move-wide/from16 v8, p6

    move-object/from16 v11, v16

    :try_start_0
    invoke-static/range {v1 .. v11}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJZLandroid/content/pm/PackageManager;JLjava/util/concurrent/ConcurrentHashMap;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    move-object v1, v0

    const-string v0, "loadUsageByEndTime: "

    invoke-static {v15, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_10
    :goto_5
    return-void
.end method

.method private static b(Landroid/app/usage/UsageEvents$Event;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.xiaomi.misettings"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/usage/UsageEvents$Event;->getClassName()Ljava/lang/String;

    move-result-object p0

    const-class v0, Lcom/xiaomi/misettings/usagestats/DeviceTimeoverActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 2

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "pkgName"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "time"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "foregroundCount"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private static b(Landroid/content/Context;JJ)[Z
    .locals 9

    const-string v0, "LR-AppUsageStatsFactory"

    const-string v1, "findLastTimeRangeEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    new-array v1, v1, [Z

    cmp-long v2, p3, p1

    if-nez v2, :cond_0

    return-object v1

    :cond_0
    const/4 v8, 0x0

    move-object v3, p0

    move-wide v4, p3

    move-wide v6, p1

    invoke-static/range {v3 .. v8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJZ)Ljava/util/List;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    const/4 p4, 0x1

    sub-int/2addr p3, p4

    :goto_0
    if-ltz p3, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "findLastTimeRangeEvent index"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    int-to-long v2, p3

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    mul-long/2addr v2, v4

    add-long/2addr v2, p1

    add-long/2addr v4, v2

    invoke-static {p0, v2, v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/ca;->a(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/utils/ba;->a(Landroid/app/usage/UsageEvents;)[Z

    move-result-object v2

    aget-boolean v3, v2, p4

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    sput p4, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    sput-wide p1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "_timeRange find:"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget p2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-wide p1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    invoke-virtual {p0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    aput-boolean p4, v1, v4

    aput-boolean p4, v1, p4

    return-object v1

    :cond_1
    aget-boolean v2, v2, v4

    if-eqz v2, :cond_2

    const-string p0, "has other stop event"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    aput-boolean p4, v1, v4

    goto :goto_1

    :cond_2
    add-int/lit8 p3, p3, -0x1

    goto :goto_0

    :cond_3
    :goto_1
    return-object v1
.end method

.method public static declared-synchronized c(Landroid/content/Context;Z)Lcom/xiaomi/misettings/usagestats/f/g;
    .locals 18

    move-object/from16 v0, p0

    const-class v9, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;

    monitor-enter v9

    :try_start_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v10

    sget-wide v1, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    sub-long v12, v10, v1

    new-instance v14, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/f/j;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v12, v13}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-direct {v14, v1}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    if-eqz p1, :cond_0

    invoke-static {v12, v13}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(J)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v0, "LR-AppUsageStatsFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadUsageYesterday: from memory"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14, v1}, Lcom/xiaomi/misettings/usagestats/f/g;->a(Ljava/util/concurrent/ConcurrentHashMap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v9

    return-object v14

    :cond_0
    :try_start_1
    invoke-virtual {v14}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-static {v0, v1, v12, v13}, Lcom/xiaomi/misettings/usagestats/utils/E;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentHashMap;J)Z

    move-result v15

    if-nez v15, :cond_2

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-wide v2, v10

    move-wide v4, v12

    invoke-static/range {v1 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJZ)Ljava/util/List;

    move-result-object v1

    const-string v2, "LR-AppUsageStatsFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadUsageYesterday: from os, serializeTime="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, ",timeSize="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move-wide v5, v12

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Ljava/lang/Long;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-object/from16 v1, p0

    move-object v2, v14

    move-wide v7, v12

    invoke-static/range {v1 .. v8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJ)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b()V

    goto :goto_1

    :cond_2
    const-string v1, "LR-AppUsageStatsFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadUsageYesterday: from cache"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/H;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v14}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v6

    move-object/from16 v1, p0

    move-wide v2, v12

    move-wide v4, v10

    invoke-static/range {v1 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJLjava/util/concurrent/ConcurrentHashMap;)V

    const-wide/16 v1, 0x0

    invoke-virtual {v14, v1, v2}, Lcom/xiaomi/misettings/usagestats/f/g;->a(J)V

    invoke-virtual {v14}, Lcom/xiaomi/misettings/usagestats/f/g;->i()V

    if-nez v15, :cond_3

    invoke-virtual {v14}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-static {v0, v1, v12, v13}, Lcom/xiaomi/misettings/usagestats/utils/E;->b(Landroid/content/Context;Ljava/util/concurrent/ConcurrentHashMap;J)V

    :cond_3
    if-eqz p1, :cond_4

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->e:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    monitor-exit v9

    return-object v14

    :catchall_0
    move-exception v0

    monitor-exit v9

    throw v0
.end method

.method private static c(Landroid/content/Context;JJ)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ)",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/misettings/usagestats/f/d;",
            ">;"
        }
    .end annotation

    const-string v0, "LR-AppUsageStatsFactory"

    const-string v1, "loadUsageAccurately()......"

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(J)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    :cond_1
    invoke-static {p0, v0, p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/E;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentHashMap;J)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0, p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/utils/ca;->a(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents;

    move-result-object v2

    move-object v1, p0

    move-wide v3, p1

    move-wide v5, p3

    move-object v7, v0

    invoke-static/range {v1 .. v7}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Landroid/app/usage/UsageEvents;JJLjava/util/concurrent/ConcurrentHashMap;)V

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJLjava/util/concurrent/ConcurrentHashMap;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide p3

    invoke-static {p1, p2, p3, p4}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(JJ)Z

    move-result p3

    if-nez p3, :cond_3

    invoke-static {p0, v0, p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/E;->b(Landroid/content/Context;Ljava/util/concurrent/ConcurrentHashMap;J)V

    goto :goto_0

    :cond_2
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;JJLjava/util/concurrent/ConcurrentHashMap;)V

    :cond_3
    :goto_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->e:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method private static declared-synchronized c()V
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->e:Ljava/util/concurrent/ConcurrentMap;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->e:Ljava/util/concurrent/ConcurrentMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static c(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJZ)V
    .locals 0

    invoke-static/range {p0 .. p8}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;JJJZ)V

    return-void
.end method

.method private static c(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "printLastEvent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-wide v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "LR-AppUsageStatsFactory"

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static d(Landroid/content/Context;JJ)V
    .locals 8

    const-string v0, "LR-AppUsageStatsFactory"

    const-string v1, "saveEventCache"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory$EventWrapper;

    sget v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a:I

    sget-object v1, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->b:Ljava/lang/String;

    sget-wide v3, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c:J

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory$EventWrapper;-><init>(ILjava/lang/String;J)V

    move-object v3, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v2 .. v7}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory$EventWrapper;->saveCache(Landroid/content/Context;JJ)V

    return-void
.end method
