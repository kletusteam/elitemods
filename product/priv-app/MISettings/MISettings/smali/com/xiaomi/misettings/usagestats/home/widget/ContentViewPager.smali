.class public Lcom/xiaomi/misettings/usagestats/home/widget/ContentViewPager;
.super Lmiuix/viewpager/widget/ViewPager;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private ma:I

.field private na:Z

.field private oa:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lmiuix/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/H;->b()Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/ContentViewPager;->oa:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/ContentViewPager;->f()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lmiuix/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/H;->b()Z

    move-result p1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/ContentViewPager;->oa:Z

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/home/widget/ContentViewPager;->f()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "ContentViewPager"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private f()V
    .locals 1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/ContentViewPager;->ma:I

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const-string v0, "dispatchTouchEvent"

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/home/widget/ContentViewPager;->a(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/home/widget/ContentViewPager;->na:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-super {p0, p1}, Lmiuix/viewpager/widget/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    const-string p1, "onTouchListener"

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/home/widget/ContentViewPager;->a(Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    invoke-super {p0, p1}, Lmiuix/viewpager/widget/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    iput-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/home/widget/ContentViewPager;->na:Z

    return-void
.end method
