.class Lcom/xiaomi/misettings/usagestats/home/category/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroidx/recyclerview/widget/RecyclerView$j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/misettings/usagestats/home/category/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/home/category/k;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/home/category/k;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/MotionEvent;)V
    .locals 7
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/MotionEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->z:Landroidx/core/view/e;

    invoke-virtual {p1, p2}, Landroidx/core/view/e;->a(Landroid/view/MotionEvent;)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    const/4 v0, -0x1

    if-eq p1, v0, :cond_7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result p1

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget v1, v1, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    if-ltz v1, :cond_1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    invoke-virtual {v2, p1, p2, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(ILandroid/view/MotionEvent;I)V

    :cond_1
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    if-eqz v3, :cond_7

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq p1, v5, :cond_6

    const/4 v6, 0x2

    if-eq p1, v6, :cond_5

    const/4 v1, 0x3

    if-eq p1, v1, :cond_4

    const/4 v0, 0x6

    if-eq p1, v0, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget v1, v1, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    if-ne v0, v1, :cond_7

    if-nez p1, :cond_3

    move v4, v5

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget v1, v0, Lcom/xiaomi/misettings/usagestats/home/category/k;->o:I

    invoke-virtual {v0, p2, v1, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroid/view/MotionEvent;II)V

    goto :goto_1

    :cond_4
    iget-object p1, v2, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0

    :cond_5
    if-ltz v1, :cond_7

    iget p1, v2, Lcom/xiaomi/misettings/usagestats/home/category/k;->o:I

    invoke-virtual {v2, p2, p1, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroid/view/MotionEvent;II)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    invoke-virtual {p1, v3}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView$t;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object p2, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->s:Ljava/lang/Runnable;

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->s:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->invalidate()V

    goto :goto_1

    :cond_6
    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    const/4 p2, 0x0

    invoke-virtual {p1, p2, v4}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView$t;I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iput v0, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    :cond_7
    :goto_1
    return-void
.end method

.method public a(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView$t;I)V

    :cond_0
    return-void
.end method

.method public b(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/MotionEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->z:Landroidx/core/view/e;

    invoke-virtual {p1, p2}, Landroidx/core/view/e;->a(Landroid/view/MotionEvent;)Z

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    iput v2, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->d:F

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->e:F

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->b()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object v2, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    if-nez v2, :cond_3

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroid/view/MotionEvent;)Lcom/xiaomi/misettings/usagestats/home/category/k$c;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/k;->d:F

    iget v4, p1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->j:F

    sub-float/2addr v3, v4

    iput v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/k;->d:F

    iget v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/k;->e:F

    iget v4, p1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->k:F

    sub-float/2addr v3, v4

    iput v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/k;->e:F

    iget-object v3, p1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-virtual {v2, v3, v0}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView$t;Z)V

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object v2, v2, Lcom/xiaomi/misettings/usagestats/home/category/k;->a:Ljava/util/List;

    iget-object v3, p1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    iget-object v3, v3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object v3, v2, Lcom/xiaomi/misettings/usagestats/home/category/k;->m:Lcom/xiaomi/misettings/usagestats/home/category/k$a;

    iget-object v2, v2, Lcom/xiaomi/misettings/usagestats/home/category/k;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v4, p1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-virtual {v3, v2, v4}, Lcom/xiaomi/misettings/usagestats/home/category/k$a;->a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$t;)V

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object v3, p1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->e:Landroidx/recyclerview/widget/RecyclerView$t;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/k$c;->f:I

    invoke-virtual {v2, v3, p1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView$t;I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget v2, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->o:I

    invoke-virtual {p1, p2, v2, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroid/view/MotionEvent;II)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x3

    const/4 v3, -0x1

    if-eq p1, v2, :cond_2

    if-eq p1, v0, :cond_2

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget v2, v2, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    if-eq v2, v3, :cond_3

    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    if-ltz v2, :cond_3

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    invoke-virtual {v3, p1, p2, v2}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(ILandroid/view/MotionEvent;I)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iput v3, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->l:I

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v1}, Lcom/xiaomi/misettings/usagestats/home/category/k;->a(Landroidx/recyclerview/widget/RecyclerView$t;I)V

    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->t:Landroid/view/VelocityTracker;

    if-eqz p1, :cond_4

    invoke-virtual {p1, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_4
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/category/e;->a:Lcom/xiaomi/misettings/usagestats/home/category/k;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/home/category/k;->c:Landroidx/recyclerview/widget/RecyclerView$t;

    if-eqz p1, :cond_5

    goto :goto_1

    :cond_5
    move v0, v1

    :goto_1
    return v0
.end method
