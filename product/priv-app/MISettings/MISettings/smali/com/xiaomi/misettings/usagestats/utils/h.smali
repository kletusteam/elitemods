.class public Lcom/xiaomi/misettings/usagestats/utils/h;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/f;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/utils/f;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/h;->a:Ljava/util/HashMap;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/utils/g;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/utils/g;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/usagestats/utils/h;->b:Ljava/util/HashMap;

    return-void
.end method

.method public static a(Ljava/lang/String;I)I
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/h;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/xiaomi/misettings/usagestats/utils/h;->b:Ljava/util/HashMap;

    invoke-virtual {p1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0

    :cond_0
    const/4 p0, -0x1

    packed-switch p1, :pswitch_data_0

    return p0

    :pswitch_0
    const p0, 0x1af02

    return p0

    :pswitch_1
    const p0, 0x1aefe

    return p0

    :pswitch_2
    const p0, 0x1af00

    return p0

    :pswitch_3
    const p0, 0x1af01

    return p0

    :pswitch_4
    const p0, 0x1aeff

    :pswitch_5
    return p0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
