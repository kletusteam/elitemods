.class Lcom/xiaomi/misettings/usagestats/ui/z;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;Z)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/z;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/ui/z;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onCancel: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/ui/z;->a:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "showDialog"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean p1, p0, Lcom/xiaomi/misettings/usagestats/ui/z;->a:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/z;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->j(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/z;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->g(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)I

    move-result v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/ui/z;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->i(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)I

    move-result v0

    :goto_0
    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;I)I

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/z;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->a(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :goto_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/ui/z;->b:Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;->b(Lcom/xiaomi/misettings/usagestats/ui/NewAppUsageDetailFragment;)V

    return-void
.end method
