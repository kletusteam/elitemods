.class public Lcom/xiaomi/misettings/usagestats/h/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/h/a$a;
    }
.end annotation


# direct methods
.method private static a(Ljava/util/List;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/h/a$a;",
            ">;)J"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const-wide/16 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/h/a$a;

    iget-wide v2, v2, Lcom/xiaomi/misettings/usagestats/h/a$a;->b:J

    add-long/2addr v0, v2

    goto :goto_0

    :cond_0
    return-wide v0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-direct {v1, v4, v2, v3}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V

    const/4 v1, 0x0

    const-string v2, "default_category"

    invoke-static {p0, v2, v1}, Lcom/xiaomi/misettings/usagestats/provider/a;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {p0, v0, v2, v1}, Lcom/xiaomi/misettings/usagestats/h/a;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;ZZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;ZZ)Ljava/lang/String;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "Niel-DataHandler"

    if-eqz p2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0, p1, v2}, Lcom/xiaomi/misettings/usagestats/b/a/g;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;Ljava/util/List;)V

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {p0, v2, v0}, Lcom/xiaomi/misettings/usagestats/h/a;->b(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    const-string p1, "...... get by category."

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/usagestats/h/a;->a(Lcom/xiaomi/misettings/usagestats/f/g;Ljava/util/List;)V

    invoke-static {p0, v2, v0}, Lcom/xiaomi/misettings/usagestats/h/a;->a(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    const-string p1, "...... get by app."

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    if-eqz p3, :cond_1

    invoke-static {p0, v0, p2}, Lcom/xiaomi/misettings/usagestats/h/a;->b(Landroid/content/Context;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    invoke-static {p0, v0, p2}, Lcom/xiaomi/misettings/usagestats/h/a;->a(Landroid/content/Context;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;Z)Ljava/lang/String;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/h/a$a;",
            ">;Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-static/range {p1 .. p1}, Lcom/xiaomi/misettings/usagestats/h/a;->a(Ljava/util/List;)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    const-string v6, "Niel-DataHandler"

    if-gtz v5, :cond_0

    const-string v0, "Ops! Invalid data since sum is zero."

    invoke-static {v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ""

    return-object v0

    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v7, 0x3

    if-le v5, v7, :cond_1

    move v5, v7

    goto :goto_0

    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    :goto_0
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/xiaomi/misettings/usagestats/utils/m;->f(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v10

    const/4 v11, 0x0

    move-wide v13, v3

    const/4 v3, 0x0

    :goto_1
    const/high16 v4, 0x3f800000    # 1.0f

    if-ge v3, v5, :cond_2

    move-object/from16 v15, p1

    :try_start_0
    invoke-interface {v15, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v12, v16

    check-cast v12, Lcom/xiaomi/misettings/usagestats/h/a$a;

    move-object/from16 v17, v8

    iget-wide v7, v12, Lcom/xiaomi/misettings/usagestats/h/a$a;->b:J

    move/from16 v18, v5

    long-to-float v5, v7

    mul-float/2addr v5, v4

    long-to-float v4, v1

    div-float/2addr v5, v4

    add-float/2addr v11, v5

    add-long/2addr v13, v7

    iget-object v4, v12, Lcom/xiaomi/misettings/usagestats/h/a$a;->a:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v7, v8}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v8, v17

    invoke-static {v8, v4, v5, v7}, Lcom/xiaomi/misettings/usagestats/h/a;->a(Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    move/from16 v5, v18

    const/4 v7, 0x3

    goto :goto_1

    :cond_2
    move-object/from16 v15, p1

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v5, 0x3

    if-le v3, v5, :cond_4

    if-eqz p2, :cond_3

    const v3, 0x7f130077

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_3
    const v3, 0x7f130454

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_2
    sub-float/2addr v4, v11

    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    sub-long v11, v1, v13

    invoke-static {v0, v11, v12}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/h/a;->a(Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string v3, "stTotal"

    const v4, 0x7f13033e

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v5, v2

    invoke-virtual {v10, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "isDarkMode"

    invoke-static/range {p0 .. p0}, Lcom/xiaomi/misettings/f;->c(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v9, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "appVersion"

    invoke-static/range {p0 .. p0}, Lcom/misettings/common/utils/f;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "stData"

    invoke-virtual {v8}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    const-string v1, "format: "

    invoke-static {v6, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_3
    invoke-virtual {v9}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/h/a$a;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/e;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/h/a$a;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/e;->d()J

    move-result-wide v3

    invoke-direct {v1, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/h/a$a;-><init>(Ljava/lang/String;J)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static a(Lcom/xiaomi/misettings/usagestats/f/g;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/misettings/usagestats/f/g;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/e;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/d;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    goto :goto_0

    :cond_2
    new-instance v2, Lcom/xiaomi/misettings/usagestats/f/e;

    invoke-direct {v2}, Lcom/xiaomi/misettings/usagestats/f/e;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/xiaomi/misettings/usagestats/f/e;->b(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lcom/xiaomi/misettings/usagestats/f/e;->a(J)V

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-void
.end method

.method private static a(Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "stName"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "stPercent"

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "stUsageTime"

    invoke-virtual {v0, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string p1, "Niel-DataHandler"

    const-string p2, "createJSONObject: "

    invoke-static {p1, p2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/util/List;Z)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/h/a$a;",
            ">;Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/h/a;->a(Ljava/util/List;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p0, v0, v2

    const-string p2, "Niel-DataHandler"

    if-gtz p0, :cond_0

    const-string p0, "Ops! Invalid data since sum is zero."

    invoke-static {p2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string p0, ""

    return-object p0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p0

    const/4 v2, 0x5

    if-le p0, v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    new-instance p0, Lorg/json/JSONArray;

    invoke-direct {p0}, Lorg/json/JSONArray;-><init>()V

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_2

    :try_start_0
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/misettings/usagestats/h/a$a;

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    const-string v7, "name"

    iget-object v8, v5, Lcom/xiaomi/misettings/usagestats/h/a$a;->a:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "usageTime"

    iget-wide v8, v5, Lcom/xiaomi/misettings/usagestats/h/a$a;->b:J

    invoke-virtual {v6, v7, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    invoke-virtual {p0, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    const-string p1, "usageDetail"

    invoke-virtual {v3, p1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "totalTime"

    invoke-virtual {v3, p0, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p0

    const-string p1, "formatToNum: "

    invoke-static {p2, p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static b(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/h;",
            ">;",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/h/a$a;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/f/h;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/h/a$a;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/m;->f(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v2

    sget-object v3, Lcom/xiaomi/misettings/usagestats/b/a/g;->c:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/h;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/h;->f()J

    move-result-wide v3

    invoke-direct {v1, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/h/a$a;-><init>(Ljava/lang/String;J)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method
