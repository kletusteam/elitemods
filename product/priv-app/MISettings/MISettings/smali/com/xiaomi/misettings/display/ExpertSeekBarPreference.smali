.class public Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;
.super Lcom/xiaomi/misettings/widget/SeekBarPreference;

# interfaces
.implements Lcom/xiaomi/misettings/display/s;


# instance fields
.field private b:I

.field private c:Lmiuix/androidbasewidget/widget/SeekBar;

.field private d:Lcom/xiaomi/misettings/display/s;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/widget/SeekBarPreference;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->b()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/widget/SeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->b()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/misettings/widget/SeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->b()V

    return-void
.end method

.method private a(I)V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->b:I

    invoke-static {v0, v1, p1}, Lcom/xiaomi/misettings/display/a/c;->a(Landroid/content/Context;II)V

    return-void
.end method

.method private b()V
    .locals 1

    sget v0, Lcom/xiaomi/misettings/display/k;->miuix_preference_widget_seekbar:I

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setLayoutResource(I)V

    return-void
.end method

.method private d()Z
    .locals 2

    iget v0, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->b:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private e()V
    .locals 3

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/display/a/b;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/display/a/a;

    move-result-object v0

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    iget v2, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->b:I

    invoke-virtual {v0, v2}, Lcom/xiaomi/misettings/display/a/a;->a(I)I

    move-result v0

    sget v2, Lcom/xiaomi/misettings/display/a/a;->x:I

    sub-int/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    iget v2, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->b:I

    invoke-virtual {v0, v2}, Lcom/xiaomi/misettings/display/a/a;->a(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public a(ILcom/xiaomi/misettings/display/s;)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->b:I

    iput-object p2, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->d:Lcom/xiaomi/misettings/display/s;

    return-void
.end method

.method public c()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->e()V

    return-void
.end method

.method public onBindViewHolder(Landroidx/preference/B;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/xiaomi/misettings/widget/SeekBarPreference;->onBindViewHolder(Landroidx/preference/B;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/xiaomi/misettings/display/h;->miuix_preference_item_padding_start:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/xiaomi/misettings/display/h;->miuix_preference_item_padding_end:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v1, v3, v4, v0, v5}, Landroid/view/View;->setPaddingRelative(IIII)V

    sget v0, Lcom/xiaomi/misettings/display/j;->seekbar:I

    invoke-virtual {p1, v0}, Landroidx/preference/B;->b(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/androidbasewidget/widget/SeekBar;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Lmiuix/androidbasewidget/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getPaddingBottom()I

    move-result v1

    invoke-virtual {p1, v2, v0, v2, v1}, Landroid/widget/SeekBar;->setPaddingRelative(IIII)V

    :cond_0
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1a

    if-lt p1, v0, :cond_2

    iget p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->b:I

    invoke-static {p1}, Lcom/xiaomi/misettings/display/a/a;->b(I)Landroid/util/Range;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    invoke-virtual {p1}, Landroid/util/Range;->getLower()Ljava/lang/Comparable;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget v2, Lcom/xiaomi/misettings/display/a/a;->x:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMin(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    invoke-virtual {p1}, Landroid/util/Range;->getUpper()Ljava/lang/Comparable;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    sget v1, Lcom/xiaomi/misettings/display/a/a;->x:I

    sub-int/2addr p1, v1

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setMax(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    invoke-virtual {p1}, Landroid/util/Range;->getLower()Ljava/lang/Comparable;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMin(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    invoke-virtual {p1}, Landroid/util/Range;->getUpper()Ljava/lang/Comparable;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setMax(I)V

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->e()V

    :cond_2
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/xiaomi/misettings/widget/SeekBarPreference;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    iget-boolean p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->e:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->c:Lmiuix/androidbasewidget/widget/SeekBar;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->isAccessibilityFocused()Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->d()Z

    move-result p1

    if-eqz p1, :cond_1

    sget p1, Lcom/xiaomi/misettings/display/a/a;->x:I

    add-int/2addr p2, p1

    :cond_1
    invoke-direct {p0, p2}, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->a(I)V

    iget-boolean p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->e:Z

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->d:Lcom/xiaomi/misettings/display/s;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/xiaomi/misettings/display/s;->c()V

    :cond_2
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->e:Z

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->e:Z

    iget-object p1, p0, Lcom/xiaomi/misettings/display/ExpertSeekBarPreference;->d:Lcom/xiaomi/misettings/display/s;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/xiaomi/misettings/display/s;->c()V

    :cond_0
    return-void
.end method
