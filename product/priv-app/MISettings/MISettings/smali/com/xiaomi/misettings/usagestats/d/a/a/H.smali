.class public Lcom/xiaomi/misettings/usagestats/d/a/a/H;
.super Lcom/xiaomi/misettings/usagestats/d/a/a/s;


# instance fields
.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Lcom/xiaomi/misettings/usagestats/d/d/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const v0, 0x7f0e0087

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const p1, 0x7f0b0253

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->c:Landroid/view/View;

    const p1, 0x7f0b0254

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->d:Landroid/view/View;

    const p1, 0x7f0b0255

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->e:Landroid/view/View;

    const p1, 0x7f0b0256

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->b(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->f:Landroid/view/View;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->c:Landroid/view/View;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/a/a/i;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/i;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/H;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->c:Landroid/view/View;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/a/a/h;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/h;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/H;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->d:Landroid/view/View;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/a/a/k;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/k;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/H;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->e:Landroid/view/View;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/a/a/l;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/l;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/H;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->f:Landroid/view/View;

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/a/a/j;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/j;-><init>(Lcom/xiaomi/misettings/usagestats/d/a/a/H;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public synthetic a(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/miui/greenguard/manager/ExtraRouteManager;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V
    .locals 0

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->g:Lcom/xiaomi/misettings/usagestats/d/d/i;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->g:Lcom/xiaomi/misettings/usagestats/d/d/i;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/d/i;->b()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean;->getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public synthetic b(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/miui/greenguard/manager/ExtraRouteManager;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->g:Lcom/xiaomi/misettings/usagestats/d/d/i;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/d/d/i;->b()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean;->getUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public synthetic c(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/miui/greenguard/manager/ExtraRouteManager;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic d()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->f:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public synthetic d(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a/H;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/miui/greenguard/manager/ExtraRouteManager;->e(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
