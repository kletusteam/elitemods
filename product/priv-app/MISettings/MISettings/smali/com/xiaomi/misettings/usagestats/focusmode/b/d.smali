.class public Lcom/xiaomi/misettings/usagestats/focusmode/b/d;
.super Lcom/xiaomi/misettings/usagestats/d/b/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/xiaomi/misettings/usagestats/d/b/b<",
        "Lb/c/a/a/a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusHistoryHeaderView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 4

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/b/b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const p1, 0x7f0b01a0

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusHistoryHeaderView;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/d;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusHistoryHeaderView;

    invoke-static {}, Lcom/xiaomi/misettings/f;->a()Z

    move-result p1

    const/16 v0, 0x8

    const v1, 0x7f0b01b8

    const/4 v2, 0x0

    const v3, 0x7f0b00e3

    if-eqz p1, :cond_0

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V
    .locals 0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/b/d;->b:Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusHistoryHeaderView;

    check-cast p2, Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;

    invoke-virtual {p1, p2}, Lcom/xiaomi/misettings/usagestats/focusmode/widget/FocusHistoryHeaderView;->setLevelData(Lcom/xiaomi/misettings/usagestats/focusmode/model/FocusLevelData;)V

    return-void
.end method

.method public bridge synthetic a(Landroidx/recyclerview/widget/RecyclerView$a;Ljava/lang/Object;I)V
    .locals 0

    check-cast p2, Lb/c/a/a/a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/focusmode/b/d;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lb/c/a/a/a;I)V

    return-void
.end method
