.class public Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;
.super Lcom/xiaomi/misettings/usagestats/c/c;


# instance fields
.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Lmiuix/slidingwidget/widget/SlidingButton;

.field private h:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/c/c;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->g:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->b(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->g:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0, p1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->c(Z)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->a()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->d(Z)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->d(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->b(Z)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->c(Z)V

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    return-object p0
.end method

.method private c(Z)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    return-object p0
.end method

.method private d(Z)V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->d:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const v1, 0x7f130437

    goto :goto_0

    :cond_0
    const v1, 0x7f130434

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f130436

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f130433

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->e:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic e(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    return-object p0
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->g:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/devicelimit/b/a;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/a;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)V

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->h:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/devicelimit/b/b;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/b;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->c:Landroid/view/View;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/d;-><init>(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic g(Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method protected b()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    const v1, 0x7f0e0076

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b0163

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b0162

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->g:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b015f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b020f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b0161

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->a:Landroid/view/View;

    const v1, 0x7f0b0160

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/c/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/b;->b()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->a(Z)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/b/e;->e()V

    return-void
.end method
