.class public Lcom/xiaomi/misettings/usagestats/utils/A;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Ljava/lang/Runnable;

.field private c:Ljava/lang/Runnable;

.field private d:Ljava/lang/Runnable;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/A;-><init>(Ljava/lang/Runnable;Ljava/lang/Runnable;Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;Ljava/lang/Runnable;Ljava/lang/Runnable;Z)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->b:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->c:Ljava/lang/Runnable;

    iput-object p3, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->d:Ljava/lang/Runnable;

    new-instance v7, Lcom/xiaomi/misettings/usagestats/utils/z;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    move-object v0, v7

    move-object v1, p0

    move-object v3, p1

    move v4, p4

    move-object v5, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/xiaomi/misettings/usagestats/utils/z;-><init>(Lcom/xiaomi/misettings/usagestats/utils/A;Landroid/os/Looper;Ljava/lang/Runnable;ZLjava/lang/Runnable;Ljava/lang/Runnable;)V

    iput-object v7, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->a:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/utils/A;->c()V

    return-void
.end method

.method public b()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recover, mHasRecover: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", mHasRecycler: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BackgroundRecyclerManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->e:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->f:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->f:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->e:Z

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->a:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "remove msg recycler"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recycler,mHasRecycler: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BackgroundRecyclerManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->a:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->f:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->a:Landroid/os/Handler;

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    iput-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->f:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->e:Z

    :cond_1
    return-void
.end method

.method public d()V
    .locals 3

    const-string v0, "BackgroundRecyclerManager"

    const-string v1, "release"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->e:Z

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->f:Z

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_1
    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->a:Landroid/os/Handler;

    :cond_2
    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->b:Ljava/lang/Runnable;

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->c:Ljava/lang/Runnable;

    iput-object v2, p0, Lcom/xiaomi/misettings/usagestats/utils/A;->d:Ljava/lang/Runnable;

    return-void
.end method
