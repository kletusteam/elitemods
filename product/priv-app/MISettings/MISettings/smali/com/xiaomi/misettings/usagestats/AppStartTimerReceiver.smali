.class public Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->a:Z

    return-void
.end method

.method static synthetic a(Landroid/content/Context;)V
    .locals 3

    const-class v0, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->b(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->a(Landroid/content/Context;Ljava/lang/Class;ZZ)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;ZZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class<",
            "*>;ZZ)V"
        }
    .end annotation

    const-string v0, "AppStartTimerReceiver"

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, p0, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const/4 p1, 0x1

    if-eqz p2, :cond_1

    move v2, p1

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    :goto_0
    invoke-virtual {p0, v1, v2, p1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    if-nez p2, :cond_2

    if-eqz p3, :cond_2

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result p0

    invoke-static {p0}, Landroid/os/Process;->killProcess(I)V

    const/4 p0, 0x0

    invoke-static {p0}, Ljava/lang/System;->exit(I)V

    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "enableComponent: "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "enableComponent err:"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceive: action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppStartTimerReceiver"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/dataprovider/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-ge v0, v2, :cond_1

    return-void

    :cond_1
    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->e(Landroid/content/Context;)V

    return-void

    :cond_2
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->f(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object p2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/k;

    invoke-direct {v0, p0, p1}, Lcom/xiaomi/misettings/usagestats/k;-><init>(Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_3
    const-string v0, "android.intent.action.LOCKED_BOOT_COMPLETED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->f(Landroid/content/Context;)V

    goto :goto_0

    :cond_4
    const-string v0, "miui.android.intent.action.SCREEN_ON"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string p2, "handleIntent: SCREEN_ON"

    invoke-static {v1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/i/h;->C(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->e(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->d(Landroid/content/Context;)V

    goto :goto_0

    :cond_5
    const-string v0, "miui.android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_6

    const-string p2, "handleIntent: SCREEN_OFF"

    invoke-static {v1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/i/h;->E(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->d(Landroid/content/Context;)V

    :cond_6
    :goto_0
    return-void
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    return v2

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    return v2

    :cond_1
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->z(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/widget/desktop/service/UsageStatsUpdateService;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    return v2

    :cond_3
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/p;->f(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    return v2

    :cond_4
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/j;->j(Landroid/content/Context;)Z

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p0, :cond_5

    return v2

    :catch_0
    :cond_5
    return v0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/b;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private d(Landroid/content/Context;)V
    .locals 3

    const-string v0, "AppStartTimerReceiver"

    if-nez p1, :cond_0

    const-string p1, "context is null"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "renoir"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/xiaomi/misettings/display/b;->a(Landroid/content/Context;I)V

    const-string p1, "Anti has been closed on screen change"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private static e(Landroid/content/Context;)V
    .locals 3

    const-class v0, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->a(Landroid/content/Context;Ljava/lang/Class;ZZ)V

    return-void
.end method

.method private f(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tryRecoverFocusMode"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppStartTimerReceiver"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->a:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->i(Landroid/content/Context;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;->b(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/c;

    invoke-direct {v1, p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/c;-><init>(Lcom/xiaomi/misettings/usagestats/AppStartTimerReceiver;Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method
