.class public Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;
.super Lmiuix/appcompat/app/Fragment;

# interfaces
.implements Ljava/util/Observer;
.implements Lcom/xiaomi/misettings/display/RefreshRate/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment$a;
    }
.end annotation


# static fields
.field private static b:I = 0x1

.field private static c:I = 0x2

.field private static d:I = 0x3

.field private static e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private f:Landroid/content/Context;

.field private g:Landroid/view/View;

.field private h:Landroidx/recyclerview/widget/RecyclerView;

.field private i:Lmiuix/springback/view/SpringBackLayout;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/xiaomi/misettings/display/RefreshRate/p;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field

.field protected m:Landroid/view/View;

.field protected n:Lmiuix/view/f;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/display/RefreshRate/m;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcom/xiaomi/misettings/display/RefreshRate/l;

.field private s:Lmiuix/view/f$a;

.field private t:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->e:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/Fragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->j:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->o:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->p:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->q:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/misettings/display/RefreshRate/s;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/display/RefreshRate/s;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->s:Lmiuix/view/f$a;

    new-instance v0, Lcom/xiaomi/misettings/display/RefreshRate/t;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/display/RefreshRate/t;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->t:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->j:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Ljava/util/List;)Ljava/util/List;
    .locals 0

    sput-object p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->e:Ljava/util/List;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Lmiuix/view/f$a;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->s:Lmiuix/view/f$a;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    return-object p0
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;

    invoke-virtual {v0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->b()Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/AppSearchFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->q()V

    return-void
.end method

.method static synthetic d(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Lcom/xiaomi/misettings/display/RefreshRate/p;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->k:Lcom/xiaomi/misettings/display/RefreshRate/p;

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->p()V

    return-void
.end method

.method static synthetic f(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->r()V

    return-void
.end method

.method static synthetic g(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Landroid/text/TextWatcher;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->t:Landroid/text/TextWatcher;

    return-object p0
.end method

.method static synthetic h(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->f:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic i()I
    .locals 1

    sget v0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->d:I

    return v0
.end method

.method static synthetic i(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->j:Ljava/util/List;

    return-object p0
.end method

.method static synthetic j()I
    .locals 1

    sget v0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->c:I

    return v0
.end method

.method static synthetic j(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->q:Ljava/util/List;

    return-object p0
.end method

.method static synthetic k()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic k(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->p:Ljava/util/List;

    return-object p0
.end method

.method static synthetic l(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->o:Ljava/util/List;

    return-object p0
.end method

.method static synthetic m(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)Lcom/xiaomi/misettings/display/RefreshRate/l;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->r:Lcom/xiaomi/misettings/display/RefreshRate/l;

    return-object p0
.end method

.method private o()V
    .locals 1

    new-instance v0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment$a;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment$a;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private p()V
    .locals 7

    const-string v0, "HighRefreshOptionsFragment"

    const-string v1, " here is doAggregateAndSetAdapter "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/AppCompatActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/display/RefreshRate/e;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->r:Lcom/xiaomi/misettings/display/RefreshRate/l;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/display/RefreshRate/l;->a()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->j:Ljava/util/List;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->r:Lcom/xiaomi/misettings/display/RefreshRate/l;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/display/RefreshRate/l;->b()Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->e:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->q:Ljava/util/List;

    new-instance v4, Lcom/xiaomi/misettings/display/RefreshRate/m;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v5

    sget v6, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->d:I

    invoke-direct {v4, v5, v1, v3, v6}, Lcom/xiaomi/misettings/display/RefreshRate/m;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->j:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->p:Ljava/util/List;

    new-instance v3, Lcom/xiaomi/misettings/display/RefreshRate/m;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v4

    const/4 v5, 0x0

    sget v6, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->c:I

    invoke-direct {v3, v4, v1, v5, v6}, Lcom/xiaomi/misettings/display/RefreshRate/m;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->o:Ljava/util/List;

    new-instance v4, Lcom/xiaomi/misettings/display/RefreshRate/m;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v5

    sget v6, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->c:I

    invoke-direct {v4, v5, v1, v3, v6}, Lcom/xiaomi/misettings/display/RefreshRate/m;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->q()V

    new-instance v0, Lcom/xiaomi/misettings/display/RefreshRate/v;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/display/RefreshRate/v;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)V

    invoke-static {v0}, Lcom/misettings/common/utils/j;->a(Ljava/lang/Runnable;)V

    :cond_4
    :goto_1
    return-void
.end method

.method private q()V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->p:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->o:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    new-instance v1, Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->f:Landroid/content/Context;

    sget v3, Lcom/xiaomi/misettings/display/l;->opened_high_refresh_options:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->b:I

    invoke-direct {v1, v2, v3}, Lcom/xiaomi/misettings/display/RefreshRate/m;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    new-instance v1, Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->f:Landroid/content/Context;

    sget v3, Lcom/xiaomi/misettings/display/l;->closed_high_refresh_options:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->b:I

    invoke-direct {v1, v2, v3}, Lcom/xiaomi/misettings/display/RefreshRate/m;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    new-instance v1, Lcom/xiaomi/misettings/display/RefreshRate/m;

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->f:Landroid/content/Context;

    sget v3, Lcom/xiaomi/misettings/display/l;->follow_apps_settings:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->b:I

    invoke-direct {v1, v2, v3}, Lcom/xiaomi/misettings/display/RefreshRate/m;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->q:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->a(Ljava/util/List;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->o:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsActivity;->b(Ljava/util/List;)V

    return-void
.end method

.method private r()V
    .locals 3

    new-instance v0, Lcom/xiaomi/misettings/display/RefreshRate/p;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    invoke-direct {v0, v1, v2, p0}, Lcom/xiaomi/misettings/display/RefreshRate/p;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/xiaomi/misettings/display/RefreshRate/k;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->k:Lcom/xiaomi/misettings/display/RefreshRate/p;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->k:Lcom/xiaomi/misettings/display/RefreshRate/p;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemViewCacheSize(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getRecycledViewPool()Landroidx/recyclerview/widget/RecyclerView$l;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroidx/recyclerview/widget/RecyclerView$l;->a(II)V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    sget p3, Lcom/xiaomi/misettings/display/k;->layout_refresh_search:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/xiaomi/misettings/display/RefreshRate/m;Z)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->r:Lcom/xiaomi/misettings/display/RefreshRate/l;

    iget-object v2, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    iget-boolean v3, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->e:Z

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/misettings/display/RefreshRate/l;->a(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->o:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->p:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->o:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->p:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->r:Lcom/xiaomi/misettings/display/RefreshRate/l;

    iget-object v2, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, p2}, Lcom/xiaomi/misettings/display/RefreshRate/l;->a(Ljava/lang/String;I)V

    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->q()V

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p2

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/xiaomi/misettings/display/RefreshRate/u;

    invoke-direct {v2, p0, v0, p2}, Lcom/xiaomi/misettings/display/RefreshRate/u;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;II)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->f:Landroid/content/Context;

    iget-object v0, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->a:Ljava/lang/String;

    iget-boolean p1, p1, Lcom/xiaomi/misettings/display/RefreshRate/m;->e:Z

    const-string v1, "miui.intent.action.HIGH_REFRESH_SWITCH"

    invoke-static {p2, v1, v0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public a(Lmiuix/view/f$a;)V
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    check-cast p1, Lmiuix/view/f;

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->n:Lmiuix/view/f;

    :cond_0
    return-void
.end method

.method protected l()Landroidx/recyclerview/widget/RecyclerView$g;
    .locals 2

    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->k(I)V

    return-object v0
.end method

.method protected m()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected n()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/appcompat/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->f:Landroid/content/Context;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->f:Landroid/content/Context;

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/display/RefreshRate/B;->a()Lcom/xiaomi/misettings/display/RefreshRate/B;

    move-result-object p1

    invoke-virtual {p1, p0}, Ljava/util/Observable;->addObserver(Ljava/util/Observer;)V

    new-instance p1, Lcom/xiaomi/misettings/display/RefreshRate/l;

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->f:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/xiaomi/misettings/display/RefreshRate/l;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->r:Lcom/xiaomi/misettings/display/RefreshRate/l;

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    const-string v0, "HighRefreshOptionsFragment"

    const-string v1, " here is onStart "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->f()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroidx/appcompat/app/ActionBar;->i()V

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->o()V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onStop()V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    sget-object v0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const-string p2, "HighRefreshOptionsFragment"

    const-string v0, " here is onViewCreated "

    invoke-static {p2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget p2, Lcom/xiaomi/misettings/display/j;->id_list:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    sget p2, Lcom/xiaomi/misettings/display/j;->id_loading_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->g:Landroid/view/View;

    sget p2, Lcom/xiaomi/misettings/display/j;->id_spring_back:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/springback/view/SpringBackLayout;

    iput-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->i:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->n()V

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->l()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    iget-object p2, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->h:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    sget p2, Lcom/xiaomi/misettings/display/j;->header_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->m:Landroid/view/View;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->m:Landroid/view/View;

    const p2, 0x1020009

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    sget p2, Lcom/xiaomi/misettings/display/l;->search_app_tips:I

    invoke-virtual {p0, p2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->m:Landroid/view/View;

    new-instance p2, Lcom/xiaomi/misettings/display/RefreshRate/r;

    invoke-direct {p2, p0}, Lcom/xiaomi/misettings/display/RefreshRate/r;-><init>(Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/xiaomi/misettings/display/RefreshRate/HighRefreshOptionsFragment;->m:Landroid/view/View;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method
