.class public Lcom/xiaomi/misettings/usagestats/d/f/h;
.super Ljava/lang/Object;


# static fields
.field private static volatile a:Lcom/xiaomi/misettings/usagestats/d/f/h;


# instance fields
.field private b:Landroid/content/SharedPreferences;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "curve_line"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/f/h;->b:Landroid/content/SharedPreferences;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/d/f/h;
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/f/h;->a:Lcom/xiaomi/misettings/usagestats/d/f/h;

    if-nez v0, :cond_1

    const-class v0, Lcom/xiaomi/misettings/usagestats/d/f/h;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/d/f/h;->a:Lcom/xiaomi/misettings/usagestats/d/f/h;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/d/f/h;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/d/f/h;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/d/f/h;->a:Lcom/xiaomi/misettings/usagestats/d/f/h;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/d/f/h;->a:Lcom/xiaomi/misettings/usagestats/d/f/h;

    return-object p0
.end method


# virtual methods
.method public a()I
    .locals 3

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/f/h;->a:Lcom/xiaomi/misettings/usagestats/d/f/h;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/f/h;->b:Landroid/content/SharedPreferences;

    const-string v2, "current_index"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0

    :cond_0
    return v1
.end method

.method public a(I)V
    .locals 2

    sget-object v0, Lcom/xiaomi/misettings/usagestats/d/f/h;->a:Lcom/xiaomi/misettings/usagestats/d/f/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/f/h;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "current_index"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method
