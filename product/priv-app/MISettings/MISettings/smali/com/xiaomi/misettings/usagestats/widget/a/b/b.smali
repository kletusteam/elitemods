.class public Lcom/xiaomi/misettings/usagestats/widget/a/b/b;
.super Lcom/xiaomi/misettings/usagestats/widget/a/b/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/c;-><init>()V

    const-class v0, Lcom/xiaomi/misettings/usagestats/widget/desktop/widget/SimpleUsageStatsWidget;

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/widget/a/b/c;->b:Ljava/lang/Class;

    return-void
.end method

.method private a(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/misettings/usagestats/UsageStatsMainActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x0

    const/high16 v2, 0x4000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    return-object p1
.end method

.method private b(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    const v1, 0x7f0b03c3

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    const v1, 0x7f0b039d

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object p1

    const v0, 0x7f0b038e

    invoke-virtual {p2, v0, p1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-void
.end method

.method private c(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 5

    if-nez p2, :cond_0

    return-void

    :cond_0
    const-string v0, "UpdateWidgetHelper"

    const-string v1, "realUpdateWidget"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    invoke-virtual {p0, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/widget/RemoteViews;)V

    const v0, 0x7f0b039d

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->e()J

    move-result-wide v1

    invoke-static {p1, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    if-lez v0, :cond_1

    const v0, 0x7f130352

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->c()J

    move-result-wide v3

    invoke-static {p1, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f130457

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const v2, 0x7f0b038e

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {p2, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->c(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->f(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    return-void
.end method

.method private d(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    const v1, 0x7f0b038b

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v2, 0x7f1300ee

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v2, 0x7f0b039d

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const/16 v0, 0x8

    const v2, 0x7f0b038e

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v2, 0x7f0b0257

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v2, 0x7f0b025a

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->f(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    return-void
.end method

.method private e(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 3

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    const v1, 0x7f0b038b

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v2, 0x7f1300ef

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v1, 0x7f0b039d

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const/16 v0, 0x8

    const v1, 0x7f0b038e

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v1, 0x7f0b0257

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v1, 0x7f0b025a

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->f(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    return-void
.end method

.method private f(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 3

    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/c;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "this.getComponentName()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/c;->a()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UpdateWidgetHelper"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object p1

    invoke-virtual {p1, v0, p2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;I)Landroid/widget/RemoteViews;
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    invoke-direct {v0, p1, p2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto/32 :goto_3

    nop

    :goto_2
    new-instance v0, Landroid/widget/RemoteViews;

    goto/32 :goto_0

    nop

    :goto_3
    return-object v0
.end method

.method protected a(I)Lcom/xiaomi/misettings/usagestats/widget/a/a/a;
    .locals 3

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    if-ltz p1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;

    return-object p1

    :cond_1
    return-object v1
.end method

.method public a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;I)Landroid/widget/RemoteViews;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->d(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 8

    const v0, 0x7f0b039d

    const-string v1, "--"

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v1, 0x7f070109

    invoke-virtual {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result p1

    int-to-float p1, p1

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1, p1}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    const v3, 0x7f0b039d

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/xiaomi/misettings/usagestats/widget/a/a/a;)V
    .locals 4

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->a()I

    move-result v0

    const v1, 0x7f0b02bb

    const/16 v2, 0x64

    const/4 v3, 0x0

    invoke-virtual {p2, v1, v2, v0, v3}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->b()Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ic_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->b()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    :goto_0
    const p3, 0x7f0b023f

    invoke-static {p1}, Lcom/misettings/common/utils/d;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual {p2, p3, p1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V
    .locals 7

    const v0, 0x7f0700e7

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v4

    if-eqz p3, :cond_0

    const p3, 0x7f0700cc

    goto :goto_0

    :cond_0
    const p3, 0x7f0700d1

    :goto_0
    invoke-virtual {p0, p1, p3}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v6

    const v2, 0x7f0b039d

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v1 .. v6}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    return-void
.end method

.method protected a(Landroid/widget/RemoteViews;)V
    .locals 2

    const v0, 0x7f0b038b

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const/4 v0, 0x0

    const v1, 0x7f0b039d

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v1, 0x7f0b0257

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    return-void
.end method

.method protected b()I
    .locals 1

    const v0, 0x7f0e003f

    return v0
.end method

.method protected b(Landroid/content/Context;I)I
    .locals 0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    return p1
.end method

.method public b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;I)Landroid/widget/RemoteViews;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->e(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    return-void
.end method

.method protected b(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/xiaomi/misettings/usagestats/widget/a/a/a;)V
    .locals 2

    const v0, 0x7f0b023b

    if-nez p3, :cond_0

    const/4 p1, 0x4

    invoke-virtual {p2, v0, p1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/xiaomi/misettings/usagestats/widget/a/a/a;)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->c(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/xiaomi/misettings/usagestats/widget/a/a/a;)V

    return-void
.end method

.method protected b(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x0

    if-eqz p3, :cond_0

    const v3, 0x7f070108

    invoke-virtual {v0, v1, v3}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v3

    move v9, v3

    goto :goto_0

    :cond_0
    move v9, v2

    :goto_0
    const v5, 0x7f0b038e

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v4 .. v9}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    const v11, 0x7f0b039d

    const/4 v12, 0x0

    const v3, 0x7f0700db

    if-eqz p3, :cond_1

    move v4, v3

    goto :goto_1

    :cond_1
    const v4, 0x7f0700e7

    :goto_1
    invoke-virtual {v0, v1, v4}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v13

    const/4 v14, 0x0

    if-eqz p3, :cond_2

    move v15, v2

    goto :goto_2

    :cond_2
    invoke-virtual {v0, v1, v3}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v3

    move v15, v3

    :goto_2
    move-object/from16 v10, p2

    invoke-virtual/range {v10 .. v15}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    const v3, 0x7f0b039d

    if-eqz p3, :cond_3

    const v4, 0x7f0700fd

    goto :goto_3

    :cond_3
    const v4, 0x7f070102

    :goto_3
    invoke-virtual {v0, v1, v4}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;I)I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v4, p2

    invoke-virtual {v4, v3, v2, v1}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    return-void
.end method

.method public c(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(Landroid/content/Context;I)Landroid/widget/RemoteViews;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->c(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    return-void
.end method

.method protected c(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/xiaomi/misettings/usagestats/widget/a/a/a;)V
    .locals 2

    invoke-virtual {p3}, Lcom/xiaomi/misettings/usagestats/widget/a/a/a;->d()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    const p3, 0x7f0b039d

    invoke-virtual {p2, p3, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    return-void
.end method

.method protected c(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V
    .locals 9

    const v0, 0x7f0b0258

    invoke-virtual {p2, v0}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    const v1, 0x7f0b0259

    invoke-virtual {p2, v1}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/widget/a/c/d;->d()Ljava/util/List;

    move-result-object v2

    const/16 v3, 0x8

    const/4 v4, 0x0

    if-eqz v2, :cond_7

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto/16 :goto_4

    :cond_0
    const v2, 0x7f0b025d

    if-eqz p3, :cond_1

    move v5, v4

    goto :goto_0

    :cond_1
    move v5, v3

    :goto_0
    invoke-virtual {p2, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    move v2, v4

    :goto_1
    const v5, 0x7f0e0041

    const v6, 0x7f0e003a

    const/4 v7, 0x2

    if-ge v2, v7, :cond_3

    new-instance v7, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2, v0, v7}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(I)Lcom/xiaomi/misettings/usagestats/widget/a/a/a;

    move-result-object v6

    invoke-virtual {p0, p1, v7, v6}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/xiaomi/misettings/usagestats/widget/a/a/a;)V

    if-nez v2, :cond_2

    new-instance v6, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2, v0, v6}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    const v0, 0x7f0b025e

    if-eqz p3, :cond_4

    goto :goto_2

    :cond_4
    move v3, v4

    :goto_2
    invoke-virtual {p2, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    move p3, v7

    :goto_3
    const/4 v0, 0x4

    if-ge p3, v0, :cond_6

    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    invoke-virtual {p0, p3}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->a(I)Lcom/xiaomi/misettings/usagestats/widget/a/a/a;

    move-result-object v2

    invoke-virtual {p0, p1, v0, v2}, Lcom/xiaomi/misettings/usagestats/widget/a/b/b;->b(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/xiaomi/misettings/usagestats/widget/a/a/a;)V

    if-ne p3, v7, :cond_5

    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    :cond_5
    add-int/lit8 p3, p3, 0x1

    goto :goto_3

    :cond_6
    return-void

    :cond_7
    :goto_4
    const p1, 0x7f0b025a

    invoke-virtual {p2, p1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const p1, 0x7f0b0257

    invoke-virtual {p2, p1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    return-void
.end method
