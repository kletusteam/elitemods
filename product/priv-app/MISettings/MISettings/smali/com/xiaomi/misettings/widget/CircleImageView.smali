.class public Lcom/xiaomi/misettings/widget/CircleImageView;
.super Landroidx/appcompat/widget/AppCompatImageView;


# instance fields
.field private c:I

.field private d:Landroid/graphics/Path;

.field private e:Landroid/graphics/RectF;

.field private f:Landroid/view/ViewOutlineProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/misettings/widget/CircleImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/misettings/widget/CircleImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/widget/CircleImageView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/widget/CircleImageView;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->c:I

    return p0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-le v1, v0, :cond_0

    iget v0, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->c:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->d:Landroid/graphics/Path;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->draw(Landroid/graphics/Canvas;)V

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_1
    return-void
.end method

.method public setDefaultRadius()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41200000    # 10.0f

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->c:I

    iget v0, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->c:I

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/widget/CircleImageView;->setmRadius(I)V

    return-void
.end method

.method public setHasRadius(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object p1

    const/high16 v0, 0x41200000    # 10.0f

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/f;->a(Landroid/content/Context;F)I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->c:I

    iget p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->c:I

    invoke-virtual {p0, p1}, Lcom/xiaomi/misettings/widget/CircleImageView;->setmRadius(I)V

    return-void
.end method

.method public setmRadius(I)V
    .locals 6

    iget v0, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->c:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iput p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->c:I

    iget-object p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->d:Landroid/graphics/Path;

    if-nez p1, :cond_1

    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->d:Landroid/graphics/Path;

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->e:Landroid/graphics/RectF;

    if-nez p1, :cond_2

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->e:Landroid/graphics/RectF;

    :cond_2
    iget p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->c:I

    const/16 v3, 0x15

    if-eqz p1, :cond_5

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v3, p1, :cond_4

    iget-object p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->f:Landroid/view/ViewOutlineProvider;

    if-nez p1, :cond_3

    new-instance p1, Lcom/xiaomi/misettings/widget/a;

    invoke-direct {p1, p0}, Lcom/xiaomi/misettings/widget/a;-><init>(Lcom/xiaomi/misettings/widget/CircleImageView;)V

    iput-object p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->f:Landroid/view/ViewOutlineProvider;

    :cond_3
    iget-object p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->f:Landroid/view/ViewOutlineProvider;

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    :cond_4
    iget-object p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->e:Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/4 v4, 0x0

    invoke-virtual {p1, v4, v4, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->d:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    iget-object p1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->d:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->e:Landroid/graphics/RectF;

    iget v2, p0, Lcom/xiaomi/misettings/widget/CircleImageView;->c:I

    int-to-float v4, v2

    int-to-float v2, v2

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {p1, v1, v4, v2, v5}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    goto :goto_1

    :cond_5
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v3, p1, :cond_6

    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    :cond_6
    :goto_1
    if-eqz v0, :cond_7

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v3, p1, :cond_7

    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidateOutline()V

    :cond_7
    return-void
.end method
