.class public Lcom/xiaomi/misettings/usagestats/home/database/appname/f;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/xiaomi/misettings/usagestats/home/database/appname/f;


# instance fields
.field private final b:Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/Application;->c()Landroid/content/Context;

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-class v0, Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;

    const-string v1, "app_name"

    invoke-static {p1, v0, v1}, Landroidx/room/u;->a(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;)Landroidx/room/v$a;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/room/v$a;->a()Landroidx/room/v$a;

    invoke-virtual {p1}, Landroidx/room/v$a;->b()Landroidx/room/v;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->b:Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->b:Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;

    :goto_0
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/home/database/appname/f;
    .locals 2

    const-class v0, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->a:Lcom/xiaomi/misettings/usagestats/home/database/appname/f;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;

    invoke-direct {v1, p0}, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->a:Lcom/xiaomi/misettings/usagestats/home/database/appname/f;

    :cond_0
    sget-object p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->a:Lcom/xiaomi/misettings/usagestats/home/database/appname/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static b()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->a:Lcom/xiaomi/misettings/usagestats/home/database/appname/f;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->b:Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;->l()Lcom/xiaomi/misettings/usagestats/home/database/appname/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/database/appname/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    return-object v1
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->b:Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0

    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;->l()Lcom/xiaomi/misettings/usagestats/home/database/appname/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/xiaomi/misettings/usagestats/home/database/appname/b;->a()Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public varargs a([Lcom/xiaomi/misettings/usagestats/home/database/appname/a;)[Ljava/lang/Long;
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/home/database/appname/f;->b:Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    new-array p1, v1, [Ljava/lang/Long;

    return-object p1

    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/home/database/appname/AppNameManagerDatabase;->l()Lcom/xiaomi/misettings/usagestats/home/database/appname/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/xiaomi/misettings/usagestats/home/database/appname/b;->a([Lcom/xiaomi/misettings/usagestats/home/database/appname/a;)[Ljava/lang/Long;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    new-array p1, v1, [Ljava/lang/Long;

    return-object p1
.end method
