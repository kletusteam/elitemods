.class Lcom/xiaomi/misettings/usagestats/focusmode/G;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->p()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/u;->a(Ljava/lang/String;)Z

    move-result p1

    const-string v0, ""

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/c/q;->m(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f130181

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->a(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;Ljava/lang/String;)V

    :goto_0
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->b(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->c(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/TextView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Lmiuix/androidbasewidget/widget/StateEditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->e(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Lmiuix/appcompat/app/j;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->e(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Lmiuix/appcompat/app/j;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->dismiss()V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->c(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/focusmode/G;->a:Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;->d(Lcom/xiaomi/misettings/usagestats/focusmode/FocusSettingsFragment;)Lmiuix/androidbasewidget/widget/StateEditText;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_1
    return-void
.end method
