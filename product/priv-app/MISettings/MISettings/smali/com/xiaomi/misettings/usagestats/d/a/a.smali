.class public Lcom/xiaomi/misettings/usagestats/d/a/a;
.super Landroidx/recyclerview/widget/RecyclerView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/misettings/usagestats/d/a/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/xiaomi/misettings/usagestats/d/a/a/s;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:I

.field private c:I

.field private d:Landroidx/recyclerview/widget/RecyclerView;

.field private e:Lcom/xiaomi/misettings/usagestats/d/a/b;

.field private f:Lcom/miui/greenguard/entity/FamilyBean;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/content/Context;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/a/a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    return-void
.end method

.method private a(ILjava/util/List;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget v2, v2, Lcom/xiaomi/misettings/usagestats/d/d/i;->a:I

    if-ne v2, p1, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    return-object p1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-object v0
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/f/g;I)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/b;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->c:I

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    return-void
.end method

.method private b(Lcom/xiaomi/misettings/usagestats/f/g;)V
    .locals 4

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_4

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v2

    if-eqz v2, :cond_3

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-direct {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Lcom/xiaomi/misettings/usagestats/f/g;I)V

    goto :goto_1

    :cond_2
    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->c(I)V

    invoke-direct {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->b(Lcom/xiaomi/misettings/usagestats/f/g;I)V

    goto :goto_1

    :cond_3
    invoke-direct {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->c(Lcom/xiaomi/misettings/usagestats/f/g;I)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method private b(Lcom/xiaomi/misettings/usagestats/f/g;I)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/d;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/d$a;

    iget-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->d:Ljava/util/List;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->c:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/d$a;->c:Ljava/util/List;

    iget v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->c:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/xiaomi/misettings/usagestats/b/a/g;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;Ljava/util/List;)V

    invoke-virtual {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/d/d/i;

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    iget v2, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->a:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-direct {p0, v3, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(ILjava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/d/d/f$a;

    iget-object v3, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v3, Lcom/xiaomi/misettings/usagestats/d/d/f$a;

    iget-boolean v3, v3, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->a:Z

    iput-boolean v3, v2, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->a:Z

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v1, Lcom/xiaomi/misettings/usagestats/d/d/f$a;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->b:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    iput-object v1, v2, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->b:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    goto :goto_1

    :cond_1
    iget v2, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->a:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    invoke-direct {p0, v3, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(ILjava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget-object v3, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v3, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget-boolean v3, v3, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    iput-boolean v3, v2, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget-object v1, v1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->d:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    iput-object v1, v2, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->d:Lcom/xiaomi/misettings/usagestats/d/d/j$b;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "HomeContentAdapter"

    const-string v1, "adapterResumeStatus error"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    return-void
.end method

.method private c(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget-object p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    instance-of v0, p1, Lcom/xiaomi/misettings/usagestats/d/d/a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/d/a;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/xiaomi/misettings/usagestats/d/d/a;->a:Z

    :cond_1
    return-void
.end method

.method private c(Lcom/xiaomi/misettings/usagestats/f/g;I)V
    .locals 10

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    instance-of v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/c;

    if-nez v1, :cond_0

    return-void

    :cond_0
    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/c;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/c;->h:Z

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/misettings/usagestats/f/m;

    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/m;->h()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/misettings/usagestats/f/k;

    invoke-virtual {v5}, Lcom/xiaomi/misettings/usagestats/f/k;->d()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v6

    iget-wide v6, v6, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v8

    iget-wide v8, v8, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_1

    const-string v4, "HomeContentAdapter"

    const-string v6, "replaceTodayData: "

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-virtual {v5, v4, p1}, Lcom/xiaomi/misettings/usagestats/f/k;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V

    :cond_2
    invoke-virtual {v2}, Lcom/xiaomi/misettings/usagestats/f/m;->g()Ljava/util/List;

    move-result-object v4

    move v5, v1

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_4

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v6

    iget-wide v6, v6, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-virtual {v8}, Lcom/xiaomi/misettings/usagestats/f/g;->b()Lcom/xiaomi/misettings/usagestats/f/j;

    move-result-object v8

    iget-wide v8, v8, Lcom/xiaomi/misettings/usagestats/f/j;->a:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_3

    move v1, v5

    goto :goto_1

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_4
    :goto_1
    invoke-interface {v4, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-virtual {v2, p1, v3}, Lcom/xiaomi/misettings/usagestats/f/m;->a(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Ljava/util/List;)V

    return-void
.end method

.method private d(I)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_1

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->c(I)V

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private e(I)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_1

    invoke-virtual {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    const/16 v2, 0x9

    if-eq v1, v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-direct {p0, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->c(I)V

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    return-void
.end method

.method public a(I)V
    .locals 2

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->b:I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result p1

    if-lez p1, :cond_1

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->d(I)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->e(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->d:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;II)V
    .locals 1

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->i:Ljava/util/List;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->i:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/a/a$a;

    invoke-interface {v0, p2, p3}, Lcom/xiaomi/misettings/usagestats/d/a/a$a;->a(II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lcom/miui/greenguard/entity/FamilyBean;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->f:Lcom/miui/greenguard/entity/FamilyBean;

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/d/a/a$a;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->i:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->i:Ljava/util/List;

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/d/a/a/s;I)V
    .locals 2
    .param p1    # Lcom/xiaomi/misettings/usagestats/d/a/a/s;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->a:I

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->c(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget v1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->b:I

    invoke-virtual {p1, p0, v0, p2, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a/s;->a(Landroidx/recyclerview/widget/RecyclerView$a;Lcom/xiaomi/misettings/usagestats/d/d/i;II)V

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/d/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->e:Lcom/xiaomi/misettings/usagestats/d/a/b;

    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/d/d/j$a;)V
    .locals 4

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/j;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget-boolean v2, p1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    iput-boolean v2, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    iget v2, p1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->a:I

    iput v2, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->a:I

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->b:I

    iput p1, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->b:I

    invoke-virtual {p0, v1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method public a(Lcom/xiaomi/misettings/usagestats/f/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->b(Lcom/xiaomi/misettings/usagestats/f/g;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/f/m;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->c(I)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/d/f/b;->a(Ljava/util/List;)Lcom/xiaomi/misettings/usagestats/d/d/i;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    const-string p1, "HomeContentAdapter"

    const-string v0, "notifyWeekDetailList: "

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method public a(Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;I)V"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a;->b(Ljava/util/List;I)V

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    return-void
.end method

.method public a(Z)V
    .locals 4

    const-string v0, "HomeContentAdapter"

    const-string v1, "notifyDeviceLimitItem"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/f;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;

    iput-boolean p1, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->a:Z

    invoke-virtual {p0, v1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->a:I

    return-void
.end method

.method public b(Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/d/d/i;",
            ">;I)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->b(Ljava/util/List;)V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->b:I

    iput p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->c:I

    :cond_1
    :goto_0
    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/d/f/i;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    return-void
.end method

.method public d()Lcom/miui/greenguard/entity/FamilyBean;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->f:Lcom/miui/greenguard/entity/FamilyBean;

    return-object v0
.end method

.method public e()Lcom/xiaomi/misettings/usagestats/d/a/b;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->e:Lcom/xiaomi/misettings/usagestats/d/a/b;

    return-object v0
.end method

.method public f()V
    .locals 4

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    const/16 v3, 0x8

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-direct {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->c(I)V

    invoke-virtual {p0, v1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public g()V
    .locals 6

    const-string v0, "HomeContentAdapter"

    const-string v1, "notifyDeviceLimitItem"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_3

    invoke-virtual {p0, v2}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/f;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->a:Z

    iget-object v5, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v4

    iput v4, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->d:I

    iget-object v4, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v1

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->c:I

    if-eqz v3, :cond_1

    sget-object v1, Lcom/xiaomi/misettings/usagestats/d/d/f$b;->b:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->b:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/xiaomi/misettings/usagestats/d/d/f$b;->c:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    iput-object v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/f$a;->b:Lcom/xiaomi/misettings/usagestats/d/d/f$b;

    :goto_1
    invoke-virtual {p0, v2}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    goto :goto_2

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    :goto_2
    return-void
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/d/i;

    iget p1, p1, Lcom/xiaomi/misettings/usagestats/d/d/i;->a:I

    return p1
.end method

.method public h()V
    .locals 4

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_3

    invoke-virtual {p0, v1}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemViewType(I)I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/j;

    iget-object v0, v0, Lcom/xiaomi/misettings/usagestats/d/d/i;->e:Ljava/lang/Object;

    check-cast v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    iget-object v2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/misettings/usagestats/i/h;->m(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    iget-object v3, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/i/h;->h(Landroid/content/Context;)I

    move-result v3

    iput v3, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->a:I

    if-eqz v2, :cond_1

    sget-object v2, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->a:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    iput-object v2, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->d:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    goto :goto_1

    :cond_1
    sget-object v2, Lcom/xiaomi/misettings/usagestats/d/d/j$b;->b:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    iput-object v2, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->d:Lcom/xiaomi/misettings/usagestats/d/d/j$b;

    :goto_1
    invoke-virtual {p0, v1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    :goto_2
    return-void
.end method

.method public i()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->d:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/d/a/a;->getItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/xiaomi/misettings/usagestats/d/a/a/s;

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a;->a(Lcom/xiaomi/misettings/usagestats/d/a/a/s;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$t;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/d/a/a/s;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/xiaomi/misettings/usagestats/d/a/a/s;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const p1, 0x7f0e0065

    const v0, 0x7f0e0066

    const/4 v1, 0x0

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    new-instance p1, Lcom/xiaomi/misettings/usagestats/d/a/a/K;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    const v0, 0x7f0e0155

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/K;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto/16 :goto_1

    :pswitch_1
    new-instance p1, Lcom/xiaomi/misettings/usagestats/d/a/a/I;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-direct {p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/I;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    :pswitch_2
    new-instance p1, Lcom/xiaomi/misettings/usagestats/d/a/a/H;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-direct {p1, p2}, Lcom/xiaomi/misettings/usagestats/d/a/a/H;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    :pswitch_3
    new-instance p2, Lcom/xiaomi/misettings/usagestats/d/a/a/F;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-static {v0, p1, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/F;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    :pswitch_4
    new-instance p1, Lcom/xiaomi/misettings/usagestats/d/a/a/B;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/B;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_1

    :pswitch_5
    new-instance p1, Lcom/xiaomi/misettings/usagestats/d/a/a/G;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    const v0, 0x7f0e0069

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/G;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_1

    :pswitch_6
    new-instance p1, Lcom/xiaomi/misettings/usagestats/d/a/a/t;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    const v0, 0x7f0e0064

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/t;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_1

    :pswitch_7
    new-instance p1, Lcom/xiaomi/misettings/usagestats/d/a/a/J;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    const v0, 0x7f0e006c

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/J;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_1

    :pswitch_8
    new-instance p2, Lcom/xiaomi/misettings/usagestats/d/a/a/E;

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-static {v0, p1, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/xiaomi/misettings/usagestats/d/a/a/E;-><init>(Landroid/content/Context;Landroid/view/View;)V

    :goto_0
    move-object p1, p2

    goto :goto_1

    :pswitch_9
    new-instance p1, Lcom/xiaomi/misettings/usagestats/d/a/a/y;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/y;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_1

    :pswitch_a
    new-instance p1, Lcom/xiaomi/misettings/usagestats/d/a/a/D;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    const v0, 0x7f0e0068

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/D;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_1

    :pswitch_b
    new-instance p1, Lcom/xiaomi/misettings/usagestats/d/a/a/u;

    iget-object p2, p0, Lcom/xiaomi/misettings/usagestats/d/a/a;->h:Landroid/content/Context;

    const v0, 0x7f0e006d

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/xiaomi/misettings/usagestats/d/a/a/u;-><init>(Landroid/content/Context;Landroid/view/View;)V

    :goto_1
    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
