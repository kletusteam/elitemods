.class public Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;
.super Landroid/app/Service;


# static fields
.field public static a:I


# instance fields
.field private b:Landroid/os/CountDownTimer;

.field private c:Ljava/lang/String;

.field private d:Lmiui/process/IForegroundInfoListener$Stub;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/xiaomi/misettings/usagestats/service/h;

    invoke-direct {v0, p0}, Lcom/xiaomi/misettings/usagestats/service/h;-><init>(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;)V

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->d:Lmiui/process/IForegroundInfoListener$Stub;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->e:Z

    return-void
.end method

.method private a(J)Landroid/app/Notification;
    .locals 4
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x1a
    .end annotation

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.android.settings.steady_on_screen"

    invoke-direct {v0, v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1302ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/L;->b(J)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    aput-object p1, v3, p2

    const p1, 0x7f1302f0

    invoke-virtual {v1, p1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    const p1, 0x7f080186

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    return-object p1
.end method

.method public static a(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRemainTime = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v2, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BizSvr_steady_service"

    invoke-static {v2, v1}, Lmiui/util/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    const/high16 v2, 0x4000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->c:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->b(J)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/i/h;->e(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/i/h;->d(Landroid/content/Context;Z)V

    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;)Landroid/os/CountDownTimer;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->b:Landroid/os/CountDownTimer;

    return-object p0
.end method

.method private b(J)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    const v0, 0x1af68

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a(J)Landroid/app/Notification;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    :cond_0
    return-void
.end method

.method private static b(Landroid/content/Context;)V
    .locals 1

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/NotificationManager;

    if-eqz p0, :cond_0

    const v0, 0x1af68

    invoke-virtual {p0, v0}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->m(Landroid/content/Context;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enable:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, "/isStartSteadyOn:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "BizSvr_steady_service"

    invoke-static {v1, p1}, Lmiui/util/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->i(Landroid/content/Context;)I

    move-result p1

    sput p1, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a:I

    invoke-direct {p0}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->c()V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    sget v0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a:I

    int-to-long v0, v0

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/i/h;->a(Landroid/content/Context;J)V

    :goto_0
    return-void
.end method

.method private c()V
    .locals 5

    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/app/Service;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->d(Landroid/content/Context;)Landroid/app/NotificationChannelGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannelGroup(Landroid/app/NotificationChannelGroup;)V

    new-instance v1, Landroid/app/NotificationChannel;

    const v2, 0x7f130031

    invoke-virtual {p0, v2}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const-string v4, "com.android.settings.steady_on_screen"

    invoke-direct {v1, v4, v2, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v2}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->enableLights(Z)V

    const-string v2, "app_timer"

    invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->setGroup(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->b:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->b:Landroid/os/CountDownTimer;

    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->b:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    :cond_0
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->e:Z

    const-string v0, "BizSvr_steady_service"

    const-string v1, "SteadyOnService ===onCreate"

    invoke-static {v0, v1}, Lmiui/util/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->f(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->y(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "BizSvr_steady_service"

    const-string v1, "onDestroy = "

    invoke-static {v0, v1}, Lmiui/util/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->b(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a()V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/i/h;->d(Landroid/content/Context;Z)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    if-eqz p1, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "intent.getAction()"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "BizSvr_steady_service"

    invoke-static {p3, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->e:Z

    const/4 p3, 0x1

    if-eqz p2, :cond_2

    const/4 p2, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "action_steady_on"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    move p1, p3

    goto :goto_0

    :cond_1
    move p1, p2

    :goto_0
    invoke-direct {p0, p1}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->b(Z)V

    iput-boolean p2, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->e:Z

    :cond_2
    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/i/h;->m(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_3

    invoke-direct {p0, p3}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a(Z)V

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    return p3

    :cond_3
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->i(Landroid/content/Context;)I

    move-result p1

    sput p1, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a:I

    sget p1, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a:I

    if-gtz p1, :cond_4

    invoke-direct {p0, p3}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a(Z)V

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    return p3

    :cond_4
    int-to-long p1, p1

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->b(J)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a()V

    new-instance p1, Lcom/xiaomi/misettings/usagestats/service/i;

    sget p2, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->a:I

    mul-int/lit16 p2, p2, 0x3e8

    int-to-long v2, p2

    const-wide/16 v4, 0x3e8

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/misettings/usagestats/service/i;-><init>(Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;JJ)V

    iput-object p1, p0, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->b:Landroid/os/CountDownTimer;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/service/SteadyOnService;->b()V

    return p3
.end method
