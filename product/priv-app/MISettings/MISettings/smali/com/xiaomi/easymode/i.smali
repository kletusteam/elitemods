.class Lcom/xiaomi/easymode/i;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/easymode/EasyModeFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/easymode/EasyModeFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/easymode/EasyModeFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object p1, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {p1}, Lcom/xiaomi/easymode/EasyModeFragment;->i()Z

    move-result p1

    const-string v0, "elderly_mode"

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object p1, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {p1, v1}, Lcom/xiaomi/easymode/EasyModeFragment;->b(Z)Landroid/content/Intent;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {v0, v1}, Lcom/xiaomi/easymode/EasyModeFragment;->c(Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Landroid/content/res/MiuiConfiguration;->getScaleMode()I

    move-result v2

    const-string v3, "previous_font"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object p1, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {p1, v1}, Lcom/xiaomi/easymode/EasyModeFragment;->b(Z)Landroid/content/Intent;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {v0, v1}, Lcom/xiaomi/easymode/EasyModeFragment;->c(Z)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, -0x2

    invoke-static {v2}, Lb/c/a/b/b/a;->a(I)Landroid/os/UserHandle;

    move-result-object v3

    invoke-virtual {v1, p1, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    iget-object p1, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v2}, Lb/c/a/b/b/a;->a(I)Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    iget-object p1, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {p1}, Lcom/xiaomi/easymode/EasyModeFragment;->g(Lcom/xiaomi/easymode/EasyModeFragment;)Landroid/content/Intent;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {v0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    iget-object p1, p0, Lcom/xiaomi/easymode/i;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-virtual {p1}, Lmiuix/appcompat/app/Fragment;->g()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void
.end method
