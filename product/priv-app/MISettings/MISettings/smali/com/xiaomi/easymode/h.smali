.class Lcom/xiaomi/easymode/h;
.super Ljava/lang/Object;

# interfaces
.implements Landroidx/viewpager/widget/ViewPager$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/easymode/EasyModeFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/easymode/EasyModeFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/easymode/EasyModeFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 2

    iget-object p3, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {p3}, Lcom/xiaomi/easymode/EasyModeFragment;->a(Lcom/xiaomi/easymode/EasyModeFragment;)I

    move-result p3

    if-ne p1, p3, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object p3, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {p3}, Lcom/xiaomi/easymode/EasyModeFragment;->b(Lcom/xiaomi/easymode/EasyModeFragment;)Landroid/widget/TextView;

    move-result-object p3

    const/high16 v0, 0x3f800000    # 1.0f

    if-eqz p1, :cond_1

    sub-float v1, v0, p2

    goto :goto_1

    :cond_1
    move v1, p2

    :goto_1
    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object p3, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {p3}, Lcom/xiaomi/easymode/EasyModeFragment;->c(Lcom/xiaomi/easymode/EasyModeFragment;)Landroid/widget/TextView;

    move-result-object p3

    if-eqz p1, :cond_2

    sub-float p2, v0, p2

    :cond_2
    invoke-virtual {p3, p2}, Landroid/widget/TextView;->setAlpha(F)V

    return-void
.end method

.method public onPageSelected(I)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {v0, p1}, Lcom/xiaomi/easymode/EasyModeFragment;->a(Lcom/xiaomi/easymode/EasyModeFragment;I)I

    iget-object v0, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {v0}, Lcom/xiaomi/easymode/EasyModeFragment;->b(Lcom/xiaomi/easymode/EasyModeFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {v1}, Lcom/xiaomi/easymode/EasyModeFragment;->d(Lcom/xiaomi/easymode/EasyModeFragment;)[I

    move-result-object v1

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {v0}, Lcom/xiaomi/easymode/EasyModeFragment;->c(Lcom/xiaomi/easymode/EasyModeFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {v1}, Lcom/xiaomi/easymode/EasyModeFragment;->e(Lcom/xiaomi/easymode/EasyModeFragment;)[I

    move-result-object v1

    aget p1, v1, p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object p1, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {p1}, Lcom/xiaomi/easymode/EasyModeFragment;->b(Lcom/xiaomi/easymode/EasyModeFragment;)Landroid/widget/TextView;

    move-result-object p1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object p1, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {p1}, Lcom/xiaomi/easymode/EasyModeFragment;->c(Lcom/xiaomi/easymode/EasyModeFragment;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object p1, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {p1}, Lcom/xiaomi/easymode/EasyModeFragment;->f(Lcom/xiaomi/easymode/EasyModeFragment;)Lcom/xiaomi/easymode/widget/ViewPagerIndicator;

    move-result-object p1

    iget-object v0, p0, Lcom/xiaomi/easymode/h;->a:Lcom/xiaomi/easymode/EasyModeFragment;

    invoke-static {v0}, Lcom/xiaomi/easymode/EasyModeFragment;->a(Lcom/xiaomi/easymode/EasyModeFragment;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/xiaomi/easymode/widget/ViewPagerIndicator;->setSelected(I)V

    return-void
.end method
