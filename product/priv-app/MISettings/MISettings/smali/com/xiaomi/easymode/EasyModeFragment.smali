.class public Lcom/xiaomi/easymode/EasyModeFragment;
.super Lcom/misettings/common/base/BaseFragment;


# instance fields
.field private c:Landroidx/viewpager/widget/ViewPager;

.field private d:Landroid/widget/RelativeLayout;

.field private e:Lcom/xiaomi/easymode/a;

.field private f:I

.field private g:Landroid/widget/Button;

.field private h:Lcom/xiaomi/easymode/widget/ViewPagerIndicator;

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/easymode/ContentFragment;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:[I

.field private m:[I


# direct methods
.method public constructor <init>()V
    .locals 6

    invoke-direct {p0}, Lcom/misettings/common/base/BaseFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->f:I

    const/4 v1, 0x3

    new-array v2, v1, [I

    sget v3, Lcom/xiaomi/easymode/q;->display_title:I

    aput v3, v2, v0

    sget v3, Lcom/xiaomi/easymode/q;->contact_title:I

    const/4 v4, 0x1

    aput v3, v2, v4

    sget v3, Lcom/xiaomi/easymode/q;->touch_title:I

    const/4 v5, 0x2

    aput v3, v2, v5

    iput-object v2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->l:[I

    new-array v1, v1, [I

    sget v2, Lcom/xiaomi/easymode/q;->display_description:I

    aput v2, v1, v0

    sget v0, Lcom/xiaomi/easymode/q;->contact_description:I

    aput v0, v1, v4

    sget v0, Lcom/xiaomi/easymode/q;->touch_description:I

    aput v0, v1, v5

    iput-object v1, p0, Lcom/xiaomi/easymode/EasyModeFragment;->m:[I

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/easymode/EasyModeFragment;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->f:I

    return p0
.end method

.method static synthetic a(Lcom/xiaomi/easymode/EasyModeFragment;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/easymode/EasyModeFragment;->f:I

    return p1
.end method

.method static synthetic b(Lcom/xiaomi/easymode/EasyModeFragment;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->j:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/easymode/EasyModeFragment;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->k:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic d(Lcom/xiaomi/easymode/EasyModeFragment;)[I
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->l:[I

    return-object p0
.end method

.method static synthetic e(Lcom/xiaomi/easymode/EasyModeFragment;)[I
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->m:[I

    return-object p0
.end method

.method static synthetic f(Lcom/xiaomi/easymode/EasyModeFragment;)Lcom/xiaomi/easymode/widget/ViewPagerIndicator;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->h:Lcom/xiaomi/easymode/widget/ViewPagerIndicator;

    return-object p0
.end method

.method static synthetic g(Lcom/xiaomi/easymode/EasyModeFragment;)Landroid/content/Intent;
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/easymode/EasyModeFragment;->k()Landroid/content/Intent;

    move-result-object p0

    return-object p0
.end method

.method static synthetic h(Lcom/xiaomi/easymode/EasyModeFragment;)Landroid/widget/RelativeLayout;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->d:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method private k()Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.miui.home"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    sget p3, Lcom/xiaomi/easymode/o;->easy_mode_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public b(Z)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.xiaomi.action.ELDERLY_MODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "status"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const p1, 0x1000020

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0
.end method

.method public c(Z)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "easy_mode_update_font"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.MiuiSettingsReceiver"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "status"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const p1, 0x10000020

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0
.end method

.method public i()Z
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "elderly_mode"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    return v1
.end method

.method public j()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->d:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    move-result v0

    int-to-double v1, v0

    const-wide v3, 0x400199999999999aL    # 2.2

    div-double/2addr v1, v3

    double-to-int v1, v1

    iget-object v2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->d:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->d:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->i:Ljava/util/ArrayList;

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Lcom/misettings/common/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->i:Ljava/util/ArrayList;

    sget-boolean p2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->i:Ljava/util/ArrayList;

    const-string v2, "global"

    invoke-static {v2, v0}, Lcom/xiaomi/easymode/ContentFragment;->a(Ljava/lang/String;Z)Lcom/xiaomi/easymode/ContentFragment;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->i:Ljava/util/ArrayList;

    const-string v2, "presence"

    invoke-static {v2, v0}, Lcom/xiaomi/easymode/ContentFragment;->a(Ljava/lang/String;Z)Lcom/xiaomi/easymode/ContentFragment;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->i:Ljava/util/ArrayList;

    const-string v2, "contact"

    invoke-static {v2, v1}, Lcom/xiaomi/easymode/ContentFragment;->a(Ljava/lang/String;Z)Lcom/xiaomi/easymode/ContentFragment;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    sget p2, Lcom/xiaomi/easymode/n;->indicator_lyt:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/easymode/widget/ViewPagerIndicator;

    iput-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->h:Lcom/xiaomi/easymode/widget/ViewPagerIndicator;

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->h:Lcom/xiaomi/easymode/widget/ViewPagerIndicator;

    invoke-virtual {p2, v0}, Lcom/xiaomi/easymode/widget/ViewPagerIndicator;->setCycle(Z)V

    iget-object v2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->h:Lcom/xiaomi/easymode/widget/ViewPagerIndicator;

    const/4 v3, 0x1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v4, Lcom/xiaomi/easymode/l;->indicator_width:I

    invoke-virtual {p2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v5, Lcom/xiaomi/easymode/l;->indicator_height:I

    invoke-virtual {p2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v6, Lcom/xiaomi/easymode/k;->indicator_normal:I

    invoke-virtual {p2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v7, Lcom/xiaomi/easymode/k;->indicator_pressed:I

    invoke-virtual {p2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual/range {v2 .. v7}, Lcom/xiaomi/easymode/widget/ViewPagerIndicator;->setIndicatorColorOrShape(IIIII)V

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->h:Lcom/xiaomi/easymode/widget/ViewPagerIndicator;

    iget-object v2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p2, v2, v1}, Lcom/xiaomi/easymode/widget/ViewPagerIndicator;->setIndicatorNum(II)V

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->h:Lcom/xiaomi/easymode/widget/ViewPagerIndicator;

    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/16 v3, 0x8

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_1

    :cond_1
    move v2, v1

    :goto_1
    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    sget p2, Lcom/xiaomi/easymode/n;->content_title:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->j:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->j:Landroid/widget/TextView;

    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    move v3, v1

    :goto_2
    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    sget p2, Lcom/xiaomi/easymode/n;->content_description:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->k:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->k:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->k:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/Scroller;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setScroller(Landroid/widget/Scroller;)V

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->k:Landroid/widget/TextView;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVerticalScrollBarEnabled(Z)V

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->k:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    sget p2, Lcom/xiaomi/easymode/n;->start_mode:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    iput-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->g:Landroid/widget/Button;

    new-array p2, v0, [Landroid/view/View;

    iget-object v2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->g:Landroid/widget/Button;

    aput-object v2, p2, v1

    invoke-static {p2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p2

    invoke-interface {p2}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object p2

    const/high16 v2, 0x3f800000    # 1.0f

    new-array v3, v1, [Lmiuix/animation/m$b;

    invoke-interface {p2, v2, v3}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    iget-object v2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->g:Landroid/widget/Button;

    new-array v3, v1, [Lmiuix/animation/a/a;

    invoke-interface {p2, v2, v3}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->g:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/xiaomi/easymode/EasyModeFragment;->i()Z

    move-result v2

    if-eqz v2, :cond_3

    sget v2, Lcom/xiaomi/easymode/q;->exit_easymode:I

    goto :goto_3

    :cond_3
    sget v2, Lcom/xiaomi/easymode/q;->start_easymode:I

    :goto_3
    invoke-virtual {p2, v2}, Landroid/widget/Button;->setText(I)V

    new-instance p2, Lcom/xiaomi/easymode/a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/easymode/EasyModeFragment;->i:Ljava/util/ArrayList;

    invoke-direct {p2, v2, v3}, Lcom/xiaomi/easymode/a;-><init>(Landroidx/fragment/app/FragmentManager;Ljava/util/ArrayList;)V

    iput-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->e:Lcom/xiaomi/easymode/a;

    sget p2, Lcom/xiaomi/easymode/n;->viewpager:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroidx/viewpager/widget/ViewPager;

    iput-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->c:Landroidx/viewpager/widget/ViewPager;

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->c:Landroidx/viewpager/widget/ViewPager;

    iget-object v2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->e:Lcom/xiaomi/easymode/a;

    invoke-virtual {p2, v2}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/f;)V

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->c:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {p2, v0}, Landroidx/viewpager/widget/ViewPager;->setOffscreenPageLimit(I)V

    sget-boolean p2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez p2, :cond_4

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->c:Landroidx/viewpager/widget/ViewPager;

    new-instance v0, Lcom/xiaomi/easymode/h;

    invoke-direct {v0, p0}, Lcom/xiaomi/easymode/h;-><init>(Lcom/xiaomi/easymode/EasyModeFragment;)V

    invoke-virtual {p2, v0}, Landroidx/viewpager/widget/ViewPager;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$d;)V

    :cond_4
    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->l:[I

    aget v0, v0, v1

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/xiaomi/easymode/EasyModeFragment;->m:[I

    aget v0, v0, v1

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object p2, p0, Lcom/xiaomi/easymode/EasyModeFragment;->g:Landroid/widget/Button;

    new-instance v0, Lcom/xiaomi/easymode/i;

    invoke-direct {v0, p0}, Lcom/xiaomi/easymode/i;-><init>(Lcom/xiaomi/easymode/EasyModeFragment;)V

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget p2, Lcom/xiaomi/easymode/n;->viewpager_wrapper:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout;

    iput-object p1, p0, Lcom/xiaomi/easymode/EasyModeFragment;->d:Landroid/widget/RelativeLayout;

    iget-object p1, p0, Lcom/xiaomi/easymode/EasyModeFragment;->d:Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    new-instance p2, Lcom/xiaomi/easymode/j;

    invoke-direct {p2, p0}, Lcom/xiaomi/easymode/j;-><init>(Lcom/xiaomi/easymode/EasyModeFragment;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    return-void
.end method
