.class Lcom/xiaomi/easymode/f;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/easymode/ContentFragment;->g()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xiaomi/easymode/ContentFragment;


# direct methods
.method constructor <init>(Lcom/xiaomi/easymode/ContentFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/easymode/f;->a:Lcom/xiaomi/easymode/ContentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2

    invoke-static {}, Lcom/xiaomi/easymode/ContentFragment;->f()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "playVideo onError mErrorCount = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/xiaomi/easymode/f;->a:Lcom/xiaomi/easymode/ContentFragment;

    invoke-static {p3}, Lcom/xiaomi/easymode/ContentFragment;->c(Lcom/xiaomi/easymode/ContentFragment;)I

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/xiaomi/easymode/f;->a:Lcom/xiaomi/easymode/ContentFragment;

    invoke-static {p1}, Lcom/xiaomi/easymode/ContentFragment;->e(Lcom/xiaomi/easymode/ContentFragment;)Lcom/xiaomi/easymode/ContentFragment$a;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/xiaomi/easymode/f;->a:Lcom/xiaomi/easymode/ContentFragment;

    invoke-static {p1}, Lcom/xiaomi/easymode/ContentFragment;->d(Lcom/xiaomi/easymode/ContentFragment;)I

    move-result p1

    const/4 p2, 0x5

    if-gt p1, p2, :cond_0

    iget-object p1, p0, Lcom/xiaomi/easymode/f;->a:Lcom/xiaomi/easymode/ContentFragment;

    invoke-static {p1}, Lcom/xiaomi/easymode/ContentFragment;->e(Lcom/xiaomi/easymode/ContentFragment;)Lcom/xiaomi/easymode/ContentFragment$a;

    move-result-object p1

    const/4 p2, 0x1

    const-wide/16 v0, 0x64

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
