.class public Lcom/xiaomi/easymode/widget/CViewPager;
.super Landroidx/viewpager/widget/ViewPager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/easymode/widget/CViewPager$a;
    }
.end annotation


# instance fields
.field private a:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroidx/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/easymode/widget/CViewPager;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroidx/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/easymode/widget/CViewPager;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/easymode/widget/CViewPager;)F
    .locals 0

    iget p0, p0, Lcom/xiaomi/easymode/widget/CViewPager;->a:F

    return p0
.end method

.method private a()V
    .locals 2

    iget v0, p0, Lcom/xiaomi/easymode/widget/CViewPager;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setClipToOutline(Z)V

    new-instance v0, Lcom/xiaomi/easymode/widget/CViewPager$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/easymode/widget/CViewPager$a;-><init>(Lcom/xiaomi/easymode/widget/CViewPager;Lcom/xiaomi/easymode/widget/a;)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/xiaomi/easymode/l;->card_radius_global:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/xiaomi/easymode/l;->card_radius:I

    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/xiaomi/easymode/widget/CViewPager;->a:F

    invoke-direct {p0}, Lcom/xiaomi/easymode/widget/CViewPager;->a()V

    return-void
.end method
