.class public Lcom/xiaomi/easymode/EasyModeActivity;
.super Lcom/misettings/common/base/SubSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/misettings/common/base/SubSettings;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/misettings/common/base/SubSettings;->onCreate(Landroid/os/Bundle;)V

    sget p1, Lcom/xiaomi/easymode/q;->title_activity_easy_mode:I

    invoke-virtual {p0, p1}, Lcom/misettings/common/base/SubSettings;->setTitle(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.xiaomi.misettings"

    const-string v2, "com.xiaomi.misettings.usagestats.ExitMultiWindowActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
