.class public Lcom/xiaomi/easymode/ContentFragment;
.super Landroidx/fragment/app/Fragment;

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/easymode/ContentFragment$a;
    }
.end annotation


# static fields
.field private static a:Ljava/lang/String; = "ContentFragment"

.field private static b:Ljava/lang/String; = "videoName"

.field private static c:Ljava/lang/String; = "background"

.field private static d:Ljava/lang/String; = "title"

.field private static e:Ljava/lang/String; = "content"

.field private static f:Ljava/lang/String; = "enable"


# instance fields
.field private g:Landroid/view/TextureView;

.field private volatile h:Landroid/view/Surface;

.field private i:Z

.field private j:Landroid/graphics/Bitmap;

.field private k:Landroid/media/MediaPlayer;

.field private l:Landroid/widget/ImageView;

.field private m:Landroid/os/HandlerThread;

.field private n:Lcom/xiaomi/easymode/ContentFragment$a;

.field private volatile o:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/easymode/ContentFragment;->o:I

    return-void
.end method

.method private a(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 2

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iput p3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 p3, 0x0

    iput-boolean p3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/16 p3, 0xa0

    iput p3, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    iput p3, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    sget-object p3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object p3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/xiaomi/easymode/ContentFragment;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/easymode/ContentFragment;->l:Landroid/widget/ImageView;

    return-object p0
.end method

.method public static a(Ljava/lang/String;Z)Lcom/xiaomi/easymode/ContentFragment;
    .locals 8

    new-instance v0, Lcom/xiaomi/easymode/ContentFragment;

    invoke-direct {v0}, Lcom/xiaomi/easymode/ContentFragment;-><init>()V

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x4c186305

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq v1, v2, :cond_2

    const v2, -0x4a16fc5d

    if-eq v1, v2, :cond_1

    const v2, 0x38b72420

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "contact"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    move p0, v4

    goto :goto_1

    :cond_1
    const-string v1, "global"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    move p0, v3

    goto :goto_1

    :cond_2
    const-string v1, "presence"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    const/4 p0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 p0, -0x1

    :goto_1
    if-eqz p0, :cond_6

    if-eq p0, v4, :cond_5

    if-eq p0, v3, :cond_4

    goto :goto_2

    :cond_4
    sget v2, Lcom/xiaomi/easymode/p;->video_global:I

    sget v3, Lcom/xiaomi/easymode/q;->display_title:I

    sget v4, Lcom/xiaomi/easymode/q;->display_description:I

    sget v5, Lcom/xiaomi/easymode/m;->pic_global:I

    move-object v1, v7

    move v6, p1

    invoke-static/range {v1 .. v6}, Lcom/xiaomi/easymode/ContentFragment;->a(Landroid/os/Bundle;IIIIZ)V

    goto :goto_2

    :cond_5
    sget v2, Lcom/xiaomi/easymode/p;->video_contact:I

    sget v3, Lcom/xiaomi/easymode/q;->contact_title:I

    sget v4, Lcom/xiaomi/easymode/q;->contact_description:I

    sget v5, Lcom/xiaomi/easymode/m;->pic_contact:I

    move-object v1, v7

    move v6, p1

    invoke-static/range {v1 .. v6}, Lcom/xiaomi/easymode/ContentFragment;->a(Landroid/os/Bundle;IIIIZ)V

    goto :goto_2

    :cond_6
    sget v2, Lcom/xiaomi/easymode/p;->video_presence:I

    sget v3, Lcom/xiaomi/easymode/q;->display_title:I

    sget v4, Lcom/xiaomi/easymode/q;->display_description:I

    sget v5, Lcom/xiaomi/easymode/m;->pic_presence:I

    move-object v1, v7

    move v6, p1

    invoke-static/range {v1 .. v6}, Lcom/xiaomi/easymode/ContentFragment;->a(Landroid/os/Bundle;IIIIZ)V

    :goto_2
    invoke-virtual {v0, v7}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;IIIIZ)V
    .locals 1

    sget-object v0, Lcom/xiaomi/easymode/ContentFragment;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object p1, Lcom/xiaomi/easymode/ContentFragment;->d:Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object p1, Lcom/xiaomi/easymode/ContentFragment;->e:Ljava/lang/String;

    invoke-virtual {p0, p1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object p1, Lcom/xiaomi/easymode/ContentFragment;->c:Ljava/lang/String;

    invoke-virtual {p0, p1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object p1, Lcom/xiaomi/easymode/ContentFragment;->f:Ljava/lang/String;

    invoke-virtual {p0, p1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic b(Lcom/xiaomi/easymode/ContentFragment;)Landroid/media/MediaPlayer;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    return-object p0
.end method

.method static synthetic c(Lcom/xiaomi/easymode/ContentFragment;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/easymode/ContentFragment;->o:I

    return p0
.end method

.method static synthetic d(Lcom/xiaomi/easymode/ContentFragment;)I
    .locals 2

    iget v0, p0, Lcom/xiaomi/easymode/ContentFragment;->o:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/xiaomi/easymode/ContentFragment;->o:I

    return v0
.end method

.method static synthetic e(Lcom/xiaomi/easymode/ContentFragment;)Lcom/xiaomi/easymode/ContentFragment$a;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/easymode/ContentFragment;->n:Lcom/xiaomi/easymode/ContentFragment$a;

    return-object p0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/xiaomi/easymode/ContentFragment;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/xiaomi/easymode/ContentFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/easymode/ContentFragment;->i()V

    return-void
.end method

.method private i()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v1, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/xiaomi/easymode/ContentFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "releaseVideo error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public g()V
    .locals 4

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android.resource://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/xiaomi/easymode/ContentFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/easymode/ContentFragment;->h:Landroid/view/Surface;

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    if-nez v1, :cond_1

    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    iget-object v1, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/xiaomi/easymode/ContentFragment;->h:Landroid/view/Surface;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/xiaomi/easymode/d;

    invoke-direct {v1, p0}, Lcom/xiaomi/easymode/d;-><init>(Lcom/xiaomi/easymode/ContentFragment;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/xiaomi/easymode/e;

    invoke-direct {v1, p0}, Lcom/xiaomi/easymode/e;-><init>(Lcom/xiaomi/easymode/ContentFragment;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/xiaomi/easymode/f;

    invoke-direct {v1, p0}, Lcom/xiaomi/easymode/f;-><init>(Lcom/xiaomi/easymode/ContentFragment;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    goto :goto_1

    :cond_3
    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/misettings/common/utils/j;->a()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/misettings/common/utils/j;->a()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/easymode/b;

    invoke-direct {v1, p0}, Lcom/xiaomi/easymode/b;-><init>(Lcom/xiaomi/easymode/ContentFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/xiaomi/easymode/ContentFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "playVideo error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/misettings/common/utils/j;->a()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/misettings/common/utils/j;->a()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/xiaomi/easymode/g;

    invoke-direct {v2, p0}, Lcom/xiaomi/easymode/g;-><init>(Lcom/xiaomi/easymode/ContentFragment;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-void
.end method

.method public h()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->k:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/xiaomi/easymode/ContentFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopVideo error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroid/os/HandlerThread;

    const-string v0, "start_video"

    const/4 v1, 0x5

    invoke-direct {p1, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object p1, p0, Lcom/xiaomi/easymode/ContentFragment;->m:Landroid/os/HandlerThread;

    iget-object p1, p0, Lcom/xiaomi/easymode/ContentFragment;->m:Landroid/os/HandlerThread;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    new-instance p1, Lcom/xiaomi/easymode/ContentFragment$a;

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->m:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, p0, v0}, Lcom/xiaomi/easymode/ContentFragment$a;-><init>(Lcom/xiaomi/easymode/ContentFragment;Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/xiaomi/easymode/ContentFragment;->n:Lcom/xiaomi/easymode/ContentFragment$a;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    sget p3, Lcom/xiaomi/easymode/o;->pager_content:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    sget p2, Lcom/xiaomi/easymode/n;->video_lyt:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/FrameLayout;

    sget p3, Lcom/xiaomi/easymode/n;->video_img:I

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/xiaomi/easymode/ContentFragment;->l:Landroid/widget/ImageView;

    sget p3, Lcom/xiaomi/easymode/n;->video_content:I

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/view/TextureView;

    iput-object p3, p0, Lcom/xiaomi/easymode/ContentFragment;->g:Landroid/view/TextureView;

    iget-object p3, p0, Lcom/xiaomi/easymode/ContentFragment;->g:Landroid/view/TextureView;

    invoke-virtual {p3, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/easymode/ContentFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x8

    invoke-direct {p0, p3, v0, v1}, Lcom/xiaomi/easymode/ContentFragment;->a(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object p3

    iput-object p3, p0, Lcom/xiaomi/easymode/ContentFragment;->j:Landroid/graphics/Bitmap;

    iget-object p3, p0, Lcom/xiaomi/easymode/ContentFragment;->j:Landroid/graphics/Bitmap;

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p3

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/easymode/ContentFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    sget-object p3, Lcom/xiaomi/easymode/ContentFragment;->f:Ljava/lang/String;

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p2

    iput-boolean p2, p0, Lcom/xiaomi/easymode/ContentFragment;->i:Z

    :cond_1
    return-object p1
.end method

.method public onDestroy()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->j:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->j:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/xiaomi/easymode/ContentFragment;->n:Lcom/xiaomi/easymode/ContentFragment$a;

    if-eqz v1, :cond_1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v1, p0, Lcom/xiaomi/easymode/ContentFragment;->n:Lcom/xiaomi/easymode/ContentFragment$a;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->m:Landroid/os/HandlerThread;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    :cond_2
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->n:Lcom/xiaomi/easymode/ContentFragment$a;

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    iget-boolean v0, p0, Lcom/xiaomi/easymode/ContentFragment;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->n:Lcom/xiaomi/easymode/ContentFragment$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->h:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->n:Lcom/xiaomi/easymode/ContentFragment$a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    new-instance p2, Landroid/view/Surface;

    invoke-direct {p2, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object p2, p0, Lcom/xiaomi/easymode/ContentFragment;->h:Landroid/view/Surface;

    iget-object p1, p0, Lcom/xiaomi/easymode/ContentFragment;->n:Lcom/xiaomi/easymode/ContentFragment$a;

    if-eqz p1, :cond_0

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    iget-object p1, p0, Lcom/xiaomi/easymode/ContentFragment;->n:Lcom/xiaomi/easymode/ContentFragment$a;

    if-eqz p1, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->setUserVisibleHint(Z)V

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/xiaomi/easymode/ContentFragment;->i:Z

    iget-object v0, p0, Lcom/xiaomi/easymode/ContentFragment;->n:Lcom/xiaomi/easymode/ContentFragment$a;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/xiaomi/easymode/ContentFragment;->i:Z

    iget-object p1, p0, Lcom/xiaomi/easymode/ContentFragment;->n:Lcom/xiaomi/easymode/ContentFragment$a;

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_0
    return-void
.end method
