.class public Lcom/miui/greenguard/receiver/NetworkChangeReceiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Lcom/miui/greenguard/receiver/e;

    invoke-direct {v0, p0}, Lcom/miui/greenguard/receiver/e;-><init>(Lcom/miui/greenguard/receiver/NetworkChangeReceiver;)V

    iput-object v0, p0, Lcom/miui/greenguard/receiver/NetworkChangeReceiver;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/miui/greenguard/receiver/NetworkChangeReceiver;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/greenguard/receiver/NetworkChangeReceiver;->c(Landroid/content/Context;)V

    return-void
.end method

.method private c(Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, Lb/e/a/b/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    new-instance v1, Lcom/miui/greenguard/receiver/a;

    invoke-direct {v1, p0, p1}, Lcom/miui/greenguard/receiver/a;-><init>(Lcom/miui/greenguard/receiver/NetworkChangeReceiver;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/manager/a/g;->b(Lcom/miui/greenguard/manager/a/g$c;)V

    :cond_0
    return-void
.end method

.method private d(Landroid/content/Context;)V
    .locals 0

    invoke-static {p1}, Lcom/miui/greenguard/upload/d;->a(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public synthetic a(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/greenguard/receiver/NetworkChangeReceiver;->d(Landroid/content/Context;)V

    invoke-static {}, Lcom/miui/greenguard/manager/p;->b()V

    return-void
.end method

.method public synthetic b(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    new-instance v1, Lcom/miui/greenguard/receiver/b;

    invoke-direct {v1, p0, p1}, Lcom/miui/greenguard/receiver/b;-><init>(Lcom/miui/greenguard/receiver/NetworkChangeReceiver;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/manager/a/g;->a(Lcom/miui/greenguard/manager/a/g$b;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    :try_start_0
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/ConnectivityManager;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string p2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/miui/greenguard/receiver/NetworkChangeReceiver;->a:Landroid/os/Handler;

    const/16 p2, 0x64

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p1, p0, Lcom/miui/greenguard/receiver/NetworkChangeReceiver;->a:Landroid/os/Handler;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    iget-object p2, p0, Lcom/miui/greenguard/receiver/NetworkChangeReceiver;->a:Landroid/os/Handler;

    const-wide/16 v0, 0x7d0

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "NetworkChangeReceiver onReceiver exception:"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "NetworkChangeReceiver"

    invoke-static {p2, p1}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void
.end method
