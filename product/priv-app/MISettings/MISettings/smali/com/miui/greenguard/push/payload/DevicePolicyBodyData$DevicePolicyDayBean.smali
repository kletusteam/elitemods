.class public Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DevicePolicyDayBean"
.end annotation


# instance fields
.field private durationPerDay:I

.field private enable:Z

.field private status:I

.field private unit:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDurationPerDay()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->durationPerDay:I

    return v0
.end method

.method public getHour()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->durationPerDay:I

    div-int/lit8 v0, v0, 0x3c

    div-int/lit8 v0, v0, 0x3c

    return v0
.end method

.method public getMin()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->durationPerDay:I

    div-int/lit8 v0, v0, 0x3c

    rem-int/lit8 v0, v0, 0x3c

    return v0
.end method

.method public getStatus()I
    .locals 1

    iget-boolean v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->enable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    iget v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->status:I

    return v0
.end method

.method public getTotalMin()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->durationPerDay:I

    div-int/lit8 v0, v0, 0x3c

    return v0
.end method

.method public getUnit()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->unit:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->unit:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->unit:Ljava/util/List;

    return-object v0
.end method

.method public isEnable()Z
    .locals 2

    iget-boolean v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->enable:Z

    const/4 v1, 0x1

    if-nez v0, :cond_1

    iget v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->status:I

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public setDurationPerDay(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->durationPerDay:I

    return-void
.end method

.method public setDurationPerDayByHourMin(II)V
    .locals 0

    mul-int/lit8 p1, p1, 0x3c

    mul-int/lit8 p1, p1, 0x3c

    mul-int/lit8 p2, p2, 0x3c

    add-int/2addr p1, p2

    iput p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->durationPerDay:I

    return-void
.end method

.method public setEnable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->enable:Z

    iput p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->status:I

    return-void
.end method

.method public setStatus(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->status:I

    return-void
.end method

.method public setUnit(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->unit:Ljava/util/List;

    return-void
.end method
