.class public Lcom/miui/greenguard/params/PostAppOverrideParam;
.super Lb/e/b/d/c;


# instance fields
.field private appName:Ljava/lang/String;

.field private deviceId:Ljava/lang/String;

.field private overTimes:I

.field private pkgName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lb/e/b/d/c;-><init>()V

    const-string v0, "6bbb663df2728d8cd58d42c1f98130d0ba94a5b6"

    iput-object v0, p0, Lcom/miui/greenguard/params/PostAppOverrideParam;->deviceId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAppName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/params/PostAppOverrideParam;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/params/PostAppOverrideParam;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getOverTimes()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/params/PostAppOverrideParam;->overTimes:I

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    const-string v0, "/ward/operation/application-overtime"

    return-object v0
.end method

.method public getPkgName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/params/PostAppOverrideParam;->pkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getResultClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lb/e/b/g/b;",
            ">;"
        }
    .end annotation

    const-class v0, Lb/e/b/g/a;

    return-object v0
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostAppOverrideParam;->appName:Ljava/lang/String;

    return-void
.end method

.method public setDeviceId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostAppOverrideParam;->deviceId:Ljava/lang/String;

    return-void
.end method

.method public setOverTimes(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/params/PostAppOverrideParam;->overTimes:I

    return-void
.end method

.method public setPkgName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostAppOverrideParam;->pkgName:Ljava/lang/String;

    return-void
.end method
