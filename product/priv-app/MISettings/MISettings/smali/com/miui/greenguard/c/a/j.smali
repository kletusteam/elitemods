.class public Lcom/miui/greenguard/c/a/j;
.super Lcom/miui/greenguard/c/a/a/d;


# instance fields
.field private a:Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/a/d;-><init>()V

    iput-object p2, p0, Lcom/miui/greenguard/c/a/j;->a:Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;

    iput-object p1, p0, Lcom/miui/greenguard/c/a/j;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    invoke-super {p0}, Lcom/miui/greenguard/c/a/a/d;->a()V

    :try_start_0
    iget-object v0, p0, Lcom/miui/greenguard/c/a/j;->a:Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;

    iget-boolean v0, v0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->enable:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->v(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/i/h;->c()V

    iget-object v0, p0, Lcom/miui/greenguard/c/a/j;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->B(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/c/a/j;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-class v0, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;

    invoke-static {v0}, Lcom/xiaomi/misettings/tools/c;->b(Ljava/lang/Class;)V

    :cond_1
    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->z(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/i/h;->c()V

    iget-object v0, p0, Lcom/miui/greenguard/c/a/j;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/i/h;->D(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "DoMandatoryRestCmd"

    const-string v2, "DoMandatoryRestCmd error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method protected d()V
    .locals 3

    invoke-super {p0}, Lcom/miui/greenguard/c/a/a/d;->d()V

    iget-object v0, p0, Lcom/miui/greenguard/c/a/j;->a:Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-boolean v1, v0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->enable:Z

    iget-object v2, p0, Lcom/miui/greenguard/c/a/j;->b:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/xiaomi/misettings/usagestats/i/h;->b(Landroid/content/Context;Z)V

    iget v1, v0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->continuousDuration:I

    div-int/lit8 v1, v1, 0x3c

    iget-object v2, p0, Lcom/miui/greenguard/c/a/j;->b:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->b(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/miui/greenguard/c/a/j;->b:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/xiaomi/misettings/usagestats/i/h;->b(Landroid/content/Context;I)V

    iget v0, v0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->restTime:I

    div-int/lit8 v0, v0, 0x3c

    iget-object v1, p0, Lcom/miui/greenguard/c/a/j;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/miui/greenguard/c/a/j;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/i/h;->d(Landroid/content/Context;I)V

    return-void
.end method
