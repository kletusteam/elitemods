.class public Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;
.super Lcom/miui/greenguard/push/payload/BaseBodyData;

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;,
        Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;
    }
.end annotation


# instance fields
.field private holiday:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

.field private unrestrictedApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/SimpleAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private workingDay:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/push/payload/BaseBodyData;-><init>()V

    return-void
.end method


# virtual methods
.method protected clone()Ljava/lang/Object;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getHoliday()Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->holiday:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    return-object v0
.end method

.method public getUnrestrictedApps()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/SimpleAppInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->unrestrictedApps:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->unrestrictedApps:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->unrestrictedApps:Ljava/util/List;

    return-object v0
.end method

.method public getWorkingDay()Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->workingDay:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    return-object v0
.end method

.method public setHoliday(Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->holiday:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    return-void
.end method

.method public setUnrestrictedApps(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/SimpleAppInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->unrestrictedApps:Ljava/util/List;

    return-void
.end method

.method public setWorkingDay(Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->workingDay:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    return-void
.end method
