.class public Lcom/miui/greenguard/params/GetDashBordParam;
.super Lcom/miui/greenguard/params/GetBaseParam;


# instance fields
.field private date:I

.field private dateType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/params/GetBaseParam;-><init>()V

    return-void
.end method


# virtual methods
.method public getDate()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/params/GetDashBordParam;->date:I

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    const-string v0, "/admin/usage-board"

    return-object v0
.end method

.method public getResultClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lb/e/b/g/b;",
            ">;"
        }
    .end annotation

    const-class v0, Lcom/miui/greenguard/result/DashBordResult;

    return-object v0
.end method

.method public setDataType(Z)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/params/GetDashBordParam;->dateType:I

    return-void
.end method

.method public setDate(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/params/GetDashBordParam;->date:I

    return-void
.end method
