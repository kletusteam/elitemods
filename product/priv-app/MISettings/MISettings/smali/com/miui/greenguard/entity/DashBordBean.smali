.class public Lcom/miui/greenguard/entity/DashBordBean;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;,
        Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;,
        Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;,
        Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;
    }
.end annotation


# instance fields
.field private appUsage:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "appUsage"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;",
            ">;"
        }
    .end annotation
.end field

.field private currentDate:J

.field private date:I

.field private dateType:I

.field private deviceUsage:Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "deviceUsage"
    .end annotation
.end field

.field private familyBean:Lcom/miui/greenguard/entity/FamilyBean;

.field private mandatoryRest:Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mandatoryRest"
    .end annotation
.end field

.field private selectIndex:I

.field private selectTimeStamp:J

.field private today:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "today"
    .end annotation
.end field

.field private unlock:Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "unlock"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->selectIndex:I

    return-void
.end method


# virtual methods
.method public getAppUsage()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/DashBordBean$AppUsageBean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->appUsage:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->appUsage:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->appUsage:Ljava/util/List;

    return-object v0
.end method

.method public getCurrentDate()J
    .locals 2

    iget-wide v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->currentDate:J

    return-wide v0
.end method

.method public getDate()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->date:I

    return v0
.end method

.method public getDateType()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->dateType:I

    return v0
.end method

.method public getDeviceUsage()Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->deviceUsage:Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;

    invoke-direct {v0}, Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;-><init>()V

    iput-object v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->deviceUsage:Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->deviceUsage:Lcom/miui/greenguard/entity/DashBordBean$DeviceUsageBean;

    return-object v0
.end method

.method public getFamilyBean()Lcom/miui/greenguard/entity/FamilyBean;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->familyBean:Lcom/miui/greenguard/entity/FamilyBean;

    return-object v0
.end method

.method public getMandatoryRest()Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->mandatoryRest:Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;

    return-object v0
.end method

.method public getSelectIndex()I
    .locals 2

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean;->isWeekDateData()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->selectIndex:I

    const/4 v1, -0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    iput v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->selectIndex:I

    :cond_0
    iget v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->selectIndex:I

    return v0
.end method

.method public getSelectTimeStamp()J
    .locals 2

    iget-wide v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->selectTimeStamp:J

    return-wide v0
.end method

.method public getToday()J
    .locals 2

    iget-wide v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->today:J

    return-wide v0
.end method

.method public getUnlock()Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->unlock:Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;

    invoke-direct {v0}, Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;-><init>()V

    iput-object v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->unlock:Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->unlock:Lcom/miui/greenguard/entity/DashBordBean$UnlockBean;

    return-object v0
.end method

.method public isDailyDateData()Z
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->dateType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isFillAllWeekData()Z
    .locals 4

    iget-wide v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->currentDate:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isWeekDateData()Z
    .locals 2

    iget v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->dateType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public setCurrentDate(J)V
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide p1

    :cond_0
    iput-wide p1, p0, Lcom/miui/greenguard/entity/DashBordBean;->currentDate:J

    return-void
.end method

.method public setDate(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/DashBordBean;->date:I

    return-void
.end method

.method public setDateTypeDaily()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->dateType:I

    return-void
.end method

.method public setDateTypeWeek()V
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lcom/miui/greenguard/entity/DashBordBean;->dateType:I

    return-void
.end method

.method public setFamilyBean(Lcom/miui/greenguard/entity/FamilyBean;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/entity/DashBordBean;->familyBean:Lcom/miui/greenguard/entity/FamilyBean;

    return-void
.end method

.method public setSelectIndex(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/DashBordBean;->selectIndex:I

    return-void
.end method

.method public setSelectTimeStamp(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/greenguard/entity/DashBordBean;->selectTimeStamp:J

    return-void
.end method
