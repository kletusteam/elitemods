.class public Lcom/miui/greenguard/c/a/e;
.super Lcom/miui/greenguard/c/a/f;


# instance fields
.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/AppPolicyBodyData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppPolicyBodyData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/greenguard/c/a/f;-><init>(Landroid/content/Context;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/miui/greenguard/c/a/e;->d:Ljava/util/List;

    iget-object p1, p0, Lcom/miui/greenguard/c/a/e;->d:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/AppPolicyBodyData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/miui/greenguard/c/a/f;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/miui/greenguard/c/a/e;->d:Ljava/util/List;

    return-void
.end method

.method private j()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/c/a/e;->d:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method protected a()V
    .locals 4

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/e;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    new-instance v1, Lcom/miui/greenguard/c/a/a;

    invoke-direct {v1, p0}, Lcom/miui/greenguard/c/a/a;-><init>(Lcom/miui/greenguard/c/a/e;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method protected b()V
    .locals 0

    invoke-super {p0}, Lcom/miui/greenguard/c/a/f;->b()V

    return-void
.end method

.method protected d()V
    .locals 7

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/e;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/e;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->c(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/miui/greenguard/c/a/e;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/miui/greenguard/c/a/e;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/e;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->getDurationPerDay()I

    move-result v5

    sget v6, Lcom/xiaomi/misettings/usagestats/utils/L;->b:I

    div-int/2addr v5, v6

    invoke-virtual {v2}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->getPolicyType()I

    move-result v6

    if-nez v6, :cond_1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    move v6, v0

    :goto_1
    invoke-virtual {v2}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->getLimitFlag()I

    move-result v2

    invoke-static {v3, v4, v5, v6, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Ljava/lang/String;IZI)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected g()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/miui/greenguard/c/a/e;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->getPkgName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()V
    .locals 2

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Z)V

    return-void
.end method
