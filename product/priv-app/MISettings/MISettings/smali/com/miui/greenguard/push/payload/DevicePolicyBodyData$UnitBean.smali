.class public Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnitBean"
.end annotation


# instance fields
.field private beginTime:Ljava/lang/String;

.field private dataType:I

.field private endTime:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getMinuteByStr(Ljava/lang/String;)I
    .locals 3

    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    aget-object v0, p1, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3c

    aget-object p1, p1, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    add-int/2addr v0, p1

    return v0

    :cond_0
    return v1
.end method


# virtual methods
.method public getBeginTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->beginTime:Ljava/lang/String;

    return-object v0
.end method

.method public getBeginTotalMin()I
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->beginTime:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->getMinuteByStr(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getDataType()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->dataType:I

    return v0
.end method

.method public getEndTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->endTime:Ljava/lang/String;

    return-object v0
.end method

.method public getEndTotalMin()I
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->endTime:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->getMinuteByStr(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public setBeginTime(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->beginTime:Ljava/lang/String;

    return-void
.end method

.method public setDataType(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->dataType:I

    return-void
.end method

.method public setEndTime(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$UnitBean;->endTime:Ljava/lang/String;

    return-void
.end method
