.class public Lcom/miui/greenguard/entity/applimit/AppLimitEntity;
.super Ljava/lang/Object;


# instance fields
.field private appName:Ljava/lang/String;

.field private durationPerHoliday:I

.field private durationPerWorkday:I

.field private limitFlagHoliday:I

.field private limitFlagWorkday:I

.field private pkgName:Ljava/lang/String;

.field private prolongCount:I

.field private status:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public getDurationPerHoliday()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->durationPerHoliday:I

    return v0
.end method

.method public getDurationPerWorkday()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->durationPerWorkday:I

    return v0
.end method

.method public getLimitFlagHoliday()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->limitFlagHoliday:I

    return v0
.end method

.method public getLimitFlagWorkday()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->limitFlagWorkday:I

    return v0
.end method

.method public getPkgName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->pkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getProlongCount()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->prolongCount:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->status:I

    return v0
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->appName:Ljava/lang/String;

    return-void
.end method

.method public setDurationPerHoliday(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->durationPerHoliday:I

    return-void
.end method

.method public setDurationPerWorkday(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->durationPerWorkday:I

    return-void
.end method

.method public setLimitFlagHoliday(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->limitFlagHoliday:I

    return-void
.end method

.method public setLimitFlagWorkday(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->limitFlagWorkday:I

    return-void
.end method

.method public setPkgName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->pkgName:Ljava/lang/String;

    return-void
.end method

.method public setProlongCount(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->prolongCount:I

    return-void
.end method

.method public setStatus(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/applimit/AppLimitEntity;->status:I

    return-void
.end method
