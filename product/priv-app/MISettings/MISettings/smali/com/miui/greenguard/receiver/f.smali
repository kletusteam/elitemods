.class public Lcom/miui/greenguard/receiver/f;
.super Ljava/lang/Object;


# static fields
.field private static a:Landroid/os/Handler;


# direct methods
.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const-string v0, "PackageInstallHelper"

    const-string v1, "handler start"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    return-void

    :cond_0
    sget-object v1, Lcom/miui/greenguard/receiver/f;->a:Landroid/os/Handler;

    if-nez v1, :cond_1

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/miui/greenguard/receiver/f;->a:Landroid/os/Handler;

    :cond_1
    :try_start_0
    sget-object v1, Lcom/miui/greenguard/receiver/f;->a:Landroid/os/Handler;

    new-instance v2, Lcom/miui/greenguard/receiver/d;

    invoke-direct {v2, p1, p0}, Lcom/miui/greenguard/receiver/d;-><init>(Landroid/content/Intent;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p0, "PackageInstallHelper handler error"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string p0, "handler end"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/g;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/xiaomi/misettings/usagestats/b/a/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->g(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0, p1, v2}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void

    :cond_1
    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result p1

    invoke-static {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result p1

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    sub-int/2addr p1, v1

    invoke-static {p0, v0, p1}, Lcom/xiaomi/misettings/usagestats/utils/j;->b(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/j;->k(Landroid/content/Context;)V

    :cond_2
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/a/p;->a()Lcom/xiaomi/misettings/usagestats/b/a/p;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/xiaomi/misettings/usagestats/b/a/p;->a(Landroid/content/Context;)V

    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object p1

    new-instance v0, Lcom/miui/greenguard/receiver/c;

    invoke-direct {v0, p0, p2}, Lcom/miui/greenguard/receiver/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 2

    const-string v0, "PackageInstallHelper"

    const-string v1, "PackageInstallHelper handler real start"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/d/c;->a()V

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/miui/greenguard/receiver/f;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0, v0}, Lcom/miui/greenguard/receiver/f;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/miui/greenguard/upload/d;->c(Landroid/content/Context;)V

    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doAppChange:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PackageInstallHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    const-string v0, "package:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    aget-object p1, p1, v2

    :cond_0
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/display/RefreshRate/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0, p1, v2}, Lcom/xiaomi/misettings/usagestats/utils/p;->c(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_1
    const-string v0, "android.intent.action.PACKAGE_ADDED"

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_2

    return-void

    :cond_2
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result p2

    if-ne p2, v2, :cond_3

    move p2, v2

    goto :goto_0

    :cond_3
    const/4 p2, 0x0

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doAppChange limit:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_5

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_4

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "doAppChange hasSuspendByAppLimit"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1, v2}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void

    :cond_4
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->k(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result p2

    invoke-static {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/p;->d(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result p2

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/L;->f(J)I

    move-result v0

    sub-int/2addr p2, v0

    invoke-static {p0, p1, p2}, Lcom/xiaomi/misettings/usagestats/utils/p;->c(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/p;->j(Landroid/content/Context;)V

    :cond_5
    return-void
.end method
