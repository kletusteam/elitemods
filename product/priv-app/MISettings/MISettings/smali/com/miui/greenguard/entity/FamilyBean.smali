.class public Lcom/miui/greenguard/entity/FamilyBean;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;
    }
.end annotation


# instance fields
.field private child:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "child"
    .end annotation
.end field

.field private currentSelectDevice:I

.field private devices:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "devices"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;",
            ">;"
        }
    .end annotation
.end field

.field private icon:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "icon"
    .end annotation
.end field

.field private nickName:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nickName"
    .end annotation
.end field

.field private owner:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "owner"
    .end annotation
.end field

.field private role:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "role"
    .end annotation
.end field

.field private userId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "userId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->currentSelectDevice:I

    return-void
.end method


# virtual methods
.method protected clone()Ljava/lang/Object;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public cloneSelf()Lcom/miui/greenguard/entity/FamilyBean;
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/miui/greenguard/entity/FamilyBean;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/greenguard/entity/FamilyBean;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    new-instance v0, Lcom/miui/greenguard/entity/FamilyBean;

    invoke-direct {v0}, Lcom/miui/greenguard/entity/FamilyBean;-><init>()V

    return-object v0
.end method

.method public getCurrentSelectDeviceIndex()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->currentSelectDevice:I

    return v0
.end method

.method public getDevices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->devices:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->devices:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->devices:Ljava/util/List;

    return-object v0
.end method

.method public getDevicesStrList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/FamilyBean;->getDevices()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    invoke-static {v3}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->access$000(Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getIcon()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->icon:Ljava/lang/String;

    return-object v0
.end method

.method public getNickName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->nickName:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectDevice()Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;
    .locals 2

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/FamilyBean;->getDevices()Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/miui/greenguard/entity/FamilyBean;->currentSelectDevice:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    return-object v0
.end method

.method public getSelectDeviceName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/FamilyBean;->getDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->currentSelectDevice:I

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/FamilyBean;->getDevices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/miui/greenguard/entity/FamilyBean;->getDevices()Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/miui/greenguard/entity/FamilyBean;->currentSelectDevice:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;

    invoke-static {v0}, Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;->access$000(Lcom/miui/greenguard/entity/FamilyBean$DevicesBean;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const-string v0, ""

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public hasDevice()Z
    .locals 1

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/FamilyBean;->getDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasMultiDevice()Z
    .locals 2

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/FamilyBean;->getDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isAllShowAccountList()Z
    .locals 3

    iget v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->role:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public isChild()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->child:Z

    return v0
.end method

.method public isOrganizer()Z
    .locals 2

    iget v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->role:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->child:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isOwner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/greenguard/entity/FamilyBean;->owner:Z

    return v0
.end method

.method public noDevice()Z
    .locals 1

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/FamilyBean;->getDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public setCurrentSelectDevice(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/FamilyBean;->currentSelectDevice:I

    return-void
.end method

.method public setIcon(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/entity/FamilyBean;->icon:Ljava/lang/String;

    return-void
.end method

.method public setNickName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/entity/FamilyBean;->nickName:Ljava/lang/String;

    return-void
.end method
