.class public Lcom/miui/greenguard/upload/model/AppUsageBean$UseTime;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/greenguard/upload/model/AppUsageBean;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UseTime"
.end annotation


# instance fields
.field private hour:I

.field private useTime:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/greenguard/upload/model/AppUsageBean$UseTime;->useTime:I

    return-void
.end method


# virtual methods
.method public getHour()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/upload/model/AppUsageBean$UseTime;->hour:I

    return v0
.end method

.method public getUseTime()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/upload/model/AppUsageBean$UseTime;->useTime:I

    return v0
.end method

.method public setHour(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/upload/model/AppUsageBean$UseTime;->hour:I

    return-void
.end method

.method public setUseTime(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/upload/model/AppUsageBean$UseTime;->useTime:I

    return-void
.end method
