.class public Lcom/miui/greenguard/push/payload/UnLimitAppBody;
.super Lcom/miui/greenguard/push/payload/BaseBodyData;


# instance fields
.field private applications:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "applications"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/SimpleAppInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/push/payload/BaseBodyData;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/SimpleAppInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/greenguard/push/payload/UnLimitAppBody;->applications:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/greenguard/push/payload/UnLimitAppBody;->applications:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/push/payload/UnLimitAppBody;->applications:Ljava/util/List;

    return-object v0
.end method
