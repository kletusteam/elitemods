.class public Lcom/miui/greenguard/entity/UnlockBean;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "UnlockBean"


# instance fields
.field private dayBeginningTime:J

.field private mFirstUnlockTime:J

.field private totalUnlock:I

.field private unlockList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public unlockTimeStamps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/greenguard/entity/UnlockBean;->totalUnlock:I

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/greenguard/entity/UnlockBean;->dayBeginningTime:J

    invoke-direct {p0}, Lcom/miui/greenguard/entity/UnlockBean;->init()V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/greenguard/entity/UnlockBean;->totalUnlock:I

    iput-wide p1, p0, Lcom/miui/greenguard/entity/UnlockBean;->dayBeginningTime:J

    invoke-direct {p0}, Lcom/miui/greenguard/entity/UnlockBean;->init()V

    return-void
.end method

.method private calIndex(J)I
    .locals 6

    iget-wide v0, p0, Lcom/miui/greenguard/entity/UnlockBean;->dayBeginningTime:J

    cmp-long v2, p1, v0

    const/4 v3, -0x1

    if-ltz v2, :cond_2

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    add-long/2addr v4, v0

    cmp-long v2, p1, v4

    if-lez v2, :cond_0

    goto :goto_1

    :cond_0
    sub-long/2addr p1, v0

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    div-long/2addr p1, v0

    long-to-int p1, p1

    if-ltz p1, :cond_1

    sget p2, Lcom/xiaomi/misettings/usagestats/utils/L;->c:I

    if-ge p1, p2, :cond_1

    goto :goto_0

    :cond_1
    move p1, v3

    :goto_0
    return p1

    :cond_2
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "addUnlock()... incorrect time:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "UnlockBean"

    invoke-static {p2, p1}, Lcom/xiaomi/misettings/usagestats/utils/H;->b(Ljava/lang/String;Ljava/lang/String;)V

    return v3
.end method

.method private init()V
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/greenguard/entity/UnlockBean;->unlockList:Ljava/util/List;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget v2, Lcom/xiaomi/misettings/usagestats/utils/L;->c:I

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/miui/greenguard/entity/UnlockBean;->unlockList:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/greenguard/entity/UnlockBean;->unlockTimeStamps:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addUnlock(J)V
    .locals 4

    iget-wide v0, p0, Lcom/miui/greenguard/entity/UnlockBean;->mFirstUnlockTime:J

    cmp-long v2, v0, p1

    if-gtz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    iput-wide p1, p0, Lcom/miui/greenguard/entity/UnlockBean;->mFirstUnlockTime:J

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/miui/greenguard/entity/UnlockBean;->calIndex(J)I

    move-result v0

    if-ltz v0, :cond_2

    iget-object v1, p0, Lcom/miui/greenguard/entity/UnlockBean;->unlockList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/miui/greenguard/entity/UnlockBean;->unlockTimeStamps:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/miui/greenguard/entity/UnlockBean;->unlockList:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    add-int/lit8 p2, p2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget p1, p0, Lcom/miui/greenguard/entity/UnlockBean;->totalUnlock:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/miui/greenguard/entity/UnlockBean;->totalUnlock:I

    :cond_2
    return-void
.end method

.method public getDayBeginningTime()J
    .locals 2

    iget-wide v0, p0, Lcom/miui/greenguard/entity/UnlockBean;->dayBeginningTime:J

    return-wide v0
.end method

.method public getTotalUnlock()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/UnlockBean;->totalUnlock:I

    return v0
.end method

.method public getUnlockList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/greenguard/entity/UnlockBean;->unlockList:Ljava/util/List;

    return-object v0
.end method

.method public setDayBeginningTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/greenguard/entity/UnlockBean;->dayBeginningTime:J

    return-void
.end method

.method public setTotalUnlock(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/UnlockBean;->totalUnlock:I

    return-void
.end method

.method public setUnlockList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/greenguard/entity/UnlockBean;->unlockList:Ljava/util/List;

    return-void
.end method

.method public setmFirstUnlockTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/greenguard/entity/UnlockBean;->mFirstUnlockTime:J

    return-void
.end method
