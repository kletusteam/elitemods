.class public Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;
.super Landroid/app/Service;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    api = 0x1a
.end annotation


# static fields
.field private static final a:J

.field private static final b:[B


# instance fields
.field private c:I

.field private d:Z

.field private e:Landroid/app/PendingIntent;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Landroid/os/HandlerThread;

.field private i:Landroid/os/Handler;

.field private j:Landroid/app/Notification$Builder;

.field private k:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    sput-wide v0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a:J

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->b:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->f:Z

    new-instance v0, Lcom/miui/greenguard/devicelimit/b;

    invoke-direct {v0, p0}, Lcom/miui/greenguard/devicelimit/b;-><init>(Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;)V

    iput-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->k:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;)I
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getBeginTime()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getBeginTime()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method private a(I)Landroid/app/Notification;
    .locals 3

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.android.settings.usagestats_devicelimit"

    invoke-direct {v0, v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-direct {p0, p1}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->c(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-direct {p0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->c()Landroid/app/PendingIntent;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    const p1, 0x7f08018b

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;
    .locals 3

    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->j:Landroid/app/Notification$Builder;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.android.settings.usagestats_devicelimit"

    invoke-direct {v0, v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->j:Landroid/app/Notification$Builder;

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->j:Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->j:Landroid/app/Notification$Builder;

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->b(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    iget-object p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->j:Landroid/app/Notification$Builder;

    invoke-direct {p0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->c()Landroid/app/PendingIntent;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    iget-object p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->j:Landroid/app/Notification$Builder;

    const p2, 0x7f08018b

    invoke-virtual {p1, p2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    iget-object p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->j:Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f08018f

    invoke-static {p2, p3}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/drawable/Icon;)Landroid/app/Notification$Builder;

    iget-object p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->j:Landroid/app/Notification$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p2

    invoke-virtual {p1, p2, p3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    iget-object p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->j:Landroid/app/Notification$Builder;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    iget-object p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->j:Landroid/app/Notification$Builder;

    invoke-virtual {p1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/util/List;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;",
            ">;)",
            "Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_a

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_3

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move-object v2, v0

    move-object v3, v2

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getBeginTime()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getEndTime()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getBeginTime()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getEndTime()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/xiaomi/misettings/usagestats/utils/L;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    if-nez v3, :cond_3

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :cond_3
    if-nez v2, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_4
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_5
    sget-object v1, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->b:[B

    monitor-enter v1

    if-eqz v2, :cond_6

    :try_start_0
    invoke-interface {p1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_2

    :cond_6
    :goto_1
    if-eqz v3, :cond_7

    invoke-interface {p1, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    :cond_7
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v1, Lcom/miui/greenguard/devicelimit/a;->a:Lcom/miui/greenguard/devicelimit/a;

    invoke-static {p1, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_8
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getBeginTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getEndTime()Ljava/lang/String;

    move-result-object v3

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    const-wide/16 v6, 0x1e

    mul-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(Ljava/lang/String;Ljava/lang/String;J)I

    move-result v2

    iput v2, v1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->offForbiddenMinute:I

    iget v2, v1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->offForbiddenMinute:I

    if-eqz v2, :cond_8

    return-object v1

    :cond_9
    return-object v0

    :goto_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_a
    :goto_3
    return-object v0
.end method

.method private a()V
    .locals 5

    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/app/Service;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/H;->d(Landroid/content/Context;)Landroid/app/NotificationChannelGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannelGroup(Landroid/app/NotificationChannelGroup;)V

    new-instance v1, Landroid/app/NotificationChannel;

    const v2, 0x7f130031

    invoke-virtual {p0, v2}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.android.settings.usagestats_devicelimit"

    const/4 v4, 0x2

    invoke-direct {v1, v3, v2, v4}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v2}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->enableLights(Z)V

    const-string v2, "app_timer"

    invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->setGroup(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "isProlong"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->f:Z

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ensureIsProlong: isProlong="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->f:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "GreenGardDeviceLimitMonitorService"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic a(Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->g()V

    return-void
.end method

.method private a(Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f1301cb

    invoke-virtual {p0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getBeginTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getEndTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getBeginTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getEndTime()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    invoke-direct {p0, v1, p1, v2}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a(Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;

    move-result-object p1

    const v1, 0x1aefa

    invoke-virtual {p0, v1, p1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    invoke-direct {p0, v0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "jumpToTimeOver:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GreenGardDeviceLimitMonitorService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->c(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lb/e/a/b/b;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "jumpToTimeOver: currentTopPkg="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->c()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;

    move-result-object v4

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->c(J)Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "jumpToTimeOver: allProlongPackageNames="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ProlongPackageNames="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v4, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    sget-object v4, Lcom/xiaomi/misettings/usagestats/devicelimit/CurrentNoLimitAppMonitorService;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;

    move-result-object v4

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/f;->d(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    sget-object v3, Lcom/miui/greenguard/manager/j;->b:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    const-string v0, "jumpToTimeOver: startTimeOverActivity"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/i/h;->b(Landroid/content/Context;)V

    invoke-static {p0, v2, p1}, Lcom/xiaomi/misettings/usagestats/TimeoverActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v2}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->c(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    :goto_1
    const-string p1, "startSuspendAllApps"

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v4}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->c(Landroid/content/Context;Ljava/lang/String;)V

    :cond_4
    :goto_2
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/CharSequence;
    .locals 6

    int-to-long v0, p3

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v0, v2

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez p3, :cond_0

    const p3, 0x7f130371

    new-array v0, v2, [Ljava/lang/Object;

    aput-object p1, v0, v4

    aput-object p2, v0, v3

    invoke-virtual {p0, p3, v0}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const p3, 0x7f130374

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v4

    aput-object p2, v5, v3

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v5, v2

    invoke-virtual {p0, p3, v5}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private b()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/service/MainProcessService;->a(Landroid/content/Context;Z)V

    return-void
.end method

.method private b(I)V
    .locals 8

    iget-boolean v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->d(I)V

    goto :goto_0

    :cond_0
    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    int-to-long v2, p1

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v4, v2

    cmp-long v4, v4, v0

    if-gtz v4, :cond_1

    invoke-direct {p0, p1}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->d(I)V

    goto :goto_0

    :cond_1
    const v4, 0x1aefa

    invoke-direct {p0, p1}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a(I)Landroid/app/Notification;

    move-result-object p1

    invoke-virtual {p0, v4, p1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v2, v4

    sub-long/2addr v2, v0

    const-wide/16 v0, 0x1e

    mul-long v6, v4, v0

    cmp-long p1, v2, v6

    if-lez p1, :cond_2

    mul-long v2, v4, v0

    :cond_2
    iget-object p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->k:Ljava/lang/Runnable;

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void
.end method

.method private c()Landroid/app/PendingIntent;
    .locals 3

    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->e:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.action.usagestas.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.xiaomi.misettings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    const/high16 v2, 0x4000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->e:Landroid/app/PendingIntent;

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->e:Landroid/app/PendingIntent;

    return-object v0
.end method

.method private c(I)Ljava/lang/CharSequence;
    .locals 9

    int-to-long v0, p1

    sget-wide v2, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v0, v2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez p1, :cond_0

    const p1, 0x7f130375

    new-array v0, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget v3, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->c:I

    int-to-long v3, v3

    sget-wide v5, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v3, v5

    invoke-static {v1, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, p1, v0}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    iget-boolean p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->d:Z

    const v4, 0x7f130372

    if-eqz p1, :cond_2

    const-wide/16 v5, 0x2

    sget-wide v7, Lcom/xiaomi/misettings/usagestats/utils/L;->f:J

    mul-long/2addr v7, v5

    cmp-long p1, v0, v7

    if-gtz p1, :cond_1

    new-array p1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v2

    invoke-virtual {p0, v4, p1}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-array p1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->b(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v2

    invoke-virtual {p0, v4, p1}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    new-array p1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v2

    invoke-virtual {p0, v4, p1}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private d()I
    .locals 5

    new-instance v0, Lcom/xiaomi/misettings/usagestats/f/g;

    new-instance v1, Lcom/xiaomi/misettings/usagestats/f/j;

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-direct {v1, v4, v2, v3}, Lcom/xiaomi/misettings/usagestats/f/j;-><init>(Ljava/util/Calendar;J)V

    invoke-direct {v0, v1}, Lcom/xiaomi/misettings/usagestats/f/g;-><init>(Lcom/xiaomi/misettings/usagestats/f/j;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/g;)V

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/g;->d()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/H;->a(Landroid/content/Context;J)V

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/f/g;->e()I

    move-result v0

    return v0
.end method

.method private d(I)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a(I)Landroid/app/Notification;

    move-result-object p1

    const v0, 0x1aefa

    invoke-virtual {p0, v0, p1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    iget-object p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->k:Ljava/lang/Runnable;

    sget-wide v1, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private e()Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;
    .locals 2

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->d()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/l;->b(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;

    invoke-static {v0, v1}, Lb/e/a/b/c;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a(Ljava/util/List;)Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private f()V
    .locals 5

    invoke-direct {p0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->d()I

    move-result v0

    iget v1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->c:I

    sub-int/2addr v1, v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remainTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "min,totalUsageTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "min"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "GreenGardDeviceLimitMonitorService"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-gtz v1, :cond_0

    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->h()V

    goto/16 :goto_2

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remainTime=>0"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->e()Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "inForbiddenPeriods:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, v0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->offForbiddenMinute:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-eqz v0, :cond_3

    iget v3, v0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->offForbiddenMinute:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    invoke-direct {p0, v0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a(Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;)V

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getEndTime()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/L;->a(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x1e

    if-le v0, v1, :cond_2

    const-wide/16 v0, 0x1e

    sget-wide v3, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    mul-long/2addr v3, v0

    goto :goto_0

    :cond_2
    sget-wide v3, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a:J

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "forbidden over check delay time "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lb/e/a/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    :cond_3
    const-string v3, "not in forbinden"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_4

    iget v2, v0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->offForbiddenMinute:I

    if-lez v2, :cond_4

    if-ge v2, v1, :cond_4

    const v1, 0x1aefa

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getBeginTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->getEndTime()Ljava/lang/String;

    move-result-object v3

    iget v0, v0, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;->offForbiddenMinute:I

    invoke-direct {p0, v2, v3, v0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a(Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->k:Ljava/lang/Runnable;

    sget-wide v2, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_4
    invoke-direct {p0, v1}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->b(I)V

    :goto_1
    invoke-direct {p0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->b()V

    :goto_2
    return-void
.end method

.method private g()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->f()V

    return-void
.end method

.method private h()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a(I)Landroid/app/Notification;

    move-result-object v0

    const v1, 0x1aefa

    invoke-virtual {p0, v1, v0}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "GreenGardDeviceLimitMonitorService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->i:Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "device limit monitor"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->h:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->h:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->i:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a()V

    const v0, 0x7f130373

    invoke-virtual {p0, v0}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->g:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->h:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Service;->stopForeground(Z)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->b(Landroid/content/Context;)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    const-string p2, "GreenGardDeviceLimitMonitorService"

    const-string p3, "onStartCommand:"

    invoke-static {p2, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->d()Z

    move-result p3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "weekdayTodayRemote:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, p3}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result p3

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->g(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/i/h;->m(Landroid/content/Context;)Z

    move-result p2

    if-nez p2, :cond_2

    invoke-static {p1, v1}, Lcom/xiaomi/misettings/usagestats/service/MainProcessService;->a(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_0
    iput p3, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->c:I

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onStartCommand: commonLimitTime="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->d:Z

    iget p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->c:I

    if-lez p1, :cond_1

    iget-object p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object p2, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->k:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const p1, 0x1aefa

    iget p2, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->c:I

    invoke-direct {p0, p2}, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->a(I)Landroid/app/Notification;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    iget-object p1, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->i:Landroid/os/Handler;

    iget-object p2, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->k:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "onStartCommand: limit time is not available "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p3, p0, Lcom/miui/greenguard/devicelimit/GreenGardDeviceLimitMonitorService;->c:I

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    const/4 p1, 0x1

    return p1
.end method
