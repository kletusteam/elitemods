.class public Lcom/miui/greenguard/a/a;
.super Ljava/lang/Object;


# direct methods
.method public static a(Lcom/xiaomi/misettings/usagestats/f/k;)Lcom/miui/greenguard/entity/UnlockBean;
    .locals 5

    new-instance v0, Lcom/miui/greenguard/entity/UnlockBean;

    invoke-direct {v0}, Lcom/miui/greenguard/entity/UnlockBean;-><init>()V

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result v1

    const-string v2, "DataConvertAdapter"

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/misettings/common/utils/h;->a()Lb/a/b/q;

    move-result-object v1

    invoke-virtual {v1, p0}, Lb/a/b/q;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/k;->c()Lcom/xiaomi/misettings/usagestats/f/i;

    move-result-object p0

    iget-object v1, p0, Lcom/xiaomi/misettings/usagestats/f/i;->d:Ljava/util/List;

    iput-object v1, v0, Lcom/miui/greenguard/entity/UnlockBean;->unlockTimeStamps:Ljava/util/List;

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/i;->e()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/entity/UnlockBean;->setUnlockList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/i;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/entity/UnlockBean;->setTotalUnlock(I)V

    invoke-virtual {p0}, Lcom/xiaomi/misettings/usagestats/f/i;->b()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/miui/greenguard/entity/UnlockBean;->setmFirstUnlockTime(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "covertDayUsageDetailToUnlock error"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0
.end method
