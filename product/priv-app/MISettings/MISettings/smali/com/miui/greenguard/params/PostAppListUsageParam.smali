.class public Lcom/miui/greenguard/params/PostAppListUsageParam;
.super Lb/e/b/d/c;


# instance fields
.field private appFlows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/greenguard/upload/model/AppUsageBean;",
            ">;"
        }
    .end annotation
.end field

.field private deviceId:Ljava/lang/String;

.field private occurTime:J

.field private version:J


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lb/e/b/d/c;-><init>()V

    sget-object v0, Lcom/miui/greenguard/b/a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/greenguard/params/PostAppListUsageParam;->deviceId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAppFlows()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/upload/model/AppUsageBean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/greenguard/params/PostAppListUsageParam;->appFlows:Ljava/util/List;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/params/PostAppListUsageParam;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    const-string v0, "/ward/app-traffic"

    return-object v0
.end method

.method public getResultClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lb/e/b/g/b;",
            ">;"
        }
    .end annotation

    const-class v0, Lb/e/b/g/a;

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    iget-wide v0, p0, Lcom/miui/greenguard/params/PostAppListUsageParam;->version:J

    return-wide v0
.end method

.method public setAppFlows(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/upload/model/AppUsageBean;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/greenguard/params/PostAppListUsageParam;->appFlows:Ljava/util/List;

    return-void
.end method

.method public setDeviceId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostAppListUsageParam;->deviceId:Ljava/lang/String;

    return-void
.end method

.method public setOccurTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/greenguard/params/PostAppListUsageParam;->occurTime:J

    return-void
.end method

.method public setVersion(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/greenguard/params/PostAppListUsageParam;->version:J

    return-void
.end method
