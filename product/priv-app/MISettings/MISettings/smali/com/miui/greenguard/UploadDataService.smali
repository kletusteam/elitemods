.class public Lcom/miui/greenguard/UploadDataService;
.super Landroid/app/IntentService;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "upload app usage data..."

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    const-string p1, "UploadDataService"

    const-string v0, "onHandleIntent: ready to upload data"

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/IntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->c(Landroid/content/Context;Z)Lcom/xiaomi/misettings/usagestats/f/g;

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/Y;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/Y;->c(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/IntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/Y;->d(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/a/q;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/misettings/usagestats/a/q;->c()V

    const-string v0, "onHandleIntent: has over data"

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method
