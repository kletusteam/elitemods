.class public Lcom/miui/greenguard/push/payload/AppSwitchBodyData;
.super Lcom/miui/greenguard/push/payload/BaseBodyData;


# instance fields
.field private enable:Z

.field private pkgName:Ljava/lang/String;

.field private status:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/push/payload/BaseBodyData;-><init>()V

    return-void
.end method


# virtual methods
.method public getPkgName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->pkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()I
    .locals 1

    iget-boolean v0, p0, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->enable:Z

    return v0
.end method

.method public isEnable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->enable:Z

    return v0
.end method

.method public setEnable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->enable:Z

    return-void
.end method

.method public setPkgName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->pkgName:Ljava/lang/String;

    return-void
.end method

.method public setStatus(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->status:I

    return-void
.end method
