.class public Lcom/miui/greenguard/manager/l;
.super Ljava/lang/Object;


# direct methods
.method private static a()V
    .locals 0

    return-void
.end method

.method public static a(J)V
    .locals 5

    const-string v0, "BizSvr_device_Ctrl_Manager"

    const-string v1, "restartAllPolicyService"

    invoke-static {v0, v1}, Lb/e/a/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v1

    new-instance v2, Lcom/miui/greenguard/manager/a;

    invoke-direct {v2, v0}, Lcom/miui/greenguard/manager/a;-><init>(Landroid/content/Context;)V

    const-wide/16 v3, 0x64

    cmp-long v0, p0, v3

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 p0, 0x1f4

    :goto_0
    invoke-virtual {v1, v2, p0, p1}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    sget-boolean p0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz p0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/miui/greenguard/manager/l;->a()V

    invoke-static {}, Lcom/miui/greenguard/manager/p;->b()V

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object p0

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result p0

    if-eqz p0, :cond_1

    const-wide/16 v0, 0xbb8

    invoke-static {v0, v1}, Lcom/miui/greenguard/manager/l;->a(J)V

    :cond_1
    return-void
.end method

.method static synthetic b(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->e(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Z)V

    invoke-static {p0, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->a(Landroid/content/Context;Z)V

    invoke-static {p0}, Lcom/miui/greenguard/upload/d;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->j(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/i/h;->a()V

    return-void
.end method
