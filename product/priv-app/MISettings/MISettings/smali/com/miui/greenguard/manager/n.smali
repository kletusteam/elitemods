.class final Lcom/miui/greenguard/manager/n;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/greenguard/manager/ExtraRouteManager$Entity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/miui/greenguard/manager/ExtraRouteManager$Entity;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/miui/greenguard/manager/ExtraRouteManager$Entity;
    .locals 1

    new-instance v0, Lcom/miui/greenguard/manager/ExtraRouteManager$Entity;

    invoke-direct {v0, p1}, Lcom/miui/greenguard/manager/ExtraRouteManager$Entity;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/miui/greenguard/manager/n;->createFromParcel(Landroid/os/Parcel;)Lcom/miui/greenguard/manager/ExtraRouteManager$Entity;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/miui/greenguard/manager/ExtraRouteManager$Entity;
    .locals 0

    const/4 p1, 0x0

    new-array p1, p1, [Lcom/miui/greenguard/manager/ExtraRouteManager$Entity;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/miui/greenguard/manager/n;->newArray(I)[Lcom/miui/greenguard/manager/ExtraRouteManager$Entity;

    move-result-object p1

    return-object p1
.end method
