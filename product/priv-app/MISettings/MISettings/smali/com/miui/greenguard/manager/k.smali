.class public Lcom/miui/greenguard/manager/k;
.super Ljava/lang/Object;


# static fields
.field public static a:Z

.field private static b:Z


# direct methods
.method public static a()V
    .locals 4

    sget-boolean v0, Lcom/miui/greenguard/manager/k;->a:Z

    const/4 v1, 0x1

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, -0x1

    const-string v3, "misettings_debug"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    sput-boolean v1, Lcom/miui/greenguard/manager/k;->b:Z

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lb/e/a/b/d;->a()V

    :cond_2
    return-void
.end method

.method public static b()[Ljava/lang/String;
    .locals 5

    sget-boolean v0, Lcom/miui/greenguard/manager/k;->b:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f030022

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    const/4 v3, 0x1

    add-int/2addr v2, v3

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "1"

    aput-object v4, v2, v1

    array-length v4, v0

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2

    :cond_0
    new-array v0, v1, [Ljava/lang/String;

    return-object v0
.end method

.method public static c()Z
    .locals 1

    sget-boolean v0, Lcom/miui/greenguard/manager/k;->b:Z

    return v0
.end method
