.class public Lcom/miui/greenguard/upload/d;
.super Ljava/lang/Object;


# static fields
.field private static final a:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    sget-wide v0, Lcom/xiaomi/misettings/usagestats/utils/L;->e:J

    const-wide/16 v2, 0x14

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/miui/greenguard/upload/d;->a:J

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 0

    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 6

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v2

    const-wide/16 v3, 0x0

    const-string v5, "PRE_LOAD_USAGESTATS_KEY"

    invoke-virtual {v2, v5, v3, v4}, Lcom/misettings/common/utils/p;->a(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/miui/greenguard/upload/a;

    invoke-direct {v1, p0}, Lcom/miui/greenguard/upload/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->b(Ljava/lang/Runnable;)V

    invoke-static {p0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v1

    invoke-virtual {v0, v5, v1, v2}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;J)V

    :goto_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    invoke-static {p0}, Lcom/miui/greenguard/upload/d;->c(Landroid/content/Context;)V

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result v0

    if-nez v0, :cond_3

    return-void

    :cond_3
    new-instance v0, Lcom/miui/greenguard/upload/b;

    invoke-direct {v0}, Lcom/miui/greenguard/upload/b;-><init>()V

    invoke-static {p0, v0}, Lcom/miui/greenguard/manager/j;->a(Landroid/content/Context;Lb/e/b/b/a;)V

    invoke-static {p0}, Lcom/miui/greenguard/manager/j;->d(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/miui/greenguard/manager/j;->e(Landroid/content/Context;)V

    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 2

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lb/c/b/b/d;->a()Lb/c/b/b/d;

    move-result-object v0

    new-instance v1, Lcom/miui/greenguard/upload/c;

    invoke-direct {v1, p0}, Lcom/miui/greenguard/upload/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lb/c/b/b/d;->a(Ljava/lang/Runnable;)V

    return-void
.end method
