.class public Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;
.super Lcom/miui/greenguard/push/payload/BaseBodyData;


# instance fields
.field public continuousDuration:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "continuousDuration"
    .end annotation
.end field

.field public enable:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "enable"
    .end annotation
.end field

.field public restTime:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "restTime"
    .end annotation
.end field

.field public status:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/push/payload/BaseBodyData;-><init>()V

    return-void
.end method


# virtual methods
.method public getStatus()I
    .locals 1

    iget-boolean v0, p0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->enable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    iget v0, p0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->status:I

    return v0
.end method

.method public setContinuousDuration(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->continuousDuration:I

    return-void
.end method

.method public setEnable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->enable:Z

    return-void
.end method

.method public setRestTime(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->restTime:I

    return-void
.end method
