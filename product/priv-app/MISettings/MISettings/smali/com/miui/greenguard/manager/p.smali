.class public Lcom/miui/greenguard/manager/p;
.super Ljava/lang/Object;


# direct methods
.method static synthetic a(Lcom/miui/greenguard/result/DeviceRestrictionResult$DataBean;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/greenguard/manager/p;->b(Lcom/miui/greenguard/result/DeviceRestrictionResult$DataBean;)V

    return-void
.end method

.method static synthetic a(Z)V
    .locals 2

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    new-instance v1, Lcom/miui/greenguard/manager/c;

    invoke-direct {v1, p0}, Lcom/miui/greenguard/manager/c;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/manager/a/g;->a(Lcom/miui/greenguard/manager/a/g$b;)V

    return-void
.end method

.method static synthetic a()Z
    .locals 1

    invoke-static {}, Lcom/miui/greenguard/manager/p;->c()Z

    move-result v0

    return v0
.end method

.method public static b()V
    .locals 1

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-static {}, Lcom/miui/greenguard/manager/p;->c()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/miui/greenguard/manager/p;->c(Z)V

    :cond_2
    return-void
.end method

.method private static b(Lcom/miui/greenguard/result/DeviceRestrictionResult$DataBean;)V
    .locals 11

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    invoke-virtual {p0}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DataBean;->getApplication()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ge v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DataBean;->getApplication()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/greenguard/result/DeviceRestrictionResult$ApplicationBean;

    new-instance v7, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;

    invoke-direct {v7}, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;-><init>()V

    invoke-virtual {v5}, Lcom/miui/greenguard/result/DeviceRestrictionResult$ApplicationBean;->isEnable()Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->setEnable(Z)V

    invoke-virtual {v5}, Lcom/miui/greenguard/result/DeviceRestrictionResult$ApplicationBean;->getPkgName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->setPkgName(Ljava/lang/String;)V

    new-instance v8, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;

    invoke-direct {v8}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;-><init>()V

    invoke-virtual {v5}, Lcom/miui/greenguard/result/DeviceRestrictionResult$ApplicationBean;->getWorkingDay()Lcom/miui/greenguard/result/DeviceRestrictionResult$DayConfigBean;

    move-result-object v9

    invoke-virtual {v9}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DayConfigBean;->getDurationPerDay()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->setDurationPerDay(I)V

    invoke-virtual {v5}, Lcom/miui/greenguard/result/DeviceRestrictionResult$ApplicationBean;->getPkgName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->setPkgName(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->setPolicyType(I)V

    new-instance v9, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;

    invoke-direct {v9}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;-><init>()V

    invoke-virtual {v5}, Lcom/miui/greenguard/result/DeviceRestrictionResult$ApplicationBean;->getHoliday()Lcom/miui/greenguard/result/DeviceRestrictionResult$DayConfigBean;

    move-result-object v10

    invoke-virtual {v10}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DayConfigBean;->getDurationPerDay()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->setDurationPerDay(I)V

    invoke-virtual {v5}, Lcom/miui/greenguard/result/DeviceRestrictionResult$ApplicationBean;->getPkgName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v5}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->setPkgName(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->setPolicyType(I)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    new-instance v4, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;

    invoke-direct {v4}, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;-><init>()V

    invoke-virtual {p0}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DataBean;->getMandatoryRest()Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->getContinuousDuration()I

    move-result v5

    iput v5, v4, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->continuousDuration:I

    invoke-virtual {p0}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DataBean;->getMandatoryRest()Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->isEnable()Z

    move-result v5

    iput-boolean v5, v4, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->enable:Z

    invoke-virtual {p0}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DataBean;->getMandatoryRest()Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->getRestTime()I

    move-result v5

    iput v5, v4, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;->restTime:I

    move v5, v3

    :goto_1
    invoke-virtual {p0}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DataBean;->getAppType()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v5, v7, :cond_1

    invoke-virtual {p0}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DataBean;->getAppType()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/miui/greenguard/result/DeviceRestrictionResult$AppTypeBean;

    new-instance v8, Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;

    invoke-direct {v8}, Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;-><init>()V

    invoke-virtual {v7}, Lcom/miui/greenguard/result/DeviceRestrictionResult$AppTypeBean;->getAppType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;->setAppType(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/miui/greenguard/result/DeviceRestrictionResult$AppTypeBean;->isEnable()Z

    move-result v9

    invoke-virtual {v8, v9}, Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;->setEnable(Z)V

    new-instance v9, Lcom/miui/greenguard/c/a/h;

    invoke-direct {v9, v0, v8}, Lcom/miui/greenguard/c/a/h;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;)V

    invoke-virtual {v9}, Lcom/miui/greenguard/c/a/a/d;->c()V

    new-instance v8, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;

    invoke-direct {v8}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;-><init>()V

    invoke-virtual {v8, v3}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->setPolicyType(I)V

    invoke-virtual {v7}, Lcom/miui/greenguard/result/DeviceRestrictionResult$AppTypeBean;->getAppType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->setAppType(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/miui/greenguard/result/DeviceRestrictionResult$AppTypeBean;->getWorkingDay()Lcom/miui/greenguard/result/DeviceRestrictionResult$DayConfigBean;

    move-result-object v9

    invoke-virtual {v9}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DayConfigBean;->getDurationPerDay()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->setDurationPerDay(I)V

    new-instance v9, Lcom/miui/greenguard/c/a/g;

    invoke-direct {v9, v0, v8}, Lcom/miui/greenguard/c/a/g;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;)V

    invoke-virtual {v9}, Lcom/miui/greenguard/c/a/a/d;->c()V

    new-instance v8, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;

    invoke-direct {v8}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;-><init>()V

    invoke-virtual {v8, v6}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->setPolicyType(I)V

    invoke-virtual {v7}, Lcom/miui/greenguard/result/DeviceRestrictionResult$AppTypeBean;->getAppType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->setAppType(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/miui/greenguard/result/DeviceRestrictionResult$AppTypeBean;->getHoliday()Lcom/miui/greenguard/result/DeviceRestrictionResult$DayConfigBean;

    move-result-object v7

    invoke-virtual {v7}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DayConfigBean;->getDurationPerDay()I

    move-result v7

    invoke-virtual {v8, v7}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->setDurationPerDay(I)V

    new-instance v7, Lcom/miui/greenguard/c/a/g;

    invoke-direct {v7, v0, v8}, Lcom/miui/greenguard/c/a/g;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;)V

    invoke-virtual {v7}, Lcom/miui/greenguard/c/a/a/d;->c()V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    new-instance v3, Lcom/miui/greenguard/c/a/f;

    invoke-direct {v3, v0, v1}, Lcom/miui/greenguard/c/a/f;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v3}, Lcom/miui/greenguard/c/a/a/d;->c()V

    new-instance v1, Lcom/miui/greenguard/c/a/e;

    invoke-direct {v1, v0, v2}, Lcom/miui/greenguard/c/a/e;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v1}, Lcom/miui/greenguard/c/a/a/d;->c()V

    new-instance v1, Lcom/miui/greenguard/c/a/i;

    invoke-virtual {p0}, Lcom/miui/greenguard/result/DeviceRestrictionResult$DataBean;->getDevice()Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    move-result-object p0

    invoke-direct {v1, v0, p0}, Lcom/miui/greenguard/c/a/i;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;)V

    invoke-virtual {v1}, Lcom/miui/greenguard/c/a/i;->f()Lcom/miui/greenguard/c/a/i;

    invoke-virtual {v1}, Lcom/miui/greenguard/c/a/a/d;->c()V

    new-instance p0, Lcom/miui/greenguard/c/a/j;

    invoke-direct {p0, v0, v4}, Lcom/miui/greenguard/c/a/j;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/d;->c()V

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object p0

    const-string v0, "has_save_config"

    invoke-virtual {p0, v0, v6}, Lb/e/a/b/h;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic b(Z)V
    .locals 0

    invoke-static {p0}, Lcom/miui/greenguard/manager/p;->d(Z)V

    return-void
.end method

.method private static c(Z)V
    .locals 2

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    new-instance v1, Lcom/miui/greenguard/manager/b;

    invoke-direct {v1, p0}, Lcom/miui/greenguard/manager/b;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/manager/a/g;->b(Lcom/miui/greenguard/manager/a/g$c;)V

    return-void
.end method

.method private static c()Z
    .locals 3

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    const-string v1, "has_save_config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lb/e/a/b/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static d(Z)V
    .locals 2

    new-instance v0, Lcom/miui/greenguard/params/DeviceRestrictionParam;

    invoke-direct {v0}, Lcom/miui/greenguard/params/DeviceRestrictionParam;-><init>()V

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/greenguard/manager/a/g;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/params/GetBaseParam;->setDeviceId(Ljava/lang/String;)V

    new-instance v1, Lcom/miui/greenguard/manager/o;

    invoke-direct {v1, p0}, Lcom/miui/greenguard/manager/o;-><init>(Z)V

    invoke-static {v0, v1}, Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;

    return-void
.end method
