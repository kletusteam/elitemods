.class public Lcom/miui/greenguard/c/a/k;
.super Lcom/miui/greenguard/c/a/a/d;


# instance fields
.field private final a:[I

.field private b:Landroid/content/Context;

.field private c:Lcom/miui/greenguard/push/payload/ProlongAppBodyData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/ProlongAppBodyData;)V
    .locals 1

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/a/d;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/miui/greenguard/c/a/k;->a:[I

    iput-object p1, p0, Lcom/miui/greenguard/c/a/k;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/miui/greenguard/c/a/k;->c:Lcom/miui/greenguard/push/payload/ProlongAppBodyData;

    return-void

    :array_0
    .array-data 4
        0x14
        0x1e
        0x28
        0x7fffffff
    .end array-data
.end method

.method private e()I
    .locals 2

    iget-object v0, p0, Lcom/miui/greenguard/c/a/k;->c:Lcom/miui/greenguard/push/payload/ProlongAppBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/ProlongAppBodyData;->getExtendTime()I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/miui/greenguard/c/a/k;->c:Lcom/miui/greenguard/push/payload/ProlongAppBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/ProlongAppBodyData;->getExtendTime()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/miui/greenguard/c/a/k;->a:[I

    iget-object v1, p0, Lcom/miui/greenguard/c/a/k;->c:Lcom/miui/greenguard/push/payload/ProlongAppBodyData;

    invoke-virtual {v1}, Lcom/miui/greenguard/push/payload/ProlongAppBodyData;->getExtendTime()I

    move-result v1

    aget v0, v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected a()V
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/c/a/k;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    return-void
.end method

.method protected b()V
    .locals 2

    iget-object v0, p0, Lcom/miui/greenguard/c/a/k;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/miui/greenguard/c/a/k;->c:Lcom/miui/greenguard/push/payload/ProlongAppBodyData;

    invoke-virtual {v1}, Lcom/miui/greenguard/push/payload/ProlongAppBodyData;->getPkgName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method

.method protected d()V
    .locals 8

    iget-object v0, p0, Lcom/miui/greenguard/c/a/k;->c:Lcom/miui/greenguard/push/payload/ProlongAppBodyData;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/ProlongAppBodyData;->getPkgName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/c/a/k;->c:Lcom/miui/greenguard/push/payload/ProlongAppBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/ProlongAppBodyData;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const-string v0, "cmd-prolong"

    const-string v1, "disagree to prolong time"

    invoke-static {v0, v1}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/miui/greenguard/c/a/k;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/miui/greenguard/c/a/k;->c:Lcom/miui/greenguard/push/payload/ProlongAppBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/ProlongAppBodyData;->getPkgName()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static/range {v2 .. v7}, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->a(Landroid/content/Context;Ljava/lang/String;JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/L;->f(J)I

    move-result v0

    iget-object v1, p0, Lcom/miui/greenguard/c/a/k;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/miui/greenguard/c/a/k;->c:Lcom/miui/greenguard/push/payload/ProlongAppBodyData;

    invoke-virtual {v2}, Lcom/miui/greenguard/push/payload/ProlongAppBodyData;->getPkgName()Ljava/lang/String;

    move-result-object v2

    int-to-long v3, v0

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/k;->e()I

    move-result v0

    invoke-static {v1, v2, v3, v4, v0}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->a(Landroid/content/Context;Ljava/lang/String;JI)V

    :cond_2
    :goto_0
    return-void
.end method
