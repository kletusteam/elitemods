.class public Lcom/miui/greenguard/params/PostUnlockParam;
.super Lb/e/b/d/c;


# instance fields
.field private deviceId:Ljava/lang/String;

.field private occurTime:J

.field private unlockTimes:I

.field private unlocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lb/e/b/d/c;-><init>()V

    sget-object v0, Lcom/miui/greenguard/b/a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/greenguard/params/PostUnlockParam;->deviceId:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/greenguard/params/PostUnlockParam;->unlocks:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/params/PostUnlockParam;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getOccurTime()J
    .locals 2

    iget-wide v0, p0, Lcom/miui/greenguard/params/PostUnlockParam;->occurTime:J

    return-wide v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    const-string v0, "/ward/unlock"

    return-object v0
.end method

.method public getResultClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lb/e/b/g/b;",
            ">;"
        }
    .end annotation

    const-class v0, Lb/e/b/g/a;

    return-object v0
.end method

.method public getUnlockTimes()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/params/PostUnlockParam;->unlockTimes:I

    return v0
.end method

.method public getUnlocks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/greenguard/params/PostUnlockParam;->unlocks:Ljava/util/List;

    return-object v0
.end method

.method public setDeviceId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostUnlockParam;->deviceId:Ljava/lang/String;

    return-void
.end method

.method public setOccurTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/greenguard/params/PostUnlockParam;->occurTime:J

    return-void
.end method

.method public setUnlockTimes(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/params/PostUnlockParam;->unlockTimes:I

    return-void
.end method

.method public setUnlocks(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/greenguard/params/PostUnlockParam;->unlocks:Ljava/util/List;

    return-void
.end method
