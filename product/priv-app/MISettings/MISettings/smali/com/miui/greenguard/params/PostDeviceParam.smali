.class public Lcom/miui/greenguard/params/PostDeviceParam;
.super Lb/e/b/d/c;


# instance fields
.field private clientVersion:Ljava/lang/String;

.field private deviceType:Ljava/lang/String;

.field private model:Ljava/lang/String;

.field private oaid:Ljava/lang/String;

.field private os:Ljava/lang/String;

.field private osSoftwareVersion:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "osSoftwareVersion"
    .end annotation
.end field

.field private versionNum:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lb/e/b/d/c;-><init>()V

    const-string v0, "Android"

    iput-object v0, p0, Lcom/miui/greenguard/params/PostDeviceParam;->os:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getPath()Ljava/lang/String;
    .locals 1

    const-string v0, "/ward/device"

    return-object v0
.end method

.method public getResultClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lb/e/b/g/b;",
            ">;"
        }
    .end annotation

    const-class v0, Lcom/miui/greenguard/result/DeviceIdResult;

    return-object v0
.end method

.method public setClientVersion(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostDeviceParam;->clientVersion:Ljava/lang/String;

    return-void
.end method

.method public setDeviceType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostDeviceParam;->deviceType:Ljava/lang/String;

    return-void
.end method

.method public setModel(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostDeviceParam;->model:Ljava/lang/String;

    return-void
.end method

.method public setOaid(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostDeviceParam;->oaid:Ljava/lang/String;

    return-void
.end method

.method public setOs(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostDeviceParam;->os:Ljava/lang/String;

    return-void
.end method

.method public setOsSoftwareVersion(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostDeviceParam;->osSoftwareVersion:Ljava/lang/String;

    return-void
.end method

.method public setVersionNum(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostDeviceParam;->versionNum:Ljava/lang/String;

    return-void
.end method
