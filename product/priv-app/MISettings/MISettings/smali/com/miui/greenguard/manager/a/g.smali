.class public Lcom/miui/greenguard/manager/a/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/greenguard/manager/a/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/greenguard/manager/a/g$c;,
        Lcom/miui/greenguard/manager/a/g$b;,
        Lcom/miui/greenguard/manager/a/g$a;
    }
.end annotation


# static fields
.field private static a:Lcom/miui/greenguard/entity/FamilyBean;

.field public static b:Z


# instance fields
.field private final c:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/greenguard/manager/a/g;->c:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/greenguard/manager/a/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/manager/a/g;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/miui/greenguard/manager/a/g$c;)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/miui/greenguard/manager/a/g$c;->call()V

    :cond_0
    return-void
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const/4 v0, 0x0

    const-string v1, "xiaomi_account_is_child"

    invoke-static {p0, v1, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    move v0, v1

    :cond_0
    return v0
.end method

.method public static e()Lcom/miui/greenguard/manager/a/g;
    .locals 1

    sget-object v0, Lcom/miui/greenguard/manager/a/g$a;->a:Lcom/miui/greenguard/manager/a/g;

    return-object v0
.end method

.method private o()Lcom/miui/greenguard/entity/FamilyBean;
    .locals 3

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v1, "account_info"

    invoke-virtual {v0, v1}, Lcom/misettings/common/utils/p;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/misettings/common/utils/h;->a()Lb/a/b/q;

    move-result-object v1

    const-class v2, Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v1, v0, v2}, Lb/a/b/q;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/greenguard/entity/FamilyBean;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/greenguard/entity/FamilyBean;

    invoke-direct {v0}, Lcom/miui/greenguard/entity/FamilyBean;-><init>()V

    :cond_0
    return-object v0
.end method

.method private p()V
    .locals 3

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, La/m/a/b;->a(Landroid/content/Context;)La/m/a/b;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "miui.token.change"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, La/m/a/b;->a(Landroid/content/Intent;)Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    const-string v0, "MiSettingAccountManager"

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/misettings/usagestats/utils/F;->a(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/F;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/utils/F;->c()V

    invoke-static {}, Lb/e/a/a/e;->c()Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/miui/greenguard/manager/m;->a(Landroid/content/Context;Z)V

    const-string v1, "accountChange exit:"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v1

    const-string v3, "has_save_config"

    invoke-virtual {v1, v3, v2}, Lb/e/a/b/h;->b(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->b()V

    invoke-static {}, Lb/e/a/a/e;->a()V

    invoke-direct {p0}, Lcom/miui/greenguard/manager/a/g;->p()V

    goto :goto_0

    :cond_1
    const-string v1, "accountChange login:"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lb/e/a/a/e;->a()V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/miui/greenguard/manager/a/g;->b(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "accountChange error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public a(Lcom/miui/greenguard/entity/FamilyBean;)V
    .locals 2

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    invoke-static {}, Lcom/misettings/common/utils/h;->a()Lb/a/b/q;

    move-result-object v1

    invoke-virtual {v1, p1}, Lb/a/b/q;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "account_info"

    invoke-virtual {v0, v1, p1}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->n()V

    return-void
.end method

.method public a(Lcom/miui/greenguard/manager/a/g$b;)V
    .locals 1

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->i()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/greenguard/manager/m;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/greenguard/manager/m;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/miui/greenguard/manager/a/d;

    invoke-direct {v0, p0, p1}, Lcom/miui/greenguard/manager/a/d;-><init>(Lcom/miui/greenguard/manager/a/g;Lcom/miui/greenguard/manager/a/g$b;)V

    invoke-static {v0}, Lcom/miui/greenguard/manager/m;->a(Lb/e/b/b/a;)V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_4

    invoke-interface {p1}, Lcom/miui/greenguard/manager/a/g$b;->call()V

    :cond_4
    :goto_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/entity/FamilyBean;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v1}, Lcom/miui/greenguard/entity/FamilyBean;->isOwner()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {p0, p1}, Lcom/miui/greenguard/manager/a/g;->a(Lcom/miui/greenguard/entity/FamilyBean;)V

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method public synthetic a(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->m()V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->f()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/miui/greenguard/manager/m;->b(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/b/a/p;->a()Lcom/xiaomi/misettings/usagestats/b/a/p;

    move-result-object p1

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/xiaomi/misettings/usagestats/b/a/p;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->k()V

    invoke-static {}, Lcom/miui/greenguard/manager/p;->b()V

    invoke-direct {p0}, Lcom/miui/greenguard/manager/a/g;->p()V

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/miui/greenguard/upload/d;->b(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)Z
    .locals 4

    const-string v0, "com.miui.greenguard"

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/m;->e(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    invoke-static {p1, v0}, Lb/e/a/b/b;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v2

    if-eqz v1, :cond_0

    const-wide/32 v0, 0x13488c5

    cmp-long p1, v2, v0

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b()V
    .locals 3

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/misettings/common/utils/p;->a(Landroid/content/Context;)Lcom/misettings/common/utils/p;

    move-result-object v0

    const-string v1, "account_info"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/misettings/common/utils/p;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public b(Lcom/miui/greenguard/manager/a/g$c;)V
    .locals 1

    invoke-static {}, Lb/e/a/a/e;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/miui/greenguard/manager/a/b;

    invoke-direct {v0, p1}, Lcom/miui/greenguard/manager/a/b;-><init>(Lcom/miui/greenguard/manager/a/g$c;)V

    invoke-static {v0}, Lb/e/a/a/e;->a(Lb/e/a/a/h;)V

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lcom/miui/greenguard/manager/a/g$c;->call()V

    :goto_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/miui/greenguard/manager/a/a;

    invoke-direct {v0, p0, p1}, Lcom/miui/greenguard/manager/a/a;-><init>(Lcom/miui/greenguard/manager/a/g;Z)V

    invoke-static {v0}, Lb/e/a/a/e;->a(Lb/e/a/a/h;)V

    return-void
.end method

.method public c()Lcom/miui/greenguard/entity/FamilyBean;
    .locals 1

    sget-object v0, Lcom/miui/greenguard/manager/a/g;->a:Lcom/miui/greenguard/entity/FamilyBean;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/miui/greenguard/manager/a/g;->o()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v0

    sput-object v0, Lcom/miui/greenguard/manager/a/g;->a:Lcom/miui/greenguard/entity/FamilyBean;

    :cond_0
    sget-object v0, Lcom/miui/greenguard/manager/a/g;->a:Lcom/miui/greenguard/entity/FamilyBean;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/greenguard/manager/m;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 5

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->g()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/greenguard/manager/m;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/greenguard/manager/a/g;->b(Landroid/content/Context;)Z

    move-result v0

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "childAccount system"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiSettingAccountManager"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "childAccount  AccountUtils.getXiaomiAccount()"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lb/e/a/a/e;->c()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->i()Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    invoke-static {}, Lb/e/a/a/e;->c()Landroid/accounts/Account;

    move-result-object v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_3
    return v0

    :cond_4
    :goto_0
    return v1
.end method

.method public g()Z
    .locals 1

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    return v0
.end method

.method public h()Z
    .locals 2

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->g()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    sget-boolean v0, Lcom/miui/greenguard/manager/a/g;->b:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->i()Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->c()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/greenguard/entity/FamilyBean;->isOrganizer()Z

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/greenguard/manager/a/g;->c:Z

    return v0
.end method

.method public j()V
    .locals 1

    new-instance v0, Lcom/miui/greenguard/manager/a/f;

    invoke-direct {v0, p0}, Lcom/miui/greenguard/manager/a/f;-><init>(Lcom/miui/greenguard/manager/a/g;)V

    invoke-static {v0}, Lcom/miui/greenguard/manager/m;->a(Lb/e/b/b/a;)V

    return-void
.end method

.method public k()V
    .locals 2

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/greenguard/manager/m;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/miui/greenguard/params/GetFamilyParam;

    invoke-direct {v0}, Lcom/miui/greenguard/params/GetFamilyParam;-><init>()V

    new-instance v1, Lcom/miui/greenguard/manager/a/e;

    invoke-direct {v1, p0}, Lcom/miui/greenguard/manager/a/e;-><init>(Lcom/miui/greenguard/manager/a/g;)V

    invoke-static {v0, v1}, Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;

    :cond_0
    return-void
.end method

.method public l()V
    .locals 1

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/greenguard/manager/a/g;->b(Z)V

    return-void
.end method

.method public m()V
    .locals 1

    invoke-virtual {p0}, Lcom/miui/greenguard/manager/a/g;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/greenguard/manager/a/g;->a(Lcom/miui/greenguard/manager/a/g$b;)V

    :cond_0
    return-void
.end method

.method public n()V
    .locals 3

    invoke-static {}, Lcom/miui/greenguard/manager/a/g;->e()Lcom/miui/greenguard/manager/a/g;

    move-result-object v0

    invoke-direct {v0}, Lcom/miui/greenguard/manager/a/g;->o()Lcom/miui/greenguard/entity/FamilyBean;

    move-result-object v0

    sput-object v0, Lcom/miui/greenguard/manager/a/g;->a:Lcom/miui/greenguard/entity/FamilyBean;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateAccountInfo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/misettings/common/utils/h;->a()Lb/a/b/q;

    move-result-object v1

    sget-object v2, Lcom/miui/greenguard/manager/a/g;->a:Lcom/miui/greenguard/entity/FamilyBean;

    invoke-virtual {v1, v2}, Lb/a/b/q;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiSettingAccountManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
