.class public Lcom/miui/greenguard/params/PostMandatoryRestParam;
.super Lb/e/b/d/c;


# instance fields
.field private continuousDuration:I

.field private deviceId:Ljava/lang/String;

.field private enable:Z

.field private restTime:I

.field private uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lb/e/b/d/c;-><init>()V

    sget-object v0, Lcom/miui/greenguard/b/a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/greenguard/params/PostMandatoryRestParam;->deviceId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/params/PostMandatoryRestParam;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    const-string v0, "/admin/device/mandatory-rest"

    return-object v0
.end method

.method public getResultClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lb/e/b/g/b;",
            ">;"
        }
    .end annotation

    const-class v0, Lb/e/b/g/a;

    return-object v0
.end method

.method public setContinuousDuration(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/params/PostMandatoryRestParam;->continuousDuration:I

    return-void
.end method

.method public setDeviceId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostMandatoryRestParam;->deviceId:Ljava/lang/String;

    return-void
.end method

.method public setEnable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/greenguard/params/PostMandatoryRestParam;->enable:Z

    return-void
.end method

.method public setRestTime(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/params/PostMandatoryRestParam;->restTime:I

    return-void
.end method

.method public setUid(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/params/PostMandatoryRestParam;->uid:Ljava/lang/String;

    return-void
.end method
