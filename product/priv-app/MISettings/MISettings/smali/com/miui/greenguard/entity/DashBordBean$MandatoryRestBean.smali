.class public Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/greenguard/entity/DashBordBean;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MandatoryRestBean"
.end annotation


# instance fields
.field private continuousDuration:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "continuousDuration"
    .end annotation
.end field

.field private enable:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "enable"
    .end annotation
.end field

.field private restTime:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "restTime"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContinuousDuration()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->continuousDuration:I

    return v0
.end method

.method public getRestTime()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->restTime:I

    return v0
.end method

.method public isEnable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->enable:Z

    return v0
.end method

.method public setContinuousDuration(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->continuousDuration:I

    return-void
.end method

.method public setEnable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->enable:Z

    return-void
.end method

.method public setRestTime(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->restTime:I

    return-void
.end method

.method public toSteadyOnDetail()Lcom/xiaomi/misettings/usagestats/d/d/j$a;
    .locals 2

    new-instance v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;

    invoke-direct {v0}, Lcom/xiaomi/misettings/usagestats/d/d/j$a;-><init>()V

    invoke-virtual {p0}, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->isEnable()Z

    move-result v1

    iput-boolean v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->c:Z

    iget v1, p0, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->restTime:I

    div-int/lit8 v1, v1, 0x3c

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->b:I

    iget v1, p0, Lcom/miui/greenguard/entity/DashBordBean$MandatoryRestBean;->continuousDuration:I

    div-int/lit8 v1, v1, 0x3c

    iput v1, v0, Lcom/xiaomi/misettings/usagestats/d/d/j$a;->a:I

    return-object v0
.end method
