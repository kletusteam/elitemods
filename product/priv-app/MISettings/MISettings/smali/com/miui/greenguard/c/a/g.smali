.class public Lcom/miui/greenguard/c/a/g;
.super Lcom/miui/greenguard/c/a/a/c;


# instance fields
.field private b:Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/greenguard/c/a/a/c;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/miui/greenguard/c/a/g;->b:Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;

    return-void
.end method


# virtual methods
.method protected d()V
    .locals 5

    invoke-super {p0}, Lcom/miui/greenguard/c/a/a/d;->d()V

    iget-object v0, p0, Lcom/miui/greenguard/c/a/g;->b:Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/c;->g()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->getDurationPerDay()I

    move-result v3

    sget v4, Lcom/xiaomi/misettings/usagestats/utils/L;->b:I

    div-int/2addr v3, v4

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->getPolicyType()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v2, v3, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->a(Landroid/content/Context;Ljava/lang/String;IZ)V

    return-void
.end method

.method protected e()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/miui/greenguard/c/a/g;->b:Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->getAppType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/greenguard/c/a/g;->b:Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->getAppType()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/xiaomi/misettings/usagestats/b/a/g;->g:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/c/a/g;->b:Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->getCategoryId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/greenguard/c/a/g;->b:Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;->getCategoryId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/miui/greenguard/c/a/a/c;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
