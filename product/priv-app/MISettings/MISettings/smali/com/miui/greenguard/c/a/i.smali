.class public Lcom/miui/greenguard/c/a/i;
.super Lcom/miui/greenguard/c/a/a/d;


# instance fields
.field private a:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

.field private b:Landroid/content/Context;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/a/d;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/greenguard/c/a/i;->c:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;)V
    .locals 1

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/a/d;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/greenguard/c/a/i;->c:Z

    iput-object p2, p0, Lcom/miui/greenguard/c/a/i;->a:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    iput-object p1, p0, Lcom/miui/greenguard/c/a/i;->b:Landroid/content/Context;

    return-void
.end method

.method protected static a(Landroid/content/Context;Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;
    .locals 3

    new-instance v0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    invoke-direct {v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "weekDaycreate:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;Z)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DoDevicePolicyCmd"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;Z)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->setStatus(I)V

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->isEnable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->setEnable(Z)V

    invoke-static {}, Lcom/miui/greenguard/c/a/i;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->setEnable(Z)V

    :cond_0
    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->d(Landroid/content/Context;Z)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3c

    if-nez v1, :cond_2

    if-eqz p1, :cond_1

    const/16 v1, 0x4650

    goto :goto_0

    :cond_1
    const/16 v1, 0x7080

    :cond_2
    :goto_0
    invoke-virtual {v0, v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->setDurationPerDay(I)V

    invoke-static {p0, p1}, Lcom/xiaomi/misettings/usagestats/controller/l;->b(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object p0

    const-class p1, Lcom/xiaomi/misettings/usagestats/devicelimit/model/DeviceUnUsableTimeInfo;

    invoke-static {p0, p1}, Lb/e/a/b/c;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, Lcom/miui/greenguard/c/a/a/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->setUnit(Ljava/util/List;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;
    .locals 2

    new-instance v0, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    invoke-direct {v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;-><init>()V

    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/miui/greenguard/c/a/i;->a(Landroid/content/Context;Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->setWorkingDay(Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;)V

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/miui/greenguard/c/a/i;->a(Landroid/content/Context;Z)Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->setHoliday(Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;)V

    invoke-static {}, Lcom/miui/greenguard/c/a/i;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;)V

    :cond_0
    return-object v0
.end method

.method private a(Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;Z)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->isEnable()Z

    move-result v0

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getDurationPerDay()I

    move-result v1

    div-int/lit8 v1, v1, 0x3c

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;->getUnit()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/miui/greenguard/c/a/a/d;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iget-object v2, p0, Lcom/miui/greenguard/c/a/i;->b:Landroid/content/Context;

    invoke-static {v2, v0, p2}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;ZZ)V

    iget-object v0, p0, Lcom/miui/greenguard/c/a/i;->b:Landroid/content/Context;

    invoke-static {v0, v1, p2}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;IZ)V

    iget-object v0, p0, Lcom/miui/greenguard/c/a/i;->b:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/xiaomi/misettings/usagestats/controller/l;->a(Landroid/content/Context;Ljava/util/List;Z)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "weekDay:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, "-"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/miui/greenguard/c/a/i;->b:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/xiaomi/misettings/usagestats/controller/l;->c(Landroid/content/Context;Z)I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "DoDevicePolicyCmd"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic b(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->e(Landroid/content/Context;)V

    return-void
.end method

.method private static g()Z
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->h(Landroid/content/Context;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    const-string v0, "DoDevicePolicyCmd"

    const-string v1, "isAdapterOldVersion error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected a()V
    .locals 5

    iget-object v0, p0, Lcom/miui/greenguard/c/a/i;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v1

    new-instance v2, Lcom/miui/greenguard/c/a/c;

    invoke-direct {v2, v0}, Lcom/miui/greenguard/c/a/c;-><init>(Landroid/content/Context;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method protected b()V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 4

    iget-object v0, p0, Lcom/miui/greenguard/c/a/i;->a:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->getWorkingDay()Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/greenguard/c/a/i;->a:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    invoke-virtual {v1}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->getHoliday()Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/greenguard/c/a/i;->a:Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    invoke-virtual {v2}, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;->getUnrestrictedApps()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v0, v3}, Lcom/miui/greenguard/c/a/i;->a(Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;Z)V

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/miui/greenguard/c/a/i;->a(Lcom/miui/greenguard/push/payload/DevicePolicyBodyData$DevicePolicyDayBean;Z)V

    iget-boolean v0, p0, Lcom/miui/greenguard/c/a/i;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/greenguard/c/a/i;->b:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/miui/greenguard/c/a/l;->a(Landroid/content/Context;Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method public e()V
    .locals 1

    invoke-static {}, Lcom/miui/greenguard/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/controller/l;->e(Landroid/content/Context;)V

    return-void
.end method

.method public f()Lcom/miui/greenguard/c/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/greenguard/c/a/i;->c:Z

    return-object p0
.end method
