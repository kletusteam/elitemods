.class public Lcom/miui/greenguard/c/b;
.super Ljava/lang/Object;


# direct methods
.method public static final a(Ljava/lang/String;)Lcom/miui/greenguard/c/c;
    .locals 17

    move-object/from16 v0, p0

    const-string v1, "dealMessageJson2 -- "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dealMessageJson data="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "HttpUtils--push"

    invoke-static {v3, v2}, Lb/e/a/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {p0 .. p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    return-object v4

    :cond_0
    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "data"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "no data..."

    invoke-static {v3, v0}, Lb/e/a/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    :cond_1
    new-instance v7, Lcom/miui/greenguard/c/c;

    invoke-direct {v7}, Lcom/miui/greenguard/c/c;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move v9, v6

    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v9, v10, :cond_9

    new-instance v10, Lcom/miui/greenguard/c/c$a;

    invoke-direct {v10}, Lcom/miui/greenguard/c/c$a;-><init>()V

    invoke-virtual {v0, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    const-string v12, "header"

    invoke-virtual {v11, v12}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    const-string v13, "body"

    invoke-virtual {v11, v13}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    new-instance v13, Lcom/miui/greenguard/c/c$a$a;

    invoke-direct {v13}, Lcom/miui/greenguard/c/c$a$a;-><init>()V

    const-string v14, "cmd"

    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v14

    if-eqz v14, :cond_2

    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v15

    if-lez v15, :cond_2

    invoke-virtual {v14, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v14

    goto :goto_1

    :cond_2
    const-string v14, ""

    :goto_1
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/miui/greenguard/c/c$a$a;->a(Ljava/lang/String;)V

    const-string v14, "cmdnum"

    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v13, v14}, Lcom/miui/greenguard/c/c$a$a;->a(I)V

    const-string v14, "flownum"

    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Lcom/miui/greenguard/c/c$a$a;->b(Ljava/lang/String;)V

    invoke-virtual {v10, v13}, Lcom/miui/greenguard/c/c$a;->a(Lcom/miui/greenguard/c/c$a$a;)V

    invoke-virtual {v13}, Lcom/miui/greenguard/c/c$a$a;->a()Ljava/lang/String;

    move-result-object v12

    const/4 v14, -0x1

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v15

    const/16 v4, 0x682

    if-eq v15, v4, :cond_7

    const/16 v4, 0x69f

    if-eq v15, v4, :cond_6

    const/16 v4, 0x6be

    if-eq v15, v4, :cond_5

    const/16 v4, 0x69c

    if-eq v15, v4, :cond_4

    const/16 v4, 0x69d

    if-eq v15, v4, :cond_3

    packed-switch v15, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    const-string v4, "43"

    invoke-virtual {v12, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x4

    goto :goto_3

    :pswitch_1
    const-string v4, "42"

    invoke-virtual {v12, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v2

    goto :goto_3

    :pswitch_2
    const-string v4, "41"

    invoke-virtual {v12, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v6

    goto :goto_3

    :cond_3
    const-string v4, "52"

    invoke-virtual {v12, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x3

    goto :goto_3

    :cond_4
    const-string v4, "51"

    invoke-virtual {v12, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v5

    goto :goto_3

    :cond_5
    const-string v4, "64"

    invoke-virtual {v12, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x7

    goto :goto_3

    :cond_6
    const-string v4, "54"

    invoke-virtual {v12, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x6

    goto :goto_3

    :cond_7
    const-string v4, "46"

    invoke-virtual {v12, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x5

    goto :goto_3

    :cond_8
    :goto_2
    move v4, v14

    :goto_3
    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_3
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v11, Lcom/miui/greenguard/push/payload/UnLimitAppBody;

    invoke-static {v4, v11}, Lb/e/a/b/c;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/push/payload/BaseBodyData;

    goto/16 :goto_5

    :pswitch_4
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v11, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;

    invoke-static {v4, v11}, Lb/e/a/b/c;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/push/payload/BaseBodyData;

    goto :goto_5

    :pswitch_5
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v11, Lcom/miui/greenguard/push/payload/ProlongAppBodyData;

    invoke-static {v4, v11}, Lb/e/a/b/c;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/push/payload/BaseBodyData;

    goto :goto_5

    :pswitch_6
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v11, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    invoke-static {v4, v11}, Lb/e/a/b/c;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/push/payload/BaseBodyData;

    goto :goto_5

    :pswitch_7
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v11, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;

    invoke-static {v4, v11}, Lb/e/a/b/c;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/push/payload/BaseBodyData;

    goto :goto_5

    :pswitch_8
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v11, Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;

    invoke-static {v4, v11}, Lb/e/a/b/c;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/push/payload/BaseBodyData;

    goto :goto_5

    :pswitch_9
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v11, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;

    invoke-static {v4, v11}, Lb/e/a/b/c;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/push/payload/BaseBodyData;

    goto :goto_5

    :pswitch_a
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v11, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;

    invoke-static {v4, v11}, Lb/e/a/b/c;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/push/payload/BaseBodyData;

    goto :goto_5

    :goto_4
    const-string v4, "%shere no more on cmd %d!"

    new-array v11, v2, [Ljava/lang/Object;

    aput-object v1, v11, v6

    invoke-virtual {v13}, Lcom/miui/greenguard/c/c$a$a;->a()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v5

    invoke-static {v4, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    :goto_5
    invoke-virtual {v10, v4}, Lcom/miui/greenguard/c/c$a;->a(Lcom/miui/greenguard/push/payload/BaseBodyData;)V

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v7, v8}, Lcom/miui/greenguard/c/c;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v16, v7

    goto :goto_6

    :catch_0
    move-exception v0

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    const-string v0, "%sexception: %s"

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v16, 0x0

    :goto_6
    return-object v16

    nop

    :pswitch_data_0
    .packed-switch 0x67d
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
