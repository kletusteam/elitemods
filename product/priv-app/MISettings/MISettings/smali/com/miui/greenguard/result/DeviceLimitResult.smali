.class public Lcom/miui/greenguard/result/DeviceLimitResult;
.super Lb/e/b/g/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/greenguard/result/DeviceLimitResult$AppInfo;,
        Lcom/miui/greenguard/result/DeviceLimitResult$DayBean;,
        Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;
    }
.end annotation


# instance fields
.field private dataBean:Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lb/e/b/g/a;-><init>()V

    return-void
.end method


# virtual methods
.method public getDataBean()Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/result/DeviceLimitResult;->dataBean:Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;

    return-object v0
.end method

.method public isEnable()Z
    .locals 3

    iget-object v0, p0, Lcom/miui/greenguard/result/DeviceLimitResult;->dataBean:Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;->access$000(Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;)Lcom/miui/greenguard/result/DeviceLimitResult$DayBean;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/greenguard/result/DeviceLimitResult;->dataBean:Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;

    invoke-static {v0}, Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;->access$000(Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;)Lcom/miui/greenguard/result/DeviceLimitResult$DayBean;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/greenguard/result/DeviceLimitResult$DayBean;->access$100(Lcom/miui/greenguard/result/DeviceLimitResult$DayBean;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/miui/greenguard/result/DeviceLimitResult;->dataBean:Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;

    invoke-static {v0}, Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;->access$200(Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;)Lcom/miui/greenguard/result/DeviceLimitResult$DayBean;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/greenguard/result/DeviceLimitResult;->dataBean:Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;

    invoke-static {v0}, Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;->access$200(Lcom/miui/greenguard/result/DeviceLimitResult$DataBean;)Lcom/miui/greenguard/result/DeviceLimitResult$DayBean;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/greenguard/result/DeviceLimitResult$DayBean;->access$100(Lcom/miui/greenguard/result/DeviceLimitResult$DayBean;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    :cond_1
    return v1
.end method
