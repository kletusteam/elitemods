.class public Lcom/miui/greenguard/c/a/a/c;
.super Lcom/miui/greenguard/c/a/a/d;


# instance fields
.field a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/a/d;-><init>()V

    iput-object p1, p0, Lcom/miui/greenguard/c/a/a/c;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 5

    invoke-super {p0}, Lcom/miui/greenguard/c/a/a/d;->a()V

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/c;->g()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v1

    new-instance v2, Lcom/miui/greenguard/c/a/a/a;

    invoke-direct {v2, p0, v0}, Lcom/miui/greenguard/c/a/a/a;-><init>(Lcom/miui/greenguard/c/a/a/c;Landroid/content/Context;)V

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public synthetic a(Landroid/content/Context;)V
    .locals 5

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/c;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    if-eqz v3, :cond_1

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->c()Z

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/j;->d(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v1

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "todayLimitTime:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "/todayUsageTime:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "BaseCategoryLimitCmd"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sub-int/2addr v1, v2

    invoke-static {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/j;->b(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-static {p1}, Lcom/xiaomi/misettings/usagestats/utils/j;->k(Landroid/content/Context;)V

    goto :goto_1

    :cond_1
    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/utils/j;->b(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/xiaomi/misettings/usagestats/controller/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p1, v0, v2}, Lcom/xiaomi/misettings/usagestats/controller/b;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    :goto_1
    return-void
.end method

.method protected b()V
    .locals 0

    invoke-super {p0}, Lcom/miui/greenguard/c/a/a/d;->b()V

    return-void
.end method

.method protected e()Ljava/lang/String;
    .locals 0

    const p0, 0x0

    throw p0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/c;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/xiaomi/misettings/usagestats/b/a/g;->h:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/c;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public g()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/c/a/a/c;->a:Landroid/content/Context;

    return-object v0
.end method
