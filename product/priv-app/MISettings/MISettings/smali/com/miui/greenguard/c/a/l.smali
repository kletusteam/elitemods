.class public Lcom/miui/greenguard/c/a/l;
.super Lcom/miui/greenguard/c/a/a/d;


# instance fields
.field private a:Lcom/miui/greenguard/push/payload/UnLimitAppBody;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/UnLimitAppBody;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/a/d;-><init>()V

    iput-object p2, p0, Lcom/miui/greenguard/c/a/l;->a:Lcom/miui/greenguard/push/payload/UnLimitAppBody;

    iput-object p1, p0, Lcom/miui/greenguard/c/a/l;->b:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/controller/l;->e(Landroid/content/Context;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/SimpleAppInfo;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->c()Ljava/util/List;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/greenguard/push/payload/SimpleAppInfo;

    invoke-virtual {v5}, Lcom/miui/greenguard/push/payload/SimpleAppInfo;->getPkgName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    move p1, v3

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge p1, v4, :cond_3

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_3
    move p1, v3

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_4

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;

    move-result-object v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->a(Ljava/lang/String;Z)Z

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p0, v1, v3}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    :cond_4
    :goto_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p1

    if-ge v3, p1, :cond_5

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;

    move-result-object p1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/a/b;->a(Ljava/lang/String;Z)Z

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {p0, p1, v1}, Lcom/xiaomi/misettings/usagestats/devicelimit/c/g;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_5
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 5

    iget-object v0, p0, Lcom/miui/greenguard/c/a/l;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v1

    new-instance v2, Lcom/miui/greenguard/c/a/d;

    invoke-direct {v2, v0}, Lcom/miui/greenguard/c/a/d;-><init>(Landroid/content/Context;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method protected b()V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 2

    iget-object v0, p0, Lcom/miui/greenguard/c/a/l;->a:Lcom/miui/greenguard/push/payload/UnLimitAppBody;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/miui/greenguard/c/a/l;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/UnLimitAppBody;->getApplications()Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/greenguard/c/a/l;->a(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method
