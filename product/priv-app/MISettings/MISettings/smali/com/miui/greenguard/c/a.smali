.class public Lcom/miui/greenguard/c/a;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Lcom/miui/greenguard/c/a;


# instance fields
.field private c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/miui/greenguard/c/a;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/greenguard/c/a;->c:Landroid/content/Context;

    return-void
.end method

.method public static final a(Landroid/content/Context;)Lcom/miui/greenguard/c/a;
    .locals 2

    sget-object v0, Lcom/miui/greenguard/c/a;->b:Lcom/miui/greenguard/c/a;

    if-nez v0, :cond_1

    sget-object v0, Lcom/miui/greenguard/c/a;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/miui/greenguard/c/a;->b:Lcom/miui/greenguard/c/a;

    if-nez v1, :cond_0

    new-instance v1, Lcom/miui/greenguard/c/a;

    invoke-direct {v1, p0}, Lcom/miui/greenguard/c/a;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/miui/greenguard/c/a;->b:Lcom/miui/greenguard/c/a;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    sget-object p0, Lcom/miui/greenguard/c/a;->b:Lcom/miui/greenguard/c/a;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 6

    const-string v0, "PushCommandManager"

    if-nez p1, :cond_0

    const-string p1, "analyzeJson para json is null!"

    invoke-static {v0, p1}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1}, Lcom/miui/greenguard/c/b;->a(Ljava/lang/String;)Lcom/miui/greenguard/c/c;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/miui/greenguard/c/c;->getData()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_9

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    goto/16 :goto_2

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/c/c$a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cmd"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/miui/greenguard/c/c$a;->b()Lcom/miui/greenguard/c/c$a$a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/miui/greenguard/c/c$a$a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/miui/greenguard/c/c$a;->b()Lcom/miui/greenguard/c/c$a$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/greenguard/c/c$a$a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v4

    const/16 v5, 0x682

    if-eq v4, v5, :cond_7

    const/16 v5, 0x69f

    if-eq v4, v5, :cond_6

    const/16 v5, 0x6be

    if-eq v4, v5, :cond_5

    const/16 v5, 0x69c

    if-eq v4, v5, :cond_4

    const/16 v5, 0x69d

    if-eq v4, v5, :cond_3

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    const-string v4, "43"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v3, 0x4

    goto :goto_1

    :pswitch_1
    const-string v4, "42"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v3, 0x2

    goto :goto_1

    :pswitch_2
    const-string v4, "41"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    const-string v4, "52"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v3, 0x3

    goto :goto_1

    :cond_4
    const-string v4, "51"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v3, 0x1

    goto :goto_1

    :cond_5
    const-string v4, "64"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v3, 0x7

    goto :goto_1

    :cond_6
    const-string v4, "54"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v3, 0x6

    goto :goto_1

    :cond_7
    const-string v4, "46"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v3, 0x5

    :cond_8
    :goto_1
    packed-switch v3, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_3
    new-instance v2, Lcom/miui/greenguard/c/a/l;

    iget-object v3, p0, Lcom/miui/greenguard/c/a;->c:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/miui/greenguard/c/c$a;->a()Lcom/miui/greenguard/push/payload/BaseBodyData;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/push/payload/UnLimitAppBody;

    invoke-direct {v2, v3, v1}, Lcom/miui/greenguard/c/a/l;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/UnLimitAppBody;)V

    invoke-virtual {v2}, Lcom/miui/greenguard/c/a/a/d;->c()V

    goto/16 :goto_0

    :pswitch_4
    new-instance v2, Lcom/miui/greenguard/c/a/j;

    iget-object v3, p0, Lcom/miui/greenguard/c/a;->c:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/miui/greenguard/c/c$a;->a()Lcom/miui/greenguard/push/payload/BaseBodyData;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;

    invoke-direct {v2, v3, v1}, Lcom/miui/greenguard/c/a/j;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/MandatoryRestBodyData;)V

    invoke-virtual {v2}, Lcom/miui/greenguard/c/a/a/d;->c()V

    goto/16 :goto_0

    :pswitch_5
    new-instance v2, Lcom/miui/greenguard/c/a/k;

    iget-object v3, p0, Lcom/miui/greenguard/c/a;->c:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/miui/greenguard/c/c$a;->a()Lcom/miui/greenguard/push/payload/BaseBodyData;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/push/payload/ProlongAppBodyData;

    invoke-direct {v2, v3, v1}, Lcom/miui/greenguard/c/a/k;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/ProlongAppBodyData;)V

    invoke-virtual {v2}, Lcom/miui/greenguard/c/a/a/d;->c()V

    goto/16 :goto_0

    :pswitch_6
    new-instance v2, Lcom/miui/greenguard/c/a/i;

    iget-object v3, p0, Lcom/miui/greenguard/c/a;->c:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/miui/greenguard/c/c$a;->a()Lcom/miui/greenguard/push/payload/BaseBodyData;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;

    invoke-direct {v2, v3, v1}, Lcom/miui/greenguard/c/a/i;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/DevicePolicyBodyData;)V

    invoke-virtual {v2}, Lcom/miui/greenguard/c/a/a/d;->c()V

    goto/16 :goto_0

    :pswitch_7
    new-instance v2, Lcom/miui/greenguard/c/a/g;

    iget-object v3, p0, Lcom/miui/greenguard/c/a;->c:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/miui/greenguard/c/c$a;->a()Lcom/miui/greenguard/push/payload/BaseBodyData;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;

    invoke-direct {v2, v3, v1}, Lcom/miui/greenguard/c/a/g;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppTypePolicyBodyData;)V

    invoke-virtual {v2}, Lcom/miui/greenguard/c/a/a/d;->c()V

    goto/16 :goto_0

    :pswitch_8
    new-instance v2, Lcom/miui/greenguard/c/a/h;

    iget-object v3, p0, Lcom/miui/greenguard/c/a;->c:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/miui/greenguard/c/c$a;->a()Lcom/miui/greenguard/push/payload/BaseBodyData;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;

    invoke-direct {v2, v3, v1}, Lcom/miui/greenguard/c/a/h;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppTypeSwitchBodyData;)V

    invoke-virtual {v2}, Lcom/miui/greenguard/c/a/a/d;->c()V

    goto/16 :goto_0

    :pswitch_9
    new-instance v2, Lcom/miui/greenguard/c/a/e;

    iget-object v3, p0, Lcom/miui/greenguard/c/a;->c:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/miui/greenguard/c/c$a;->a()Lcom/miui/greenguard/push/payload/BaseBodyData;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;

    invoke-direct {v2, v3, v1}, Lcom/miui/greenguard/c/a/e;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppPolicyBodyData;)V

    invoke-virtual {v2}, Lcom/miui/greenguard/c/a/a/d;->c()V

    goto/16 :goto_0

    :pswitch_a
    new-instance v2, Lcom/miui/greenguard/c/a/f;

    iget-object v3, p0, Lcom/miui/greenguard/c/a;->c:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/miui/greenguard/c/c$a;->a()Lcom/miui/greenguard/push/payload/BaseBodyData;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;

    invoke-direct {v2, v3, v1}, Lcom/miui/greenguard/c/a/f;-><init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppSwitchBodyData;)V

    invoke-virtual {v2}, Lcom/miui/greenguard/c/a/a/d;->c()V

    goto/16 :goto_0

    :cond_9
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x67d
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
