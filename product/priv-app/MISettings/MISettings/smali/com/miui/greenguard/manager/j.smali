.class public Lcom/miui/greenguard/manager/j;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/miui/greenguard/manager/d;

    invoke-direct {v0}, Lcom/miui/greenguard/manager/d;-><init>()V

    sput-object v0, Lcom/miui/greenguard/manager/j;->a:Ljava/util/List;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/p;->a:Ljava/util/List;

    sput-object v0, Lcom/miui/greenguard/manager/j;->b:Ljava/util/List;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->g:Ljava/util/List;

    sput-object v0, Lcom/miui/greenguard/manager/j;->c:Ljava/util/List;

    sget-object v0, Lcom/xiaomi/misettings/usagestats/utils/AppUsageStatsFactory;->h:Ljava/util/List;

    sput-object v0, Lcom/miui/greenguard/manager/j;->d:Ljava/util/List;

    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    return-object v2

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/utils/H;->l(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const-string v4, "AppControlManager"

    if-eqz p0, :cond_4

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, v5}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/PackageInfo;

    iget-object v6, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    sget-object v7, Lcom/miui/greenguard/manager/j;->a:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    goto :goto_0

    :cond_2
    sget-object v7, Lcom/miui/greenguard/manager/j;->c:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {v6}, Lcom/miui/greenguard/manager/j;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAllStatAppList error"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getInstallAppList: duration="

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v0

    invoke-virtual {p0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ",packageCount="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, p0}, Lb/e/a/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public static a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/k;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/xiaomi/misettings/usagestats/f/k;",
            ")",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/upload/model/AppUsageBean;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lcom/miui/greenguard/manager/j;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_a

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    :cond_0
    if-nez p1, :cond_1

    return-object v1

    :cond_1
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/k;->a()Lcom/xiaomi/misettings/usagestats/f/g;

    move-result-object v0

    if-nez v0, :cond_2

    return-object v1

    :cond_2
    invoke-virtual {p1}, Lcom/xiaomi/misettings/usagestats/f/k;->b()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_3

    return-object v1

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/misettings/usagestats/f/h;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/h;->b()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/misettings/usagestats/f/d;

    if-nez v3, :cond_5

    goto :goto_0

    :cond_5
    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gtz v4, :cond_6

    goto :goto_0

    :cond_6
    new-instance v4, Lcom/miui/greenguard/upload/model/AppUsageBean;

    invoke-direct {v4}, Lcom/miui/greenguard/upload/model/AppUsageBean;-><init>()V

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/BaseAppBeam;->setAppName(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/miui/greenguard/upload/model/BaseAppBeam;->getAppName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/BaseAppBeam;->setAppName(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/b;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/BaseAppBeam;->setPkgName(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v8

    long-to-int v5, v8

    div-int/lit16 v5, v5, 0x3e8

    if-nez v5, :cond_8

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/d;->i()J

    move-result-wide v8

    cmp-long v6, v8, v6

    if-lez v6, :cond_8

    const/4 v5, 0x1

    :cond_8
    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/AppUsageBean;->setUseTime(I)V

    sget-object v5, Lcom/xiaomi/misettings/usagestats/b/a/g;->g:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/xiaomi/misettings/usagestats/f/h;->e()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/AppUsageBean;->setAppType(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/xiaomi/misettings/usagestats/f/d;->h()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/miui/greenguard/upload/model/AppUsageBean;->setDayDetail(Ljava/util/ArrayList;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_9
    return-object v0

    :cond_a
    :goto_1
    return-object v1
.end method

.method static synthetic a(IILjava/util/List;JJLjava/lang/String;Lb/e/b/b/a;)V
    .locals 0

    invoke-static/range {p0 .. p8}, Lcom/miui/greenguard/manager/j;->b(IILjava/util/List;JJLjava/lang/String;Lb/e/b/b/a;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lb/e/b/b/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lb/e/b/b/a<",
            "Lb/e/b/g/a;",
            ">;)V"
        }
    .end annotation

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/d/c;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/f/k;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/miui/greenguard/manager/j;->a(Landroid/content/Context;Lb/e/b/b/a;Lcom/xiaomi/misettings/usagestats/f/k;J)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lb/e/b/b/a;Lcom/xiaomi/misettings/usagestats/f/k;J)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lb/e/b/b/a<",
            "Lb/e/b/g/a;",
            ">;",
            "Lcom/xiaomi/misettings/usagestats/f/k;",
            "J)V"
        }
    .end annotation

    invoke-static {p0, p2}, Lcom/miui/greenguard/manager/j;->a(Landroid/content/Context;Lcom/xiaomi/misettings/usagestats/f/k;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {p2}, Lcom/miui/greenguard/a/a;->a(Lcom/xiaomi/misettings/usagestats/f/k;)Lcom/miui/greenguard/entity/UnlockBean;

    move-result-object p2

    new-instance v0, Lcom/miui/greenguard/manager/f;

    invoke-direct {v0}, Lcom/miui/greenguard/manager/f;-><init>()V

    invoke-static {p0, p2, v0}, Lcom/miui/greenguard/manager/j;->a(Landroid/content/Context;Lcom/miui/greenguard/entity/UnlockBean;Lb/e/b/b/a;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p2

    div-int/lit8 p2, p2, 0x32

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    rem-int/lit8 v0, v0, 0x32

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    add-int v1, p2, v0

    invoke-static {p0}, Lcom/miui/greenguard/manager/m;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    const/4 v0, 0x0

    move-wide v5, p3

    move-object v8, p1

    invoke-static/range {v0 .. v8}, Lcom/miui/greenguard/manager/j;->b(IILjava/util/List;JJLjava/lang/String;Lb/e/b/b/a;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/miui/greenguard/entity/UnlockBean;Lb/e/b/b/a;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/miui/greenguard/entity/UnlockBean;",
            "Lb/e/b/b/a<",
            "Lb/e/b/g/a;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/miui/greenguard/params/PostUnlockParam;

    invoke-direct {v0}, Lcom/miui/greenguard/params/PostUnlockParam;-><init>()V

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/UnlockBean;->getDayBeginningTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/UnlockBean;->getDayBeginningTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/xiaomi/misettings/usagestats/utils/L;->d(J)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/miui/greenguard/entity/UnlockBean;->getDayBeginningTime()J

    move-result-wide v1

    goto :goto_1

    :cond_2
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    :goto_1
    invoke-virtual {v0, v1, v2}, Lcom/miui/greenguard/params/PostUnlockParam;->setOccurTime(J)V

    invoke-static {p0}, Lcom/miui/greenguard/manager/m;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/miui/greenguard/params/PostUnlockParam;->setDeviceId(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/UnlockBean;->getUnlockList()Ljava/util/List;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/miui/greenguard/params/PostUnlockParam;->setUnlocks(Ljava/util/List;)V

    invoke-virtual {p1}, Lcom/miui/greenguard/entity/UnlockBean;->getTotalUnlock()I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/miui/greenguard/params/PostUnlockParam;->setUnlockTimes(I)V

    invoke-static {v0, p2}, Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;

    return-void
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;IILjava/util/List;JLjava/lang/String;)V
    .locals 0

    invoke-static/range {p0 .. p7}, Lcom/miui/greenguard/manager/j;->b(Landroid/content/Context;Ljava/lang/String;IILjava/util/List;JLjava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/pm/PackageInfo;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    iget-object p0, p0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    iget p0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v1, p0, 0x1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    and-int/lit16 p0, p0, 0x80

    if-ne p0, v2, :cond_2

    :cond_1
    move v0, v2

    :cond_2
    :goto_0
    return v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/miui/greenguard/manager/j;->d:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static b(Landroid/content/Context;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/upload/model/AppBean;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lcom/miui/greenguard/manager/j;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/miui/greenguard/upload/model/AppBean;

    invoke-direct {v4}, Lcom/miui/greenguard/upload/model/AppBean;-><init>()V

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {p0, v5}, Lcom/xiaomi/misettings/usagestats/utils/m;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/BaseAppBeam;->setAppName(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/miui/greenguard/upload/model/BaseAppBeam;->getAppName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/BaseAppBeam;->setAppName(Ljava/lang/String;)V

    :cond_2
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/BaseAppBeam;->setPkgName(Ljava/lang/String;)V

    iget-wide v5, v3, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-virtual {v4, v5, v6}, Lcom/miui/greenguard/upload/model/AppBean;->setFirstInstallTime(J)V

    iget-wide v5, v3, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-virtual {v4, v5, v6}, Lcom/miui/greenguard/upload/model/AppBean;->setLastUpdateTime(J)V

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/AppBean;->setVersion(Ljava/lang/String;)V

    :try_start_0
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v6, Lcom/miui/greenguard/b/b;->a:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    const/4 v7, 0x1

    if-eqz v6, :cond_3

    invoke-virtual {v4, v7}, Lcom/miui/greenguard/upload/model/AppBean;->setSource(I)V

    goto :goto_1

    :cond_3
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/AppBean;->setSource(I)V

    goto :goto_1

    :cond_4
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/AppBean;->setSource(I)V

    :goto_1
    invoke-virtual {v4, v7}, Lcom/miui/greenguard/upload/model/AppBean;->setStatus(I)V

    invoke-static {v3}, Lcom/miui/greenguard/manager/j;->a(Landroid/content/pm/PackageInfo;)Z

    move-result v5

    xor-int/2addr v5, v7

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/AppBean;->setCanUninstall(Z)V

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v5}, Lcom/miui/greenguard/manager/j;->b(Ljava/lang/String;)Z

    move-result v5

    xor-int/2addr v5, v7

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/AppBean;->setRestriction(Z)V

    invoke-static {v3}, Lcom/miui/greenguard/manager/j;->b(Landroid/content/pm/PackageInfo;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/miui/greenguard/upload/model/AppBean;->setNewFlag(Z)V

    sget-object v5, Lcom/xiaomi/misettings/usagestats/b/a/g;->g:Ljava/util/Map;

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/xiaomi/misettings/usagestats/b/a/g;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/miui/greenguard/upload/model/AppBean;->setAppType(Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getUploadAppList error"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AppControlManager"

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    return-object v2

    :cond_6
    :goto_2
    const/4 p0, 0x0

    return-object p0
.end method

.method private static b(IILjava/util/List;JJLjava/lang/String;Lb/e/b/b/a;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/upload/model/AppUsageBean;",
            ">;JJ",
            "Ljava/lang/String;",
            "Lb/e/b/b/a<",
            "Lb/e/b/g/a;",
            ">;)V"
        }
    .end annotation

    move v1, p0

    move v2, p1

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uploadAppUsage_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "AppControlManager"

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-lt v1, v2, :cond_0

    new-instance v0, Lb/e/b/g/a;

    invoke-direct {v0}, Lb/e/b/g/a;-><init>()V

    move-object/from16 v9, p8

    invoke-interface {v9, v0}, Lb/e/b/b/a;->a(Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v9, p8

    const/16 v0, 0x32

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    mul-int/lit8 v8, v1, 0x32

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v10

    sub-int/2addr v10, v8

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/2addr v0, v8

    move-object/from16 v10, p2

    invoke-interface {v10, v8, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance v11, Lcom/miui/greenguard/params/PostAppListUsageParam;

    invoke-direct {v11}, Lcom/miui/greenguard/params/PostAppListUsageParam;-><init>()V

    invoke-virtual {v11, v4, v5}, Lcom/miui/greenguard/params/PostAppListUsageParam;->setVersion(J)V

    invoke-virtual {v11, v3}, Lcom/miui/greenguard/params/PostAppListUsageParam;->setAppFlows(Ljava/util/List;)V

    const-wide/16 v12, 0x0

    cmp-long v0, v6, v12

    if-nez v0, :cond_1

    move-wide v12, v4

    goto :goto_0

    :cond_1
    move-wide v12, v6

    :goto_0
    invoke-virtual {v11, v12, v13}, Lcom/miui/greenguard/params/PostAppListUsageParam;->setOccurTime(J)V

    move-object/from16 v8, p7

    invoke-virtual {v11, v8}, Lcom/miui/greenguard/params/PostAppListUsageParam;->setDeviceId(Ljava/lang/String;)V

    new-instance v12, Lcom/miui/greenguard/manager/g;

    move-object v0, v12

    move v1, p0

    move v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/miui/greenguard/manager/g;-><init>(IILjava/util/List;JJLjava/lang/String;Lb/e/b/b/a;)V

    invoke-static {v11, v12}, Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;

    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;IILjava/util/List;JLjava/lang/String;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/upload/model/AppBean;",
            ">;J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move v3, p2

    move v4, p3

    move-wide/from16 v6, p5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "uploadApps"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "=="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppControlManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-lt v4, v3, :cond_0

    invoke-static {p0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v0

    const-string v1, "local_app_list_new"

    move-object v2, p1

    invoke-virtual {v0, v1, p1}, Lb/e/a/b/h;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    move-object v2, p1

    const/16 v0, 0x32

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    mul-int/lit8 v5, v4, 0x32

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v8

    sub-int/2addr v8, v5

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/2addr v0, v5

    move-object/from16 v8, p4

    invoke-interface {v8, v5, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance v9, Lcom/miui/greenguard/params/PutAppListParam;

    invoke-direct {v9}, Lcom/miui/greenguard/params/PutAppListParam;-><init>()V

    invoke-virtual {v9, v6, v7}, Lcom/miui/greenguard/params/PutAppListParam;->setOccurTime(J)V

    invoke-virtual {v9, v6, v7}, Lcom/miui/greenguard/params/PutAppListParam;->setVersion(J)V

    invoke-virtual {v9, v1}, Lcom/miui/greenguard/params/PutAppListParam;->setAppList(Ljava/util/List;)V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/miui/greenguard/params/PutAppListParam;->setTotal(I)V

    move-object/from16 v10, p7

    invoke-virtual {v9, v10}, Lcom/miui/greenguard/params/PutAppListParam;->setDeviceId(Ljava/lang/String;)V

    new-instance v11, Lcom/miui/greenguard/manager/e;

    move-object v0, v11

    move-object v1, p0

    move v3, p2

    move v4, p3

    move-object/from16 v5, p4

    move-wide/from16 v6, p5

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/miui/greenguard/manager/e;-><init>(Landroid/content/Context;Ljava/lang/String;IILjava/util/List;JLjava/lang/String;)V

    invoke-static {v9, v11}, Lb/e/b/l;->a(Lb/e/b/d/e;Lb/e/b/b/a;)Lc/a/a/b/c;

    return-void
.end method

.method public static b(Landroid/content/pm/PackageInfo;)Z
    .locals 4

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    cmp-long p0, v0, v2

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/miui/greenguard/manager/j;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 10

    const-string v0, "AppControlManager"

    const-string v1, "postAppList pre"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/miui/greenguard/manager/j;->b(Landroid/content/Context;)Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_2

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/greenguard/upload/model/AppBean;

    invoke-virtual {v4}, Lcom/miui/greenguard/upload/model/AppBean;->toSimpleString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {p0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v3

    const-string v4, ""

    const-string v5, "account_user_id"

    invoke-virtual {v3, v5, v4}, Lb/e/a/b/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/miui/greenguard/manager/m;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lb/e/a/b/h;->a(Landroid/content/Context;)Lb/e/a/b/h;

    move-result-object v1

    const-string v5, "local_app_list_new"

    invoke-virtual {v1, v5, v4}, Lb/e/a/b/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upload is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saveStr is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_not_upload_"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    div-int/lit8 v4, v1, 0x32

    rem-int/lit8 v5, v1, 0x32

    if-nez v5, :cond_4

    goto :goto_1

    :cond_4
    const/4 v2, 0x1

    :goto_1
    add-int/2addr v4, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app totalCount and update:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v9}, Lcom/miui/greenguard/manager/j;->b(Landroid/content/Context;Ljava/lang/String;IILjava/util/List;JLjava/lang/String;)V

    :cond_5
    :goto_2
    return-void
.end method

.method public static d(Landroid/content/Context;)V
    .locals 6

    const-string v0, "AppControlManager"

    const-string v1, "postHistoryUnlockData"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/o;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/o;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/xiaomi/misettings/usagestats/a/o;->c(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-instance v4, Lcom/miui/greenguard/entity/UnlockBean;

    invoke-direct {v4, v2, v3}, Lcom/miui/greenguard/entity/UnlockBean;-><init>(J)V

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/o;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/o;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/xiaomi/misettings/usagestats/a/o;->a(Lcom/miui/greenguard/entity/UnlockBean;)Z

    new-instance v5, Lcom/miui/greenguard/manager/h;

    invoke-direct {v5, p0, v2, v3}, Lcom/miui/greenguard/manager/h;-><init>(Landroid/content/Context;J)V

    invoke-static {p0, v4, v5}, Lcom/miui/greenguard/manager/j;->a(Landroid/content/Context;Lcom/miui/greenguard/entity/UnlockBean;Lb/e/b/b/a;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    :goto_1
    const-string p0, "postHistoryUnlockData empty"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static e(Landroid/content/Context;)V
    .locals 8

    const-string v0, "AppControlManager"

    const-string v1, "uploadHistoryUsageData"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/xiaomi/misettings/usagestats/a/d;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/a/d;

    move-result-object v1

    invoke-static {}, Lcom/xiaomi/misettings/usagestats/utils/L;->e()J

    move-result-wide v2

    sget-wide v4, Lcom/xiaomi/misettings/usagestats/utils/L;->g:J

    const-wide/16 v6, 0x1f

    mul-long/2addr v4, v6

    sub-long/2addr v2, v4

    invoke-virtual {v1, p0, v2, v3}, Lcom/xiaomi/misettings/usagestats/a/d;->b(Landroid/content/Context;J)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    :cond_0
    invoke-static {}, Lcom/miui/greenguard/manager/k;->c()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    move v2, v3

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "dates is"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    new-instance v0, Lcom/miui/greenguard/manager/i;

    invoke-direct {v0, v4, v5, p0}, Lcom/miui/greenguard/manager/i;-><init>(JLandroid/content/Context;)V

    invoke-static {p0, v4, v5}, Lcom/xiaomi/misettings/usagestats/d/c;->a(Landroid/content/Context;J)Lcom/xiaomi/misettings/usagestats/f/k;

    move-result-object v2

    invoke-static {p0, v0, v2, v4, v5}, Lcom/miui/greenguard/manager/j;->a(Landroid/content/Context;Lb/e/b/b/a;Lcom/xiaomi/misettings/usagestats/f/k;J)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    return-void

    :cond_3
    :goto_2
    const-string p0, "dates isEmpty"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
