.class public Lcom/miui/greenguard/c/a/f;
.super Lcom/miui/greenguard/c/a/a/b;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/AppSwitchBodyData;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/greenguard/c/a/a/b;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/miui/greenguard/push/payload/AppSwitchBodyData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/greenguard/c/a/a/b;-><init>(Landroid/content/Context;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/miui/greenguard/c/a/f;->b:Ljava/util/List;

    iget-object p1, p0, Lcom/miui/greenguard/c/a/f;->b:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/miui/greenguard/push/payload/AppSwitchBodyData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/miui/greenguard/c/a/a/b;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/miui/greenguard/c/a/f;->b:Ljava/util/List;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/miui/greenguard/c/a/f;->c:Z

    return-void
.end method

.method private a(Lcom/miui/greenguard/push/payload/AppSwitchBodyData;)V
    .locals 2

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->isEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/f;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->c(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/f;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->k(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/p;->j(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/f;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/i;->b(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/f;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/controller/i;->c(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/f;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->getPkgName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->getStatus()I

    move-result p1

    invoke-static {v0, v1, p1}, Lcom/xiaomi/misettings/usagestats/utils/p;->b(Landroid/content/Context;Ljava/lang/String;I)V

    return-void
.end method

.method private i()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/c/a/f;->b:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method protected a()V
    .locals 4

    invoke-super {p0}, Lcom/miui/greenguard/c/a/a/d;->a()V

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/f;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/miui/greenguard/c/a/f;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/miui/greenguard/c/a/f;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;

    invoke-virtual {v2}, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->isEnable()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/greenguard/c/a/f;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;

    invoke-virtual {v3}, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->getPkgName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/xiaomi/misettings/usagestats/controller/i;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/misettings/usagestats/utils/ga;->b(Landroid/content/Context;)Lcom/xiaomi/misettings/usagestats/utils/ga;

    move-result-object v0

    new-instance v1, Lcom/miui/greenguard/c/a/b;

    invoke-direct {v1, p0}, Lcom/miui/greenguard/c/a/b;-><init>(Lcom/miui/greenguard/c/a/f;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/xiaomi/misettings/usagestats/utils/ga;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method protected b()V
    .locals 1

    invoke-super {p0}, Lcom/miui/greenguard/c/a/a/d;->b()V

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/f;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/miui/greenguard/c/a/f;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/greenguard/c/a/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    nop

    :cond_1
    return-void
.end method

.method protected d()V
    .locals 2

    invoke-super {p0}, Lcom/miui/greenguard/c/a/a/d;->d()V

    invoke-direct {p0}, Lcom/miui/greenguard/c/a/f;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/miui/greenguard/c/a/f;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/miui/greenguard/c/a/f;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;

    invoke-direct {p0, v1}, Lcom/miui/greenguard/c/a/f;->a(Lcom/miui/greenguard/push/payload/AppSwitchBodyData;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected g()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/miui/greenguard/c/a/f;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;

    invoke-virtual {v0}, Lcom/miui/greenguard/push/payload/AppSwitchBodyData;->getPkgName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()V
    .locals 2

    invoke-virtual {p0}, Lcom/miui/greenguard/c/a/a/b;->f()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/misettings/usagestats/utils/p;->a(Landroid/content/Context;Z)V

    return-void
.end method
