.class public Lcom/miui/greenguard/push/payload/AppPolicyBodyData;
.super Lcom/miui/greenguard/push/payload/BaseBodyData;


# instance fields
.field private durationPerDay:I

.field private limitFlag:I

.field private pkgName:Ljava/lang/String;

.field private policyType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/greenguard/push/payload/BaseBodyData;-><init>()V

    return-void
.end method


# virtual methods
.method public getDurationPerDay()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->durationPerDay:I

    return v0
.end method

.method public getLimitFlag()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->limitFlag:I

    return v0
.end method

.method public getPkgName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->pkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getPolicyType()I
    .locals 1

    iget v0, p0, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->policyType:I

    return v0
.end method

.method public setDurationPerDay(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->durationPerDay:I

    return-void
.end method

.method public setLimitFlag(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->limitFlag:I

    return-void
.end method

.method public setPkgName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->pkgName:Ljava/lang/String;

    return-void
.end method

.method public setPolicyType(I)V
    .locals 0

    iput p1, p0, Lcom/miui/greenguard/push/payload/AppPolicyBodyData;->policyType:I

    return-void
.end method
