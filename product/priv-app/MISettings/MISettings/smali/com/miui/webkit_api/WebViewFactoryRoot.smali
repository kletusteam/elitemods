.class Lcom/miui/webkit_api/WebViewFactoryRoot;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String; = "WebViewFactoryRoot"

.field private static b:Z

.field private static c:Lcom/miui/webkit_api/b/g;

.field private static d:Z

.field private static e:Lcom/miui/webkit_api/b/g;

.field private static final f:Ljava/lang/Object;

.field private static g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/miui/webkit_api/c/ae;

    invoke-direct {v0}, Lcom/miui/webkit_api/c/ae;-><init>()V

    sput-object v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->c:Lcom/miui/webkit_api/b/g;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->d:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->f:Ljava/lang/Object;

    const/4 v0, 0x1

    sput v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->g:I

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Z)V
    .locals 4

    sget-object v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->f:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sput-boolean p0, Lcom/miui/webkit_api/WebViewFactoryRoot;->b:Z

    const-string v1, "WebViewFactoryRoot"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fource using System WebView: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method static a()Z
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/miui/webkit_api/b/g;->a()Z

    move-result v0

    return v0
.end method

.method static b()Z
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/miui/webkit_api/b/g;->b()Z

    move-result v0

    return v0
.end method

.method static c()I
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    sget v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->g:I

    return v0
.end method

.method static d()Lcom/miui/webkit_api/WebView$MiWebViewMode;
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/miui/webkit_api/b/g;->p()Lcom/miui/webkit_api/WebView$MiWebViewMode;

    move-result-object v0

    return-object v0
.end method

.method static e()Lcom/miui/webkit_api/b/g;
    .locals 2

    sget-object v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->f:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lcom/miui/webkit_api/WebViewFactoryRoot;->b:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    sput v1, Lcom/miui/webkit_api/WebViewFactoryRoot;->g:I

    sget-object v1, Lcom/miui/webkit_api/WebViewFactoryRoot;->c:Lcom/miui/webkit_api/b/g;

    monitor-exit v0

    return-object v1

    :cond_0
    sget-boolean v1, Lcom/miui/webkit_api/WebViewFactoryRoot;->d:Z

    if-nez v1, :cond_1

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->f()V

    :cond_1
    sget-object v1, Lcom/miui/webkit_api/WebViewFactoryRoot;->e:Lcom/miui/webkit_api/b/g;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/miui/webkit_api/WebViewFactoryRoot;->e:Lcom/miui/webkit_api/b/g;

    monitor-exit v0

    return-object v1

    :cond_2
    sget-object v1, Lcom/miui/webkit_api/WebViewFactoryRoot;->c:Lcom/miui/webkit_api/b/g;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static f()V
    .locals 2

    const/4 v0, 0x1

    sput-boolean v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->d:Z

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->g()Z

    move-result v0

    const-string v1, "WebViewFactoryRoot"

    if-eqz v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/af;

    invoke-direct {v0}, Lcom/miui/webkit_api/a/af;-><init>()V

    sput-object v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->e:Lcom/miui/webkit_api/b/g;

    const/4 v0, 0x0

    sput v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->g:I

    const-string v0, "Use Mi Webview"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/miui/webkit_api/a/af;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->h()V

    goto :goto_0

    :cond_0
    const-string v0, "Use System Webview"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->h()V

    :cond_1
    :goto_0
    return-void
.end method

.method private static g()Z
    .locals 3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/16 v2, 0x13

    if-ge v0, v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected minimum android sdk version is 19, actual version is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "WebViewFactoryRoot"

    invoke-static {v2, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_0
    invoke-static {}, Lcom/miui/webkit_api/a/af;->d()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/webkit_api/a/af;->e()I

    move-result v0

    sput v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->g:I

    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private static h()V
    .locals 3

    sget-boolean v0, Lcom/miui/webkit_api/WebViewFactoryRoot;->b:Z

    const-string v1, "WebViewFactoryRoot"

    if-eqz v0, :cond_0

    const-string v0, "sForceUsingSystemWebView is ture, so will not download MiWebView"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {}, Lcom/miui/webkit_api/a/af;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "BrowserWebViewFactory.getMiWebViewDir() returns null, so will not download MiWebView"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    sget-object v0, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string v2, "armeabi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/miui/webkit_api/WebView;->getPackageDownloader()Lcom/miui/webkit_api/PackageDownloader;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/miui/webkit_api/WebView;->getPackageDownloader()Lcom/miui/webkit_api/PackageDownloader;

    move-result-object v0

    invoke-static {}, Lcom/miui/webkit_api/a/af;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/miui/webkit_api/PackageDownloader;->download(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "WebView.getPackageDownloader() returns null, so will not download MiWebView"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_4
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected cpu_abi is arm, actual cpu_abi: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", so will not download miui webview chromium."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
