.class Lcom/miui/webkit_api/a/aj;
.super Lcom/miui/webkit_api/MiuiSettings;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/aj$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webview.MiuiSettings"

.field private static final b:Ljava/lang/String; = "MiuiSettingsImpl"


# instance fields
.field private c:Lcom/miui/webkit_api/a/aj$a;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/MiuiSettings;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/aj;->d:Ljava/lang/Object;

    return-void
.end method

.method public static a()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreIntVersion()I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const v2, 0x10002

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    :catch_0
    :cond_0
    return v0
.end method

.method private b()Lcom/miui/webkit_api/a/aj$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/aj;->c:Lcom/miui/webkit_api/a/aj$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/aj$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/aj;->d:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/aj$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/aj;->c:Lcom/miui/webkit_api/a/aj$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/aj;->c:Lcom/miui/webkit_api/a/aj$a;

    return-object v0
.end method


# virtual methods
.method public setIsIncognito(Z)V
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/aj;->b()Lcom/miui/webkit_api/a/aj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/aj;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/aj$a;->a(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setIsIncognito, catch Exception: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "MiuiSettingsImpl"

    invoke-static {v0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
