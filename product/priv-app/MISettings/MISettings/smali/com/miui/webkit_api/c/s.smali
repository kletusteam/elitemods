.class Lcom/miui/webkit_api/c/s;
.super Lcom/miui/webkit_api/WebBackForwardList;


# instance fields
.field private a:Landroid/webkit/WebBackForwardList;


# direct methods
.method public constructor <init>(Landroid/webkit/WebBackForwardList;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebBackForwardList;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/s;->a:Landroid/webkit/WebBackForwardList;

    return-void
.end method


# virtual methods
.method public getCurrentIndex()I
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/s;->a:Landroid/webkit/WebBackForwardList;

    invoke-virtual {v0}, Landroid/webkit/WebBackForwardList;->getCurrentIndex()I

    move-result v0

    return v0
.end method

.method public getCurrentItem()Lcom/miui/webkit_api/WebHistoryItem;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/c/s;->a:Landroid/webkit/WebBackForwardList;

    invoke-virtual {v0}, Landroid/webkit/WebBackForwardList;->getCurrentItem()Landroid/webkit/WebHistoryItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/c/u;

    invoke-direct {v1, v0}, Lcom/miui/webkit_api/c/u;-><init>(Landroid/webkit/WebHistoryItem;)V

    return-object v1
.end method

.method public getItemAtIndex(I)Lcom/miui/webkit_api/WebHistoryItem;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/s;->a:Landroid/webkit/WebBackForwardList;

    invoke-virtual {v0, p1}, Landroid/webkit/WebBackForwardList;->getItemAtIndex(I)Landroid/webkit/WebHistoryItem;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance v0, Lcom/miui/webkit_api/c/u;

    invoke-direct {v0, p1}, Lcom/miui/webkit_api/c/u;-><init>(Landroid/webkit/WebHistoryItem;)V

    return-object v0
.end method

.method public getSize()I
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/s;->a:Landroid/webkit/WebBackForwardList;

    invoke-virtual {v0}, Landroid/webkit/WebBackForwardList;->getSize()I

    move-result v0

    return v0
.end method
