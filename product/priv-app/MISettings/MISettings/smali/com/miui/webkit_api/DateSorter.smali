.class public Lcom/miui/webkit_api/DateSorter;
.super Ljava/lang/Object;


# static fields
.field public static final DAY_COUNT:I = 0x5


# instance fields
.field private a:Lcom/miui/webkit_api/b/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/miui/webkit_api/b/g;->b(Landroid/content/Context;)Lcom/miui/webkit_api/b/a;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/webkit_api/DateSorter;->a:Lcom/miui/webkit_api/b/a;

    return-void
.end method


# virtual methods
.method public getBoundary(I)J
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/DateSorter;->a:Lcom/miui/webkit_api/b/a;

    invoke-interface {v0, p1}, Lcom/miui/webkit_api/b/a;->b(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getIndex(J)I
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/DateSorter;->a:Lcom/miui/webkit_api/b/a;

    invoke-interface {v0, p1, p2}, Lcom/miui/webkit_api/b/a;->a(J)I

    move-result p1

    return p1
.end method

.method public getLabel(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/DateSorter;->a:Lcom/miui/webkit_api/b/a;

    invoke-interface {v0, p1}, Lcom/miui/webkit_api/b/a;->a(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
