.class public Lcom/miui/webkit_api/WebViewFragment;
.super Landroid/app/Fragment;


# instance fields
.field private a:Lcom/miui/webkit_api/WebView;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getWebView()Lcom/miui/webkit_api/WebView;
    .locals 1

    iget-boolean v0, p0, Lcom/miui/webkit_api/WebViewFragment;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/webkit_api/WebViewFragment;->a:Lcom/miui/webkit_api/WebView;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    iget-object p1, p0, Lcom/miui/webkit_api/WebViewFragment;->a:Lcom/miui/webkit_api/WebView;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/webkit_api/WebView;->destroy()V

    :cond_0
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x17

    if-lt p1, p2, :cond_1

    new-instance p1, Lcom/miui/webkit_api/WebView;

    invoke-virtual {p0}, Landroid/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/miui/webkit_api/WebView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/miui/webkit_api/WebViewFragment;->a:Lcom/miui/webkit_api/WebView;

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/miui/webkit_api/WebView;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/miui/webkit_api/WebView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/miui/webkit_api/WebViewFragment;->a:Lcom/miui/webkit_api/WebView;

    :goto_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/miui/webkit_api/WebViewFragment;->b:Z

    iget-object p1, p0, Lcom/miui/webkit_api/WebViewFragment;->a:Lcom/miui/webkit_api/WebView;

    return-object p1
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebViewFragment;->a:Lcom/miui/webkit_api/WebView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/webkit_api/WebView;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/webkit_api/WebViewFragment;->a:Lcom/miui/webkit_api/WebView;

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/webkit_api/WebViewFragment;->b:Z

    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/miui/webkit_api/WebViewFragment;->a:Lcom/miui/webkit_api/WebView;

    invoke-virtual {v0}, Lcom/miui/webkit_api/WebView;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebViewFragment;->a:Lcom/miui/webkit_api/WebView;

    invoke-virtual {v0}, Lcom/miui/webkit_api/WebView;->onResume()V

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    return-void
.end method
