.class public Lcom/miui/webkit_api/a/v;
.super Lcom/miui/webkit_api/WebIconDatabase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/v$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webkit.WebIconDatabase"

.field private static d:Lcom/miui/webkit_api/WebIconDatabase;


# instance fields
.field private b:Lcom/miui/webkit_api/a/v$a;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebIconDatabase;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/v;->c:Ljava/lang/Object;

    return-void
.end method

.method public static a()Lcom/miui/webkit_api/WebIconDatabase;
    .locals 2

    sget-object v0, Lcom/miui/webkit_api/a/v;->d:Lcom/miui/webkit_api/WebIconDatabase;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/miui/webkit_api/a/v$a;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/miui/webkit_api/a/v;

    invoke-direct {v1, v0}, Lcom/miui/webkit_api/a/v;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/miui/webkit_api/a/v;->d:Lcom/miui/webkit_api/WebIconDatabase;

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/a/v;->d:Lcom/miui/webkit_api/WebIconDatabase;

    return-object v0
.end method

.method private b()Lcom/miui/webkit_api/a/v$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/v;->b:Lcom/miui/webkit_api/a/v$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/v$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/v;->c:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/v$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/v;->b:Lcom/miui/webkit_api/a/v$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/v;->b:Lcom/miui/webkit_api/a/v$a;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/v;->b()Lcom/miui/webkit_api/a/v$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/v;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/v$a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public open(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/v;->b()Lcom/miui/webkit_api/a/v$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/v;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/v$a;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public releaseIconForPageUrl(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/v;->b()Lcom/miui/webkit_api/a/v$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/v;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/v$a;->c(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public removeAllIcons()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/v;->b()Lcom/miui/webkit_api/a/v$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/v;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/v$a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public requestIconForPageUrl(Ljava/lang/String;Lcom/miui/webkit_api/WebIconDatabase$IconListener;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/v;->b()Lcom/miui/webkit_api/a/v$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/v;->c:Ljava/lang/Object;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lcom/miui/webkit_api/a/ak;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    :goto_0
    invoke-virtual {v0, v1, p1, p2}, Lcom/miui/webkit_api/a/v$a;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public retainIconForPageUrl(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/v;->b()Lcom/miui/webkit_api/a/v$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/v;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/v$a;->b(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
