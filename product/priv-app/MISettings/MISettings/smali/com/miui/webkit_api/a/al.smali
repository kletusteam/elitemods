.class public Lcom/miui/webkit_api/a/al;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/al$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "WebViewClassLoader"

.field private static final b:Ljava/lang/String; = "android.app.AppGlobals"

.field private static final c:Ljava/lang/String; = "android.app.Application"

.field private static final d:I = 0x133ed5b

.field private static final e:I = 0x133ec97

.field private static final f:Ljava/lang/String; = "com.android.browser"

.field private static final g:Ljava/lang/String; = "com.android.browser.debug"

.field private static final h:Ljava/lang/String; = "com.miui.webkit_api.support.KernelContextSetter"

.field private static final i:Ljava/lang/String; = "com.miui.org.chromium.base.library_loader.NativeLibraries"

.field private static final j:Ljava/lang/String; = "miwebview"

.field private static final k:Ljava/lang/String; = "com.miui.webkit_api.support.AssetPathSetter"

.field private static final l:Ljava/lang/String; = "VERSION"

.field private static m:[Ljava/lang/String;

.field private static n:Z

.field private static o:Landroid/content/Context;

.field private static p:Ljava/lang/ClassLoader;

.field private static q:Ljava/lang/String;

.field private static r:I

.field private static s:Lcom/miui/webkit_api/WebView$MiWebViewMode;

.field private static t:Ljava/lang/String;

.field private static u:Z

.field private static v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MiWebView.apk"

    aput-object v2, v0, v1

    const/4 v2, 0x1

    const-string v3, "libmiui_chromium.so"

    aput-object v3, v0, v2

    invoke-static {}, Lcom/miui/webkit_api/a/al;->g()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v0, v4

    sput-object v0, Lcom/miui/webkit_api/a/al;->m:[Ljava/lang/String;

    sput-boolean v1, Lcom/miui/webkit_api/a/al;->n:Z

    sput v2, Lcom/miui/webkit_api/a/al;->r:I

    sget-object v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;->None:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    sput-object v0, Lcom/miui/webkit_api/a/al;->s:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    sput-boolean v1, Lcom/miui/webkit_api/a/al;->u:Z

    sput-boolean v1, Lcom/miui/webkit_api/a/al;->v:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/miui/webkit_api/a/al;->b()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/a/al;->q:Ljava/lang/String;

    return-object v0
.end method

.method static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const/4 v0, 0x0

    const-string v1, "miwebview"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a(Landroid/app/Application;)Z
    .locals 8

    const-string v0, "WebViewClassLoader"

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/miui/webkit_api/a/al;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/miui/webkit_api/a/al;->q:Ljava/lang/String;

    invoke-static {}, Lcom/miui/webkit_api/a/al;->k()Z

    move-result v3

    if-nez v3, :cond_0

    return v2

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/miui/webkit_api/a/al;->q:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/miui/webkit_api/a/al;->m:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ldalvik/system/DexClassLoader;

    sget-object v5, Lcom/miui/webkit_api/a/al;->q:Ljava/lang/String;

    sget-object v6, Lcom/miui/webkit_api/a/al;->q:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Application;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/ClassLoader;->getParent()Ljava/lang/ClassLoader;

    move-result-object p0

    invoke-direct {v4, v3, v5, v6, p0}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    sput-object v4, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    sget-object p0, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    const-string v4, "com.miui.webkit_api.support.AssetPathSetter"

    invoke-virtual {p0, v4}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p0

    const-string v4, "setAssetPath"

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Class;

    const-class v7, Ljava/lang/String;

    aput-object v7, v6, v2

    invoke-virtual {p0, v4, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p0

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v3, v4, v2

    invoke-virtual {p0, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p0, Lcom/miui/webkit_api/WebView$MiWebViewMode;->Plugin:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    sput-object p0, Lcom/miui/webkit_api/a/al;->s:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    const-string p0, "Init ClassLoader by uninstalled apk success."

    invoke-static {v0, p0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v5

    :catch_0
    move-exception p0

    sput-object v1, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Init ClassLoader by uninstalled apk failed, catch exception: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v2
.end method

.method private static a(Landroid/app/Application;Z)Z
    .locals 9

    const-string v0, "WebViewClassLoader"

    invoke-static {}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->a()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 p0, 0x5

    sput p0, Lcom/miui/webkit_api/a/al;->r:I

    return v2

    :cond_0
    const/4 v1, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x0

    :try_start_0
    invoke-static {}, Lcom/miui/webkit_api/a/al;->j()Z

    move-result v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    if-eqz v5, :cond_1

    :try_start_1
    const-string v5, "com.android.browser.debug"

    invoke-virtual {p0, v5, v1}, Landroid/app/Application;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v5

    sput-object v5, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    const-string v5, "Create context by installed package: com.android.browser.debug"

    invoke-static {v0, v5}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move v6, v3

    goto :goto_1

    :catch_0
    move-exception v5

    move v6, v3

    goto :goto_0

    :catch_1
    move-exception v5

    move v6, v2

    :goto_0
    :try_start_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File use-debug-browser exists, create context by installed package: com.android.browser.debugfailed, catch exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move v6, v2

    :goto_1
    sget-object v5, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;

    if-nez v5, :cond_2

    invoke-static {p0}, Lcom/miui/webkit_api/a/al;->c(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_2

    return v2

    :cond_2
    sget-object v5, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    if-nez v5, :cond_3

    :try_start_4
    const-string v5, "com.android.browser"

    invoke-virtual {p0, v5, v1}, Landroid/app/Application;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v5

    sput-object v5, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;

    const-string v5, "Create context by installed package com.android.browser"

    invoke-static {v0, v5}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    :catch_2
    move-exception v5

    :try_start_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Create context by package com.android.browser failed, catch exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_2
    sget-object v5, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;

    if-eqz v5, :cond_5

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x13

    if-gt v5, v7, :cond_4

    new-instance v5, Lcom/miui/webkit_api/a/al$a;

    sget-object v7, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;

    invoke-direct {v5, p0, v7}, Lcom/miui/webkit_api/a/al$a;-><init>(Landroid/app/Application;Landroid/content/Context;)V

    invoke-static {v5}, Lcom/miui/a/a/a/a;->a(Landroid/content/Context;)V

    :cond_4
    sget-object p0, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p0

    sput-object p0, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    sget-object p0, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    const-string v5, "com.miui.webkit_api.support.KernelContextSetter"

    invoke-virtual {p0, v5}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p0

    const-string v5, "setKernelContext"

    new-array v7, v3, [Ljava/lang/Class;

    const-class v8, Landroid/content/Context;

    aput-object v8, v7, v2

    invoke-virtual {p0, v5, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p0

    new-array v5, v3, [Ljava/lang/Object;

    sget-object v7, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;

    aput-object v7, v5, v2

    invoke-virtual {p0, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_5

    if-nez v6, :cond_5

    invoke-static {}, Lcom/miui/webkit_api/a/al;->l()Z

    move-result p0

    if-nez p0, :cond_5

    sput-boolean v3, Lcom/miui/webkit_api/a/al;->u:Z

    sput-object v4, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;

    sput-object v4, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    const-string p0, "Installed package\'s kernel version is too old "

    invoke-static {v0, p0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_3

    :catch_3
    move-exception p0

    sput-object v4, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;

    sput-object v4, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Create context by installed package failed, catch exception: "

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_3
    sget-object p0, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    const/4 p1, 0x4

    if-nez p0, :cond_7

    invoke-static {}, Lcom/miui/webkit_api/util/b;->a()Z

    move-result p0

    if-eqz p0, :cond_6

    goto :goto_4

    :cond_6
    move p1, v1

    :goto_4
    sput p1, Lcom/miui/webkit_api/a/al;->r:I

    return v2

    :cond_7
    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreIntVersion()I

    move-result p0

    sget v1, Lcom/miui/webkit_api/VersionInfo;->AVAILABLE_CORE_VENSION:I

    if-ge p0, v1, :cond_8

    sput-object v4, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    sput-object v4, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;

    sput p1, Lcom/miui/webkit_api/a/al;->r:I

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "Expected minimun core version 1.2, actual version is "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreVersion()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    :cond_8
    sget-object p0, Lcom/miui/webkit_api/WebView$MiWebViewMode;->Shared:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    sput-object p0, Lcom/miui/webkit_api/a/al;->s:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    return v3
.end method

.method private static b(Landroid/content/Context;)I
    .locals 4

    const-string v0, "WebViewClassLoader"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "com.android.browser"

    invoke-virtual {p0, v2, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0

    iget v1, p0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getInstalledApkVersionCode catch exception e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Browser version code is "

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method public static b()Ljava/lang/ClassLoader;
    .locals 1

    sget-boolean v0, Lcom/miui/webkit_api/a/al;->n:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/miui/webkit_api/a/al;->h()V

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    return-object v0
.end method

.method static b(Ljava/lang/String;)V
    .locals 2

    sput-object p0, Lcom/miui/webkit_api/a/al;->t:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Required minimum kernel version "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "WebViewClassLoader"

    invoke-static {v0, p0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static c()Z
    .locals 5

    invoke-static {}, Lcom/miui/webkit_api/a/al;->b()Ljava/lang/ClassLoader;

    sget-object v0, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v2, 0x1

    :try_start_0
    const-string v3, "com.miui.webkit.WebViewFactory"

    invoke-virtual {v0, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v3, "getProvider"

    new-array v4, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    const/4 v3, 0x0

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    sput v1, Lcom/miui/webkit_api/a/al;->r:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    :catch_0
    move-exception v0

    sput v2, Lcom/miui/webkit_api/a/al;->r:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkWebViewAvailable failed, catch exception e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "WebViewClassLoader"

    invoke-static {v2, v0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method private static c(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p0}, Lcom/miui/webkit_api/a/al;->b(Landroid/content/Context;)I

    move-result p0

    const/4 v0, 0x0

    if-nez p0, :cond_0

    const/4 p0, 0x3

    sput p0, Lcom/miui/webkit_api/a/al;->r:I

    return v0

    :cond_0
    const v1, 0x133ec97

    if-lt p0, v1, :cond_2

    const v1, 0x133ed5b

    if-le p0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p0, 0x4

    sput p0, Lcom/miui/webkit_api/a/al;->r:I

    return v0

    :cond_2
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    const-string v1, "\\."

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_2

    array-length v1, p0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    :try_start_0
    aget-object v1, p0, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    aget-object p0, p0, v2

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 p0, p0, 0x10

    shl-int p0, v1, p0

    sget v1, Lcom/miui/webkit_api/VersionInfo;->AVAILABLE_CORE_VENSION:I

    if-lt p0, v1, :cond_2

    move v0, v2

    :catch_0
    :cond_2
    :goto_0
    return v0
.end method

.method static d()I
    .locals 1

    sget v0, Lcom/miui/webkit_api/a/al;->r:I

    return v0
.end method

.method private static d(Ljava/lang/String;)[I
    .locals 5

    const-string v0, "\\."

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    return-object v1

    :cond_0
    :try_start_0
    new-array v0, v2, [I

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, p0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v0, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v0

    :catch_0
    return-object v1
.end method

.method static e()Z
    .locals 1

    sget-boolean v0, Lcom/miui/webkit_api/a/al;->v:Z

    return v0
.end method

.method static f()Lcom/miui/webkit_api/WebView$MiWebViewMode;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/a/al;->s:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    return-object v0
.end method

.method private static g()Ljava/lang/String;
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    const-string v0, "libmiui_chromium_support-26.so"

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "libmiui_chromium_support-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".so"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static h()V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    sput-boolean v1, Lcom/miui/webkit_api/a/al;->n:Z

    invoke-static {}, Lcom/miui/webkit_api/a/al;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    const-string v3, "android.app.AppGlobals"

    invoke-virtual {v2, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getInitialApplication"

    const/4 v4, 0x0

    new-array v5, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Application;

    invoke-static {v2, v1}, Lcom/miui/webkit_api/a/al;->a(Landroid/app/Application;Z)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v2}, Lcom/miui/webkit_api/a/al;->a(Landroid/app/Application;)Z

    sget-object v3, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    if-nez v3, :cond_1

    sget-boolean v3, Lcom/miui/webkit_api/a/al;->u:Z

    if-eqz v3, :cond_1

    invoke-static {v2, v4}, Lcom/miui/webkit_api/a/al;->a(Landroid/app/Application;Z)Z

    sput-boolean v1, Lcom/miui/webkit_api/a/al;->v:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    sput-object v0, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    sput-object v0, Lcom/miui/webkit_api/a/al;->o:Landroid/content/Context;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Create ClassLoader failed, catch exception e: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WebViewClassLoader"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private static i()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    const-class v1, Lcom/miui/webkit_api/a/al;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const-string v2, "com.miui.webkit_api.support.KernelContextSetter"

    invoke-virtual {v1, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->b()Z

    move-result v1

    if-nez v1, :cond_0

    return v0

    :cond_0
    const-class v0, Lcom/miui/webkit_api/a/al;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    sput-object v0, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    sget-object v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;->BuiltIn:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    sput-object v0, Lcom/miui/webkit_api/a/al;->s:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    const-string v0, "WebViewClassLoader"

    const-string v1, "Use built-in webview library"

    invoke-static {v0, v1}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    :catch_0
    return v0
.end method

.method private static j()Z
    .locals 3

    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "/local/tmp/use-debug-browser"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isUseDebugPackage, return false, catch Exception e: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WebViewClassLoader"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method private static k()Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/miui/webkit_api/a/al;->m:[Ljava/lang/String;

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const-string v4, "/"

    const-string v5, "WebViewClassLoader"

    if-ge v3, v1, :cond_1

    aget-object v6, v0, v3

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/miui/webkit_api/a/al;->q:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkUninstalledApkFilesAvailable failed, because file: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " is not exist."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/miui/webkit_api/a/al;->q:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "VERSION"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    :try_start_0
    new-instance v3, Ljava/io/InputStreamReader;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkUninstalledApkFilesAvailable, actual version: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/miui/webkit_api/a/al;->c(Ljava/lang/String;)Z

    move-result v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    const/4 v0, 0x1

    return v0

    :cond_2
    :try_start_3
    const-string v1, "checkUninstalledApkFilesAvailable failed, invalid version"

    invoke-static {v5, v1}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    return v2

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1
    :try_start_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkUninstalledApkFilesAvailable failed, catch Exception: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    return v2

    :catchall_2
    move-exception v1

    :goto_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    throw v1

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkUninstalledApkFilesAvailable, "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " is not exists."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v2
.end method

.method private static l()Z
    .locals 7

    const-string v0, "WebViewClassLoader"

    sget-object v1, Lcom/miui/webkit_api/a/al;->t:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    return v3

    :cond_0
    invoke-static {v1}, Lcom/miui/webkit_api/a/al;->d(Ljava/lang/String;)[I

    move-result-object v1

    if-nez v1, :cond_1

    return v3

    :cond_1
    const/4 v2, 0x0

    :try_start_0
    sget-object v4, Lcom/miui/webkit_api/a/al;->p:Ljava/lang/ClassLoader;

    const-string v5, "com.miui.org.chromium.base.library_loader.NativeLibraries"

    invoke-virtual {v4, v5}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v5, "sVersionNumber"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Installed apk\'s kernel version "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v5, "_alpha"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    return v3

    :cond_2
    invoke-static {v4}, Lcom/miui/webkit_api/a/al;->d(Ljava/lang/String;)[I

    move-result-object v4

    if-nez v4, :cond_3

    const-string v1, "Failed to parse installed kernel version"

    invoke-static {v0, v1}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    :cond_3
    const/4 v0, 0x3

    aget v5, v4, v0

    if-nez v5, :cond_4

    return v3

    :cond_4
    aget v5, v4, v2

    aget v6, v1, v2

    if-le v5, v6, :cond_5

    return v3

    :cond_5
    aget v5, v4, v2

    aget v6, v1, v2

    if-ne v5, v6, :cond_6

    aget v4, v4, v0

    aget v0, v1, v0

    if-lt v4, v0, :cond_6

    return v3

    :cond_6
    return v2

    :catch_0
    const-string v1, "Failed to get installed apk\'s kernel version"

    invoke-static {v0, v1}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v2
.end method
