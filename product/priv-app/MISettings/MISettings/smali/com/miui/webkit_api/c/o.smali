.class Lcom/miui/webkit_api/c/o;
.super Lcom/miui/webkit_api/ServiceWorkerWebSettings;


# instance fields
.field private a:Landroid/webkit/ServiceWorkerWebSettings;


# direct methods
.method constructor <init>(Landroid/webkit/ServiceWorkerWebSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/ServiceWorkerWebSettings;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/o;->a:Landroid/webkit/ServiceWorkerWebSettings;

    return-void
.end method


# virtual methods
.method public getAllowContentAccess()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/o;->a:Landroid/webkit/ServiceWorkerWebSettings;

    invoke-virtual {v0}, Landroid/webkit/ServiceWorkerWebSettings;->getAllowContentAccess()Z

    move-result v0

    return v0
.end method

.method public getAllowFileAccess()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/o;->a:Landroid/webkit/ServiceWorkerWebSettings;

    invoke-virtual {v0}, Landroid/webkit/ServiceWorkerWebSettings;->getAllowFileAccess()Z

    move-result v0

    return v0
.end method

.method public getBlockNetworkLoads()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/o;->a:Landroid/webkit/ServiceWorkerWebSettings;

    invoke-virtual {v0}, Landroid/webkit/ServiceWorkerWebSettings;->getBlockNetworkLoads()Z

    move-result v0

    return v0
.end method

.method public getCacheMode()I
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/o;->a:Landroid/webkit/ServiceWorkerWebSettings;

    invoke-virtual {v0}, Landroid/webkit/ServiceWorkerWebSettings;->getCacheMode()I

    move-result v0

    return v0
.end method

.method public setAllowContentAccess(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/o;->a:Landroid/webkit/ServiceWorkerWebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/ServiceWorkerWebSettings;->setAllowContentAccess(Z)V

    return-void
.end method

.method public setAllowFileAccess(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/o;->a:Landroid/webkit/ServiceWorkerWebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/ServiceWorkerWebSettings;->setAllowFileAccess(Z)V

    return-void
.end method

.method public setBlockNetworkLoads(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/o;->a:Landroid/webkit/ServiceWorkerWebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/ServiceWorkerWebSettings;->setBlockNetworkLoads(Z)V

    return-void
.end method

.method public setCacheMode(I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/o;->a:Landroid/webkit/ServiceWorkerWebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/ServiceWorkerWebSettings;->setCacheMode(I)V

    return-void
.end method
