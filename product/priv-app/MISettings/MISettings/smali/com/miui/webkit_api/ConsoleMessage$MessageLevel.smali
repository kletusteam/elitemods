.class public final enum Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/webkit_api/ConsoleMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MessageLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DEBUG:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

.field public static final enum ERROR:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

.field public static final enum LOG:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

.field public static final enum TIP:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

.field public static final enum WARNING:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

.field private static final synthetic a:[Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    const/4 v1, 0x0

    const-string v2, "TIP"

    invoke-direct {v0, v2, v1}, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->TIP:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    new-instance v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    const/4 v2, 0x1

    const-string v3, "LOG"

    invoke-direct {v0, v3, v2}, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->LOG:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    new-instance v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    const/4 v3, 0x2

    const-string v4, "WARNING"

    invoke-direct {v0, v4, v3}, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->WARNING:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    new-instance v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    const/4 v4, 0x3

    const-string v5, "ERROR"

    invoke-direct {v0, v5, v4}, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->ERROR:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    new-instance v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    const/4 v5, 0x4

    const-string v6, "DEBUG"

    invoke-direct {v0, v6, v5}, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->DEBUG:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    sget-object v6, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->TIP:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    aput-object v6, v0, v1

    sget-object v1, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->LOG:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->WARNING:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->ERROR:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->DEBUG:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    aput-object v1, v0, v5

    sput-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->a:[Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;
    .locals 1

    const-class v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    return-object p0
.end method

.method public static values()[Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->a:[Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    invoke-virtual {v0}, [Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    return-object v0
.end method
