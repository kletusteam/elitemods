.class Lcom/miui/webkit_api/a/o;
.super Lcom/miui/webkit_api/SslErrorHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/o$a;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/a/o$a;

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/SslErrorHandler;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/o;->b:Ljava/lang/Object;

    return-void
.end method

.method private b()Lcom/miui/webkit_api/a/o$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/o;->a:Lcom/miui/webkit_api/a/o$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/o$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/o;->b:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/o$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/o;->a:Lcom/miui/webkit_api/a/o$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/o;->a:Lcom/miui/webkit_api/a/o$a;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/o;->b:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public cancel()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/o;->b()Lcom/miui/webkit_api/a/o$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/o;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/o$a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public proceed()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/o;->b()Lcom/miui/webkit_api/a/o$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/o;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/o$a;->a(Ljava/lang/Object;)V

    return-void
.end method
