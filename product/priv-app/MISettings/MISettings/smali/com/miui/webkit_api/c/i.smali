.class Lcom/miui/webkit_api/c/i;
.super Lcom/miui/webkit_api/JsPromptResult;


# instance fields
.field private a:Landroid/webkit/JsPromptResult;


# direct methods
.method constructor <init>(Landroid/webkit/JsPromptResult;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/JsPromptResult;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/i;->a:Landroid/webkit/JsPromptResult;

    return-void
.end method


# virtual methods
.method a()Landroid/webkit/JsPromptResult;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/webkit_api/c/i;->a:Landroid/webkit/JsPromptResult;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public cancel()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/i;->a:Landroid/webkit/JsPromptResult;

    invoke-virtual {v0}, Landroid/webkit/JsPromptResult;->cancel()V

    return-void
.end method

.method public confirm()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/i;->a:Landroid/webkit/JsPromptResult;

    invoke-virtual {v0}, Landroid/webkit/JsPromptResult;->confirm()V

    return-void
.end method

.method public confirm(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/i;->a:Landroid/webkit/JsPromptResult;

    invoke-virtual {v0, p1}, Landroid/webkit/JsPromptResult;->confirm(Ljava/lang/String;)V

    return-void
.end method
