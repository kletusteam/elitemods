.class Lcom/miui/webkit_api/a/a;
.super Lcom/miui/webkit_api/ClientCertRequest;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/a$a;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/a/a$a;

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/ClientCertRequest;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/a;->b:Ljava/lang/Object;

    return-void
.end method

.method private b()Lcom/miui/webkit_api/a/a$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/a;->a:Lcom/miui/webkit_api/a/a$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/a$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/a;->b:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/a$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/a;->a:Lcom/miui/webkit_api/a/a$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/a;->a:Lcom/miui/webkit_api/a/a$a;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/a;->b:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public cancel()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/a;->b()Lcom/miui/webkit_api/a/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/a;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/a$a;->f(Ljava/lang/Object;)V

    return-void
.end method

.method public getHost()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/a;->b()Lcom/miui/webkit_api/a/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/a;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/a$a;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKeyTypes()[Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/a;->b()Lcom/miui/webkit_api/a/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/a;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/a$a;->a(Ljava/lang/Object;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPort()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/a;->b()Lcom/miui/webkit_api/a/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/a;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/a$a;->d(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getPrincipals()[Ljava/security/Principal;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/a;->b()Lcom/miui/webkit_api/a/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/a;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/a$a;->b(Ljava/lang/Object;)[Ljava/security/Principal;

    move-result-object v0

    return-object v0
.end method

.method public ignore()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/a;->b()Lcom/miui/webkit_api/a/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/a;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/a$a;->e(Ljava/lang/Object;)V

    return-void
.end method

.method public proceed(Ljava/security/PrivateKey;[Ljava/security/cert/X509Certificate;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/a;->b()Lcom/miui/webkit_api/a/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/a;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, Lcom/miui/webkit_api/a/a$a;->a(Ljava/lang/Object;Ljava/security/PrivateKey;[Ljava/security/cert/X509Certificate;)V

    return-void
.end method
