.class public Lcom/miui/webkit_api/WebView$WebViewTransport;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/webkit_api/WebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WebViewTransport"
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/webkit_api/WebView;

.field private b:Lcom/miui/webkit_api/WebView;

.field private c:Lcom/miui/webkit_api/b/h;


# direct methods
.method public constructor <init>(Lcom/miui/webkit_api/WebView;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/webkit_api/WebView$WebViewTransport;->a:Lcom/miui/webkit_api/WebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/miui/webkit_api/b/g;->a(Ljava/lang/Object;)Lcom/miui/webkit_api/b/h;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/webkit_api/WebView$WebViewTransport;->c:Lcom/miui/webkit_api/b/h;

    return-void
.end method


# virtual methods
.method public declared-synchronized getWebView()Lcom/miui/webkit_api/WebView;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/miui/webkit_api/WebView$WebViewTransport;->b:Lcom/miui/webkit_api/WebView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setWebView(Lcom/miui/webkit_api/WebView;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/miui/webkit_api/WebView$WebViewTransport;->b:Lcom/miui/webkit_api/WebView;

    iget-object v0, p0, Lcom/miui/webkit_api/WebView$WebViewTransport;->c:Lcom/miui/webkit_api/b/h;

    invoke-interface {v0, p1}, Lcom/miui/webkit_api/b/h;->a(Lcom/miui/webkit_api/WebView;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
