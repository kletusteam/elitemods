.class Lcom/miui/webkit_api/c/w;
.super Lcom/miui/webkit_api/WebResourceError;


# instance fields
.field private a:Landroid/webkit/WebResourceError;


# direct methods
.method constructor <init>(Landroid/webkit/WebResourceError;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebResourceError;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/w;->a:Landroid/webkit/WebResourceError;

    return-void
.end method


# virtual methods
.method a()Landroid/webkit/WebResourceError;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/webkit_api/c/w;->a:Landroid/webkit/WebResourceError;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public getDescription()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/w;->a:Landroid/webkit/WebResourceError;

    invoke-virtual {v0}, Landroid/webkit/WebResourceError;->getDescription()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getErrorCode()I
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/w;->a:Landroid/webkit/WebResourceError;

    invoke-virtual {v0}, Landroid/webkit/WebResourceError;->getErrorCode()I

    move-result v0

    return v0
.end method
