.class Lcom/miui/webkit_api/a/g;
.super Lcom/miui/webkit_api/HttpAuthHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/g$a;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/a/g$a;

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/HttpAuthHandler;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/g;->b:Ljava/lang/Object;

    return-void
.end method

.method private b()Lcom/miui/webkit_api/a/g$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/g;->a:Lcom/miui/webkit_api/a/g$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/g$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/g;->b:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/g$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/g;->a:Lcom/miui/webkit_api/a/g$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/g;->a:Lcom/miui/webkit_api/a/g$a;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/miui/webkit_api/a/g;->b:Ljava/lang/Object;

    goto/32 :goto_0

    nop
.end method

.method public cancel()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/g;->b()Lcom/miui/webkit_api/a/g$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/g;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/g$a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public proceed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/g;->b()Lcom/miui/webkit_api/a/g$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/g;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, Lcom/miui/webkit_api/a/g$a;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public useHttpAuthUsernamePassword()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/g;->b()Lcom/miui/webkit_api/a/g$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/g;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/g$a;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
