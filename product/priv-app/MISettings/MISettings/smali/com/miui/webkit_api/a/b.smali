.class Lcom/miui/webkit_api/a/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/b$b;,
        Lcom/miui/webkit_api/a/b$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webkit.ConsoleMessage"

.field static b:Lcom/miui/webkit_api/a/b$a;


# instance fields
.field private c:Lcom/miui/webkit_api/a/b$b;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/b;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/miui/webkit_api/ConsoleMessage$MessageLevel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/miui/webkit_api/a/b;->h()Lcom/miui/webkit_api/a/b$b;

    move-result-object v0

    invoke-static {p4}, Lcom/miui/webkit_api/a/b;->a(Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;)Ljava/lang/Object;

    move-result-object p4

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/webkit_api/a/b$b;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/webkit_api/a/b;->d:Ljava/lang/Object;

    return-void
.end method

.method static a(Ljava/lang/Object;)Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/a/b;->a()Lcom/miui/webkit_api/a/b$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/miui/webkit_api/a/b$a;->a(Ljava/lang/Object;)Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    move-result-object p0

    return-object p0
.end method

.method static a()Lcom/miui/webkit_api/a/b$a;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/a/b;->b:Lcom/miui/webkit_api/a/b$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/b$a;

    invoke-direct {v0}, Lcom/miui/webkit_api/a/b$a;-><init>()V

    sput-object v0, Lcom/miui/webkit_api/a/b;->b:Lcom/miui/webkit_api/a/b$a;

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/a/b;->b:Lcom/miui/webkit_api/a/b$a;

    return-object v0
.end method

.method static a(Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;)Ljava/lang/Object;
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/a/b;->a()Lcom/miui/webkit_api/a/b$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/miui/webkit_api/a/b$a;->a(Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    invoke-static {}, Lcom/miui/webkit_api/a/b;->a()Lcom/miui/webkit_api/a/b$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/webkit_api/a/b$a;->a()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/miui/webkit_api/a/b$b;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/b;->c:Lcom/miui/webkit_api/a/b$b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/b$b;

    iget-object v1, p0, Lcom/miui/webkit_api/a/b;->d:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/b$b;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/b;->c:Lcom/miui/webkit_api/a/b$b;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/b;->c:Lcom/miui/webkit_api/a/b$b;

    return-object v0
.end method


# virtual methods
.method c()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/b;->d:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public d()Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/b;->h()Lcom/miui/webkit_api/a/b$b;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/b;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/b$b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/webkit_api/a/b;->a(Ljava/lang/Object;)Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/b;->h()Lcom/miui/webkit_api/a/b$b;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/b;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/b$b;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/b;->h()Lcom/miui/webkit_api/a/b$b;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/b;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/b$b;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/b;->h()Lcom/miui/webkit_api/a/b$b;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/b;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/b$b;->d(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
