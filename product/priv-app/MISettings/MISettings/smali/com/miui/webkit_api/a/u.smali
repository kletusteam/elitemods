.class public Lcom/miui/webkit_api/a/u;
.super Lcom/miui/webkit_api/WebHistoryItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/u$a;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/a/u$a;

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebHistoryItem;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/u;->b:Ljava/lang/Object;

    return-void
.end method

.method private a()Lcom/miui/webkit_api/a/u$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/u;->a:Lcom/miui/webkit_api/a/u$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/u$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/u;->b:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/u$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/u;->a:Lcom/miui/webkit_api/a/u$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/u;->a:Lcom/miui/webkit_api/a/u$a;

    return-object v0
.end method


# virtual methods
.method public getFavicon()Landroid/graphics/Bitmap;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/u;->a()Lcom/miui/webkit_api/a/u$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/u;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/u$a;->d(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getOriginalUrl()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/u;->a()Lcom/miui/webkit_api/a/u$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/u;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/u$a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/u;->a()Lcom/miui/webkit_api/a/u$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/u;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/u$a;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/u;->a()Lcom/miui/webkit_api/a/u$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/u;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/u$a;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
