.class Lcom/miui/webkit_api/a/q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/webkit_api/ValueCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/q$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/miui/webkit_api/ValueCallback<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/a/q$a;

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/q;->b:Ljava/lang/Object;

    return-void
.end method

.method private b()Lcom/miui/webkit_api/a/q$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/q;->a:Lcom/miui/webkit_api/a/q$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/q$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/q;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/q$a;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/q;->a:Lcom/miui/webkit_api/a/q$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/q;->a:Lcom/miui/webkit_api/a/q$a;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/q;->b:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public onReceiveValue(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/miui/webkit_api/a/q;->b()Lcom/miui/webkit_api/a/q$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/q;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/q$a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
