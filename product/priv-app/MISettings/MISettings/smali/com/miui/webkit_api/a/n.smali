.class Lcom/miui/webkit_api/a/n;
.super Lcom/miui/webkit_api/ServiceWorkerWebSettings;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/n$a;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/a/n$a;

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/ServiceWorkerWebSettings;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/n;->b:Ljava/lang/Object;

    return-void
.end method

.method private a()Lcom/miui/webkit_api/a/n$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/n;->a:Lcom/miui/webkit_api/a/n$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/n$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/n;->b:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/n$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/n;->a:Lcom/miui/webkit_api/a/n$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/n;->a:Lcom/miui/webkit_api/a/n$a;

    return-object v0
.end method


# virtual methods
.method public getAllowContentAccess()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/n;->a()Lcom/miui/webkit_api/a/n$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/n;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/n$a;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getAllowFileAccess()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/n;->a()Lcom/miui/webkit_api/a/n$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/n;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/n$a;->c(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getBlockNetworkLoads()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/n;->a()Lcom/miui/webkit_api/a/n$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/n;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/n$a;->d(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getCacheMode()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/n;->a()Lcom/miui/webkit_api/a/n$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/n;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/n$a;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public setAllowContentAccess(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/n;->a()Lcom/miui/webkit_api/a/n$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/n;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/n$a;->a(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setAllowFileAccess(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/n;->a()Lcom/miui/webkit_api/a/n$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/n;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/n$a;->b(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setBlockNetworkLoads(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/n;->a()Lcom/miui/webkit_api/a/n$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/n;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/n$a;->c(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setCacheMode(I)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/n;->a()Lcom/miui/webkit_api/a/n$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/n;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/n$a;->a(Ljava/lang/Object;I)V

    return-void
.end method
