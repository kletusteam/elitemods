.class Lcom/miui/webkit_api/a/x;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/webkit_api/WebResourceRequest;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/x$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "BrowserWebResourceRequest"


# instance fields
.field private b:Lcom/miui/webkit_api/a/x$a;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/x;->c:Ljava/lang/Object;

    return-void
.end method

.method private b()Lcom/miui/webkit_api/a/x$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/x;->b:Lcom/miui/webkit_api/a/x$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/x$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/x;->c:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/x$a;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/x;->b:Lcom/miui/webkit_api/a/x$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/x;->b:Lcom/miui/webkit_api/a/x$a;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/miui/webkit_api/a/x;->c:Ljava/lang/Object;

    goto/32 :goto_0

    nop
.end method

.method public getMethod()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/x;->b()Lcom/miui/webkit_api/a/x$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/x;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/x$a;->e(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequestHeaders()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/miui/webkit_api/a/x;->b()Lcom/miui/webkit_api/a/x$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/x;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/x$a;->f(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getUrl()Landroid/net/Uri;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/x;->b()Lcom/miui/webkit_api/a/x$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/x;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/x$a;->a(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public hasGesture()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/x;->b()Lcom/miui/webkit_api/a/x$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/x;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/x$a;->d(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isForMainFrame()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/x;->b()Lcom/miui/webkit_api/a/x$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/x;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/x$a;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isRedirect()Z
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/x;->b()Lcom/miui/webkit_api/a/x$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/x;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/x$a;->c(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "current browser apk is not support isRedirect(), current version is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreIntVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", expected version is 0x00010006, so will return false."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrowserWebResourceRequest"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method
