.class public Lcom/miui/webkit_api/a/s;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/s$b;,
        Lcom/miui/webkit_api/a/s$a;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/WebView;

.field private b:Lcom/miui/webkit_api/WebChromeClient;


# direct methods
.method constructor <init>(Lcom/miui/webkit_api/WebView;Lcom/miui/webkit_api/WebChromeClient;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    iput-object p2, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    return-void
.end method


# virtual methods
.method public getDefaultVideoPoster()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    invoke-virtual {v0}, Lcom/miui/webkit_api/WebChromeClient;->getDefaultVideoPoster()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getVideoLoadingProgressView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    invoke-virtual {v0}, Lcom/miui/webkit_api/WebChromeClient;->getVideoLoadingProgressView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getVisitedHistory(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/q;

    invoke-direct {v1, p1}, Lcom/miui/webkit_api/a/q;-><init>(Ljava/lang/Object;)V

    move-object p1, v1

    :goto_0
    invoke-virtual {v0, p1}, Lcom/miui/webkit_api/WebChromeClient;->getVisitedHistory(Lcom/miui/webkit_api/ValueCallback;)V

    return-void
.end method

.method public onCloseWindow(Ljava/lang/Object;)V
    .locals 1

    iget-object p1, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    invoke-virtual {p1, v0}, Lcom/miui/webkit_api/WebChromeClient;->onCloseWindow(Lcom/miui/webkit_api/WebView;)V

    return-void
.end method

.method public onConsoleMessage(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/webkit_api/WebChromeClient;->onConsoleMessage(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public onConsoleMessage(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_0

    move-object v1, v0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/b;

    invoke-direct {v1, p1}, Lcom/miui/webkit_api/a/b;-><init>(Ljava/lang/Object;)V

    :goto_0
    iget-object v2, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/miui/webkit_api/ConsoleMessage;

    invoke-virtual {v1}, Lcom/miui/webkit_api/a/b;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1}, Lcom/miui/webkit_api/a/b;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/miui/webkit_api/a/b;->g()I

    move-result v4

    invoke-virtual {v1}, Lcom/miui/webkit_api/a/b;->d()Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    move-result-object v1

    invoke-direct {v0, p1, v3, v4, v1}, Lcom/miui/webkit_api/ConsoleMessage;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/miui/webkit_api/ConsoleMessage$MessageLevel;)V

    :goto_1
    invoke-virtual {v2, v0}, Lcom/miui/webkit_api/WebChromeClient;->onConsoleMessage(Lcom/miui/webkit_api/ConsoleMessage;)Z

    move-result p1

    return p1
.end method

.method public onCreateWindow(Ljava/lang/Object;ZZLandroid/os/Message;)Z
    .locals 1

    iget-object p1, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    invoke-virtual {p1, v0, p2, p3, p4}, Lcom/miui/webkit_api/WebChromeClient;->onCreateWindow(Lcom/miui/webkit_api/WebView;ZZLandroid/os/Message;)Z

    move-result p1

    return p1
.end method

.method public onExceededDatabaseQuota(Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/Object;)V
    .locals 11

    move-object v0, p0

    move-object/from16 v1, p9

    iget-object v2, v0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    move-object v10, v1

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/miui/webkit_api/a/aa$a;

    invoke-direct {v3, v1}, Lcom/miui/webkit_api/a/aa$a;-><init>(Ljava/lang/Object;)V

    move-object v10, v3

    :goto_0
    move-object v1, v2

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-virtual/range {v1 .. v10}, Lcom/miui/webkit_api/WebChromeClient;->onExceededDatabaseQuota(Ljava/lang/String;Ljava/lang/String;JJJLcom/miui/webkit_api/WebStorage$QuotaUpdater;)V

    return-void
.end method

.method public onGeolocationPermissionsHidePrompt()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    invoke-virtual {v0}, Lcom/miui/webkit_api/WebChromeClient;->onGeolocationPermissionsHidePrompt()V

    return-void
.end method

.method public onGeolocationPermissionsShowPrompt(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/f$a;

    invoke-direct {v1, p2}, Lcom/miui/webkit_api/a/f$a;-><init>(Ljava/lang/Object;)V

    move-object p2, v1

    :goto_0
    invoke-virtual {v0, p1, p2}, Lcom/miui/webkit_api/WebChromeClient;->onGeolocationPermissionsShowPrompt(Ljava/lang/String;Lcom/miui/webkit_api/GeolocationPermissions$Callback;)V

    return-void
.end method

.method public onHideCustomView()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    invoke-virtual {v0}, Lcom/miui/webkit_api/WebChromeClient;->onHideCustomView()V

    return-void
.end method

.method public onJsAlert(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 2

    iget-object p1, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    if-nez p4, :cond_0

    const/4 p4, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/i;

    invoke-direct {v1, p4}, Lcom/miui/webkit_api/a/i;-><init>(Ljava/lang/Object;)V

    move-object p4, v1

    :goto_0
    invoke-virtual {p1, v0, p2, p3, p4}, Lcom/miui/webkit_api/WebChromeClient;->onJsAlert(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsResult;)Z

    move-result p1

    return p1
.end method

.method public onJsBeforeUnload(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 2

    iget-object p1, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    if-nez p4, :cond_0

    const/4 p4, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/i;

    invoke-direct {v1, p4}, Lcom/miui/webkit_api/a/i;-><init>(Ljava/lang/Object;)V

    move-object p4, v1

    :goto_0
    invoke-virtual {p1, v0, p2, p3, p4}, Lcom/miui/webkit_api/WebChromeClient;->onJsBeforeUnload(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsResult;)Z

    move-result p1

    return p1
.end method

.method public onJsConfirm(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 2

    iget-object p1, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    if-nez p4, :cond_0

    const/4 p4, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/i;

    invoke-direct {v1, p4}, Lcom/miui/webkit_api/a/i;-><init>(Ljava/lang/Object;)V

    move-object p4, v1

    :goto_0
    invoke-virtual {p1, v0, p2, p3, p4}, Lcom/miui/webkit_api/WebChromeClient;->onJsConfirm(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsResult;)Z

    move-result p1

    return p1
.end method

.method public onJsPrompt(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 6

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v1, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    if-nez p5, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/miui/webkit_api/a/h;

    invoke-direct {p1, p5}, Lcom/miui/webkit_api/a/h;-><init>(Ljava/lang/Object;)V

    :goto_0
    move-object v5, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/miui/webkit_api/WebChromeClient;->onJsPrompt(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsPromptResult;)Z

    move-result p1

    return p1
.end method

.method public onJsTimeout()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    invoke-virtual {v0}, Lcom/miui/webkit_api/WebChromeClient;->onJsTimeout()Z

    move-result v0

    return v0
.end method

.method public onPermissionRequest(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/k;

    invoke-direct {v1, p1}, Lcom/miui/webkit_api/a/k;-><init>(Ljava/lang/Object;)V

    move-object p1, v1

    :goto_0
    invoke-virtual {v0, p1}, Lcom/miui/webkit_api/WebChromeClient;->onPermissionRequest(Lcom/miui/webkit_api/PermissionRequest;)V

    return-void
.end method

.method public onPermissionRequestCanceled(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/k;

    invoke-direct {v1, p1}, Lcom/miui/webkit_api/a/k;-><init>(Ljava/lang/Object;)V

    move-object p1, v1

    :goto_0
    invoke-virtual {v0, p1}, Lcom/miui/webkit_api/WebChromeClient;->onPermissionRequestCanceled(Lcom/miui/webkit_api/PermissionRequest;)V

    return-void
.end method

.method public onProgressChanged(Ljava/lang/Object;I)V
    .locals 1

    iget-object p1, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    invoke-virtual {p1, v0, p2}, Lcom/miui/webkit_api/WebChromeClient;->onProgressChanged(Lcom/miui/webkit_api/WebView;I)V

    return-void
.end method

.method public onReachedMaxAppCacheSize(JJLjava/lang/Object;)V
    .locals 6

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    if-nez p5, :cond_0

    const/4 p5, 0x0

    move-object v5, p5

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/aa$a;

    invoke-direct {v1, p5}, Lcom/miui/webkit_api/a/aa$a;-><init>(Ljava/lang/Object;)V

    move-object v5, v1

    :goto_0
    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/miui/webkit_api/WebChromeClient;->onReachedMaxAppCacheSize(JJLcom/miui/webkit_api/WebStorage$QuotaUpdater;)V

    return-void
.end method

.method public onReceivedIcon(Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object p1, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    invoke-virtual {p1, v0, p2}, Lcom/miui/webkit_api/WebChromeClient;->onReceivedIcon(Lcom/miui/webkit_api/WebView;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onReceivedTitle(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    iget-object p1, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    invoke-virtual {p1, v0, p2}, Lcom/miui/webkit_api/WebChromeClient;->onReceivedTitle(Lcom/miui/webkit_api/WebView;Ljava/lang/String;)V

    return-void
.end method

.method public onReceivedTouchIconUrl(Ljava/lang/Object;Ljava/lang/String;Z)V
    .locals 1

    iget-object p1, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    invoke-virtual {p1, v0, p2, p3}, Lcom/miui/webkit_api/WebChromeClient;->onReceivedTouchIconUrl(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Z)V

    return-void
.end method

.method public onRequestFocus(Ljava/lang/Object;)V
    .locals 1

    iget-object p1, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    invoke-virtual {p1, v0}, Lcom/miui/webkit_api/WebChromeClient;->onRequestFocus(Lcom/miui/webkit_api/WebView;)V

    return-void
.end method

.method public onShowCustomView(Landroid/view/View;ILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    if-nez p3, :cond_0

    const/4 p3, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/s$a;

    invoke-direct {v1, p3}, Lcom/miui/webkit_api/a/s$a;-><init>(Ljava/lang/Object;)V

    move-object p3, v1

    :goto_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/webkit_api/WebChromeClient;->onShowCustomView(Landroid/view/View;ILcom/miui/webkit_api/WebChromeClient$CustomViewCallback;)V

    return-void
.end method

.method public onShowCustomView(Landroid/view/View;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/s$a;

    invoke-direct {v1, p2}, Lcom/miui/webkit_api/a/s$a;-><init>(Ljava/lang/Object;)V

    move-object p2, v1

    :goto_0
    invoke-virtual {v0, p1, p2}, Lcom/miui/webkit_api/WebChromeClient;->onShowCustomView(Landroid/view/View;Lcom/miui/webkit_api/WebChromeClient$CustomViewCallback;)V

    return-void
.end method

.method public onShowFileChooser(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3

    iget-object p1, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->a:Lcom/miui/webkit_api/WebView;

    const/4 v1, 0x0

    if-nez p2, :cond_0

    move-object v2, v1

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/miui/webkit_api/a/q;

    invoke-direct {v2, p2}, Lcom/miui/webkit_api/a/q;-><init>(Ljava/lang/Object;)V

    :goto_0
    if-nez p3, :cond_1

    goto :goto_1

    :cond_1
    new-instance v1, Lcom/miui/webkit_api/a/s$b;

    invoke-direct {v1, p3}, Lcom/miui/webkit_api/a/s$b;-><init>(Ljava/lang/Object;)V

    :goto_1
    invoke-virtual {p1, v0, v2, v1}, Lcom/miui/webkit_api/WebChromeClient;->onShowFileChooser(Lcom/miui/webkit_api/WebView;Lcom/miui/webkit_api/ValueCallback;Lcom/miui/webkit_api/WebChromeClient$FileChooserParams;)Z

    move-result p1

    return p1
.end method

.method public openFileChooser(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    if-eqz p1, :cond_0

    new-instance p2, Lcom/miui/webkit_api/a/q;

    invoke-direct {p2, p1}, Lcom/miui/webkit_api/a/q;-><init>(Ljava/lang/Object;)V

    const/4 p1, 0x0

    invoke-virtual {p2, p1}, Lcom/miui/webkit_api/a/q;->onReceiveValue(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public setSuperMethods(Ljava/lang/Object;)V
    .locals 5

    iget-object v0, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    if-eqz v0, :cond_0

    :try_start_0
    const-class v0, Lcom/miui/webkit_api/WebChromeClient;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Lcom/miui/webkit_api/b/c;

    const/4 v4, 0x0

    aput-object v3, v2, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "a"

    :try_start_1
    invoke-virtual {v0, v3, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    iget-object v2, p0, Lcom/miui/webkit_api/a/s;->b:Lcom/miui/webkit_api/WebChromeClient;

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v3, Lcom/miui/webkit_api/a/t;

    invoke-direct {v3, p1}, Lcom/miui/webkit_api/a/t;-><init>(Ljava/lang/Object;)V

    aput-object v3, v1, v4

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public setupAutoFill(Landroid/os/Message;)V
    .locals 0

    return-void
.end method
