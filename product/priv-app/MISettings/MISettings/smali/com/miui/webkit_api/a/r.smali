.class Lcom/miui/webkit_api/a/r;
.super Lcom/miui/webkit_api/WebBackForwardList;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/r$a;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/a/r$a;

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebBackForwardList;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/r;->b:Ljava/lang/Object;

    return-void
.end method

.method private a()Lcom/miui/webkit_api/a/r$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/r;->a:Lcom/miui/webkit_api/a/r$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/r$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/r;->b:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/r$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/r;->a:Lcom/miui/webkit_api/a/r$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/r;->a:Lcom/miui/webkit_api/a/r$a;

    return-object v0
.end method


# virtual methods
.method public getCurrentIndex()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/r;->a()Lcom/miui/webkit_api/a/r$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/r;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/r$a;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getCurrentItem()Lcom/miui/webkit_api/WebHistoryItem;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/r;->a()Lcom/miui/webkit_api/a/r$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/r;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/r$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/a/u;

    invoke-direct {v1, v0}, Lcom/miui/webkit_api/a/u;-><init>(Ljava/lang/Object;)V

    return-object v1
.end method

.method public getItemAtIndex(I)Lcom/miui/webkit_api/WebHistoryItem;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/r;->a()Lcom/miui/webkit_api/a/r$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/r;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/r$a;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance v0, Lcom/miui/webkit_api/a/u;

    invoke-direct {v0, p1}, Lcom/miui/webkit_api/a/u;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public getSize()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/r;->a()Lcom/miui/webkit_api/a/r$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/r;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/r$a;->c(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
