.class Lcom/miui/webkit_api/a/k;
.super Lcom/miui/webkit_api/PermissionRequest;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/k$a;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/a/k$a;

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/PermissionRequest;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/k;->b:Ljava/lang/Object;

    return-void
.end method

.method private b()Lcom/miui/webkit_api/a/k$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/k;->a:Lcom/miui/webkit_api/a/k$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/k$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/k;->b:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/k$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/k;->a:Lcom/miui/webkit_api/a/k$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/k;->a:Lcom/miui/webkit_api/a/k$a;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/miui/webkit_api/a/k;->b:Ljava/lang/Object;

    goto/32 :goto_0

    nop
.end method

.method public deny()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/k;->b()Lcom/miui/webkit_api/a/k$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/k;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/k$a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method public getOrigin()Landroid/net/Uri;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/k;->b()Lcom/miui/webkit_api/a/k$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/k;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/k$a;->a(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getResources()[Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/k;->b()Lcom/miui/webkit_api/a/k$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/k;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/k$a;->b(Ljava/lang/Object;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public grant([Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/k;->b()Lcom/miui/webkit_api/a/k$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/k;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/k$a;->a(Ljava/lang/Object;[Ljava/lang/String;)V

    return-void
.end method
