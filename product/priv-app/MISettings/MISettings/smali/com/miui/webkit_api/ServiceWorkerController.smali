.class public abstract Lcom/miui/webkit_api/ServiceWorkerController;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/miui/webkit_api/ServiceWorkerController;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/miui/webkit_api/ServiceWorkerController;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/ServiceWorkerController;->a:Lcom/miui/webkit_api/ServiceWorkerController;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/miui/webkit_api/b/g;->m()Lcom/miui/webkit_api/ServiceWorkerController;

    move-result-object v0

    sput-object v0, Lcom/miui/webkit_api/ServiceWorkerController;->a:Lcom/miui/webkit_api/ServiceWorkerController;

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/ServiceWorkerController;->a:Lcom/miui/webkit_api/ServiceWorkerController;

    return-object v0
.end method


# virtual methods
.method public abstract getServiceWorkerWebSettings()Lcom/miui/webkit_api/ServiceWorkerWebSettings;
.end method

.method public abstract setServiceWorkerClient(Lcom/miui/webkit_api/ServiceWorkerClient;)V
.end method
