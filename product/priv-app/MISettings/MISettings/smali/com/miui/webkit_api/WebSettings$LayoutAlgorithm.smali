.class public final enum Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/webkit_api/WebSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LayoutAlgorithm"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum NARROW_COLUMNS:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

.field public static final enum NORMAL:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

.field public static final enum SINGLE_COLUMN:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

.field public static final enum TEXT_AUTOSIZING:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

.field private static final synthetic a:[Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    const/4 v1, 0x0

    const-string v2, "NORMAL"

    invoke-direct {v0, v2, v1}, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->NORMAL:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    new-instance v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    const/4 v2, 0x1

    const-string v3, "SINGLE_COLUMN"

    invoke-direct {v0, v3, v2}, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->SINGLE_COLUMN:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    new-instance v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    const/4 v3, 0x2

    const-string v4, "NARROW_COLUMNS"

    invoke-direct {v0, v4, v3}, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->NARROW_COLUMNS:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    new-instance v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    const/4 v4, 0x3

    const-string v5, "TEXT_AUTOSIZING"

    invoke-direct {v0, v5, v4}, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    sget-object v5, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->NORMAL:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    aput-object v5, v0, v1

    sget-object v1, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->SINGLE_COLUMN:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    aput-object v1, v0, v2

    sget-object v1, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->NARROW_COLUMNS:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    aput-object v1, v0, v3

    sget-object v1, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    aput-object v1, v0, v4

    sput-object v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->a:[Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;
    .locals 1

    const-class v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    return-object p0
.end method

.method public static values()[Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->a:[Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    invoke-virtual {v0}, [Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    return-object v0
.end method
