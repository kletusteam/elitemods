.class public final Lcom/miui/webkit_api/a/d;
.super Lcom/miui/webkit_api/CookieSyncManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/d$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webkit.CookieSyncManager"

.field private static d:Lcom/miui/webkit_api/a/d;


# instance fields
.field private b:Lcom/miui/webkit_api/a/d$a;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/CookieSyncManager;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/d;->c:Ljava/lang/Object;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/miui/webkit_api/a/d;
    .locals 2

    const-class v0, Lcom/miui/webkit_api/a/d;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/miui/webkit_api/a/d;->d:Lcom/miui/webkit_api/a/d;

    if-nez v1, :cond_1

    invoke-static {p0}, Lcom/miui/webkit_api/a/d$a;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    monitor-exit v0

    return-object p0

    :cond_0
    :try_start_1
    new-instance v1, Lcom/miui/webkit_api/a/d;

    invoke-direct {v1, p0}, Lcom/miui/webkit_api/a/d;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/miui/webkit_api/a/d;->d:Lcom/miui/webkit_api/a/d;

    :cond_1
    sget-object p0, Lcom/miui/webkit_api/a/d;->d:Lcom/miui/webkit_api/a/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized b()Lcom/miui/webkit_api/a/d;
    .locals 3

    const-class v0, Lcom/miui/webkit_api/a/d;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/miui/webkit_api/a/d;->d:Lcom/miui/webkit_api/a/d;

    if-nez v1, :cond_1

    invoke-static {}, Lcom/miui/webkit_api/a/d$a;->a()Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    const/4 v1, 0x0

    monitor-exit v0

    return-object v1

    :cond_0
    :try_start_1
    new-instance v2, Lcom/miui/webkit_api/a/d;

    invoke-direct {v2, v1}, Lcom/miui/webkit_api/a/d;-><init>(Ljava/lang/Object;)V

    sput-object v2, Lcom/miui/webkit_api/a/d;->d:Lcom/miui/webkit_api/a/d;

    :cond_1
    sget-object v1, Lcom/miui/webkit_api/a/d;->d:Lcom/miui/webkit_api/a/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private c()Lcom/miui/webkit_api/a/d$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/d;->b:Lcom/miui/webkit_api/a/d$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/d$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/d;->c:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/d$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/d;->b:Lcom/miui/webkit_api/a/d$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/d;->b:Lcom/miui/webkit_api/a/d$a;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/miui/webkit_api/a/d;->c:Ljava/lang/Object;

    goto/32 :goto_0

    nop
.end method

.method public resetSync()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/d;->c()Lcom/miui/webkit_api/a/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/d;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/d$a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method public run()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/d;->c()Lcom/miui/webkit_api/a/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/d;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/d$a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public startSync()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/d;->c()Lcom/miui/webkit_api/a/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/d;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/d$a;->d(Ljava/lang/Object;)V

    return-void
.end method

.method public stopSync()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/d;->c()Lcom/miui/webkit_api/a/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/d;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/d$a;->e(Ljava/lang/Object;)V

    return-void
.end method

.method public sync()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/d;->c()Lcom/miui/webkit_api/a/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/d;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/d$a;->b(Ljava/lang/Object;)V

    return-void
.end method
