.class Lcom/miui/webkit_api/a/h;
.super Lcom/miui/webkit_api/JsPromptResult;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/h$a;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/a/h$a;

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/JsPromptResult;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/h;->b:Ljava/lang/Object;

    return-void
.end method

.method private b()Lcom/miui/webkit_api/a/h$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/h;->a:Lcom/miui/webkit_api/a/h$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/h$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/h;->b:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/h$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/h;->a:Lcom/miui/webkit_api/a/h$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/h;->a:Lcom/miui/webkit_api/a/h$a;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/miui/webkit_api/a/h;->b:Ljava/lang/Object;

    goto/32 :goto_0

    nop
.end method

.method public final cancel()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/h;->b()Lcom/miui/webkit_api/a/h$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/h;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/h$a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final confirm()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/h;->b()Lcom/miui/webkit_api/a/h$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/h;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/h$a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public confirm(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/h;->b()Lcom/miui/webkit_api/a/h$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/h;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/h$a;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
