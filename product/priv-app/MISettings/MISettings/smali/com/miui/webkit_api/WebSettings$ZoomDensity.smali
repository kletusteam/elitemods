.class public final enum Lcom/miui/webkit_api/WebSettings$ZoomDensity;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/webkit_api/WebSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ZoomDensity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/webkit_api/WebSettings$ZoomDensity;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CLOSE:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

.field public static final enum FAR:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

.field public static final enum MEDIUM:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

.field private static final synthetic b:[Lcom/miui/webkit_api/WebSettings$ZoomDensity;


# instance fields
.field a:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    const/4 v1, 0x0

    const-string v2, "FAR"

    const/16 v3, 0x96

    invoke-direct {v0, v2, v1, v3}, Lcom/miui/webkit_api/WebSettings$ZoomDensity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->FAR:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    new-instance v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    const/4 v2, 0x1

    const-string v3, "MEDIUM"

    const/16 v4, 0x64

    invoke-direct {v0, v3, v2, v4}, Lcom/miui/webkit_api/WebSettings$ZoomDensity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->MEDIUM:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    new-instance v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    const/4 v3, 0x2

    const-string v4, "CLOSE"

    const/16 v5, 0x4b

    invoke-direct {v0, v4, v3, v5}, Lcom/miui/webkit_api/WebSettings$ZoomDensity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->CLOSE:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    sget-object v4, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->FAR:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    aput-object v4, v0, v1

    sget-object v1, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->MEDIUM:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    aput-object v1, v0, v2

    sget-object v1, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->CLOSE:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    aput-object v1, v0, v3

    sput-object v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->b:[Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->a:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/webkit_api/WebSettings$ZoomDensity;
    .locals 1

    const-class v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    return-object p0
.end method

.method public static values()[Lcom/miui/webkit_api/WebSettings$ZoomDensity;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->b:[Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    invoke-virtual {v0}, [Lcom/miui/webkit_api/WebSettings$ZoomDensity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    return-object v0
.end method
