.class public Lcom/miui/webkit_api/WebChromeClient;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/WebChromeClient$FileChooserParams;,
        Lcom/miui/webkit_api/WebChromeClient$CustomViewCallback;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/b/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/miui/webkit_api/b/c;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    return-void
.end method


# virtual methods
.method public getDefaultVideoPoster()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/miui/webkit_api/b/c;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVideoLoadingProgressView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/miui/webkit_api/b/c;->e()Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVisitedHistory(Lcom/miui/webkit_api/ValueCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/webkit_api/ValueCallback<",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/ValueCallback;)V

    :cond_0
    return-void
.end method

.method public onCloseWindow(Lcom/miui/webkit_api/WebView;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/miui/webkit_api/b/c;->b(Lcom/miui/webkit_api/WebView;)V

    :cond_0
    return-void
.end method

.method public onConsoleMessage(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lcom/miui/webkit_api/b/c;->a(Ljava/lang/String;ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onConsoleMessage(Lcom/miui/webkit_api/ConsoleMessage;)Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/ConsoleMessage;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onCreateWindow(Lcom/miui/webkit_api/WebView;ZZLandroid/os/Message;)Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/WebView;ZZLandroid/os/Message;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onExceededDatabaseQuota(Ljava/lang/String;Ljava/lang/String;JJJLcom/miui/webkit_api/WebStorage$QuotaUpdater;)V
    .locals 11

    move-object v0, p0

    iget-object v1, v0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v1, :cond_0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-object/from16 v10, p9

    invoke-interface/range {v1 .. v10}, Lcom/miui/webkit_api/b/c;->a(Ljava/lang/String;Ljava/lang/String;JJJLcom/miui/webkit_api/WebStorage$QuotaUpdater;)V

    :cond_0
    return-void
.end method

.method public onGeolocationPermissionsHidePrompt()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/miui/webkit_api/b/c;->b()V

    :cond_0
    return-void
.end method

.method public onGeolocationPermissionsShowPrompt(Ljava/lang/String;Lcom/miui/webkit_api/GeolocationPermissions$Callback;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/miui/webkit_api/b/c;->a(Ljava/lang/String;Lcom/miui/webkit_api/GeolocationPermissions$Callback;)V

    :cond_0
    return-void
.end method

.method public onHideCustomView()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/miui/webkit_api/b/c;->a()V

    :cond_0
    return-void
.end method

.method public onJsAlert(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsResult;)Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsResult;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onJsBeforeUnload(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsResult;)Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/miui/webkit_api/b/c;->c(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsResult;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onJsConfirm(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsResult;)Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/miui/webkit_api/b/c;->b(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsResult;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onJsPrompt(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsPromptResult;)Z
    .locals 6

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/miui/webkit_api/JsPromptResult;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onJsTimeout()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/miui/webkit_api/b/c;->c()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onPermissionRequest(Lcom/miui/webkit_api/PermissionRequest;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/PermissionRequest;)V

    :cond_0
    return-void
.end method

.method public onPermissionRequestCanceled(Lcom/miui/webkit_api/PermissionRequest;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/miui/webkit_api/b/c;->b(Lcom/miui/webkit_api/PermissionRequest;)V

    :cond_0
    return-void
.end method

.method public onProgressChanged(Lcom/miui/webkit_api/WebView;I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/WebView;I)V

    :cond_0
    return-void
.end method

.method public onReachedMaxAppCacheSize(JJLcom/miui/webkit_api/WebStorage$QuotaUpdater;)V
    .locals 6

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/miui/webkit_api/b/c;->a(JJLcom/miui/webkit_api/WebStorage$QuotaUpdater;)V

    :cond_0
    return-void
.end method

.method public onReceivedIcon(Lcom/miui/webkit_api/WebView;Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/WebView;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public onReceivedTitle(Lcom/miui/webkit_api/WebView;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/WebView;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onReceivedTouchIconUrl(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/WebView;Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public onRequestFocus(Lcom/miui/webkit_api/WebView;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/WebView;)V

    :cond_0
    return-void
.end method

.method public onShowCustomView(Landroid/view/View;ILcom/miui/webkit_api/WebChromeClient$CustomViewCallback;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lcom/miui/webkit_api/b/c;->a(Landroid/view/View;ILcom/miui/webkit_api/WebChromeClient$CustomViewCallback;)V

    :cond_0
    return-void
.end method

.method public onShowCustomView(Landroid/view/View;Lcom/miui/webkit_api/WebChromeClient$CustomViewCallback;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/miui/webkit_api/b/c;->a(Landroid/view/View;Lcom/miui/webkit_api/WebChromeClient$CustomViewCallback;)V

    :cond_0
    return-void
.end method

.method public onShowFileChooser(Lcom/miui/webkit_api/WebView;Lcom/miui/webkit_api/ValueCallback;Lcom/miui/webkit_api/WebChromeClient$FileChooserParams;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/webkit_api/WebView;",
            "Lcom/miui/webkit_api/ValueCallback<",
            "[",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/miui/webkit_api/WebChromeClient$FileChooserParams;",
            ")Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/webkit_api/WebChromeClient;->a:Lcom/miui/webkit_api/b/c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lcom/miui/webkit_api/b/c;->a(Lcom/miui/webkit_api/WebView;Lcom/miui/webkit_api/ValueCallback;Lcom/miui/webkit_api/WebChromeClient$FileChooserParams;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
