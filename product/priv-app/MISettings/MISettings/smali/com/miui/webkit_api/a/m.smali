.class public Lcom/miui/webkit_api/a/m;
.super Lcom/miui/webkit_api/ServiceWorkerController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/m$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webkit.ServiceWorkerController"

.field private static final b:Ljava/lang/String; = "BrowserServiceWorkerController"

.field private static e:Lcom/miui/webkit_api/ServiceWorkerController;


# instance fields
.field private c:Lcom/miui/webkit_api/a/m$a;

.field private d:Ljava/lang/Object;

.field private f:Lcom/miui/webkit_api/ServiceWorkerWebSettings;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/ServiceWorkerController;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/m;->d:Ljava/lang/Object;

    return-void
.end method

.method static a()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreIntVersion()I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const v2, 0x10005

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    :catch_0
    :cond_0
    return v0
.end method

.method public static b()Lcom/miui/webkit_api/ServiceWorkerController;
    .locals 5

    const-string v0, "BrowserServiceWorkerController"

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/miui/webkit_api/a/m;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/miui/webkit_api/a/m;->e:Lcom/miui/webkit_api/ServiceWorkerController;

    if-nez v2, :cond_1

    invoke-static {}, Lcom/miui/webkit_api/a/m$a;->a()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    return-object v1

    :cond_0
    new-instance v3, Lcom/miui/webkit_api/a/m;

    invoke-direct {v3, v2}, Lcom/miui/webkit_api/a/m;-><init>(Ljava/lang/Object;)V

    sput-object v3, Lcom/miui/webkit_api/a/m;->e:Lcom/miui/webkit_api/ServiceWorkerController;

    :cond_1
    sget-object v0, Lcom/miui/webkit_api/a/m;->e:Lcom/miui/webkit_api/ServiceWorkerController;

    return-object v0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "current browser apk is not support getInstance(), current version is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreIntVersion()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", expected version is 0x00010006"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getInstance() catch Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private c()Lcom/miui/webkit_api/a/m$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/m;->c:Lcom/miui/webkit_api/a/m$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/m$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/m;->d:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/m$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/m;->c:Lcom/miui/webkit_api/a/m$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/m;->c:Lcom/miui/webkit_api/a/m$a;

    return-object v0
.end method


# virtual methods
.method public getServiceWorkerWebSettings()Lcom/miui/webkit_api/ServiceWorkerWebSettings;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/miui/webkit_api/a/m;->f:Lcom/miui/webkit_api/ServiceWorkerWebSettings;

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/miui/webkit_api/a/m;->c()Lcom/miui/webkit_api/a/m$a;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/webkit_api/a/m;->d:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/miui/webkit_api/a/m$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    new-instance v2, Lcom/miui/webkit_api/a/n;

    invoke-direct {v2, v1}, Lcom/miui/webkit_api/a/n;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/miui/webkit_api/a/m;->f:Lcom/miui/webkit_api/ServiceWorkerWebSettings;

    :cond_1
    iget-object v0, p0, Lcom/miui/webkit_api/a/m;->f:Lcom/miui/webkit_api/ServiceWorkerWebSettings;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getServiceWorkerWebSettings() catch Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BrowserServiceWorkerController"

    invoke-static {v2, v1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public setServiceWorkerClient(Lcom/miui/webkit_api/ServiceWorkerClient;)V
    .locals 3

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/m;->c()Lcom/miui/webkit_api/a/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/m;->d:Ljava/lang/Object;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/miui/webkit_api/a/l;

    invoke-direct {v2, p1}, Lcom/miui/webkit_api/a/l;-><init>(Lcom/miui/webkit_api/ServiceWorkerClient;)V

    invoke-static {v2}, Lcom/miui/webkit_api/a/ak;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/m$a;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setServiceWorkerClient(client) catch Exception: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "BrowserServiceWorkerController"

    invoke-static {v0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method
