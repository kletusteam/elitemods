.class Lcom/miui/webkit_api/c/l;
.super Lcom/miui/webkit_api/PermissionRequest;


# static fields
.field private static final b:Ljava/lang/String; = "android.webkit.resource.VIDEO_CAPTURE"

.field private static final c:Ljava/lang/String; = "android.webkit.resource.AUDIO_CAPTURE"

.field private static final d:Ljava/lang/String; = "android.webkit.resource.PROTECTED_MEDIA_ID"

.field private static final e:Ljava/lang/String; = "android.webkit.resource.MIDI_SYSEX"


# instance fields
.field private a:Landroid/webkit/PermissionRequest;


# direct methods
.method constructor <init>(Landroid/webkit/PermissionRequest;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/PermissionRequest;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/l;->a:Landroid/webkit/PermissionRequest;

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "android.webkit.resource.VIDEO_CAPTURE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "com.miui.webkit.resource.VIDEO_CAPTURE"

    return-object p0

    :cond_0
    const-string v0, "android.webkit.resource.AUDIO_CAPTURE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p0, "com.miui.webkit.resource.AUDIO_CAPTURE"

    return-object p0

    :cond_1
    const-string v0, "android.webkit.resource.PROTECTED_MEDIA_ID"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p0, "com.miui.webkit.resource.PROTECTED_MEDIA_ID"

    return-object p0

    :cond_2
    const-string v0, "android.webkit.resource.MIDI_SYSEX"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string p0, "com.miui.webkit.resource.MIDI_SYSEX"

    :cond_3
    return-object p0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "com.miui.webkit.resource.VIDEO_CAPTURE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "android.webkit.resource.VIDEO_CAPTURE"

    return-object p0

    :cond_0
    const-string v0, "com.miui.webkit.resource.AUDIO_CAPTURE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p0, "android.webkit.resource.AUDIO_CAPTURE"

    return-object p0

    :cond_1
    const-string v0, "com.miui.webkit.resource.PROTECTED_MEDIA_ID"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p0, "android.webkit.resource.PROTECTED_MEDIA_ID"

    :cond_2
    return-object p0
.end method


# virtual methods
.method a()Landroid/webkit/PermissionRequest;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/miui/webkit_api/c/l;->a:Landroid/webkit/PermissionRequest;

    goto/32 :goto_0

    nop
.end method

.method public deny()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/l;->a:Landroid/webkit/PermissionRequest;

    invoke-virtual {v0}, Landroid/webkit/PermissionRequest;->deny()V

    return-void
.end method

.method public getOrigin()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/l;->a:Landroid/webkit/PermissionRequest;

    invoke-virtual {v0}, Landroid/webkit/PermissionRequest;->getOrigin()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getResources()[Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/miui/webkit_api/c/l;->a:Landroid/webkit/PermissionRequest;

    invoke-virtual {v0}, Landroid/webkit/PermissionRequest;->getResources()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-static {v3}, Lcom/miui/webkit_api/c/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public grant([Ljava/lang/String;)V
    .locals 4

    array-length v0, p1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v3, p1, v2

    invoke-static {v3}, Lcom/miui/webkit_api/c/l;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/miui/webkit_api/c/l;->a:Landroid/webkit/PermissionRequest;

    invoke-virtual {p1, v1}, Landroid/webkit/PermissionRequest;->grant([Ljava/lang/String;)V

    return-void
.end method
