.class public Lcom/miui/webkit_api/a/ab$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/webkit_api/b/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/webkit_api/a/ab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/ab$a$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webkit.WebView$HitTestResult"


# instance fields
.field private b:Lcom/miui/webkit_api/a/ab$a$a;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/ab$a;->c:Ljava/lang/Object;

    return-void
.end method

.method private c()Lcom/miui/webkit_api/a/ab$a$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/ab$a;->b:Lcom/miui/webkit_api/a/ab$a$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/ab$a$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/ab$a;->c:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/ab$a$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/ab$a;->b:Lcom/miui/webkit_api/a/ab$a$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/ab$a;->b:Lcom/miui/webkit_api/a/ab$a$a;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/ab$a;->c()Lcom/miui/webkit_api/a/ab$a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/ab$a;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/ab$a$a;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/ab$a;->c()Lcom/miui/webkit_api/a/ab$a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/ab$a;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/ab$a$a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
