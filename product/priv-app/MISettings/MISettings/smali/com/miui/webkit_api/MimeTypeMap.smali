.class public abstract Lcom/miui/webkit_api/MimeTypeMap;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/miui/webkit_api/MimeTypeMap;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/miui/webkit_api/b/g;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getSingleton()Lcom/miui/webkit_api/MimeTypeMap;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/MimeTypeMap;->a:Lcom/miui/webkit_api/MimeTypeMap;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/miui/webkit_api/b/g;->l()Lcom/miui/webkit_api/MimeTypeMap;

    move-result-object v0

    sput-object v0, Lcom/miui/webkit_api/MimeTypeMap;->a:Lcom/miui/webkit_api/MimeTypeMap;

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/MimeTypeMap;->a:Lcom/miui/webkit_api/MimeTypeMap;

    return-object v0
.end method


# virtual methods
.method public abstract getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract hasExtension(Ljava/lang/String;)Z
.end method

.method public abstract hasMimeType(Ljava/lang/String;)Z
.end method
