.class public Lcom/miui/webkit_api/c/v;
.super Lcom/miui/webkit_api/WebIconDatabase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/c/v$a;
    }
.end annotation


# instance fields
.field private a:Landroid/webkit/WebIconDatabase;


# direct methods
.method public constructor <init>(Landroid/webkit/WebIconDatabase;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebIconDatabase;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/v;->a:Landroid/webkit/WebIconDatabase;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/v;->a:Landroid/webkit/WebIconDatabase;

    invoke-virtual {v0}, Landroid/webkit/WebIconDatabase;->close()V

    return-void
.end method

.method public open(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/v;->a:Landroid/webkit/WebIconDatabase;

    invoke-virtual {v0, p1}, Landroid/webkit/WebIconDatabase;->open(Ljava/lang/String;)V

    return-void
.end method

.method public releaseIconForPageUrl(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/v;->a:Landroid/webkit/WebIconDatabase;

    invoke-virtual {v0, p1}, Landroid/webkit/WebIconDatabase;->releaseIconForPageUrl(Ljava/lang/String;)V

    return-void
.end method

.method public removeAllIcons()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/v;->a:Landroid/webkit/WebIconDatabase;

    invoke-virtual {v0}, Landroid/webkit/WebIconDatabase;->removeAllIcons()V

    return-void
.end method

.method public requestIconForPageUrl(Ljava/lang/String;Lcom/miui/webkit_api/WebIconDatabase$IconListener;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/c/v;->a:Landroid/webkit/WebIconDatabase;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/c/v$a;

    invoke-direct {v1, p2}, Lcom/miui/webkit_api/c/v$a;-><init>(Lcom/miui/webkit_api/WebIconDatabase$IconListener;)V

    move-object p2, v1

    :goto_0
    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebIconDatabase;->requestIconForPageUrl(Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V

    return-void
.end method

.method public retainIconForPageUrl(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/v;->a:Landroid/webkit/WebIconDatabase;

    invoke-virtual {v0, p1}, Landroid/webkit/WebIconDatabase;->retainIconForPageUrl(Ljava/lang/String;)V

    return-void
.end method
