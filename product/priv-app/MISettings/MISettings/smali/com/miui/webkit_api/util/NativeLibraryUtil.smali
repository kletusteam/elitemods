.class public Lcom/miui/webkit_api/util/NativeLibraryUtil;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String; = "NativeLibraryUtil"

.field private static final b:Ljava/lang/String; = "mi_webview_lib"

.field private static final c:Ljava/lang/String; = "mi_lib_arm.stamp"

.field private static final d:Ljava/lang/String; = "mi_lib_arm64.stamp"

.field private static final e:Ljava/lang/String; = "mi_webview_sdk"

.field private static final f:Ljava/lang/String; = "arm64_version"

.field private static final g:Ljava/lang/String; = "arm_version"

.field private static final h:Ljava/lang/String; = "arm64_last_modified"

.field private static final i:Ljava/lang/String; = "arm_last_modified"

.field private static final j:Ljava/lang/String; = "libmiui_chromium_lite.so"

.field private static final k:Ljava/lang/String; = "arm.so"

.field private static final l:Ljava/lang/String; = "arm64.so"

.field private static final m:Ljava/lang/String; = "libmiui_chromium.so"

.field private static final n:Ljava/lang/String; = "com.miui.org.chromium.base.library_loader.MiuiNativeLibraryConfig"

.field private static o:Ljava/lang/Class;

.field private static p:Z

.field private static q:Ljava/lang/Object;

.field private static r:Z

.field private static s:Z

.field private static t:Landroid/content/Context;

.field private static u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/webkit_api/MiuiDelegate$PreloadCallback;",
            ">;"
        }
    .end annotation
.end field

.field private static v:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->q:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->u:Ljava/util/ArrayList;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->v:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sput-object p0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->t:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/miui/webkit_api/MiuiDelegate$PreloadCallback;)V
    .locals 3

    sget-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->q:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lcom/miui/webkit_api/util/NativeLibraryUtil;->s:Z

    if-eqz v1, :cond_1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->a(Lcom/miui/webkit_api/MiuiDelegate$PreloadCallback;)V

    :cond_0
    monitor-exit v0

    return-void

    :cond_1
    if-eqz p1, :cond_2

    sget-object v1, Lcom/miui/webkit_api/util/NativeLibraryUtil;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    sget-boolean p1, Lcom/miui/webkit_api/util/NativeLibraryUtil;->r:Z

    if-eqz p1, :cond_3

    monitor-exit v0

    return-void

    :cond_3
    const/4 p1, 0x1

    sput-boolean p1, Lcom/miui/webkit_api/util/NativeLibraryUtil;->r:Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    sput-object p1, Lcom/miui/webkit_api/util/NativeLibraryUtil;->t:Landroid/content/Context;

    invoke-static {}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->c()Z

    move-result p1

    if-nez p1, :cond_4

    invoke-static {}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->d()V

    return-void

    :cond_4
    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->c(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-static {}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->d()V

    return-void

    :cond_5
    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->d(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_6

    invoke-static {}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->d()V

    return-void

    :cond_6
    invoke-static {}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->a()Z

    move-result p1

    sput-boolean p1, Lcom/miui/webkit_api/util/NativeLibraryUtil;->p:Z

    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "decompressed version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , latest version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NativeLibraryUtil"

    invoke-static {v2, v1}, Lcom/miui/webkit_api/util/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_7

    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->i(Landroid/content/Context;)V

    invoke-static {}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->d()V

    return-void

    :cond_7
    invoke-static {p0, v0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_8

    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->i(Landroid/content/Context;)V

    :cond_8
    invoke-static {}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->d()V

    return-void

    :catchall_0
    move-exception p0

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p0
.end method

.method private static a(Lcom/miui/webkit_api/MiuiDelegate$PreloadCallback;)V
    .locals 2

    sget-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->v:Landroid/os/Handler;

    new-instance v1, Lcom/miui/webkit_api/util/NativeLibraryUtil$1;

    invoke-direct {v1, p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil$1;-><init>(Lcom/miui/webkit_api/MiuiDelegate$PreloadCallback;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static a()Z
    .locals 2

    const-string v0, "java.library.path"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "64"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->e(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    const-string v2, "miui_lib_util"

    invoke-static {v2}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->nativeDecompress(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p0, "NativeLibraryUtil"

    const-string p1, "decompress library file failed"

    invoke-static {p0, p1}, Lcom/miui/webkit_api/util/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x0

    return p0

    :cond_0
    invoke-static {p0, p1}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->b(Landroid/content/Context;Ljava/lang/String;)V

    const/4 p0, 0x1

    return p0
.end method

.method public static b(Landroid/content/Context;)Ljava/io/File;
    .locals 2

    const-string v0, "mi_webview_lib"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object p0

    sget-boolean v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->p:Z

    if-eqz v0, :cond_0

    const-string v0, "arm64.so"

    goto :goto_0

    :cond_0
    const-string v0, "arm.so"

    :goto_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    const-string v0, "mi_webview_sdk"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-boolean v1, Lcom/miui/webkit_api/util/NativeLibraryUtil;->p:Z

    if-eqz v1, :cond_0

    const-string v1, "arm64_version"

    goto :goto_0

    :cond_0
    const-string v1, "arm_version"

    :goto_0
    sget-boolean v2, Lcom/miui/webkit_api/util/NativeLibraryUtil;->p:Z

    if-eqz v2, :cond_1

    const-string v2, "arm64_last_modified"

    goto :goto_1

    :cond_1
    const-string v2, "arm_last_modified"

    :goto_1
    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object p0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-interface {p1, v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public static b()Z
    .locals 2

    sget-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->t:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-static {v0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->t:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method private static c()Z
    .locals 2

    sget-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->o:Ljava/lang/Class;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "com.miui.org.chromium.base.library_loader.MiuiNativeLibraryConfig"

    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->o:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

.method private static c(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    iget-object p0, p0, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    const-string v1, "libmiui_chromium.so"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p0

    return p0
.end method

.method private static d()V
    .locals 2

    sget-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->q:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    sput-boolean v1, Lcom/miui/webkit_api/util/NativeLibraryUtil;->s:Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/webkit_api/MiuiDelegate$PreloadCallback;

    invoke-static {v1}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->a(Lcom/miui/webkit_api/MiuiDelegate$PreloadCallback;)V

    goto :goto_0

    :cond_0
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private static d(Landroid/content/Context;)Z
    .locals 0

    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->e(Landroid/content/Context;)Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result p0

    return p0
.end method

.method private static e(Landroid/content/Context;)Ljava/io/File;
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    iget-object p0, p0, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    const-string v1, "libmiui_chromium_lite.so"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 9

    const-string v0, "mi_webview_sdk"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-boolean v1, Lcom/miui/webkit_api/util/NativeLibraryUtil;->p:Z

    if-eqz v1, :cond_0

    const-string v1, "arm64_version"

    goto :goto_0

    :cond_0
    const-string v1, "arm_version"

    :goto_0
    sget-boolean v2, Lcom/miui/webkit_api/util/NativeLibraryUtil;->p:Z

    if-eqz v2, :cond_1

    const-string v2, "arm64_last_modified"

    goto :goto_1

    :cond_1
    const-string v2, "arm_last_modified"

    :goto_1
    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x0

    invoke-interface {v0, v2, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-object v3

    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    cmp-long p0, v7, v5

    if-eqz p0, :cond_3

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-object v3

    :cond_3
    return-object v4
.end method

.method private static g(Landroid/content/Context;)Ljava/io/File;
    .locals 2

    const-string v0, "mi_webview_lib"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object p0

    sget-boolean v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->p:Z

    if-eqz v0, :cond_0

    const-string v0, "mi_lib_arm64.stamp"

    goto :goto_0

    :cond_0
    const-string v0, "mi_lib_arm.stamp"

    :goto_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private static h(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p0

    const/16 v0, 0x200

    const/4 v1, 0x0

    :try_start_0
    new-array v0, v0, [B

    sget-boolean v2, Lcom/miui/webkit_api/util/NativeLibraryUtil;->p:Z

    if-eqz v2, :cond_0

    const-string v2, "mi_lib_arm64.stamp"

    goto :goto_0

    :cond_0
    const-string v2, "mi_lib_arm.stamp"

    :goto_0
    invoke-virtual {p0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4, v2}, Ljava/lang/String;-><init>([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p0, :cond_1

    :try_start_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    :cond_1
    return-object v3

    :catchall_0
    move-exception v0

    move-object v5, v0

    move-object v0, p0

    move-object p0, v5

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v5, v0

    move-object v0, p0

    move-object p0, v5

    goto :goto_1

    :catchall_1
    move-exception p0

    move-object v0, v1

    goto :goto_2

    :catch_2
    move-exception p0

    move-object v0, v1

    :goto_1
    :try_start_3
    const-string v2, "NativeLibraryUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "read latest version failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, Lcom/miui/webkit_api/util/a;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-eqz v0, :cond_2

    :try_start_4
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    :cond_2
    return-object v1

    :catchall_2
    move-exception p0

    :goto_2
    if-eqz v0, :cond_3

    :try_start_5
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    :cond_3
    throw p0
.end method

.method private static i(Landroid/content/Context;)V
    .locals 6

    :try_start_0
    sget-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->o:Ljava/lang/Class;

    const-string v1, "setDecompressedLibraryPath"

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v5

    const/4 p0, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/miui/webkit_api/util/NativeLibraryUtil;->o:Ljava/lang/Class;

    const-string v1, "setUseDecompressedLibrary"

    new-array v3, v2, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "update library config failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "NativeLibraryUtil"

    invoke-static {v0, p0}, Lcom/miui/webkit_api/util/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private static native nativeDecompress(Ljava/lang/String;Ljava/lang/String;)Z
.end method
