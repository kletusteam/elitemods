.class public Lcom/miui/webkit_api/c/aa;
.super Lcom/miui/webkit_api/WebStorage;


# instance fields
.field private a:Landroid/webkit/WebStorage;


# direct methods
.method public constructor <init>(Landroid/webkit/WebStorage;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebStorage;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/aa;->a:Landroid/webkit/WebStorage;

    return-void
.end method


# virtual methods
.method public deleteAllData()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/aa;->a:Landroid/webkit/WebStorage;

    invoke-virtual {v0}, Landroid/webkit/WebStorage;->deleteAllData()V

    return-void
.end method

.method public deleteOrigin(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/aa;->a:Landroid/webkit/WebStorage;

    invoke-virtual {v0, p1}, Landroid/webkit/WebStorage;->deleteOrigin(Ljava/lang/String;)V

    return-void
.end method

.method public getOrigins(Lcom/miui/webkit_api/ValueCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/webkit_api/ValueCallback<",
            "Ljava/util/Map;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/webkit_api/c/aa;->a:Landroid/webkit/WebStorage;

    new-instance v1, Lcom/miui/webkit_api/c/q;

    invoke-direct {v1, p1}, Lcom/miui/webkit_api/c/q;-><init>(Lcom/miui/webkit_api/ValueCallback;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebStorage;->getOrigins(Landroid/webkit/ValueCallback;)V

    return-void
.end method

.method public getQuotaForOrigin(Ljava/lang/String;Lcom/miui/webkit_api/ValueCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/miui/webkit_api/ValueCallback<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/webkit_api/c/aa;->a:Landroid/webkit/WebStorage;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/c/q;

    invoke-direct {v1, p2}, Lcom/miui/webkit_api/c/q;-><init>(Lcom/miui/webkit_api/ValueCallback;)V

    move-object p2, v1

    :goto_0
    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebStorage;->getQuotaForOrigin(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    return-void
.end method

.method public getUsageForOrigin(Ljava/lang/String;Lcom/miui/webkit_api/ValueCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/miui/webkit_api/ValueCallback<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/webkit_api/c/aa;->a:Landroid/webkit/WebStorage;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/c/q;

    invoke-direct {v1, p2}, Lcom/miui/webkit_api/c/q;-><init>(Lcom/miui/webkit_api/ValueCallback;)V

    move-object p2, v1

    :goto_0
    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebStorage;->getUsageForOrigin(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    return-void
.end method

.method public setQuotaForOrigin(Ljava/lang/String;J)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/aa;->a:Landroid/webkit/WebStorage;

    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/WebStorage;->setQuotaForOrigin(Ljava/lang/String;J)V

    return-void
.end method
