.class Lcom/miui/webkit_api/c/z;
.super Lcom/miui/webkit_api/WebSettings;


# static fields
.field private static final a:Ljava/lang/String; = "SystemWebSettings"


# instance fields
.field private b:Landroid/webkit/WebSettings;


# direct methods
.method public constructor <init>(Landroid/webkit/WebSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebSettings;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    return-void
.end method


# virtual methods
.method a(Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;)Landroid/webkit/WebSettings$LayoutAlgorithm;
    .locals 1

    goto/32 :goto_a

    nop

    :goto_0
    return-object p1

    :goto_1
    goto/32 :goto_b

    nop

    :goto_2
    sget-object p1, Landroid/webkit/WebSettings$LayoutAlgorithm;->NARROW_COLUMNS:Landroid/webkit/WebSettings$LayoutAlgorithm;

    goto/32 :goto_9

    nop

    :goto_3
    if-eq p1, v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_5

    nop

    :goto_4
    sget-object p1, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    goto/32 :goto_6

    nop

    :goto_5
    sget-object p1, Landroid/webkit/WebSettings$LayoutAlgorithm;->NARROW_COLUMNS:Landroid/webkit/WebSettings$LayoutAlgorithm;

    goto/32 :goto_0

    nop

    :goto_6
    return-object p1

    :goto_7
    goto/32 :goto_e

    nop

    :goto_8
    if-eq p1, v0, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_d

    nop

    :goto_9
    return-object p1

    :goto_a
    sget-object v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->NORMAL:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    goto/32 :goto_12

    nop

    :goto_b
    sget-object v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    goto/32 :goto_c

    nop

    :goto_c
    if-eq p1, v0, :cond_2

    goto/32 :goto_14

    :cond_2
    goto/32 :goto_15

    nop

    :goto_d
    sget-object p1, Landroid/webkit/WebSettings$LayoutAlgorithm;->SINGLE_COLUMN:Landroid/webkit/WebSettings$LayoutAlgorithm;

    goto/32 :goto_f

    nop

    :goto_e
    sget-object v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->SINGLE_COLUMN:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    goto/32 :goto_8

    nop

    :goto_f
    return-object p1

    :goto_10
    goto/32 :goto_11

    nop

    :goto_11
    sget-object v0, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->NARROW_COLUMNS:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    goto/32 :goto_3

    nop

    :goto_12
    if-eq p1, v0, :cond_3

    goto/32 :goto_7

    :cond_3
    goto/32 :goto_4

    nop

    :goto_13
    return-object p1

    :goto_14
    goto/32 :goto_2

    nop

    :goto_15
    sget-object p1, Landroid/webkit/WebSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Landroid/webkit/WebSettings$LayoutAlgorithm;

    goto/32 :goto_13

    nop
.end method

.method a(Lcom/miui/webkit_api/WebSettings$PluginState;)Landroid/webkit/WebSettings$PluginState;
    .locals 1

    goto/32 :goto_9

    nop

    :goto_0
    if-eq p1, v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_d

    nop

    :goto_1
    return-object p1

    :goto_2
    goto/32 :goto_7

    nop

    :goto_3
    sget-object p1, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    goto/32 :goto_b

    nop

    :goto_4
    return-object p1

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    if-eq p1, v0, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_e

    nop

    :goto_7
    sget-object v0, Lcom/miui/webkit_api/WebSettings$PluginState;->OFF:Lcom/miui/webkit_api/WebSettings$PluginState;

    goto/32 :goto_0

    nop

    :goto_8
    sget-object v0, Lcom/miui/webkit_api/WebSettings$PluginState;->ON_DEMAND:Lcom/miui/webkit_api/WebSettings$PluginState;

    goto/32 :goto_6

    nop

    :goto_9
    sget-object v0, Lcom/miui/webkit_api/WebSettings$PluginState;->ON:Lcom/miui/webkit_api/WebSettings$PluginState;

    goto/32 :goto_a

    nop

    :goto_a
    if-eq p1, v0, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_3

    nop

    :goto_b
    return-object p1

    :goto_c
    goto/32 :goto_8

    nop

    :goto_d
    sget-object p1, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    goto/32 :goto_4

    nop

    :goto_e
    sget-object p1, Landroid/webkit/WebSettings$PluginState;->ON_DEMAND:Landroid/webkit/WebSettings$PluginState;

    goto/32 :goto_1

    nop

    :goto_f
    sget-object p1, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    goto/32 :goto_10

    nop

    :goto_10
    return-object p1
.end method

.method a(Lcom/miui/webkit_api/WebSettings$RenderPriority;)Landroid/webkit/WebSettings$RenderPriority;
    .locals 1

    goto/32 :goto_d

    nop

    :goto_0
    sget-object p1, Landroid/webkit/WebSettings$RenderPriority;->NORMAL:Landroid/webkit/WebSettings$RenderPriority;

    goto/32 :goto_f

    nop

    :goto_1
    sget-object p1, Landroid/webkit/WebSettings$RenderPriority;->LOW:Landroid/webkit/WebSettings$RenderPriority;

    goto/32 :goto_8

    nop

    :goto_2
    if-eq p1, v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_1

    nop

    :goto_3
    return-object p1

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    return-object p1

    :goto_6
    goto/32 :goto_c

    nop

    :goto_7
    sget-object p1, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    goto/32 :goto_5

    nop

    :goto_8
    return-object p1

    :goto_9
    goto/32 :goto_0

    nop

    :goto_a
    sget-object v0, Lcom/miui/webkit_api/WebSettings$RenderPriority;->HIGH:Lcom/miui/webkit_api/WebSettings$RenderPriority;

    goto/32 :goto_e

    nop

    :goto_b
    sget-object p1, Landroid/webkit/WebSettings$RenderPriority;->NORMAL:Landroid/webkit/WebSettings$RenderPriority;

    goto/32 :goto_3

    nop

    :goto_c
    sget-object v0, Lcom/miui/webkit_api/WebSettings$RenderPriority;->LOW:Lcom/miui/webkit_api/WebSettings$RenderPriority;

    goto/32 :goto_2

    nop

    :goto_d
    sget-object v0, Lcom/miui/webkit_api/WebSettings$RenderPriority;->NORMAL:Lcom/miui/webkit_api/WebSettings$RenderPriority;

    goto/32 :goto_10

    nop

    :goto_e
    if-eq p1, v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_7

    nop

    :goto_f
    return-object p1

    :goto_10
    if-eq p1, v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_b

    nop
.end method

.method a(Lcom/miui/webkit_api/WebSettings$TextSize;)Landroid/webkit/WebSettings$TextSize;
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    sget-object v0, Lcom/miui/webkit_api/WebSettings$TextSize;->LARGEST:Lcom/miui/webkit_api/WebSettings$TextSize;

    goto/32 :goto_f

    nop

    :goto_1
    sget-object p1, Landroid/webkit/WebSettings$TextSize;->NORMAL:Landroid/webkit/WebSettings$TextSize;

    goto/32 :goto_12

    nop

    :goto_2
    if-eq p1, v0, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_16

    nop

    :goto_3
    return-object p1

    :goto_4
    return-object p1

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    sget-object p1, Landroid/webkit/WebSettings$TextSize;->LARGEST:Landroid/webkit/WebSettings$TextSize;

    goto/32 :goto_9

    nop

    :goto_7
    sget-object v0, Lcom/miui/webkit_api/WebSettings$TextSize;->SMALLEST:Lcom/miui/webkit_api/WebSettings$TextSize;

    goto/32 :goto_2

    nop

    :goto_8
    sget-object v0, Lcom/miui/webkit_api/WebSettings$TextSize;->SMALLER:Lcom/miui/webkit_api/WebSettings$TextSize;

    goto/32 :goto_11

    nop

    :goto_9
    return-object p1

    :goto_a
    goto/32 :goto_1a

    nop

    :goto_b
    sget-object v0, Lcom/miui/webkit_api/WebSettings$TextSize;->NORMAL:Lcom/miui/webkit_api/WebSettings$TextSize;

    goto/32 :goto_17

    nop

    :goto_c
    return-object p1

    :goto_d
    goto/32 :goto_b

    nop

    :goto_e
    sget-object p1, Landroid/webkit/WebSettings$TextSize;->LARGER:Landroid/webkit/WebSettings$TextSize;

    goto/32 :goto_4

    nop

    :goto_f
    if-eq p1, v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_6

    nop

    :goto_10
    sget-object v0, Lcom/miui/webkit_api/WebSettings$TextSize;->LARGER:Lcom/miui/webkit_api/WebSettings$TextSize;

    goto/32 :goto_15

    nop

    :goto_11
    if-eq p1, v0, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_14

    nop

    :goto_12
    return-object p1

    :goto_13
    goto/32 :goto_10

    nop

    :goto_14
    sget-object p1, Landroid/webkit/WebSettings$TextSize;->SMALLER:Landroid/webkit/WebSettings$TextSize;

    goto/32 :goto_c

    nop

    :goto_15
    if-eq p1, v0, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_e

    nop

    :goto_16
    sget-object p1, Landroid/webkit/WebSettings$TextSize;->SMALLEST:Landroid/webkit/WebSettings$TextSize;

    goto/32 :goto_18

    nop

    :goto_17
    if-eq p1, v0, :cond_4

    goto/32 :goto_13

    :cond_4
    goto/32 :goto_1

    nop

    :goto_18
    return-object p1

    :goto_19
    goto/32 :goto_8

    nop

    :goto_1a
    sget-object p1, Landroid/webkit/WebSettings$TextSize;->NORMAL:Landroid/webkit/WebSettings$TextSize;

    goto/32 :goto_3

    nop
.end method

.method a(Lcom/miui/webkit_api/WebSettings$ZoomDensity;)Landroid/webkit/WebSettings$ZoomDensity;
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    sget-object p1, Landroid/webkit/WebSettings$ZoomDensity;->FAR:Landroid/webkit/WebSettings$ZoomDensity;

    goto/32 :goto_f

    nop

    :goto_1
    sget-object v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->MEDIUM:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    goto/32 :goto_e

    nop

    :goto_2
    if-eq p1, v0, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    sget-object v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->FAR:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    goto/32 :goto_2

    nop

    :goto_4
    if-eq p1, v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_9

    nop

    :goto_5
    sget-object p1, Landroid/webkit/WebSettings$ZoomDensity;->MEDIUM:Landroid/webkit/WebSettings$ZoomDensity;

    goto/32 :goto_a

    nop

    :goto_6
    return-object p1

    :goto_7
    return-object p1

    :goto_8
    goto/32 :goto_d

    nop

    :goto_9
    sget-object p1, Landroid/webkit/WebSettings$ZoomDensity;->CLOSE:Landroid/webkit/WebSettings$ZoomDensity;

    goto/32 :goto_7

    nop

    :goto_a
    return-object p1

    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    sget-object v0, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->CLOSE:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    goto/32 :goto_4

    nop

    :goto_d
    sget-object p1, Landroid/webkit/WebSettings$ZoomDensity;->MEDIUM:Landroid/webkit/WebSettings$ZoomDensity;

    goto/32 :goto_6

    nop

    :goto_e
    if-eq p1, v0, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_5

    nop

    :goto_f
    return-object p1

    :goto_10
    goto/32 :goto_1

    nop
.end method

.method a(Landroid/webkit/WebSettings$LayoutAlgorithm;)Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    sget-object p1, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->SINGLE_COLUMN:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    goto/32 :goto_10

    nop

    :goto_1
    sget-object v0, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    goto/32 :goto_9

    nop

    :goto_2
    return-object p1

    :goto_3
    goto/32 :goto_8

    nop

    :goto_4
    if-eq p1, v0, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_0

    nop

    :goto_5
    sget-object p1, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->NORMAL:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    goto/32 :goto_b

    nop

    :goto_6
    if-eq p1, v0, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_d

    nop

    :goto_7
    sget-object v0, Landroid/webkit/WebSettings$LayoutAlgorithm;->SINGLE_COLUMN:Landroid/webkit/WebSettings$LayoutAlgorithm;

    goto/32 :goto_4

    nop

    :goto_8
    sget-object v0, Landroid/webkit/WebSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Landroid/webkit/WebSettings$LayoutAlgorithm;

    goto/32 :goto_6

    nop

    :goto_9
    if-eq p1, v0, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_5

    nop

    :goto_a
    sget-object p1, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->NARROW_COLUMNS:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    goto/32 :goto_2

    nop

    :goto_b
    return-object p1

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    sget-object p1, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    goto/32 :goto_12

    nop

    :goto_e
    sget-object p1, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->NARROW_COLUMNS:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    goto/32 :goto_f

    nop

    :goto_f
    return-object p1

    :goto_10
    return-object p1

    :goto_11
    goto/32 :goto_14

    nop

    :goto_12
    return-object p1

    :goto_13
    goto/32 :goto_e

    nop

    :goto_14
    sget-object v0, Landroid/webkit/WebSettings$LayoutAlgorithm;->NARROW_COLUMNS:Landroid/webkit/WebSettings$LayoutAlgorithm;

    goto/32 :goto_15

    nop

    :goto_15
    if-eq p1, v0, :cond_3

    goto/32 :goto_3

    :cond_3
    goto/32 :goto_a

    nop
.end method

.method a(Landroid/webkit/WebSettings$PluginState;)Lcom/miui/webkit_api/WebSettings$PluginState;
    .locals 1

    goto/32 :goto_10

    nop

    :goto_0
    if-eq p1, v0, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_f

    nop

    :goto_1
    if-eq p1, v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_3

    nop

    :goto_2
    sget-object p1, Lcom/miui/webkit_api/WebSettings$PluginState;->OFF:Lcom/miui/webkit_api/WebSettings$PluginState;

    goto/32 :goto_5

    nop

    :goto_3
    sget-object p1, Lcom/miui/webkit_api/WebSettings$PluginState;->ON:Lcom/miui/webkit_api/WebSettings$PluginState;

    goto/32 :goto_9

    nop

    :goto_4
    sget-object v0, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    goto/32 :goto_0

    nop

    :goto_5
    return-object p1

    :goto_6
    sget-object p1, Lcom/miui/webkit_api/WebSettings$PluginState;->ON_DEMAND:Lcom/miui/webkit_api/WebSettings$PluginState;

    goto/32 :goto_7

    nop

    :goto_7
    return-object p1

    :goto_8
    goto/32 :goto_4

    nop

    :goto_9
    return-object p1

    :goto_a
    goto/32 :goto_c

    nop

    :goto_b
    if-eq p1, v0, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_6

    nop

    :goto_c
    sget-object v0, Landroid/webkit/WebSettings$PluginState;->ON_DEMAND:Landroid/webkit/WebSettings$PluginState;

    goto/32 :goto_b

    nop

    :goto_d
    return-object p1

    :goto_e
    goto/32 :goto_2

    nop

    :goto_f
    sget-object p1, Lcom/miui/webkit_api/WebSettings$PluginState;->OFF:Lcom/miui/webkit_api/WebSettings$PluginState;

    goto/32 :goto_d

    nop

    :goto_10
    sget-object v0, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    goto/32 :goto_1

    nop
.end method

.method a(Landroid/webkit/WebSettings$RenderPriority;)Lcom/miui/webkit_api/WebSettings$RenderPriority;
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    if-eq p1, v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_8

    nop

    :goto_1
    return-object p1

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    sget-object p1, Lcom/miui/webkit_api/WebSettings$RenderPriority;->NORMAL:Lcom/miui/webkit_api/WebSettings$RenderPriority;

    goto/32 :goto_d

    nop

    :goto_4
    sget-object v0, Landroid/webkit/WebSettings$RenderPriority;->NORMAL:Landroid/webkit/WebSettings$RenderPriority;

    goto/32 :goto_f

    nop

    :goto_5
    sget-object v0, Landroid/webkit/WebSettings$RenderPriority;->LOW:Landroid/webkit/WebSettings$RenderPriority;

    goto/32 :goto_7

    nop

    :goto_6
    sget-object p1, Lcom/miui/webkit_api/WebSettings$RenderPriority;->LOW:Lcom/miui/webkit_api/WebSettings$RenderPriority;

    goto/32 :goto_9

    nop

    :goto_7
    if-eq p1, v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_6

    nop

    :goto_8
    sget-object p1, Lcom/miui/webkit_api/WebSettings$RenderPriority;->HIGH:Lcom/miui/webkit_api/WebSettings$RenderPriority;

    goto/32 :goto_1

    nop

    :goto_9
    return-object p1

    :goto_a
    goto/32 :goto_c

    nop

    :goto_b
    sget-object v0, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    goto/32 :goto_0

    nop

    :goto_c
    sget-object p1, Lcom/miui/webkit_api/WebSettings$RenderPriority;->NORMAL:Lcom/miui/webkit_api/WebSettings$RenderPriority;

    goto/32 :goto_10

    nop

    :goto_d
    return-object p1

    :goto_e
    goto/32 :goto_b

    nop

    :goto_f
    if-eq p1, v0, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_3

    nop

    :goto_10
    return-object p1
.end method

.method a(Landroid/webkit/WebSettings$TextSize;)Lcom/miui/webkit_api/WebSettings$TextSize;
    .locals 1

    goto/32 :goto_10

    nop

    :goto_0
    return-object p1

    :goto_1
    goto/32 :goto_f

    nop

    :goto_2
    sget-object v0, Landroid/webkit/WebSettings$TextSize;->NORMAL:Landroid/webkit/WebSettings$TextSize;

    goto/32 :goto_6

    nop

    :goto_3
    if-eq p1, v0, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_e

    nop

    :goto_4
    sget-object p1, Lcom/miui/webkit_api/WebSettings$TextSize;->SMALLER:Lcom/miui/webkit_api/WebSettings$TextSize;

    goto/32 :goto_16

    nop

    :goto_5
    sget-object p1, Lcom/miui/webkit_api/WebSettings$TextSize;->SMALLEST:Lcom/miui/webkit_api/WebSettings$TextSize;

    goto/32 :goto_0

    nop

    :goto_6
    if-eq p1, v0, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_a

    nop

    :goto_7
    return-object p1

    :goto_8
    goto/32 :goto_1a

    nop

    :goto_9
    if-eq p1, v0, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_18

    nop

    :goto_a
    sget-object p1, Lcom/miui/webkit_api/WebSettings$TextSize;->NORMAL:Lcom/miui/webkit_api/WebSettings$TextSize;

    goto/32 :goto_11

    nop

    :goto_b
    return-object p1

    :goto_c
    sget-object v0, Landroid/webkit/WebSettings$TextSize;->LARGER:Landroid/webkit/WebSettings$TextSize;

    goto/32 :goto_9

    nop

    :goto_d
    sget-object p1, Lcom/miui/webkit_api/WebSettings$TextSize;->NORMAL:Lcom/miui/webkit_api/WebSettings$TextSize;

    goto/32 :goto_b

    nop

    :goto_e
    sget-object p1, Lcom/miui/webkit_api/WebSettings$TextSize;->LARGEST:Lcom/miui/webkit_api/WebSettings$TextSize;

    goto/32 :goto_13

    nop

    :goto_f
    sget-object v0, Landroid/webkit/WebSettings$TextSize;->SMALLER:Landroid/webkit/WebSettings$TextSize;

    goto/32 :goto_19

    nop

    :goto_10
    sget-object v0, Landroid/webkit/WebSettings$TextSize;->SMALLEST:Landroid/webkit/WebSettings$TextSize;

    goto/32 :goto_15

    nop

    :goto_11
    return-object p1

    :goto_12
    goto/32 :goto_c

    nop

    :goto_13
    return-object p1

    :goto_14
    goto/32 :goto_d

    nop

    :goto_15
    if-eq p1, v0, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_5

    nop

    :goto_16
    return-object p1

    :goto_17
    goto/32 :goto_2

    nop

    :goto_18
    sget-object p1, Lcom/miui/webkit_api/WebSettings$TextSize;->LARGER:Lcom/miui/webkit_api/WebSettings$TextSize;

    goto/32 :goto_7

    nop

    :goto_19
    if-eq p1, v0, :cond_4

    goto/32 :goto_17

    :cond_4
    goto/32 :goto_4

    nop

    :goto_1a
    sget-object v0, Landroid/webkit/WebSettings$TextSize;->LARGEST:Landroid/webkit/WebSettings$TextSize;

    goto/32 :goto_3

    nop
.end method

.method a(Landroid/webkit/WebSettings$ZoomDensity;)Lcom/miui/webkit_api/WebSettings$ZoomDensity;
    .locals 1

    goto/32 :goto_f

    nop

    :goto_0
    return-object p1

    :goto_1
    goto/32 :goto_7

    nop

    :goto_2
    return-object p1

    :goto_3
    goto/32 :goto_9

    nop

    :goto_4
    sget-object p1, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->FAR:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    goto/32 :goto_0

    nop

    :goto_5
    sget-object p1, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->MEDIUM:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    goto/32 :goto_a

    nop

    :goto_6
    if-eq p1, v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_d

    nop

    :goto_7
    sget-object v0, Landroid/webkit/WebSettings$ZoomDensity;->MEDIUM:Landroid/webkit/WebSettings$ZoomDensity;

    goto/32 :goto_10

    nop

    :goto_8
    sget-object p1, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->MEDIUM:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    goto/32 :goto_2

    nop

    :goto_9
    sget-object v0, Landroid/webkit/WebSettings$ZoomDensity;->CLOSE:Landroid/webkit/WebSettings$ZoomDensity;

    goto/32 :goto_6

    nop

    :goto_a
    return-object p1

    :goto_b
    return-object p1

    :goto_c
    goto/32 :goto_5

    nop

    :goto_d
    sget-object p1, Lcom/miui/webkit_api/WebSettings$ZoomDensity;->CLOSE:Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    goto/32 :goto_b

    nop

    :goto_e
    if-eq p1, v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_4

    nop

    :goto_f
    sget-object v0, Landroid/webkit/WebSettings$ZoomDensity;->FAR:Landroid/webkit/WebSettings$ZoomDensity;

    goto/32 :goto_e

    nop

    :goto_10
    if-eq p1, v0, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_8

    nop
.end method

.method public enableSmoothTransition()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->enableSmoothTransition()Z

    move-result v0

    return v0
.end method

.method public getAllowContentAccess()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getAllowContentAccess()Z

    move-result v0

    return v0
.end method

.method public getAllowFileAccess()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getAllowFileAccess()Z

    move-result v0

    return v0
.end method

.method public getAllowFileAccessFromFileURLs()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getAllowFileAccessFromFileURLs()Z

    move-result v0

    return v0
.end method

.method public getAllowUniversalAccessFromFileURLs()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getAllowUniversalAccessFromFileURLs()Z

    move-result v0

    return v0
.end method

.method public getBlockNetworkImage()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getBlockNetworkImage()Z

    move-result v0

    return v0
.end method

.method public getBlockNetworkLoads()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getBlockNetworkLoads()Z

    move-result v0

    return v0
.end method

.method public getBuiltInZoomControls()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getBuiltInZoomControls()Z

    move-result v0

    return v0
.end method

.method public getCacheMode()I
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getCacheMode()I

    move-result v0

    return v0
.end method

.method public getCursiveFontFamily()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getCursiveFontFamily()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDatabaseEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getDatabaseEnabled()Z

    move-result v0

    return v0
.end method

.method public getDatabasePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getDatabasePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultFixedFontSize()I
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getDefaultFixedFontSize()I

    move-result v0

    return v0
.end method

.method public getDefaultFontSize()I
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getDefaultFontSize()I

    move-result v0

    return v0
.end method

.method public getDefaultTextEncodingName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getDefaultTextEncodingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultZoom()Lcom/miui/webkit_api/WebSettings$ZoomDensity;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getDefaultZoom()Landroid/webkit/WebSettings$ZoomDensity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getDefaultZoom()Landroid/webkit/WebSettings$ZoomDensity;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/miui/webkit_api/c/z;->a(Landroid/webkit/WebSettings$ZoomDensity;)Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getDisabledActionModeMenuItems()I
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getDisabledActionModeMenuItems()I

    move-result v0

    return v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "method getDisabledActionModeMenuItems() was added in API level 24, current android version is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", so will return WebSettings.MENU_ITEM_NONE."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SystemWebSettings"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public getDisplayZoomControls()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getDisplayZoomControls()Z

    move-result v0

    return v0
.end method

.method public getDomStorageEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getDomStorageEnabled()Z

    move-result v0

    return v0
.end method

.method public getFantasyFontFamily()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getFantasyFontFamily()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFixedFontFamily()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getFixedFontFamily()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJavaScriptCanOpenWindowsAutomatically()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getJavaScriptCanOpenWindowsAutomatically()Z

    move-result v0

    return v0
.end method

.method public getJavaScriptEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getJavaScriptEnabled()Z

    move-result v0

    return v0
.end method

.method public getLayoutAlgorithm()Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getLayoutAlgorithm()Landroid/webkit/WebSettings$LayoutAlgorithm;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/miui/webkit_api/c/z;->a(Landroid/webkit/WebSettings$LayoutAlgorithm;)Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getLightTouchEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getLightTouchEnabled()Z

    move-result v0

    return v0
.end method

.method public getLoadWithOverviewMode()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getLoadWithOverviewMode()Z

    move-result v0

    return v0
.end method

.method public getLoadsImagesAutomatically()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getLoadsImagesAutomatically()Z

    move-result v0

    return v0
.end method

.method public getMediaPlaybackRequiresUserGesture()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getMediaPlaybackRequiresUserGesture()Z

    move-result v0

    return v0
.end method

.method public getMinimumFontSize()I
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getMinimumFontSize()I

    move-result v0

    return v0
.end method

.method public getMinimumLogicalFontSize()I
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getMinimumLogicalFontSize()I

    move-result v0

    return v0
.end method

.method public getMixedContentMode()I
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getMixedContentMode()I

    move-result v0

    return v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "method getMixedContentMode() was added in API level 21, current android version is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", so will return WebSettings.MIXED_CONTENT_ALWAYS_ALLOW."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SystemWebSettings"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public getOffscreenPreRaster()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getOffscreenPreRaster()Z

    move-result v0

    return v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "method getOffscreenPreRaster() was added in API level 23, current android version is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", so will return false."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SystemWebSettings"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public getPluginState()Lcom/miui/webkit_api/WebSettings$PluginState;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getPluginState()Landroid/webkit/WebSettings$PluginState;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getPluginState()Landroid/webkit/WebSettings$PluginState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/miui/webkit_api/c/z;->a(Landroid/webkit/WebSettings$PluginState;)Lcom/miui/webkit_api/WebSettings$PluginState;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getSansSerifFontFamily()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getSansSerifFontFamily()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSaveFormData()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getSaveFormData()Z

    move-result v0

    return v0
.end method

.method public getSavePassword()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getSavePassword()Z

    move-result v0

    return v0
.end method

.method public getSerifFontFamily()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getSerifFontFamily()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStandardFontFamily()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getStandardFontFamily()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getTextSize()Lcom/miui/webkit_api/WebSettings$TextSize;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getTextSize()Landroid/webkit/WebSettings$TextSize;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getTextSize()Landroid/webkit/WebSettings$TextSize;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/miui/webkit_api/c/z;->a(Landroid/webkit/WebSettings$TextSize;)Lcom/miui/webkit_api/WebSettings$TextSize;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTextZoom()I
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getTextZoom()I

    move-result v0

    return v0
.end method

.method public getUseWideViewPort()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUseWideViewPort()Z

    move-result v0

    return v0
.end method

.method public getUserAgentString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAcceptThirdPartyCookies(Z)V
    .locals 0

    return-void
.end method

.method public setAllowContentAccess(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    return-void
.end method

.method public setAllowFileAccess(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    return-void
.end method

.method public setAllowFileAccessFromFileURLs(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    return-void
.end method

.method public setAllowUniversalAccessFromFileURLs(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    return-void
.end method

.method public setAppCacheEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    return-void
.end method

.method public setAppCacheMaxSize(J)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebSettings;->setAppCacheMaxSize(J)V

    return-void
.end method

.method public setAppCachePath(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    return-void
.end method

.method public setBlockNetworkImage(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    return-void
.end method

.method public setBlockNetworkLoads(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setBlockNetworkLoads(Z)V

    return-void
.end method

.method public setBuiltInZoomControls(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    return-void
.end method

.method public setCacheMode(I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    return-void
.end method

.method public setCursiveFontFamily(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setCursiveFontFamily(Ljava/lang/String;)V

    return-void
.end method

.method public setDatabaseEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    return-void
.end method

.method public setDatabasePath(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setDatabasePath(Ljava/lang/String;)V

    return-void
.end method

.method public setDefaultFixedFontSize(I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setDefaultFixedFontSize(I)V

    return-void
.end method

.method public setDefaultFontSize(I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setDefaultFontSize(I)V

    return-void
.end method

.method public setDefaultTextEncodingName(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    return-void
.end method

.method public setDefaultZoom(Lcom/miui/webkit_api/WebSettings$ZoomDensity;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/webkit_api/c/z;->a(Lcom/miui/webkit_api/WebSettings$ZoomDensity;)Landroid/webkit/WebSettings$ZoomDensity;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setDefaultZoom(Landroid/webkit/WebSettings$ZoomDensity;)V

    return-void
.end method

.method public setDisabledActionModeMenuItems(I)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setDisabledActionModeMenuItems(I)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "method setDisabledActionModeMenuItems(int menuItems) was added in API level 24, current android version is "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", so will do nothing."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SystemWebSettings"

    invoke-static {v0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public setDisplayZoomControls(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    return-void
.end method

.method public setDomStorageEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    return-void
.end method

.method public setEnableSmoothTransition(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setEnableSmoothTransition(Z)V

    return-void
.end method

.method public setFantasyFontFamily(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setFantasyFontFamily(Ljava/lang/String;)V

    return-void
.end method

.method public setFixedFontFamily(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setFixedFontFamily(Ljava/lang/String;)V

    return-void
.end method

.method public setGeolocationDatabasePath(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setGeolocationDatabasePath(Ljava/lang/String;)V

    return-void
.end method

.method public setGeolocationEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setGeolocationEnabled(Z)V

    return-void
.end method

.method public setJavaScriptCanOpenWindowsAutomatically(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    return-void
.end method

.method public setJavaScriptEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    return-void
.end method

.method public setLayoutAlgorithm(Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/webkit_api/c/z;->a(Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;)Landroid/webkit/WebSettings$LayoutAlgorithm;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    return-void
.end method

.method public setLightTouchEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setLightTouchEnabled(Z)V

    return-void
.end method

.method public setLoadWithOverviewMode(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    return-void
.end method

.method public setLoadsImagesAutomatically(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    return-void
.end method

.method public setMediaPlaybackRequiresUserGesture(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setMediaPlaybackRequiresUserGesture(Z)V

    return-void
.end method

.method public setMinimumFontSize(I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setMinimumFontSize(I)V

    return-void
.end method

.method public setMinimumLogicalFontSize(I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setMinimumLogicalFontSize(I)V

    return-void
.end method

.method public setMixedContentMode(I)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setMixedContentMode(I)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "method setMixedContentMode(int mode) was added in API level 21, current android version is "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", so will do nothing."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SystemWebSettings"

    invoke-static {v0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public setNeedInitialFocus(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setNeedInitialFocus(Z)V

    return-void
.end method

.method public setOffscreenPreRaster(Z)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setOffscreenPreRaster(Z)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "method setOffscreenPreRaster(boolean enabled) was added in API level 23, current android version is "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", so will do nothing."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SystemWebSettings"

    invoke-static {v0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public setPluginState(Lcom/miui/webkit_api/WebSettings$PluginState;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/webkit_api/c/z;->a(Lcom/miui/webkit_api/WebSettings$PluginState;)Landroid/webkit/WebSettings$PluginState;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    return-void
.end method

.method public setRenderPriority(Lcom/miui/webkit_api/WebSettings$RenderPriority;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/webkit_api/c/z;->a(Lcom/miui/webkit_api/WebSettings$RenderPriority;)Landroid/webkit/WebSettings$RenderPriority;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    return-void
.end method

.method public setSansSerifFontFamily(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setSansSerifFontFamily(Ljava/lang/String;)V

    return-void
.end method

.method public setSaveFormData(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    return-void
.end method

.method public setSavePassword(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    return-void
.end method

.method public setSerifFontFamily(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setSerifFontFamily(Ljava/lang/String;)V

    return-void
.end method

.method public setStandardFontFamily(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setStandardFontFamily(Ljava/lang/String;)V

    return-void
.end method

.method public setSupportMultipleWindows(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    return-void
.end method

.method public setSupportZoom(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    return-void
.end method

.method public declared-synchronized setTextSize(Lcom/miui/webkit_api/WebSettings$TextSize;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/webkit_api/c/z;->a(Lcom/miui/webkit_api/WebSettings$TextSize;)Landroid/webkit/WebSettings$TextSize;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setTextSize(Landroid/webkit/WebSettings$TextSize;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setTextZoom(I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    return-void
.end method

.method public setUseWideViewPort(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    return-void
.end method

.method public setUserAgentString(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    return-void
.end method

.method public supportMultipleWindows()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->supportMultipleWindows()Z

    move-result v0

    return v0
.end method

.method public supportZoom()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/z;->b:Landroid/webkit/WebSettings;

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->supportZoom()Z

    move-result v0

    return v0
.end method
