.class public Lcom/miui/webkit_api/a/aa;
.super Lcom/miui/webkit_api/WebStorage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/aa$a;,
        Lcom/miui/webkit_api/a/aa$b;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webkit.WebStorage"

.field private static d:Lcom/miui/webkit_api/WebStorage;


# instance fields
.field private b:Lcom/miui/webkit_api/a/aa$b;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebStorage;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/aa;->c:Ljava/lang/Object;

    return-void
.end method

.method public static a()Lcom/miui/webkit_api/WebStorage;
    .locals 2

    sget-object v0, Lcom/miui/webkit_api/a/aa;->d:Lcom/miui/webkit_api/WebStorage;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/miui/webkit_api/a/aa$b;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/miui/webkit_api/a/aa;

    invoke-direct {v1, v0}, Lcom/miui/webkit_api/a/aa;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/miui/webkit_api/a/aa;->d:Lcom/miui/webkit_api/WebStorage;

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/a/aa;->d:Lcom/miui/webkit_api/WebStorage;

    return-object v0
.end method

.method private b()Lcom/miui/webkit_api/a/aa$b;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/aa;->b:Lcom/miui/webkit_api/a/aa$b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/aa$b;

    iget-object v1, p0, Lcom/miui/webkit_api/a/aa;->c:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/aa$b;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/aa;->b:Lcom/miui/webkit_api/a/aa$b;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/aa;->b:Lcom/miui/webkit_api/a/aa$b;

    return-object v0
.end method


# virtual methods
.method public deleteAllData()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/aa;->b()Lcom/miui/webkit_api/a/aa$b;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/aa;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/aa$b;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public deleteOrigin(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/aa;->b()Lcom/miui/webkit_api/a/aa$b;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/aa;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/aa$b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public getOrigins(Lcom/miui/webkit_api/ValueCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/webkit_api/ValueCallback<",
            "Ljava/util/Map;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/miui/webkit_api/a/aa;->b()Lcom/miui/webkit_api/a/aa$b;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/aa;->c:Ljava/lang/Object;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/miui/webkit_api/a/ak;->h(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/aa$b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public getQuotaForOrigin(Ljava/lang/String;Lcom/miui/webkit_api/ValueCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/miui/webkit_api/ValueCallback<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/miui/webkit_api/a/aa;->b()Lcom/miui/webkit_api/a/aa$b;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/aa;->c:Ljava/lang/Object;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lcom/miui/webkit_api/a/ak;->h(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    :goto_0
    invoke-virtual {v0, v1, p1, p2}, Lcom/miui/webkit_api/a/aa$b;->b(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public getUsageForOrigin(Ljava/lang/String;Lcom/miui/webkit_api/ValueCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/miui/webkit_api/ValueCallback<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/miui/webkit_api/a/aa;->b()Lcom/miui/webkit_api/a/aa$b;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/aa;->c:Ljava/lang/Object;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lcom/miui/webkit_api/a/ak;->h(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    :goto_0
    invoke-virtual {v0, v1, p1, p2}, Lcom/miui/webkit_api/a/aa$b;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setQuotaForOrigin(Ljava/lang/String;J)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/aa;->b()Lcom/miui/webkit_api/a/aa$b;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/aa;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/miui/webkit_api/a/aa$b;->a(Ljava/lang/Object;Ljava/lang/String;J)V

    return-void
.end method
