.class Lcom/miui/webkit_api/c/m;
.super Landroid/webkit/ServiceWorkerClient;


# instance fields
.field private a:Lcom/miui/webkit_api/ServiceWorkerClient;


# direct methods
.method constructor <init>(Lcom/miui/webkit_api/ServiceWorkerClient;)V
    .locals 0

    invoke-direct {p0}, Landroid/webkit/ServiceWorkerClient;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/m;->a:Lcom/miui/webkit_api/ServiceWorkerClient;

    return-void
.end method


# virtual methods
.method public shouldInterceptRequest(Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;
    .locals 3

    iget-object v0, p0, Lcom/miui/webkit_api/c/m;->a:Lcom/miui/webkit_api/ServiceWorkerClient;

    const/4 v1, 0x0

    if-nez p1, :cond_0

    move-object v2, v1

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/miui/webkit_api/c/x;

    invoke-direct {v2, p1}, Lcom/miui/webkit_api/c/x;-><init>(Landroid/webkit/WebResourceRequest;)V

    :goto_0
    invoke-virtual {v0, v2}, Lcom/miui/webkit_api/ServiceWorkerClient;->shouldInterceptRequest(Lcom/miui/webkit_api/WebResourceRequest;)Lcom/miui/webkit_api/WebResourceResponse;

    move-result-object p1

    if-nez p1, :cond_1

    return-object v1

    :cond_1
    invoke-virtual {p1}, Lcom/miui/webkit_api/WebResourceResponse;->getObject()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/webkit/WebResourceResponse;

    return-object p1
.end method
