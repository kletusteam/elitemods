.class Lcom/miui/webkit_api/c/h;
.super Lcom/miui/webkit_api/HttpAuthHandler;


# instance fields
.field private a:Landroid/webkit/HttpAuthHandler;


# direct methods
.method constructor <init>(Landroid/webkit/HttpAuthHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/HttpAuthHandler;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/h;->a:Landroid/webkit/HttpAuthHandler;

    return-void
.end method


# virtual methods
.method a()Landroid/webkit/HttpAuthHandler;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/miui/webkit_api/c/h;->a:Landroid/webkit/HttpAuthHandler;

    goto/32 :goto_0

    nop
.end method

.method public cancel()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/h;->a:Landroid/webkit/HttpAuthHandler;

    invoke-virtual {v0}, Landroid/webkit/HttpAuthHandler;->cancel()V

    return-void
.end method

.method public proceed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/h;->a:Landroid/webkit/HttpAuthHandler;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/HttpAuthHandler;->proceed(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public useHttpAuthUsernamePassword()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/h;->a:Landroid/webkit/HttpAuthHandler;

    invoke-virtual {v0}, Landroid/webkit/HttpAuthHandler;->useHttpAuthUsernamePassword()Z

    move-result v0

    return v0
.end method
