.class public Lcom/miui/webkit_api/a/j;
.super Lcom/miui/webkit_api/MimeTypeMap;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/j$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webkit.MimeTypeMap"

.field private static d:Lcom/miui/webkit_api/a/j;


# instance fields
.field private b:Lcom/miui/webkit_api/a/j$a;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/MimeTypeMap;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/j;->c:Ljava/lang/Object;

    return-void
.end method

.method public static a()Lcom/miui/webkit_api/a/j;
    .locals 2

    sget-object v0, Lcom/miui/webkit_api/a/j;->d:Lcom/miui/webkit_api/a/j;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/miui/webkit_api/a/j$a;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/j;

    invoke-static {}, Lcom/miui/webkit_api/a/j$a;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/j;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/miui/webkit_api/a/j;->d:Lcom/miui/webkit_api/a/j;

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/a/j;->d:Lcom/miui/webkit_api/a/j;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/miui/webkit_api/a/j$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private b()Lcom/miui/webkit_api/a/j$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/j;->b:Lcom/miui/webkit_api/a/j$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/j$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/j;->c:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/j$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/j;->b:Lcom/miui/webkit_api/a/j$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/j;->b:Lcom/miui/webkit_api/a/j$a;

    return-object v0
.end method


# virtual methods
.method public getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/j;->b()Lcom/miui/webkit_api/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/j;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/j$a;->d(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/j;->b()Lcom/miui/webkit_api/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/j;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/j$a;->b(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public hasExtension(Ljava/lang/String;)Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/j;->b()Lcom/miui/webkit_api/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/j;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/j$a;->c(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public hasMimeType(Ljava/lang/String;)Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/j;->b()Lcom/miui/webkit_api/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/j;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/j$a;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
