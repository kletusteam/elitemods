.class public Lcom/miui/webkit_api/c/ad;
.super Lcom/miui/webkit_api/WebViewDatabase;


# instance fields
.field private a:Landroid/webkit/WebViewDatabase;


# direct methods
.method public constructor <init>(Landroid/webkit/WebViewDatabase;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebViewDatabase;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/ad;->a:Landroid/webkit/WebViewDatabase;

    return-void
.end method


# virtual methods
.method public clearFormData()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/ad;->a:Landroid/webkit/WebViewDatabase;

    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->clearFormData()V

    return-void
.end method

.method public clearHttpAuthUsernamePassword()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/ad;->a:Landroid/webkit/WebViewDatabase;

    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->clearHttpAuthUsernamePassword()V

    return-void
.end method

.method public clearUsernamePassword()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/ad;->a:Landroid/webkit/WebViewDatabase;

    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->clearUsernamePassword()V

    return-void
.end method

.method public hasFormData()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/ad;->a:Landroid/webkit/WebViewDatabase;

    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->hasFormData()Z

    move-result v0

    return v0
.end method

.method public hasHttpAuthUsernamePassword()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/ad;->a:Landroid/webkit/WebViewDatabase;

    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->hasHttpAuthUsernamePassword()Z

    move-result v0

    return v0
.end method

.method public hasUsernamePassword()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/ad;->a:Landroid/webkit/WebViewDatabase;

    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->hasUsernamePassword()Z

    move-result v0

    return v0
.end method
