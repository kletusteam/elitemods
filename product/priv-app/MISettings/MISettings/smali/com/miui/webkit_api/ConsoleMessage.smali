.class public Lcom/miui/webkit_api/ConsoleMessage;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/miui/webkit_api/ConsoleMessage$MessageLevel;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/ConsoleMessage;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/miui/webkit_api/ConsoleMessage;->c:Ljava/lang/String;

    iput p3, p0, Lcom/miui/webkit_api/ConsoleMessage;->d:I

    iput-object p4, p0, Lcom/miui/webkit_api/ConsoleMessage;->a:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    return-void
.end method


# virtual methods
.method public lineNumber()I
    .locals 1

    iget v0, p0, Lcom/miui/webkit_api/ConsoleMessage;->d:I

    return v0
.end method

.method public message()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/ConsoleMessage;->b:Ljava/lang/String;

    return-object v0
.end method

.method public messageLevel()Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/ConsoleMessage;->a:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    return-object v0
.end method

.method public sourceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/ConsoleMessage;->c:Ljava/lang/String;

    return-object v0
.end method
