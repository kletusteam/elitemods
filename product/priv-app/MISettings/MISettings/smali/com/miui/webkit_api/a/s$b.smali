.class public Lcom/miui/webkit_api/a/s$b;
.super Lcom/miui/webkit_api/WebChromeClient$FileChooserParams;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/webkit_api/a/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/s$b$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webkit.WebChromeClient$FileChooserParams"


# instance fields
.field private b:Lcom/miui/webkit_api/a/s$b$a;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebChromeClient$FileChooserParams;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/s$b;->c:Ljava/lang/Object;

    return-void
.end method

.method public static a(ILandroid/content/Intent;)[Landroid/net/Uri;
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/webkit_api/a/s$b$a;->a(ILandroid/content/Intent;)[Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method private b()Lcom/miui/webkit_api/a/s$b$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/s$b;->b:Lcom/miui/webkit_api/a/s$b$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/s$b$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/s$b;->c:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/s$b$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/s$b;->b:Lcom/miui/webkit_api/a/s$b$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/s$b;->b:Lcom/miui/webkit_api/a/s$b$a;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/miui/webkit_api/a/s$b;->c:Ljava/lang/Object;

    goto/32 :goto_0

    nop
.end method

.method public createIntent()Landroid/content/Intent;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/s$b;->b()Lcom/miui/webkit_api/a/s$b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/s$b;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/s$b$a;->f(Ljava/lang/Object;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getAcceptTypes()[Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/s$b;->b()Lcom/miui/webkit_api/a/s$b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/s$b;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/s$b$a;->b(Ljava/lang/Object;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFilenameHint()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/s$b;->b()Lcom/miui/webkit_api/a/s$b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/s$b;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/s$b$a;->e(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMode()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/s$b;->b()Lcom/miui/webkit_api/a/s$b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/s$b;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/s$b$a;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/s$b;->b()Lcom/miui/webkit_api/a/s$b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/s$b;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/s$b$a;->d(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public isCaptureEnabled()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/s$b;->b()Lcom/miui/webkit_api/a/s$b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/s$b;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/s$b$a;->c(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
