.class Lcom/miui/webkit_api/a/ai;
.super Lcom/miui/webkit_api/MiuiGlobalSettings;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/ai$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webview.MiuiGlobalSettings"

.field private static final b:Ljava/lang/String; = "MiuiGlobalSettingsImpl"


# instance fields
.field private c:Lcom/miui/webkit_api/a/ai$a;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/MiuiGlobalSettings;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/ai;->d:Ljava/lang/Object;

    return-void
.end method

.method static a()Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreIntVersion()I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const v2, 0x10003

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSupportSetAdBlockEnabled(), catch exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiGlobalSettingsImpl"

    invoke-static {v2, v1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method private b()Lcom/miui/webkit_api/a/ai$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/ai;->c:Lcom/miui/webkit_api/a/ai$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/ai$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/ai;->d:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/ai$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/ai;->c:Lcom/miui/webkit_api/a/ai$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/ai;->c:Lcom/miui/webkit_api/a/ai$a;

    return-object v0
.end method


# virtual methods
.method public getLoadsImagesOnDemand()Z
    .locals 3

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/ai;->b()Lcom/miui/webkit_api/a/ai$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/ai;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/ai$a;->a(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLoadsImagesOnDemand() catch exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiGlobalSettingsImpl"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public setAdBlockEnabled(Landroid/content/Context;Z)V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/miui/webkit_api/a/ai;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/miui/webkit_api/a/ai;->b()Lcom/miui/webkit_api/a/ai$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/ai;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, Lcom/miui/webkit_api/a/ai$a;->a(Ljava/lang/Object;Landroid/content/Context;Z)V

    goto :goto_0

    :cond_0
    const-string p1, "MiuiGlobalSettingsImpl"

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "current browser apk is not support setAdBlockEnabled, current version is "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreIntVersion()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", expected version is 0x00010004"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "current browser apk is not support setAdBlockEnabled, catch exception: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "MiuiGlobalSettings"

    invoke-static {p2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setErrorPageConfig(ZZLjava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/ai;->b()Lcom/miui/webkit_api/a/ai$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/ai;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/miui/webkit_api/a/ai$a;->a(Ljava/lang/Object;ZZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "setErrorPageConfig(replace, subframe, override), catch exception: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "MiuiGlobalSettingsImpl"

    invoke-static {p2, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public setLoadsImagesOnDemand(Z)V
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/ai;->b()Lcom/miui/webkit_api/a/ai$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/ai;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/ai$a;->a(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setLoadsImagesOnDemand(enabled) catch exception: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "MiuiGlobalSettingsImpl"

    invoke-static {v0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public setWebGLNoValidateEnabled(Z)V
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/ai;->b()Lcom/miui/webkit_api/a/ai$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/ai;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/ai$a;->b(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setWebGLNoValidateEnabled(enabled) catch exception: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "MiuiGlobalSettingsImpl"

    invoke-static {v0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
