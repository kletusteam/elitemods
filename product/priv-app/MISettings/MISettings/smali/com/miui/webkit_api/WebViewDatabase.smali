.class public abstract Lcom/miui/webkit_api/WebViewDatabase;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/miui/webkit_api/WebViewDatabase;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/miui/webkit_api/WebViewDatabase;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/WebViewDatabase;->a:Lcom/miui/webkit_api/WebViewDatabase;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/miui/webkit_api/b/g;->d(Landroid/content/Context;)Lcom/miui/webkit_api/WebViewDatabase;

    move-result-object p0

    sput-object p0, Lcom/miui/webkit_api/WebViewDatabase;->a:Lcom/miui/webkit_api/WebViewDatabase;

    :cond_0
    sget-object p0, Lcom/miui/webkit_api/WebViewDatabase;->a:Lcom/miui/webkit_api/WebViewDatabase;

    return-object p0
.end method


# virtual methods
.method public abstract clearFormData()V
.end method

.method public abstract clearHttpAuthUsernamePassword()V
.end method

.method public abstract clearUsernamePassword()V
.end method

.method public abstract hasFormData()Z
.end method

.method public abstract hasHttpAuthUsernamePassword()Z
.end method

.method public abstract hasUsernamePassword()Z
.end method
