.class public Lcom/miui/webkit_api/c/d;
.super Lcom/miui/webkit_api/CookieSyncManager;


# instance fields
.field private a:Landroid/webkit/CookieSyncManager;


# direct methods
.method public constructor <init>(Landroid/webkit/CookieSyncManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/CookieSyncManager;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/d;->a:Landroid/webkit/CookieSyncManager;

    return-void
.end method


# virtual methods
.method public resetSync()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/d;->a:Landroid/webkit/CookieSyncManager;

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->resetSync()V

    return-void
.end method

.method public run()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/d;->a:Landroid/webkit/CookieSyncManager;

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->run()V

    return-void
.end method

.method public startSync()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/d;->a:Landroid/webkit/CookieSyncManager;

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->startSync()V

    return-void
.end method

.method public stopSync()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/d;->a:Landroid/webkit/CookieSyncManager;

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->stopSync()V

    return-void
.end method

.method public sync()V
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/c/d;->a:Landroid/webkit/CookieSyncManager;

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    return-void
.end method
