.class public Lcom/miui/webkit_api/a/z;
.super Lcom/miui/webkit_api/WebSettings;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/z$b;,
        Lcom/miui/webkit_api/a/z$d;,
        Lcom/miui/webkit_api/a/z$f;,
        Lcom/miui/webkit_api/a/z$e;,
        Lcom/miui/webkit_api/a/z$a;,
        Lcom/miui/webkit_api/a/z$c;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webkit.WebSettings"

.field static b:Lcom/miui/webkit_api/a/z$a; = null

.field static c:Lcom/miui/webkit_api/a/z$e; = null

.field static d:Lcom/miui/webkit_api/a/z$f; = null

.field static e:Lcom/miui/webkit_api/a/z$d; = null

.field static f:Lcom/miui/webkit_api/a/z$b; = null

.field private static final g:Ljava/lang/String; = "BrowserWebSettings"


# instance fields
.field private h:Lcom/miui/webkit_api/a/z$c;

.field private i:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebSettings;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    return-void
.end method

.method static a()Lcom/miui/webkit_api/a/z$a;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/a/z;->b:Lcom/miui/webkit_api/a/z$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/z$a;

    invoke-direct {v0}, Lcom/miui/webkit_api/a/z$a;-><init>()V

    sput-object v0, Lcom/miui/webkit_api/a/z;->b:Lcom/miui/webkit_api/a/z$a;

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/a/z;->b:Lcom/miui/webkit_api/a/z$a;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/miui/webkit_api/a/z$c;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    invoke-static {}, Lcom/miui/webkit_api/a/z;->a()Lcom/miui/webkit_api/a/z$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/webkit_api/a/z$a;->a()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method static c()Lcom/miui/webkit_api/a/z$e;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/a/z;->c:Lcom/miui/webkit_api/a/z$e;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/z$e;

    invoke-direct {v0}, Lcom/miui/webkit_api/a/z$e;-><init>()V

    sput-object v0, Lcom/miui/webkit_api/a/z;->c:Lcom/miui/webkit_api/a/z$e;

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/a/z;->c:Lcom/miui/webkit_api/a/z$e;

    return-object v0
.end method

.method static d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    invoke-static {}, Lcom/miui/webkit_api/a/z;->c()Lcom/miui/webkit_api/a/z$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/webkit_api/a/z$e;->a()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method static e()Lcom/miui/webkit_api/a/z$f;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/a/z;->d:Lcom/miui/webkit_api/a/z$f;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/z$f;

    invoke-direct {v0}, Lcom/miui/webkit_api/a/z$f;-><init>()V

    sput-object v0, Lcom/miui/webkit_api/a/z;->d:Lcom/miui/webkit_api/a/z$f;

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/a/z;->d:Lcom/miui/webkit_api/a/z$f;

    return-object v0
.end method

.method static f()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    invoke-static {}, Lcom/miui/webkit_api/a/z;->e()Lcom/miui/webkit_api/a/z$f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/webkit_api/a/z$f;->a()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method static g()Lcom/miui/webkit_api/a/z$d;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/a/z;->e:Lcom/miui/webkit_api/a/z$d;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/z$d;

    invoke-direct {v0}, Lcom/miui/webkit_api/a/z$d;-><init>()V

    sput-object v0, Lcom/miui/webkit_api/a/z;->e:Lcom/miui/webkit_api/a/z$d;

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/a/z;->e:Lcom/miui/webkit_api/a/z$d;

    return-object v0
.end method

.method static h()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    invoke-static {}, Lcom/miui/webkit_api/a/z;->g()Lcom/miui/webkit_api/a/z$d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/webkit_api/a/z$d;->a()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method static i()Lcom/miui/webkit_api/a/z$b;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/a/z;->f:Lcom/miui/webkit_api/a/z$b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/z$b;

    invoke-direct {v0}, Lcom/miui/webkit_api/a/z$b;-><init>()V

    sput-object v0, Lcom/miui/webkit_api/a/z;->f:Lcom/miui/webkit_api/a/z$b;

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/a/z;->f:Lcom/miui/webkit_api/a/z$b;

    return-object v0
.end method

.method static j()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    invoke-static {}, Lcom/miui/webkit_api/a/z;->i()Lcom/miui/webkit_api/a/z$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/webkit_api/a/z$b;->a()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method private k()Lcom/miui/webkit_api/a/z$c;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/z;->h:Lcom/miui/webkit_api/a/z$c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/z$c;

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/z$c;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/z;->h:Lcom/miui/webkit_api/a/z$c;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/z;->h:Lcom/miui/webkit_api/a/z$c;

    return-object v0
.end method


# virtual methods
.method public enableSmoothTransition()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->h(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getAllowContentAccess()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getAllowFileAccess()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->e(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getAllowFileAccessFromFileURLs()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->J(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getAllowUniversalAccessFromFileURLs()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->I(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getBlockNetworkImage()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->C(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getBlockNetworkLoads()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->D(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getBuiltInZoomControls()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->c(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getCacheMode()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->O(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getCursiveFontFamily()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->v(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDatabaseEnabled()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->G(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getDatabasePath()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->F(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultFixedFontSize()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->A(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getDefaultFontSize()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->z(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getDefaultTextEncodingName()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->M(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultZoom()Lcom/miui/webkit_api/WebSettings$ZoomDensity;
    .locals 3

    invoke-static {}, Lcom/miui/webkit_api/a/z;->e()Lcom/miui/webkit_api/a/z$f;

    move-result-object v0

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/miui/webkit_api/a/z$c;->m(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$f;->a(Ljava/lang/Object;)Lcom/miui/webkit_api/WebSettings$ZoomDensity;

    move-result-object v0

    return-object v0
.end method

.method public getDisabledActionModeMenuItems()I
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->R(Ljava/lang/Object;)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "current browser apk is not support setDisabledActionModeMenuItems(int menuItems), current version is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreIntVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", expected version is 0x00010006, so will return WebSettings.MENU_ITEM_NONE."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrowserWebSettings"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public getDisplayZoomControls()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->d(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getDomStorageEnabled()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->E(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getFantasyFontFamily()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->w(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFixedFontFamily()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJavaScriptCanOpenWindowsAutomatically()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->L(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getJavaScriptEnabled()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->H(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getLayoutAlgorithm()Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;
    .locals 3

    invoke-static {}, Lcom/miui/webkit_api/a/z;->a()Lcom/miui/webkit_api/a/z$a;

    move-result-object v0

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/miui/webkit_api/a/z$c;->q(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$a;->a(Ljava/lang/Object;)Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    move-result-object v0

    return-object v0
.end method

.method public getLightTouchEnabled()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->n(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getLoadWithOverviewMode()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getLoadsImagesAutomatically()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->B(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getMediaPlaybackRequiresUserGesture()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getMinimumFontSize()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->x(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getMinimumLogicalFontSize()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->y(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getMixedContentMode()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->P(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getOffscreenPreRaster()Z
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->Q(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "current browser apk is not support getOffscreenPreRaster(), current version is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreIntVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", expected version is 0x00010006, so will return false."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrowserWebSettings"

    invoke-static {v1, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public getPluginState()Lcom/miui/webkit_api/WebSettings$PluginState;
    .locals 3

    invoke-static {}, Lcom/miui/webkit_api/a/z;->i()Lcom/miui/webkit_api/a/z$b;

    move-result-object v0

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/miui/webkit_api/a/z$c;->K(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$b;->a(Ljava/lang/Object;)Lcom/miui/webkit_api/WebSettings$PluginState;

    move-result-object v0

    return-object v0
.end method

.method public getSansSerifFontFamily()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->t(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSaveFormData()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->i(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getSavePassword()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->j(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getSerifFontFamily()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->u(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStandardFontFamily()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->r(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getTextSize()Lcom/miui/webkit_api/WebSettings$TextSize;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/miui/webkit_api/a/z;->c()Lcom/miui/webkit_api/a/z$e;

    move-result-object v0

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/miui/webkit_api/a/z$c;->l(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$e;->a(Ljava/lang/Object;)Lcom/miui/webkit_api/WebSettings$TextSize;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTextZoom()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->k(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getUseWideViewPort()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->o(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getUserAgentString()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->N(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAcceptThirdPartyCookies(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->k(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setAllowContentAccess(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->f(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setAllowFileAccess(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->e(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setAllowFileAccessFromFileURLs(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->t(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setAllowUniversalAccessFromFileURLs(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->s(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setAppCacheEnabled(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->u(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setAppCacheMaxSize(J)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, Lcom/miui/webkit_api/a/z$c;->a(Ljava/lang/Object;J)V

    return-void
.end method

.method public setAppCachePath(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->i(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setBlockNetworkImage(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->p(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setBlockNetworkLoads(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->q(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setBuiltInZoomControls(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->c(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setCacheMode(I)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->f(Ljava/lang/Object;I)V

    return-void
.end method

.method public setCursiveFontFamily(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setDatabaseEnabled(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->v(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setDatabasePath(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setDefaultFixedFontSize(I)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->e(Ljava/lang/Object;I)V

    return-void
.end method

.method public setDefaultFontSize(I)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->d(Ljava/lang/Object;I)V

    return-void
.end method

.method public setDefaultTextEncodingName(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->j(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setDefaultZoom(Lcom/miui/webkit_api/WebSettings$ZoomDensity;)V
    .locals 3

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-static {}, Lcom/miui/webkit_api/a/z;->e()Lcom/miui/webkit_api/a/z$f;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/miui/webkit_api/a/z$f;->a(Lcom/miui/webkit_api/WebSettings$ZoomDensity;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public setDisabledActionModeMenuItems(I)V
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->h(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "current browser apk is not support setDisabledActionModeMenuItems(int menuItems), current version is "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreIntVersion()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", expected version is 0x00010006."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "BrowserWebSettings"

    invoke-static {v0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public setDisplayZoomControls(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->d(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setDomStorageEnabled(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->w(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setEnableSmoothTransition(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->h(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setFantasyFontFamily(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setFixedFontFamily(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->b(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setGeolocationDatabasePath(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->h(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setGeolocationEnabled(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->x(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setJavaScriptCanOpenWindowsAutomatically(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->y(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setJavaScriptEnabled(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->r(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setLayoutAlgorithm(Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;)V
    .locals 3

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-static {}, Lcom/miui/webkit_api/a/z;->a()Lcom/miui/webkit_api/a/z$a;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/miui/webkit_api/a/z$a;->a(Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public setLightTouchEnabled(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->l(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setLoadWithOverviewMode(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->g(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setLoadsImagesAutomatically(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->o(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setMediaPlaybackRequiresUserGesture(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->b(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setMinimumFontSize(I)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->b(Ljava/lang/Object;I)V

    return-void
.end method

.method public setMinimumLogicalFontSize(I)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->c(Ljava/lang/Object;I)V

    return-void
.end method

.method public setMixedContentMode(I)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->g(Ljava/lang/Object;I)V

    return-void
.end method

.method public setNeedInitialFocus(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->z(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setOffscreenPreRaster(Z)V
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->A(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "current browser apk is not support setOffscreenPreRaster(enabled), current version is "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/miui/webkit_api/VersionInfo;->getCoreIntVersion()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", expected version is 0x00010006"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "BrowserWebSettings"

    invoke-static {v0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public setPluginState(Lcom/miui/webkit_api/WebSettings$PluginState;)V
    .locals 3

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-static {}, Lcom/miui/webkit_api/a/z;->i()Lcom/miui/webkit_api/a/z$b;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/miui/webkit_api/a/z$b;->a(Lcom/miui/webkit_api/WebSettings$PluginState;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->d(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public setRenderPriority(Lcom/miui/webkit_api/WebSettings$RenderPriority;)V
    .locals 3

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-static {}, Lcom/miui/webkit_api/a/z;->g()Lcom/miui/webkit_api/a/z$d;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/miui/webkit_api/a/z$d;->a(Lcom/miui/webkit_api/WebSettings$RenderPriority;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->e(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public setSansSerifFontFamily(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->c(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setSaveFormData(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->i(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setSavePassword(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->j(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setSerifFontFamily(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setStandardFontFamily(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setSupportMultipleWindows(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->n(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setSupportZoom(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->a(Ljava/lang/Object;Z)V

    return-void
.end method

.method public declared-synchronized setTextSize(Lcom/miui/webkit_api/WebSettings$TextSize;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-static {}, Lcom/miui/webkit_api/a/z;->c()Lcom/miui/webkit_api/a/z$e;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/miui/webkit_api/a/z$e;->a(Lcom/miui/webkit_api/WebSettings$TextSize;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setTextZoom(I)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->a(Ljava/lang/Object;I)V

    return-void
.end method

.method public setUseWideViewPort(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->m(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setUserAgentString(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/miui/webkit_api/a/z$c;->k(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public supportMultipleWindows()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->p(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public supportZoom()Z
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/z;->k()Lcom/miui/webkit_api/a/z$c;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/z;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/z$c;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
