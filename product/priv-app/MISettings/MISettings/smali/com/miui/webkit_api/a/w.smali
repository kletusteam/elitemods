.class Lcom/miui/webkit_api/a/w;
.super Lcom/miui/webkit_api/WebResourceError;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/a/w$a;
    }
.end annotation


# instance fields
.field private a:Lcom/miui/webkit_api/a/w$a;

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/WebResourceError;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/a/w;->b:Ljava/lang/Object;

    return-void
.end method

.method private b()Lcom/miui/webkit_api/a/w$a;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/a/w;->a:Lcom/miui/webkit_api/a/w$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/webkit_api/a/w$a;

    iget-object v1, p0, Lcom/miui/webkit_api/a/w;->b:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/miui/webkit_api/a/w$a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/webkit_api/a/w;->a:Lcom/miui/webkit_api/a/w$a;

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/w;->a:Lcom/miui/webkit_api/a/w$a;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/w;->b:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public getDescription()Ljava/lang/CharSequence;
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/w;->b()Lcom/miui/webkit_api/a/w$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/w;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/w$a;->b(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getErrorCode()I
    .locals 2

    invoke-direct {p0}, Lcom/miui/webkit_api/a/w;->b()Lcom/miui/webkit_api/a/w$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/webkit_api/a/w;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/a/w$a;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
