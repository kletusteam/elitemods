.class public Lcom/miui/webkit_api/c/n;
.super Lcom/miui/webkit_api/ServiceWorkerController;


# instance fields
.field private a:Landroid/webkit/ServiceWorkerController;

.field private b:Lcom/miui/webkit_api/ServiceWorkerWebSettings;


# direct methods
.method public constructor <init>(Landroid/webkit/ServiceWorkerController;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/webkit_api/ServiceWorkerController;-><init>()V

    iput-object p1, p0, Lcom/miui/webkit_api/c/n;->a:Landroid/webkit/ServiceWorkerController;

    return-void
.end method


# virtual methods
.method public getServiceWorkerWebSettings()Lcom/miui/webkit_api/ServiceWorkerWebSettings;
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/c/n;->b:Lcom/miui/webkit_api/ServiceWorkerWebSettings;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/webkit_api/c/n;->a:Landroid/webkit/ServiceWorkerController;

    invoke-virtual {v0}, Landroid/webkit/ServiceWorkerController;->getServiceWorkerWebSettings()Landroid/webkit/ServiceWorkerWebSettings;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/c/o;

    invoke-direct {v1, v0}, Lcom/miui/webkit_api/c/o;-><init>(Landroid/webkit/ServiceWorkerWebSettings;)V

    iput-object v1, p0, Lcom/miui/webkit_api/c/n;->b:Lcom/miui/webkit_api/ServiceWorkerWebSettings;

    :cond_1
    iget-object v0, p0, Lcom/miui/webkit_api/c/n;->b:Lcom/miui/webkit_api/ServiceWorkerWebSettings;

    return-object v0
.end method

.method public setServiceWorkerClient(Lcom/miui/webkit_api/ServiceWorkerClient;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/webkit_api/c/n;->a:Landroid/webkit/ServiceWorkerController;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/miui/webkit_api/c/m;

    invoke-direct {v1, p1}, Lcom/miui/webkit_api/c/m;-><init>(Lcom/miui/webkit_api/ServiceWorkerClient;)V

    move-object p1, v1

    :goto_0
    invoke-virtual {v0, p1}, Landroid/webkit/ServiceWorkerController;->setServiceWorkerClient(Landroid/webkit/ServiceWorkerClient;)V

    return-void
.end method
