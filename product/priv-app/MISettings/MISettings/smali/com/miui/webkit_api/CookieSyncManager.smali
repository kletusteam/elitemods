.class public abstract Lcom/miui/webkit_api/CookieSyncManager;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/miui/webkit_api/CookieSyncManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized createInstance(Landroid/content/Context;)Lcom/miui/webkit_api/CookieSyncManager;
    .locals 2

    const-class v0, Lcom/miui/webkit_api/CookieSyncManager;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/miui/webkit_api/CookieSyncManager;->a:Lcom/miui/webkit_api/CookieSyncManager;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/miui/webkit_api/b/g;->a(Landroid/content/Context;)Lcom/miui/webkit_api/CookieSyncManager;

    move-result-object p0

    sput-object p0, Lcom/miui/webkit_api/CookieSyncManager;->a:Lcom/miui/webkit_api/CookieSyncManager;

    :cond_0
    sget-object p0, Lcom/miui/webkit_api/CookieSyncManager;->a:Lcom/miui/webkit_api/CookieSyncManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized getInstance()Lcom/miui/webkit_api/CookieSyncManager;
    .locals 2

    const-class v0, Lcom/miui/webkit_api/CookieSyncManager;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/miui/webkit_api/CookieSyncManager;->a:Lcom/miui/webkit_api/CookieSyncManager;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/miui/webkit_api/WebViewFactoryRoot;->e()Lcom/miui/webkit_api/b/g;

    move-result-object v1

    invoke-interface {v1}, Lcom/miui/webkit_api/b/g;->j()Lcom/miui/webkit_api/CookieSyncManager;

    move-result-object v1

    sput-object v1, Lcom/miui/webkit_api/CookieSyncManager;->a:Lcom/miui/webkit_api/CookieSyncManager;

    :cond_0
    sget-object v1, Lcom/miui/webkit_api/CookieSyncManager;->a:Lcom/miui/webkit_api/CookieSyncManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public abstract resetSync()V
.end method

.method public abstract run()V
.end method

.method public abstract startSync()V
.end method

.method public abstract stopSync()V
.end method

.method public abstract sync()V
.end method
