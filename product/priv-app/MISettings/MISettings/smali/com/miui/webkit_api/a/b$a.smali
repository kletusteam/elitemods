.class Lcom/miui/webkit_api/a/b$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/webkit_api/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field static final a:Ljava/lang/String; = "com.miui.webkit.ConsoleMessage$MessageLevel"


# instance fields
.field private b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "com.miui.webkit.ConsoleMessage$MessageLevel"

    invoke-static {v0}, Lcom/miui/webkit_api/a/al;->a(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/webkit_api/a/b$a;->b:Ljava/lang/Class;

    iget-object v0, p0, Lcom/miui/webkit_api/a/b$a;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_5

    aget-object v3, v0, v2

    check-cast v3, Ljava/lang/Enum;

    invoke-virtual {v3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v4

    const-string v5, "TIP"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iput-object v3, p0, Lcom/miui/webkit_api/a/b$a;->c:Ljava/lang/Object;

    goto :goto_1

    :cond_0
    const-string v5, "LOG"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iput-object v3, p0, Lcom/miui/webkit_api/a/b$a;->d:Ljava/lang/Object;

    goto :goto_1

    :cond_1
    const-string v5, "WARNING"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iput-object v3, p0, Lcom/miui/webkit_api/a/b$a;->e:Ljava/lang/Object;

    goto :goto_1

    :cond_2
    const-string v5, "ERROR"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iput-object v3, p0, Lcom/miui/webkit_api/a/b$a;->f:Ljava/lang/Object;

    goto :goto_1

    :cond_3
    const-string v5, "DEBUG"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iput-object v3, p0, Lcom/miui/webkit_api/a/b$a;->g:Ljava/lang/Object;

    :cond_4
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;
    .locals 1

    iget-object v0, p0, Lcom/miui/webkit_api/a/b$a;->c:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->TIP:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    return-object p1

    :cond_0
    iget-object v0, p0, Lcom/miui/webkit_api/a/b$a;->d:Ljava/lang/Object;

    if-ne p1, v0, :cond_1

    sget-object p1, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->LOG:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    return-object p1

    :cond_1
    iget-object v0, p0, Lcom/miui/webkit_api/a/b$a;->e:Ljava/lang/Object;

    if-ne p1, v0, :cond_2

    sget-object p1, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->WARNING:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    return-object p1

    :cond_2
    iget-object v0, p0, Lcom/miui/webkit_api/a/b$a;->f:Ljava/lang/Object;

    if-ne p1, v0, :cond_3

    sget-object p1, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->ERROR:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    return-object p1

    :cond_3
    iget-object v0, p0, Lcom/miui/webkit_api/a/b$a;->g:Ljava/lang/Object;

    if-ne p1, v0, :cond_4

    sget-object p1, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->DEBUG:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    return-object p1

    :cond_4
    sget-object p1, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->TIP:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    return-object p1
.end method

.method public a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/webkit_api/a/b$a;->b:Ljava/lang/Class;

    return-object v0
.end method

.method public a(Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;)Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->TIP:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/miui/webkit_api/a/b$a;->c:Ljava/lang/Object;

    return-object p1

    :cond_0
    sget-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->LOG:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lcom/miui/webkit_api/a/b$a;->d:Ljava/lang/Object;

    return-object p1

    :cond_1
    sget-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->WARNING:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lcom/miui/webkit_api/a/b$a;->e:Ljava/lang/Object;

    return-object p1

    :cond_2
    sget-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->ERROR:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    if-ne p1, v0, :cond_3

    iget-object p1, p0, Lcom/miui/webkit_api/a/b$a;->f:Ljava/lang/Object;

    return-object p1

    :cond_3
    sget-object v0, Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;->DEBUG:Lcom/miui/webkit_api/ConsoleMessage$MessageLevel;

    if-ne p1, v0, :cond_4

    iget-object p1, p0, Lcom/miui/webkit_api/a/b$a;->g:Ljava/lang/Object;

    return-object p1

    :cond_4
    iget-object p1, p0, Lcom/miui/webkit_api/a/b$a;->c:Ljava/lang/Object;

    return-object p1
.end method
