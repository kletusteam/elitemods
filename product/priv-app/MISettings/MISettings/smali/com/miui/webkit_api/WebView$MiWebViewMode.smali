.class public final enum Lcom/miui/webkit_api/WebView$MiWebViewMode;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/webkit_api/WebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MiWebViewMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/webkit_api/WebView$MiWebViewMode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BuiltIn:Lcom/miui/webkit_api/WebView$MiWebViewMode;

.field public static final enum None:Lcom/miui/webkit_api/WebView$MiWebViewMode;

.field public static final enum Plugin:Lcom/miui/webkit_api/WebView$MiWebViewMode;

.field public static final enum Shared:Lcom/miui/webkit_api/WebView$MiWebViewMode;

.field private static final synthetic a:[Lcom/miui/webkit_api/WebView$MiWebViewMode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;

    const/4 v1, 0x0

    const-string v2, "None"

    invoke-direct {v0, v2, v1}, Lcom/miui/webkit_api/WebView$MiWebViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;->None:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    new-instance v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;

    const/4 v2, 0x1

    const-string v3, "Shared"

    invoke-direct {v0, v3, v2}, Lcom/miui/webkit_api/WebView$MiWebViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;->Shared:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    new-instance v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;

    const/4 v3, 0x2

    const-string v4, "BuiltIn"

    invoke-direct {v0, v4, v3}, Lcom/miui/webkit_api/WebView$MiWebViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;->BuiltIn:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    new-instance v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;

    const/4 v4, 0x3

    const-string v5, "Plugin"

    invoke-direct {v0, v5, v4}, Lcom/miui/webkit_api/WebView$MiWebViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;->Plugin:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/miui/webkit_api/WebView$MiWebViewMode;

    sget-object v5, Lcom/miui/webkit_api/WebView$MiWebViewMode;->None:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    aput-object v5, v0, v1

    sget-object v1, Lcom/miui/webkit_api/WebView$MiWebViewMode;->Shared:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/miui/webkit_api/WebView$MiWebViewMode;->BuiltIn:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/miui/webkit_api/WebView$MiWebViewMode;->Plugin:Lcom/miui/webkit_api/WebView$MiWebViewMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;->a:[Lcom/miui/webkit_api/WebView$MiWebViewMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/webkit_api/WebView$MiWebViewMode;
    .locals 1

    const-class v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/miui/webkit_api/WebView$MiWebViewMode;

    return-object p0
.end method

.method public static values()[Lcom/miui/webkit_api/WebView$MiWebViewMode;
    .locals 1

    sget-object v0, Lcom/miui/webkit_api/WebView$MiWebViewMode;->a:[Lcom/miui/webkit_api/WebView$MiWebViewMode;

    invoke-virtual {v0}, [Lcom/miui/webkit_api/WebView$MiWebViewMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/webkit_api/WebView$MiWebViewMode;

    return-object v0
.end method
