.class public abstract Lcom/miui/webkit_api/MiuiDelegate;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/webkit_api/MiuiDelegate$PreloadCallback;
    }
.end annotation


# static fields
.field public static final ENABLE_PAGE_COMMIT_ON_FILE_SCHEME_MIUI_QUIRK:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearBrowsingData(ZZZZJ)V
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "MiuiDelegate"

    const-string p1, "Current WebView is system webview, so MiuiDelegate.clearBrowsingData(incognito, cache, cookies, localStorage, intervalInSec) will do nothing"

    invoke-static {p0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static/range {p0 .. p5}, Lcom/miui/webkit_api/a/ah;->a(ZZZZJ)V

    :goto_0
    return-void
.end method

.method public static enablePerformanceTimingReport(Ljava/lang/String;Z)V
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "MiuiDelegate"

    const-string p1, "Current WebView is system webview, so MiuiDelegate.enablePerformanceTimingReport(pattern, iframe) will do nothing"

    invoke-static {p0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lcom/miui/webkit_api/a/ah;->a(Ljava/lang/String;Z)V

    :goto_0
    return-void
.end method

.method public static enableResourceTimingReport(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "MiuiDelegate"

    const-string v0, "Current WebView is system webview, so MiuiDelegate.enableResourceTimingReport(pattern) will do nothing"

    invoke-static {p0, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/miui/webkit_api/a/ah;->a(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static getDownstreamKbps()I
    .locals 2

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MiuiDelegate"

    const-string v1, "Current WebView is system webview, so MiuiDelegate.getDownstreamKbps() return 0"

    invoke-static {v0, v1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-static {}, Lcom/miui/webkit_api/a/ah;->i()I

    move-result v0

    return v0
.end method

.method public static getEffectiveConnectionType()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MiuiDelegate"

    const-string v1, "Current WebView is system webview, so MiuiDelegate.getEffectiveConnectionType() return null"

    invoke-static {v0, v1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-static {}, Lcom/miui/webkit_api/a/ah;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getGlobalSettings()Lcom/miui/webkit_api/MiuiGlobalSettings;
    .locals 2

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MiuiDelegate"

    const-string v1, "Current WebView is system webview, so MiuiDelegate.getGlobalSettings() return null"

    invoke-static {v0, v1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-static {}, Lcom/miui/webkit_api/a/ah;->c()Lcom/miui/webkit_api/MiuiGlobalSettings;

    move-result-object v0

    return-object v0
.end method

.method public static getHttpRtt()I
    .locals 2

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MiuiDelegate"

    const-string v1, "Current WebView is system webview, so MiuiDelegate.getHttpRtt() return 0"

    invoke-static {v0, v1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-static {}, Lcom/miui/webkit_api/a/ah;->g()I

    move-result v0

    return v0
.end method

.method public static getTransportRtt()I
    .locals 2

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MiuiDelegate"

    const-string v1, "Current WebView is system webview, so MiuiDelegate.getTransportRtt() return 0"

    invoke-static {v0, v1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-static {}, Lcom/miui/webkit_api/a/ah;->h()I

    move-result v0

    return v0
.end method

.method public static preconnectUrl(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "MiuiDelegate"

    const-string v0, "Current webview is system webview, so MiuiDelegate.preconnectUrl(url) will do nothing"

    invoke-static {p0, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/miui/webkit_api/a/ah;->b(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static prefetchContent([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "MiuiDelegate"

    const-string p1, "Current WebView is system webview, so MiuiDelegate.prefetchContent(prefetchUrls, keyResources) will do nothing"

    invoke-static {p0, p1}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lcom/miui/webkit_api/a/ah;->a([Ljava/lang/String;[Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static preloadNativeLibrary(Landroid/content/Context;Lcom/miui/webkit_api/MiuiDelegate$PreloadCallback;)V
    .locals 2

    invoke-static {p0}, Lcom/miui/webkit_api/util/NativeLibraryUtil;->a(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/miui/webkit_api/MiuiDelegate$1;

    invoke-direct {v1, p0, p1}, Lcom/miui/webkit_api/MiuiDelegate$1;-><init>(Landroid/content/Context;Lcom/miui/webkit_api/MiuiDelegate$PreloadCallback;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public static prerenderUrl(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "MiuiDelegate"

    const-string v0, "Current webview is system webview, so MiuiDelegate.prerenderUrl(url) will do nothing"

    invoke-static {p0, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/miui/webkit_api/a/ah;->c(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static setEnableMiuiQuirks(I)V
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "MiuiDelegate"

    const-string v0, "Current WebView is system webview, so MiuiDelegate.setEnableMiuiQuirks(flags) will do nothing"

    invoke-static {p0, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/miui/webkit_api/a/ah;->a(I)V

    :goto_0
    return-void
.end method

.method public static setNetworkClient(Lcom/miui/webkit_api/MiuiNetworkClient;)V
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "MiuiDelegate"

    const-string v0, "Current WebView is system webview, so MiuiDelegate.setNetworkClient(client) will do nothing"

    invoke-static {p0, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/miui/webkit_api/a/ah;->a(Lcom/miui/webkit_api/MiuiNetworkClient;)V

    :goto_0
    return-void
.end method

.method public static setPerformaceModeEnabled(Z)V
    .locals 1

    invoke-static {}, Lcom/miui/webkit_api/WebView;->isSystemWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "MiuiDelegate"

    const-string v0, "Current WebView is system webview, so MiuiDelegate.setPerformaceModeEnabled(enabled) will do nothing"

    invoke-static {p0, v0}, Lcom/miui/webkit_api/util/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/miui/webkit_api/a/ah;->a(Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method public abstract calculateAdsCount()I
.end method

.method public abstract checkIfReadModeAvailable(Z)Z
.end method

.method public abstract getSettings()Lcom/miui/webkit_api/MiuiSettings;
.end method

.method public abstract setAllowGeolocationOnInsecureOrigins(Z)V
.end method

.method public abstract setSlideOverscrollHandler(Lcom/miui/webkit_api/MiuiSlideOverscrollHandler;)V
.end method

.method public abstract setTopControlsHeight(IZ)V
.end method

.method public abstract setWebViewClient(Lcom/miui/webkit_api/MiuiWebViewClient;)V
.end method

.method public abstract updateTopControlsState(ZZZ)V
.end method
