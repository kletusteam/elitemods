.class Lcom/miui/support/cardview/a/a;
.super Landroid/view/ViewOutlineProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/support/cardview/a/b;->c(Lcom/miui/support/cardview/a/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/support/cardview/a/c;

.field final synthetic b:Lcom/miui/support/cardview/a/b;


# direct methods
.method constructor <init>(Lcom/miui/support/cardview/a/b;Lcom/miui/support/cardview/a/c;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/support/cardview/a/a;->b:Lcom/miui/support/cardview/a/b;

    iput-object p2, p0, Lcom/miui/support/cardview/a/a;->a:Lcom/miui/support/cardview/a/c;

    invoke-direct {p0}, Landroid/view/ViewOutlineProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public getOutline(Landroid/view/View;Landroid/graphics/Outline;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/miui/support/cardview/a/a;->a:Lcom/miui/support/cardview/a/c;

    invoke-interface {p1}, Lcom/miui/support/cardview/a/c;->b()F

    move-result p1

    invoke-virtual {p2, p1}, Landroid/graphics/Outline;->setAlpha(F)V

    iget-object p1, p0, Lcom/miui/support/cardview/a/a;->a:Lcom/miui/support/cardview/a/c;

    invoke-interface {p1}, Lcom/miui/support/cardview/a/c;->a()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->getOutline(Landroid/graphics/Outline;)V

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x1c

    if-lt p1, p2, :cond_1

    iget-object p1, p0, Lcom/miui/support/cardview/a/a;->a:Lcom/miui/support/cardview/a/c;

    invoke-interface {p1}, Lcom/miui/support/cardview/a/c;->a()Landroid/view/View;

    move-result-object p1

    iget-object p2, p0, Lcom/miui/support/cardview/a/a;->a:Lcom/miui/support/cardview/a/c;

    invoke-interface {p2}, Lcom/miui/support/cardview/a/c;->c()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/view/View;->setOutlineSpotShadowColor(I)V

    :cond_1
    :goto_0
    return-void
.end method
