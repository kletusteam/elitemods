.class public Lcom/miui/support/cardview/a/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/support/cardview/a/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private c(Lcom/miui/support/cardview/a/c;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    invoke-interface {p1}, Lcom/miui/support/cardview/a/c;->a()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/miui/support/cardview/a/a;

    invoke-direct {v1, p0, p1}, Lcom/miui/support/cardview/a/a;-><init>(Lcom/miui/support/cardview/a/b;Lcom/miui/support/cardview/a/c;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/miui/support/cardview/a/c;)I
    .locals 0

    invoke-interface {p1}, Lcom/miui/support/cardview/a/c;->c()I

    move-result p1

    return p1
.end method

.method public a(Lcom/miui/support/cardview/a/c;F)V
    .locals 0

    invoke-interface {p1, p2}, Lcom/miui/support/cardview/a/c;->a(F)V

    invoke-direct {p0, p1}, Lcom/miui/support/cardview/a/b;->c(Lcom/miui/support/cardview/a/c;)V

    return-void
.end method

.method public a(Lcom/miui/support/cardview/a/c;I)V
    .locals 0

    invoke-interface {p1, p2}, Lcom/miui/support/cardview/a/c;->a(I)V

    invoke-direct {p0, p1}, Lcom/miui/support/cardview/a/b;->c(Lcom/miui/support/cardview/a/c;)V

    return-void
.end method

.method public b(Lcom/miui/support/cardview/a/c;)F
    .locals 0

    invoke-interface {p1}, Lcom/miui/support/cardview/a/c;->b()F

    move-result p1

    return p1
.end method
