.class public Lcom/miui/support/cardview/CardView;
.super Landroidx/cardview/widget/CardView;


# static fields
.field private static final j:Lcom/miui/support/cardview/a/d;


# instance fields
.field private final k:Lcom/miui/support/cardview/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/miui/support/cardview/a/b;

    invoke-direct {v0}, Lcom/miui/support/cardview/a/b;-><init>()V

    sput-object v0, Lcom/miui/support/cardview/CardView;->j:Lcom/miui/support/cardview/a/d;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroidx/cardview/widget/CardView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/miui/support/cardview/a;

    invoke-direct {v0, p0}, Lcom/miui/support/cardview/a;-><init>(Lcom/miui/support/cardview/CardView;)V

    iput-object v0, p0, Lcom/miui/support/cardview/CardView;->k:Lcom/miui/support/cardview/a/c;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/miui/support/cardview/CardView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroidx/cardview/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/miui/support/cardview/a;

    invoke-direct {v0, p0}, Lcom/miui/support/cardview/a;-><init>(Lcom/miui/support/cardview/CardView;)V

    iput-object v0, p0, Lcom/miui/support/cardview/CardView;->k:Lcom/miui/support/cardview/a/c;

    invoke-direct {p0, p1, p2}, Lcom/miui/support/cardview/CardView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroidx/cardview/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p3, Lcom/miui/support/cardview/a;

    invoke-direct {p3, p0}, Lcom/miui/support/cardview/a;-><init>(Lcom/miui/support/cardview/CardView;)V

    iput-object p3, p0, Lcom/miui/support/cardview/CardView;->k:Lcom/miui/support/cardview/a/c;

    invoke-direct {p0, p1, p2}, Lcom/miui/support/cardview/CardView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/miui/support/cardview/b;->CardView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget p1, Lcom/miui/support/cardview/b;->CardView_shadowColor:I

    const/high16 p2, -0x1000000

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    sget p2, Lcom/miui/support/cardview/b;->CardView_shadowAlpha:I

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, p2, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result p2

    const/4 v2, 0x0

    cmpg-float v3, p2, v2

    if-gez v3, :cond_0

    move p2, v2

    goto :goto_0

    :cond_0
    cmpl-float v2, p2, v1

    if-lez v2, :cond_1

    move p2, v1

    :cond_1
    :goto_0
    sget-object v1, Lcom/miui/support/cardview/CardView;->j:Lcom/miui/support/cardview/a/d;

    iget-object v2, p0, Lcom/miui/support/cardview/CardView;->k:Lcom/miui/support/cardview/a/c;

    invoke-interface {v1, v2, p1}, Lcom/miui/support/cardview/a/d;->a(Lcom/miui/support/cardview/a/c;I)V

    sget-object p1, Lcom/miui/support/cardview/CardView;->j:Lcom/miui/support/cardview/a/d;

    iget-object v1, p0, Lcom/miui/support/cardview/CardView;->k:Lcom/miui/support/cardview/a/c;

    invoke-interface {p1, v1, p2}, Lcom/miui/support/cardview/a/d;->a(Lcom/miui/support/cardview/a/c;F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_2
    return-void

    :catchall_0
    move-exception p1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_3
    throw p1
.end method


# virtual methods
.method public getShadowAlpha()F
    .locals 2

    sget-object v0, Lcom/miui/support/cardview/CardView;->j:Lcom/miui/support/cardview/a/d;

    iget-object v1, p0, Lcom/miui/support/cardview/CardView;->k:Lcom/miui/support/cardview/a/c;

    invoke-interface {v0, v1}, Lcom/miui/support/cardview/a/d;->b(Lcom/miui/support/cardview/a/c;)F

    move-result v0

    return v0
.end method

.method public getShadowColor()I
    .locals 2

    sget-object v0, Lcom/miui/support/cardview/CardView;->j:Lcom/miui/support/cardview/a/d;

    iget-object v1, p0, Lcom/miui/support/cardview/CardView;->k:Lcom/miui/support/cardview/a/c;

    invoke-interface {v0, v1}, Lcom/miui/support/cardview/a/d;->a(Lcom/miui/support/cardview/a/c;)I

    move-result v0

    return v0
.end method

.method public setShadowAlpha(F)V
    .locals 2

    sget-object v0, Lcom/miui/support/cardview/CardView;->j:Lcom/miui/support/cardview/a/d;

    iget-object v1, p0, Lcom/miui/support/cardview/CardView;->k:Lcom/miui/support/cardview/a/c;

    invoke-interface {v0, v1, p1}, Lcom/miui/support/cardview/a/d;->a(Lcom/miui/support/cardview/a/c;F)V

    return-void
.end method

.method public setShadowColor(I)V
    .locals 2

    sget-object v0, Lcom/miui/support/cardview/CardView;->j:Lcom/miui/support/cardview/a/d;

    iget-object v1, p0, Lcom/miui/support/cardview/CardView;->k:Lcom/miui/support/cardview/a/c;

    invoke-interface {v0, v1, p1}, Lcom/miui/support/cardview/a/d;->a(Lcom/miui/support/cardview/a/c;I)V

    return-void
.end method
