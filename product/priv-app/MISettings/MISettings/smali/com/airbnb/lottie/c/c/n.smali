.class public Lcom/airbnb/lottie/c/c/n;
.super Lcom/airbnb/lottie/c/c/c;


# instance fields
.field private final A:Landroid/graphics/Paint;

.field private final B:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/airbnb/lottie/c/d;",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/a/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private final C:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final D:Lcom/airbnb/lottie/a/b/n;

.field private final E:Lcom/airbnb/lottie/LottieDrawable;

.field private final F:Lcom/airbnb/lottie/h;

.field private G:Lcom/airbnb/lottie/a/b/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/airbnb/lottie/a/b/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private H:Lcom/airbnb/lottie/a/b/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/airbnb/lottie/a/b/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private I:Lcom/airbnb/lottie/a/b/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/airbnb/lottie/a/b/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private J:Lcom/airbnb/lottie/a/b/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/airbnb/lottie/a/b/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final w:Ljava/lang/StringBuilder;

.field private final x:Landroid/graphics/RectF;

.field private final y:Landroid/graphics/Matrix;

.field private final z:Landroid/graphics/Paint;


# direct methods
.method constructor <init>(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/g;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/airbnb/lottie/c/c/c;-><init>(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/g;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/airbnb/lottie/c/c/n;->w:Ljava/lang/StringBuilder;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/airbnb/lottie/c/c/n;->x:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/airbnb/lottie/c/c/n;->y:Landroid/graphics/Matrix;

    new-instance v0, Lcom/airbnb/lottie/c/c/k;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/airbnb/lottie/c/c/k;-><init>(Lcom/airbnb/lottie/c/c/n;I)V

    iput-object v0, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    new-instance v0, Lcom/airbnb/lottie/c/c/l;

    invoke-direct {v0, p0, v1}, Lcom/airbnb/lottie/c/c/l;-><init>(Lcom/airbnb/lottie/c/c/n;I)V

    iput-object v0, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/airbnb/lottie/c/c/n;->B:Ljava/util/Map;

    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/airbnb/lottie/c/c/n;->C:Landroid/util/LongSparseArray;

    iput-object p1, p0, Lcom/airbnb/lottie/c/c/n;->E:Lcom/airbnb/lottie/LottieDrawable;

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/c/g;->a()Lcom/airbnb/lottie/h;

    move-result-object p1

    iput-object p1, p0, Lcom/airbnb/lottie/c/c/n;->F:Lcom/airbnb/lottie/h;

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/c/g;->q()Lcom/airbnb/lottie/c/a/j;

    move-result-object p1

    invoke-virtual {p1}, Lcom/airbnb/lottie/c/a/j;->a()Lcom/airbnb/lottie/a/b/n;

    move-result-object p1

    iput-object p1, p0, Lcom/airbnb/lottie/c/c/n;->D:Lcom/airbnb/lottie/a/b/n;

    iget-object p1, p0, Lcom/airbnb/lottie/c/c/n;->D:Lcom/airbnb/lottie/a/b/n;

    invoke-virtual {p1, p0}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/a/b/a$a;)V

    iget-object p1, p0, Lcom/airbnb/lottie/c/c/n;->D:Lcom/airbnb/lottie/a/b/n;

    invoke-virtual {p0, p1}, Lcom/airbnb/lottie/c/c/c;->a(Lcom/airbnb/lottie/a/b/a;)V

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/c/g;->r()Lcom/airbnb/lottie/c/a/k;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p1, Lcom/airbnb/lottie/c/a/k;->a:Lcom/airbnb/lottie/c/a/a;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/a/a;->a()Lcom/airbnb/lottie/a/b/a;

    move-result-object p2

    iput-object p2, p0, Lcom/airbnb/lottie/c/c/n;->G:Lcom/airbnb/lottie/a/b/a;

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->G:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p2, p0}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/a/b/a$a;)V

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->G:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p0, p2}, Lcom/airbnb/lottie/c/c/c;->a(Lcom/airbnb/lottie/a/b/a;)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object p2, p1, Lcom/airbnb/lottie/c/a/k;->b:Lcom/airbnb/lottie/c/a/a;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/a/a;->a()Lcom/airbnb/lottie/a/b/a;

    move-result-object p2

    iput-object p2, p0, Lcom/airbnb/lottie/c/c/n;->H:Lcom/airbnb/lottie/a/b/a;

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->H:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p2, p0}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/a/b/a$a;)V

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->H:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p0, p2}, Lcom/airbnb/lottie/c/c/c;->a(Lcom/airbnb/lottie/a/b/a;)V

    :cond_1
    if-eqz p1, :cond_2

    iget-object p2, p1, Lcom/airbnb/lottie/c/a/k;->c:Lcom/airbnb/lottie/c/a/b;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/a/b;->a()Lcom/airbnb/lottie/a/b/a;

    move-result-object p2

    iput-object p2, p0, Lcom/airbnb/lottie/c/c/n;->I:Lcom/airbnb/lottie/a/b/a;

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->I:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p2, p0}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/a/b/a$a;)V

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->I:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p0, p2}, Lcom/airbnb/lottie/c/c/c;->a(Lcom/airbnb/lottie/a/b/a;)V

    :cond_2
    if-eqz p1, :cond_3

    iget-object p1, p1, Lcom/airbnb/lottie/c/a/k;->d:Lcom/airbnb/lottie/c/a/b;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/airbnb/lottie/c/a/b;->a()Lcom/airbnb/lottie/a/b/a;

    move-result-object p1

    iput-object p1, p0, Lcom/airbnb/lottie/c/c/n;->J:Lcom/airbnb/lottie/a/b/a;

    iget-object p1, p0, Lcom/airbnb/lottie/c/c/n;->J:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p1, p0}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/a/b/a$a;)V

    iget-object p1, p0, Lcom/airbnb/lottie/c/c/n;->J:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p0, p1}, Lcom/airbnb/lottie/c/c/c;->a(Lcom/airbnb/lottie/a/b/a;)V

    :cond_3
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/airbnb/lottie/c/c;FF)F
    .locals 9

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/c;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/airbnb/lottie/c/d;->a(CLjava/lang/String;Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->F:Lcom/airbnb/lottie/h;

    invoke-virtual {v3}, Lcom/airbnb/lottie/h;->b()Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/airbnb/lottie/c/d;

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    float-to-double v3, v0

    invoke-virtual {v2}, Lcom/airbnb/lottie/c/d;->b()D

    move-result-wide v5

    float-to-double v7, p3

    mul-double/2addr v5, v7

    invoke-static {}, Lcom/airbnb/lottie/f/h;->a()F

    move-result v0

    float-to-double v7, v0

    mul-double/2addr v5, v7

    float-to-double v7, p4

    mul-double/2addr v5, v7

    add-double/2addr v3, v5

    double-to-float v0, v3

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    invoke-virtual {p1, p2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v1

    add-int/2addr v1, p2

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/airbnb/lottie/c/c/n;->a(I)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    add-int/2addr v1, v3

    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v2

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->C:Landroid/util/LongSparseArray;

    int-to-long v3, v0

    invoke-virtual {v2, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object p1, p0, Lcom/airbnb/lottie/c/c/n;->C:Landroid/util/LongSparseArray;

    invoke-virtual {p1, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1

    :cond_2
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/n;->w:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    :goto_2
    if-ge p2, v1, :cond_3

    invoke-virtual {p1, p2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->w:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v0

    add-int/2addr p2, v0

    goto :goto_2

    :cond_3
    iget-object p1, p0, Lcom/airbnb/lottie/c/c/n;->w:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->C:Landroid/util/LongSparseArray;

    invoke-virtual {p2, v3, v4, p1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    return-object p1
.end method

.method private a(Lcom/airbnb/lottie/c/d;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/c/d;",
            ")",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/a/a/e;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/n;->B:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/n;->B:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lcom/airbnb/lottie/c/d;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/airbnb/lottie/c/b/n;

    new-instance v5, Lcom/airbnb/lottie/a/a/e;

    iget-object v6, p0, Lcom/airbnb/lottie/c/c/n;->E:Lcom/airbnb/lottie/LottieDrawable;

    invoke-direct {v5, v6, p0, v4}, Lcom/airbnb/lottie/a/a/e;-><init>(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/c;Lcom/airbnb/lottie/c/b/n;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/n;->B:Ljava/util/Map;

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v2
.end method

.method private a(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "\r"

    const-string v1, "\r\n"

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "\n"

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V
    .locals 2

    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {p3, p1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method private a(Lcom/airbnb/lottie/c/b$a;Landroid/graphics/Canvas;F)V
    .locals 2

    sget-object v0, Lcom/airbnb/lottie/c/c/m;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    neg-float p1, p3

    const/high16 p3, 0x40000000    # 2.0f

    div-float/2addr p1, p3

    invoke-virtual {p2, p1, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    :cond_1
    neg-float p1, p3

    invoke-virtual {p2, p1, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_2
    :goto_0
    return-void
.end method

.method private a(Lcom/airbnb/lottie/c/b;Landroid/graphics/Matrix;Lcom/airbnb/lottie/c/c;Landroid/graphics/Canvas;)V
    .locals 17

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p4

    iget-wide v0, v9, Lcom/airbnb/lottie/c/b;->c:D

    double-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float v11, v0, v1

    invoke-static/range {p2 .. p2}, Lcom/airbnb/lottie/f/h;->a(Landroid/graphics/Matrix;)F

    move-result v12

    iget-object v0, v9, Lcom/airbnb/lottie/c/b;->a:Ljava/lang/String;

    iget-wide v1, v9, Lcom/airbnb/lottie/c/b;->f:D

    double-to-float v1, v1

    invoke-static {}, Lcom/airbnb/lottie/f/h;->a()F

    move-result v2

    mul-float v13, v1, v2

    invoke-direct {v8, v0}, Lcom/airbnb/lottie/c/c/n;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v15

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v15, :cond_0

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v6, p3

    invoke-direct {v8, v1, v6, v11, v12}, Lcom/airbnb/lottie/c/c/n;->a(Ljava/lang/String;Lcom/airbnb/lottie/c/c;FF)F

    move-result v0

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Canvas;->save()I

    iget-object v2, v9, Lcom/airbnb/lottie/c/b;->d:Lcom/airbnb/lottie/c/b$a;

    invoke-direct {v8, v2, v10, v0}, Lcom/airbnb/lottie/c/c/n;->a(Lcom/airbnb/lottie/c/b$a;Landroid/graphics/Canvas;F)V

    add-int/lit8 v0, v15, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, v13

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    int-to-float v2, v7

    mul-float/2addr v2, v13

    sub-float/2addr v2, v0

    const/4 v0, 0x0

    invoke-virtual {v10, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move v6, v12

    move/from16 v16, v7

    move v7, v11

    invoke-direct/range {v0 .. v7}, Lcom/airbnb/lottie/c/c/n;->a(Ljava/lang/String;Lcom/airbnb/lottie/c/b;Landroid/graphics/Matrix;Lcom/airbnb/lottie/c/c;Landroid/graphics/Canvas;FF)V

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Canvas;->restore()V

    add-int/lit8 v7, v16, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Lcom/airbnb/lottie/c/b;Lcom/airbnb/lottie/c/c;Landroid/graphics/Matrix;Landroid/graphics/Canvas;)V
    .locals 7

    invoke-static {p3}, Lcom/airbnb/lottie/f/h;->a(Landroid/graphics/Matrix;)F

    move-result v0

    iget-object v1, p0, Lcom/airbnb/lottie/c/c/n;->E:Lcom/airbnb/lottie/LottieDrawable;

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/c;->c()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, Lcom/airbnb/lottie/LottieDrawable;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object p2

    if-nez p2, :cond_0

    return-void

    :cond_0
    iget-object v1, p1, Lcom/airbnb/lottie/c/b;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->E:Lcom/airbnb/lottie/LottieDrawable;

    invoke-virtual {v2}, Lcom/airbnb/lottie/LottieDrawable;->p()Lcom/airbnb/lottie/T;

    move-result-object v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    invoke-virtual {v2, p2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    iget-wide v2, p1, Lcom/airbnb/lottie/c/b;->c:D

    invoke-static {}, Lcom/airbnb/lottie/f/h;->a()F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    move-result v2

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-wide v2, p1, Lcom/airbnb/lottie/c/b;->f:D

    double-to-float p2, v2

    invoke-static {}, Lcom/airbnb/lottie/f/h;->a()F

    move-result v2

    mul-float/2addr p2, v2

    invoke-direct {p0, v1}, Lcom/airbnb/lottie/c/c/n;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    iget-object v6, p1, Lcom/airbnb/lottie/c/b;->d:Lcom/airbnb/lottie/c/b$a;

    invoke-direct {p0, v6, p4, v5}, Lcom/airbnb/lottie/c/c/n;->a(Lcom/airbnb/lottie/c/b$a;Landroid/graphics/Canvas;F)V

    add-int/lit8 v5, v2, -0x1

    int-to-float v5, v5

    mul-float/2addr v5, p2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    int-to-float v6, v3

    mul-float/2addr v6, p2

    sub-float/2addr v6, v5

    const/4 v5, 0x0

    invoke-virtual {p4, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-direct {p0, v4, p1, p4, v0}, Lcom/airbnb/lottie/c/c/n;->a(Ljava/lang/String;Lcom/airbnb/lottie/c/b;Landroid/graphics/Canvas;F)V

    invoke-virtual {p4, p3}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v2, v1}, Lcom/airbnb/lottie/T;->a(Ljava/lang/String;)Ljava/lang/String;

    const/4 p1, 0x0

    throw p1
.end method

.method private a(Lcom/airbnb/lottie/c/d;Landroid/graphics/Matrix;FLcom/airbnb/lottie/c/b;Landroid/graphics/Canvas;)V
    .locals 7

    invoke-direct {p0, p1}, Lcom/airbnb/lottie/c/c/n;->a(Lcom/airbnb/lottie/c/d;)Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/airbnb/lottie/a/a/e;

    invoke-virtual {v2}, Lcom/airbnb/lottie/a/a/e;->getPath()Landroid/graphics/Path;

    move-result-object v2

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->x:Landroid/graphics/RectF;

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->y:Landroid/graphics/Matrix;

    invoke-virtual {v3, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->y:Landroid/graphics/Matrix;

    const/4 v4, 0x0

    iget-wide v5, p4, Lcom/airbnb/lottie/c/b;->g:D

    neg-double v5, v5

    double-to-float v5, v5

    invoke-static {}, Lcom/airbnb/lottie/f/h;->a()F

    move-result v6

    mul-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->y:Landroid/graphics/Matrix;

    invoke-virtual {v3, p3, p3}, Landroid/graphics/Matrix;->preScale(FF)Z

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->y:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    iget-boolean v3, p4, Lcom/airbnb/lottie/c/b;->k:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, p5}, Lcom/airbnb/lottie/c/c/n;->a(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, p5}, Lcom/airbnb/lottie/c/c/n;->a(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    goto :goto_1

    :cond_0
    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, p5}, Lcom/airbnb/lottie/c/c/n;->a(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, p5}, Lcom/airbnb/lottie/c/c/n;->a(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V
    .locals 8

    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p3

    move-object v2, p1

    move-object v7, p2

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/airbnb/lottie/c/b;Landroid/graphics/Canvas;)V
    .locals 0

    iget-boolean p2, p2, Lcom/airbnb/lottie/c/b;->k:Z

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    invoke-direct {p0, p1, p2, p3}, Lcom/airbnb/lottie/c/c/n;->a(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    invoke-direct {p0, p1, p2, p3}, Lcom/airbnb/lottie/c/c/n;->a(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    invoke-direct {p0, p1, p2, p3}, Lcom/airbnb/lottie/c/c/n;->a(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    invoke-direct {p0, p1, p2, p3}, Lcom/airbnb/lottie/c/c/n;->a(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/airbnb/lottie/c/b;Landroid/graphics/Canvas;F)V
    .locals 5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-direct {p0, p1, v1}, Lcom/airbnb/lottie/c/c/n;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    invoke-direct {p0, v2, p2, p3}, Lcom/airbnb/lottie/c/c/n;->a(Ljava/lang/String;Lcom/airbnb/lottie/c/b;Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v0, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v2

    iget v3, p2, Lcom/airbnb/lottie/c/b;->e:I

    int-to-float v3, v3

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v3, v4

    iget-object v4, p0, Lcom/airbnb/lottie/c/c/n;->J:Lcom/airbnb/lottie/a/b/a;

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/airbnb/lottie/a/b/a;->f()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    add-float/2addr v3, v4

    :cond_0
    mul-float/2addr v3, p4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/airbnb/lottie/c/b;Landroid/graphics/Matrix;Lcom/airbnb/lottie/c/c;Landroid/graphics/Canvas;FF)V
    .locals 8

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p4}, Lcom/airbnb/lottie/c/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4}, Lcom/airbnb/lottie/c/c;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/airbnb/lottie/c/d;->a(CLjava/lang/String;Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->F:Lcom/airbnb/lottie/h;

    invoke-virtual {v2}, Lcom/airbnb/lottie/h;->b()Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/airbnb/lottie/c/d;

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    move-object v2, p0

    move-object v3, v1

    move-object v4, p3

    move v5, p7

    move-object v6, p2

    move-object v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/airbnb/lottie/c/c/n;->a(Lcom/airbnb/lottie/c/d;Landroid/graphics/Matrix;FLcom/airbnb/lottie/c/b;Landroid/graphics/Canvas;)V

    invoke-virtual {v1}, Lcom/airbnb/lottie/c/d;->b()D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v1, p7

    invoke-static {}, Lcom/airbnb/lottie/f/h;->a()F

    move-result v2

    mul-float/2addr v1, v2

    mul-float/2addr v1, p6

    iget v2, p2, Lcom/airbnb/lottie/c/b;->e:I

    int-to-float v2, v2

    const/high16 v3, 0x41200000    # 10.0f

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/n;->J:Lcom/airbnb/lottie/a/b/a;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/airbnb/lottie/a/b/a;->f()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    add-float/2addr v2, v3

    :cond_1
    mul-float/2addr v2, p6

    add-float/2addr v1, v2

    const/4 v2, 0x0

    invoke-virtual {p5, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(I)Z
    .locals 2

    invoke-static {p1}, Ljava/lang/Character;->getType(I)I

    move-result v0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->getType(I)I

    move-result v0

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->getType(I)I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->getType(I)I

    move-result v0

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->getType(I)I

    move-result p1

    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lcom/airbnb/lottie/c/c/c;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/n;->F:Lcom/airbnb/lottie/h;

    invoke-virtual {p2}, Lcom/airbnb/lottie/h;->a()Landroid/graphics/Rect;

    move-result-object p2

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p2

    int-to-float p2, p2

    iget-object p3, p0, Lcom/airbnb/lottie/c/c/n;->F:Lcom/airbnb/lottie/h;

    invoke-virtual {p3}, Lcom/airbnb/lottie/h;->a()Landroid/graphics/Rect;

    move-result-object p3

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result p3

    int-to-float p3, p3

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0, p2, p3}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method public a(Ljava/lang/Object;Lcom/airbnb/lottie/g/c;)V
    .locals 1
    .param p2    # Lcom/airbnb/lottie/g/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/airbnb/lottie/g/c<",
            "TT;>;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/airbnb/lottie/c/c/c;->a(Ljava/lang/Object;Lcom/airbnb/lottie/g/c;)V

    sget-object v0, Lcom/airbnb/lottie/J;->a:Ljava/lang/Integer;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/n;->G:Lcom/airbnb/lottie/a/b/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/g/c;)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/airbnb/lottie/J;->b:Ljava/lang/Integer;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/n;->H:Lcom/airbnb/lottie/a/b/a;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p2}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/g/c;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/airbnb/lottie/J;->o:Ljava/lang/Float;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/n;->I:Lcom/airbnb/lottie/a/b/a;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p2}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/g/c;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/airbnb/lottie/J;->p:Ljava/lang/Float;

    if-ne p1, v0, :cond_3

    iget-object p1, p0, Lcom/airbnb/lottie/c/c/n;->J:Lcom/airbnb/lottie/a/b/a;

    if-eqz p1, :cond_3

    invoke-virtual {p1, p2}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/g/c;)V

    :cond_3
    :goto_0
    return-void
.end method

.method b(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 7

    goto/32 :goto_24

    nop

    :goto_0
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_1
    check-cast v1, Ljava/lang/Integer;

    goto/32 :goto_42

    nop

    :goto_2
    float-to-double v5, v5

    goto/32 :goto_14

    nop

    :goto_3
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    goto/32 :goto_32

    nop

    :goto_4
    check-cast v0, Lcom/airbnb/lottie/c/c;

    goto/32 :goto_19

    nop

    :goto_5
    invoke-virtual {v1}, Lcom/airbnb/lottie/a/b/a;->f()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_6
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    goto/32 :goto_5

    nop

    :goto_7
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    goto/32 :goto_49

    nop

    :goto_8
    invoke-virtual {v0}, Lcom/airbnb/lottie/h;->f()Ljava/util/Map;

    move-result-object v0

    goto/32 :goto_2a

    nop

    :goto_9
    invoke-direct {p0, p3, v0, p2, p1}, Lcom/airbnb/lottie/c/c/n;->a(Lcom/airbnb/lottie/c/b;Lcom/airbnb/lottie/c/c;Landroid/graphics/Matrix;Landroid/graphics/Canvas;)V

    :goto_a
    goto/32 :goto_45

    nop

    :goto_b
    goto/16 :goto_29

    :goto_c
    goto/32 :goto_3

    nop

    :goto_d
    div-int/2addr v1, v2

    goto/32 :goto_50

    nop

    :goto_e
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    goto/32 :goto_18

    nop

    :goto_f
    invoke-virtual {p3}, Lcom/airbnb/lottie/LottieDrawable;->w()Z

    move-result p3

    goto/32 :goto_59

    nop

    :goto_10
    invoke-virtual {v1}, Lcom/airbnb/lottie/a/b/a;->f()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_57

    nop

    :goto_11
    invoke-static {p2}, Lcom/airbnb/lottie/f/h;->a(Landroid/graphics/Matrix;)F

    move-result v1

    goto/32 :goto_e

    nop

    :goto_12
    check-cast v1, Ljava/lang/Integer;

    goto/32 :goto_5b

    nop

    :goto_13
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto/32 :goto_4e

    nop

    :goto_14
    mul-double/2addr v3, v5

    goto/32 :goto_1f

    nop

    :goto_15
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :goto_16
    goto/32 :goto_44

    nop

    :goto_17
    invoke-virtual {v1}, Lcom/airbnb/lottie/a/b/o;->c()Lcom/airbnb/lottie/a/b/a;

    move-result-object v1

    goto/32 :goto_56

    nop

    :goto_18
    iget-wide v3, p3, Lcom/airbnb/lottie/c/b;->j:D

    goto/32 :goto_46

    nop

    :goto_19
    if-eqz v0, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_3d

    nop

    :goto_1a
    invoke-direct {p0, p3, p2, v0, p1}, Lcom/airbnb/lottie/c/c/n;->a(Lcom/airbnb/lottie/c/b;Landroid/graphics/Matrix;Lcom/airbnb/lottie/c/c;Landroid/graphics/Canvas;)V

    goto/32 :goto_4a

    nop

    :goto_1b
    if-nez v1, :cond_1

    goto/32 :goto_4b

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_1c
    iget-object p3, p0, Lcom/airbnb/lottie/c/c/n;->E:Lcom/airbnb/lottie/LottieDrawable;

    goto/32 :goto_f

    nop

    :goto_1d
    return-void

    :goto_1e
    goto/32 :goto_33

    nop

    :goto_1f
    float-to-double v5, v1

    goto/32 :goto_2d

    nop

    :goto_20
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    goto/32 :goto_30

    nop

    :goto_21
    mul-int/lit16 v1, v1, 0xff

    goto/32 :goto_d

    nop

    :goto_22
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/n;->I:Lcom/airbnb/lottie/a/b/a;

    goto/32 :goto_40

    nop

    :goto_23
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/c;->u:Lcom/airbnb/lottie/a/b/o;

    goto/32 :goto_37

    nop

    :goto_24
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    goto/32 :goto_1c

    nop

    :goto_25
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/32 :goto_b

    nop

    :goto_26
    invoke-virtual {p3}, Lcom/airbnb/lottie/a/b/a;->f()Ljava/lang/Object;

    move-result-object p3

    goto/32 :goto_4c

    nop

    :goto_27
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/n;->E:Lcom/airbnb/lottie/LottieDrawable;

    goto/32 :goto_52

    nop

    :goto_28
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :goto_29
    goto/32 :goto_23

    nop

    :goto_2a
    iget-object v1, p3, Lcom/airbnb/lottie/c/b;->b:Ljava/lang/String;

    goto/32 :goto_0

    nop

    :goto_2b
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :goto_2c
    goto/32 :goto_27

    nop

    :goto_2d
    mul-double/2addr v3, v5

    goto/32 :goto_43

    nop

    :goto_2e
    return-void

    :goto_2f
    const/16 v2, 0x64

    goto/32 :goto_58

    nop

    :goto_30
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/32 :goto_22

    nop

    :goto_31
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/c;->u:Lcom/airbnb/lottie/a/b/o;

    goto/32 :goto_17

    nop

    :goto_32
    iget v2, p3, Lcom/airbnb/lottie/c/b;->i:I

    goto/32 :goto_28

    nop

    :goto_33
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/n;->G:Lcom/airbnb/lottie/a/b/a;

    goto/32 :goto_55

    nop

    :goto_34
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->A:Landroid/graphics/Paint;

    goto/32 :goto_10

    nop

    :goto_35
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    :goto_36
    goto/32 :goto_39

    nop

    :goto_37
    invoke-virtual {v1}, Lcom/airbnb/lottie/a/b/o;->c()Lcom/airbnb/lottie/a/b/a;

    move-result-object v1

    goto/32 :goto_2f

    nop

    :goto_38
    iget v2, p3, Lcom/airbnb/lottie/c/b;->h:I

    goto/32 :goto_15

    nop

    :goto_39
    iget-object p3, p0, Lcom/airbnb/lottie/c/c/n;->D:Lcom/airbnb/lottie/a/b/n;

    goto/32 :goto_26

    nop

    :goto_3a
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    goto/32 :goto_13

    nop

    :goto_3b
    move v1, v2

    goto/32 :goto_3e

    nop

    :goto_3c
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/n;->F:Lcom/airbnb/lottie/h;

    goto/32 :goto_8

    nop

    :goto_3d
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/32 :goto_1d

    nop

    :goto_3e
    goto/16 :goto_5c

    :goto_3f
    goto/32 :goto_31

    nop

    :goto_40
    if-nez v1, :cond_2

    goto/32 :goto_4f

    :cond_2
    goto/32 :goto_7

    nop

    :goto_41
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/32 :goto_20

    nop

    :goto_42
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/32 :goto_53

    nop

    :goto_43
    double-to-float v1, v3

    goto/32 :goto_2b

    nop

    :goto_44
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/n;->H:Lcom/airbnb/lottie/a/b/a;

    goto/32 :goto_54

    nop

    :goto_45
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/32 :goto_2e

    nop

    :goto_46
    invoke-static {}, Lcom/airbnb/lottie/f/h;->a()F

    move-result v5

    goto/32 :goto_2

    nop

    :goto_47
    goto/16 :goto_16

    :goto_48
    goto/32 :goto_4d

    nop

    :goto_49
    invoke-virtual {v1}, Lcom/airbnb/lottie/a/b/a;->f()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_51

    nop

    :goto_4a
    goto/16 :goto_a

    :goto_4b
    goto/32 :goto_9

    nop

    :goto_4c
    check-cast p3, Lcom/airbnb/lottie/c/b;

    goto/32 :goto_3c

    nop

    :goto_4d
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    goto/32 :goto_38

    nop

    :goto_4e
    goto/16 :goto_2c

    :goto_4f
    goto/32 :goto_11

    nop

    :goto_50
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/n;->z:Landroid/graphics/Paint;

    goto/32 :goto_41

    nop

    :goto_51
    check-cast v1, Ljava/lang/Float;

    goto/32 :goto_3a

    nop

    :goto_52
    invoke-virtual {v1}, Lcom/airbnb/lottie/LottieDrawable;->w()Z

    move-result v1

    goto/32 :goto_1b

    nop

    :goto_53
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/32 :goto_47

    nop

    :goto_54
    if-nez v1, :cond_3

    goto/32 :goto_c

    :cond_3
    goto/32 :goto_34

    nop

    :goto_55
    if-nez v1, :cond_4

    goto/32 :goto_48

    :cond_4
    goto/32 :goto_6

    nop

    :goto_56
    invoke-virtual {v1}, Lcom/airbnb/lottie/a/b/a;->f()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_12

    nop

    :goto_57
    check-cast v1, Ljava/lang/Integer;

    goto/32 :goto_5a

    nop

    :goto_58
    if-eqz v1, :cond_5

    goto/32 :goto_3f

    :cond_5
    goto/32 :goto_3b

    nop

    :goto_59
    if-eqz p3, :cond_6

    goto/32 :goto_36

    :cond_6
    goto/32 :goto_35

    nop

    :goto_5a
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/32 :goto_25

    nop

    :goto_5b
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_5c
    goto/32 :goto_21

    nop
.end method
