.class public Lcom/airbnb/lottie/c/c/e;
.super Lcom/airbnb/lottie/c/c/c;


# instance fields
.field private w:Lcom/airbnb/lottie/a/b/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/airbnb/lottie/a/b/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Landroid/graphics/RectF;

.field private final z:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/g;Ljava/util/List;Lcom/airbnb/lottie/h;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/LottieDrawable;",
            "Lcom/airbnb/lottie/c/c/g;",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/c/g;",
            ">;",
            "Lcom/airbnb/lottie/h;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/airbnb/lottie/c/c/c;-><init>(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/g;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/airbnb/lottie/c/c/e;->x:Ljava/util/List;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/airbnb/lottie/c/c/e;->y:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/airbnb/lottie/c/c/e;->z:Landroid/graphics/RectF;

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/c/g;->s()Lcom/airbnb/lottie/c/a/b;

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/a/b;->a()Lcom/airbnb/lottie/a/b/a;

    move-result-object p2

    iput-object p2, p0, Lcom/airbnb/lottie/c/c/e;->w:Lcom/airbnb/lottie/a/b/a;

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/e;->w:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p0, p2}, Lcom/airbnb/lottie/c/c/c;->a(Lcom/airbnb/lottie/a/b/a;)V

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/e;->w:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p2, p0}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/a/b/a$a;)V

    goto :goto_0

    :cond_0
    iput-object v0, p0, Lcom/airbnb/lottie/c/c/e;->w:Lcom/airbnb/lottie/a/b/a;

    :goto_0
    new-instance p2, Landroid/util/LongSparseArray;

    invoke-virtual {p4}, Lcom/airbnb/lottie/h;->i()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p2, v1}, Landroid/util/LongSparseArray;-><init>(I)V

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    move-object v3, v0

    :goto_1
    const/4 v4, 0x0

    if-ltz v1, :cond_4

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/airbnb/lottie/c/c/g;

    invoke-static {v5, p1, p4}, Lcom/airbnb/lottie/c/c/c;->a(Lcom/airbnb/lottie/c/c/g;Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/h;)Lcom/airbnb/lottie/c/c/c;

    move-result-object v6

    if-nez v6, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {v6}, Lcom/airbnb/lottie/c/c/c;->b()Lcom/airbnb/lottie/c/c/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/airbnb/lottie/c/c/g;->b()J

    move-result-wide v7

    invoke-virtual {p2, v7, v8, v6}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    if-eqz v3, :cond_2

    invoke-virtual {v3, v6}, Lcom/airbnb/lottie/c/c/c;->a(Lcom/airbnb/lottie/c/c/c;)V

    move-object v3, v0

    goto :goto_2

    :cond_2
    iget-object v7, p0, Lcom/airbnb/lottie/c/c/e;->x:Ljava/util/List;

    invoke-interface {v7, v4, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    sget-object v4, Lcom/airbnb/lottie/c/c/d;->a:[I

    invoke-virtual {v5}, Lcom/airbnb/lottie/c/c/g;->f()Lcom/airbnb/lottie/c/c/g$b;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    aget v4, v4, v5

    if-eq v4, v2, :cond_3

    const/4 v5, 0x2

    if-eq v4, v5, :cond_3

    goto :goto_2

    :cond_3
    move-object v3, v6

    :goto_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_4
    :goto_3
    invoke-virtual {p2}, Landroid/util/LongSparseArray;->size()I

    move-result p1

    if-ge v4, p1, :cond_7

    invoke-virtual {p2, v4}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide p3

    invoke-virtual {p2, p3, p4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/airbnb/lottie/c/c/c;

    if-nez p1, :cond_5

    goto :goto_4

    :cond_5
    invoke-virtual {p1}, Lcom/airbnb/lottie/c/c/c;->b()Lcom/airbnb/lottie/c/c/g;

    move-result-object p3

    invoke-virtual {p3}, Lcom/airbnb/lottie/c/c/g;->h()J

    move-result-wide p3

    invoke-virtual {p2, p3, p4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/airbnb/lottie/c/c/c;

    if-eqz p3, :cond_6

    invoke-virtual {p1, p3}, Lcom/airbnb/lottie/c/c/c;->b(Lcom/airbnb/lottie/c/c/c;)V

    :cond_6
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_7
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 2
    .param p1    # F
        .annotation build Landroidx/annotation/FloatRange;
            from = 0.0
            to = 1.0
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/airbnb/lottie/c/c/c;->a(F)V

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/e;->w:Lcom/airbnb/lottie/a/b/a;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/airbnb/lottie/c/c/c;->n:Lcom/airbnb/lottie/LottieDrawable;

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieDrawable;->e()Lcom/airbnb/lottie/h;

    move-result-object p1

    invoke-virtual {p1}, Lcom/airbnb/lottie/h;->c()F

    move-result p1

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/e;->w:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {v0}, Lcom/airbnb/lottie/a/b/a;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    float-to-long v0, v0

    long-to-float v0, v0

    div-float p1, v0, p1

    :cond_0
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/c;->o:Lcom/airbnb/lottie/c/c/g;

    invoke-virtual {v0}, Lcom/airbnb/lottie/c/c/g;->t()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/c;->o:Lcom/airbnb/lottie/c/c/g;

    invoke-virtual {v0}, Lcom/airbnb/lottie/c/c/g;->t()F

    move-result v0

    div-float/2addr p1, v0

    :cond_1
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/c;->o:Lcom/airbnb/lottie/c/c/g;

    invoke-virtual {v0}, Lcom/airbnb/lottie/c/c/g;->p()F

    move-result v0

    sub-float/2addr p1, v0

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/e;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    iget-object v1, p0, Lcom/airbnb/lottie/c/c/e;->x:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/airbnb/lottie/c/c/c;

    invoke-virtual {v1, p1}, Lcom/airbnb/lottie/c/c/c;->a(F)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lcom/airbnb/lottie/c/c/c;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/e;->x:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 p3, 0x1

    sub-int/2addr p2, p3

    :goto_0
    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/e;->y:Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/e;->x:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/c/c/c;

    iget-object v1, p0, Lcom/airbnb/lottie/c/c/e;->y:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/airbnb/lottie/c/c/c;->m:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v2, p3}, Lcom/airbnb/lottie/c/c/c;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/e;->y:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;Lcom/airbnb/lottie/g/c;)V
    .locals 1
    .param p2    # Lcom/airbnb/lottie/g/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/airbnb/lottie/g/c<",
            "TT;>;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/airbnb/lottie/c/c/c;->a(Ljava/lang/Object;Lcom/airbnb/lottie/g/c;)V

    sget-object v0, Lcom/airbnb/lottie/J;->A:Ljava/lang/Float;

    if-ne p1, v0, :cond_1

    if-nez p2, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/airbnb/lottie/c/c/e;->w:Lcom/airbnb/lottie/a/b/a;

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/airbnb/lottie/a/b/p;

    invoke-direct {p1, p2}, Lcom/airbnb/lottie/a/b/p;-><init>(Lcom/airbnb/lottie/g/c;)V

    iput-object p1, p0, Lcom/airbnb/lottie/c/c/e;->w:Lcom/airbnb/lottie/a/b/a;

    iget-object p1, p0, Lcom/airbnb/lottie/c/c/e;->w:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p0, p1}, Lcom/airbnb/lottie/c/c/c;->a(Lcom/airbnb/lottie/a/b/a;)V

    :cond_1
    :goto_0
    return-void
.end method

.method b(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 5

    goto/32 :goto_9

    nop

    :goto_0
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/e;->x:Ljava/util/List;

    goto/32 :goto_1d

    nop

    :goto_1
    iget-object v3, p0, Lcom/airbnb/lottie/c/c/e;->x:Ljava/util/List;

    goto/32 :goto_11

    nop

    :goto_2
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/c;->o:Lcom/airbnb/lottie/c/c/g;

    goto/32 :goto_8

    nop

    :goto_3
    goto/16 :goto_1a

    :goto_4
    goto/32 :goto_19

    nop

    :goto_5
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/e;->z:Landroid/graphics/RectF;

    goto/32 :goto_2

    nop

    :goto_6
    sub-int/2addr v1, v2

    :goto_7
    goto/32 :goto_22

    nop

    :goto_8
    invoke-virtual {v2}, Lcom/airbnb/lottie/c/c/g;->j()I

    move-result v2

    goto/32 :goto_15

    nop

    :goto_9
    const-string v0, "CompositionLayer#draw"

    goto/32 :goto_13

    nop

    :goto_a
    return-void

    :goto_b
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/e;->z:Landroid/graphics/RectF;

    goto/32 :goto_c

    nop

    :goto_c
    invoke-virtual {p2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto/32 :goto_0

    nop

    :goto_d
    if-eqz v3, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_16

    nop

    :goto_e
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_20

    nop

    :goto_f
    check-cast v3, Lcom/airbnb/lottie/c/c/c;

    goto/32 :goto_26

    nop

    :goto_10
    invoke-static {v0}, Lcom/airbnb/lottie/c;->b(Ljava/lang/String;)F

    goto/32 :goto_a

    nop

    :goto_11
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_f

    nop

    :goto_12
    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    goto/32 :goto_b

    nop

    :goto_13
    invoke-static {v0}, Lcom/airbnb/lottie/c;->a(Ljava/lang/String;)V

    goto/32 :goto_1e

    nop

    :goto_14
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/32 :goto_10

    nop

    :goto_15
    int-to-float v2, v2

    goto/32 :goto_1f

    nop

    :goto_16
    iget-object v3, p0, Lcom/airbnb/lottie/c/c/e;->z:Landroid/graphics/RectF;

    goto/32 :goto_17

    nop

    :goto_17
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    move-result v3

    goto/32 :goto_3

    nop

    :goto_18
    if-nez v3, :cond_1

    goto/32 :goto_27

    :cond_1
    goto/32 :goto_1

    nop

    :goto_19
    move v3, v2

    :goto_1a
    goto/32 :goto_18

    nop

    :goto_1b
    const/4 v4, 0x0

    goto/32 :goto_12

    nop

    :goto_1c
    const/4 v2, 0x1

    goto/32 :goto_6

    nop

    :goto_1d
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_1c

    nop

    :goto_1e
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    goto/32 :goto_5

    nop

    :goto_1f
    iget-object v3, p0, Lcom/airbnb/lottie/c/c/c;->o:Lcom/airbnb/lottie/c/c/g;

    goto/32 :goto_25

    nop

    :goto_20
    goto/16 :goto_7

    :goto_21
    goto/32 :goto_14

    nop

    :goto_22
    if-gez v1, :cond_2

    goto/32 :goto_21

    :cond_2
    goto/32 :goto_24

    nop

    :goto_23
    invoke-virtual {v3}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v3

    goto/32 :goto_d

    nop

    :goto_24
    iget-object v3, p0, Lcom/airbnb/lottie/c/c/e;->z:Landroid/graphics/RectF;

    goto/32 :goto_23

    nop

    :goto_25
    invoke-virtual {v3}, Lcom/airbnb/lottie/c/c/g;->i()I

    move-result v3

    goto/32 :goto_28

    nop

    :goto_26
    invoke-virtual {v3, p1, p2, p3}, Lcom/airbnb/lottie/c/c/c;->a(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    :goto_27
    goto/32 :goto_e

    nop

    :goto_28
    int-to-float v3, v3

    goto/32 :goto_1b

    nop
.end method

.method protected b(Lcom/airbnb/lottie/c/e;ILjava/util/List;Lcom/airbnb/lottie/c/e;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/c/e;",
            "I",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/e;",
            ">;",
            "Lcom/airbnb/lottie/c/e;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/e;->x:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/airbnb/lottie/c/c/e;->x:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/airbnb/lottie/c/c/c;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/airbnb/lottie/c/c/c;->a(Lcom/airbnb/lottie/c/e;ILjava/util/List;Lcom/airbnb/lottie/c/e;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
