.class public Lcom/airbnb/lottie/a/a/u;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/airbnb/lottie/a/a/p;
.implements Lcom/airbnb/lottie/a/b/a$a;


# instance fields
.field private final a:Landroid/graphics/Path;

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Lcom/airbnb/lottie/LottieDrawable;

.field private final e:Lcom/airbnb/lottie/a/b/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/airbnb/lottie/a/b/a<",
            "*",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/airbnb/lottie/a/a/c;


# direct methods
.method public constructor <init>(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/c;Lcom/airbnb/lottie/c/b/o;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/airbnb/lottie/a/a/u;->a:Landroid/graphics/Path;

    new-instance v0, Lcom/airbnb/lottie/a/a/c;

    invoke-direct {v0}, Lcom/airbnb/lottie/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/airbnb/lottie/a/a/u;->g:Lcom/airbnb/lottie/a/a/c;

    invoke-virtual {p3}, Lcom/airbnb/lottie/c/b/o;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/airbnb/lottie/a/a/u;->b:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/airbnb/lottie/c/b/o;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/airbnb/lottie/a/a/u;->c:Z

    iput-object p1, p0, Lcom/airbnb/lottie/a/a/u;->d:Lcom/airbnb/lottie/LottieDrawable;

    invoke-virtual {p3}, Lcom/airbnb/lottie/c/b/o;->b()Lcom/airbnb/lottie/c/a/h;

    move-result-object p1

    invoke-virtual {p1}, Lcom/airbnb/lottie/c/a/h;->a()Lcom/airbnb/lottie/a/b/a;

    move-result-object p1

    iput-object p1, p0, Lcom/airbnb/lottie/a/a/u;->e:Lcom/airbnb/lottie/a/b/a;

    iget-object p1, p0, Lcom/airbnb/lottie/a/a/u;->e:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p2, p1}, Lcom/airbnb/lottie/c/c/c;->a(Lcom/airbnb/lottie/a/b/a;)V

    iget-object p1, p0, Lcom/airbnb/lottie/a/a/u;->e:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {p1, p0}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/a/b/a$a;)V

    return-void
.end method

.method private b()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/airbnb/lottie/a/a/u;->f:Z

    iget-object v0, p0, Lcom/airbnb/lottie/a/a/u;->d:Lcom/airbnb/lottie/LottieDrawable;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieDrawable;->invalidateSelf()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lcom/airbnb/lottie/a/a/u;->b()V

    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/a/a/d;",
            ">;",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/a/a/d;",
            ">;)V"
        }
    .end annotation

    const/4 p2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/a/a/d;

    instance-of v1, v0, Lcom/airbnb/lottie/a/a/w;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/airbnb/lottie/a/a/w;

    invoke-virtual {v0}, Lcom/airbnb/lottie/a/a/w;->e()Lcom/airbnb/lottie/c/b/r$a;

    move-result-object v1

    sget-object v2, Lcom/airbnb/lottie/c/b/r$a;->a:Lcom/airbnb/lottie/c/b/r$a;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/airbnb/lottie/a/a/u;->g:Lcom/airbnb/lottie/a/a/c;

    invoke-virtual {v1, v0}, Lcom/airbnb/lottie/a/a/c;->a(Lcom/airbnb/lottie/a/a/w;)V

    invoke-virtual {v0, p0}, Lcom/airbnb/lottie/a/a/w;->a(Lcom/airbnb/lottie/a/b/a$a;)V

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 3

    iget-boolean v0, p0, Lcom/airbnb/lottie/a/a/u;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/airbnb/lottie/a/a/u;->a:Landroid/graphics/Path;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/airbnb/lottie/a/a/u;->a:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-boolean v0, p0, Lcom/airbnb/lottie/a/a/u;->c:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/airbnb/lottie/a/a/u;->f:Z

    iget-object v0, p0, Lcom/airbnb/lottie/a/a/u;->a:Landroid/graphics/Path;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/airbnb/lottie/a/a/u;->a:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/airbnb/lottie/a/a/u;->e:Lcom/airbnb/lottie/a/b/a;

    invoke-virtual {v2}, Lcom/airbnb/lottie/a/b/a;->f()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Path;

    invoke-virtual {v0, v2}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    iget-object v0, p0, Lcom/airbnb/lottie/a/a/u;->a:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v2}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    iget-object v0, p0, Lcom/airbnb/lottie/a/a/u;->g:Lcom/airbnb/lottie/a/a/c;

    iget-object v2, p0, Lcom/airbnb/lottie/a/a/u;->a:Landroid/graphics/Path;

    invoke-virtual {v0, v2}, Lcom/airbnb/lottie/a/a/c;->a(Landroid/graphics/Path;)V

    iput-boolean v1, p0, Lcom/airbnb/lottie/a/a/u;->f:Z

    iget-object v0, p0, Lcom/airbnb/lottie/a/a/u;->a:Landroid/graphics/Path;

    return-object v0
.end method
