.class public abstract Lcom/airbnb/lottie/f/a;
.super Landroid/animation/ValueAnimator;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Landroid/animation/ValueAnimator$AnimatorUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Landroid/animation/Animator$AnimatorListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/airbnb/lottie/f/a;->a:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/airbnb/lottie/f/a;->b:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_3
    return-void

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_5
    if-nez v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_2

    nop

    :goto_6
    goto :goto_1

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    iget-object v0, p0, Lcom/airbnb/lottie/f/a;->b:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_9
    invoke-interface {v1, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    goto/32 :goto_6

    nop

    :goto_a
    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    goto/32 :goto_9

    nop
.end method

.method a(Z)V
    .locals 4

    goto/32 :goto_4

    nop

    :goto_0
    invoke-interface {v1, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    goto/32 :goto_d

    nop

    :goto_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    goto/32 :goto_a

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    goto/32 :goto_f

    nop

    :goto_4
    iget-object v0, p0, Lcom/airbnb/lottie/f/a;->b:Ljava/util/Set;

    goto/32 :goto_2

    nop

    :goto_5
    goto :goto_3

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    if-ge v2, v3, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_10

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_9
    if-nez v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_8

    nop

    :goto_a
    const/16 v3, 0x1a

    goto/32 :goto_7

    nop

    :goto_b
    return-void

    :goto_c
    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    goto/32 :goto_1

    nop

    :goto_d
    goto :goto_3

    :goto_e
    goto/32 :goto_b

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_9

    nop

    :goto_10
    invoke-interface {v1, p0, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;Z)V

    goto/32 :goto_5

    nop
.end method

.method public addListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/f/a;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/f/a;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method b()V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_4

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_2

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_5
    goto :goto_1

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    return-void

    :goto_8
    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-interface {v1, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationRepeat(Landroid/animation/Animator;)V

    goto/32 :goto_5

    nop

    :goto_a
    iget-object v0, p0, Lcom/airbnb/lottie/f/a;->b:Ljava/util/Set;

    goto/32 :goto_0

    nop
.end method

.method b(Z)V
    .locals 4

    goto/32 :goto_c

    nop

    :goto_0
    invoke-interface {v1, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    goto/32 :goto_d

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_2
    const/16 v3, 0x1a

    goto/32 :goto_3

    nop

    :goto_3
    if-ge v2, v3, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_9

    nop

    :goto_4
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    goto/32 :goto_2

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    goto/32 :goto_a

    nop

    :goto_7
    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    goto/32 :goto_4

    nop

    :goto_8
    return-void

    :goto_9
    invoke-interface {v1, p0, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;Z)V

    goto/32 :goto_f

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_b
    if-nez v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_1

    nop

    :goto_c
    iget-object v0, p0, Lcom/airbnb/lottie/f/a;->b:Ljava/util/Set;

    goto/32 :goto_5

    nop

    :goto_d
    goto :goto_6

    :goto_e
    goto/32 :goto_8

    nop

    :goto_f
    goto :goto_6

    :goto_10
    goto/32 :goto_0

    nop
.end method

.method c()V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-interface {v1, p0}, Landroid/animation/ValueAnimator$AnimatorUpdateListener;->onAnimationUpdate(Landroid/animation/ValueAnimator;)V

    goto/32 :goto_6

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    iget-object v0, p0, Lcom/airbnb/lottie/f/a;->a:Ljava/util/Set;

    goto/32 :goto_1

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_6
    goto :goto_2

    :goto_7
    goto/32 :goto_9

    nop

    :goto_8
    check-cast v1, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    goto/32 :goto_0

    nop

    :goto_9
    return-void

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_4

    nop
.end method

.method public getStartDelay()J
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "LottieAnimator does not support getStartDelay."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeAllListeners()V
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/f/a;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public removeAllUpdateListeners()V
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/f/a;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public removeListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/f/a;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/f/a;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public bridge synthetic setDuration(J)Landroid/animation/Animator;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/airbnb/lottie/f/a;->setDuration(J)Landroid/animation/ValueAnimator;

    const/4 p1, 0x0

    throw p1
.end method

.method public setDuration(J)Landroid/animation/ValueAnimator;
    .locals 0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "LottieAnimator does not support setDuration."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setInterpolator(Landroid/animation/TimeInterpolator;)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "LottieAnimator does not support setInterpolator."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setStartDelay(J)V
    .locals 0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "LottieAnimator does not support setStartDelay."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
