.class public Lcom/airbnb/lottie/e/y;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/airbnb/lottie/e/K;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/airbnb/lottie/e/K<",
        "Landroid/graphics/PointF;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/airbnb/lottie/e/y;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/airbnb/lottie/e/y;

    invoke-direct {v0}, Lcom/airbnb/lottie/e/y;-><init>()V

    sput-object v0, Lcom/airbnb/lottie/e/y;->a:Lcom/airbnb/lottie/e/y;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/airbnb/lottie/e/a/c;F)Landroid/graphics/PointF;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/airbnb/lottie/e/q;->a(Lcom/airbnb/lottie/e/a/c;F)Landroid/graphics/PointF;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Lcom/airbnb/lottie/e/a/c;F)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/airbnb/lottie/e/y;->a(Lcom/airbnb/lottie/e/a/c;F)Landroid/graphics/PointF;

    move-result-object p1

    return-object p1
.end method
