.class public final enum Lcom/airbnb/lottie/Q;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/airbnb/lottie/Q;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/airbnb/lottie/Q;

.field public static final enum b:Lcom/airbnb/lottie/Q;

.field public static final enum c:Lcom/airbnb/lottie/Q;

.field private static final synthetic d:[Lcom/airbnb/lottie/Q;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/airbnb/lottie/Q;

    const/4 v1, 0x0

    const-string v2, "AUTOMATIC"

    invoke-direct {v0, v2, v1}, Lcom/airbnb/lottie/Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/airbnb/lottie/Q;->a:Lcom/airbnb/lottie/Q;

    new-instance v0, Lcom/airbnb/lottie/Q;

    const/4 v2, 0x1

    const-string v3, "HARDWARE"

    invoke-direct {v0, v3, v2}, Lcom/airbnb/lottie/Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/airbnb/lottie/Q;->b:Lcom/airbnb/lottie/Q;

    new-instance v0, Lcom/airbnb/lottie/Q;

    const/4 v3, 0x2

    const-string v4, "SOFTWARE"

    invoke-direct {v0, v4, v3}, Lcom/airbnb/lottie/Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/airbnb/lottie/Q;->c:Lcom/airbnb/lottie/Q;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/airbnb/lottie/Q;

    sget-object v4, Lcom/airbnb/lottie/Q;->a:Lcom/airbnb/lottie/Q;

    aput-object v4, v0, v1

    sget-object v1, Lcom/airbnb/lottie/Q;->b:Lcom/airbnb/lottie/Q;

    aput-object v1, v0, v2

    sget-object v1, Lcom/airbnb/lottie/Q;->c:Lcom/airbnb/lottie/Q;

    aput-object v1, v0, v3

    sput-object v0, Lcom/airbnb/lottie/Q;->d:[Lcom/airbnb/lottie/Q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/airbnb/lottie/Q;
    .locals 1

    const-class v0, Lcom/airbnb/lottie/Q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/airbnb/lottie/Q;

    return-object p0
.end method

.method public static values()[Lcom/airbnb/lottie/Q;
    .locals 1

    sget-object v0, Lcom/airbnb/lottie/Q;->d:[Lcom/airbnb/lottie/Q;

    invoke-virtual {v0}, [Lcom/airbnb/lottie/Q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/airbnb/lottie/Q;

    return-object v0
.end method
