.class Lcom/airbnb/lottie/d/b;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/airbnb/lottie/d/b;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/airbnb/lottie/d/b;->b:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/airbnb/lottie/d/b;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    sget-object v2, Lcom/airbnb/lottie/d/a;->a:Lcom/airbnb/lottie/d/a;

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Lcom/airbnb/lottie/d/b;->a(Ljava/lang/String;Lcom/airbnb/lottie/d/a;Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/airbnb/lottie/d/b;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    sget-object v2, Lcom/airbnb/lottie/d/a;->b:Lcom/airbnb/lottie/d/a;

    invoke-static {p1, v2, v3}, Lcom/airbnb/lottie/d/b;->a(Ljava/lang/String;Lcom/airbnb/lottie/d/a;Z)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_1

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private static a(Ljava/lang/String;Lcom/airbnb/lottie/d/a;Z)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "lottie_cache_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\\W+"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/d/a;->a()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    iget-object p0, p1, Lcom/airbnb/lottie/d/a;->d:Ljava/lang/String;

    :goto_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method a()Landroid/util/Pair;
    .locals 5
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair<",
            "Lcom/airbnb/lottie/d/a;",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    goto/32 :goto_1a

    nop

    :goto_0
    invoke-static {v1}, Lcom/airbnb/lottie/f/d;->a(Ljava/lang/String;)V

    goto/32 :goto_19

    nop

    :goto_1
    sget-object v0, Lcom/airbnb/lottie/d/a;->b:Lcom/airbnb/lottie/d/a;

    goto/32 :goto_2

    nop

    :goto_2
    goto :goto_12

    :goto_3
    goto/32 :goto_11

    nop

    :goto_4
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_5
    const-string v3, ".zip"

    goto/32 :goto_8

    nop

    :goto_6
    if-eqz v1, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_13

    nop

    :goto_7
    const-string v4, "Cache hit for "

    goto/32 :goto_15

    nop

    :goto_8
    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_1b

    nop

    :goto_9
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_b
    return-object v0

    :goto_c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop

    :goto_e
    const-string v4, " at "

    goto/32 :goto_c

    nop

    :goto_f
    return-object v1

    :catch_0
    goto/32 :goto_b

    nop

    :goto_10
    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/32 :goto_f

    nop

    :goto_11
    sget-object v0, Lcom/airbnb/lottie/d/a;->a:Lcom/airbnb/lottie/d/a;

    :goto_12
    goto/32 :goto_18

    nop

    :goto_13
    return-object v0

    :goto_14
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_4

    nop

    :goto_15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_16
    iget-object v4, p0, Lcom/airbnb/lottie/d/b;->b:Ljava/lang/String;

    goto/32 :goto_17

    nop

    :goto_17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_e

    nop

    :goto_18
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_19
    new-instance v1, Landroid/util/Pair;

    goto/32 :goto_10

    nop

    :goto_1a
    const/4 v0, 0x0

    :try_start_1
    iget-object v1, p0, Lcom/airbnb/lottie/d/b;->b:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/airbnb/lottie/d/b;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/32 :goto_6

    nop

    :goto_1b
    if-nez v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_1

    nop

    :goto_1c
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop
.end method

.method a(Ljava/io/InputStream;Lcom/airbnb/lottie/d/a;)Ljava/io/File;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_9

    nop

    :goto_0
    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    new-instance p2, Ljava/io/FileOutputStream;

    invoke-direct {p2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto/32 :goto_b

    nop

    :goto_1
    throw p2

    :goto_2
    const/4 v1, 0x1

    goto/32 :goto_4

    nop

    :goto_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    goto/32 :goto_1

    nop

    :goto_4
    invoke-static {v0, p2, v1}, Lcom/airbnb/lottie/d/b;->a(Ljava/lang/String;Lcom/airbnb/lottie/d/a;Z)Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_5

    nop

    :goto_5
    new-instance v0, Ljava/io/File;

    goto/32 :goto_a

    nop

    :goto_6
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p2

    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_8
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    goto/32 :goto_6

    nop

    :goto_9
    iget-object v0, p0, Lcom/airbnb/lottie/d/b;->b:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_a
    iget-object v1, p0, Lcom/airbnb/lottie/d/b;->a:Landroid/content/Context;

    goto/32 :goto_7

    nop

    :goto_b
    const/16 v1, 0x400

    :try_start_2
    new-array v1, v1, [B

    :goto_c
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {p2, v1, v3, v2}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_c

    :cond_0
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/32 :goto_8

    nop
.end method

.method a(Lcom/airbnb/lottie/d/a;)V
    .locals 4

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_1
    invoke-static {v0, p1, v1}, Lcom/airbnb/lottie/d/b;->a(Ljava/lang/String;Lcom/airbnb/lottie/d/a;Z)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_11

    nop

    :goto_2
    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_3
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_15

    nop

    :goto_4
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_5
    invoke-static {v2}, Lcom/airbnb/lottie/f/d;->a(Ljava/lang/String;)V

    goto/32 :goto_d

    nop

    :goto_6
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_7
    const-string v3, ")"

    goto/32 :goto_22

    nop

    :goto_8
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1f

    nop

    :goto_9
    const-string v2, "Unable to rename cache file "

    goto/32 :goto_6

    nop

    :goto_a
    iget-object v0, p0, Lcom/airbnb/lottie/d/b;->b:Ljava/lang/String;

    goto/32 :goto_13

    nop

    :goto_b
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result p1

    goto/32 :goto_17

    nop

    :goto_c
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_d
    if-eqz p1, :cond_0

    goto/32 :goto_20

    :cond_0
    goto/32 :goto_1e

    nop

    :goto_e
    const-string v2, ""

    goto/32 :goto_1b

    nop

    :goto_f
    const-string v3, "Copying temp file to real file ("

    goto/32 :goto_14

    nop

    :goto_10
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1c

    nop

    :goto_11
    new-instance v0, Ljava/io/File;

    goto/32 :goto_27

    nop

    :goto_12
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9

    nop

    :goto_13
    const/4 v1, 0x1

    goto/32 :goto_1

    nop

    :goto_14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_15
    const-string v0, " to "

    goto/32 :goto_10

    nop

    :goto_16
    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto/32 :goto_18

    nop

    :goto_17
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_26

    nop

    :goto_18
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_23

    nop

    :goto_19
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto/32 :goto_b

    nop

    :goto_1a
    return-void

    :goto_1b
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_21

    nop

    :goto_1c
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_25

    nop

    :goto_1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_1e
    new-instance p1, Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_1f
    invoke-static {p1}, Lcom/airbnb/lottie/f/d;->b(Ljava/lang/String;)V

    :goto_20
    goto/32 :goto_1a

    nop

    :goto_21
    new-instance v1, Ljava/io/File;

    goto/32 :goto_19

    nop

    :goto_22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1d

    nop

    :goto_23
    const-string v1, ".temp"

    goto/32 :goto_e

    nop

    :goto_24
    const-string v0, "."

    goto/32 :goto_c

    nop

    :goto_25
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_24

    nop

    :goto_26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_f

    nop

    :goto_27
    iget-object v1, p0, Lcom/airbnb/lottie/d/b;->a:Landroid/content/Context;

    goto/32 :goto_2

    nop
.end method
