.class public Lcom/airbnb/lottie/c/c/g;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/airbnb/lottie/c/c/g$b;,
        Lcom/airbnb/lottie/c/c/g$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/airbnb/lottie/h;

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Lcom/airbnb/lottie/c/c/g$a;

.field private final f:J

.field private final g:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/g;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/airbnb/lottie/c/a/l;

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:F

.field private final n:F

.field private final o:I

.field private final p:I

.field private final q:Lcom/airbnb/lottie/c/a/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final r:Lcom/airbnb/lottie/c/a/k;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final s:Lcom/airbnb/lottie/c/a/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/g/a<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field private final u:Lcom/airbnb/lottie/c/c/g$b;

.field private final v:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/airbnb/lottie/h;Ljava/lang/String;JLcom/airbnb/lottie/c/c/g$a;JLjava/lang/String;Ljava/util/List;Lcom/airbnb/lottie/c/a/l;IIIFFIILcom/airbnb/lottie/c/a/j;Lcom/airbnb/lottie/c/a/k;Ljava/util/List;Lcom/airbnb/lottie/c/c/g$b;Lcom/airbnb/lottie/c/a/b;Z)V
    .locals 3
    .param p9    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p19    # Lcom/airbnb/lottie/c/a/j;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p20    # Lcom/airbnb/lottie/c/a/k;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p23    # Lcom/airbnb/lottie/c/a/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/b;",
            ">;",
            "Lcom/airbnb/lottie/h;",
            "Ljava/lang/String;",
            "J",
            "Lcom/airbnb/lottie/c/c/g$a;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/g;",
            ">;",
            "Lcom/airbnb/lottie/c/a/l;",
            "IIIFFII",
            "Lcom/airbnb/lottie/c/a/j;",
            "Lcom/airbnb/lottie/c/a/k;",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/g/a<",
            "Ljava/lang/Float;",
            ">;>;",
            "Lcom/airbnb/lottie/c/c/g$b;",
            "Lcom/airbnb/lottie/c/a/b;",
            "Z)V"
        }
    .end annotation

    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->a:Ljava/util/List;

    move-object v1, p2

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->b:Lcom/airbnb/lottie/h;

    move-object v1, p3

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->c:Ljava/lang/String;

    move-wide v1, p4

    iput-wide v1, v0, Lcom/airbnb/lottie/c/c/g;->d:J

    move-object v1, p6

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->e:Lcom/airbnb/lottie/c/c/g$a;

    move-wide v1, p7

    iput-wide v1, v0, Lcom/airbnb/lottie/c/c/g;->f:J

    move-object v1, p9

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->g:Ljava/lang/String;

    move-object v1, p10

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->h:Ljava/util/List;

    move-object v1, p11

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->i:Lcom/airbnb/lottie/c/a/l;

    move v1, p12

    iput v1, v0, Lcom/airbnb/lottie/c/c/g;->j:I

    move/from16 v1, p13

    iput v1, v0, Lcom/airbnb/lottie/c/c/g;->k:I

    move/from16 v1, p14

    iput v1, v0, Lcom/airbnb/lottie/c/c/g;->l:I

    move/from16 v1, p15

    iput v1, v0, Lcom/airbnb/lottie/c/c/g;->m:F

    move/from16 v1, p16

    iput v1, v0, Lcom/airbnb/lottie/c/c/g;->n:F

    move/from16 v1, p17

    iput v1, v0, Lcom/airbnb/lottie/c/c/g;->o:I

    move/from16 v1, p18

    iput v1, v0, Lcom/airbnb/lottie/c/c/g;->p:I

    move-object/from16 v1, p19

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->q:Lcom/airbnb/lottie/c/a/j;

    move-object/from16 v1, p20

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->r:Lcom/airbnb/lottie/c/a/k;

    move-object/from16 v1, p21

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->t:Ljava/util/List;

    move-object/from16 v1, p22

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->u:Lcom/airbnb/lottie/c/c/g$b;

    move-object/from16 v1, p23

    iput-object v1, v0, Lcom/airbnb/lottie/c/c/g;->s:Lcom/airbnb/lottie/c/a/b;

    move/from16 v1, p24

    iput-boolean v1, v0, Lcom/airbnb/lottie/c/c/g;->v:Z

    return-void
.end method


# virtual methods
.method a()Lcom/airbnb/lottie/h;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->b:Lcom/airbnb/lottie/h;

    goto/32 :goto_0

    nop
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/airbnb/lottie/c/c/g;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/airbnb/lottie/c/c/g;->b:Lcom/airbnb/lottie/h;

    invoke-virtual {p0}, Lcom/airbnb/lottie/c/c/g;->h()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/airbnb/lottie/h;->a(J)Lcom/airbnb/lottie/c/c/g;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "\t\tParents: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/airbnb/lottie/c/c/g;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/g;->b:Lcom/airbnb/lottie/h;

    invoke-virtual {v2}, Lcom/airbnb/lottie/c/c/g;->h()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/airbnb/lottie/h;->a(J)Lcom/airbnb/lottie/c/c/g;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_0

    const-string v3, "->"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/airbnb/lottie/c/c/g;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/airbnb/lottie/c/c/g;->b:Lcom/airbnb/lottie/h;

    invoke-virtual {v2}, Lcom/airbnb/lottie/c/c/g;->h()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/airbnb/lottie/h;->a(J)Lcom/airbnb/lottie/c/c/g;

    move-result-object v2

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p0}, Lcom/airbnb/lottie/c/c/g;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\tMasks: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/airbnb/lottie/c/c/g;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {p0}, Lcom/airbnb/lottie/c/c/g;->o()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/airbnb/lottie/c/c/g;->n()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\tBackground: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/airbnb/lottie/c/c/g;->o()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/airbnb/lottie/c/c/g;->n()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/airbnb/lottie/c/c/g;->m()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v4, "%dx%d %X\n"

    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v2, p0, Lcom/airbnb/lottie/c/c/g;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\tShapes:\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/airbnb/lottie/c/c/g;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\t\t"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/airbnb/lottie/c/c/g;->d:J

    return-wide v0
.end method

.method c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/g/a<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->t:Ljava/util/List;

    goto/32 :goto_0

    nop
.end method

.method public d()Lcom/airbnb/lottie/c/c/g$a;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->e:Lcom/airbnb/lottie/c/c/g$a;

    return-object v0
.end method

.method e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/g;",
            ">;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->h:Ljava/util/List;

    goto/32 :goto_0

    nop
.end method

.method f()Lcom/airbnb/lottie/c/c/g$b;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->u:Lcom/airbnb/lottie/c/c/g$b;

    goto/32 :goto_0

    nop
.end method

.method g()Ljava/lang/String;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->c:Ljava/lang/String;

    goto/32 :goto_0

    nop
.end method

.method h()J
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-wide v0, p0, Lcom/airbnb/lottie/c/c/g;->f:J

    goto/32 :goto_1

    nop

    :goto_1
    return-wide v0
.end method

.method i()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lcom/airbnb/lottie/c/c/g;->p:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method j()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget v0, p0, Lcom/airbnb/lottie/c/c/g;->o:I

    goto/32 :goto_0

    nop
.end method

.method k()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->g:Ljava/lang/String;

    goto/32 :goto_0

    nop
.end method

.method l()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/b/b;",
            ">;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->a:Ljava/util/List;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method m()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget v0, p0, Lcom/airbnb/lottie/c/c/g;->l:I

    goto/32 :goto_0

    nop
.end method

.method n()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lcom/airbnb/lottie/c/c/g;->k:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method o()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget v0, p0, Lcom/airbnb/lottie/c/c/g;->j:I

    goto/32 :goto_0

    nop
.end method

.method p()F
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v1, p0, Lcom/airbnb/lottie/c/c/g;->b:Lcom/airbnb/lottie/h;

    goto/32 :goto_3

    nop

    :goto_1
    iget v0, p0, Lcom/airbnb/lottie/c/c/g;->n:F

    goto/32 :goto_0

    nop

    :goto_2
    div-float/2addr v0, v1

    goto/32 :goto_4

    nop

    :goto_3
    invoke-virtual {v1}, Lcom/airbnb/lottie/h;->d()F

    move-result v1

    goto/32 :goto_2

    nop

    :goto_4
    return v0
.end method

.method q()Lcom/airbnb/lottie/c/a/j;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->q:Lcom/airbnb/lottie/c/a/j;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method r()Lcom/airbnb/lottie/c/a/k;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->r:Lcom/airbnb/lottie/c/a/k;

    goto/32 :goto_0

    nop
.end method

.method s()Lcom/airbnb/lottie/c/a/b;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->s:Lcom/airbnb/lottie/c/a/b;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method t()F
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lcom/airbnb/lottie/c/c/g;->m:F

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/airbnb/lottie/c/c/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method u()Lcom/airbnb/lottie/c/a/l;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/g;->i:Lcom/airbnb/lottie/c/a/l;

    goto/32 :goto_0

    nop
.end method

.method public v()Z
    .locals 1

    iget-boolean v0, p0, Lcom/airbnb/lottie/c/c/g;->v:Z

    return v0
.end method
