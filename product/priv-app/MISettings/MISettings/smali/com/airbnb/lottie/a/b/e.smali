.class public Lcom/airbnb/lottie/a/b/e;
.super Lcom/airbnb/lottie/a/b/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/airbnb/lottie/a/b/f<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/g/a<",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/airbnb/lottie/a/b/f;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method a(Lcom/airbnb/lottie/g/a;F)Ljava/lang/Integer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/g/a<",
            "Ljava/lang/Integer;",
            ">;F)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    return-object p1

    :goto_2
    invoke-virtual {p0, p1, p2}, Lcom/airbnb/lottie/a/b/e;->b(Lcom/airbnb/lottie/g/a;F)I

    move-result p1

    goto/32 :goto_0

    nop
.end method

.method bridge synthetic a(Lcom/airbnb/lottie/g/a;F)Ljava/lang/Object;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/airbnb/lottie/a/b/e;->a(Lcom/airbnb/lottie/g/a;F)Ljava/lang/Integer;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    return-object p1
.end method

.method b(Lcom/airbnb/lottie/g/a;F)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/g/a<",
            "Ljava/lang/Integer;",
            ">;F)I"
        }
    .end annotation

    goto/32 :goto_15

    nop

    :goto_0
    iget-object v5, p1, Lcom/airbnb/lottie/g/a;->c:Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->e()F

    move-result v8

    goto/32 :goto_5

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_6

    nop

    :goto_3
    if-nez v1, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_18

    nop

    :goto_4
    new-instance p1, Ljava/lang/IllegalStateException;

    goto/32 :goto_f

    nop

    :goto_5
    move v6, p2

    goto/32 :goto_19

    nop

    :goto_6
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto/32 :goto_12

    nop

    :goto_7
    iget-object v4, p1, Lcom/airbnb/lottie/g/a;->b:Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_8
    invoke-virtual {p1}, Lcom/airbnb/lottie/g/a;->f()I

    move-result v0

    goto/32 :goto_14

    nop

    :goto_9
    return p1

    :goto_a
    goto/32 :goto_4

    nop

    :goto_b
    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->d()F

    move-result v7

    goto/32 :goto_1

    nop

    :goto_c
    check-cast v0, Ljava/lang/Integer;

    goto/32 :goto_2

    nop

    :goto_d
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    goto/32 :goto_7

    nop

    :goto_e
    if-nez v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_16

    nop

    :goto_f
    const-string p2, "Missing values for keyframe."

    goto/32 :goto_10

    nop

    :goto_10
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1b

    nop

    :goto_11
    if-nez v0, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_1c

    nop

    :goto_12
    return p1

    :goto_13
    goto/32 :goto_8

    nop

    :goto_14
    invoke-virtual {p1}, Lcom/airbnb/lottie/g/a;->c()I

    move-result p1

    goto/32 :goto_17

    nop

    :goto_15
    iget-object v0, p1, Lcom/airbnb/lottie/g/a;->b:Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_16
    iget-object v1, p0, Lcom/airbnb/lottie/a/b/a;->e:Lcom/airbnb/lottie/g/c;

    goto/32 :goto_3

    nop

    :goto_17
    invoke-static {v0, p1, p2}, Lcom/airbnb/lottie/f/g;->a(IIF)I

    move-result p1

    goto/32 :goto_9

    nop

    :goto_18
    iget v2, p1, Lcom/airbnb/lottie/g/a;->e:F

    goto/32 :goto_1a

    nop

    :goto_19
    invoke-virtual/range {v1 .. v8}, Lcom/airbnb/lottie/g/c;->a(FFLjava/lang/Object;Ljava/lang/Object;FFF)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_1a
    iget-object v0, p1, Lcom/airbnb/lottie/g/a;->f:Ljava/lang/Float;

    goto/32 :goto_d

    nop

    :goto_1b
    throw p1

    :goto_1c
    iget-object v0, p1, Lcom/airbnb/lottie/g/a;->c:Ljava/lang/Object;

    goto/32 :goto_e

    nop
.end method

.method public i()I
    .locals 2

    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->a()Lcom/airbnb/lottie/g/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->c()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/airbnb/lottie/a/b/e;->b(Lcom/airbnb/lottie/g/a;F)I

    move-result v0

    return v0
.end method
