.class public final enum Lcom/airbnb/lottie/c/b/q$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/airbnb/lottie/c/b/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/airbnb/lottie/c/b/q$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/airbnb/lottie/c/b/q$b;

.field public static final enum b:Lcom/airbnb/lottie/c/b/q$b;

.field public static final enum c:Lcom/airbnb/lottie/c/b/q$b;

.field private static final synthetic d:[Lcom/airbnb/lottie/c/b/q$b;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/airbnb/lottie/c/b/q$b;

    const/4 v1, 0x0

    const-string v2, "MITER"

    invoke-direct {v0, v2, v1}, Lcom/airbnb/lottie/c/b/q$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/airbnb/lottie/c/b/q$b;->a:Lcom/airbnb/lottie/c/b/q$b;

    new-instance v0, Lcom/airbnb/lottie/c/b/q$b;

    const/4 v2, 0x1

    const-string v3, "ROUND"

    invoke-direct {v0, v3, v2}, Lcom/airbnb/lottie/c/b/q$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/airbnb/lottie/c/b/q$b;->b:Lcom/airbnb/lottie/c/b/q$b;

    new-instance v0, Lcom/airbnb/lottie/c/b/q$b;

    const/4 v3, 0x2

    const-string v4, "BEVEL"

    invoke-direct {v0, v4, v3}, Lcom/airbnb/lottie/c/b/q$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/airbnb/lottie/c/b/q$b;->c:Lcom/airbnb/lottie/c/b/q$b;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/airbnb/lottie/c/b/q$b;

    sget-object v4, Lcom/airbnb/lottie/c/b/q$b;->a:Lcom/airbnb/lottie/c/b/q$b;

    aput-object v4, v0, v1

    sget-object v1, Lcom/airbnb/lottie/c/b/q$b;->b:Lcom/airbnb/lottie/c/b/q$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/airbnb/lottie/c/b/q$b;->c:Lcom/airbnb/lottie/c/b/q$b;

    aput-object v1, v0, v3

    sput-object v0, Lcom/airbnb/lottie/c/b/q$b;->d:[Lcom/airbnb/lottie/c/b/q$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/airbnb/lottie/c/b/q$b;
    .locals 1

    const-class v0, Lcom/airbnb/lottie/c/b/q$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/airbnb/lottie/c/b/q$b;

    return-object p0
.end method

.method public static values()[Lcom/airbnb/lottie/c/b/q$b;
    .locals 1

    sget-object v0, Lcom/airbnb/lottie/c/b/q$b;->d:[Lcom/airbnb/lottie/c/b/q$b;

    invoke-virtual {v0}, [Lcom/airbnb/lottie/c/b/q$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/airbnb/lottie/c/b/q$b;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/graphics/Paint$Join;
    .locals 2

    sget-object v0, Lcom/airbnb/lottie/c/b/p;->b:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    return-object v0

    :cond_1
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    return-object v0

    :cond_2
    sget-object v0, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    return-object v0
.end method
