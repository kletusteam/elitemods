.class public abstract Lcom/airbnb/lottie/a/b/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/airbnb/lottie/a/b/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "A:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/a/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/airbnb/lottie/g/a<",
            "TK;>;>;"
        }
    .end annotation
.end field

.field private d:F

.field protected e:Lcom/airbnb/lottie/g/c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/airbnb/lottie/g/c<",
            "TA;>;"
        }
    .end annotation
.end field

.field private f:Lcom/airbnb/lottie/g/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/airbnb/lottie/g/a<",
            "TK;>;"
        }
    .end annotation
.end field

.field private g:Lcom/airbnb/lottie/g/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/airbnb/lottie/g/a<",
            "TK;>;"
        }
    .end annotation
.end field

.field private h:F

.field private i:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TA;"
        }
    .end annotation
.end field

.field private j:F

.field private k:F


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/airbnb/lottie/g/a<",
            "TK;>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/airbnb/lottie/a/b/a;->a:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/airbnb/lottie/a/b/a;->b:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/airbnb/lottie/a/b/a;->d:F

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/airbnb/lottie/a/b/a;->h:F

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/airbnb/lottie/a/b/a;->i:Ljava/lang/Object;

    iput v0, p0, Lcom/airbnb/lottie/a/b/a;->j:F

    iput v0, p0, Lcom/airbnb/lottie/a/b/a;->k:F

    iput-object p1, p0, Lcom/airbnb/lottie/a/b/a;->c:Ljava/util/List;

    return-void
.end method

.method private i()F
    .locals 2
    .annotation build Landroidx/annotation/FloatRange;
        from = 0.0
        to = 1.0
    .end annotation

    iget v0, p0, Lcom/airbnb/lottie/a/b/a;->j:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/g/a;

    invoke-virtual {v0}, Lcom/airbnb/lottie/g/a;->d()F

    move-result v0

    :goto_0
    iput v0, p0, Lcom/airbnb/lottie/a/b/a;->j:F

    :cond_1
    iget v0, p0, Lcom/airbnb/lottie/a/b/a;->j:F

    return v0
.end method


# virtual methods
.method protected a()Lcom/airbnb/lottie/g/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/airbnb/lottie/g/a<",
            "TK;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->f:Lcom/airbnb/lottie/g/a;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/airbnb/lottie/a/b/a;->d:F

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/g/a;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->f:Lcom/airbnb/lottie/g/a;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/g/a;

    iget v1, p0, Lcom/airbnb/lottie/a/b/a;->d:F

    invoke-virtual {v0}, Lcom/airbnb/lottie/g/a;->d()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    iget-object v1, p0, Lcom/airbnb/lottie/a/b/a;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/g/a;

    iget v2, p0, Lcom/airbnb/lottie/a/b/a;->d:F

    invoke-virtual {v0, v2}, Lcom/airbnb/lottie/g/a;->a(F)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    :goto_1
    iput-object v0, p0, Lcom/airbnb/lottie/a/b/a;->f:Lcom/airbnb/lottie/g/a;

    return-object v0
.end method

.method abstract a(Lcom/airbnb/lottie/g/a;F)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/g/a<",
            "TK;>;F)TA;"
        }
    .end annotation
.end method

.method public a(F)V
    .locals 2
    .param p1    # F
        .annotation build Landroidx/annotation/FloatRange;
            from = 0.0
            to = 1.0
        .end annotation
    .end param

    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->a()Lcom/airbnb/lottie/g/a;

    move-result-object v0

    invoke-direct {p0}, Lcom/airbnb/lottie/a/b/a;->i()F

    move-result v1

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    invoke-direct {p0}, Lcom/airbnb/lottie/a/b/a;->i()F

    move-result p1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->b()F

    move-result v1

    cmpl-float v1, p1, v1

    if-lez v1, :cond_2

    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->b()F

    move-result p1

    :cond_2
    :goto_0
    iget v1, p0, Lcom/airbnb/lottie/a/b/a;->d:F

    cmpl-float v1, p1, v1

    if-nez v1, :cond_3

    return-void

    :cond_3
    iput p1, p0, Lcom/airbnb/lottie/a/b/a;->d:F

    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->a()Lcom/airbnb/lottie/g/a;

    move-result-object p1

    if-ne v0, p1, :cond_4

    invoke-virtual {p1}, Lcom/airbnb/lottie/g/a;->g()Z

    move-result p1

    if-nez p1, :cond_5

    :cond_4
    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->g()V

    :cond_5
    return-void
.end method

.method public a(Lcom/airbnb/lottie/a/b/a$a;)V
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lcom/airbnb/lottie/g/c;)V
    .locals 2
    .param p1    # Lcom/airbnb/lottie/g/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/g/c<",
            "TA;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->e:Lcom/airbnb/lottie/g/c;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/g/c;->a(Lcom/airbnb/lottie/a/b/a;)V

    :cond_0
    iput-object p1, p0, Lcom/airbnb/lottie/a/b/a;->e:Lcom/airbnb/lottie/g/c;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p0}, Lcom/airbnb/lottie/g/c;->a(Lcom/airbnb/lottie/a/b/a;)V

    :cond_1
    return-void
.end method

.method b()F
    .locals 2
    .annotation build Landroidx/annotation/FloatRange;
        from = 0.0
        to = 1.0
    .end annotation

    goto/32 :goto_13

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_12

    nop

    :goto_1
    iput v0, p0, Lcom/airbnb/lottie/a/b/a;->k:F

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_d

    nop

    :goto_4
    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->c:Ljava/util/List;

    goto/32 :goto_c

    nop

    :goto_5
    iget v0, p0, Lcom/airbnb/lottie/a/b/a;->k:F

    goto/32 :goto_b

    nop

    :goto_6
    if-eqz v0, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_a

    nop

    :goto_7
    goto :goto_11

    :goto_8
    goto/32 :goto_4

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_a
    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->c:Ljava/util/List;

    goto/32 :goto_9

    nop

    :goto_b
    return v0

    :goto_c
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_3

    nop

    :goto_d
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_e
    const/high16 v1, -0x40800000    # -1.0f

    goto/32 :goto_f

    nop

    :goto_f
    cmpl-float v0, v0, v1

    goto/32 :goto_6

    nop

    :goto_10
    invoke-virtual {v0}, Lcom/airbnb/lottie/g/a;->a()F

    move-result v0

    :goto_11
    goto/32 :goto_1

    nop

    :goto_12
    const/high16 v0, 0x3f800000    # 1.0f

    goto/32 :goto_7

    nop

    :goto_13
    iget v0, p0, Lcom/airbnb/lottie/a/b/a;->k:F

    goto/32 :goto_e

    nop

    :goto_14
    check-cast v0, Lcom/airbnb/lottie/g/a;

    goto/32 :goto_10

    nop
.end method

.method protected c()F
    .locals 2

    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->a()Lcom/airbnb/lottie/g/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/airbnb/lottie/g/a;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, v0, Lcom/airbnb/lottie/g/a;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->d()F

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    return v0
.end method

.method d()F
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    return v1

    :goto_1
    iget-boolean v0, p0, Lcom/airbnb/lottie/a/b/a;->b:Z

    goto/32 :goto_c

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_3
    return v1

    :goto_4
    goto/32 :goto_b

    nop

    :goto_5
    return v1

    :goto_6
    goto/32 :goto_f

    nop

    :goto_7
    if-nez v2, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_3

    nop

    :goto_8
    sub-float/2addr v2, v0

    goto/32 :goto_10

    nop

    :goto_9
    sub-float/2addr v1, v2

    goto/32 :goto_e

    nop

    :goto_a
    invoke-virtual {v0}, Lcom/airbnb/lottie/g/a;->g()Z

    move-result v2

    goto/32 :goto_7

    nop

    :goto_b
    iget v1, p0, Lcom/airbnb/lottie/a/b/a;->d:F

    goto/32 :goto_11

    nop

    :goto_c
    const/4 v1, 0x0

    goto/32 :goto_2

    nop

    :goto_d
    invoke-virtual {v0}, Lcom/airbnb/lottie/g/a;->d()F

    move-result v0

    goto/32 :goto_8

    nop

    :goto_e
    invoke-virtual {v0}, Lcom/airbnb/lottie/g/a;->a()F

    move-result v2

    goto/32 :goto_d

    nop

    :goto_f
    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->a()Lcom/airbnb/lottie/g/a;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_10
    div-float/2addr v1, v2

    goto/32 :goto_0

    nop

    :goto_11
    invoke-virtual {v0}, Lcom/airbnb/lottie/g/a;->d()F

    move-result v2

    goto/32 :goto_9

    nop
.end method

.method public e()F
    .locals 1

    iget v0, p0, Lcom/airbnb/lottie/a/b/a;->d:F

    return v0
.end method

.method public f()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TA;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->a()Lcom/airbnb/lottie/g/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/airbnb/lottie/a/b/a;->c()F

    move-result v1

    iget-object v2, p0, Lcom/airbnb/lottie/a/b/a;->e:Lcom/airbnb/lottie/g/c;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/airbnb/lottie/a/b/a;->g:Lcom/airbnb/lottie/g/a;

    if-ne v0, v2, :cond_0

    iget v2, p0, Lcom/airbnb/lottie/a/b/a;->h:F

    cmpl-float v2, v2, v1

    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/airbnb/lottie/a/b/a;->i:Ljava/lang/Object;

    return-object v0

    :cond_0
    iput-object v0, p0, Lcom/airbnb/lottie/a/b/a;->g:Lcom/airbnb/lottie/g/a;

    iput v1, p0, Lcom/airbnb/lottie/a/b/a;->h:F

    invoke-virtual {p0, v0, v1}, Lcom/airbnb/lottie/a/b/a;->a(Lcom/airbnb/lottie/g/a;F)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/airbnb/lottie/a/b/a;->i:Ljava/lang/Object;

    return-object v0
.end method

.method public g()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/airbnb/lottie/a/b/a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/airbnb/lottie/a/b/a;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/airbnb/lottie/a/b/a$a;

    invoke-interface {v1}, Lcom/airbnb/lottie/a/b/a$a;->a()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/airbnb/lottie/a/b/a;->b:Z

    return-void
.end method
