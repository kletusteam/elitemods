.class public abstract Lcom/airbnb/lottie/e/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/airbnb/lottie/e/a/c$b;,
        Lcom/airbnb/lottie/e/a/c$a;
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field b:I

.field c:[I

.field d:[Ljava/lang/String;

.field e:[I

.field f:Z

.field g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x80

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/airbnb/lottie/e/a/c;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/16 v2, 0x1f

    if-gt v1, v2, :cond_0

    sget-object v2, Lcom/airbnb/lottie/e/a/c;->a:[Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const-string v4, "\\u%04x"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/airbnb/lottie/e/a/c;->a:[Ljava/lang/String;

    const/16 v1, 0x22

    const-string v2, "\\\""

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "\\\\"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\\t"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "\\b"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "\\n"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "\\r"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "\\f"

    aput-object v2, v0, v1

    return-void
.end method

.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x20

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/airbnb/lottie/e/a/c;->c:[I

    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, Lcom/airbnb/lottie/e/a/c;->d:[Ljava/lang/String;

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/airbnb/lottie/e/a/c;->e:[I

    return-void
.end method

.method public static a(Lf/k;)Lcom/airbnb/lottie/e/a/c;
    .locals 1

    new-instance v0, Lcom/airbnb/lottie/e/a/e;

    invoke-direct {v0, p0}, Lcom/airbnb/lottie/e/a/e;-><init>(Lf/k;)V

    return-object v0
.end method

.method static synthetic a(Lf/j;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/airbnb/lottie/e/a/c;->b(Lf/j;Ljava/lang/String;)V

    return-void
.end method

.method private static b(Lf/j;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/airbnb/lottie/e/a/c;->a:[Ljava/lang/String;

    const/16 v1, 0x22

    invoke-interface {p0, v1}, Lf/j;->writeByte(I)Lf/j;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v3, v2, :cond_5

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x80

    if-ge v5, v6, :cond_0

    aget-object v5, v0, v5

    if-nez v5, :cond_2

    goto :goto_2

    :cond_0
    const/16 v6, 0x2028

    if-ne v5, v6, :cond_1

    const-string v5, "\\u2028"

    goto :goto_1

    :cond_1
    const/16 v6, 0x2029

    if-ne v5, v6, :cond_4

    const-string v5, "\\u2029"

    :cond_2
    :goto_1
    if-ge v4, v3, :cond_3

    invoke-interface {p0, p1, v4, v3}, Lf/j;->a(Ljava/lang/String;II)Lf/j;

    :cond_3
    invoke-interface {p0, v5}, Lf/j;->a(Ljava/lang/String;)Lf/j;

    add-int/lit8 v4, v3, 0x1

    :cond_4
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    if-ge v4, v2, :cond_6

    invoke-interface {p0, p1, v4, v2}, Lf/j;->a(Ljava/lang/String;II)Lf/j;

    :cond_6
    invoke-interface {p0, v1}, Lf/j;->writeByte(I)Lf/j;

    return-void
.end method


# virtual methods
.method public abstract a(Lcom/airbnb/lottie/e/a/c$a;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract a()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract b()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method final b(I)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    iget-object v1, p0, Lcom/airbnb/lottie/e/a/c;->c:[I

    goto/32 :goto_f

    nop

    :goto_1
    array-length v1, v0

    goto/32 :goto_1f

    nop

    :goto_2
    iget v0, p0, Lcom/airbnb/lottie/e/a/c;->b:I

    goto/32 :goto_0

    nop

    :goto_3
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1b

    nop

    :goto_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_6
    iput v2, p0, Lcom/airbnb/lottie/e/a/c;->b:I

    goto/32 :goto_20

    nop

    :goto_7
    new-instance p1, Lcom/airbnb/lottie/e/a/a;

    goto/32 :goto_22

    nop

    :goto_8
    iget-object v0, p0, Lcom/airbnb/lottie/e/a/c;->c:[I

    goto/32 :goto_16

    nop

    :goto_9
    throw p1

    :goto_a
    goto/32 :goto_8

    nop

    :goto_b
    return-void

    :goto_c
    mul-int/lit8 v1, v1, 0x2

    goto/32 :goto_21

    nop

    :goto_d
    invoke-direct {p1, v0}, Lcom/airbnb/lottie/e/a/a;-><init>(Ljava/lang/String;)V

    goto/32 :goto_9

    nop

    :goto_e
    iput-object v0, p0, Lcom/airbnb/lottie/e/a/c;->c:[I

    goto/32 :goto_10

    nop

    :goto_f
    array-length v2, v1

    goto/32 :goto_17

    nop

    :goto_10
    iget-object v0, p0, Lcom/airbnb/lottie/e/a/c;->d:[Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_11
    iput-object v0, p0, Lcom/airbnb/lottie/e/a/c;->d:[Ljava/lang/String;

    goto/32 :goto_18

    nop

    :goto_12
    goto :goto_a

    :goto_13
    goto/32 :goto_7

    nop

    :goto_14
    array-length v0, v1

    goto/32 :goto_26

    nop

    :goto_15
    const-string v1, "Nesting too deep at "

    goto/32 :goto_5

    nop

    :goto_16
    iget v1, p0, Lcom/airbnb/lottie/e/a/c;->b:I

    goto/32 :goto_25

    nop

    :goto_17
    if-eq v0, v2, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_1e

    nop

    :goto_18
    iget-object v0, p0, Lcom/airbnb/lottie/e/a/c;->e:[I

    goto/32 :goto_23

    nop

    :goto_19
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_1a
    invoke-virtual {p0}, Lcom/airbnb/lottie/e/a/c;->getPath()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_27

    nop

    :goto_1b
    check-cast v0, [Ljava/lang/String;

    goto/32 :goto_11

    nop

    :goto_1c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_15

    nop

    :goto_1d
    iput-object v0, p0, Lcom/airbnb/lottie/e/a/c;->e:[I

    goto/32 :goto_12

    nop

    :goto_1e
    const/16 v2, 0x100

    goto/32 :goto_24

    nop

    :goto_1f
    mul-int/lit8 v1, v1, 0x2

    goto/32 :goto_3

    nop

    :goto_20
    aput p1, v0, v1

    goto/32 :goto_b

    nop

    :goto_21
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_22
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_1c

    nop

    :goto_23
    array-length v1, v0

    goto/32 :goto_c

    nop

    :goto_24
    if-ne v0, v2, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_14

    nop

    :goto_25
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_6

    nop

    :goto_26
    mul-int/lit8 v0, v0, 0x2

    goto/32 :goto_19

    nop

    :goto_27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop
.end method

.method final e(Ljava/lang/String;)Lcom/airbnb/lottie/e/a/b;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/airbnb/lottie/e/a/b;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    new-instance v0, Lcom/airbnb/lottie/e/a/b;

    goto/32 :goto_9

    nop

    :goto_1
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4

    nop

    :goto_2
    throw v0

    :goto_3
    invoke-direct {v0, p1}, Lcom/airbnb/lottie/e/a/b;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_4
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_5
    const-string p1, " at path "

    goto/32 :goto_7

    nop

    :goto_6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_8
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_9
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_a
    invoke-virtual {p0}, Lcom/airbnb/lottie/e/a/c;->getPath()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_8

    nop
.end method

.method public final getPath()Ljava/lang/String;
    .locals 4

    iget v0, p0, Lcom/airbnb/lottie/e/a/c;->b:I

    iget-object v1, p0, Lcom/airbnb/lottie/e/a/c;->c:[I

    iget-object v2, p0, Lcom/airbnb/lottie/e/a/c;->d:[Ljava/lang/String;

    iget-object v3, p0, Lcom/airbnb/lottie/e/a/c;->e:[I

    invoke-static {v0, v1, v2, v3}, Lcom/airbnb/lottie/e/a/d;->a(I[I[Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract p()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract q()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract r()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract s()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract t()D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract u()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract v()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract w()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract x()Lcom/airbnb/lottie/e/a/c$b;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract y()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract z()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
