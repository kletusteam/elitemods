.class public Lcom/airbnb/lottie/c/c/i;
.super Lcom/airbnb/lottie/c/c/c;


# instance fields
.field private final w:Lcom/airbnb/lottie/a/a/e;


# direct methods
.method constructor <init>(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/g;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/airbnb/lottie/c/c/c;-><init>(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/g;)V

    new-instance v0, Lcom/airbnb/lottie/c/b/n;

    invoke-virtual {p2}, Lcom/airbnb/lottie/c/c/g;->l()Ljava/util/List;

    move-result-object p2

    const-string v1, "__container"

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, v2}, Lcom/airbnb/lottie/c/b/n;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    new-instance p2, Lcom/airbnb/lottie/a/a/e;

    invoke-direct {p2, p1, p0, v0}, Lcom/airbnb/lottie/a/a/e;-><init>(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/c;Lcom/airbnb/lottie/c/b/n;)V

    iput-object p2, p0, Lcom/airbnb/lottie/c/c/i;->w:Lcom/airbnb/lottie/a/a/e;

    iget-object p1, p0, Lcom/airbnb/lottie/c/c/i;->w:Lcom/airbnb/lottie/a/a/e;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/airbnb/lottie/a/a/e;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lcom/airbnb/lottie/c/c/c;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object p2, p0, Lcom/airbnb/lottie/c/c/i;->w:Lcom/airbnb/lottie/a/a/e;

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/c;->m:Landroid/graphics/Matrix;

    invoke-virtual {p2, p1, v0, p3}, Lcom/airbnb/lottie/a/a/e;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    return-void
.end method

.method b(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/airbnb/lottie/a/a/e;->a(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lcom/airbnb/lottie/c/c/i;->w:Lcom/airbnb/lottie/a/a/e;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method protected b(Lcom/airbnb/lottie/c/e;ILjava/util/List;Lcom/airbnb/lottie/c/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/c/e;",
            "I",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/e;",
            ">;",
            "Lcom/airbnb/lottie/c/e;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/airbnb/lottie/c/c/i;->w:Lcom/airbnb/lottie/a/a/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/airbnb/lottie/a/a/e;->a(Lcom/airbnb/lottie/c/e;ILjava/util/List;Lcom/airbnb/lottie/c/e;)V

    return-void
.end method
