.class public Lcom/airbnb/lottie/c/b/q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/airbnb/lottie/c/b/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/airbnb/lottie/c/b/q$b;,
        Lcom/airbnb/lottie/c/b/q$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/airbnb/lottie/c/a/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/airbnb/lottie/c/a/a;

.field private final e:Lcom/airbnb/lottie/c/a/d;

.field private final f:Lcom/airbnb/lottie/c/a/b;

.field private final g:Lcom/airbnb/lottie/c/b/q$a;

.field private final h:Lcom/airbnb/lottie/c/b/q$b;

.field private final i:F

.field private final j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/airbnb/lottie/c/a/b;Ljava/util/List;Lcom/airbnb/lottie/c/a/a;Lcom/airbnb/lottie/c/a/d;Lcom/airbnb/lottie/c/a/b;Lcom/airbnb/lottie/c/b/q$a;Lcom/airbnb/lottie/c/b/q$b;FZ)V
    .locals 0
    .param p2    # Lcom/airbnb/lottie/c/a/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/airbnb/lottie/c/a/b;",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/a/b;",
            ">;",
            "Lcom/airbnb/lottie/c/a/a;",
            "Lcom/airbnb/lottie/c/a/d;",
            "Lcom/airbnb/lottie/c/a/b;",
            "Lcom/airbnb/lottie/c/b/q$a;",
            "Lcom/airbnb/lottie/c/b/q$b;",
            "FZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/airbnb/lottie/c/b/q;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/airbnb/lottie/c/b/q;->b:Lcom/airbnb/lottie/c/a/b;

    iput-object p3, p0, Lcom/airbnb/lottie/c/b/q;->c:Ljava/util/List;

    iput-object p4, p0, Lcom/airbnb/lottie/c/b/q;->d:Lcom/airbnb/lottie/c/a/a;

    iput-object p5, p0, Lcom/airbnb/lottie/c/b/q;->e:Lcom/airbnb/lottie/c/a/d;

    iput-object p6, p0, Lcom/airbnb/lottie/c/b/q;->f:Lcom/airbnb/lottie/c/a/b;

    iput-object p7, p0, Lcom/airbnb/lottie/c/b/q;->g:Lcom/airbnb/lottie/c/b/q$a;

    iput-object p8, p0, Lcom/airbnb/lottie/c/b/q;->h:Lcom/airbnb/lottie/c/b/q$b;

    iput p9, p0, Lcom/airbnb/lottie/c/b/q;->i:F

    iput-boolean p10, p0, Lcom/airbnb/lottie/c/b/q;->j:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/c;)Lcom/airbnb/lottie/a/a/d;
    .locals 1

    new-instance v0, Lcom/airbnb/lottie/a/a/v;

    invoke-direct {v0, p1, p2, p0}, Lcom/airbnb/lottie/a/a/v;-><init>(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/c;Lcom/airbnb/lottie/c/b/q;)V

    return-object v0
.end method

.method public a()Lcom/airbnb/lottie/c/b/q$a;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/q;->g:Lcom/airbnb/lottie/c/b/q$a;

    return-object v0
.end method

.method public b()Lcom/airbnb/lottie/c/a/a;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/q;->d:Lcom/airbnb/lottie/c/a/a;

    return-object v0
.end method

.method public c()Lcom/airbnb/lottie/c/a/b;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/q;->b:Lcom/airbnb/lottie/c/a/b;

    return-object v0
.end method

.method public d()Lcom/airbnb/lottie/c/b/q$b;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/q;->h:Lcom/airbnb/lottie/c/b/q$b;

    return-object v0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/c/a/b;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/q;->c:Ljava/util/List;

    return-object v0
.end method

.method public f()F
    .locals 1

    iget v0, p0, Lcom/airbnb/lottie/c/b/q;->i:F

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/q;->a:Ljava/lang/String;

    return-object v0
.end method

.method public h()Lcom/airbnb/lottie/c/a/d;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/q;->e:Lcom/airbnb/lottie/c/a/d;

    return-object v0
.end method

.method public i()Lcom/airbnb/lottie/c/a/b;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/q;->f:Lcom/airbnb/lottie/c/a/b;

    return-object v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/airbnb/lottie/c/b/q;->j:Z

    return v0
.end method
