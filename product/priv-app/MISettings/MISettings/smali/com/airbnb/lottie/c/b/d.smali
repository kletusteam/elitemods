.class public Lcom/airbnb/lottie/c/b/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/airbnb/lottie/c/b/b;


# instance fields
.field private final a:Lcom/airbnb/lottie/c/b/f;

.field private final b:Landroid/graphics/Path$FillType;

.field private final c:Lcom/airbnb/lottie/c/a/c;

.field private final d:Lcom/airbnb/lottie/c/a/d;

.field private final e:Lcom/airbnb/lottie/c/a/f;

.field private final f:Lcom/airbnb/lottie/c/a/f;

.field private final g:Ljava/lang/String;

.field private final h:Lcom/airbnb/lottie/c/a/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final i:Lcom/airbnb/lottie/c/a/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/airbnb/lottie/c/b/f;Landroid/graphics/Path$FillType;Lcom/airbnb/lottie/c/a/c;Lcom/airbnb/lottie/c/a/d;Lcom/airbnb/lottie/c/a/f;Lcom/airbnb/lottie/c/a/f;Lcom/airbnb/lottie/c/a/b;Lcom/airbnb/lottie/c/a/b;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/airbnb/lottie/c/b/d;->a:Lcom/airbnb/lottie/c/b/f;

    iput-object p3, p0, Lcom/airbnb/lottie/c/b/d;->b:Landroid/graphics/Path$FillType;

    iput-object p4, p0, Lcom/airbnb/lottie/c/b/d;->c:Lcom/airbnb/lottie/c/a/c;

    iput-object p5, p0, Lcom/airbnb/lottie/c/b/d;->d:Lcom/airbnb/lottie/c/a/d;

    iput-object p6, p0, Lcom/airbnb/lottie/c/b/d;->e:Lcom/airbnb/lottie/c/a/f;

    iput-object p7, p0, Lcom/airbnb/lottie/c/b/d;->f:Lcom/airbnb/lottie/c/a/f;

    iput-object p1, p0, Lcom/airbnb/lottie/c/b/d;->g:Ljava/lang/String;

    iput-object p8, p0, Lcom/airbnb/lottie/c/b/d;->h:Lcom/airbnb/lottie/c/a/b;

    iput-object p9, p0, Lcom/airbnb/lottie/c/b/d;->i:Lcom/airbnb/lottie/c/a/b;

    iput-boolean p10, p0, Lcom/airbnb/lottie/c/b/d;->j:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/c;)Lcom/airbnb/lottie/a/a/d;
    .locals 1

    new-instance v0, Lcom/airbnb/lottie/a/a/i;

    invoke-direct {v0, p1, p2, p0}, Lcom/airbnb/lottie/a/a/i;-><init>(Lcom/airbnb/lottie/LottieDrawable;Lcom/airbnb/lottie/c/c/c;Lcom/airbnb/lottie/c/b/d;)V

    return-object v0
.end method

.method public a()Lcom/airbnb/lottie/c/a/f;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/d;->f:Lcom/airbnb/lottie/c/a/f;

    return-object v0
.end method

.method public b()Landroid/graphics/Path$FillType;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/d;->b:Landroid/graphics/Path$FillType;

    return-object v0
.end method

.method public c()Lcom/airbnb/lottie/c/a/c;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/d;->c:Lcom/airbnb/lottie/c/a/c;

    return-object v0
.end method

.method public d()Lcom/airbnb/lottie/c/b/f;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/d;->a:Lcom/airbnb/lottie/c/b/f;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/d;->g:Ljava/lang/String;

    return-object v0
.end method

.method public f()Lcom/airbnb/lottie/c/a/d;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/d;->d:Lcom/airbnb/lottie/c/a/d;

    return-object v0
.end method

.method public g()Lcom/airbnb/lottie/c/a/f;
    .locals 1

    iget-object v0, p0, Lcom/airbnb/lottie/c/b/d;->e:Lcom/airbnb/lottie/c/a/f;

    return-object v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/airbnb/lottie/c/b/d;->j:Z

    return v0
.end method
