.class Lcom/misettings/common/utils/k;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/misettings/common/utils/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/misettings/common/utils/l;


# direct methods
.method constructor <init>(Lcom/misettings/common/utils/l;)V
    .locals 0

    iput-object p1, p0, Lcom/misettings/common/utils/k;->a:Lcom/misettings/common/utils/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBindingDied(Landroid/content/ComponentName;)V
    .locals 1

    const-string p1, "MemoryOptimizationService"

    const-string v0, "MemoryOptimization onBindingDied"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onNullBinding(Landroid/content/ComponentName;)V
    .locals 1

    const-string p1, "MemoryOptimizationService"

    const-string v0, "MemoryOptimization onNullBinding"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    iget-object p1, p0, Lcom/misettings/common/utils/k;->a:Lcom/misettings/common/utils/l;

    invoke-static {p2}, Lcom/android/settings/b/a$a;->a(Landroid/os/IBinder;)Lcom/android/settings/b/a;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/misettings/common/utils/l;->a(Lcom/misettings/common/utils/l;Lcom/android/settings/b/a;)Lcom/android/settings/b/a;

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    const-string p1, "MemoryOptimizationService"

    const-string v0, "MemoryOptimization onServiceDisconnected"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
