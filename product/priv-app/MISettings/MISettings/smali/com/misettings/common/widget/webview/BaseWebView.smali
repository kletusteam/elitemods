.class public Lcom/misettings/common/widget/webview/BaseWebView;
.super Lcom/miui/webkit_api/WebView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/misettings/common/widget/webview/BaseWebView$a;,
        Lcom/misettings/common/widget/webview/BaseWebView$b;
    }
.end annotation


# instance fields
.field private c:Lcom/misettings/common/widget/webview/BaseWebView$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/webkit_api/WebView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/misettings/common/widget/webview/BaseWebView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/webkit_api/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lcom/misettings/common/widget/webview/BaseWebView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/webkit_api/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Lcom/misettings/common/widget/webview/BaseWebView;->a()V

    return-void
.end method

.method private c()V
    .locals 4

    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->getSettings()Lcom/miui/webkit_api/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/WebSettings;->setSupportZoom(Z)V

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/WebSettings;->setBuiltInZoomControls(Z)V

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/WebSettings;->setDisplayZoomControls(Z)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/miui/webkit_api/WebSettings;->setUseWideViewPort(Z)V

    invoke-virtual {v0, v2}, Lcom/miui/webkit_api/WebSettings;->setLoadWithOverviewMode(Z)V

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/miui/webkit_api/WebSettings;->setMixedContentMode(I)V

    invoke-virtual {v0, v2}, Lcom/miui/webkit_api/WebSettings;->setJavaScriptEnabled(Z)V

    sget-object v3, Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;->NORMAL:Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;

    invoke-virtual {v0, v3}, Lcom/miui/webkit_api/WebSettings;->setLayoutAlgorithm(Lcom/miui/webkit_api/WebSettings$LayoutAlgorithm;)V

    invoke-virtual {v0, v2}, Lcom/miui/webkit_api/WebSettings;->setDomStorageEnabled(Z)V

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/WebSettings;->setGeolocationEnabled(Z)V

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/WebSettings;->setAllowFileAccess(Z)V

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/WebSettings;->setSupportMultipleWindows(Z)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/AbsoluteLayout;->setVerticalScrollBarEnabled(Z)V

    invoke-direct {p0}, Lcom/misettings/common/widget/webview/BaseWebView;->c()V

    new-instance v0, Lcom/misettings/common/widget/webview/BaseWebView$b;

    invoke-direct {v0, p0}, Lcom/misettings/common/widget/webview/BaseWebView$b;-><init>(Lcom/misettings/common/widget/webview/BaseWebView;)V

    iput-object v0, p0, Lcom/misettings/common/widget/webview/BaseWebView;->c:Lcom/misettings/common/widget/webview/BaseWebView$b;

    iget-object v0, p0, Lcom/misettings/common/widget/webview/BaseWebView;->c:Lcom/misettings/common/widget/webview/BaseWebView$b;

    invoke-virtual {p0, v0}, Lcom/miui/webkit_api/WebView;->setWebViewClient(Lcom/miui/webkit_api/WebViewClient;)V

    new-instance v0, Lcom/misettings/common/widget/webview/BaseWebView$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/misettings/common/widget/webview/BaseWebView$a;-><init>(Lcom/misettings/common/widget/webview/a;)V

    invoke-virtual {p0, v0}, Lcom/miui/webkit_api/WebView;->setWebChromeClient(Lcom/miui/webkit_api/WebChromeClient;)V

    return-void
.end method

.method protected a(ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected b()V
    .locals 0

    return-void
.end method

.method public destroy()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Landroid/widget/AbsoluteLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/widget/AbsoluteLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/widget/AbsoluteLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->stopLoading()V

    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->getSettings()Lcom/miui/webkit_api/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/webkit_api/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->clearView()V

    invoke-virtual {p0}, Landroid/widget/AbsoluteLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/misettings/common/widget/webview/BaseWebView;->c:Lcom/misettings/common/widget/webview/BaseWebView$b;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/misettings/common/widget/webview/BaseWebView;->c:Lcom/misettings/common/widget/webview/BaseWebView$b;

    invoke-virtual {v0}, Lcom/misettings/common/widget/webview/BaseWebView$b;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    invoke-super {p0}, Lcom/miui/webkit_api/WebView;->destroy()V

    return-void
.end method

.method public setOnScrollChangeListener(Landroid/view/View$OnScrollChangeListener;)V
    .locals 1

    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnScrollChangeListener(Landroid/view/View$OnScrollChangeListener;)V

    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    invoke-virtual {p0}, Lcom/miui/webkit_api/WebView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method
