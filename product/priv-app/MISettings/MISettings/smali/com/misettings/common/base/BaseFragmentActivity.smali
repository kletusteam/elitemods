.class public abstract Lcom/misettings/common/base/BaseFragmentActivity;
.super Lcom/misettings/common/base/BaseActivity;


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Landroidx/fragment/app/Fragment;
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/misettings/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/misettings/common/base/BaseFragmentActivity;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/misettings/common/base/BaseFragmentActivity;->a()Landroidx/fragment/app/Fragment;

    move-result-object p1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1, p1}, Landroidx/fragment/app/qa;->b(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    invoke-virtual {v0}, Landroidx/fragment/app/qa;->a()I

    return-void
.end method
