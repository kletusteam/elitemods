.class public Lcom/misettings/common/base/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/misettings/common/base/a$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/misettings/common/base/a$a;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/misettings/common/base/a;->a:Landroid/content/Context;

    new-instance p1, Lcom/misettings/common/base/a$a;

    invoke-direct {p1}, Lcom/misettings/common/base/a$a;-><init>()V

    iput-object p1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Context must be non-null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Landroid/app/Fragment;Landroid/content/Intent;I)V
    .locals 0

    invoke-virtual {p1, p2, p3}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public a()Lcom/misettings/common/base/a;
    .locals 2

    iget-object v0, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/misettings/common/base/a$a;->a:Z

    return-object p0
.end method

.method public a(I)Lcom/misettings/common/base/a;
    .locals 2

    iget-object v0, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget v1, v0, Lcom/misettings/common/base/a$a;->h:I

    or-int/2addr p1, v1

    iput p1, v0, Lcom/misettings/common/base/a$a;->h:I

    return-object p0
.end method

.method public a(Landroid/app/Fragment;I)Lcom/misettings/common/base/a;
    .locals 1

    iget-object v0, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iput p2, v0, Lcom/misettings/common/base/a$a;->j:I

    iput-object p1, v0, Lcom/misettings/common/base/a$a;->i:Landroid/app/Fragment;

    return-object p0
.end method

.method public a(Landroid/os/Bundle;)Lcom/misettings/common/base/a;
    .locals 1

    iget-object v0, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iput-object p1, v0, Lcom/misettings/common/base/a$a;->k:Landroid/os/Bundle;

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/misettings/common/base/a;
    .locals 1

    iget-object v0, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iput-object p1, v0, Lcom/misettings/common/base/a$a;->e:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public a(Ljava/lang/Class;)Lcom/misettings/common/base/a;
    .locals 1

    iget-object v0, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iput-object p1, v0, Lcom/misettings/common/base/a$a;->l:Ljava/lang/Class;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/misettings/common/base/a;
    .locals 1

    iget-object v0, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iput-object p1, v0, Lcom/misettings/common/base/a$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/lang/String;I)Lcom/misettings/common/base/a;
    .locals 1

    iget-object v0, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iput-object p1, v0, Lcom/misettings/common/base/a$a;->d:Ljava/lang/String;

    iput p2, v0, Lcom/misettings/common/base/a$a;->c:I

    const/4 p1, 0x0

    iput-object p1, v0, Lcom/misettings/common/base/a$a;->e:Ljava/lang/CharSequence;

    return-object p0
.end method

.method a(Landroid/content/Intent;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Lcom/misettings/common/base/a;->a:Landroid/content/Context;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/32 :goto_0

    nop
.end method

.method public b(I)Lcom/misettings/common/base/a;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/misettings/common/base/a;->a(Ljava/lang/String;I)Lcom/misettings/common/base/a;

    return-object p0
.end method

.method public b()V
    .locals 3

    iget-boolean v0, p0, Lcom/misettings/common/base/a;->c:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/misettings/common/base/a;->c:Z

    invoke-virtual {p0}, Lcom/misettings/common/base/a;->c()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget-object v2, v2, Lcom/misettings/common/base/a$a;->i:Landroid/app/Fragment;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget-object v2, v0, Lcom/misettings/common/base/a$a;->i:Landroid/app/Fragment;

    iget v0, v0, Lcom/misettings/common/base/a$a;->j:I

    invoke-direct {p0, v2, v1, v0}, Lcom/misettings/common/base/a;->a(Landroid/app/Fragment;Landroid/content/Intent;I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v1}, Lcom/misettings/common/base/a;->a(Landroid/content/Intent;)V

    :goto_1
    return-void

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This launcher has already been executed. Do not reuse"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget-object v1, v1, Lcom/misettings/common/base/a$a;->l:Ljava/lang/Class;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/misettings/common/base/a;->a:Landroid/content/Context;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/misettings/common/base/a;->a:Landroid/content/Context;

    const-class v2, Lcom/misettings/common/base/SubSettings;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    :goto_0
    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget-object v1, v1, Lcom/misettings/common/base/a$a;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget-object v1, v1, Lcom/misettings/common/base/a$a;->b:Ljava/lang/String;

    const-string v2, ":settings:show_fragment"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget v1, v1, Lcom/misettings/common/base/a$a;->g:I

    if-ltz v1, :cond_2

    const-string v2, ":settings:source_metrics"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget-object v1, v1, Lcom/misettings/common/base/a$a;->k:Landroid/os/Bundle;

    const-string v2, ":settings:show_fragment_args"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget-object v1, v1, Lcom/misettings/common/base/a$a;->d:Ljava/lang/String;

    const-string v2, ":settings:show_fragment_title_res_package_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget v1, v1, Lcom/misettings/common/base/a$a;->c:I

    const-string v2, ":settings:show_fragment_title_resid"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget v1, v1, Lcom/misettings/common/base/a$a;->c:I

    const-string v2, ":android:show_fragment_title"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget-object v1, v1, Lcom/misettings/common/base/a$a;->e:Ljava/lang/CharSequence;

    const-string v2, ":settings:show_fragment_title"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget-boolean v1, v1, Lcom/misettings/common/base/a$a;->f:Z

    const-string v2, ":settings:show_fragment_as_shortcut"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget v1, v1, Lcom/misettings/common/base/a$a;->h:I

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/misettings/common/base/a;->b:Lcom/misettings/common/base/a$a;

    iget-boolean v1, v1, Lcom/misettings/common/base/a$a;->a:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/misettings/common/utils/m;->a(Landroid/content/Intent;I)V

    :cond_1
    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Source metrics category must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Destination fragment must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
