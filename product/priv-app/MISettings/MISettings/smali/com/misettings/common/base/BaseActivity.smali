.class public Lcom/misettings/common/base/BaseActivity;
.super Lmiuix/appcompat/app/AppCompatActivity;


# static fields
.field private static final EXTRA_SPLIT_MODE:Ljava/lang/String; = "miui.extra.splitmode"

.field private static final EXTRA_VALUE_FORCE_SPLIT:I = 0x8

.field public static final FLAG_MIUI_CANCEL_SPLIT:I = 0x8

.field public static final FLAG_MIUI_SPLIT_ACTIVITY:I = 0x4

.field private static final FORCE_FSG_NAV_BAR:Ljava/lang/String; = "force_fsg_nav_bar"

.field private static final GESTURE_TYPE_CLASSICAL:I = 0x0

.field private static final GESTURE_TYPE_FSG:I = 0x1

.field private static final TYPE_FILEMANAGER:Ljava/lang/String; = "filemanager.dir"


# instance fields
.field memoryOptimizationUtil:Lcom/misettings/common/utils/l;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/AppCompatActivity;-><init>()V

    new-instance v0, Lcom/misettings/common/utils/l;

    invoke-direct {v0}, Lcom/misettings/common/utils/l;-><init>()V

    iput-object v0, p0, Lcom/misettings/common/base/BaseActivity;->memoryOptimizationUtil:Lcom/misettings/common/utils/l;

    return-void
.end method

.method private needCancelSplit(Landroid/content/Intent;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/misettings/common/utils/n;->a(Landroid/content/Intent;)I

    move-result v1

    and-int/lit8 v1, v1, 0x8

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result p1

    const/high16 v1, 0x10000000

    and-int/2addr p1, v1

    if-eqz p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private setNavigationBarTransparent()V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "force_fsg_nav_bar"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/high16 v1, 0x8000000

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public isClassicalNavBar()Z
    .locals 3

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "force_fsg_nav_bar"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method protected isSplitStyleActivity()Z
    .locals 5

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0xf

    const/4 v3, 0x3

    const/4 v4, 0x1

    if-eq v2, v3, :cond_1

    move v2, v4

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    return v1

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/misettings/common/utils/n;->a(Landroid/content/Intent;)I

    move-result v2

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_4

    invoke-direct {p0, v0}, Lcom/misettings/common/base/BaseActivity;->needCancelSplit(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "miui.extra.splitmode"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    :cond_3
    move v1, v4

    :cond_4
    return v1
.end method

.method public isSupportMemoryOptimized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-static {}, Lcom/misettings/common/utils/n;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    invoke-static {}, Lcom/misettings/common/utils/m;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/misettings/common/utils/m;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    :goto_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_2
    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/misettings/common/base/BaseActivity;->setNavigationBarTransparent()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/d;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Lcom/misettings/common/base/BaseActivity;->isSplitStyleActivity()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/misettings/common/utils/n;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/d;->e(I)V

    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/d;->g(Z)V

    :cond_4
    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onStart()V

    iget-object v0, p0, Lcom/misettings/common/base/BaseActivity;->memoryOptimizationUtil:Lcom/misettings/common/utils/l;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/misettings/common/utils/l;->a(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/AppCompatActivity;->onStop()V

    invoke-virtual {p0}, Lcom/misettings/common/base/BaseActivity;->isSupportMemoryOptimized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/misettings/common/base/BaseActivity;->memoryOptimizationUtil:Lcom/misettings/common/utils/l;

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/misettings/common/utils/p;->d(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/misettings/common/base/BaseActivity;->memoryOptimizationUtil:Lcom/misettings/common/utils/l;

    invoke-virtual {v0, p0}, Lcom/misettings/common/utils/l;->b(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/misettings/common/base/BaseActivity;->memoryOptimizationUtil:Lcom/misettings/common/utils/l;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Lcom/misettings/common/utils/l;->c(Landroid/content/Context;)V

    :cond_1
    :goto_0
    return-void
.end method
