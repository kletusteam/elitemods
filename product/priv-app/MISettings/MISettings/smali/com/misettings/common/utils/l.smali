.class public Lcom/misettings/common/utils/l;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/content/ServiceConnection;

.field private b:Lcom/android/settings/b/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/misettings/common/utils/k;

    invoke-direct {v0, p0}, Lcom/misettings/common/utils/k;-><init>(Lcom/misettings/common/utils/l;)V

    iput-object v0, p0, Lcom/misettings/common/utils/l;->a:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic a(Lcom/misettings/common/utils/l;Lcom/android/settings/b/a;)Lcom/android/settings/b/a;
    .locals 0

    iput-object p1, p0, Lcom/misettings/common/utils/l;->b:Lcom/android/settings/b/a;

    return-object p1
.end method

.method private a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    .locals 1

    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "tryUnbindMemoryService error:"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "MemoryOptimizationService"

    invoke-static {p2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.MEMORY_OPTIMIZATION_INIT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.htmlviewer"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    iget-object v1, p0, Lcom/misettings/common/utils/l;->a:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "MemoryOptimizationService"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;ZJ)V
    .locals 2

    iget-object v0, p0, Lcom/misettings/common/utils/l;->b:Lcom/android/settings/b/a;

    if-nez v0, :cond_0

    const-string p2, "MemoryOptimizationService"

    const-string p3, "MemoryOptimization proxy is null"

    invoke-static {p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p2, p0, Lcom/misettings/common/utils/l;->a:Landroid/content/ServiceConnection;

    invoke-direct {p0, p1, p2}, Lcom/misettings/common/utils/l;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    goto :goto_1

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.MEMORY_OPTIMIZATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p2

    const-string v1, "optimized_package"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "restart_process"

    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p2, "delay_time"

    invoke-virtual {v0, p2, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :try_start_0
    iget-object p2, p0, Lcom/misettings/common/utils/l;->b:Lcom/android/settings/b/a;

    invoke-interface {p2, v0}, Lcom/android/settings/b/a;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    iget-object p2, p0, Lcom/misettings/common/utils/l;->a:Landroid/content/ServiceConnection;

    invoke-direct {p0, p1, p2}, Lcom/misettings/common/utils/l;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    :goto_1
    return-void
.end method

.method public b(Landroid/content/Context;)V
    .locals 6

    const-string v2, "com.android.htmlviewer"

    const/4 v3, 0x1

    const-wide/16 v4, 0x7530

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/misettings/common/utils/l;->a(Landroid/content/Context;Ljava/lang/String;ZJ)V

    return-void
.end method

.method public c(Landroid/content/Context;)V
    .locals 1

    iget-object v0, p0, Lcom/misettings/common/utils/l;->a:Landroid/content/ServiceConnection;

    invoke-direct {p0, p1, v0}, Lcom/misettings/common/utils/l;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    return-void
.end method
