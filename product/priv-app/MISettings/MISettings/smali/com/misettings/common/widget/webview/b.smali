.class Lcom/misettings/common/widget/webview/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/misettings/common/widget/webview/c;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/misettings/common/widget/webview/c;


# direct methods
.method constructor <init>(Lcom/misettings/common/widget/webview/c;)V
    .locals 0

    iput-object p1, p0, Lcom/misettings/common/widget/webview/b;->a:Lcom/misettings/common/widget/webview/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/misettings/common/widget/webview/b;->a:Lcom/misettings/common/widget/webview/c;

    iget-object v0, v0, Lcom/misettings/common/widget/webview/c;->a:Lcom/misettings/common/widget/webview/BaseWebView$b;

    invoke-static {v0}, Lcom/misettings/common/widget/webview/BaseWebView$b;->a(Lcom/misettings/common/widget/webview/BaseWebView$b;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/misettings/common/widget/webview/b;->a:Lcom/misettings/common/widget/webview/c;

    iget-object v0, v0, Lcom/misettings/common/widget/webview/c;->a:Lcom/misettings/common/widget/webview/BaseWebView$b;

    invoke-static {v0}, Lcom/misettings/common/widget/webview/BaseWebView$b;->a(Lcom/misettings/common/widget/webview/BaseWebView$b;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/misettings/common/widget/webview/b;->a:Lcom/misettings/common/widget/webview/c;

    iget-object v0, v0, Lcom/misettings/common/widget/webview/c;->a:Lcom/misettings/common/widget/webview/BaseWebView$b;

    invoke-static {v0}, Lcom/misettings/common/widget/webview/BaseWebView$b;->a(Lcom/misettings/common/widget/webview/BaseWebView$b;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/misettings/common/widget/webview/BaseWebView;

    invoke-virtual {v0}, Lcom/miui/webkit_api/WebView;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/misettings/common/widget/webview/b;->a:Lcom/misettings/common/widget/webview/c;

    iget-object v0, v0, Lcom/misettings/common/widget/webview/c;->a:Lcom/misettings/common/widget/webview/BaseWebView$b;

    invoke-static {v0}, Lcom/misettings/common/widget/webview/BaseWebView$b;->a(Lcom/misettings/common/widget/webview/BaseWebView$b;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/misettings/common/widget/webview/BaseWebView;

    invoke-virtual {v0}, Lcom/miui/webkit_api/WebView;->stopLoading()V

    iget-object v0, p0, Lcom/misettings/common/widget/webview/b;->a:Lcom/misettings/common/widget/webview/c;

    iget-object v0, v0, Lcom/misettings/common/widget/webview/c;->a:Lcom/misettings/common/widget/webview/BaseWebView$b;

    invoke-static {v0}, Lcom/misettings/common/widget/webview/BaseWebView$b;->a(Lcom/misettings/common/widget/webview/BaseWebView$b;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/misettings/common/widget/webview/BaseWebView;

    invoke-virtual {v0}, Lcom/misettings/common/widget/webview/BaseWebView;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    iget-object v0, p0, Lcom/misettings/common/widget/webview/b;->a:Lcom/misettings/common/widget/webview/c;

    iget-object v0, v0, Lcom/misettings/common/widget/webview/c;->a:Lcom/misettings/common/widget/webview/BaseWebView$b;

    iget-object v0, v0, Lcom/misettings/common/widget/webview/BaseWebView$b;->c:Ljava/util/Timer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/misettings/common/widget/webview/b;->a:Lcom/misettings/common/widget/webview/c;

    iget-object v0, v0, Lcom/misettings/common/widget/webview/c;->a:Lcom/misettings/common/widget/webview/BaseWebView$b;

    iget-object v0, v0, Lcom/misettings/common/widget/webview/BaseWebView$b;->c:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    :cond_1
    return-void
.end method
