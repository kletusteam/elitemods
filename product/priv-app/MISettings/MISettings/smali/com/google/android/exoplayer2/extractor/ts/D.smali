.class public final Lcom/google/android/exoplayer2/extractor/ts/D;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader;


# instance fields
.field private final a:Lcom/google/android/exoplayer2/extractor/ts/C;

.field private final b:Lcom/google/android/exoplayer2/util/t;

.field private c:I

.field private d:I

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/extractor/ts/C;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->a:Lcom/google/android/exoplayer2/extractor/ts/C;

    new-instance p1, Lcom/google/android/exoplayer2/util/t;

    const/16 v0, 0x20

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->f:Z

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/util/C;Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->a:Lcom/google/android/exoplayer2/extractor/ts/C;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/extractor/ts/C;->a(Lcom/google/android/exoplayer2/util/C;Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->f:Z

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/util/t;I)V
    .locals 8

    const/4 v0, 0x1

    and-int/2addr p2, v0

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    move p2, v0

    goto :goto_0

    :cond_0
    move p2, v1

    :goto_0
    const/4 v2, -0x1

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_1

    :cond_1
    move v3, v2

    :goto_1
    iget-boolean v4, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->f:Z

    if-eqz v4, :cond_3

    if-nez p2, :cond_2

    return-void

    :cond_2
    iput-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->f:Z

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    :cond_3
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result p2

    if-lez p2, :cond_9

    iget p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    const/4 v3, 0x3

    if-ge p2, v3, :cond_6

    if-nez p2, :cond_4

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result p2

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result v4

    sub-int/2addr v4, v0

    invoke-virtual {p1, v4}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    const/16 v4, 0xff

    if-ne p2, v4, :cond_4

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->f:Z

    return-void

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result p2

    iget v4, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    rsub-int/lit8 v4, v4, 0x3

    invoke-static {p2, v4}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    iget v5, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    invoke-virtual {p1, v4, v5, p2}, Lcom/google/android/exoplayer2/util/t;->a([BII)V

    iget v4, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    add-int/2addr v4, p2

    iput v4, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    iget p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    if-ne p2, v3, :cond_3

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, v3}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, v0}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result p2

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result v4

    and-int/lit16 v5, p2, 0x80

    if-eqz v5, :cond_5

    move v5, v0

    goto :goto_3

    :cond_5
    move v5, v1

    :goto_3
    iput-boolean v5, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->e:Z

    and-int/lit8 p2, p2, 0xf

    shl-int/lit8 p2, p2, 0x8

    or-int/2addr p2, v4

    add-int/2addr p2, v3

    iput p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->c:I

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->b()I

    move-result p2

    iget v4, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->c:I

    if-ge p2, v4, :cond_3

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p2

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    const/16 v5, 0x1002

    iget v6, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->c:I

    array-length v7, p2

    mul-int/lit8 v7, v7, 0x2

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    invoke-static {p2, v1, v4, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_2

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result p2

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->c:I

    iget v4, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    sub-int/2addr v3, v4

    invoke-static {p2, v3}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v3

    iget v4, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    invoke-virtual {p1, v3, v4, p2}, Lcom/google/android/exoplayer2/util/t;->a([BII)V

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    add-int/2addr v3, p2

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    iget p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->c:I

    if-ne p2, v3, :cond_3

    iget-boolean p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->e:Z

    if-eqz p2, :cond_8

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p2

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->c:I

    invoke-static {p2, v1, v3, v2}, Lcom/google/android/exoplayer2/util/E;->a([BIII)I

    move-result p2

    if-eqz p2, :cond_7

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->f:Z

    return-void

    :cond_7
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->c:I

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {p2, v3}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    goto :goto_4

    :cond_8
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, v3}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    :goto_4
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->a:Lcom/google/android/exoplayer2/extractor/ts/C;

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-interface {p2, v3}, Lcom/google/android/exoplayer2/extractor/ts/C;->a(Lcom/google/android/exoplayer2/util/t;)V

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/ts/D;->d:I

    goto/16 :goto_2

    :cond_9
    return-void
.end method
