.class public final Lcom/google/android/exoplayer2/sa$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/sa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field public static final a:Ljava/lang/Object;

.field private static final b:Lcom/google/android/exoplayer2/W;


# instance fields
.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public e:Lcom/google/android/exoplayer2/W;

.field public f:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public g:J

.field public h:J

.field public i:J

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:I

.field public o:I

.field public p:J

.field public q:J

.field public r:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer2/sa$b;->a:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/exoplayer2/W$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/W$a;-><init>()V

    const-string v1, "com.google.android.exoplayer2.Timeline"

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/W$a;->b(Ljava/lang/String;)Lcom/google/android/exoplayer2/W$a;

    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/W$a;->a(Landroid/net/Uri;)Lcom/google/android/exoplayer2/W$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/W$a;->a()Lcom/google/android/exoplayer2/W;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/sa$b;->b:Lcom/google/android/exoplayer2/W;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/exoplayer2/sa$b;->a:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/exoplayer2/sa$b;->c:Ljava/lang/Object;

    sget-object v0, Lcom/google/android/exoplayer2/sa$b;->b:Lcom/google/android/exoplayer2/W;

    iput-object v0, p0, Lcom/google/android/exoplayer2/sa$b;->e:Lcom/google/android/exoplayer2/W;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/sa$b;->p:J

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/Object;Lcom/google/android/exoplayer2/W;Ljava/lang/Object;JJJZZZJJIIJ)Lcom/google/android/exoplayer2/sa$b;
    .locals 3
    .param p2    # Lcom/google/android/exoplayer2/W;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    iput-object v2, v0, Lcom/google/android/exoplayer2/sa$b;->c:Ljava/lang/Object;

    if-eqz v1, :cond_0

    move-object v2, v1

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/google/android/exoplayer2/sa$b;->b:Lcom/google/android/exoplayer2/W;

    :goto_0
    iput-object v2, v0, Lcom/google/android/exoplayer2/sa$b;->e:Lcom/google/android/exoplayer2/W;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/google/android/exoplayer2/W;->b:Lcom/google/android/exoplayer2/W$d;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/google/android/exoplayer2/W$d;->h:Ljava/lang/Object;

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    iput-object v1, v0, Lcom/google/android/exoplayer2/sa$b;->d:Ljava/lang/Object;

    move-object v1, p3

    iput-object v1, v0, Lcom/google/android/exoplayer2/sa$b;->f:Ljava/lang/Object;

    move-wide v1, p4

    iput-wide v1, v0, Lcom/google/android/exoplayer2/sa$b;->g:J

    move-wide v1, p6

    iput-wide v1, v0, Lcom/google/android/exoplayer2/sa$b;->h:J

    move-wide v1, p8

    iput-wide v1, v0, Lcom/google/android/exoplayer2/sa$b;->i:J

    move v1, p10

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/sa$b;->j:Z

    move v1, p11

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/sa$b;->k:Z

    move v1, p12

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/sa$b;->l:Z

    move-wide/from16 v1, p13

    iput-wide v1, v0, Lcom/google/android/exoplayer2/sa$b;->p:J

    move-wide/from16 v1, p15

    iput-wide v1, v0, Lcom/google/android/exoplayer2/sa$b;->q:J

    move/from16 v1, p17

    iput v1, v0, Lcom/google/android/exoplayer2/sa$b;->n:I

    move/from16 v1, p18

    iput v1, v0, Lcom/google/android/exoplayer2/sa$b;->o:I

    move-wide/from16 v1, p19

    iput-wide v1, v0, Lcom/google/android/exoplayer2/sa$b;->r:J

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/sa$b;->m:Z

    return-object v0
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/sa$b;->p:J

    return-wide v0
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/sa$b;->q:J

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/sa$b;->r:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const-class v2, Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    goto/16 :goto_1

    :cond_1
    check-cast p1, Lcom/google/android/exoplayer2/sa$b;

    iget-object v2, p0, Lcom/google/android/exoplayer2/sa$b;->c:Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/exoplayer2/sa$b;->c:Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/exoplayer2/sa$b;->e:Lcom/google/android/exoplayer2/W;

    iget-object v3, p1, Lcom/google/android/exoplayer2/sa$b;->e:Lcom/google/android/exoplayer2/W;

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/exoplayer2/sa$b;->f:Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/exoplayer2/sa$b;->f:Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->g:J

    iget-wide v4, p1, Lcom/google/android/exoplayer2/sa$b;->g:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->h:J

    iget-wide v4, p1, Lcom/google/android/exoplayer2/sa$b;->h:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->i:J

    iget-wide v4, p1, Lcom/google/android/exoplayer2/sa$b;->i:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/sa$b;->j:Z

    iget-boolean v3, p1, Lcom/google/android/exoplayer2/sa$b;->j:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/sa$b;->k:Z

    iget-boolean v3, p1, Lcom/google/android/exoplayer2/sa$b;->k:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/sa$b;->l:Z

    iget-boolean v3, p1, Lcom/google/android/exoplayer2/sa$b;->l:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/sa$b;->m:Z

    iget-boolean v3, p1, Lcom/google/android/exoplayer2/sa$b;->m:Z

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->p:J

    iget-wide v4, p1, Lcom/google/android/exoplayer2/sa$b;->p:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->q:J

    iget-wide v4, p1, Lcom/google/android/exoplayer2/sa$b;->q:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/exoplayer2/sa$b;->n:I

    iget v3, p1, Lcom/google/android/exoplayer2/sa$b;->n:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/exoplayer2/sa$b;->o:I

    iget v3, p1, Lcom/google/android/exoplayer2/sa$b;->o:I

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->r:J

    iget-wide v4, p1, Lcom/google/android/exoplayer2/sa$b;->r:J

    cmp-long p1, v2, v4

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 6

    iget-object v0, p0, Lcom/google/android/exoplayer2/sa$b;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const/16 v1, 0xd9

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/google/android/exoplayer2/sa$b;->e:Lcom/google/android/exoplayer2/W;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/W;->hashCode()I

    move-result v0

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/google/android/exoplayer2/sa$b;->f:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->g:J

    const/16 v0, 0x20

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->h:J

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->i:J

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/sa$b;->j:Z

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/sa$b;->k:Z

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/sa$b;->l:Z

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/sa$b;->m:Z

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->p:J

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->q:J

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget v2, p0, Lcom/google/android/exoplayer2/sa$b;->n:I

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget v2, p0, Lcom/google/android/exoplayer2/sa$b;->o:I

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lcom/google/android/exoplayer2/sa$b;->r:J

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/2addr v1, v0

    return v1
.end method
