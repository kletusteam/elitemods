.class final Lcom/google/android/exoplayer2/trackselection/e$b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/trackselection/e$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/trackselection/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/upstream/g;

.field private final b:F

.field private final c:J

.field private d:[[J
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/exoplayer2/upstream/g;FJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/trackselection/e$b;->a:Lcom/google/android/exoplayer2/upstream/g;

    iput p2, p0, Lcom/google/android/exoplayer2/trackselection/e$b;->b:F

    iput-wide p3, p0, Lcom/google/android/exoplayer2/trackselection/e$b;->c:J

    return-void
.end method


# virtual methods
.method a([[J)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/trackselection/e$b;->d:[[J

    goto/32 :goto_3

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_2
    array-length v0, p1

    goto/32 :goto_8

    nop

    :goto_3
    return-void

    :goto_4
    const/4 v0, 0x0

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    goto :goto_5

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    const/4 v1, 0x2

    goto/32 :goto_9

    nop

    :goto_9
    if-ge v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_1

    nop

    :goto_a
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    goto/32 :goto_0

    nop
.end method
