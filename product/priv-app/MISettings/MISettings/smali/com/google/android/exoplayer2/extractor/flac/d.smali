.class final Lcom/google/android/exoplayer2/extractor/flac/d;
.super Lcom/google/android/exoplayer2/extractor/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/flac/d$a;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/extractor/q;IJJ)V
    .locals 16

    move-object/from16 v0, p1

    invoke-static/range {p1 .. p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/flac/b;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer2/extractor/flac/b;-><init>(Lcom/google/android/exoplayer2/extractor/q;)V

    new-instance v2, Lcom/google/android/exoplayer2/extractor/flac/d$a;

    const/4 v3, 0x0

    move/from16 v4, p2

    invoke-direct {v2, v0, v4, v3}, Lcom/google/android/exoplayer2/extractor/flac/d$a;-><init>(Lcom/google/android/exoplayer2/extractor/q;ILcom/google/android/exoplayer2/extractor/flac/c;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/extractor/q;->b()J

    move-result-wide v3

    iget-wide v7, v0, Lcom/google/android/exoplayer2/extractor/q;->j:J

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/extractor/q;->a()J

    move-result-wide v13

    iget v0, v0, Lcom/google/android/exoplayer2/extractor/q;->c:I

    const/4 v5, 0x6

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v15

    const-wide/16 v5, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v9, p3

    move-wide/from16 v11, p5

    invoke-direct/range {v0 .. v15}, Lcom/google/android/exoplayer2/extractor/b;-><init>(Lcom/google/android/exoplayer2/extractor/b$d;Lcom/google/android/exoplayer2/extractor/b$f;JJJJJJI)V

    return-void
.end method
