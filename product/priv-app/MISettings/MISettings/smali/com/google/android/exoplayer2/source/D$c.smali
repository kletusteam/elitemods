.class final Lcom/google/android/exoplayer2/source/D$c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/source/SampleStream;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/source/D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field private final a:I

.field final synthetic b:Lcom/google/android/exoplayer2/source/D;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/source/D;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D$c;->b:Lcom/google/android/exoplayer2/source/D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/exoplayer2/source/D$c;->a:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/D$c;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/source/D$c;->a:I

    return p0
.end method


# virtual methods
.method public a(J)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D$c;->b:Lcom/google/android/exoplayer2/source/D;

    iget v1, p0, Lcom/google/android/exoplayer2/source/D$c;->a:I

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/exoplayer2/source/D;->a(IJ)I

    move-result p1

    return p1
.end method

.method public a(Lcom/google/android/exoplayer2/S;Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;Z)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D$c;->b:Lcom/google/android/exoplayer2/source/D;

    iget v1, p0, Lcom/google/android/exoplayer2/source/D$c;->a:I

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/android/exoplayer2/source/D;->a(ILcom/google/android/exoplayer2/S;Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;Z)I

    move-result p1

    return p1
.end method

.method public a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D$c;->b:Lcom/google/android/exoplayer2/source/D;

    iget v1, p0, Lcom/google/android/exoplayer2/source/D$c;->a:I

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/source/D;->b(I)V

    return-void
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D$c;->b:Lcom/google/android/exoplayer2/source/D;

    iget v1, p0, Lcom/google/android/exoplayer2/source/D$c;->a:I

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/source/D;->a(I)Z

    move-result v0

    return v0
.end method
