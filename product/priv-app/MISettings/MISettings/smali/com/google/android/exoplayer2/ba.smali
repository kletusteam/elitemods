.class final Lcom/google/android/exoplayer2/ba;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/android/exoplayer2/sa$a;

.field private final b:Lcom/google/android/exoplayer2/sa$b;

.field private final c:Lcom/google/android/exoplayer2/a/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final d:Landroid/os/Handler;

.field private e:J

.field private f:I

.field private g:Z

.field private h:Lcom/google/android/exoplayer2/Z;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/google/android/exoplayer2/Z;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/google/android/exoplayer2/Z;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private k:I

.field private l:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private m:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/a/a;Landroid/os/Handler;)V
    .locals 0
    .param p1    # Lcom/google/android/exoplayer2/a/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/ba;->c:Lcom/google/android/exoplayer2/a/a;

    iput-object p2, p0, Lcom/google/android/exoplayer2/ba;->d:Landroid/os/Handler;

    new-instance p1, Lcom/google/android/exoplayer2/sa$a;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/sa$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    new-instance p1, Lcom/google/android/exoplayer2/sa$b;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/sa$b;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/ba;->b:Lcom/google/android/exoplayer2/sa$b;

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;)J
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p1, p2, v0}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    iget-object v1, p0, Lcom/google/android/exoplayer2/ba;->l:Ljava/lang/Object;

    const/4 v2, -0x1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v1

    if-eq v1, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p1, v1, v3}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v1

    iget v1, v1, Lcom/google/android/exoplayer2/sa$a;->c:I

    if-ne v1, v0, :cond_0

    iget-wide p1, p0, Lcom/google/android/exoplayer2/ba;->m:J

    return-wide p1

    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    :goto_0
    if-eqz v1, :cond_2

    iget-object v3, v1, Lcom/google/android/exoplayer2/Z;->b:Ljava/lang/Object;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object p1, v1, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object p1, p1, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide p1, p1, Lcom/google/android/exoplayer2/source/y$a;->d:J

    return-wide p1

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    :goto_1
    if-eqz v1, :cond_4

    iget-object v3, v1, Lcom/google/android/exoplayer2/Z;->b:Ljava/lang/Object;

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v3

    if-eq v3, v2, :cond_3

    iget-object v4, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p1, v3, v4}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v3

    iget v3, v3, Lcom/google/android/exoplayer2/sa$a;->c:I

    if-ne v3, v0, :cond_3

    iget-object p1, v1, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object p1, p1, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide p1, p1, Lcom/google/android/exoplayer2/source/y$a;->d:J

    return-wide p1

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    goto :goto_1

    :cond_4
    iget-wide v0, p0, Lcom/google/android/exoplayer2/ba;->e:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/android/exoplayer2/ba;->e:J

    iget-object p1, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    if-nez p1, :cond_5

    iput-object p2, p0, Lcom/google/android/exoplayer2/ba;->l:Ljava/lang/Object;

    iput-wide v0, p0, Lcom/google/android/exoplayer2/ba;->m:J

    :cond_5
    return-wide v0
.end method

.method private a(Lcom/google/android/exoplayer2/fa;)Lcom/google/android/exoplayer2/aa;
    .locals 7
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v1, p1, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v2, p1, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v3, p1, Lcom/google/android/exoplayer2/fa;->d:J

    iget-wide v5, p1, Lcom/google/android/exoplayer2/fa;->q:J

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/aa;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/Z;J)Lcom/google/android/exoplayer2/aa;
    .locals 18
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v8, p1

    move-object/from16 v10, p2

    iget-object v11, v10, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/Z;->d()J

    move-result-wide v0

    iget-wide v2, v11, Lcom/google/android/exoplayer2/aa;->e:J

    add-long/2addr v0, v2

    sub-long v6, v0, p3

    iget-boolean v0, v11, Lcom/google/android/exoplayer2/aa;->f:Z

    const/4 v14, -0x1

    const/4 v15, 0x0

    if-eqz v0, :cond_4

    iget-object v0, v11, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {v8, v0}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v1

    iget-object v2, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    iget-object v3, v9, Lcom/google/android/exoplayer2/ba;->b:Lcom/google/android/exoplayer2/sa$b;

    iget v0, v9, Lcom/google/android/exoplayer2/ba;->f:I

    iget-boolean v5, v9, Lcom/google/android/exoplayer2/ba;->g:Z

    move v4, v0

    move-object/from16 v0, p1

    const-wide/16 v12, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;Lcom/google/android/exoplayer2/sa$b;IZ)I

    move-result v0

    if-ne v0, v14, :cond_0

    return-object v15

    :cond_0
    iget-object v1, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    const/4 v2, 0x1

    invoke-virtual {v8, v0, v1, v2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;Z)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v1

    iget v3, v1, Lcom/google/android/exoplayer2/sa$a;->c:I

    iget-object v1, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/sa$a;->b:Ljava/lang/Object;

    iget-object v2, v11, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v4, v2, Lcom/google/android/exoplayer2/source/y$a;->d:J

    iget-object v2, v9, Lcom/google/android/exoplayer2/ba;->b:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v8, v3, v2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v2

    iget v2, v2, Lcom/google/android/exoplayer2/sa$b;->n:I

    if-ne v2, v0, :cond_3

    iget-object v1, v9, Lcom/google/android/exoplayer2/ba;->b:Lcom/google/android/exoplayer2/sa$b;

    iget-object v2, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    invoke-static {v12, v13, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    move-object/from16 v0, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/exoplayer2/sa;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IJJ)Landroid/util/Pair;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v15

    :cond_1
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v4, v0, Lcom/google/android/exoplayer2/Z;->b:Ljava/lang/Object;

    invoke-virtual {v4, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v0, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/source/y$a;->d:J

    goto :goto_0

    :cond_2
    iget-wide v4, v9, Lcom/google/android/exoplayer2/ba;->e:J

    const-wide/16 v6, 0x1

    add-long/2addr v6, v4

    iput-wide v6, v9, Lcom/google/android/exoplayer2/ba;->e:J

    :goto_0
    move-wide v12, v2

    const-wide v16, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_1

    :cond_3
    move-wide/from16 v16, v12

    :goto_1
    iget-object v6, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    move-object/from16 v0, p1

    move-wide v2, v12

    invoke-static/range {v0 .. v6}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;JJLcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/source/y$a;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v3, v16

    move-wide v5, v12

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/aa;

    move-result-object v0

    return-object v0

    :cond_4
    const-wide/16 v12, 0x0

    iget-object v10, v11, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v0, v10, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v1, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v8, v0, v1}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v10}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    iget v3, v10, Lcom/google/android/exoplayer2/source/y$a;->b:I

    iget-object v0, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/sa$a;->a(I)I

    move-result v0

    if-ne v0, v14, :cond_5

    return-object v15

    :cond_5
    iget-object v1, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    iget v2, v10, Lcom/google/android/exoplayer2/source/y$a;->c:I

    invoke-virtual {v1, v3, v2}, Lcom/google/android/exoplayer2/sa$a;->b(II)I

    move-result v4

    if-ge v4, v0, :cond_7

    iget-object v0, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/exoplayer2/sa$a;->c(II)Z

    move-result v0

    if-nez v0, :cond_6

    goto :goto_2

    :cond_6
    iget-object v2, v10, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-wide v5, v11, Lcom/google/android/exoplayer2/aa;->c:J

    iget-wide v10, v10, Lcom/google/android/exoplayer2/source/y$a;->d:J

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide v7, v10

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;IIJJ)Lcom/google/android/exoplayer2/aa;

    move-result-object v15

    :goto_2
    return-object v15

    :cond_7
    iget-wide v0, v11, Lcom/google/android/exoplayer2/aa;->c:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v2, v0, v2

    if-nez v2, :cond_9

    iget-object v1, v9, Lcom/google/android/exoplayer2/ba;->b:Lcom/google/android/exoplayer2/sa$b;

    iget-object v2, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    iget v3, v2, Lcom/google/android/exoplayer2/sa$a;->c:I

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    invoke-static {v12, v13, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    move-object/from16 v0, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/exoplayer2/sa;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IJJ)Landroid/util/Pair;

    move-result-object v0

    if-nez v0, :cond_8

    return-object v15

    :cond_8
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :cond_9
    move-wide v3, v0

    iget-object v2, v10, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-wide v5, v11, Lcom/google/android/exoplayer2/aa;->c:J

    iget-wide v10, v10, Lcom/google/android/exoplayer2/source/y$a;->d:J

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide v7, v10

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;JJJ)Lcom/google/android/exoplayer2/aa;

    move-result-object v0

    return-object v0

    :cond_a
    iget-object v0, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    iget-wide v1, v11, Lcom/google/android/exoplayer2/aa;->d:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/sa$a;->b(J)I

    move-result v3

    if-ne v3, v14, :cond_b

    iget-object v2, v10, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-wide v5, v11, Lcom/google/android/exoplayer2/aa;->e:J

    iget-wide v10, v10, Lcom/google/android/exoplayer2/source/y$a;->d:J

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide v3, v5

    move-wide v7, v10

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;JJJ)Lcom/google/android/exoplayer2/aa;

    move-result-object v0

    return-object v0

    :cond_b
    iget-object v0, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/sa$a;->c(I)I

    move-result v4

    iget-object v0, v9, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/exoplayer2/sa$a;->c(II)Z

    move-result v0

    if-nez v0, :cond_c

    goto :goto_3

    :cond_c
    iget-object v2, v10, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-wide v5, v11, Lcom/google/android/exoplayer2/aa;->e:J

    iget-wide v10, v10, Lcom/google/android/exoplayer2/source/y$a;->d:J

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide v7, v10

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;IIJJ)Lcom/google/android/exoplayer2/aa;

    move-result-object v15

    :goto_3
    return-object v15
.end method

.method private a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/aa;
    .locals 9
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p2, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p5, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    iget p6, p2, Lcom/google/android/exoplayer2/source/y$a;->b:I

    iget v0, p2, Lcom/google/android/exoplayer2/source/y$a;->c:I

    invoke-virtual {p5, p6, v0}, Lcom/google/android/exoplayer2/sa$a;->c(II)Z

    move-result p5

    if-nez p5, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v2, p2, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget v3, p2, Lcom/google/android/exoplayer2/source/y$a;->b:I

    iget v4, p2, Lcom/google/android/exoplayer2/source/y$a;->c:I

    iget-wide v7, p2, Lcom/google/android/exoplayer2/source/y$a;->d:J

    move-object v0, p0

    move-object v1, p1

    move-wide v5, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;IIJJ)Lcom/google/android/exoplayer2/aa;

    move-result-object p1

    return-object p1

    :cond_1
    iget-object v2, p2, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-wide v7, p2, Lcom/google/android/exoplayer2/source/y$a;->d:J

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p5

    move-wide v5, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;JJJ)Lcom/google/android/exoplayer2/aa;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;IIJJ)Lcom/google/android/exoplayer2/aa;
    .locals 17

    move-object/from16 v0, p0

    new-instance v7, Lcom/google/android/exoplayer2/source/y$a;

    move-object v1, v7

    move-object/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move-wide/from16 v5, p7

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/source/y$a;-><init>(Ljava/lang/Object;IIJ)V

    iget-object v1, v7, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v2, v0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    move-object/from16 v3, p1

    invoke-virtual {v3, v1, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v1

    iget v2, v7, Lcom/google/android/exoplayer2/source/y$a;->b:I

    iget v3, v7, Lcom/google/android/exoplayer2/source/y$a;->c:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/exoplayer2/sa$a;->a(II)J

    move-result-wide v9

    iget-object v1, v0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    move/from16 v2, p3

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/sa$a;->c(I)I

    move-result v1

    const-wide/16 v2, 0x0

    if-ne v4, v1, :cond_0

    iget-object v1, v0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/sa$a;->b()J

    move-result-wide v4

    goto :goto_0

    :cond_0
    move-wide v4, v2

    :goto_0
    const-wide v11, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, v9, v11

    if-eqz v1, :cond_1

    cmp-long v1, v4, v9

    if-ltz v1, :cond_1

    const-wide/16 v4, 0x1

    sub-long v4, v9, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    move-wide v3, v1

    goto :goto_1

    :cond_1
    move-wide v3, v4

    :goto_1
    new-instance v14, Lcom/google/android/exoplayer2/aa;

    const-wide v11, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v1, v14

    move-object v2, v7

    move-wide/from16 v5, p5

    move-wide v7, v11

    move v11, v13

    move v12, v15

    move/from16 v13, v16

    invoke-direct/range {v1 .. v13}, Lcom/google/android/exoplayer2/aa;-><init>(Lcom/google/android/exoplayer2/source/y$a;JJJJZZZ)V

    return-object v14
.end method

.method private a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;JJJ)Lcom/google/android/exoplayer2/aa;
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    iget-object v5, v0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1, v2, v5}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    iget-object v5, v0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v5, v3, v4}, Lcom/google/android/exoplayer2/sa$a;->a(J)I

    move-result v5

    new-instance v7, Lcom/google/android/exoplayer2/source/y$a;

    move-wide/from16 v8, p7

    invoke-direct {v7, v2, v8, v9, v5}, Lcom/google/android/exoplayer2/source/y$a;-><init>(Ljava/lang/Object;JI)V

    invoke-direct {v0, v7}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/source/y$a;)Z

    move-result v2

    invoke-direct {v0, v1, v7}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;)Z

    move-result v17

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;Z)Z

    move-result v18

    const-wide v8, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v1, -0x1

    if-eq v5, v1, :cond_0

    iget-object v1, v0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1, v5}, Lcom/google/android/exoplayer2/sa$a;->b(I)J

    move-result-wide v5

    move-wide v12, v5

    goto :goto_0

    :cond_0
    move-wide v12, v8

    :goto_0
    cmp-long v1, v12, v8

    if-eqz v1, :cond_2

    const-wide/high16 v5, -0x8000000000000000L

    cmp-long v1, v12, v5

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    move-wide v14, v12

    goto :goto_2

    :cond_2
    :goto_1
    iget-object v1, v0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    iget-wide v5, v1, Lcom/google/android/exoplayer2/sa$a;->d:J

    move-wide v14, v5

    :goto_2
    cmp-long v1, v14, v8

    if-eqz v1, :cond_3

    cmp-long v1, v3, v14

    if-ltz v1, :cond_3

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x1

    sub-long v5, v14, v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    :cond_3
    move-wide v8, v3

    new-instance v1, Lcom/google/android/exoplayer2/aa;

    move-object v6, v1

    move-wide/from16 v10, p5

    move/from16 v16, v2

    invoke-direct/range {v6 .. v18}, Lcom/google/android/exoplayer2/aa;-><init>(Lcom/google/android/exoplayer2/source/y$a;JJJJZZZ)V

    return-object v1
.end method

.method private static a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;JJLcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/source/y$a;
    .locals 6

    invoke-virtual {p0, p1, p6}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p6, p2, p3}, Lcom/google/android/exoplayer2/sa$a;->b(J)I

    move-result v2

    const/4 p0, -0x1

    if-ne v2, p0, :cond_0

    invoke-virtual {p6, p2, p3}, Lcom/google/android/exoplayer2/sa$a;->a(J)I

    move-result p0

    new-instance p2, Lcom/google/android/exoplayer2/source/y$a;

    invoke-direct {p2, p1, p4, p5, p0}, Lcom/google/android/exoplayer2/source/y$a;-><init>(Ljava/lang/Object;JI)V

    return-object p2

    :cond_0
    invoke-virtual {p6, v2}, Lcom/google/android/exoplayer2/sa$a;->c(I)I

    move-result v3

    new-instance p0, Lcom/google/android/exoplayer2/source/y$a;

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/y$a;-><init>(Ljava/lang/Object;IIJ)V

    return-object p0
.end method

.method private a(JJ)Z
    .locals 2

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    cmp-long p1, p1, p3

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private a(Lcom/google/android/exoplayer2/aa;Lcom/google/android/exoplayer2/aa;)Z
    .locals 4

    iget-wide v0, p1, Lcom/google/android/exoplayer2/aa;->b:J

    iget-wide v2, p2, Lcom/google/android/exoplayer2/aa;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object p1, p1, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-object p2, p2, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/source/y$a;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private a(Lcom/google/android/exoplayer2/sa;)Z
    .locals 9

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v2, v0, Lcom/google/android/exoplayer2/Z;->b:Ljava/lang/Object;

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v2

    move v4, v2

    :goto_0
    iget-object v5, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    iget-object v6, p0, Lcom/google/android/exoplayer2/ba;->b:Lcom/google/android/exoplayer2/sa$b;

    iget v7, p0, Lcom/google/android/exoplayer2/ba;->f:I

    iget-boolean v8, p0, Lcom/google/android/exoplayer2/ba;->g:Z

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;Lcom/google/android/exoplayer2/sa$b;IZ)I

    move-result v4

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-boolean v2, v2, Lcom/google/android/exoplayer2/aa;->f:Z

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v2

    const/4 v3, -0x1

    if-eq v4, v3, :cond_4

    if-nez v2, :cond_2

    goto :goto_2

    :cond_2
    iget-object v3, v2, Lcom/google/android/exoplayer2/Z;->b:Ljava/lang/Object;

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v3

    if-eq v3, v4, :cond_3

    goto :goto_2

    :cond_3
    move-object v0, v2

    goto :goto_0

    :cond_4
    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/Z;)Z

    move-result v2

    iget-object v3, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    invoke-virtual {p0, p1, v3}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/aa;)Lcom/google/android/exoplayer2/aa;

    move-result-object p1

    iput-object p1, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    xor-int/lit8 p1, v2, 0x1

    return p1
.end method

.method private a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;)Z
    .locals 3

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/source/y$a;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p2, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p1, v0, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    iget-object p2, p2, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result p2

    iget-object v2, p0, Lcom/google/android/exoplayer2/ba;->b:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {p1, v0, v2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object p1

    iget p1, p1, Lcom/google/android/exoplayer2/sa$b;->o:I

    if-ne p1, p2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;Z)Z
    .locals 6

    iget-object p2, p2, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v1

    iget-object p2, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p1, v1, p2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object p2

    iget p2, p2, Lcom/google/android/exoplayer2/sa$a;->c:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->b:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {p1, p2, v0}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object p2

    iget-boolean p2, p2, Lcom/google/android/exoplayer2/sa$b;->k:Z

    if-nez p2, :cond_0

    iget-object v2, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/ba;->b:Lcom/google/android/exoplayer2/sa$b;

    iget v4, p0, Lcom/google/android/exoplayer2/ba;->f:I

    iget-boolean v5, p0, Lcom/google/android/exoplayer2/ba;->g:Z

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/sa;->b(ILcom/google/android/exoplayer2/sa$a;Lcom/google/android/exoplayer2/sa$b;IZ)Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private a(Lcom/google/android/exoplayer2/source/y$a;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget p1, p1, Lcom/google/android/exoplayer2/source/y$a;->e:I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private h()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->c:Lcom/google/android/exoplayer2/a/a;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/common/collect/r;->f()Lcom/google/common/collect/r$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v2, v2, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v0, v2}, Lcom/google/common/collect/r$a;->a(Ljava/lang/Object;)Lcom/google/common/collect/r$a;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    iget-object v1, v1, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    :goto_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/ba;->d:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/exoplayer2/y;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/exoplayer2/y;-><init>(Lcom/google/android/exoplayer2/ba;Lcom/google/common/collect/r$a;Lcom/google/android/exoplayer2/source/y$a;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/exoplayer2/Z;
    .locals 3
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    if-ne v0, v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->i()V

    iget v0, p0, Lcom/google/android/exoplayer2/ba;->k:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/ba;->k:I

    iget v0, p0, Lcom/google/android/exoplayer2/ba;->k:I

    if-nez v0, :cond_2

    iput-object v1, p0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    iget-object v1, v0, Lcom/google/android/exoplayer2/Z;->b:Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/android/exoplayer2/ba;->l:Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/y$a;->d:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/ba;->m:J

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ba;->h()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    return-object v0
.end method

.method public a([Lcom/google/android/exoplayer2/RendererCapabilities;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/upstream/e;Lcom/google/android/exoplayer2/da;Lcom/google/android/exoplayer2/aa;Lcom/google/android/exoplayer2/trackselection/p;)Lcom/google/android/exoplayer2/Z;
    .locals 11

    move-object v0, p0

    move-object/from16 v8, p5

    iget-object v1, v0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    if-nez v1, :cond_1

    iget-object v1, v8, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v1, v8, Lcom/google/android/exoplayer2/aa;->c:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v3, v1, v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->d()J

    move-result-wide v1

    iget-object v3, v0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    iget-object v3, v3, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-wide v3, v3, Lcom/google/android/exoplayer2/aa;->e:J

    add-long/2addr v1, v3

    iget-wide v3, v8, Lcom/google/android/exoplayer2/aa;->b:J

    sub-long/2addr v1, v3

    :goto_0
    move-wide v3, v1

    new-instance v10, Lcom/google/android/exoplayer2/Z;

    move-object v1, v10

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/exoplayer2/Z;-><init>([Lcom/google/android/exoplayer2/RendererCapabilities;JLcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/upstream/e;Lcom/google/android/exoplayer2/da;Lcom/google/android/exoplayer2/aa;Lcom/google/android/exoplayer2/trackselection/p;)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v10}, Lcom/google/android/exoplayer2/Z;->a(Lcom/google/android/exoplayer2/Z;)V

    goto :goto_1

    :cond_2
    iput-object v10, v0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    iput-object v10, v0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    :goto_1
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/exoplayer2/ba;->l:Ljava/lang/Object;

    iput-object v10, v0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    iget v1, v0, Lcom/google/android/exoplayer2/ba;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/exoplayer2/ba;->k:I

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ba;->h()V

    return-object v10
.end method

.method public a(JLcom/google/android/exoplayer2/fa;)Lcom/google/android/exoplayer2/aa;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    if-nez v0, :cond_0

    invoke-direct {p0, p3}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/fa;)Lcom/google/android/exoplayer2/aa;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p3, p3, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-direct {p0, p3, v0, p1, p2}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/Z;J)Lcom/google/android/exoplayer2/aa;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/aa;)Lcom/google/android/exoplayer2/aa;
    .locals 13

    iget-object v1, p2, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/source/y$a;)Z

    move-result v10

    invoke-direct {p0, p1, v1}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;)Z

    move-result v11

    invoke-direct {p0, p1, v1, v10}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;Z)Z

    move-result v12

    iget-object v0, p2, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p1, v0, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    iget v0, v1, Lcom/google/android/exoplayer2/source/y$a;->b:I

    iget v2, v1, Lcom/google/android/exoplayer2/source/y$a;->c:I

    invoke-virtual {p1, v0, v2}, Lcom/google/android/exoplayer2/sa$a;->a(II)J

    move-result-wide v2

    :cond_0
    :goto_0
    move-wide v8, v2

    goto :goto_1

    :cond_1
    iget-wide v2, p2, Lcom/google/android/exoplayer2/aa;->d:J

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p1, v2, v4

    if-eqz p1, :cond_2

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long p1, v2, v4

    if-nez p1, :cond_0

    :cond_2
    iget-object p1, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/sa$a;->c()J

    move-result-wide v2

    goto :goto_0

    :goto_1
    new-instance p1, Lcom/google/android/exoplayer2/aa;

    iget-wide v2, p2, Lcom/google/android/exoplayer2/aa;->b:J

    iget-wide v4, p2, Lcom/google/android/exoplayer2/aa;->c:J

    iget-wide v6, p2, Lcom/google/android/exoplayer2/aa;->d:J

    move-object v0, p1

    invoke-direct/range {v0 .. v12}, Lcom/google/android/exoplayer2/aa;-><init>(Lcom/google/android/exoplayer2/source/y$a;JJJJZZZ)V

    return-object p1
.end method

.method public a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;J)Lcom/google/android/exoplayer2/source/y$a;
    .locals 7

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/exoplayer2/ba;->a:Lcom/google/android/exoplayer2/sa$a;

    move-object v0, p1

    move-object v1, p2

    move-wide v2, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;JJLcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/source/y$a;

    move-result-object p1

    return-object p1
.end method

.method public a(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/Z;->b(J)V

    :cond_0
    return-void
.end method

.method public synthetic a(Lcom/google/common/collect/r$a;Lcom/google/android/exoplayer2/source/y$a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->c:Lcom/google/android/exoplayer2/a/a;

    invoke-virtual {p1}, Lcom/google/common/collect/r$a;->a()Lcom/google/common/collect/r;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/a/a;->a(Ljava/util/List;Lcom/google/android/exoplayer2/source/y$a;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/Z;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    move v2, v0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-object v2, p0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    return v1

    :cond_1
    iput-object p1, p0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object p1

    iget-object v2, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    if-ne p1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    iput-object v1, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    move v1, v0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/Z;->i()V

    iget v2, p0, Lcom/google/android/exoplayer2/ba;->k:I

    sub-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/exoplayer2/ba;->k:I

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/Z;->a(Lcom/google/android/exoplayer2/Z;)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ba;->h()V

    return v1
.end method

.method public a(Lcom/google/android/exoplayer2/sa;I)Z
    .locals 0

    iput p2, p0, Lcom/google/android/exoplayer2/ba;->f:I

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;)Z

    move-result p1

    return p1
.end method

.method public a(Lcom/google/android/exoplayer2/sa;JJ)Z
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    const/4 v1, 0x0

    :goto_0
    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    const/4 v2, 0x1

    if-eqz v1, :cond_8

    iget-object v3, v1, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, v3}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/aa;)Lcom/google/android/exoplayer2/aa;

    move-result-object v0

    goto :goto_1

    :cond_0
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/Z;J)Lcom/google/android/exoplayer2/aa;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/Z;)Z

    move-result p1

    xor-int/2addr p1, v2

    return p1

    :cond_1
    invoke-direct {p0, v3, v4}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/aa;Lcom/google/android/exoplayer2/aa;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/Z;)Z

    move-result p1

    xor-int/2addr p1, v2

    return p1

    :cond_2
    move-object v0, v4

    :goto_1
    iget-wide v4, v3, Lcom/google/android/exoplayer2/aa;->c:J

    invoke-virtual {v0, v4, v5}, Lcom/google/android/exoplayer2/aa;->a(J)Lcom/google/android/exoplayer2/aa;

    move-result-object v4

    iput-object v4, v1, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-wide v3, v3, Lcom/google/android/exoplayer2/aa;->e:J

    iget-wide v5, v0, Lcom/google/android/exoplayer2/aa;->e:J

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/exoplayer2/ba;->a(JJ)Z

    move-result v3

    if-nez v3, :cond_7

    iget-wide p1, v0, Lcom/google/android/exoplayer2/aa;->e:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p3, p1, v3

    if-nez p3, :cond_3

    const-wide p1, 0x7fffffffffffffffL

    goto :goto_2

    :cond_3
    invoke-virtual {v1, p1, p2}, Lcom/google/android/exoplayer2/Z;->e(J)J

    move-result-wide p1

    :goto_2
    iget-object p3, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    const/4 v0, 0x0

    if-ne v1, p3, :cond_5

    const-wide/high16 v3, -0x8000000000000000L

    cmp-long p3, p4, v3

    if-eqz p3, :cond_4

    cmp-long p1, p4, p1

    if-ltz p1, :cond_5

    :cond_4
    move p1, v2

    goto :goto_3

    :cond_5
    move p1, v0

    :goto_3
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/Z;)Z

    move-result p2

    if-nez p2, :cond_6

    if-nez p1, :cond_6

    move v0, v2

    :cond_6
    return v0

    :cond_7
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    goto :goto_0

    :cond_8
    return v2
.end method

.method public a(Lcom/google/android/exoplayer2/sa;Z)Z
    .locals 0

    iput-boolean p2, p0, Lcom/google/android/exoplayer2/ba;->g:Z

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;)Z

    move-result p1

    return p1
.end method

.method public a(Lcom/google/android/exoplayer2/source/x;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/exoplayer2/Z;->a:Lcom/google/android/exoplayer2/source/x;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b()Lcom/google/android/exoplayer2/Z;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ba;->h()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    return-object v0
.end method

.method public c()V
    .locals 3

    iget v0, p0, Lcom/google/android/exoplayer2/ba;->k:I

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/Z;

    iget-object v1, v0, Lcom/google/android/exoplayer2/Z;->b:Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/android/exoplayer2/ba;->l:Ljava/lang/Object;

    iget-object v1, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v1, v1, Lcom/google/android/exoplayer2/source/y$a;->d:J

    iput-wide v1, p0, Lcom/google/android/exoplayer2/ba;->m:J

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->i()V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    iput-object v0, p0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    iput-object v0, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/ba;->k:I

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ba;->h()V

    return-void
.end method

.method public d()Lcom/google/android/exoplayer2/Z;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    return-object v0
.end method

.method public e()Lcom/google/android/exoplayer2/Z;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->h:Lcom/google/android/exoplayer2/Z;

    return-object v0
.end method

.method public f()Lcom/google/android/exoplayer2/Z;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->i:Lcom/google/android/exoplayer2/Z;

    return-object v0
.end method

.method public g()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-boolean v1, v1, Lcom/google/android/exoplayer2/aa;->h:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ba;->j:Lcom/google/android/exoplayer2/Z;

    iget-object v0, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/aa;->e:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/ba;->k:I

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
