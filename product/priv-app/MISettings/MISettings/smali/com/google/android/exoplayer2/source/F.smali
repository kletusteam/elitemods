.class public final Lcom/google/android/exoplayer2/source/F;
.super Lcom/google/android/exoplayer2/source/j;

# interfaces
.implements Lcom/google/android/exoplayer2/source/D$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/F$a;
    }
.end annotation


# instance fields
.field private final g:Lcom/google/android/exoplayer2/W;

.field private final h:Lcom/google/android/exoplayer2/W$d;

.field private final i:Lcom/google/android/exoplayer2/upstream/l$a;

.field private final j:Lcom/google/android/exoplayer2/extractor/m;

.field private final k:Lcom/google/android/exoplayer2/drm/e;

.field private final l:Lcom/google/android/exoplayer2/upstream/x;

.field private final m:I

.field private n:Z

.field private o:J

.field private p:Z

.field private q:Z

.field private r:Lcom/google/android/exoplayer2/upstream/C;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/exoplayer2/W;Lcom/google/android/exoplayer2/upstream/l$a;Lcom/google/android/exoplayer2/extractor/m;Lcom/google/android/exoplayer2/drm/e;Lcom/google/android/exoplayer2/upstream/x;I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/j;-><init>()V

    iget-object v0, p1, Lcom/google/android/exoplayer2/W;->b:Lcom/google/android/exoplayer2/W$d;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/W$d;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/F;->h:Lcom/google/android/exoplayer2/W$d;

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/F;->g:Lcom/google/android/exoplayer2/W;

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/F;->i:Lcom/google/android/exoplayer2/upstream/l$a;

    iput-object p3, p0, Lcom/google/android/exoplayer2/source/F;->j:Lcom/google/android/exoplayer2/extractor/m;

    iput-object p4, p0, Lcom/google/android/exoplayer2/source/F;->k:Lcom/google/android/exoplayer2/drm/e;

    iput-object p5, p0, Lcom/google/android/exoplayer2/source/F;->l:Lcom/google/android/exoplayer2/upstream/x;

    iput p6, p0, Lcom/google/android/exoplayer2/source/F;->m:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/F;->n:Z

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/F;->o:J

    return-void
.end method

.method private i()V
    .locals 9

    new-instance v8, Lcom/google/android/exoplayer2/source/K;

    iget-wide v1, p0, Lcom/google/android/exoplayer2/source/F;->o:J

    iget-boolean v3, p0, Lcom/google/android/exoplayer2/source/F;->p:Z

    iget-boolean v5, p0, Lcom/google/android/exoplayer2/source/F;->q:Z

    iget-object v7, p0, Lcom/google/android/exoplayer2/source/F;->g:Lcom/google/android/exoplayer2/W;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer2/source/K;-><init>(JZZZLjava/lang/Object;Lcom/google/android/exoplayer2/W;)V

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/F;->n:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/exoplayer2/source/E;

    invoke-direct {v0, p0, v8}, Lcom/google/android/exoplayer2/source/E;-><init>(Lcom/google/android/exoplayer2/source/F;Lcom/google/android/exoplayer2/sa;)V

    goto :goto_0

    :cond_0
    move-object v0, v8

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/source/j;->a(Lcom/google/android/exoplayer2/sa;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/exoplayer2/W;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/F;->g:Lcom/google/android/exoplayer2/W;

    return-object v0
.end method

.method public a(Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/upstream/e;J)Lcom/google/android/exoplayer2/source/x;
    .locals 14

    move-object v12, p0

    iget-object v0, v12, Lcom/google/android/exoplayer2/source/F;->i:Lcom/google/android/exoplayer2/upstream/l$a;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/upstream/l$a;->a()Lcom/google/android/exoplayer2/upstream/l;

    move-result-object v2

    iget-object v0, v12, Lcom/google/android/exoplayer2/source/F;->r:Lcom/google/android/exoplayer2/upstream/C;

    if-eqz v0, :cond_0

    invoke-interface {v2, v0}, Lcom/google/android/exoplayer2/upstream/l;->a(Lcom/google/android/exoplayer2/upstream/C;)V

    :cond_0
    new-instance v13, Lcom/google/android/exoplayer2/source/D;

    iget-object v0, v12, Lcom/google/android/exoplayer2/source/F;->h:Lcom/google/android/exoplayer2/W$d;

    iget-object v1, v0, Lcom/google/android/exoplayer2/W$d;->a:Landroid/net/Uri;

    iget-object v3, v12, Lcom/google/android/exoplayer2/source/F;->j:Lcom/google/android/exoplayer2/extractor/m;

    iget-object v4, v12, Lcom/google/android/exoplayer2/source/F;->k:Lcom/google/android/exoplayer2/drm/e;

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/source/j;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/drm/c$a;

    move-result-object v5

    iget-object v6, v12, Lcom/google/android/exoplayer2/source/F;->l:Lcom/google/android/exoplayer2/upstream/x;

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/source/j;->b(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/source/A$a;

    move-result-object v7

    iget-object v0, v12, Lcom/google/android/exoplayer2/source/F;->h:Lcom/google/android/exoplayer2/W$d;

    iget-object v10, v0, Lcom/google/android/exoplayer2/W$d;->e:Ljava/lang/String;

    iget v11, v12, Lcom/google/android/exoplayer2/source/F;->m:I

    move-object v0, v13

    move-object v8, p0

    move-object/from16 v9, p2

    invoke-direct/range {v0 .. v11}, Lcom/google/android/exoplayer2/source/D;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/l;Lcom/google/android/exoplayer2/extractor/m;Lcom/google/android/exoplayer2/drm/e;Lcom/google/android/exoplayer2/drm/c$a;Lcom/google/android/exoplayer2/upstream/x;Lcom/google/android/exoplayer2/source/A$a;Lcom/google/android/exoplayer2/source/D$b;Lcom/google/android/exoplayer2/upstream/e;Ljava/lang/String;I)V

    return-object v13
.end method

.method public a(JZZ)V
    .locals 2

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    iget-wide p1, p0, Lcom/google/android/exoplayer2/source/F;->o:J

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/F;->n:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/F;->o:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/F;->p:Z

    if-ne v0, p3, :cond_1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/F;->q:Z

    if-ne v0, p4, :cond_1

    return-void

    :cond_1
    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/F;->o:J

    iput-boolean p3, p0, Lcom/google/android/exoplayer2/source/F;->p:Z

    iput-boolean p4, p0, Lcom/google/android/exoplayer2/source/F;->q:Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/F;->n:Z

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/F;->i()V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/x;)V
    .locals 0

    check-cast p1, Lcom/google/android/exoplayer2/source/D;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/D;->n()V

    return-void
.end method

.method protected a(Lcom/google/android/exoplayer2/upstream/C;)V
    .locals 0
    .param p1    # Lcom/google/android/exoplayer2/upstream/C;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/F;->r:Lcom/google/android/exoplayer2/upstream/C;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/F;->k:Lcom/google/android/exoplayer2/drm/e;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/drm/e;->prepare()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/F;->i()V

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method protected h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/F;->k:Lcom/google/android/exoplayer2/drm/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/drm/e;->release()V

    return-void
.end method
