.class public final Lcom/google/android/exoplayer2/extractor/ts/q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/ts/m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/ts/q$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/extractor/ts/E;

.field private final b:Z

.field private final c:Z

.field private final d:Lcom/google/android/exoplayer2/extractor/ts/v;

.field private final e:Lcom/google/android/exoplayer2/extractor/ts/v;

.field private final f:Lcom/google/android/exoplayer2/extractor/ts/v;

.field private g:J

.field private final h:[Z

.field private i:Ljava/lang/String;

.field private j:Lcom/google/android/exoplayer2/extractor/TrackOutput;

.field private k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

.field private l:Z

.field private m:J

.field private n:Z

.field private final o:Lcom/google/android/exoplayer2/util/t;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/extractor/ts/E;ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->a:Lcom/google/android/exoplayer2/extractor/ts/E;

    iput-boolean p2, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->b:Z

    iput-boolean p3, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->c:Z

    const/4 p1, 0x3

    new-array p1, p1, [Z

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->h:[Z

    new-instance p1, Lcom/google/android/exoplayer2/extractor/ts/v;

    const/16 p2, 0x80

    const/4 p3, 0x7

    invoke-direct {p1, p3, p2}, Lcom/google/android/exoplayer2/extractor/ts/v;-><init>(II)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    new-instance p1, Lcom/google/android/exoplayer2/extractor/ts/v;

    const/16 p3, 0x8

    invoke-direct {p1, p3, p2}, Lcom/google/android/exoplayer2/extractor/ts/v;-><init>(II)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    new-instance p1, Lcom/google/android/exoplayer2/extractor/ts/v;

    const/4 p3, 0x6

    invoke-direct {p1, p3, p2}, Lcom/google/android/exoplayer2/extractor/ts/v;-><init>(II)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    new-instance p1, Lcom/google/android/exoplayer2/util/t;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/util/t;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->o:Lcom/google/android/exoplayer2/util/t;

    return-void
.end method

.method private a(JIIJ)V
    .locals 7

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/q$a;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0, p4}, Lcom/google/android/exoplayer2/extractor/ts/v;->a(I)Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0, p4}, Lcom/google/android/exoplayer2/extractor/ts/v;->a(I)Z

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->l:Z

    const/4 v1, 0x3

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    iget-object v3, v2, Lcom/google/android/exoplayer2/extractor/ts/v;->d:[B

    iget v2, v2, Lcom/google/android/exoplayer2/extractor/ts/v;->e:I

    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    iget-object v3, v2, Lcom/google/android/exoplayer2/extractor/ts/v;->d:[B

    iget v2, v2, Lcom/google/android/exoplayer2/extractor/ts/v;->e:I

    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    iget-object v3, v2, Lcom/google/android/exoplayer2/extractor/ts/v;->d:[B

    iget v2, v2, Lcom/google/android/exoplayer2/extractor/ts/v;->e:I

    invoke-static {v3, v1, v2}, Lcom/google/android/exoplayer2/util/r;->b([BII)Lcom/google/android/exoplayer2/util/r$b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    iget-object v4, v3, Lcom/google/android/exoplayer2/extractor/ts/v;->d:[B

    iget v3, v3, Lcom/google/android/exoplayer2/extractor/ts/v;->e:I

    invoke-static {v4, v1, v3}, Lcom/google/android/exoplayer2/util/r;->a([BII)Lcom/google/android/exoplayer2/util/r$a;

    move-result-object v1

    iget v3, v2, Lcom/google/android/exoplayer2/util/r$b;->a:I

    iget v4, v2, Lcom/google/android/exoplayer2/util/r$b;->b:I

    iget v5, v2, Lcom/google/android/exoplayer2/util/r$b;->c:I

    invoke-static {v3, v4, v5}, Lcom/google/android/exoplayer2/util/f;->a(III)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->j:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    new-instance v5, Lcom/google/android/exoplayer2/Format$a;

    invoke-direct {v5}, Lcom/google/android/exoplayer2/Format$a;-><init>()V

    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->i:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/exoplayer2/Format$a;->b(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    const-string v6, "video/avc"

    invoke-virtual {v5, v6}, Lcom/google/android/exoplayer2/Format$a;->e(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v5, v3}, Lcom/google/android/exoplayer2/Format$a;->a(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    iget v3, v2, Lcom/google/android/exoplayer2/util/r$b;->e:I

    invoke-virtual {v5, v3}, Lcom/google/android/exoplayer2/Format$a;->o(I)Lcom/google/android/exoplayer2/Format$a;

    iget v3, v2, Lcom/google/android/exoplayer2/util/r$b;->f:I

    invoke-virtual {v5, v3}, Lcom/google/android/exoplayer2/Format$a;->f(I)Lcom/google/android/exoplayer2/Format$a;

    iget v3, v2, Lcom/google/android/exoplayer2/util/r$b;->g:F

    invoke-virtual {v5, v3}, Lcom/google/android/exoplayer2/Format$a;->b(F)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v5, v0}, Lcom/google/android/exoplayer2/Format$a;->a(Ljava/util/List;)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/Format$a;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    invoke-interface {v4, v0}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/Format;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->l:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/extractor/ts/q$a;->a(Lcom/google/android/exoplayer2/util/r$b;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/extractor/ts/q$a;->a(Lcom/google/android/exoplayer2/util/r$a;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->b()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->b()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/ts/v;->d:[B

    iget v0, v0, Lcom/google/android/exoplayer2/extractor/ts/v;->e:I

    invoke-static {v2, v1, v0}, Lcom/google/android/exoplayer2/util/r;->b([BII)Lcom/google/android/exoplayer2/util/r$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/extractor/ts/q$a;->a(Lcom/google/android/exoplayer2/util/r$b;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->b()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/ts/v;->d:[B

    iget v0, v0, Lcom/google/android/exoplayer2/extractor/ts/v;->e:I

    invoke-static {v2, v1, v0}, Lcom/google/android/exoplayer2/util/r;->a([BII)Lcom/google/android/exoplayer2/util/r$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/extractor/ts/q$a;->a(Lcom/google/android/exoplayer2/util/r$a;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->b()V

    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0, p4}, Lcom/google/android/exoplayer2/extractor/ts/v;->a(I)Z

    move-result p4

    if-eqz p4, :cond_4

    iget-object p4, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    iget-object v0, p4, Lcom/google/android/exoplayer2/extractor/ts/v;->d:[B

    iget p4, p4, Lcom/google/android/exoplayer2/extractor/ts/v;->e:I

    invoke-static {v0, p4}, Lcom/google/android/exoplayer2/util/r;->c([BI)I

    move-result p4

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->o:Lcom/google/android/exoplayer2/util/t;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    iget-object v1, v1, Lcom/google/android/exoplayer2/extractor/ts/v;->d:[B

    invoke-virtual {v0, v1, p4}, Lcom/google/android/exoplayer2/util/t;->a([BI)V

    iget-object p4, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->o:Lcom/google/android/exoplayer2/util/t;

    const/4 v0, 0x4

    invoke-virtual {p4, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p4, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->a:Lcom/google/android/exoplayer2/extractor/ts/E;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->o:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p4, p5, p6, v0}, Lcom/google/android/exoplayer2/extractor/ts/E;->a(JLcom/google/android/exoplayer2/util/t;)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    iget-boolean v5, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->l:Z

    iget-boolean v6, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->n:Z

    move-wide v2, p1

    move v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/exoplayer2/extractor/ts/q$a;->a(JIZZ)Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->n:Z

    :cond_5
    return-void
.end method

.method private a(JIJ)V
    .locals 7

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/q$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0, p3}, Lcom/google/android/exoplayer2/extractor/ts/v;->b(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0, p3}, Lcom/google/android/exoplayer2/extractor/ts/v;->b(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0, p3}, Lcom/google/android/exoplayer2/extractor/ts/v;->b(I)V

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    move-wide v2, p1

    move v4, p3

    move-wide v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/exoplayer2/extractor/ts/q$a;->a(JIJ)V

    return-void
.end method

.method private a([BII)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/q$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/extractor/ts/v;->a([BII)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/extractor/ts/v;->a([BII)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/extractor/ts/v;->a([BII)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/extractor/ts/q$a;->a([BII)V

    return-void
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->j:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->g:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->n:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->h:[Z

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/r;->a([Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->d:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->b()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->e:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->b()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->b()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/q$a;->b()V

    :cond_0
    return-void
.end method

.method public a(JI)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->m:J

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->n:Z

    and-int/lit8 p2, p3, 0x2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    or-int/2addr p1, p2

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->n:Z

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;)V
    .locals 4

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->a()V

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->i:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->c()I

    move-result v0

    const/4 v1, 0x2

    invoke-interface {p1, v0, v1}, Lcom/google/android/exoplayer2/extractor/k;->a(II)Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->j:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    new-instance v0, Lcom/google/android/exoplayer2/extractor/ts/q$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->j:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->b:Z

    iget-boolean v3, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->c:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/extractor/ts/q$a;-><init>(Lcom/google/android/exoplayer2/extractor/TrackOutput;ZZ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->k:Lcom/google/android/exoplayer2/extractor/ts/q$a;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->a:Lcom/google/android/exoplayer2/extractor/ts/E;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/ts/E;->a(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/util/t;)V
    .locals 14

    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/ts/q;->c()V

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->g:J

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->g:J

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->j:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v4

    invoke-interface {v3, p1, v4}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/util/t;I)V

    :goto_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->h:[Z

    invoke-static {v2, v0, v1, p1}, Lcom/google/android/exoplayer2/util/r;->a([BII[Z)I

    move-result p1

    if-ne p1, v1, :cond_0

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/exoplayer2/extractor/ts/q;->a([BII)V

    return-void

    :cond_0
    invoke-static {v2, p1}, Lcom/google/android/exoplayer2/util/r;->b([BI)I

    move-result v6

    sub-int v3, p1, v0

    if-lez v3, :cond_1

    invoke-direct {p0, v2, v0, p1}, Lcom/google/android/exoplayer2/extractor/ts/q;->a([BII)V

    :cond_1
    sub-int v10, v1, p1

    iget-wide v4, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->g:J

    int-to-long v7, v10

    sub-long/2addr v4, v7

    if-gez v3, :cond_2

    neg-int v0, v3

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    move v11, v0

    iget-wide v12, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->m:J

    move-object v7, p0

    move-wide v8, v4

    invoke-direct/range {v7 .. v13}, Lcom/google/android/exoplayer2/extractor/ts/q;->a(JIIJ)V

    iget-wide v7, p0, Lcom/google/android/exoplayer2/extractor/ts/q;->m:J

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/exoplayer2/extractor/ts/q;->a(JIJ)V

    add-int/lit8 v0, p1, 0x3

    goto :goto_0
.end method

.method public b()V
    .locals 0

    return-void
.end method
