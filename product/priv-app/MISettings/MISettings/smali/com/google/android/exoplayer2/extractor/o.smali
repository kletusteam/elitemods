.class public final Lcom/google/android/exoplayer2/extractor/o;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/o$a;
    }
.end annotation


# direct methods
.method public static a(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/extractor/q$a;
    .locals 10

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/t;->y()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result v1

    int-to-long v1, v1

    int-to-long v3, v0

    add-long/2addr v1, v3

    div-int/lit8 v0, v0, 0x12

    new-array v3, v0, [J

    new-array v4, v0, [J

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/t;->r()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-nez v8, :cond_0

    invoke-static {v3, v5}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v3

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v4

    goto :goto_1

    :cond_0
    aput-wide v6, v3, v5

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/t;->r()J

    move-result-wide v6

    aput-wide v6, v4, v5

    const/4 v6, 0x2

    invoke-virtual {p0, v6}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result v0

    int-to-long v5, v0

    sub-long/2addr v1, v5

    long-to-int v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    new-instance p0, Lcom/google/android/exoplayer2/extractor/q$a;

    invoke-direct {p0, v3, v4}, Lcom/google/android/exoplayer2/extractor/q$a;-><init>([J[J)V

    return-object p0
.end method

.method public static a(Lcom/google/android/exoplayer2/extractor/i;Z)Lcom/google/android/exoplayer2/metadata/Metadata;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    move-object p1, v0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/google/android/exoplayer2/metadata/id3/h;->a:Lcom/google/android/exoplayer2/metadata/id3/h$a;

    :goto_0
    new-instance v1, Lcom/google/android/exoplayer2/extractor/s;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/extractor/s;-><init>()V

    invoke-virtual {v1, p0, p1}, Lcom/google/android/exoplayer2/extractor/s;->a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/metadata/id3/h$a;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/metadata/Metadata;->c()I

    move-result p1

    if-nez p1, :cond_2

    :cond_1
    move-object p0, v0

    :cond_2
    return-object p0
.end method

.method private static a(Lcom/google/android/exoplayer2/extractor/i;I)Lcom/google/android/exoplayer2/metadata/flac/PictureFrame;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p0, v1, v2, p1}, Lcom/google/android/exoplayer2/extractor/i;->readFully([BII)V

    const/4 p0, 0x4

    invoke-virtual {v0, p0}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result p0

    sget-object p1, Lb/a/a/a/a;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/exoplayer2/util/t;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/google/android/exoplayer2/util/t;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result v7

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result v8

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result v9

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result v10

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result p0

    new-array v11, p0, [B

    invoke-virtual {v0, v11, v2, p0}, Lcom/google/android/exoplayer2/util/t;->a([BII)V

    new-instance p0, Lcom/google/android/exoplayer2/metadata/flac/PictureFrame;

    move-object v3, p0

    invoke-direct/range {v3 .. v11}, Lcom/google/android/exoplayer2/metadata/flac/PictureFrame;-><init>(ILjava/lang/String;Ljava/lang/String;IIII[B)V

    return-object p0
.end method

.method public static a(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {p0, v2, v3, v1}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->x()J

    move-result-wide v0

    const-wide/32 v4, 0x664c6143

    cmp-long p0, v0, v4

    if-nez p0, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method public static a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/o$a;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p0}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    new-instance v0, Lcom/google/android/exoplayer2/util/s;

    const/4 v1, 0x4

    new-array v2, v1, [B

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer2/util/s;-><init>([B)V

    iget-object v2, v0, Lcom/google/android/exoplayer2/util/s;->a:[B

    const/4 v3, 0x0

    invoke-interface {p0, v2, v3, v1}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/s;->e()Z

    move-result v2

    const/4 v3, 0x7

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v3

    const/16 v4, 0x18

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v0

    add-int/2addr v0, v1

    if-nez v3, :cond_0

    invoke-static {p0}, Lcom/google/android/exoplayer2/extractor/o;->d(Lcom/google/android/exoplayer2/extractor/i;)Lcom/google/android/exoplayer2/extractor/q;

    move-result-object p0

    iput-object p0, p1, Lcom/google/android/exoplayer2/extractor/o$a;->a:Lcom/google/android/exoplayer2/extractor/q;

    goto :goto_0

    :cond_0
    iget-object v4, p1, Lcom/google/android/exoplayer2/extractor/o$a;->a:Lcom/google/android/exoplayer2/extractor/q;

    if-eqz v4, :cond_4

    const/4 v5, 0x3

    if-ne v3, v5, :cond_1

    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/extractor/o;->b(Lcom/google/android/exoplayer2/extractor/i;I)Lcom/google/android/exoplayer2/extractor/q$a;

    move-result-object p0

    invoke-virtual {v4, p0}, Lcom/google/android/exoplayer2/extractor/q;->a(Lcom/google/android/exoplayer2/extractor/q$a;)Lcom/google/android/exoplayer2/extractor/q;

    move-result-object p0

    iput-object p0, p1, Lcom/google/android/exoplayer2/extractor/o$a;->a:Lcom/google/android/exoplayer2/extractor/q;

    goto :goto_0

    :cond_1
    if-ne v3, v1, :cond_2

    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/extractor/o;->c(Lcom/google/android/exoplayer2/extractor/i;I)Ljava/util/List;

    move-result-object p0

    invoke-virtual {v4, p0}, Lcom/google/android/exoplayer2/extractor/q;->b(Ljava/util/List;)Lcom/google/android/exoplayer2/extractor/q;

    move-result-object p0

    iput-object p0, p1, Lcom/google/android/exoplayer2/extractor/o$a;->a:Lcom/google/android/exoplayer2/extractor/q;

    goto :goto_0

    :cond_2
    const/4 v1, 0x6

    if-ne v3, v1, :cond_3

    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/extractor/o;->a(Lcom/google/android/exoplayer2/extractor/i;I)Lcom/google/android/exoplayer2/metadata/flac/PictureFrame;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {v4, p0}, Lcom/google/android/exoplayer2/extractor/q;->a(Ljava/util/List;)Lcom/google/android/exoplayer2/extractor/q;

    move-result-object p0

    iput-object p0, p1, Lcom/google/android/exoplayer2/extractor/o$a;->a:Lcom/google/android/exoplayer2/extractor/q;

    goto :goto_0

    :cond_3
    invoke-interface {p0, v0}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    :goto_0
    return v2

    :cond_4
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method public static b(Lcom/google/android/exoplayer2/extractor/i;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p0}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {p0, v2, v3, v1}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->B()I

    move-result v0

    shr-int/lit8 v1, v0, 0x2

    const/16 v2, 0x3ffe

    if-ne v1, v2, :cond_0

    invoke-interface {p0}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    return v0

    :cond_0
    invoke-interface {p0}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    new-instance p0, Lcom/google/android/exoplayer2/ea;

    const-string v0, "First frame does not start with sync code."

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/ea;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static b(Lcom/google/android/exoplayer2/extractor/i;I)Lcom/google/android/exoplayer2/extractor/q$a;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p0, v1, v2, p1}, Lcom/google/android/exoplayer2/extractor/i;->readFully([BII)V

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/o;->a(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/extractor/q$a;

    move-result-object p0

    return-object p0
.end method

.method public static b(Lcom/google/android/exoplayer2/extractor/i;Z)Lcom/google/android/exoplayer2/metadata/Metadata;
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p0}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    invoke-interface {p0}, Lcom/google/android/exoplayer2/extractor/i;->b()J

    move-result-wide v0

    invoke-static {p0, p1}, Lcom/google/android/exoplayer2/extractor/o;->a(Lcom/google/android/exoplayer2/extractor/i;Z)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object p1

    invoke-interface {p0}, Lcom/google/android/exoplayer2/extractor/i;->b()J

    move-result-wide v2

    sub-long/2addr v2, v0

    long-to-int v0, v2

    invoke-interface {p0, v0}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    return-object p1
.end method

.method private static c(Lcom/google/android/exoplayer2/extractor/i;I)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/extractor/i;",
            "I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p0, v1, v2, p1}, Lcom/google/android/exoplayer2/extractor/i;->readFully([BII)V

    const/4 p0, 0x4

    invoke-virtual {v0, p0}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    invoke-static {v0, v2, v2}, Lcom/google/android/exoplayer2/extractor/x;->a(Lcom/google/android/exoplayer2/util/t;ZZ)Lcom/google/android/exoplayer2/extractor/x$b;

    move-result-object p0

    iget-object p0, p0, Lcom/google/android/exoplayer2/extractor/x$b;->b:[Ljava/lang/String;

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static c(Lcom/google/android/exoplayer2/extractor/i;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {p0, v2, v3, v1}, Lcom/google/android/exoplayer2/extractor/i;->readFully([BII)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->x()J

    move-result-wide v0

    const-wide/32 v2, 0x664c6143

    cmp-long p0, v0, v2

    if-nez p0, :cond_0

    return-void

    :cond_0
    new-instance p0, Lcom/google/android/exoplayer2/ea;

    const-string v0, "Failed to read FLAC stream marker."

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/ea;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static d(Lcom/google/android/exoplayer2/extractor/i;)Lcom/google/android/exoplayer2/extractor/q;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x26

    new-array v1, v0, [B

    const/4 v2, 0x0

    invoke-interface {p0, v1, v2, v0}, Lcom/google/android/exoplayer2/extractor/i;->readFully([BII)V

    new-instance p0, Lcom/google/android/exoplayer2/extractor/q;

    const/4 v0, 0x4

    invoke-direct {p0, v1, v0}, Lcom/google/android/exoplayer2/extractor/q;-><init>([BI)V

    return-object p0
.end method
