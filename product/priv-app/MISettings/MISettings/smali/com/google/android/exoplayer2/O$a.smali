.class final Lcom/google/android/exoplayer2/O$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/O;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/da$c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/exoplayer2/source/J;

.field private final c:I

.field private final d:J


# direct methods
.method private constructor <init>(Ljava/util/List;Lcom/google/android/exoplayer2/source/J;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/da$c;",
            ">;",
            "Lcom/google/android/exoplayer2/source/J;",
            "IJ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/O$a;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/google/android/exoplayer2/O$a;->b:Lcom/google/android/exoplayer2/source/J;

    iput p3, p0, Lcom/google/android/exoplayer2/O$a;->c:I

    iput-wide p4, p0, Lcom/google/android/exoplayer2/O$a;->d:J

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/google/android/exoplayer2/source/J;IJLcom/google/android/exoplayer2/N;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/google/android/exoplayer2/O$a;-><init>(Ljava/util/List;Lcom/google/android/exoplayer2/source/J;IJ)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/O$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/O$a;->c:I

    return p0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/O$a;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/O$a;->a:Ljava/util/List;

    return-object p0
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/O$a;)Lcom/google/android/exoplayer2/source/J;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/O$a;->b:Lcom/google/android/exoplayer2/source/J;

    return-object p0
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/O$a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/O$a;->d:J

    return-wide v0
.end method
