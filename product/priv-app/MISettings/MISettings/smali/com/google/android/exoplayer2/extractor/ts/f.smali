.class public final Lcom/google/android/exoplayer2/extractor/ts/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/Extractor;


# static fields
.field public static final a:Lcom/google/android/exoplayer2/extractor/m;


# instance fields
.field private final b:Lcom/google/android/exoplayer2/extractor/ts/g;

.field private final c:Lcom/google/android/exoplayer2/util/t;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/extractor/ts/a;->a:Lcom/google/android/exoplayer2/extractor/ts/a;

    sput-object v0, Lcom/google/android/exoplayer2/extractor/ts/f;->a:Lcom/google/android/exoplayer2/extractor/m;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/exoplayer2/extractor/ts/g;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/ts/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->b:Lcom/google/android/exoplayer2/extractor/ts/g;

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    const/16 v1, 0xae2

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->c:Lcom/google/android/exoplayer2/util/t;

    return-void
.end method

.method static synthetic a()[Lcom/google/android/exoplayer2/extractor/Extractor;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/exoplayer2/extractor/Extractor;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/ts/f;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/extractor/ts/f;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p2

    const/4 v0, 0x0

    const/16 v1, 0xae2

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/exoplayer2/extractor/i;->read([BII)I

    move-result p1

    const/4 p2, -0x1

    if-ne p1, p2, :cond_0

    return p2

    :cond_0
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, p1}, Lcom/google/android/exoplayer2/util/t;->d(I)V

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->d:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->b:Lcom/google/android/exoplayer2/extractor/ts/g;

    const-wide/16 v1, 0x0

    const/4 p2, 0x4

    invoke-virtual {p1, v1, v2, p2}, Lcom/google/android/exoplayer2/extractor/ts/g;->a(JI)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->d:Z

    :cond_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->b:Lcom/google/android/exoplayer2/extractor/ts/g;

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/extractor/ts/g;->a(Lcom/google/android/exoplayer2/util/t;)V

    return v0
.end method

.method public a(JJ)V
    .locals 0

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->d:Z

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->b:Lcom/google/android/exoplayer2/extractor/ts/g;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/extractor/ts/g;->a()V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/k;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/f;->b:Lcom/google/android/exoplayer2/extractor/ts/g;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/exoplayer2/extractor/ts/g;->a(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;)V

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/k;->g()V

    new-instance v0, Lcom/google/android/exoplayer2/extractor/u$b;

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/u$b;-><init>(J)V

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/k;->a(Lcom/google/android/exoplayer2/extractor/u;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    invoke-interface {p1, v4, v2, v1}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->y()I

    move-result v4

    const v5, 0x494433

    if-eq v4, v5, :cond_4

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    invoke-interface {p1, v3}, Lcom/google/android/exoplayer2/extractor/i;->a(I)V

    move v1, v2

    move v4, v3

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v5

    const/4 v6, 0x6

    invoke-interface {p1, v5, v2, v6}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->B()I

    move-result v5

    const/16 v6, 0xb77

    if-eq v5, v6, :cond_1

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    add-int/lit8 v4, v4, 0x1

    sub-int v1, v4, v3

    const/16 v5, 0x2000

    if-lt v1, v5, :cond_0

    return v2

    :cond_0
    invoke-interface {p1, v4}, Lcom/google/android/exoplayer2/extractor/i;->a(I)V

    move v1, v2

    goto :goto_1

    :cond_1
    const/4 v5, 0x1

    add-int/2addr v1, v5

    const/4 v6, 0x4

    if-lt v1, v6, :cond_2

    return v5

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/exoplayer2/audio/Ac3Util;->a([B)I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_3

    return v2

    :cond_3
    add-int/lit8 v5, v5, -0x6

    invoke-interface {p1, v5}, Lcom/google/android/exoplayer2/extractor/i;->a(I)V

    goto :goto_1

    :cond_4
    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->u()I

    move-result v4

    add-int/lit8 v5, v4, 0xa

    add-int/2addr v3, v5

    invoke-interface {p1, v4}, Lcom/google/android/exoplayer2/extractor/i;->a(I)V

    goto :goto_0
.end method

.method public release()V
    .locals 0

    return-void
.end method
