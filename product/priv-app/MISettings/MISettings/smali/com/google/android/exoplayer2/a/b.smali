.class public interface abstract Lcom/google/android/exoplayer2/a/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/a/b$a;
    }
.end annotation


# virtual methods
.method public a(Lcom/google/android/exoplayer2/a/b$a;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;F)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;I)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;II)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;IIIF)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;IJ)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;IJJ)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;ILcom/google/android/exoplayer2/Format;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;ILcom/google/android/exoplayer2/decoder/e;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;ILjava/lang/String;J)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;J)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;JI)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Landroid/view/Surface;)V
    .locals 0
    .param p2    # Landroid/view/Surface;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/Format;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/W;I)V
    .locals 0
    .param p2    # Lcom/google/android/exoplayer2/W;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/audio/o;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/ga;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/metadata/Metadata;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/m;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;Ljava/io/IOException;Z)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Ljava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/b$a;ZI)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/a/b$a;)V
    .locals 0

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/a/b$a;I)V
    .locals 0

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/a/b$a;IJJ)V
    .locals 0

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/a/b$a;ILcom/google/android/exoplayer2/decoder/e;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/Format;)V
    .locals 0

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 0

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/a/b$a;Ljava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/a/b$a;Z)V
    .locals 0

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/a/b$a;ZI)V
    .locals 0

    return-void
.end method

.method public c(Lcom/google/android/exoplayer2/a/b$a;I)V
    .locals 0

    return-void
.end method

.method public c(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 0

    return-void
.end method

.method public c(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0

    return-void
.end method

.method public c(Lcom/google/android/exoplayer2/a/b$a;Z)V
    .locals 0

    return-void
.end method

.method public d(Lcom/google/android/exoplayer2/a/b$a;I)V
    .locals 0

    return-void
.end method

.method public d(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 0

    return-void
.end method

.method public d(Lcom/google/android/exoplayer2/a/b$a;Z)V
    .locals 0

    return-void
.end method

.method public e(Lcom/google/android/exoplayer2/a/b$a;I)V
    .locals 0

    return-void
.end method

.method public e(Lcom/google/android/exoplayer2/a/b$a;Z)V
    .locals 0

    invoke-interface {p0, p1, p2}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Z)V

    return-void
.end method

.method public f(Lcom/google/android/exoplayer2/a/b$a;I)V
    .locals 0

    return-void
.end method
