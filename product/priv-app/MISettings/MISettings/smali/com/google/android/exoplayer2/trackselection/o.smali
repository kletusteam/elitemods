.class public abstract Lcom/google/android/exoplayer2/trackselection/o;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/trackselection/o$a;
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/exoplayer2/trackselection/o$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private b:Lcom/google/android/exoplayer2/upstream/g;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a([Lcom/google/android/exoplayer2/RendererCapabilities;Lcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/sa;)Lcom/google/android/exoplayer2/trackselection/p;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method protected final a()Lcom/google/android/exoplayer2/upstream/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/trackselection/o;->b:Lcom/google/android/exoplayer2/upstream/g;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/upstream/g;

    return-object v0
.end method

.method public final a(Lcom/google/android/exoplayer2/trackselection/o$a;Lcom/google/android/exoplayer2/upstream/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/trackselection/o;->a:Lcom/google/android/exoplayer2/trackselection/o$a;

    iput-object p2, p0, Lcom/google/android/exoplayer2/trackselection/o;->b:Lcom/google/android/exoplayer2/upstream/g;

    return-void
.end method

.method public abstract a(Ljava/lang/Object;)V
.end method

.method protected final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/trackselection/o;->a:Lcom/google/android/exoplayer2/trackselection/o$a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/exoplayer2/trackselection/o$a;->a()V

    :cond_0
    return-void
.end method
