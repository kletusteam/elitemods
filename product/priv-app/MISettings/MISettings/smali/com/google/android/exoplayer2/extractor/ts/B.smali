.class public final Lcom/google/android/exoplayer2/extractor/ts/B;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/Extractor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/ts/B$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/exoplayer2/extractor/m;


# instance fields
.field private final b:Lcom/google/android/exoplayer2/util/C;

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/google/android/exoplayer2/extractor/ts/B$a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/exoplayer2/util/t;

.field private final e:Lcom/google/android/exoplayer2/extractor/ts/A;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:J

.field private j:Lcom/google/android/exoplayer2/extractor/ts/z;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/google/android/exoplayer2/extractor/k;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/extractor/ts/d;->a:Lcom/google/android/exoplayer2/extractor/ts/d;

    sput-object v0, Lcom/google/android/exoplayer2/extractor/ts/B;->a:Lcom/google/android/exoplayer2/extractor/m;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    new-instance v0, Lcom/google/android/exoplayer2/util/C;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/C;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/ts/B;-><init>(Lcom/google/android/exoplayer2/util/C;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/exoplayer2/util/C;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->b:Lcom/google/android/exoplayer2/util/C;

    new-instance p1, Lcom/google/android/exoplayer2/util/t;

    const/16 v0, 0x1000

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->c:Landroid/util/SparseArray;

    new-instance p1, Lcom/google/android/exoplayer2/extractor/ts/A;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/extractor/ts/A;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->e:Lcom/google/android/exoplayer2/extractor/ts/A;

    return-void
.end method

.method private a(J)V
    .locals 7

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->l:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->l:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->e:Lcom/google/android/exoplayer2/extractor/ts/A;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/A;->a()J

    move-result-wide v0

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/exoplayer2/extractor/ts/z;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->e:Lcom/google/android/exoplayer2/extractor/ts/A;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/extractor/ts/A;->b()Lcom/google/android/exoplayer2/util/C;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->e:Lcom/google/android/exoplayer2/extractor/ts/A;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/extractor/ts/A;->a()J

    move-result-wide v3

    move-object v1, v0

    move-wide v5, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/extractor/ts/z;-><init>(Lcom/google/android/exoplayer2/util/C;JJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->j:Lcom/google/android/exoplayer2/extractor/ts/z;

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->k:Lcom/google/android/exoplayer2/extractor/k;

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->j:Lcom/google/android/exoplayer2/extractor/ts/z;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/b;->a()Lcom/google/android/exoplayer2/extractor/u;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/google/android/exoplayer2/extractor/k;->a(Lcom/google/android/exoplayer2/extractor/u;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->k:Lcom/google/android/exoplayer2/extractor/k;

    new-instance p2, Lcom/google/android/exoplayer2/extractor/u$b;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->e:Lcom/google/android/exoplayer2/extractor/ts/A;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/A;->a()J

    move-result-wide v0

    invoke-direct {p2, v0, v1}, Lcom/google/android/exoplayer2/extractor/u$b;-><init>(J)V

    invoke-interface {p1, p2}, Lcom/google/android/exoplayer2/extractor/k;->a(Lcom/google/android/exoplayer2/extractor/u;)V

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic a()[Lcom/google/android/exoplayer2/extractor/Extractor;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/exoplayer2/extractor/Extractor;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/ts/B;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/extractor/ts/B;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->k:Lcom/google/android/exoplayer2/extractor/k;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getLength()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v4, :cond_0

    move v7, v5

    goto :goto_0

    :cond_0
    move v7, v6

    :goto_0
    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->e:Lcom/google/android/exoplayer2/extractor/ts/A;

    invoke-virtual {v7}, Lcom/google/android/exoplayer2/extractor/ts/A;->c()Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->e:Lcom/google/android/exoplayer2/extractor/ts/A;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/ts/A;->a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I

    move-result p1

    return p1

    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/extractor/ts/B;->a(J)V

    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->j:Lcom/google/android/exoplayer2/extractor/ts/z;

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Lcom/google/android/exoplayer2/extractor/b;->b()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->j:Lcom/google/android/exoplayer2/extractor/ts/z;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/b;->a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I

    move-result p1

    return p1

    :cond_2
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    if-eqz v4, :cond_3

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->b()J

    move-result-wide v7

    sub-long/2addr v0, v7

    goto :goto_1

    :cond_3
    move-wide v0, v2

    :goto_1
    cmp-long p2, v0, v2

    const/4 v2, -0x1

    if-eqz p2, :cond_4

    const-wide/16 v3, 0x4

    cmp-long p2, v0, v3

    if-gez p2, :cond_4

    return v2

    :cond_4
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p2

    const/4 v0, 0x4

    invoke-interface {p1, p2, v6, v0, v5}, Lcom/google/android/exoplayer2/extractor/i;->b([BIIZ)Z

    move-result p2

    if-nez p2, :cond_5

    return v2

    :cond_5
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, v6}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result p2

    const/16 v0, 0x1b9

    if-ne p2, v0, :cond_6

    return v2

    :cond_6
    const/16 v0, 0x1ba

    if-ne p2, v0, :cond_7

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p2

    const/16 v0, 0xa

    invoke-interface {p1, p2, v6, v0}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    const/16 v0, 0x9

    invoke-virtual {p2, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result p2

    and-int/lit8 p2, p2, 0x7

    add-int/lit8 p2, p2, 0xe

    invoke-interface {p1, p2}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    return v6

    :cond_7
    const/16 v0, 0x1bb

    const/4 v1, 0x2

    const/4 v2, 0x6

    if-ne p2, v0, :cond_8

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p2

    invoke-interface {p1, p2, v6, v1}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, v6}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->B()I

    move-result p2

    add-int/2addr p2, v2

    invoke-interface {p1, p2}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    return v6

    :cond_8
    and-int/lit16 v0, p2, -0x100

    shr-int/lit8 v0, v0, 0x8

    if-eq v0, v5, :cond_9

    invoke-interface {p1, v5}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    return v6

    :cond_9
    and-int/lit16 p2, p2, 0xff

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/ts/B$a;

    iget-boolean v3, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->f:Z

    if-nez v3, :cond_f

    if-nez v0, :cond_d

    const/4 v3, 0x0

    const/16 v4, 0xbd

    if-ne p2, v4, :cond_a

    new-instance v3, Lcom/google/android/exoplayer2/extractor/ts/g;

    invoke-direct {v3}, Lcom/google/android/exoplayer2/extractor/ts/g;-><init>()V

    iput-boolean v5, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->g:Z

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->i:J

    goto :goto_2

    :cond_a
    and-int/lit16 v4, p2, 0xe0

    const/16 v7, 0xc0

    if-ne v4, v7, :cond_b

    new-instance v3, Lcom/google/android/exoplayer2/extractor/ts/u;

    invoke-direct {v3}, Lcom/google/android/exoplayer2/extractor/ts/u;-><init>()V

    iput-boolean v5, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->g:Z

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->i:J

    goto :goto_2

    :cond_b
    and-int/lit16 v4, p2, 0xf0

    const/16 v7, 0xe0

    if-ne v4, v7, :cond_c

    new-instance v3, Lcom/google/android/exoplayer2/extractor/ts/n;

    invoke-direct {v3}, Lcom/google/android/exoplayer2/extractor/ts/n;-><init>()V

    iput-boolean v5, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->h:Z

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->i:J

    :cond_c
    :goto_2
    if-eqz v3, :cond_d

    new-instance v0, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;

    const/16 v4, 0x100

    invoke-direct {v0, p2, v4}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;-><init>(II)V

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->k:Lcom/google/android/exoplayer2/extractor/k;

    invoke-interface {v3, v4, v0}, Lcom/google/android/exoplayer2/extractor/ts/m;->a(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;)V

    new-instance v0, Lcom/google/android/exoplayer2/extractor/ts/B$a;

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->b:Lcom/google/android/exoplayer2/util/C;

    invoke-direct {v0, v3, v4}, Lcom/google/android/exoplayer2/extractor/ts/B$a;-><init>(Lcom/google/android/exoplayer2/extractor/ts/m;Lcom/google/android/exoplayer2/util/C;)V

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->c:Landroid/util/SparseArray;

    invoke-virtual {v3, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_d
    iget-boolean p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->g:Z

    if-eqz p2, :cond_e

    iget-boolean p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->h:Z

    if-eqz p2, :cond_e

    iget-wide v3, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->i:J

    const-wide/16 v7, 0x2000

    add-long/2addr v3, v7

    goto :goto_3

    :cond_e
    const-wide/32 v3, 0x100000

    :goto_3
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v7

    cmp-long p2, v7, v3

    if-lez p2, :cond_f

    iput-boolean v5, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->f:Z

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->k:Lcom/google/android/exoplayer2/extractor/k;

    invoke-interface {p2}, Lcom/google/android/exoplayer2/extractor/k;->g()V

    :cond_f
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p2

    invoke-interface {p1, p2, v6, v1}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, v6}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->B()I

    move-result p2

    add-int/2addr p2, v2

    if-nez v0, :cond_10

    invoke-interface {p1, p2}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    goto :goto_4

    :cond_10
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v1, p2}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v1

    invoke-interface {p1, v1, v6, p2}, Lcom/google/android/exoplayer2/extractor/i;->readFully([BII)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/extractor/ts/B$a;->a(Lcom/google/android/exoplayer2/util/t;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->b()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/util/t;->d(I)V

    :goto_4
    return v6
.end method

.method public a(JJ)V
    .locals 4

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->b:Lcom/google/android/exoplayer2/util/C;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/C;->c()J

    move-result-wide p1

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p1, p1, v0

    const/4 p2, 0x0

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    move p1, p2

    :goto_0
    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->b:Lcom/google/android/exoplayer2/util/C;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/C;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->b:Lcom/google/android/exoplayer2/util/C;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/C;->a()J

    move-result-wide v0

    cmp-long p1, v0, p3

    if-eqz p1, :cond_2

    :cond_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->b:Lcom/google/android/exoplayer2/util/C;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/C;->d()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->b:Lcom/google/android/exoplayer2/util/C;

    invoke-virtual {p1, p3, p4}, Lcom/google/android/exoplayer2/util/C;->d(J)V

    :cond_2
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->j:Lcom/google/android/exoplayer2/extractor/ts/z;

    if-eqz p1, :cond_3

    invoke-virtual {p1, p3, p4}, Lcom/google/android/exoplayer2/extractor/b;->b(J)V

    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->c:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result p1

    if-ge p2, p1, :cond_4

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->c:Landroid/util/SparseArray;

    invoke-virtual {p1, p2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/exoplayer2/extractor/ts/B$a;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/extractor/ts/B$a;->a()V

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_4
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/k;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/B;->k:Lcom/google/android/exoplayer2/extractor/k;

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0xe

    new-array v1, v0, [B

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2, v0}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    aget-byte v0, v1, v2

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    const/4 v3, 0x1

    aget-byte v4, v1, v3

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v0, v4

    const/4 v4, 0x2

    aget-byte v5, v1, v4

    and-int/lit16 v5, v5, 0xff

    const/16 v6, 0x8

    shl-int/2addr v5, v6

    or-int/2addr v0, v5

    const/4 v5, 0x3

    aget-byte v7, v1, v5

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v0, v7

    const/16 v7, 0x1ba

    if-eq v7, v0, :cond_0

    return v2

    :cond_0
    const/4 v0, 0x4

    aget-byte v7, v1, v0

    and-int/lit16 v7, v7, 0xc4

    const/16 v8, 0x44

    if-eq v7, v8, :cond_1

    return v2

    :cond_1
    const/4 v7, 0x6

    aget-byte v7, v1, v7

    and-int/2addr v7, v0

    if-eq v7, v0, :cond_2

    return v2

    :cond_2
    aget-byte v7, v1, v6

    and-int/2addr v7, v0

    if-eq v7, v0, :cond_3

    return v2

    :cond_3
    const/16 v0, 0x9

    aget-byte v0, v1, v0

    and-int/2addr v0, v3

    if-eq v0, v3, :cond_4

    return v2

    :cond_4
    const/16 v0, 0xc

    aget-byte v0, v1, v0

    and-int/2addr v0, v5

    if-eq v0, v5, :cond_5

    return v2

    :cond_5
    const/16 v0, 0xd

    aget-byte v0, v1, v0

    and-int/lit8 v0, v0, 0x7

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/i;->a(I)V

    invoke-interface {p1, v1, v2, v5}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    aget-byte p1, v1, v2

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x10

    aget-byte v0, v1, v3

    and-int/lit16 v0, v0, 0xff

    shl-int/2addr v0, v6

    or-int/2addr p1, v0

    aget-byte v0, v1, v4

    and-int/lit16 v0, v0, 0xff

    or-int/2addr p1, v0

    if-ne v3, p1, :cond_6

    move v2, v3

    :cond_6
    return v2
.end method

.method public release()V
    .locals 0

    return-void
.end method
