.class final Lcom/google/android/exoplayer2/source/D$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/upstream/z$d;
.implements Lcom/google/android/exoplayer2/source/s$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/source/D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation


# instance fields
.field private final a:J

.field private final b:Landroid/net/Uri;

.field private final c:Lcom/google/android/exoplayer2/upstream/B;

.field private final d:Lcom/google/android/exoplayer2/source/C;

.field private final e:Lcom/google/android/exoplayer2/extractor/k;

.field private final f:Lcom/google/android/exoplayer2/util/h;

.field private final g:Lcom/google/android/exoplayer2/extractor/t;

.field private volatile h:Z

.field private i:Z

.field private j:J

.field private k:Lcom/google/android/exoplayer2/upstream/DataSpec;

.field private l:J

.field private m:Lcom/google/android/exoplayer2/extractor/TrackOutput;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private n:Z

.field final synthetic o:Lcom/google/android/exoplayer2/source/D;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/source/D;Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/l;Lcom/google/android/exoplayer2/source/C;Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/util/h;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/D$a;->b:Landroid/net/Uri;

    new-instance p1, Lcom/google/android/exoplayer2/upstream/B;

    invoke-direct {p1, p3}, Lcom/google/android/exoplayer2/upstream/B;-><init>(Lcom/google/android/exoplayer2/upstream/l;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D$a;->c:Lcom/google/android/exoplayer2/upstream/B;

    iput-object p4, p0, Lcom/google/android/exoplayer2/source/D$a;->d:Lcom/google/android/exoplayer2/source/C;

    iput-object p5, p0, Lcom/google/android/exoplayer2/source/D$a;->e:Lcom/google/android/exoplayer2/extractor/k;

    iput-object p6, p0, Lcom/google/android/exoplayer2/source/D$a;->f:Lcom/google/android/exoplayer2/util/h;

    new-instance p1, Lcom/google/android/exoplayer2/extractor/t;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/extractor/t;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D$a;->g:Lcom/google/android/exoplayer2/extractor/t;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/D$a;->i:Z

    const-wide/16 p1, -0x1

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/D$a;->l:J

    invoke-static {}, Lcom/google/android/exoplayer2/source/t;->a()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/D$a;->a:J

    const-wide/16 p1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/D$a;->a(J)Lcom/google/android/exoplayer2/upstream/DataSpec;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D$a;->k:Lcom/google/android/exoplayer2/upstream/DataSpec;

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/D$a;)Lcom/google/android/exoplayer2/upstream/B;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/source/D$a;->c:Lcom/google/android/exoplayer2/upstream/B;

    return-object p0
.end method

.method private a(J)Lcom/google/android/exoplayer2/upstream/DataSpec;
    .locals 2

    new-instance v0, Lcom/google/android/exoplayer2/upstream/DataSpec$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/upstream/DataSpec$a;-><init>()V

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/D$a;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/upstream/DataSpec$a;->a(Landroid/net/Uri;)Lcom/google/android/exoplayer2/upstream/DataSpec$a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/upstream/DataSpec$a;->a(J)Lcom/google/android/exoplayer2/upstream/DataSpec$a;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    invoke-static {p1}, Lcom/google/android/exoplayer2/source/D;->d(Lcom/google/android/exoplayer2/source/D;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/upstream/DataSpec$a;->a(Ljava/lang/String;)Lcom/google/android/exoplayer2/upstream/DataSpec$a;

    const/4 p1, 0x6

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/upstream/DataSpec$a;->a(I)Lcom/google/android/exoplayer2/upstream/DataSpec$a;

    invoke-static {}, Lcom/google/android/exoplayer2/source/D;->i()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/upstream/DataSpec$a;->a(Ljava/util/Map;)Lcom/google/android/exoplayer2/upstream/DataSpec$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/DataSpec$a;->a()Lcom/google/android/exoplayer2/upstream/DataSpec;

    move-result-object p1

    return-object p1
.end method

.method private a(JJ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D$a;->g:Lcom/google/android/exoplayer2/extractor/t;

    iput-wide p1, v0, Lcom/google/android/exoplayer2/extractor/t;->a:J

    iput-wide p3, p0, Lcom/google/android/exoplayer2/source/D$a;->j:J

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/D$a;->i:Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/D$a;->n:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/D$a;JJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/source/D$a;->a(JJ)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/source/D$a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/D$a;->a:J

    return-wide v0
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/source/D$a;)Lcom/google/android/exoplayer2/upstream/DataSpec;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/source/D$a;->k:Lcom/google/android/exoplayer2/upstream/DataSpec;

    return-object p0
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/source/D$a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/D$a;->j:J

    return-wide v0
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/source/D$a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/D$a;->l:J

    return-wide v0
.end method


# virtual methods
.method public a()V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-nez v2, :cond_8

    iget-boolean v3, v1, Lcom/google/android/exoplayer2/source/D$a;->h:Z

    if-nez v3, :cond_8

    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    :try_start_0
    iget-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->g:Lcom/google/android/exoplayer2/extractor/t;

    iget-wide v13, v6, Lcom/google/android/exoplayer2/extractor/t;->a:J

    invoke-direct {v1, v13, v14}, Lcom/google/android/exoplayer2/source/D$a;->a(J)Lcom/google/android/exoplayer2/upstream/DataSpec;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->k:Lcom/google/android/exoplayer2/upstream/DataSpec;

    iget-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->c:Lcom/google/android/exoplayer2/upstream/B;

    iget-object v7, v1, Lcom/google/android/exoplayer2/source/D$a;->k:Lcom/google/android/exoplayer2/upstream/DataSpec;

    invoke-virtual {v6, v7}, Lcom/google/android/exoplayer2/upstream/B;->a(Lcom/google/android/exoplayer2/upstream/DataSpec;)J

    move-result-wide v6

    iput-wide v6, v1, Lcom/google/android/exoplayer2/source/D$a;->l:J

    iget-wide v6, v1, Lcom/google/android/exoplayer2/source/D$a;->l:J

    cmp-long v6, v6, v4

    if-eqz v6, :cond_0

    iget-wide v6, v1, Lcom/google/android/exoplayer2/source/D$a;->l:J

    add-long/2addr v6, v13

    iput-wide v6, v1, Lcom/google/android/exoplayer2/source/D$a;->l:J

    :cond_0
    iget-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    iget-object v7, v1, Lcom/google/android/exoplayer2/source/D$a;->c:Lcom/google/android/exoplayer2/upstream/B;

    invoke-virtual {v7}, Lcom/google/android/exoplayer2/upstream/B;->a()Ljava/util/Map;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;->a(Ljava/util/Map;)Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/exoplayer2/source/D;->a(Lcom/google/android/exoplayer2/source/D;Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;)Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    iget-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->c:Lcom/google/android/exoplayer2/upstream/B;

    iget-object v7, v1, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    invoke-static {v7}, Lcom/google/android/exoplayer2/source/D;->e(Lcom/google/android/exoplayer2/source/D;)Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    move-result-object v7

    if-eqz v7, :cond_1

    iget-object v7, v1, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    invoke-static {v7}, Lcom/google/android/exoplayer2/source/D;->e(Lcom/google/android/exoplayer2/source/D;)Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    move-result-object v7

    iget v7, v7, Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;->f:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_1

    new-instance v6, Lcom/google/android/exoplayer2/source/s;

    iget-object v7, v1, Lcom/google/android/exoplayer2/source/D$a;->c:Lcom/google/android/exoplayer2/upstream/B;

    iget-object v8, v1, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    invoke-static {v8}, Lcom/google/android/exoplayer2/source/D;->e(Lcom/google/android/exoplayer2/source/D;)Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    move-result-object v8

    iget v8, v8, Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;->f:I

    invoke-direct {v6, v7, v8, v1}, Lcom/google/android/exoplayer2/source/s;-><init>(Lcom/google/android/exoplayer2/upstream/l;ILcom/google/android/exoplayer2/source/s$a;)V

    iget-object v7, v1, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    invoke-virtual {v7}, Lcom/google/android/exoplayer2/source/D;->k()Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object v7

    iput-object v7, v1, Lcom/google/android/exoplayer2/source/D$a;->m:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-object v7, v1, Lcom/google/android/exoplayer2/source/D$a;->m:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-static {}, Lcom/google/android/exoplayer2/source/D;->j()Lcom/google/android/exoplayer2/Format;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/Format;)V

    :cond_1
    move-object v8, v6

    iget-object v7, v1, Lcom/google/android/exoplayer2/source/D$a;->d:Lcom/google/android/exoplayer2/source/C;

    iget-object v9, v1, Lcom/google/android/exoplayer2/source/D$a;->b:Landroid/net/Uri;

    iget-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->c:Lcom/google/android/exoplayer2/upstream/B;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/upstream/B;->a()Ljava/util/Map;

    move-result-object v10

    iget-wide v11, v1, Lcom/google/android/exoplayer2/source/D$a;->l:J

    iget-object v15, v1, Lcom/google/android/exoplayer2/source/D$a;->e:Lcom/google/android/exoplayer2/extractor/k;

    move-wide/from16 v16, v11

    move-wide v11, v13

    move-wide v4, v13

    move-wide/from16 v13, v16

    invoke-interface/range {v7 .. v15}, Lcom/google/android/exoplayer2/source/C;->a(Lcom/google/android/exoplayer2/upstream/j;Landroid/net/Uri;Ljava/util/Map;JJLcom/google/android/exoplayer2/extractor/k;)V

    iget-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    invoke-static {v6}, Lcom/google/android/exoplayer2/source/D;->e(Lcom/google/android/exoplayer2/source/D;)Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->d:Lcom/google/android/exoplayer2/source/C;

    invoke-interface {v6}, Lcom/google/android/exoplayer2/source/C;->b()V

    :cond_2
    iget-boolean v6, v1, Lcom/google/android/exoplayer2/source/D$a;->i:Z

    if-eqz v6, :cond_3

    iget-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->d:Lcom/google/android/exoplayer2/source/C;

    iget-wide v7, v1, Lcom/google/android/exoplayer2/source/D$a;->j:J

    invoke-interface {v6, v4, v5, v7, v8}, Lcom/google/android/exoplayer2/source/C;->a(JJ)V

    iput-boolean v0, v1, Lcom/google/android/exoplayer2/source/D$a;->i:Z

    :cond_3
    :goto_1
    if-nez v2, :cond_4

    iget-boolean v6, v1, Lcom/google/android/exoplayer2/source/D$a;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v6, :cond_4

    :try_start_1
    iget-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->f:Lcom/google/android/exoplayer2/util/h;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/util/h;->a()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->d:Lcom/google/android/exoplayer2/source/C;

    iget-object v7, v1, Lcom/google/android/exoplayer2/source/D$a;->g:Lcom/google/android/exoplayer2/extractor/t;

    invoke-interface {v6, v7}, Lcom/google/android/exoplayer2/source/C;->a(Lcom/google/android/exoplayer2/extractor/t;)I

    move-result v2

    iget-object v6, v1, Lcom/google/android/exoplayer2/source/D$a;->d:Lcom/google/android/exoplayer2/source/C;

    invoke-interface {v6}, Lcom/google/android/exoplayer2/source/C;->a()J

    move-result-wide v6

    iget-object v8, v1, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    invoke-static {v8}, Lcom/google/android/exoplayer2/source/D;->f(Lcom/google/android/exoplayer2/source/D;)J

    move-result-wide v8

    add-long/2addr v8, v4

    cmp-long v8, v6, v8

    if-lez v8, :cond_3

    iget-object v4, v1, Lcom/google/android/exoplayer2/source/D$a;->f:Lcom/google/android/exoplayer2/util/h;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/h;->b()Z

    iget-object v4, v1, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    invoke-static {v4}, Lcom/google/android/exoplayer2/source/D;->b(Lcom/google/android/exoplayer2/source/D;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    invoke-static {v5}, Lcom/google/android/exoplayer2/source/D;->a(Lcom/google/android/exoplayer2/source/D;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-wide v4, v6

    goto :goto_1

    :catch_0
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    if-ne v2, v3, :cond_5

    move v2, v0

    goto :goto_2

    :cond_5
    iget-object v3, v1, Lcom/google/android/exoplayer2/source/D$a;->d:Lcom/google/android/exoplayer2/source/C;

    invoke-interface {v3}, Lcom/google/android/exoplayer2/source/C;->a()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_6

    iget-object v3, v1, Lcom/google/android/exoplayer2/source/D$a;->g:Lcom/google/android/exoplayer2/extractor/t;

    iget-object v4, v1, Lcom/google/android/exoplayer2/source/D$a;->d:Lcom/google/android/exoplayer2/source/C;

    invoke-interface {v4}, Lcom/google/android/exoplayer2/source/C;->a()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/exoplayer2/extractor/t;->a:J

    :cond_6
    :goto_2
    iget-object v3, v1, Lcom/google/android/exoplayer2/source/D$a;->c:Lcom/google/android/exoplayer2/upstream/B;

    invoke-static {v3}, Lcom/google/android/exoplayer2/util/E;->a(Lcom/google/android/exoplayer2/upstream/l;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eq v2, v3, :cond_7

    iget-object v2, v1, Lcom/google/android/exoplayer2/source/D$a;->d:Lcom/google/android/exoplayer2/source/C;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/source/C;->a()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    iget-object v2, v1, Lcom/google/android/exoplayer2/source/D$a;->g:Lcom/google/android/exoplayer2/extractor/t;

    iget-object v3, v1, Lcom/google/android/exoplayer2/source/D$a;->d:Lcom/google/android/exoplayer2/source/C;

    invoke-interface {v3}, Lcom/google/android/exoplayer2/source/C;->a()J

    move-result-wide v3

    iput-wide v3, v2, Lcom/google/android/exoplayer2/extractor/t;->a:J

    :cond_7
    iget-object v2, v1, Lcom/google/android/exoplayer2/source/D$a;->c:Lcom/google/android/exoplayer2/upstream/B;

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/E;->a(Lcom/google/android/exoplayer2/upstream/l;)V

    throw v0

    :cond_8
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/util/t;)V
    .locals 9

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D$a;->n:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/D$a;->j:J

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D$a;->o:Lcom/google/android/exoplayer2/source/D;

    invoke-static {v0}, Lcom/google/android/exoplayer2/source/D;->c(Lcom/google/android/exoplayer2/source/D;)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/D$a;->j:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :goto_0
    move-wide v3, v0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v6

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D$a;->m:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-interface {v2, p1, v6}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/util/t;I)V

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(JIIILcom/google/android/exoplayer2/extractor/TrackOutput$a;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/D$a;->n:Z

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/D$a;->h:Z

    return-void
.end method
