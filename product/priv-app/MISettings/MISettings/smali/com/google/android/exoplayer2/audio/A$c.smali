.class public Lcom/google/android/exoplayer2/audio/A$c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/audio/A$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/audio/A;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field private final a:[Lcom/google/android/exoplayer2/audio/r;

.field private final b:Lcom/google/android/exoplayer2/audio/I;

.field private final c:Lcom/google/android/exoplayer2/audio/K;


# direct methods
.method public varargs constructor <init>([Lcom/google/android/exoplayer2/audio/r;)V
    .locals 2

    new-instance v0, Lcom/google/android/exoplayer2/audio/I;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/audio/I;-><init>()V

    new-instance v1, Lcom/google/android/exoplayer2/audio/K;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/audio/K;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/exoplayer2/audio/A$c;-><init>([Lcom/google/android/exoplayer2/audio/r;Lcom/google/android/exoplayer2/audio/I;Lcom/google/android/exoplayer2/audio/K;)V

    return-void
.end method

.method public constructor <init>([Lcom/google/android/exoplayer2/audio/r;Lcom/google/android/exoplayer2/audio/I;Lcom/google/android/exoplayer2/audio/K;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    array-length v0, p1

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Lcom/google/android/exoplayer2/audio/r;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A$c;->a:[Lcom/google/android/exoplayer2/audio/r;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A$c;->a:[Lcom/google/android/exoplayer2/audio/r;

    array-length v1, p1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p2, p0, Lcom/google/android/exoplayer2/audio/A$c;->b:Lcom/google/android/exoplayer2/audio/I;

    iput-object p3, p0, Lcom/google/android/exoplayer2/audio/A$c;->c:Lcom/google/android/exoplayer2/audio/K;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A$c;->a:[Lcom/google/android/exoplayer2/audio/r;

    array-length v1, p1

    aput-object p2, v0, v1

    array-length p1, p1

    add-int/lit8 p1, p1, 0x1

    aput-object p3, v0, p1

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A$c;->b:Lcom/google/android/exoplayer2/audio/I;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/I;->h()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(J)J
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A$c;->c:Lcom/google/android/exoplayer2/audio/K;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/audio/K;->a(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(Lcom/google/android/exoplayer2/ga;)Lcom/google/android/exoplayer2/ga;
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A$c;->c:Lcom/google/android/exoplayer2/audio/K;

    iget v1, p1, Lcom/google/android/exoplayer2/ga;->b:F

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/audio/K;->b(F)F

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A$c;->c:Lcom/google/android/exoplayer2/audio/K;

    iget p1, p1, Lcom/google/android/exoplayer2/ga;->c:F

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/audio/K;->a(F)F

    new-instance v0, Lcom/google/android/exoplayer2/ga;

    invoke-direct {v0, v1, p1}, Lcom/google/android/exoplayer2/ga;-><init>(FF)V

    return-object v0
.end method

.method public a(Z)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A$c;->b:Lcom/google/android/exoplayer2/audio/I;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/audio/I;->a(Z)V

    return p1
.end method

.method public b()[Lcom/google/android/exoplayer2/audio/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A$c;->a:[Lcom/google/android/exoplayer2/audio/r;

    return-object v0
.end method
