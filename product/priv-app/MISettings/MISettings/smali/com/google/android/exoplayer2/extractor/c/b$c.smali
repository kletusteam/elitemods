.class final Lcom/google/android/exoplayer2/extractor/c/b$c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/c/b$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/extractor/c/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "c"
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/extractor/k;

.field private final b:Lcom/google/android/exoplayer2/extractor/TrackOutput;

.field private final c:Lcom/google/android/exoplayer2/extractor/c/c;

.field private final d:Lcom/google/android/exoplayer2/Format;

.field private final e:I

.field private f:J

.field private g:I

.field private h:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/TrackOutput;Lcom/google/android/exoplayer2/extractor/c/c;Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ea;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->a:Lcom/google/android/exoplayer2/extractor/k;

    iput-object p2, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iput-object p3, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->c:Lcom/google/android/exoplayer2/extractor/c/c;

    iget p1, p3, Lcom/google/android/exoplayer2/extractor/c/c;->b:I

    iget p2, p3, Lcom/google/android/exoplayer2/extractor/c/c;->f:I

    mul-int/2addr p1, p2

    div-int/lit8 p1, p1, 0x8

    iget p2, p3, Lcom/google/android/exoplayer2/extractor/c/c;->e:I

    if-ne p2, p1, :cond_0

    iget p2, p3, Lcom/google/android/exoplayer2/extractor/c/c;->c:I

    mul-int v0, p2, p1

    mul-int/lit8 v0, v0, 0x8

    mul-int/2addr p2, p1

    div-int/lit8 p2, p2, 0xa

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->e:I

    new-instance p1, Lcom/google/android/exoplayer2/Format$a;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/Format$a;-><init>()V

    invoke-virtual {p1, p4}, Lcom/google/android/exoplayer2/Format$a;->e(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/Format$a;->b(I)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/Format$a;->j(I)Lcom/google/android/exoplayer2/Format$a;

    iget p2, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->e:I

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/Format$a;->h(I)Lcom/google/android/exoplayer2/Format$a;

    iget p2, p3, Lcom/google/android/exoplayer2/extractor/c/c;->b:I

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/Format$a;->c(I)Lcom/google/android/exoplayer2/Format$a;

    iget p2, p3, Lcom/google/android/exoplayer2/extractor/c/c;->c:I

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/Format$a;->l(I)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {p1, p5}, Lcom/google/android/exoplayer2/Format$a;->i(I)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/Format$a;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->d:Lcom/google/android/exoplayer2/Format;

    return-void

    :cond_0
    new-instance p3, Lcom/google/android/exoplayer2/ea;

    const/16 p4, 0x32

    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5, p4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string p4, "Expected block size: "

    invoke-virtual {p5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "; got: "

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/google/android/exoplayer2/ea;-><init>(Ljava/lang/String;)V

    throw p3
.end method


# virtual methods
.method public a(IJ)V
    .locals 9

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->a:Lcom/google/android/exoplayer2/extractor/k;

    new-instance v8, Lcom/google/android/exoplayer2/extractor/c/e;

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->c:Lcom/google/android/exoplayer2/extractor/c/c;

    int-to-long v4, p1

    const/4 v3, 0x1

    move-object v1, v8

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer2/extractor/c/e;-><init>(Lcom/google/android/exoplayer2/extractor/c/c;IJJ)V

    invoke-interface {v0, v8}, Lcom/google/android/exoplayer2/extractor/k;->a(Lcom/google/android/exoplayer2/extractor/u;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->d:Lcom/google/android/exoplayer2/Format;

    invoke-interface {p1, p2}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/Format;)V

    return-void
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->f:J

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->g:I

    const-wide/16 p1, 0x0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/c/b$c;->h:J

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;J)Z
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    :goto_0
    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    const/4 v6, 0x1

    if-lez v5, :cond_1

    iget v7, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->g:I

    iget v8, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->e:I

    if-ge v7, v8, :cond_1

    sub-int/2addr v8, v7

    int-to-long v7, v8

    invoke-static {v7, v8, v1, v2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    long-to-int v5, v7

    iget-object v7, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-object/from16 v8, p1

    invoke-interface {v7, v8, v5, v6}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/upstream/j;IZ)I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    move-wide v1, v3

    goto :goto_0

    :cond_0
    iget v3, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->g:I

    add-int/2addr v3, v5

    iput v3, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->g:I

    int-to-long v3, v5

    sub-long/2addr v1, v3

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->c:Lcom/google/android/exoplayer2/extractor/c/c;

    iget v2, v1, Lcom/google/android/exoplayer2/extractor/c/c;->e:I

    iget v3, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->g:I

    div-int/2addr v3, v2

    if-lez v3, :cond_2

    iget-wide v7, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->f:J

    iget-wide v9, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->h:J

    const-wide/32 v11, 0xf4240

    iget v1, v1, Lcom/google/android/exoplayer2/extractor/c/c;->c:I

    int-to-long v13, v1

    invoke-static/range {v9 .. v14}, Lcom/google/android/exoplayer2/util/E;->c(JJJ)J

    move-result-wide v9

    add-long v12, v7, v9

    mul-int v15, v3, v2

    iget v1, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->g:I

    sub-int/2addr v1, v15

    iget-object v11, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    const/4 v14, 0x1

    const/16 v17, 0x0

    move/from16 v16, v1

    invoke-interface/range {v11 .. v17}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(JIIILcom/google/android/exoplayer2/extractor/TrackOutput$a;)V

    iget-wide v7, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->h:J

    int-to-long v2, v3

    add-long/2addr v7, v2

    iput-wide v7, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->h:J

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->g:I

    :cond_2
    if-gtz v5, :cond_3

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    :goto_1
    return v6
.end method
