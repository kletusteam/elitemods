.class public final Lcom/google/android/exoplayer2/metadata/scte35/b;
.super Lcom/google/android/exoplayer2/metadata/h;


# instance fields
.field private final a:Lcom/google/android/exoplayer2/util/t;

.field private final b:Lcom/google/android/exoplayer2/util/s;

.field private c:Lcom/google/android/exoplayer2/util/C;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/metadata/h;-><init>()V

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->a:Lcom/google/android/exoplayer2/util/t;

    new-instance v0, Lcom/google/android/exoplayer2/util/s;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->b:Lcom/google/android/exoplayer2/util/s;

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/exoplayer2/metadata/e;Ljava/nio/ByteBuffer;)Lcom/google/android/exoplayer2/metadata/Metadata;
    .locals 6

    iget-object v0, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->c:Lcom/google/android/exoplayer2/util/C;

    if-eqz v0, :cond_0

    iget-wide v1, p1, Lcom/google/android/exoplayer2/metadata/e;->h:J

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/C;->c()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/util/C;

    iget-wide v1, p1, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->d:J

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/C;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->c:Lcom/google/android/exoplayer2/util/C;

    iget-object v0, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->c:Lcom/google/android/exoplayer2/util/C;

    iget-wide v1, p1, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->d:J

    iget-wide v3, p1, Lcom/google/android/exoplayer2/metadata/e;->h:J

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/util/C;->a(J)J

    :cond_1
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object p1

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result p2

    iget-object v0, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->a:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/util/t;->a([BI)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->b:Lcom/google/android/exoplayer2/util/s;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/util/s;->a([BI)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->b:Lcom/google/android/exoplayer2/util/s;

    const/16 p2, 0x27

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/util/s;->d(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->b:Lcom/google/android/exoplayer2/util/s;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result p1

    int-to-long v0, p1

    const/16 p1, 0x20

    shl-long/2addr v0, p1

    iget-object v2, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->b:Lcom/google/android/exoplayer2/util/s;

    invoke-virtual {v2, p1}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result p1

    int-to-long v2, p1

    or-long/2addr v0, v2

    iget-object p1, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->b:Lcom/google/android/exoplayer2/util/s;

    const/16 v2, 0x14

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/util/s;->d(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->b:Lcom/google/android/exoplayer2/util/s;

    const/16 v2, 0xc

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result p1

    iget-object v2, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->b:Lcom/google/android/exoplayer2/util/s;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->a:Lcom/google/android/exoplayer2/util/t;

    const/16 v5, 0xe

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    if-eqz v2, :cond_6

    const/16 v4, 0xff

    if-eq v2, v4, :cond_5

    const/4 p1, 0x4

    if-eq v2, p1, :cond_4

    const/4 p1, 0x5

    if-eq v2, p1, :cond_3

    const/4 p1, 0x6

    if-eq v2, p1, :cond_2

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->a:Lcom/google/android/exoplayer2/util/t;

    iget-object v2, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->c:Lcom/google/android/exoplayer2/util/C;

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/exoplayer2/metadata/scte35/TimeSignalCommand;->a(Lcom/google/android/exoplayer2/util/t;JLcom/google/android/exoplayer2/util/C;)Lcom/google/android/exoplayer2/metadata/scte35/TimeSignalCommand;

    move-result-object v3

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->a:Lcom/google/android/exoplayer2/util/t;

    iget-object v2, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->c:Lcom/google/android/exoplayer2/util/C;

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/exoplayer2/metadata/scte35/SpliceInsertCommand;->a(Lcom/google/android/exoplayer2/util/t;JLcom/google/android/exoplayer2/util/C;)Lcom/google/android/exoplayer2/metadata/scte35/SpliceInsertCommand;

    move-result-object v3

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->a:Lcom/google/android/exoplayer2/util/t;

    invoke-static {p1}, Lcom/google/android/exoplayer2/metadata/scte35/SpliceScheduleCommand;->a(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/metadata/scte35/SpliceScheduleCommand;

    move-result-object v3

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/exoplayer2/metadata/scte35/b;->a:Lcom/google/android/exoplayer2/util/t;

    invoke-static {v2, p1, v0, v1}, Lcom/google/android/exoplayer2/metadata/scte35/PrivateCommand;->a(Lcom/google/android/exoplayer2/util/t;IJ)Lcom/google/android/exoplayer2/metadata/scte35/PrivateCommand;

    move-result-object v3

    goto :goto_0

    :cond_6
    new-instance v3, Lcom/google/android/exoplayer2/metadata/scte35/SpliceNullCommand;

    invoke-direct {v3}, Lcom/google/android/exoplayer2/metadata/scte35/SpliceNullCommand;-><init>()V

    :goto_0
    const/4 p1, 0x0

    if-nez v3, :cond_7

    new-instance p2, Lcom/google/android/exoplayer2/metadata/Metadata;

    new-array p1, p1, [Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    invoke-direct {p2, p1}, Lcom/google/android/exoplayer2/metadata/Metadata;-><init>([Lcom/google/android/exoplayer2/metadata/Metadata$Entry;)V

    goto :goto_1

    :cond_7
    new-instance v0, Lcom/google/android/exoplayer2/metadata/Metadata;

    new-array p2, p2, [Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    aput-object v3, p2, p1

    invoke-direct {v0, p2}, Lcom/google/android/exoplayer2/metadata/Metadata;-><init>([Lcom/google/android/exoplayer2/metadata/Metadata$Entry;)V

    move-object p2, v0

    :goto_1
    return-object p2
.end method
