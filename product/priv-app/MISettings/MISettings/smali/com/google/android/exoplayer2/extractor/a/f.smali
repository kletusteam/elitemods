.class final Lcom/google/android/exoplayer2/extractor/a/f;
.super Lcom/google/android/exoplayer2/extractor/a/e;


# instance fields
.field private final b:Lcom/google/android/exoplayer2/util/t;

.field private final c:Lcom/google/android/exoplayer2/util/t;

.field private d:I

.field private e:Z

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/extractor/TrackOutput;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/a/e;-><init>(Lcom/google/android/exoplayer2/extractor/TrackOutput;)V

    new-instance p1, Lcom/google/android/exoplayer2/util/t;

    sget-object v0, Lcom/google/android/exoplayer2/util/r;->a:[B

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/util/t;-><init>([B)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/f;->b:Lcom/google/android/exoplayer2/util/t;

    new-instance p1, Lcom/google/android/exoplayer2/util/t;

    const/4 v0, 0x4

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/f;->c:Lcom/google/android/exoplayer2/util/t;

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/exoplayer2/util/t;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/extractor/a/e$a;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result p1

    shr-int/lit8 v0, p1, 0x4

    and-int/lit8 v0, v0, 0xf

    and-int/lit8 p1, p1, 0xf

    const/4 v1, 0x7

    if-ne p1, v1, :cond_1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/f;->g:I

    const/4 p1, 0x5

    if-eq v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1

    :cond_1
    new-instance v0, Lcom/google/android/exoplayer2/extractor/a/e$a;

    const/16 v1, 0x27

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Video format not supported: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/extractor/a/e$a;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected b(Lcom/google/android/exoplayer2/util/t;J)Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ea;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->k()I

    move-result v1

    int-to-long v1, v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    add-long v4, p2, v1

    const/4 p2, 0x1

    const/4 p3, 0x0

    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/a/f;->e:Z

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v1

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/t;-><init>([B)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v2

    invoke-virtual {p1, v1, p3, v2}, Lcom/google/android/exoplayer2/util/t;->a([BII)V

    invoke-static {v0}, Lcom/google/android/exoplayer2/video/i;->a(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/video/i;

    move-result-object p1

    iget v0, p1, Lcom/google/android/exoplayer2/video/i;->b:I

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/f;->d:I

    new-instance v0, Lcom/google/android/exoplayer2/Format$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/Format$a;-><init>()V

    const-string v1, "video/avc"

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/Format$a;->e(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    iget v1, p1, Lcom/google/android/exoplayer2/video/i;->c:I

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/Format$a;->o(I)Lcom/google/android/exoplayer2/Format$a;

    iget v1, p1, Lcom/google/android/exoplayer2/video/i;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/Format$a;->f(I)Lcom/google/android/exoplayer2/Format$a;

    iget v1, p1, Lcom/google/android/exoplayer2/video/i;->e:F

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/Format$a;->b(F)Lcom/google/android/exoplayer2/Format$a;

    iget-object p1, p1, Lcom/google/android/exoplayer2/video/i;->a:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/Format$a;->a(Ljava/util/List;)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Format$a;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/e;->a:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/Format;)V

    iput-boolean p2, p0, Lcom/google/android/exoplayer2/extractor/a/f;->e:Z

    return p3

    :cond_0
    if-ne v0, p2, :cond_4

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/f;->e:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/f;->g:I

    if-ne v0, p2, :cond_1

    move v6, p2

    goto :goto_0

    :cond_1
    move v6, p3

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/f;->f:Z

    if-nez v0, :cond_2

    if-nez v6, :cond_2

    return p3

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/f;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    aput-byte p3, v0, p3

    aput-byte p3, v0, p2

    const/4 v1, 0x2

    aput-byte p3, v0, v1

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/f;->d:I

    const/4 v1, 0x4

    rsub-int/lit8 v0, v0, 0x4

    move v7, p3

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/f;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v2

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/f;->d:I

    invoke-virtual {p1, v2, v0, v3}, Lcom/google/android/exoplayer2/util/t;->a([BII)V

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/f;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2, p3}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/f;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/t;->z()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v3, p3}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/e;->a:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-object v8, p0, Lcom/google/android/exoplayer2/extractor/a/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-interface {v3, v8, v1}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/util/t;I)V

    add-int/lit8 v7, v7, 0x4

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/e;->a:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-interface {v3, p1, v2}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/util/t;I)V

    add-int/2addr v7, v2

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/e;->a:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-interface/range {v3 .. v9}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(JIIILcom/google/android/exoplayer2/extractor/TrackOutput$a;)V

    iput-boolean p2, p0, Lcom/google/android/exoplayer2/extractor/a/f;->f:Z

    return p2

    :cond_4
    return p3
.end method
