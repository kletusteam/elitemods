.class final Lcom/google/android/exoplayer2/extractor/b/f;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/android/exoplayer2/extractor/b/g;

.field private final b:Lcom/google/android/exoplayer2/util/t;

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/exoplayer2/extractor/b/g;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/b/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    const v1, 0xfe01

    new-array v1, v1, [B

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/t;-><init>([BI)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->c:I

    return-void
.end method

.method private a(I)I
    .locals 5

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->d:I

    :cond_0
    iget v1, p0, Lcom/google/android/exoplayer2/extractor/b/f;->d:I

    add-int v2, p1, v1

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/b/f;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    iget v4, v3, Lcom/google/android/exoplayer2/extractor/b/g;->g:I

    if-ge v2, v4, :cond_1

    iget-object v2, v3, Lcom/google/android/exoplayer2/extractor/b/g;->j:[I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/b/f;->d:I

    add-int/2addr v1, p1

    aget v1, v2, v1

    add-int/2addr v0, v1

    const/16 v2, 0xff

    if-eq v1, v2, :cond_0

    :cond_1
    return v0
.end method


# virtual methods
.method public a()Lcom/google/android/exoplayer2/extractor/b/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    return-object v0
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, v0

    :goto_0
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->e:Z

    if-eqz v2, :cond_1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->e:Z

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2, v0}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    :cond_1
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->e:Z

    if-nez v2, :cond_a

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->c:I

    if-gez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    invoke-virtual {v2, p1}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    invoke-virtual {v2, p1, v1}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;Z)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_3

    :cond_2
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    iget v3, v2, Lcom/google/android/exoplayer2/extractor/b/g;->h:I

    iget v2, v2, Lcom/google/android/exoplayer2/extractor/b/g;->b:I

    and-int/2addr v2, v1

    if-ne v2, v1, :cond_3

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/b/f;->a(I)I

    move-result v2

    add-int/2addr v3, v2

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->d:I

    add-int/2addr v2, v0

    goto :goto_2

    :cond_3
    move v2, v0

    :goto_2
    invoke-interface {p1, v3}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->c:I

    goto :goto_4

    :cond_4
    :goto_3
    return v0

    :cond_5
    :goto_4
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->c:I

    invoke-direct {p0, v2}, Lcom/google/android/exoplayer2/extractor/b/f;->a(I)I

    move-result v2

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/b/f;->c:I

    iget v4, p0, Lcom/google/android/exoplayer2/extractor/b/f;->d:I

    add-int/2addr v3, v4

    if-lez v2, :cond_8

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->b()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v5

    add-int/2addr v5, v2

    if-ge v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v6

    add-int/2addr v6, v2

    invoke-static {v5, v6}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/util/t;->a([B)V

    :cond_6
    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v5

    invoke-interface {p1, v4, v5, v2}, Lcom/google/android/exoplayer2/extractor/i;->readFully([BII)V

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/util/t;->d(I)V

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    iget-object v2, v2, Lcom/google/android/exoplayer2/extractor/b/g;->j:[I

    add-int/lit8 v4, v3, -0x1

    aget v2, v2, v4

    const/16 v4, 0xff

    if-eq v2, v4, :cond_7

    move v2, v1

    goto :goto_5

    :cond_7
    move v2, v0

    :goto_5
    iput-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->e:Z

    :cond_8
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/b/f;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    iget v2, v2, Lcom/google/android/exoplayer2/extractor/b/g;->g:I

    if-ne v3, v2, :cond_9

    const/4 v3, -0x1

    :cond_9
    iput v3, p0, Lcom/google/android/exoplayer2/extractor/b/f;->c:I

    goto/16 :goto_1

    :cond_a
    return v1
.end method

.method public b()Lcom/google/android/exoplayer2/util/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    return-object v0
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/b/g;->a()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->c:I

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/b/f;->e:Z

    return-void
.end method

.method public d()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    array-length v0, v0

    const v1, 0xfe01

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/b/f;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/t;->a([B)V

    return-void
.end method
