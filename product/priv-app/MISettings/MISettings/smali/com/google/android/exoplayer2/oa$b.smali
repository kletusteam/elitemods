.class final Lcom/google/android/exoplayer2/oa$b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/video/u;
.implements Lcom/google/android/exoplayer2/audio/s;
.implements Lcom/google/android/exoplayer2/text/l;
.implements Lcom/google/android/exoplayer2/metadata/f;
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lcom/google/android/exoplayer2/AudioFocusManager$b;
.implements Lcom/google/android/exoplayer2/D$b;
.implements Lcom/google/android/exoplayer2/qa$a;
.implements Lcom/google/android/exoplayer2/Player$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/oa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/exoplayer2/oa;


# direct methods
.method private constructor <init>(Lcom/google/android/exoplayer2/oa;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/na;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/oa$b;-><init>(Lcom/google/android/exoplayer2/oa;)V

    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 0

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1}, Lcom/google/android/exoplayer2/oa;->k(Lcom/google/android/exoplayer2/oa;)V

    return-void
.end method

.method public a(IIIF)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/video/t;

    iget-object v2, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v2}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/video/t;->a(IIIF)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/video/u;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/video/u;->a(IIIF)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public a(IJ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/video/u;

    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/exoplayer2/video/u;->a(IJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(IJJ)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->d(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/google/android/exoplayer2/audio/s;

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-interface/range {v2 .. v7}, Lcom/google/android/exoplayer2/audio/s;->a(IJJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(IZ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->n(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/device/a;

    invoke-interface {v1, p1, p2}, Lcom/google/android/exoplayer2/device/a;->a(IZ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->d(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/audio/s;

    invoke-interface {v1, p1, p2}, Lcom/google/android/exoplayer2/audio/s;->a(J)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(JI)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/video/u;

    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/exoplayer2/video/u;->a(JI)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->c(Lcom/google/android/exoplayer2/oa;)Landroid/view/Surface;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/video/t;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/video/t;->b()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/video/u;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/video/u;->a(Landroid/view/Surface;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/Format;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/Format;

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/video/u;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/video/u;->a(Lcom/google/android/exoplayer2/Format;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->d(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/audio/s;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/audio/s;->a(Lcom/google/android/exoplayer2/decoder/e;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/Format;

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/decoder/e;)Lcom/google/android/exoplayer2/decoder/e;

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;I)I

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/metadata/Metadata;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->j(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/metadata/f;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/metadata/f;->a(Lcom/google/android/exoplayer2/metadata/Metadata;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;JJ)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/google/android/exoplayer2/video/u;

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-interface/range {v2 .. v7}, Lcom/google/android/exoplayer2/video/u;->a(Ljava/lang/String;JJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/text/Cue;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->i(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/text/l;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/text/l;->a(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;ZII)V

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/Format;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/Format;

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->d(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/audio/s;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/audio/s;->b(Lcom/google/android/exoplayer2/Format;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/decoder/e;)Lcom/google/android/exoplayer2/decoder/e;

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->d(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/audio/s;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/audio/s;->b(Lcom/google/android/exoplayer2/decoder/e;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;JJ)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->d(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/google/android/exoplayer2/audio/s;

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-interface/range {v2 .. v7}, Lcom/google/android/exoplayer2/audio/s;->b(Ljava/lang/String;JJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->o(Lcom/google/android/exoplayer2/oa;)Lcom/google/android/exoplayer2/util/v;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v1}, Lcom/google/android/exoplayer2/oa;->p(Lcom/google/android/exoplayer2/oa;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1}, Lcom/google/android/exoplayer2/oa;->o(Lcom/google/android/exoplayer2/oa;)Lcom/google/android/exoplayer2/util/v;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/v;->a(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/oa;Z)Z

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1}, Lcom/google/android/exoplayer2/oa;->p(Lcom/google/android/exoplayer2/oa;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1}, Lcom/google/android/exoplayer2/oa;->o(Lcom/google/android/exoplayer2/oa;)Lcom/google/android/exoplayer2/util/v;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/v;->b(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/oa;Z)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public b(ZI)V
    .locals 0

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1}, Lcom/google/android/exoplayer2/oa;->q(Lcom/google/android/exoplayer2/oa;)V

    return-void
.end method

.method public c(I)V
    .locals 0

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1}, Lcom/google/android/exoplayer2/oa;->q(Lcom/google/android/exoplayer2/oa;)V

    return-void
.end method

.method public c(Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/decoder/e;)Lcom/google/android/exoplayer2/decoder/e;

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/video/u;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/video/u;->c(Lcom/google/android/exoplayer2/decoder/e;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public d(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->e(Lcom/google/android/exoplayer2/oa;)I

    move-result v0

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;I)I

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1}, Lcom/google/android/exoplayer2/oa;->f(Lcom/google/android/exoplayer2/oa;)V

    return-void
.end method

.method public d(Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/video/u;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/video/u;->d(Lcom/google/android/exoplayer2/decoder/e;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/Format;

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/decoder/e;)Lcom/google/android/exoplayer2/decoder/e;

    return-void
.end method

.method public e(I)V
    .locals 2

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1}, Lcom/google/android/exoplayer2/oa;->l(Lcom/google/android/exoplayer2/oa;)Lcom/google/android/exoplayer2/qa;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/qa;)Lcom/google/android/exoplayer2/device/DeviceInfo;

    move-result-object p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->m(Lcom/google/android/exoplayer2/oa;)Lcom/google/android/exoplayer2/device/DeviceInfo;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/device/DeviceInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/device/DeviceInfo;)Lcom/google/android/exoplayer2/device/DeviceInfo;

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->n(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/device/a;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/device/a;->a(Lcom/google/android/exoplayer2/device/DeviceInfo;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public f(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/oa;->d()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/oa;->a(ZI)I

    move-result v2

    invoke-static {v1, v0, p1, v2}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;ZII)V

    return-void
.end method

.method public f(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/oa;->g(Lcom/google/android/exoplayer2/oa;)Z

    move-result v0

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;Z)Z

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1}, Lcom/google/android/exoplayer2/oa;->h(Lcom/google/android/exoplayer2/oa;)V

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    const/4 p1, 0x1

    invoke-static {v0, v1, p1}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;Landroid/view/Surface;Z)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1, p2, p3}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;II)V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;Landroid/view/Surface;Z)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    const/4 v1, 0x0

    invoke-static {p1, v1, v1}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;II)V

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1, p2, p3}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;II)V

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1, p3, p4}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;II)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object p1

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;Landroid/view/Surface;Z)V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;Landroid/view/Surface;Z)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/oa$b;->a:Lcom/google/android/exoplayer2/oa;

    invoke-static {p1, v0, v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/oa;II)V

    return-void
.end method
