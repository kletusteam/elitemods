.class public final Lcom/google/android/exoplayer2/extractor/c/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/Extractor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/c/b$a;,
        Lcom/google/android/exoplayer2/extractor/c/b$c;,
        Lcom/google/android/exoplayer2/extractor/c/b$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/exoplayer2/extractor/m;


# instance fields
.field private b:Lcom/google/android/exoplayer2/extractor/k;

.field private c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

.field private d:Lcom/google/android/exoplayer2/extractor/c/b$b;

.field private e:I

.field private f:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/extractor/c/a;->a:Lcom/google/android/exoplayer2/extractor/c/a;

    sput-object v0, Lcom/google/android/exoplayer2/extractor/c/b;->a:Lcom/google/android/exoplayer2/extractor/m;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/b;->e:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/c/b;->f:J

    return-void
.end method

.method static synthetic a()[Lcom/google/android/exoplayer2/extractor/Extractor;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/exoplayer2/extractor/Extractor;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/c/b;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/extractor/c/b;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method private b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/b;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/b;->b:Lcom/google/android/exoplayer2/extractor/k;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/c/b;->b()V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->d:Lcom/google/android/exoplayer2/extractor/c/b$b;

    if-nez p2, :cond_5

    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/c/d;->a(Lcom/google/android/exoplayer2/extractor/i;)Lcom/google/android/exoplayer2/extractor/c/c;

    move-result-object v3

    if-eqz v3, :cond_4

    iget p2, v3, Lcom/google/android/exoplayer2/extractor/c/c;->a:I

    const/16 v0, 0x11

    if-ne p2, v0, :cond_0

    new-instance p2, Lcom/google/android/exoplayer2/extractor/c/b$a;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/b;->b:Lcom/google/android/exoplayer2/extractor/k;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/b;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-direct {p2, v0, v1, v3}, Lcom/google/android/exoplayer2/extractor/c/b$a;-><init>(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/TrackOutput;Lcom/google/android/exoplayer2/extractor/c/c;)V

    iput-object p2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->d:Lcom/google/android/exoplayer2/extractor/c/b$b;

    goto :goto_0

    :cond_0
    const/4 v0, 0x6

    if-ne p2, v0, :cond_1

    new-instance p2, Lcom/google/android/exoplayer2/extractor/c/b$c;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/b;->b:Lcom/google/android/exoplayer2/extractor/k;

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    const/4 v5, -0x1

    const-string v4, "audio/g711-alaw"

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/extractor/c/b$c;-><init>(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/TrackOutput;Lcom/google/android/exoplayer2/extractor/c/c;Ljava/lang/String;I)V

    iput-object p2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->d:Lcom/google/android/exoplayer2/extractor/c/b$b;

    goto :goto_0

    :cond_1
    const/4 v0, 0x7

    if-ne p2, v0, :cond_2

    new-instance p2, Lcom/google/android/exoplayer2/extractor/c/b$c;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/b;->b:Lcom/google/android/exoplayer2/extractor/k;

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    const/4 v5, -0x1

    const-string v4, "audio/g711-mlaw"

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/extractor/c/b$c;-><init>(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/TrackOutput;Lcom/google/android/exoplayer2/extractor/c/c;Ljava/lang/String;I)V

    iput-object p2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->d:Lcom/google/android/exoplayer2/extractor/c/b$b;

    goto :goto_0

    :cond_2
    iget v0, v3, Lcom/google/android/exoplayer2/extractor/c/c;->f:I

    invoke-static {p2, v0}, Lcom/google/android/exoplayer2/audio/M;->a(II)I

    move-result v5

    if-eqz v5, :cond_3

    new-instance p2, Lcom/google/android/exoplayer2/extractor/c/b$c;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/b;->b:Lcom/google/android/exoplayer2/extractor/k;

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    const-string v4, "audio/raw"

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/extractor/c/b$c;-><init>(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/TrackOutput;Lcom/google/android/exoplayer2/extractor/c/c;Ljava/lang/String;I)V

    iput-object p2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->d:Lcom/google/android/exoplayer2/extractor/c/b$b;

    goto :goto_0

    :cond_3
    new-instance p1, Lcom/google/android/exoplayer2/ea;

    iget p2, v3, Lcom/google/android/exoplayer2/extractor/c/c;->a:I

    const/16 v0, 0x28

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Unsupported WAV format type: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ea;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Lcom/google/android/exoplayer2/ea;

    const-string p2, "Unsupported or unrecognized wav header."

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ea;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    :goto_0
    iget p2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->e:I

    const/4 v0, -0x1

    if-ne p2, v0, :cond_6

    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/c/d;->b(Lcom/google/android/exoplayer2/extractor/i;)Landroid/util/Pair;

    move-result-object p2

    iget-object v1, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/c/b;->e:I

    iget-object p2, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/exoplayer2/extractor/c/b;->f:J

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->d:Lcom/google/android/exoplayer2/extractor/c/b$b;

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/c/b;->e:I

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->f:J

    invoke-interface {p2, v1, v2, v3}, Lcom/google/android/exoplayer2/extractor/c/b$b;->a(IJ)V

    goto :goto_1

    :cond_6
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long p2, v1, v3

    if-nez p2, :cond_7

    iget p2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->e:I

    invoke-interface {p1, p2}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    :cond_7
    :goto_1
    iget-wide v1, p0, Lcom/google/android/exoplayer2/extractor/c/b;->f:J

    const-wide/16 v3, -0x1

    cmp-long p2, v1, v3

    const/4 v1, 0x0

    if-eqz p2, :cond_8

    const/4 p2, 0x1

    goto :goto_2

    :cond_8
    move p2, v1

    :goto_2
    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->f:J

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/c/b;->d:Lcom/google/android/exoplayer2/extractor/c/b$b;

    invoke-interface {p2, p1, v2, v3}, Lcom/google/android/exoplayer2/extractor/c/b$b;->a(Lcom/google/android/exoplayer2/extractor/i;J)Z

    move-result p1

    if-eqz p1, :cond_9

    goto :goto_3

    :cond_9
    move v0, v1

    :goto_3
    return v0
.end method

.method public a(JJ)V
    .locals 0

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/c/b;->d:Lcom/google/android/exoplayer2/extractor/c/b$b;

    if-eqz p1, :cond_0

    invoke-interface {p1, p3, p4}, Lcom/google/android/exoplayer2/extractor/c/b$b;->a(J)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/k;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/c/b;->b:Lcom/google/android/exoplayer2/extractor/k;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lcom/google/android/exoplayer2/extractor/k;->a(II)Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/b;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/k;->g()V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/c/d;->a(Lcom/google/android/exoplayer2/extractor/i;)Lcom/google/android/exoplayer2/extractor/c/c;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public release()V
    .locals 0

    return-void
.end method
