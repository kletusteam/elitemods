.class public final Lcom/google/android/exoplayer2/ui/H;
.super Ljava/lang/Object;


# static fields
.field public static final compat_button_inset_horizontal_material:I = 0x7f070114

.field public static final compat_button_inset_vertical_material:I = 0x7f070115

.field public static final compat_button_padding_horizontal_material:I = 0x7f070116

.field public static final compat_button_padding_vertical_material:I = 0x7f070117

.field public static final compat_control_corner_material:I = 0x7f070118

.field public static final compat_notification_large_icon_max_height:I = 0x7f070119

.field public static final compat_notification_large_icon_max_width:I = 0x7f07011a

.field public static final exo_bottom_bar_height:I = 0x7f070154

.field public static final exo_custom_progress_margin_bottom:I = 0x7f070155

.field public static final exo_custom_progress_max_size:I = 0x7f070156

.field public static final exo_custom_progress_thumb_size:I = 0x7f070157

.field public static final exo_error_message_height:I = 0x7f070158

.field public static final exo_error_message_margin_bottom:I = 0x7f070159

.field public static final exo_error_message_text_padding_horizontal:I = 0x7f07015a

.field public static final exo_error_message_text_padding_vertical:I = 0x7f07015b

.field public static final exo_error_message_text_size:I = 0x7f07015c

.field public static final exo_icon_margin:I = 0x7f07015d

.field public static final exo_icon_padding:I = 0x7f07015e

.field public static final exo_icon_padding_bottom:I = 0x7f07015f

.field public static final exo_icon_size:I = 0x7f070160

.field public static final exo_icon_text_size:I = 0x7f070161

.field public static final exo_media_button_height:I = 0x7f070162

.field public static final exo_media_button_width:I = 0x7f070163

.field public static final exo_setting_width:I = 0x7f070164

.field public static final exo_settings_height:I = 0x7f070165

.field public static final exo_settings_icon_size:I = 0x7f070166

.field public static final exo_settings_main_text_size:I = 0x7f070167

.field public static final exo_settings_offset:I = 0x7f070168

.field public static final exo_settings_sub_text_size:I = 0x7f070169

.field public static final exo_settings_text_height:I = 0x7f07016a

.field public static final exo_small_button_height:I = 0x7f07016b

.field public static final exo_small_button_width:I = 0x7f07016c

.field public static final exo_small_icon_padding_horizontal:I = 0x7f07016d

.field public static final exo_small_icon_padding_vertical:I = 0x7f07016e

.field public static final exo_time_view_padding:I = 0x7f07016f

.field public static final exo_time_view_width:I = 0x7f070170

.field public static final fastscroll_default_thickness:I = 0x7f070176

.field public static final fastscroll_margin:I = 0x7f070177

.field public static final fastscroll_minimum_range:I = 0x7f070178

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x7f0701c5

.field public static final item_touch_helper_swipe_escape_max_velocity:I = 0x7f0701c6

.field public static final item_touch_helper_swipe_escape_velocity:I = 0x7f0701c7

.field public static final notification_action_icon_size:I = 0x7f070404

.field public static final notification_action_text_size:I = 0x7f070405

.field public static final notification_big_circle_margin:I = 0x7f070406

.field public static final notification_content_margin_start:I = 0x7f070408

.field public static final notification_large_icon_height:I = 0x7f070409

.field public static final notification_large_icon_width:I = 0x7f07040a

.field public static final notification_main_column_padding_top:I = 0x7f07040b

.field public static final notification_media_narrow_margin:I = 0x7f07040c

.field public static final notification_right_icon_size:I = 0x7f07040e

.field public static final notification_right_side_padding_top:I = 0x7f07040f

.field public static final notification_small_icon_background_padding:I = 0x7f070410

.field public static final notification_small_icon_size_as_large:I = 0x7f070411

.field public static final notification_subtext_size:I = 0x7f070412

.field public static final notification_top_pad:I = 0x7f070413

.field public static final notification_top_pad_large_text:I = 0x7f070414

.field public static final subtitle_corner_radius:I = 0x7f07045c

.field public static final subtitle_outline_width:I = 0x7f07045d

.field public static final subtitle_shadow_offset:I = 0x7f07045e

.field public static final subtitle_shadow_radius:I = 0x7f07045f
