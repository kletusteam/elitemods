.class public final Lcom/google/android/exoplayer2/L;
.super Ljava/lang/Object;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public static a(Landroid/content/Context;)Lcom/google/android/exoplayer2/oa;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/L;->a(Landroid/content/Context;Lcom/google/android/exoplayer2/trackselection/o;)Lcom/google/android/exoplayer2/oa;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;)Lcom/google/android/exoplayer2/oa;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/I;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/I;-><init>()V

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/exoplayer2/L;->a(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/U;)Lcom/google/android/exoplayer2/oa;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/U;)Lcom/google/android/exoplayer2/oa;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {}, Lcom/google/android/exoplayer2/util/E;->c()Landroid/os/Looper;

    move-result-object v0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/exoplayer2/L;->a(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/U;Landroid/os/Looper;)Lcom/google/android/exoplayer2/oa;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/U;Landroid/os/Looper;)Lcom/google/android/exoplayer2/oa;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v4, Lcom/google/android/exoplayer2/a/a;

    sget-object v0, Lcom/google/android/exoplayer2/util/e;->a:Lcom/google/android/exoplayer2/util/e;

    invoke-direct {v4, v0}, Lcom/google/android/exoplayer2/a/a;-><init>(Lcom/google/android/exoplayer2/util/e;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/exoplayer2/L;->a(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/a/a;Landroid/os/Looper;)Lcom/google/android/exoplayer2/oa;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/a/a;Landroid/os/Looper;)Lcom/google/android/exoplayer2/oa;
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p0}, Lcom/google/android/exoplayer2/upstream/q;->a(Landroid/content/Context;)Lcom/google/android/exoplayer2/upstream/q;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/exoplayer2/L;->a(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/upstream/g;Lcom/google/android/exoplayer2/a/a;Landroid/os/Looper;)Lcom/google/android/exoplayer2/oa;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/upstream/g;Lcom/google/android/exoplayer2/a/a;Landroid/os/Looper;)Lcom/google/android/exoplayer2/oa;
    .locals 12
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v11, Lcom/google/android/exoplayer2/oa;

    new-instance v4, Lcom/google/android/exoplayer2/source/n;

    move-object v1, p0

    invoke-direct {v4, p0}, Lcom/google/android/exoplayer2/source/n;-><init>(Landroid/content/Context;)V

    sget-object v9, Lcom/google/android/exoplayer2/util/e;->a:Lcom/google/android/exoplayer2/util/e;

    const/4 v8, 0x1

    move-object v0, v11

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lcom/google/android/exoplayer2/oa;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/source/B;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/upstream/g;Lcom/google/android/exoplayer2/a/a;ZLcom/google/android/exoplayer2/util/e;Landroid/os/Looper;)V

    return-object v11
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/exoplayer2/trackselection/o;)Lcom/google/android/exoplayer2/oa;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/DefaultRenderersFactory;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/DefaultRenderersFactory;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0, p1}, Lcom/google/android/exoplayer2/L;->a(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;)Lcom/google/android/exoplayer2/oa;

    move-result-object p0

    return-object p0
.end method
