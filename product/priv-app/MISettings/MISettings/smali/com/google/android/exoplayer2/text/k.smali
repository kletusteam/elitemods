.class public abstract Lcom/google/android/exoplayer2/text/k;
.super Lcom/google/android/exoplayer2/decoder/g;

# interfaces
.implements Lcom/google/android/exoplayer2/text/e;


# instance fields
.field private a:Lcom/google/android/exoplayer2/text/e;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private b:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/decoder/g;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/k;->a:Lcom/google/android/exoplayer2/text/e;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/text/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/text/e;->a()I

    move-result v0

    return v0
.end method

.method public a(J)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/k;->a:Lcom/google/android/exoplayer2/text/e;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/text/e;

    iget-wide v1, p0, Lcom/google/android/exoplayer2/text/k;->b:J

    sub-long/2addr p1, v1

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/text/e;->a(J)I

    move-result p1

    return p1
.end method

.method public a(I)J
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/k;->a:Lcom/google/android/exoplayer2/text/e;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/text/e;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/text/e;->a(I)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/exoplayer2/text/k;->b:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public a(JLcom/google/android/exoplayer2/text/e;J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/decoder/g;->timeUs:J

    iput-object p3, p0, Lcom/google/android/exoplayer2/text/k;->a:Lcom/google/android/exoplayer2/text/e;

    const-wide p1, 0x7fffffffffffffffL

    cmp-long p1, p4, p1

    if-nez p1, :cond_0

    iget-wide p4, p0, Lcom/google/android/exoplayer2/decoder/g;->timeUs:J

    :cond_0
    iput-wide p4, p0, Lcom/google/android/exoplayer2/text/k;->b:J

    return-void
.end method

.method public b(J)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/text/Cue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/k;->a:Lcom/google/android/exoplayer2/text/e;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/text/e;

    iget-wide v1, p0, Lcom/google/android/exoplayer2/text/k;->b:J

    sub-long/2addr p1, v1

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/text/e;->b(J)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public clear()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/exoplayer2/decoder/a;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/k;->a:Lcom/google/android/exoplayer2/text/e;

    return-void
.end method
