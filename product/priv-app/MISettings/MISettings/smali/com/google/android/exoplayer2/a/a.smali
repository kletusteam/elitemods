.class public Lcom/google/android/exoplayer2/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/Player$c;
.implements Lcom/google/android/exoplayer2/metadata/f;
.implements Lcom/google/android/exoplayer2/audio/s;
.implements Lcom/google/android/exoplayer2/video/u;
.implements Lcom/google/android/exoplayer2/source/A;
.implements Lcom/google/android/exoplayer2/upstream/g$a;
.implements Lcom/google/android/exoplayer2/drm/c;
.implements Lcom/google/android/exoplayer2/video/t;
.implements Lcom/google/android/exoplayer2/audio/q;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/a/a$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lcom/google/android/exoplayer2/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/exoplayer2/util/e;

.field private final c:Lcom/google/android/exoplayer2/sa$a;

.field private final d:Lcom/google/android/exoplayer2/sa$b;

.field private final e:Lcom/google/android/exoplayer2/a/a$a;

.field private f:Lcom/google/android/exoplayer2/Player;

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/util/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/util/e;

    iput-object p1, p0, Lcom/google/android/exoplayer2/a/a;->b:Lcom/google/android/exoplayer2/util/e;

    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance p1, Lcom/google/android/exoplayer2/sa$a;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/sa$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/a/a;->c:Lcom/google/android/exoplayer2/sa$a;

    new-instance p1, Lcom/google/android/exoplayer2/sa$b;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/sa$b;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/a/a;->d:Lcom/google/android/exoplayer2/sa$b;

    new-instance p1, Lcom/google/android/exoplayer2/a/a$a;

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->c:Lcom/google/android/exoplayer2/sa$a;

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/a/a$a;-><init>(Lcom/google/android/exoplayer2/sa$a;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    return-void
.end method

.method private a(ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;
    .locals 3
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    iget-object v2, p0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    invoke-virtual {v2, p2}, Lcom/google/android/exoplayer2/a/a$a;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/sa;

    move-result-object v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/a/a;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p1

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/google/android/exoplayer2/sa;->a:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/exoplayer2/a/a;->a(Lcom/google/android/exoplayer2/sa;ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_2
    iget-object p2, p0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {p2}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/sa;->b()I

    move-result v2

    if-ge p1, v2, :cond_3

    goto :goto_2

    :cond_3
    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    sget-object p2, Lcom/google/android/exoplayer2/sa;->a:Lcom/google/android/exoplayer2/sa;

    :goto_3
    const/4 v0, 0x0

    invoke-virtual {p0, p2, p1, v0}, Lcom/google/android/exoplayer2/a/a;->a(Lcom/google/android/exoplayer2/sa;ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;
    .locals 3
    .param p1    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    move-object v1, v0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    invoke-virtual {v1, p1}, Lcom/google/android/exoplayer2/a/a$a;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/sa;

    move-result-object v1

    :goto_0
    if-eqz p1, :cond_2

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    iget-object v0, p1, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/exoplayer2/a/a;->c:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    invoke-virtual {p0, v1, v0, p1}, Lcom/google/android/exoplayer2/a/a;->a(Lcom/google/android/exoplayer2/sa;ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Player;->i()I

    move-result p1

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/sa;->b()I

    move-result v2

    if-ge p1, v2, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_4

    goto :goto_3

    :cond_4
    sget-object v1, Lcom/google/android/exoplayer2/sa;->a:Lcom/google/android/exoplayer2/sa;

    :goto_3
    invoke-virtual {p0, v1, p1, v0}, Lcom/google/android/exoplayer2/a/a;->a(Lcom/google/android/exoplayer2/sa;ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p1

    return-object p1
.end method

.method private e()Lcom/google/android/exoplayer2/a/b$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/a/a$a;->a()Lcom/google/android/exoplayer2/source/y$a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/a/a;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    return-object v0
.end method

.method private f()Lcom/google/android/exoplayer2/a/b$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/a/a$a;->b()Lcom/google/android/exoplayer2/source/y$a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/a/a;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    return-object v0
.end method

.method private g()Lcom/google/android/exoplayer2/a/b$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/a/a$a;->c()Lcom/google/android/exoplayer2/source/y$a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/a/a;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/google/android/exoplayer2/a/b$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/a/a$a;->d()Lcom/google/android/exoplayer2/source/y$a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/a/a;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/google/android/exoplayer2/sa;ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;
    .locals 17
    .param p3    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    move-object v6, v1

    goto :goto_0

    :cond_0
    move-object/from16 v6, p3

    :goto_0
    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->b:Lcom/google/android/exoplayer2/util/e;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/util/e;->b()J

    move-result-wide v2

    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/google/android/exoplayer2/sa;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->i()I

    move-result v1

    if-ne v5, v1, :cond_1

    move v1, v7

    goto :goto_1

    :cond_1
    move v1, v8

    :goto_1
    const-wide/16 v9, 0x0

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v11

    if-eqz v11, :cond_3

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->n()I

    move-result v1

    iget v11, v6, Lcom/google/android/exoplayer2/source/y$a;->b:I

    if-ne v1, v11, :cond_2

    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->h()I

    move-result v1

    iget v11, v6, Lcom/google/android/exoplayer2/source/y$a;->c:I

    if-ne v1, v11, :cond_2

    goto :goto_2

    :cond_2
    move v7, v8

    :goto_2
    if-eqz v7, :cond_6

    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->getCurrentPosition()J

    move-result-wide v9

    goto :goto_3

    :cond_3
    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->l()J

    move-result-wide v7

    goto :goto_4

    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    goto :goto_3

    :cond_5
    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->d:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v4, v5, v1}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/sa$b;->a()J

    move-result-wide v9

    :cond_6
    :goto_3
    move-wide v7, v9

    :goto_4
    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/a/a$a;->a()Lcom/google/android/exoplayer2/source/y$a;

    move-result-object v11

    new-instance v16, Lcom/google/android/exoplayer2/a/b$a;

    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v9

    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->i()I

    move-result v10

    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->getCurrentPosition()J

    move-result-wide v12

    iget-object v1, v0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->c()J

    move-result-wide v14

    move-object/from16 v1, v16

    move-object/from16 v4, p1

    move/from16 v5, p2

    invoke-direct/range {v1 .. v15}, Lcom/google/android/exoplayer2/a/b$a;-><init>(JLcom/google/android/exoplayer2/sa;ILcom/google/android/exoplayer2/source/y$a;JLcom/google/android/exoplayer2/sa;ILcom/google/android/exoplayer2/source/y$a;JJ)V

    return-object v16
.end method

.method public final a()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(F)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;F)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->b(Lcom/google/android/exoplayer2/a/b$a;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1, p2}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(IIIF)V
    .locals 8

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/a/b;

    move-object v1, v6

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;IIIF)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(IJ)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->g()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1, p2, p3}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;IJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(IJJ)V
    .locals 9

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/a/b;

    move-object v1, v7

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/exoplayer2/a/b;->b(Lcom/google/android/exoplayer2/a/b$a;IJJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 1
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/a/a;->a(ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p1

    iget-object p2, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v0, p1, p3, p4}, Lcom/google/android/exoplayer2/a/b;->b(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;Ljava/io/IOException;Z)V
    .locals 6
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/a/a;->a(ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p1

    iget-object p2, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/a/b;

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;Ljava/io/IOException;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/w;)V
    .locals 1
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/a/a;->a(ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p1

    iget-object p2, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v0, p1, p3}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/w;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1, p2}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;J)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(JI)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->g()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1, p2, p3}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;JI)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 3
    .param p1    # Landroid/view/Surface;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Landroid/view/Surface;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 3

    iget-object v0, p1, Lcom/google/android/exoplayer2/ExoPlaybackException;->h:Lcom/google/android/exoplayer2/source/y$a;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/a/a;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/ExoPlaybackException;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/Format;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->b(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/Format;)V

    const/4 v3, 0x2

    invoke-interface {v2, v0, v3, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;ILcom/google/android/exoplayer2/Format;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/Player;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/a/a$a;->a(Lcom/google/android/exoplayer2/a/a$a;)Lcom/google/common/collect/r;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/Player;

    iput-object p1, p0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/W;I)V
    .locals 3
    .param p1    # Lcom/google/android/exoplayer2/W;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1, p2}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/W;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/audio/o;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/audio/o;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->g()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/decoder/e;)V

    const/4 v3, 0x1

    invoke-interface {v2, v0, v3, p1}, Lcom/google/android/exoplayer2/a/b;->b(Lcom/google/android/exoplayer2/a/b$a;ILcom/google/android/exoplayer2/decoder/e;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/ga;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/ga;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/metadata/Metadata;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/metadata/Metadata;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/sa;I)V
    .locals 2

    iget-object p1, p0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/Player;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/a/a$a;->b(Lcom/google/android/exoplayer2/Player;)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v1, p1, p2}, Lcom/google/android/exoplayer2/a/b;->e(Lcom/google/android/exoplayer2/a/b$a;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/m;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1, p2}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/m;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;JJ)V
    .locals 6

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p2

    iget-object p3, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p3}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v0, p2, p1, p4, p5}, Lcom/google/android/exoplayer2/a/b;->b(Lcom/google/android/exoplayer2/a/b$a;Ljava/lang/String;J)V

    const/4 v2, 0x2

    move-object v1, p2

    move-object v3, p1

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;ILjava/lang/String;J)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;Lcom/google/android/exoplayer2/source/y$a;)V
    .locals 2
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/source/y$a;",
            ">;",
            "Lcom/google/android/exoplayer2/source/y$a;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/google/android/exoplayer2/Player;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/exoplayer2/a/a$a;->a(Ljava/util/List;Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/Player;)V

    return-void
.end method

.method public final a(ZI)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1, p2}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;ZI)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(I)V
    .locals 3

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/a/a;->g:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->e:Lcom/google/android/exoplayer2/a/a$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->f:Lcom/google/android/exoplayer2/Player;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/google/android/exoplayer2/Player;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/a/a$a;->a(Lcom/google/android/exoplayer2/Player;)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final b(IJJ)V
    .locals 9

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->f()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/a/b;

    move-object v1, v7

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;IJJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 1
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/a/a;->a(ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p1

    iget-object p2, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v0, p1, p3, p4}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/exoplayer2/Format;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/Format;)V

    const/4 v3, 0x1

    invoke-interface {v2, v0, v3, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;ILcom/google/android/exoplayer2/Format;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->b(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/decoder/e;)V

    const/4 v3, 0x1

    invoke-interface {v2, v0, v3, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;ILcom/google/android/exoplayer2/decoder/e;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;JJ)V
    .locals 6

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p2

    iget-object p3, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p3}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v0, p2, p1, p4, p5}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;Ljava/lang/String;J)V

    const/4 v2, 0x1

    move-object v1, p2

    move-object v3, p1

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;ILjava/lang/String;J)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->e(Lcom/google/android/exoplayer2/a/b$a;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(ZI)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1, p2}, Lcom/google/android/exoplayer2/a/b;->b(Lcom/google/android/exoplayer2/a/b$a;ZI)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/a/a;->g:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/a/a;->g:Z

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0}, Lcom/google/android/exoplayer2/a/b;->b(Lcom/google/android/exoplayer2/a/b$a;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->d(Lcom/google/android/exoplayer2/a/b$a;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 1
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/a/a;->a(ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/a/b$a;

    move-result-object p1

    iget-object p2, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v0, p1, p3, p4}, Lcom/google/android/exoplayer2/a/b;->c(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->c(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/decoder/e;)V

    const/4 v3, 0x2

    invoke-interface {v2, v0, v3, p1}, Lcom/google/android/exoplayer2/a/b;->a(Lcom/google/android/exoplayer2/a/b$a;ILcom/google/android/exoplayer2/decoder/e;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->b(Lcom/google/android/exoplayer2/a/b$a;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public final d(I)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->f(Lcom/google/android/exoplayer2/a/b$a;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final d(Lcom/google/android/exoplayer2/decoder/e;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->g()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->d(Lcom/google/android/exoplayer2/a/b$a;Lcom/google/android/exoplayer2/decoder/e;)V

    const/4 v3, 0x2

    invoke-interface {v2, v0, v3, p1}, Lcom/google/android/exoplayer2/a/b;->b(Lcom/google/android/exoplayer2/a/b$a;ILcom/google/android/exoplayer2/decoder/e;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public e(Z)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->c(Lcom/google/android/exoplayer2/a/b$a;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public f(Z)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->h()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->d(Lcom/google/android/exoplayer2/a/b$a;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onRepeatModeChanged(I)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/a/a;->e()Lcom/google/android/exoplayer2/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/a/a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/a/b;

    invoke-interface {v2, v0, p1}, Lcom/google/android/exoplayer2/a/b;->c(Lcom/google/android/exoplayer2/a/b$a;I)V

    goto :goto_0

    :cond_0
    return-void
.end method
