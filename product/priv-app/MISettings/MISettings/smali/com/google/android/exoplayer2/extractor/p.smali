.class public final Lcom/google/android/exoplayer2/extractor/p;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/u;


# instance fields
.field private final a:Lcom/google/android/exoplayer2/extractor/q;

.field private final b:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/extractor/q;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/p;->a:Lcom/google/android/exoplayer2/extractor/q;

    iput-wide p2, p0, Lcom/google/android/exoplayer2/extractor/p;->b:J

    return-void
.end method

.method private a(JJ)Lcom/google/android/exoplayer2/extractor/v;
    .locals 2

    const-wide/32 v0, 0xf4240

    mul-long/2addr p1, v0

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/p;->a:Lcom/google/android/exoplayer2/extractor/q;

    iget v0, v0, Lcom/google/android/exoplayer2/extractor/q;->e:I

    int-to-long v0, v0

    div-long/2addr p1, v0

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/p;->b:J

    add-long/2addr v0, p3

    new-instance p3, Lcom/google/android/exoplayer2/extractor/v;

    invoke-direct {p3, p1, p2, v0, v1}, Lcom/google/android/exoplayer2/extractor/v;-><init>(JJ)V

    return-object p3
.end method


# virtual methods
.method public b(J)Lcom/google/android/exoplayer2/extractor/u$a;
    .locals 9

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/p;->a:Lcom/google/android/exoplayer2/extractor/q;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/q;->k:Lcom/google/android/exoplayer2/extractor/q$a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/p;->a:Lcom/google/android/exoplayer2/extractor/q;

    iget-object v1, v0, Lcom/google/android/exoplayer2/extractor/q;->k:Lcom/google/android/exoplayer2/extractor/q$a;

    iget-object v2, v1, Lcom/google/android/exoplayer2/extractor/q$a;->a:[J

    iget-object v1, v1, Lcom/google/android/exoplayer2/extractor/q$a;->b:[J

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/q;->a(J)J

    move-result-wide v3

    const/4 v0, 0x1

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v0, v5}, Lcom/google/android/exoplayer2/util/E;->b([JJZZ)I

    move-result v3

    const-wide/16 v4, 0x0

    const/4 v6, -0x1

    if-ne v3, v6, :cond_0

    move-wide v7, v4

    goto :goto_0

    :cond_0
    aget-wide v7, v2, v3

    :goto_0
    if-ne v3, v6, :cond_1

    goto :goto_1

    :cond_1
    aget-wide v4, v1, v3

    :goto_1
    invoke-direct {p0, v7, v8, v4, v5}, Lcom/google/android/exoplayer2/extractor/p;->a(JJ)Lcom/google/android/exoplayer2/extractor/v;

    move-result-object v4

    iget-wide v5, v4, Lcom/google/android/exoplayer2/extractor/v;->b:J

    cmp-long p1, v5, p1

    if-eqz p1, :cond_3

    array-length p1, v2

    sub-int/2addr p1, v0

    if-ne v3, p1, :cond_2

    goto :goto_2

    :cond_2
    add-int/2addr v3, v0

    aget-wide p1, v2, v3

    aget-wide v0, v1, v3

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/exoplayer2/extractor/p;->a(JJ)Lcom/google/android/exoplayer2/extractor/v;

    move-result-object p1

    new-instance p2, Lcom/google/android/exoplayer2/extractor/u$a;

    invoke-direct {p2, v4, p1}, Lcom/google/android/exoplayer2/extractor/u$a;-><init>(Lcom/google/android/exoplayer2/extractor/v;Lcom/google/android/exoplayer2/extractor/v;)V

    return-object p2

    :cond_3
    :goto_2
    new-instance p1, Lcom/google/android/exoplayer2/extractor/u$a;

    invoke-direct {p1, v4}, Lcom/google/android/exoplayer2/extractor/u$a;-><init>(Lcom/google/android/exoplayer2/extractor/v;)V

    return-object p1
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/p;->a:Lcom/google/android/exoplayer2/extractor/q;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/q;->b()J

    move-result-wide v0

    return-wide v0
.end method
