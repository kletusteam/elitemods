.class Lcom/google/android/exoplayer2/ui/V;
.super Landroid/animation/AnimatorListenerAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/exoplayer2/ui/ca;->a(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/exoplayer2/ui/ca;


# direct methods
.method constructor <init>(Lcom/google/android/exoplayer2/ui/ca;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/V;->a:Lcom/google/android/exoplayer2/ui/ca;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/V;->a:Lcom/google/android/exoplayer2/ui/ca;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/ui/ca;->a(Lcom/google/android/exoplayer2/ui/ca;I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/V;->a:Lcom/google/android/exoplayer2/ui/ca;

    invoke-static {p1}, Lcom/google/android/exoplayer2/ui/ca;->c(Lcom/google/android/exoplayer2/ui/ca;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/V;->a:Lcom/google/android/exoplayer2/ui/ca;

    invoke-static {p1}, Lcom/google/android/exoplayer2/ui/ca;->d(Lcom/google/android/exoplayer2/ui/ca;)Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/V;->a:Lcom/google/android/exoplayer2/ui/ca;

    invoke-static {p1}, Lcom/google/android/exoplayer2/ui/ca;->d(Lcom/google/android/exoplayer2/ui/ca;)Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;

    move-result-object p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/V;->a:Lcom/google/android/exoplayer2/ui/ca;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/ca;->e(Lcom/google/android/exoplayer2/ui/ca;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/V;->a:Lcom/google/android/exoplayer2/ui/ca;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/ui/ca;->a(Lcom/google/android/exoplayer2/ui/ca;Z)Z

    :cond_1
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/V;->a:Lcom/google/android/exoplayer2/ui/ca;

    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/ui/ca;->a(Lcom/google/android/exoplayer2/ui/ca;I)V

    return-void
.end method
