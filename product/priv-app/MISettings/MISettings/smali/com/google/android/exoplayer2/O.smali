.class final Lcom/google/android/exoplayer2/O;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/exoplayer2/source/x$a;
.implements Lcom/google/android/exoplayer2/trackselection/o$a;
.implements Lcom/google/android/exoplayer2/da$d;
.implements Lcom/google/android/exoplayer2/J$a;
.implements Lcom/google/android/exoplayer2/ia$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/O$b;,
        Lcom/google/android/exoplayer2/O$a;,
        Lcom/google/android/exoplayer2/O$c;,
        Lcom/google/android/exoplayer2/O$f;,
        Lcom/google/android/exoplayer2/O$g;,
        Lcom/google/android/exoplayer2/O$e;,
        Lcom/google/android/exoplayer2/O$d;
    }
.end annotation


# instance fields
.field private A:Z

.field private B:I

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:I

.field private H:Lcom/google/android/exoplayer2/O$g;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private I:J

.field private J:I

.field private K:Z

.field private L:J

.field private M:Z

.field private final a:[Lcom/google/android/exoplayer2/Renderer;

.field private final b:[Lcom/google/android/exoplayer2/RendererCapabilities;

.field private final c:Lcom/google/android/exoplayer2/trackselection/o;

.field private final d:Lcom/google/android/exoplayer2/trackselection/p;

.field private final e:Lcom/google/android/exoplayer2/U;

.field private final f:Lcom/google/android/exoplayer2/upstream/g;

.field private final g:Lcom/google/android/exoplayer2/util/l;

.field private final h:Landroid/os/HandlerThread;

.field private final i:Landroid/os/Looper;

.field private final j:Lcom/google/android/exoplayer2/sa$b;

.field private final k:Lcom/google/android/exoplayer2/sa$a;

.field private final l:J

.field private final m:Z

.field private final n:Lcom/google/android/exoplayer2/J;

.field private final o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/google/android/exoplayer2/O$c;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lcom/google/android/exoplayer2/util/e;

.field private final q:Lcom/google/android/exoplayer2/O$e;

.field private final r:Lcom/google/android/exoplayer2/ba;

.field private final s:Lcom/google/android/exoplayer2/da;

.field private t:Lcom/google/android/exoplayer2/ma;

.field private u:Lcom/google/android/exoplayer2/fa;

.field private v:Lcom/google/android/exoplayer2/O$d;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>([Lcom/google/android/exoplayer2/Renderer;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/upstream/g;IZLcom/google/android/exoplayer2/a/a;Lcom/google/android/exoplayer2/ma;ZLandroid/os/Looper;Lcom/google/android/exoplayer2/util/e;Lcom/google/android/exoplayer2/O$e;)V
    .locals 0
    .param p8    # Lcom/google/android/exoplayer2/a/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p13, p0, Lcom/google/android/exoplayer2/O;->q:Lcom/google/android/exoplayer2/O$e;

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    iput-object p2, p0, Lcom/google/android/exoplayer2/O;->c:Lcom/google/android/exoplayer2/trackselection/o;

    iput-object p3, p0, Lcom/google/android/exoplayer2/O;->d:Lcom/google/android/exoplayer2/trackselection/p;

    iput-object p4, p0, Lcom/google/android/exoplayer2/O;->e:Lcom/google/android/exoplayer2/U;

    iput-object p5, p0, Lcom/google/android/exoplayer2/O;->f:Lcom/google/android/exoplayer2/upstream/g;

    iput p6, p0, Lcom/google/android/exoplayer2/O;->B:I

    iput-boolean p7, p0, Lcom/google/android/exoplayer2/O;->C:Z

    iput-object p9, p0, Lcom/google/android/exoplayer2/O;->t:Lcom/google/android/exoplayer2/ma;

    iput-boolean p10, p0, Lcom/google/android/exoplayer2/O;->x:Z

    iput-object p12, p0, Lcom/google/android/exoplayer2/O;->p:Lcom/google/android/exoplayer2/util/e;

    const/4 p6, 0x1

    iput-boolean p6, p0, Lcom/google/android/exoplayer2/O;->M:Z

    invoke-interface {p4}, Lcom/google/android/exoplayer2/U;->b()J

    move-result-wide p9

    iput-wide p9, p0, Lcom/google/android/exoplayer2/O;->l:J

    invoke-interface {p4}, Lcom/google/android/exoplayer2/U;->a()Z

    move-result p4

    iput-boolean p4, p0, Lcom/google/android/exoplayer2/O;->m:Z

    invoke-static {p3}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/trackselection/p;)Lcom/google/android/exoplayer2/fa;

    move-result-object p3

    iput-object p3, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    new-instance p3, Lcom/google/android/exoplayer2/O$d;

    iget-object p4, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-direct {p3, p4}, Lcom/google/android/exoplayer2/O$d;-><init>(Lcom/google/android/exoplayer2/fa;)V

    iput-object p3, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    array-length p3, p1

    new-array p3, p3, [Lcom/google/android/exoplayer2/RendererCapabilities;

    iput-object p3, p0, Lcom/google/android/exoplayer2/O;->b:[Lcom/google/android/exoplayer2/RendererCapabilities;

    const/4 p3, 0x0

    :goto_0
    array-length p4, p1

    if-ge p3, p4, :cond_0

    aget-object p4, p1, p3

    invoke-interface {p4, p3}, Lcom/google/android/exoplayer2/Renderer;->setIndex(I)V

    iget-object p4, p0, Lcom/google/android/exoplayer2/O;->b:[Lcom/google/android/exoplayer2/RendererCapabilities;

    aget-object p7, p1, p3

    invoke-interface {p7}, Lcom/google/android/exoplayer2/Renderer;->i()Lcom/google/android/exoplayer2/RendererCapabilities;

    move-result-object p7

    aput-object p7, p4, p3

    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/google/android/exoplayer2/J;

    invoke-direct {p1, p0, p12}, Lcom/google/android/exoplayer2/J;-><init>(Lcom/google/android/exoplayer2/J$a;Lcom/google/android/exoplayer2/util/e;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    new-instance p1, Lcom/google/android/exoplayer2/sa$b;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/sa$b;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->j:Lcom/google/android/exoplayer2/sa$b;

    new-instance p1, Lcom/google/android/exoplayer2/sa$a;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/sa$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p2, p0, p5}, Lcom/google/android/exoplayer2/trackselection/o;->a(Lcom/google/android/exoplayer2/trackselection/o$a;Lcom/google/android/exoplayer2/upstream/g;)V

    iput-boolean p6, p0, Lcom/google/android/exoplayer2/O;->K:Z

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1, p11}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p2, Lcom/google/android/exoplayer2/ba;

    invoke-direct {p2, p8, p1}, Lcom/google/android/exoplayer2/ba;-><init>(Lcom/google/android/exoplayer2/a/a;Landroid/os/Handler;)V

    iput-object p2, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    new-instance p2, Lcom/google/android/exoplayer2/da;

    invoke-direct {p2, p0, p8, p1}, Lcom/google/android/exoplayer2/da;-><init>(Lcom/google/android/exoplayer2/da$d;Lcom/google/android/exoplayer2/a/a;Landroid/os/Handler;)V

    iput-object p2, p0, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    new-instance p1, Landroid/os/HandlerThread;

    const/16 p2, -0x10

    const-string p3, "ExoPlayer:Playback"

    invoke-direct {p1, p3, p2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->h:Landroid/os/HandlerThread;

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->h:Landroid/os/HandlerThread;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->h:Landroid/os/HandlerThread;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->i:Landroid/os/Looper;

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->i:Landroid/os/Looper;

    invoke-interface {p12, p1, p0}, Lcom/google/android/exoplayer2/util/e;->a(Landroid/os/Looper;Landroid/os/Handler$Callback;)Lcom/google/android/exoplayer2/util/l;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    return-void
.end method

.method private A()V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    move-object/from16 v6, p0

    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/J;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/ga;->b:F

    iget-object v1, v6, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    iget-object v2, v6, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object v2

    const/4 v7, 0x1

    move v3, v7

    :goto_0
    if-eqz v1, :cond_a

    iget-boolean v4, v1, Lcom/google/android/exoplayer2/Z;->d:Z

    if-nez v4, :cond_0

    goto/16 :goto_4

    :cond_0
    iget-object v4, v6, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v4, v4, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v1, v0, v4}, Lcom/google/android/exoplayer2/Z;->b(FLcom/google/android/exoplayer2/sa;)Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v9

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->g()Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v4

    invoke-virtual {v9, v4}, Lcom/google/android/exoplayer2/trackselection/p;->a(Lcom/google/android/exoplayer2/trackselection/p;)Z

    move-result v4

    const/4 v14, 0x0

    if-nez v4, :cond_8

    const/4 v15, 0x4

    if-eqz v3, :cond_5

    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v4

    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/Z;)Z

    move-result v12

    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v0, v0

    new-array v5, v0, [Z

    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v10, v0, Lcom/google/android/exoplayer2/fa;->q:J

    move-object v8, v4

    move-object v13, v5

    invoke-virtual/range {v8 .. v13}, Lcom/google/android/exoplayer2/Z;->a(Lcom/google/android/exoplayer2/trackselection/p;JZ[Z)J

    move-result-wide v8

    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v10, v0, Lcom/google/android/exoplayer2/fa;->d:J

    move-object/from16 v0, p0

    move-wide v2, v8

    move-object v12, v4

    move-wide v4, v10

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/fa;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v1, v0, Lcom/google/android/exoplayer2/fa;->e:I

    if-eq v1, v15, :cond_1

    iget-wide v0, v0, Lcom/google/android/exoplayer2/fa;->q:J

    cmp-long v0, v8, v0

    if-eqz v0, :cond_1

    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    invoke-virtual {v0, v15}, Lcom/google/android/exoplayer2/O$d;->c(I)V

    invoke-direct {v6, v8, v9}, Lcom/google/android/exoplayer2/O;->b(J)V

    :cond_1
    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v0, v0

    new-array v0, v0, [Z

    :goto_1
    iget-object v1, v6, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v2, v1

    if-ge v14, v2, :cond_4

    aget-object v1, v1, v14

    invoke-static {v1}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/Renderer;)Z

    move-result v2

    aput-boolean v2, v0, v14

    iget-object v2, v12, Lcom/google/android/exoplayer2/Z;->c:[Lcom/google/android/exoplayer2/source/SampleStream;

    aget-object v2, v2, v14

    aget-boolean v3, v0, v14

    if-eqz v3, :cond_3

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Renderer;->j()Lcom/google/android/exoplayer2/source/SampleStream;

    move-result-object v3

    if-eq v2, v3, :cond_2

    invoke-direct {v6, v1}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/Renderer;)V

    goto :goto_2

    :cond_2
    aget-boolean v2, v13, v14

    if-eqz v2, :cond_3

    iget-wide v2, v6, Lcom/google/android/exoplayer2/O;->I:J

    invoke-interface {v1, v2, v3}, Lcom/google/android/exoplayer2/Renderer;->a(J)V

    :cond_3
    :goto_2
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :cond_4
    invoke-direct {v6, v0}, Lcom/google/android/exoplayer2/O;->a([Z)V

    goto :goto_3

    :cond_5
    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/Z;)Z

    iget-boolean v0, v1, Lcom/google/android/exoplayer2/Z;->d:Z

    if-eqz v0, :cond_6

    iget-object v0, v1, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-wide v2, v0, Lcom/google/android/exoplayer2/aa;->b:J

    iget-wide v4, v6, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {v1, v4, v5}, Lcom/google/android/exoplayer2/Z;->d(J)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-virtual {v1, v9, v2, v3, v14}, Lcom/google/android/exoplayer2/Z;->a(Lcom/google/android/exoplayer2/trackselection/p;JZ)J

    :cond_6
    :goto_3
    invoke-direct {v6, v7}, Lcom/google/android/exoplayer2/O;->b(Z)V

    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v0, v0, Lcom/google/android/exoplayer2/fa;->e:I

    if-eq v0, v15, :cond_7

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->p()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->K()V

    iget-object v0, v6, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/util/l;->b(I)Z

    :cond_7
    return-void

    :cond_8
    if-ne v1, v2, :cond_9

    move v3, v14

    :cond_9
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    goto/16 :goto_0

    :cond_a
    :goto_4
    return-void
.end method

.method private B()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/aa;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->x:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/O;->y:Z

    return-void
.end method

.method private C()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    invoke-interface {v3}, Lcom/google/android/exoplayer2/Renderer;->j()Lcom/google/android/exoplayer2/source/SampleStream;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Lcom/google/android/exoplayer2/Renderer;->h()V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private D()Z
    .locals 6

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->F()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->y:Z

    if-eqz v0, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-nez v0, :cond_2

    return v1

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-wide v2, p0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->e()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/Z;->g:Z

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method private E()Z
    .locals 9

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->d()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->c()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/google/android/exoplayer2/O;->a(J)J

    move-result-wide v6

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-wide v1, p0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/Z;->d(J)J

    move-result-wide v0

    move-wide v4, v0

    goto :goto_0

    :cond_1
    iget-wide v1, p0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/Z;->d(J)J

    move-result-wide v1

    iget-object v0, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-wide v3, v0, Lcom/google/android/exoplayer2/aa;->b:J

    sub-long/2addr v1, v3

    move-wide v4, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->e:Lcom/google/android/exoplayer2/U;

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/J;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    iget v8, v0, Lcom/google/android/exoplayer2/ga;->b:F

    invoke-interface/range {v3 .. v8}, Lcom/google/android/exoplayer2/U;->a(JJF)Z

    move-result v0

    return v0
.end method

.method private F()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    if-eqz v1, :cond_0

    iget v0, v0, Lcom/google/android/exoplayer2/fa;->l:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private G()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/O;->z:Z

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/J;->b()V

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    invoke-static {v3}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/Renderer;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Lcom/google/android/exoplayer2/Renderer;->start()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private H()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/J;->c()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    invoke-static {v3}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/Renderer;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/Renderer;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private I()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->d()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/exoplayer2/O;->A:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/exoplayer2/Z;->a:Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/x;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-boolean v2, v1, Lcom/google/android/exoplayer2/fa;->g:Z

    if-eq v0, v2, :cond_2

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/fa;->a(Z)Lcom/google/android/exoplayer2/fa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    :cond_2
    return-void
.end method

.method private J()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/da;->c()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->r()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->t()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->u()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->s()V

    :cond_1
    :goto_0
    return-void
.end method

.method private K()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-boolean v1, v0, Lcom/google/android/exoplayer2/Z;->d:Z

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/exoplayer2/Z;->a:Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/source/x;->d()J

    move-result-wide v4

    move-wide v8, v4

    goto :goto_0

    :cond_1
    move-wide v8, v2

    :goto_0
    cmp-long v1, v8, v2

    if-eqz v1, :cond_2

    invoke-direct {p0, v8, v9}, Lcom/google/android/exoplayer2/O;->b(J)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/fa;->q:J

    cmp-long v0, v8, v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v7, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v10, v0, Lcom/google/android/exoplayer2/fa;->d:J

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/fa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/O$d;->c(I)V

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    iget-object v2, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object v2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/J;->a(Z)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/exoplayer2/O;->I:J

    iget-wide v1, p0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/Z;->d(J)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/fa;->q:J

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/exoplayer2/O;->b(JJ)V

    iget-object v2, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iput-wide v0, v2, Lcom/google/android/exoplayer2/fa;->q:J

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->d()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->a()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/exoplayer2/fa;->o:J

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->l()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/google/android/exoplayer2/fa;->p:J

    return-void
.end method

.method private a(J)J
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->d()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    const-wide/16 v1, 0x0

    if-nez v0, :cond_0

    return-wide v1

    :cond_0
    iget-wide v3, p0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {v0, v3, v4}, Lcom/google/android/exoplayer2/Z;->d(J)J

    move-result-wide v3

    sub-long/2addr p1, v3

    invoke-static {v1, v2, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method private a(Lcom/google/android/exoplayer2/source/y$a;JZ)J
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move v5, v0

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JZZ)J

    move-result-wide p1

    return-wide p1
.end method

.method private a(Lcom/google/android/exoplayer2/source/y$a;JZZ)J
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->H()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/O;->z:Z

    const/4 v1, 0x2

    if-nez p5, :cond_0

    iget-object p5, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget p5, p5, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 v2, 0x3

    if-ne p5, v2, :cond_1

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/O;->c(I)V

    :cond_1
    iget-object p5, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {p5}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object p5

    move-object v2, p5

    :goto_0
    if-eqz v2, :cond_3

    iget-object v3, v2, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v3, v3, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/source/y$a;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v2

    goto :goto_0

    :cond_3
    :goto_1
    const-wide/16 v3, 0x0

    if-nez p4, :cond_4

    if-ne p5, v2, :cond_4

    if-eqz v2, :cond_7

    invoke-virtual {v2, p2, p3}, Lcom/google/android/exoplayer2/Z;->e(J)J

    move-result-wide p4

    cmp-long p1, p4, v3

    if-gez p1, :cond_7

    :cond_4
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length p4, p1

    move p5, v0

    :goto_2
    if-ge p5, p4, :cond_5

    aget-object v5, p1, p5

    invoke-direct {p0, v5}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/Renderer;)V

    add-int/lit8 p5, p5, 0x1

    goto :goto_2

    :cond_5
    if-eqz v2, :cond_7

    :goto_3
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object p1

    if-eq p1, v2, :cond_6

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ba;->a()Lcom/google/android/exoplayer2/Z;

    goto :goto_3

    :cond_6
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/Z;)Z

    invoke-virtual {v2, v3, v4}, Lcom/google/android/exoplayer2/Z;->c(J)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->j()V

    :cond_7
    if-eqz v2, :cond_b

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/Z;)Z

    iget-boolean p1, v2, Lcom/google/android/exoplayer2/Z;->d:Z

    if-nez p1, :cond_8

    iget-object p1, v2, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    invoke-virtual {p1, p2, p3}, Lcom/google/android/exoplayer2/aa;->b(J)Lcom/google/android/exoplayer2/aa;

    move-result-object p1

    iput-object p1, v2, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    goto :goto_4

    :cond_8
    iget-object p1, v2, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-wide p4, p1, Lcom/google/android/exoplayer2/aa;->e:J

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p1, p4, v5

    if-eqz p1, :cond_9

    cmp-long p1, p2, p4

    if-ltz p1, :cond_9

    const-wide/16 p1, 0x1

    sub-long/2addr p4, p1

    invoke-static {v3, v4, p4, p5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p2

    :cond_9
    iget-boolean p1, v2, Lcom/google/android/exoplayer2/Z;->e:Z

    if-eqz p1, :cond_a

    iget-object p1, v2, Lcom/google/android/exoplayer2/Z;->a:Lcom/google/android/exoplayer2/source/x;

    invoke-interface {p1, p2, p3}, Lcom/google/android/exoplayer2/source/x;->a(J)J

    move-result-wide p1

    iget-object p3, v2, Lcom/google/android/exoplayer2/Z;->a:Lcom/google/android/exoplayer2/source/x;

    iget-wide p4, p0, Lcom/google/android/exoplayer2/O;->l:J

    sub-long p4, p1, p4

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/O;->m:Z

    invoke-interface {p3, p4, p5, v2}, Lcom/google/android/exoplayer2/source/x;->a(JZ)V

    move-wide p2, p1

    :cond_a
    :goto_4
    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer2/O;->b(J)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->p()V

    goto :goto_5

    :cond_b
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ba;->c()V

    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer2/O;->b(J)V

    :goto_5
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/O;->b(Z)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    invoke-interface {p1, v1}, Lcom/google/android/exoplayer2/util/l;->b(I)Z

    return-wide p2
.end method

.method private a(Lcom/google/android/exoplayer2/sa;)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/sa;",
            ")",
            "Landroid/util/Pair<",
            "Lcom/google/android/exoplayer2/source/y$a;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/exoplayer2/fa;->a()Lcom/google/android/exoplayer2/source/y$a;

    move-result-object p1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p1

    return-object p1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->C:Z

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/sa;->a(Z)I

    move-result v6

    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->j:Lcom/google/android/exoplayer2/sa$b;

    iget-object v5, p0, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/exoplayer2/sa;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IJ)Landroid/util/Pair;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, p1, v4, v1, v2}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;J)Lcom/google/android/exoplayer2/source/y$a;

    move-result-object v3

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v3, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p1, v0, v4}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    iget p1, v3, Lcom/google/android/exoplayer2/source/y$a;->c:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    iget v4, v3, Lcom/google/android/exoplayer2/source/y$a;->b:I

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/sa$a;->c(I)I

    move-result v0

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/sa$a;->b()J

    move-result-wide v1

    :cond_1
    move-wide v4, v1

    :cond_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {v3, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p1

    return-object p1
.end method

.method private static a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/O$g;ZIZLcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)Landroid/util/Pair;
    .locals 12
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/sa;",
            "Lcom/google/android/exoplayer2/O$g;",
            "ZIZ",
            "Lcom/google/android/exoplayer2/sa$b;",
            "Lcom/google/android/exoplayer2/sa$a;",
            ")",
            "Landroid/util/Pair<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    move-object v7, p0

    move-object v0, p1

    move-object/from16 v8, p6

    iget-object v1, v0, Lcom/google/android/exoplayer2/O$g;->a:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v2

    const/4 v9, 0x0

    if-eqz v2, :cond_0

    return-object v9

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v10, v7

    goto :goto_0

    :cond_1
    move-object v10, v1

    :goto_0
    :try_start_0
    iget v4, v0, Lcom/google/android/exoplayer2/O$g;->b:I

    iget-wide v5, v0, Lcom/google/android/exoplayer2/O$g;->c:J

    move-object v1, v10

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/exoplayer2/sa;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IJ)Landroid/util/Pair;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p0, v10}, Lcom/google/android/exoplayer2/sa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    return-object v1

    :cond_2
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v10, v2, v8}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    iget v2, v8, Lcom/google/android/exoplayer2/sa$a;->c:I

    move-object/from16 v11, p5

    invoke-virtual {v10, v2, v11}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/exoplayer2/sa$b;->m:Z

    if-eqz v2, :cond_3

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {p0, v1, v8}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v1

    iget v3, v1, Lcom/google/android/exoplayer2/sa$a;->c:I

    iget-wide v4, v0, Lcom/google/android/exoplayer2/O$g;->c:J

    move-object v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/sa;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IJ)Landroid/util/Pair;

    move-result-object v1

    :cond_3
    return-object v1

    :cond_4
    move-object/from16 v11, p5

    if-eqz p2, :cond_5

    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    move v2, p3

    move/from16 v3, p4

    move-object v5, v10

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IZLjava/lang/Object;Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/sa;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v0, v8}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v3, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    move-object v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/sa;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IJ)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :catch_0
    :cond_5
    return-object v9
.end method

.method private static a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/O$g;Lcom/google/android/exoplayer2/ba;IZLcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/O$f;
    .locals 21
    .param p2    # Lcom/google/android/exoplayer2/O$g;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move/from16 v10, p5

    move-object/from16 v11, p7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/exoplayer2/O$f;

    invoke-static {}, Lcom/google/android/exoplayer2/fa;->a()Lcom/google/android/exoplayer2/source/y$a;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object/from16 p0, v0

    move-object/from16 p1, v1

    move-wide/from16 p2, v2

    move-wide/from16 p4, v4

    move/from16 p6, v6

    move/from16 p7, v7

    invoke-direct/range {p0 .. p7}, Lcom/google/android/exoplayer2/O$f;-><init>(Lcom/google/android/exoplayer2/source/y$a;JJZZ)V

    return-object v0

    :cond_0
    iget-object v12, v8, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v13, v12, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    move-object/from16 v14, p6

    invoke-static {v8, v11, v14}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/sa$a;Lcom/google/android/exoplayer2/sa$b;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v1, v8, Lcom/google/android/exoplayer2/fa;->d:J

    goto :goto_0

    :cond_1
    iget-wide v1, v8, Lcom/google/android/exoplayer2/fa;->q:J

    :goto_0
    move-wide v15, v1

    const-wide v17, -0x7fffffffffffffffL    # -4.9E-324

    const/16 v19, 0x1

    const/16 v20, 0x0

    const/4 v6, -0x1

    if-eqz v9, :cond_5

    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v3, p4

    move/from16 v4, p5

    move-object/from16 v5, p6

    move v14, v6

    move-object/from16 v6, p7

    invoke-static/range {v0 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/O$g;ZIZLcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)Landroid/util/Pair;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {v7, v10}, Lcom/google/android/exoplayer2/sa;->a(Z)I

    move-result v0

    move/from16 v2, v19

    move/from16 v1, v20

    goto :goto_3

    :cond_2
    iget-wide v1, v9, Lcom/google/android/exoplayer2/O$g;->c:J

    cmp-long v1, v1, v17

    if-nez v1, :cond_3

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v7, v0, v11}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v6, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    move v0, v6

    goto :goto_1

    :cond_3
    iget-object v13, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move v0, v14

    :goto_1
    iget v1, v8, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    move/from16 v1, v19

    goto :goto_2

    :cond_4
    move/from16 v1, v20

    :goto_2
    move/from16 v2, v20

    :goto_3
    move v3, v0

    move v6, v1

    move v9, v2

    goto/16 :goto_7

    :cond_5
    move v14, v6

    iget-object v1, v8, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v7, v10}, Lcom/google/android/exoplayer2/sa;->a(Z)I

    move-result v0

    :goto_4
    move v3, v0

    :goto_5
    move/from16 v6, v20

    move v9, v6

    goto/16 :goto_7

    :cond_6
    invoke-virtual {v7, v13}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v1

    if-ne v1, v14, :cond_8

    iget-object v5, v8, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    move-object/from16 v0, p6

    move-object/from16 v1, p7

    move/from16 v2, p4

    move/from16 v3, p5

    move-object v4, v13

    move-object/from16 v6, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IZLjava/lang/Object;Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/sa;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_7

    invoke-virtual {v7, v10}, Lcom/google/android/exoplayer2/sa;->a(Z)I

    move-result v0

    move/from16 v1, v19

    goto :goto_6

    :cond_7
    invoke-virtual {v7, v0, v11}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    move/from16 v1, v20

    :goto_6
    move v3, v0

    move v9, v1

    move/from16 v6, v20

    goto :goto_7

    :cond_8
    if-eqz v0, :cond_a

    cmp-long v0, v15, v17

    if-nez v0, :cond_9

    invoke-virtual {v7, v13, v11}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    goto :goto_4

    :cond_9
    iget-object v0, v8, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v1, v12, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v11}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual/range {p7 .. p7}, Lcom/google/android/exoplayer2/sa$a;->e()J

    move-result-wide v0

    add-long v4, v15, v0

    invoke-virtual {v7, v13, v11}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v3, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/sa;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IJ)Landroid/util/Pair;

    move-result-object v0

    iget-object v13, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    :cond_a
    move v3, v14

    goto :goto_5

    :goto_7
    if-eq v3, v14, :cond_b

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/sa;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IJ)Landroid/util/Pair;

    move-result-object v0

    iget-object v13, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-object/from16 v0, p3

    move-wide v1, v15

    move-wide/from16 v15, v17

    goto :goto_8

    :cond_b
    move-object/from16 v0, p3

    move-wide v1, v15

    :goto_8
    invoke-virtual {v0, v7, v13, v1, v2}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;J)Lcom/google/android/exoplayer2/source/y$a;

    move-result-object v0

    iget v3, v0, Lcom/google/android/exoplayer2/source/y$a;->e:I

    if-eq v3, v14, :cond_d

    iget v3, v12, Lcom/google/android/exoplayer2/source/y$a;->e:I

    if-eq v3, v14, :cond_c

    iget v4, v0, Lcom/google/android/exoplayer2/source/y$a;->b:I

    if-lt v4, v3, :cond_c

    goto :goto_9

    :cond_c
    move/from16 v3, v20

    goto :goto_a

    :cond_d
    :goto_9
    move/from16 v3, v19

    :goto_a
    iget-object v4, v12, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {v4, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual {v12}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v4

    if-nez v4, :cond_e

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v4

    if-nez v4, :cond_e

    if-eqz v3, :cond_e

    goto :goto_b

    :cond_e
    move/from16 v19, v20

    :goto_b
    if-eqz v19, :cond_f

    move-object v0, v12

    :cond_f
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-virtual {v0, v12}, Lcom/google/android/exoplayer2/source/y$a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-wide v1, v8, Lcom/google/android/exoplayer2/fa;->q:J

    goto :goto_c

    :cond_10
    iget-object v1, v0, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {v7, v1, v11}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    iget v1, v0, Lcom/google/android/exoplayer2/source/y$a;->c:I

    iget v2, v0, Lcom/google/android/exoplayer2/source/y$a;->b:I

    invoke-virtual {v11, v2}, Lcom/google/android/exoplayer2/sa$a;->c(I)I

    move-result v2

    if-ne v1, v2, :cond_11

    invoke-virtual/range {p7 .. p7}, Lcom/google/android/exoplayer2/sa$a;->b()J

    move-result-wide v1

    goto :goto_c

    :cond_11
    const-wide/16 v1, 0x0

    :cond_12
    :goto_c
    new-instance v3, Lcom/google/android/exoplayer2/O$f;

    move-object/from16 p0, v3

    move-object/from16 p1, v0

    move-wide/from16 p2, v1

    move-wide/from16 p4, v15

    move/from16 p6, v6

    move/from16 p7, v9

    invoke-direct/range {p0 .. p7}, Lcom/google/android/exoplayer2/O$f;-><init>(Lcom/google/android/exoplayer2/source/y$a;JJZZ)V

    return-object v3
.end method

.method private a(Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/fa;
    .locals 11
    .annotation build Landroidx/annotation/CheckResult;
    .end annotation

    move-object v0, p0

    move-object v2, p1

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/O;->K:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v3, v1, Lcom/google/android/exoplayer2/fa;->q:J

    cmp-long v1, p2, v3

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {p1, v1}, Lcom/google/android/exoplayer2/source/y$a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, v0, Lcom/google/android/exoplayer2/O;->K:Z

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->B()V

    iget-object v1, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v3, v1, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v1, v1, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object v4, v0, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/da;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v1, v0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v3, Lcom/google/android/exoplayer2/source/TrackGroupArray;->a:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    goto :goto_2

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->f()Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v3

    :goto_2
    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/android/exoplayer2/O;->d:Lcom/google/android/exoplayer2/trackselection/p;

    goto :goto_3

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->g()Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v1

    goto :goto_3

    :cond_4
    iget-object v4, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v4, v4, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {p1, v4}, Lcom/google/android/exoplayer2/source/y$a;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    sget-object v1, Lcom/google/android/exoplayer2/source/TrackGroupArray;->a:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v3, v0, Lcom/google/android/exoplayer2/O;->d:Lcom/google/android/exoplayer2/trackselection/p;

    move-object v9, v1

    move-object v10, v3

    goto :goto_4

    :cond_5
    :goto_3
    move-object v10, v1

    move-object v9, v3

    :goto_4
    iget-object v1, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->l()J

    move-result-wide v7

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-virtual/range {v1 .. v10}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/source/y$a;JJJLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;)Lcom/google/android/exoplayer2/fa;

    move-result-object v1

    return-object v1
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/O;)Lcom/google/android/exoplayer2/util/l;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    return-object p0
.end method

.method static a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IZLjava/lang/Object;Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/sa;)Ljava/lang/Object;
    .locals 9
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-virtual {p5, p4}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result p4

    invoke-virtual {p5}, Lcom/google/android/exoplayer2/sa;->a()I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x0

    move v4, p4

    move p4, v1

    :goto_0
    if-ge v2, v0, :cond_1

    if-ne p4, v1, :cond_1

    move-object v3, p5

    move-object v5, p1

    move-object v6, p0

    move v7, p2

    move v8, p3

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;Lcom/google/android/exoplayer2/sa$b;IZ)I

    move-result v4

    if-ne v4, v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p5, v4}, Lcom/google/android/exoplayer2/sa;->a(I)Ljava/lang/Object;

    move-result-object p4

    invoke-virtual {p6, p4}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result p4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-ne p4, v1, :cond_2

    const/4 p0, 0x0

    goto :goto_2

    :cond_2
    invoke-virtual {p6, p4}, Lcom/google/android/exoplayer2/sa;->a(I)Ljava/lang/Object;

    move-result-object p0

    :goto_2
    return-object p0
.end method

.method private a(F)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->g()Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/exoplayer2/trackselection/p;->c:Lcom/google/android/exoplayer2/trackselection/m;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/trackselection/m;->a()[Lcom/google/android/exoplayer2/trackselection/l;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    if-eqz v4, :cond_0

    invoke-interface {v4, p1}, Lcom/google/android/exoplayer2/trackselection/l;->a(F)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(IILcom/google/android/exoplayer2/source/J;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/O$d;->a(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/da;->a(IILcom/google/android/exoplayer2/source/J;)Lcom/google/android/exoplayer2/sa;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/sa;)V

    return-void
.end method

.method private a(IZ)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p1

    iget-object v2, v0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    aget-object v2, v2, v1

    invoke-static {v2}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/Renderer;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-void

    :cond_0
    iget-object v3, v0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-ne v3, v4, :cond_1

    move v10, v6

    goto :goto_0

    :cond_1
    move v10, v5

    :goto_0
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/Z;->g()Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v4

    iget-object v7, v4, Lcom/google/android/exoplayer2/trackselection/p;->b:[Lcom/google/android/exoplayer2/ka;

    aget-object v7, v7, v1

    iget-object v4, v4, Lcom/google/android/exoplayer2/trackselection/p;->c:Lcom/google/android/exoplayer2/trackselection/m;

    invoke-virtual {v4, v1}, Lcom/google/android/exoplayer2/trackselection/m;->a(I)Lcom/google/android/exoplayer2/trackselection/l;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/trackselection/l;)[Lcom/google/android/exoplayer2/Format;

    move-result-object v8

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->F()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v4, v4, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 v9, 0x3

    if-ne v4, v9, :cond_2

    move v15, v6

    goto :goto_1

    :cond_2
    move v15, v5

    :goto_1
    if-nez p2, :cond_3

    if-eqz v15, :cond_3

    move v9, v6

    goto :goto_2

    :cond_3
    move v9, v5

    :goto_2
    iget v4, v0, Lcom/google/android/exoplayer2/O;->G:I

    add-int/2addr v4, v6

    iput v4, v0, Lcom/google/android/exoplayer2/O;->G:I

    iget-object v4, v3, Lcom/google/android/exoplayer2/Z;->c:[Lcom/google/android/exoplayer2/source/SampleStream;

    aget-object v6, v4, v1

    iget-wide v11, v0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/Z;->e()J

    move-result-wide v13

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/Z;->d()J

    move-result-wide v16

    move-object v3, v2

    move-object v4, v7

    move-object v5, v8

    move-wide v7, v11

    move-wide v11, v13

    move-wide/from16 v13, v16

    invoke-interface/range {v3 .. v14}, Lcom/google/android/exoplayer2/Renderer;->a(Lcom/google/android/exoplayer2/ka;[Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/source/SampleStream;JZZJJ)V

    const/16 v1, 0x67

    new-instance v3, Lcom/google/android/exoplayer2/N;

    invoke-direct {v3, v0}, Lcom/google/android/exoplayer2/N;-><init>(Lcom/google/android/exoplayer2/O;)V

    invoke-interface {v2, v1, v3}, Lcom/google/android/exoplayer2/ia$b;->a(ILjava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/J;->b(Lcom/google/android/exoplayer2/Renderer;)V

    if-eqz v15, :cond_4

    invoke-interface {v2}, Lcom/google/android/exoplayer2/Renderer;->start()V

    :cond_4
    return-void
.end method

.method private a(JJ)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->F:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->E:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/O;->c(JJ)V

    return-void
.end method

.method private declared-synchronized a(Lb/a/a/a/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/a/a/h<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    invoke-interface {p1}, Lb/a/a/a/h;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized a(Lb/a/a/a/h;J)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a/a/a/h<",
            "Ljava/lang/Boolean;",
            ">;J)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->p:Lcom/google/android/exoplayer2/util/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/util/e;->b()J

    move-result-wide v0

    add-long/2addr v0, p2

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p1}, Lb/a/a/a/h;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    const-wide/16 v3, 0x0

    cmp-long v3, p2, v3

    if-lez v3, :cond_0

    :try_start_1
    invoke-virtual {p0, p2, p3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    const/4 p2, 0x1

    move v2, p2

    :goto_1
    :try_start_2
    iget-object p2, p0, Lcom/google/android/exoplayer2/O;->p:Lcom/google/android/exoplayer2/util/e;

    invoke-interface {p2}, Lcom/google/android/exoplayer2/util/e;->b()J

    move-result-wide p2

    sub-long p2, v0, p2

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private a(Lcom/google/android/exoplayer2/O$a;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/O$d;->a(I)V

    invoke-static {p1}, Lcom/google/android/exoplayer2/O$a;->a(Lcom/google/android/exoplayer2/O$a;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/android/exoplayer2/O$g;

    new-instance v1, Lcom/google/android/exoplayer2/ja;

    invoke-static {p1}, Lcom/google/android/exoplayer2/O$a;->b(Lcom/google/android/exoplayer2/O$a;)Ljava/util/List;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/exoplayer2/O$a;->c(Lcom/google/android/exoplayer2/O$a;)Lcom/google/android/exoplayer2/source/J;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/exoplayer2/ja;-><init>(Ljava/util/Collection;Lcom/google/android/exoplayer2/source/J;)V

    invoke-static {p1}, Lcom/google/android/exoplayer2/O$a;->a(Lcom/google/android/exoplayer2/O$a;)I

    move-result v2

    invoke-static {p1}, Lcom/google/android/exoplayer2/O$a;->d(Lcom/google/android/exoplayer2/O$a;)J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/exoplayer2/O$g;-><init>(Lcom/google/android/exoplayer2/sa;IJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/O;->H:Lcom/google/android/exoplayer2/O$g;

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    invoke-static {p1}, Lcom/google/android/exoplayer2/O$a;->b(Lcom/google/android/exoplayer2/O$a;)Ljava/util/List;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/exoplayer2/O$a;->c(Lcom/google/android/exoplayer2/O$a;)Lcom/google/android/exoplayer2/source/J;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/da;->a(Ljava/util/List;Lcom/google/android/exoplayer2/source/J;)Lcom/google/android/exoplayer2/sa;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/sa;)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/O$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/O$d;->a(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/da;->b()I

    move-result p2

    :cond_0
    invoke-static {p1}, Lcom/google/android/exoplayer2/O$a;->b(Lcom/google/android/exoplayer2/O$a;)Ljava/util/List;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/exoplayer2/O$a;->c(Lcom/google/android/exoplayer2/O$a;)Lcom/google/android/exoplayer2/source/J;

    move-result-object p1

    invoke-virtual {v0, p2, v1, p1}, Lcom/google/android/exoplayer2/da;->a(ILjava/util/List;Lcom/google/android/exoplayer2/source/J;)Lcom/google/android/exoplayer2/sa;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/sa;)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/O$b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/O$d;->a(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    iget v1, p1, Lcom/google/android/exoplayer2/O$b;->a:I

    iget v2, p1, Lcom/google/android/exoplayer2/O$b;->b:I

    iget v3, p1, Lcom/google/android/exoplayer2/O$b;->c:I

    iget-object p1, p1, Lcom/google/android/exoplayer2/O$b;->d:Lcom/google/android/exoplayer2/source/J;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/google/android/exoplayer2/da;->a(IIILcom/google/android/exoplayer2/source/J;)Lcom/google/android/exoplayer2/sa;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/sa;)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/O$g;)V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v0, p1

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/O$d;->a(I)V

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v8, v1, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget v11, v7, Lcom/google/android/exoplayer2/O;->B:I

    iget-boolean v12, v7, Lcom/google/android/exoplayer2/O;->C:Z

    iget-object v13, v7, Lcom/google/android/exoplayer2/O;->j:Lcom/google/android/exoplayer2/sa$b;

    iget-object v14, v7, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    const/4 v10, 0x1

    move-object/from16 v9, p1

    invoke-static/range {v8 .. v14}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/O$g;ZIZLcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)Landroid/util/Pair;

    move-result-object v1

    const-wide/16 v3, 0x0

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v8, 0x0

    if-nez v1, :cond_0

    iget-object v9, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v9, v9, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-direct {v7, v9}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa;)Landroid/util/Pair;

    move-result-object v9

    iget-object v10, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/exoplayer2/source/y$a;

    iget-object v9, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    iget-object v9, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v9, v9, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v9}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v9

    xor-int/2addr v9, v2

    move-object/from16 v19, v10

    move v10, v9

    move-object/from16 v9, v19

    move-wide/from16 v20, v5

    move-wide v5, v11

    move-wide/from16 v12, v20

    goto :goto_3

    :cond_0
    iget-object v9, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v10, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iget-wide v12, v0, Lcom/google/android/exoplayer2/O$g;->c:J

    cmp-long v12, v12, v5

    if-nez v12, :cond_1

    move-wide v12, v5

    goto :goto_0

    :cond_1
    move-wide v12, v10

    :goto_0
    iget-object v14, v7, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    iget-object v15, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v15, v15, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v14, v15, v9, v10, v11}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;J)Lcom/google/android/exoplayer2/source/y$a;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v14

    if-eqz v14, :cond_3

    iget-object v5, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v5, v5, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v6, v9, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v10, v7, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v5, v6, v10}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    iget-object v5, v7, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    iget v6, v9, Lcom/google/android/exoplayer2/source/y$a;->b:I

    invoke-virtual {v5, v6}, Lcom/google/android/exoplayer2/sa$a;->c(I)I

    move-result v5

    iget v6, v9, Lcom/google/android/exoplayer2/source/y$a;->c:I

    if-ne v5, v6, :cond_2

    iget-object v5, v7, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/sa$a;->b()J

    move-result-wide v5

    goto :goto_1

    :cond_2
    move-wide v5, v3

    :goto_1
    move v10, v2

    goto :goto_3

    :cond_3
    iget-wide v14, v0, Lcom/google/android/exoplayer2/O$g;->c:J

    cmp-long v5, v14, v5

    if-nez v5, :cond_4

    move v5, v2

    goto :goto_2

    :cond_4
    move v5, v8

    :goto_2
    move-wide/from16 v19, v10

    move v10, v5

    move-wide/from16 v5, v19

    :goto_3
    const/4 v11, 0x2

    :try_start_0
    iget-object v14, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v14, v14, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v14}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v14

    if-eqz v14, :cond_5

    iput-object v0, v7, Lcom/google/android/exoplayer2/O;->H:Lcom/google/android/exoplayer2/O$g;

    goto :goto_4

    :cond_5
    const/4 v0, 0x4

    if-nez v1, :cond_7

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v1, v1, Lcom/google/android/exoplayer2/fa;->e:I

    if-eq v1, v2, :cond_6

    invoke-direct {v7, v0}, Lcom/google/android/exoplayer2/O;->c(I)V

    :cond_6
    invoke-direct {v7, v8, v2, v8, v2}, Lcom/google/android/exoplayer2/O;->a(ZZZZ)V

    :goto_4
    move-wide v3, v5

    goto/16 :goto_9

    :cond_7
    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v9, v1}, Lcom/google/android/exoplayer2/source/y$a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-boolean v14, v1, Lcom/google/android/exoplayer2/Z;->d:Z

    if-eqz v14, :cond_8

    cmp-long v3, v5, v3

    if-eqz v3, :cond_8

    iget-object v1, v1, Lcom/google/android/exoplayer2/Z;->a:Lcom/google/android/exoplayer2/source/x;

    iget-object v3, v7, Lcom/google/android/exoplayer2/O;->t:Lcom/google/android/exoplayer2/ma;

    invoke-interface {v1, v5, v6, v3}, Lcom/google/android/exoplayer2/source/x;->a(JLcom/google/android/exoplayer2/ma;)J

    move-result-wide v3

    goto :goto_5

    :cond_8
    move-wide v3, v5

    :goto_5
    invoke-static {v3, v4}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v14

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    move-wide/from16 v17, v3

    iget-wide v2, v1, Lcom/google/android/exoplayer2/fa;->q:J

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v1

    cmp-long v1, v14, v1

    if-nez v1, :cond_b

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v1, v1, Lcom/google/android/exoplayer2/fa;->e:I

    if-eq v1, v11, :cond_9

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v1, v1, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_b

    :cond_9
    iget-object v0, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v3, v0, Lcom/google/android/exoplayer2/fa;->q:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v1, p0

    move-object v2, v9

    move-wide v5, v12

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/fa;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    if-eqz v10, :cond_a

    iget-object v0, v7, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    invoke-virtual {v0, v11}, Lcom/google/android/exoplayer2/O$d;->c(I)V

    :cond_a
    return-void

    :cond_b
    move-wide/from16 v1, v17

    goto :goto_6

    :cond_c
    move-wide v1, v5

    :goto_6
    :try_start_1
    iget-object v3, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v3, v3, Lcom/google/android/exoplayer2/fa;->e:I

    if-ne v3, v0, :cond_d

    const/4 v0, 0x1

    goto :goto_7

    :cond_d
    move v0, v8

    :goto_7
    invoke-direct {v7, v9, v1, v2, v0}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JZ)J

    move-result-wide v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v2, v5, v0

    if-eqz v2, :cond_e

    const/16 v16, 0x1

    goto :goto_8

    :cond_e
    move/from16 v16, v8

    :goto_8
    or-int v10, v10, v16

    move-wide v3, v0

    :goto_9
    move-object/from16 v1, p0

    move-object v2, v9

    move-wide v5, v12

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/fa;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    if-eqz v10, :cond_f

    iget-object v0, v7, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    invoke-virtual {v0, v11}, Lcom/google/android/exoplayer2/O$d;->c(I)V

    :cond_f
    return-void

    :catchall_0
    move-exception v0

    move-object/from16 v1, p0

    move-object v2, v9

    move-wide v3, v5

    move-wide v5, v12

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/fa;

    move-result-object v1

    iput-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    if-eqz v10, :cond_10

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    invoke-virtual {v1, v11}, Lcom/google/android/exoplayer2/O$d;->c(I)V

    :cond_10
    throw v0
.end method

.method private a(Lcom/google/android/exoplayer2/Renderer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/Renderer;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/J;->a(Lcom/google/android/exoplayer2/Renderer;)V

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/Renderer;)V

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Renderer;->d()V

    iget p1, p0, Lcom/google/android/exoplayer2/O;->G:I

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/google/android/exoplayer2/O;->G:I

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/ga;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    invoke-virtual {v0, p2}, Lcom/google/android/exoplayer2/O$d;->a(I)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {p2, p1}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/ga;)Lcom/google/android/exoplayer2/fa;

    move-result-object p2

    iput-object p2, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget p2, p1, Lcom/google/android/exoplayer2/ga;->b:F

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/O;->a(F)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p2, v1

    if-eqz v2, :cond_0

    iget v3, p1, Lcom/google/android/exoplayer2/ga;->b:F

    invoke-interface {v2, v3}, Lcom/google/android/exoplayer2/Renderer;->a(F)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/ma;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->t:Lcom/google/android/exoplayer2/ma;

    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/O$c;Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)V
    .locals 4

    iget-object v0, p1, Lcom/google/android/exoplayer2/O$c;->d:Ljava/lang/Object;

    invoke-virtual {p0, v0, p3}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object p2

    iget p2, p2, Lcom/google/android/exoplayer2/sa$b;->o:I

    const/4 v0, 0x1

    invoke-virtual {p0, p2, p3, v0}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;Z)Lcom/google/android/exoplayer2/sa$a;

    move-result-object p0

    iget-object p0, p0, Lcom/google/android/exoplayer2/sa$a;->b:Ljava/lang/Object;

    iget-wide v0, p3, Lcom/google/android/exoplayer2/sa$a;->d:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p3, v0, v2

    if-eqz p3, :cond_0

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    goto :goto_0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    invoke-virtual {p1, p2, v0, v1, p0}, Lcom/google/android/exoplayer2/O$c;->a(IJLjava/lang/Object;)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/sa;)V
    .locals 9

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/google/android/exoplayer2/O$c;

    iget v5, p0, Lcom/google/android/exoplayer2/O;->B:I

    iget-boolean v6, p0, Lcom/google/android/exoplayer2/O;->C:Z

    iget-object v7, p0, Lcom/google/android/exoplayer2/O;->j:Lcom/google/android/exoplayer2/sa$b;

    iget-object v8, p0, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v2 .. v8}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/O$c;Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/sa;IZLcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/O$c;

    iget-object v1, v1, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/ia;->a(Z)V

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/source/J;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/O$d;->a(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/da;->a(Lcom/google/android/exoplayer2/source/J;)Lcom/google/android/exoplayer2/sa;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/sa;)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->e:Lcom/google/android/exoplayer2/U;

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    iget-object p2, p2, Lcom/google/android/exoplayer2/trackselection/p;->c:Lcom/google/android/exoplayer2/trackselection/m;

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/exoplayer2/U;->a([Lcom/google/android/exoplayer2/Renderer;Lcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/m;)V

    return-void
.end method

.method private a(ZIZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    invoke-virtual {v0, p3}, Lcom/google/android/exoplayer2/O$d;->a(I)V

    iget-object p3, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    invoke-virtual {p3, p4}, Lcom/google/android/exoplayer2/O$d;->b(I)V

    iget-object p3, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {p3, p1, p2}, Lcom/google/android/exoplayer2/fa;->a(ZI)Lcom/google/android/exoplayer2/fa;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/O;->z:Z

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->F()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->H()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->K()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget p1, p1, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 p2, 0x3

    const/4 p3, 0x2

    if-ne p1, p2, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->G()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    invoke-interface {p1, p3}, Lcom/google/android/exoplayer2/util/l;->b(I)Z

    goto :goto_0

    :cond_1
    if-ne p1, p3, :cond_2

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    invoke-interface {p1, p3}, Lcom/google/android/exoplayer2/util/l;->b(I)Z

    :cond_2
    :goto_0
    return-void
.end method

.method private a(ZLjava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 4
    .param p2    # Ljava/util/concurrent/atomic/AtomicBoolean;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->D:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/O;->D:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p1, v1

    invoke-static {v2}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/Renderer;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2}, Lcom/google/android/exoplayer2/Renderer;->reset()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    monitor-enter p0

    const/4 p1, 0x1

    :try_start_0
    invoke-virtual {p2, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method private a(ZZ)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/O;->D:Z

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    move p1, v0

    goto :goto_1

    :cond_1
    :goto_0
    move p1, v1

    :goto_1
    invoke-direct {p0, p1, v0, v1, v0}, Lcom/google/android/exoplayer2/O;->a(ZZZZ)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/O$d;->a(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->e:Lcom/google/android/exoplayer2/U;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/U;->onStopped()V

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/O;->c(I)V

    return-void
.end method

.method private a(ZZZZ)V
    .locals 30

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Lcom/google/android/exoplayer2/util/l;->c(I)V

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/exoplayer2/O;->z:Z

    iget-object v0, v1, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/J;->c()V

    const-wide/16 v3, 0x0

    iput-wide v3, v1, Lcom/google/android/exoplayer2/O;->I:J

    iget-object v3, v1, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v4, v3

    move v5, v2

    :goto_0
    const-string v6, "ExoPlayerImplInternal"

    if-ge v5, v4, :cond_0

    aget-object v0, v3, v5

    :try_start_0
    invoke-direct {v1, v0}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/Renderer;)V
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ExoPlaybackException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    :goto_1
    const-string v7, "Disable failed."

    invoke-static {v6, v7, v0}, Lcom/google/android/exoplayer2/util/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    iget-object v3, v1, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v4, v3

    move v5, v2

    :goto_3
    if-ge v5, v4, :cond_1

    aget-object v0, v3, v5

    :try_start_1
    invoke-interface {v0}, Lcom/google/android/exoplayer2/Renderer;->reset()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v7, v0

    const-string v0, "Reset failed."

    invoke-static {v6, v0, v7}, Lcom/google/android/exoplayer2/util/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_1
    iput v2, v1, Lcom/google/android/exoplayer2/O;->G:I

    iget-object v0, v1, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v3, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/fa;->q:J

    iget-object v0, v1, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v6, v1, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    iget-object v7, v1, Lcom/google/android/exoplayer2/O;->j:Lcom/google/android/exoplayer2/sa$b;

    invoke-static {v0, v6, v7}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/sa$a;Lcom/google/android/exoplayer2/sa$b;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v6, v0, Lcom/google/android/exoplayer2/fa;->d:J

    goto :goto_5

    :cond_2
    iget-object v0, v1, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v6, v0, Lcom/google/android/exoplayer2/fa;->q:J

    :goto_5
    const/4 v0, 0x0

    if-eqz p2, :cond_4

    iput-object v0, v1, Lcom/google/android/exoplayer2/O;->H:Lcom/google/android/exoplayer2/O$g;

    iget-object v3, v1, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v3, v3, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-direct {v1, v3}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa;)Landroid/util/Pair;

    move-result-object v3

    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/exoplayer2/source/y$a;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    iget-object v3, v1, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v3, v3, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v4, v3}, Lcom/google/android/exoplayer2/source/y$a;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x1

    goto :goto_6

    :cond_3
    move v3, v2

    :goto_6
    move-object/from16 v19, v4

    move-wide/from16 v27, v5

    move-wide v12, v7

    goto :goto_7

    :cond_4
    move-object/from16 v19, v3

    move-wide/from16 v27, v4

    move-wide v12, v6

    move v3, v2

    :goto_7
    iget-object v4, v1, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/ba;->c()V

    iput-boolean v2, v1, Lcom/google/android/exoplayer2/O;->A:Z

    new-instance v2, Lcom/google/android/exoplayer2/fa;

    iget-object v4, v1, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v10, v4, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget v14, v4, Lcom/google/android/exoplayer2/fa;->e:I

    if-eqz p4, :cond_5

    goto :goto_8

    :cond_5
    iget-object v0, v4, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    :goto_8
    move-object v15, v0

    const/16 v16, 0x0

    if-eqz v3, :cond_6

    sget-object v0, Lcom/google/android/exoplayer2/source/TrackGroupArray;->a:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    goto :goto_9

    :cond_6
    iget-object v0, v1, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    :goto_9
    move-object/from16 v17, v0

    if-eqz v3, :cond_7

    iget-object v0, v1, Lcom/google/android/exoplayer2/O;->d:Lcom/google/android/exoplayer2/trackselection/p;

    goto :goto_a

    :cond_7
    iget-object v0, v1, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    :goto_a
    move-object/from16 v18, v0

    iget-object v0, v1, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-boolean v3, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    move/from16 v20, v3

    iget v3, v0, Lcom/google/android/exoplayer2/fa;->l:I

    move/from16 v21, v3

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    move-object/from16 v22, v0

    const-wide/16 v25, 0x0

    iget-boolean v0, v1, Lcom/google/android/exoplayer2/O;->F:Z

    move/from16 v29, v0

    move-object v9, v2

    move-object/from16 v11, v19

    move-wide/from16 v23, v27

    invoke-direct/range {v9 .. v29}, Lcom/google/android/exoplayer2/fa;-><init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V

    iput-object v2, v1, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    if-eqz p3, :cond_8

    iget-object v0, v1, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/da;->d()V

    :cond_8
    return-void
.end method

.method private a([Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->g()Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v4, v4

    if-ge v3, v4, :cond_1

    invoke-virtual {v1, v3}, Lcom/google/android/exoplayer2/trackselection/p;->a(I)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    aget-object v4, v4, v3

    invoke-interface {v4}, Lcom/google/android/exoplayer2/Renderer;->reset()V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v3, v3

    if-ge v2, v3, :cond_3

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/trackselection/p;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    aget-boolean v3, p1, v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/exoplayer2/O;->a(IZ)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x1

    iput-boolean p1, v0, Lcom/google/android/exoplayer2/Z;->g:Z

    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/O$c;Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/sa;IZLcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)Z
    .locals 15

    move-object v0, p0

    move-object/from16 v8, p1

    move-object/from16 v1, p2

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    iget-object v2, v0, Lcom/google/android/exoplayer2/O$c;->d:Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x1

    const-wide/high16 v13, -0x8000000000000000L

    if-nez v2, :cond_3

    iget-object v1, v0, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ia;->e()J

    move-result-wide v1

    cmp-long v1, v1, v13

    if-nez v1, :cond_0

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ia;->e()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/android/exoplayer2/C;->a(J)J

    move-result-wide v1

    :goto_0
    new-instance v3, Lcom/google/android/exoplayer2/O$g;

    iget-object v4, v0, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/ia;->g()Lcom/google/android/exoplayer2/sa;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/ia;->i()I

    move-result v5

    invoke-direct {v3, v4, v5, v1, v2}, Lcom/google/android/exoplayer2/O$g;-><init>(Lcom/google/android/exoplayer2/sa;IJ)V

    const/4 v4, 0x0

    move-object/from16 v1, p1

    move-object v2, v3

    move v3, v4

    move/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-static/range {v1 .. v7}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/O$g;ZIZLcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)Landroid/util/Pair;

    move-result-object v1

    if-nez v1, :cond_1

    return v11

    :cond_1
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v8, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v4, v1}, Lcom/google/android/exoplayer2/O$c;->a(IJLjava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ia;->e()J

    move-result-wide v1

    cmp-long v1, v1, v13

    if-nez v1, :cond_2

    invoke-static {v8, p0, v9, v10}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/O$c;Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)V

    :cond_2
    return v12

    :cond_3
    invoke-virtual {v8, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    return v11

    :cond_4
    iget-object v3, v0, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/ia;->e()J

    move-result-wide v3

    cmp-long v3, v3, v13

    if-nez v3, :cond_5

    invoke-static {v8, p0, v9, v10}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/O$c;Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)V

    return v12

    :cond_5
    iput v2, v0, Lcom/google/android/exoplayer2/O$c;->b:I

    iget-object v2, v0, Lcom/google/android/exoplayer2/O$c;->d:Ljava/lang/Object;

    invoke-virtual {v1, v2, v10}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    iget v2, v10, Lcom/google/android/exoplayer2/sa$a;->c:I

    invoke-virtual {v1, v2, v9}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/exoplayer2/sa$b;->m:Z

    if-eqz v1, :cond_6

    iget-wide v1, v0, Lcom/google/android/exoplayer2/O$c;->c:J

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/exoplayer2/sa$a;->e()J

    move-result-wide v3

    add-long v5, v1, v3

    iget-object v1, v0, Lcom/google/android/exoplayer2/O$c;->d:Ljava/lang/Object;

    invoke-virtual {v8, v1, v10}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v1

    iget v4, v1, Lcom/google/android/exoplayer2/sa$a;->c:I

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/exoplayer2/sa;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IJ)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v8, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v4, v1}, Lcom/google/android/exoplayer2/O$c;->a(IJLjava/lang/Object;)V

    :cond_6
    return v12
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/O;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/O;->E:Z

    return p1
.end method

.method private static a(Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/sa$a;Lcom/google/android/exoplayer2/sa$b;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object p0, p0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object p1

    iget p1, p1, Lcom/google/android/exoplayer2/sa$a;->c:I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object p0

    iget-boolean p0, p0, Lcom/google/android/exoplayer2/sa$b;->m:Z

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static a(Lcom/google/android/exoplayer2/trackselection/l;)[Lcom/google/android/exoplayer2/Format;
    .locals 4

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/google/android/exoplayer2/trackselection/l;->length()I

    move-result v1

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    new-array v2, v1, [Lcom/google/android/exoplayer2/Format;

    :goto_1
    if-ge v0, v1, :cond_1

    invoke-interface {p0, v0}, Lcom/google/android/exoplayer2/trackselection/l;->a(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method private b(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iput p1, p0, Lcom/google/android/exoplayer2/O;->B:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;I)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->c(Z)V

    :cond_0
    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->b(Z)V

    return-void
.end method

.method private b(J)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/Z;->e(J)J

    move-result-wide p1

    :goto_0
    iput-wide p1, p0, Lcom/google/android/exoplayer2/O;->I:J

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    iget-wide v0, p0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {p1, v0, v1}, Lcom/google/android/exoplayer2/J;->a(J)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length p2, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p2, :cond_2

    aget-object v1, p1, v0

    invoke-static {v1}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/Renderer;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-interface {v1, v2, v3}, Lcom/google/android/exoplayer2/Renderer;->a(J)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->w()V

    return-void
.end method

.method private b(JJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_5

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->K:Z

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x1

    sub-long/2addr p1, v0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/O;->K:Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer2/O;->J:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v2, 0x0

    if-lez v1, :cond_2

    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/O$c;

    goto :goto_0

    :cond_2
    move-object v3, v2

    :goto_0
    if-eqz v3, :cond_4

    iget v4, v3, Lcom/google/android/exoplayer2/O$c;->b:I

    if-gt v4, v0, :cond_3

    if-ne v4, v0, :cond_4

    iget-wide v3, v3, Lcom/google/android/exoplayer2/O$c;->c:J

    cmp-long v3, v3, p1

    if-lez v3, :cond_4

    :cond_3
    add-int/lit8 v1, v1, -0x1

    if-lez v1, :cond_2

    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/O$c;

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_5

    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/O$c;

    goto :goto_1

    :cond_5
    move-object v3, v2

    :goto_1
    if-eqz v3, :cond_7

    iget-object v4, v3, Lcom/google/android/exoplayer2/O$c;->d:Ljava/lang/Object;

    if-eqz v4, :cond_7

    iget v4, v3, Lcom/google/android/exoplayer2/O$c;->b:I

    if-lt v4, v0, :cond_6

    if-ne v4, v0, :cond_7

    iget-wide v4, v3, Lcom/google/android/exoplayer2/O$c;->c:J

    cmp-long v4, v4, p1

    if-gtz v4, :cond_7

    :cond_6
    add-int/lit8 v1, v1, 0x1

    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_5

    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/O$c;

    goto :goto_1

    :cond_7
    :goto_2
    if-eqz v3, :cond_d

    iget-object v4, v3, Lcom/google/android/exoplayer2/O$c;->d:Ljava/lang/Object;

    if-eqz v4, :cond_d

    iget v4, v3, Lcom/google/android/exoplayer2/O$c;->b:I

    if-ne v4, v0, :cond_d

    iget-wide v4, v3, Lcom/google/android/exoplayer2/O$c;->c:J

    cmp-long v6, v4, p1

    if-lez v6, :cond_d

    cmp-long v4, v4, p3

    if-gtz v4, :cond_d

    :try_start_0
    iget-object v4, v3, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    invoke-direct {p0, v4}, Lcom/google/android/exoplayer2/O;->e(Lcom/google/android/exoplayer2/ia;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, v3, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/ia;->b()Z

    move-result v4

    if-nez v4, :cond_9

    iget-object v3, v3, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/ia;->j()Z

    move-result v3

    if-eqz v3, :cond_8

    goto :goto_3

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_9
    :goto_3
    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_4
    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_a

    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/O$c;

    goto :goto_2

    :cond_a
    move-object v3, v2

    goto :goto_2

    :catchall_0
    move-exception p1

    iget-object p2, v3, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/ia;->b()Z

    move-result p2

    if-nez p2, :cond_b

    iget-object p2, v3, Lcom/google/android/exoplayer2/O$c;->a:Lcom/google/android/exoplayer2/ia;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/ia;->j()Z

    move-result p2

    if-eqz p2, :cond_c

    :cond_b
    iget-object p2, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_c
    throw p1

    :cond_d
    iput v1, p0, Lcom/google/android/exoplayer2/O;->J:I

    :cond_e
    :goto_5
    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/Renderer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Renderer;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Renderer;->stop()V

    :cond_0
    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/ga;Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-interface {v0, v2, p2, v1, p1}, Lcom/google/android/exoplayer2/util/l;->a(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/sa;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v5, p1

    iget-object v9, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v10, v7, Lcom/google/android/exoplayer2/O;->H:Lcom/google/android/exoplayer2/O$g;

    iget-object v11, v7, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    iget v12, v7, Lcom/google/android/exoplayer2/O;->B:I

    iget-boolean v13, v7, Lcom/google/android/exoplayer2/O;->C:Z

    iget-object v14, v7, Lcom/google/android/exoplayer2/O;->j:Lcom/google/android/exoplayer2/sa$b;

    iget-object v15, v7, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    move-object/from16 v8, p1

    invoke-static/range {v8 .. v15}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/O$g;Lcom/google/android/exoplayer2/ba;IZLcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/O$f;

    move-result-object v0

    iget-object v8, v0, Lcom/google/android/exoplayer2/O$f;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v9, v0, Lcom/google/android/exoplayer2/O$f;->c:J

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/O$f;->d:Z

    iget-wide v11, v0, Lcom/google/android/exoplayer2/O$f;->b:J

    iget-object v2, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v2, v2, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v2, v8}, Lcom/google/android/exoplayer2/source/y$a;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    const/4 v13, 0x0

    if-eqz v2, :cond_1

    iget-object v2, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v14, v2, Lcom/google/android/exoplayer2/fa;->q:J

    cmp-long v2, v11, v14

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    move v14, v13

    goto :goto_1

    :cond_1
    :goto_0
    move v14, v3

    :goto_1
    :try_start_0
    iget-boolean v0, v0, Lcom/google/android/exoplayer2/O$f;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v0, v0, Lcom/google/android/exoplayer2/fa;->e:I

    if-eq v0, v3, :cond_2

    const/4 v0, 0x4

    invoke-direct {v7, v0}, Lcom/google/android/exoplayer2/O;->c(I)V

    :cond_2
    invoke-direct {v7, v13, v13, v13, v3}, Lcom/google/android/exoplayer2/O;->a(ZZZZ)V

    :cond_3
    if-nez v14, :cond_4

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    iget-wide v3, v7, Lcom/google/android/exoplayer2/O;->I:J

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->k()J

    move-result-wide v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-object/from16 v2, p1

    move-object v15, v5

    move-wide/from16 v5, v16

    :try_start_1
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;JJ)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {v7, v13}, Lcom/google/android/exoplayer2/O;->c(Z)V

    goto :goto_3

    :cond_4
    move-object v15, v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, v7, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_6

    iget-object v2, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v2, v2, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v2, v8}, Lcom/google/android/exoplayer2/source/y$a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, v7, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    iget-object v3, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    invoke-virtual {v2, v15, v3}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/aa;)Lcom/google/android/exoplayer2/aa;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    goto :goto_2

    :cond_6
    invoke-direct {v7, v8, v11, v12, v1}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JZ)J

    move-result-wide v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-wide v3, v0

    goto :goto_4

    :cond_7
    :goto_3
    move-wide v3, v11

    :goto_4
    if-nez v14, :cond_8

    iget-object v0, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/fa;->d:J

    cmp-long v0, v9, v0

    if-eqz v0, :cond_9

    :cond_8
    move-object/from16 v1, p0

    move-object v2, v8

    move-wide v5, v9

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/fa;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->B()V

    iget-object v0, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-direct {v7, v15, v0}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/sa;)V

    iget-object v0, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v0, v15}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/sa;)Lcom/google/android/exoplayer2/fa;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v1, 0x0

    iput-object v1, v7, Lcom/google/android/exoplayer2/O;->H:Lcom/google/android/exoplayer2/O$g;

    :cond_a
    invoke-direct {v7, v13}, Lcom/google/android/exoplayer2/O;->b(Z)V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_5

    :catchall_1
    move-exception v0

    move-object v15, v5

    :goto_5
    if-nez v14, :cond_b

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v1, v1, Lcom/google/android/exoplayer2/fa;->d:J

    cmp-long v1, v9, v1

    if-eqz v1, :cond_c

    :cond_b
    move-object/from16 v1, p0

    move-object v2, v8

    move-wide v3, v11

    move-wide v5, v9

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/fa;

    move-result-object v1

    iput-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->B()V

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-direct {v7, v15, v1}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/sa;)V

    iget-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v1, v15}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/sa;)Lcom/google/android/exoplayer2/fa;

    move-result-object v1

    iput-object v1, v7, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v1, 0x0

    iput-object v1, v7, Lcom/google/android/exoplayer2/O;->H:Lcom/google/android/exoplayer2/O$g;

    :cond_d
    invoke-direct {v7, v13}, Lcom/google/android/exoplayer2/O;->b(Z)V

    throw v0
.end method

.method private b(Z)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->d()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    :goto_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v2, v2, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer2/source/y$a;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v3, v1}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/fa;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    if-nez v0, :cond_2

    iget-wide v3, v1, Lcom/google/android/exoplayer2/fa;->q:J

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->a()J

    move-result-wide v3

    :goto_1
    iput-wide v3, v1, Lcom/google/android/exoplayer2/fa;->o:J

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->l()J

    move-result-wide v3

    iput-wide v3, v1, Lcom/google/android/exoplayer2/fa;->p:J

    if-nez v2, :cond_3

    if-eqz p1, :cond_4

    :cond_3
    if-eqz v0, :cond_4

    iget-boolean p1, v0, Lcom/google/android/exoplayer2/Z;->d:Z

    if-eqz p1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->f()Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object p1

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->g()Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;)V

    :cond_4
    return-void
.end method

.method private c(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v1, v0, Lcom/google/android/exoplayer2/fa;->e:I

    if-eq v1, p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/fa;->a(I)Lcom/google/android/exoplayer2/fa;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    :cond_0
    return-void
.end method

.method private c(JJ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/util/l;->c(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    add-long/2addr p1, p3

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/exoplayer2/util/l;->a(IJ)Z

    return-void
.end method

.method private c(Lcom/google/android/exoplayer2/ga;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/J;->a(Lcom/google/android/exoplayer2/ga;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/J;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object p1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/ga;Z)V

    return-void
.end method

.method private c(Lcom/google/android/exoplayer2/ia;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ia;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ia;->f()Lcom/google/android/exoplayer2/ia$b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ia;->h()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ia;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/exoplayer2/ia$b;->a(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/ia;->a(Z)V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/ia;->a(Z)V

    throw v1
.end method

.method private c(Lcom/google/android/exoplayer2/source/x;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/source/x;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    iget-wide v0, p0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {p1, v0, v1}, Lcom/google/android/exoplayer2/ba;->a(J)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->p()V

    return-void
.end method

.method private c(Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v3, v1, Lcom/google/android/exoplayer2/fa;->q:J

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JZZ)J

    move-result-wide v3

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v1, v1, Lcom/google/android/exoplayer2/fa;->q:J

    cmp-long v1, v3, v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v5, v1, Lcom/google/android/exoplayer2/fa;->d:J

    move-object v1, p0

    move-object v2, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/fa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/O$d;->c(I)V

    :cond_0
    return-void
.end method

.method private static c(Lcom/google/android/exoplayer2/Renderer;)Z
    .locals 0

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Renderer;->getState()I

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private d(Lcom/google/android/exoplayer2/ia;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ia;->e()J

    move-result-wide v0

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->e(Lcom/google/android/exoplayer2/ia;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/O$c;

    invoke-direct {v1, p1}, Lcom/google/android/exoplayer2/O$c;-><init>(Lcom/google/android/exoplayer2/ia;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/exoplayer2/O$c;

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/O$c;-><init>(Lcom/google/android/exoplayer2/ia;)V

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v4, v1, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget v5, p0, Lcom/google/android/exoplayer2/O;->B:I

    iget-boolean v6, p0, Lcom/google/android/exoplayer2/O;->C:Z

    iget-object v7, p0, Lcom/google/android/exoplayer2/O;->j:Lcom/google/android/exoplayer2/sa$b;

    iget-object v8, p0, Lcom/google/android/exoplayer2/O;->k:Lcom/google/android/exoplayer2/sa$a;

    move-object v2, v0

    move-object v3, v4

    invoke-static/range {v2 .. v8}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/O$c;Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/sa;IZLcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->o:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/ia;->a(Z)V

    :goto_0
    return-void
.end method

.method private d(Lcom/google/android/exoplayer2/source/x;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/source/x;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ba;->d()Lcom/google/android/exoplayer2/Z;

    move-result-object p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/J;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/ga;->b:F

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/exoplayer2/Z;->a(FLcom/google/android/exoplayer2/sa;)V

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/Z;->f()Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/Z;->g()Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iget-object v0, p1, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/aa;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/O;->b(J)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->j()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v2, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object p1, p1, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-wide v3, p1, Lcom/google/android/exoplayer2/aa;->b:J

    iget-wide v5, v0, Lcom/google/android/exoplayer2/fa;->d:J

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/fa;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->p()V

    return-void
.end method

.method private d(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->F:Z

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/O;->F:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v0, v0, Lcom/google/android/exoplayer2/fa;->e:I

    if-nez p1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/util/l;->b(I)Z

    goto :goto_1

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/fa;->b(Z)Lcom/google/android/exoplayer2/fa;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    :goto_1
    return-void
.end method

.method private e(Lcom/google/android/exoplayer2/ia;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ia;->c()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->i:Landroid/os/Looper;

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/ia;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget p1, p1, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 v0, 0x3

    const/4 v1, 0x2

    if-eq p1, v0, :cond_0

    if-ne p1, v1, :cond_2

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    invoke-interface {p1, v1}, Lcom/google/android/exoplayer2/util/l;->b(I)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/16 v1, 0xf

    invoke-interface {v0, v1, p1}, Lcom/google/android/exoplayer2/util/l;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_2
    :goto_0
    return-void
.end method

.method private e(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/O;->x:Z

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->B()V

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/O;->y:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->c(Z)V

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->b(Z)V

    :cond_0
    return-void
.end method

.method private f(Lcom/google/android/exoplayer2/ia;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ia;->c()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "TAG"

    const-string v1, "Trying to send message on a dead thread."

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/util/n;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/ia;->a(Z)V

    return-void

    :cond_0
    new-instance v1, Lcom/google/android/exoplayer2/w;

    invoke-direct {v1, p0, p1}, Lcom/google/android/exoplayer2/w;-><init>(Lcom/google/android/exoplayer2/O;Lcom/google/android/exoplayer2/ia;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private f(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/O;->C:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/ba;->a(Lcom/google/android/exoplayer2/sa;Z)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->c(Z)V

    :cond_0
    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->b(Z)V

    return-void
.end method

.method private g(Z)Z
    .locals 6

    iget v0, p0, Lcom/google/android/exoplayer2/O;->G:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->o()Z

    move-result p1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    return v0

    :cond_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-boolean p1, p1, Lcom/google/android/exoplayer2/fa;->g:Z

    const/4 v1, 0x1

    if-nez p1, :cond_2

    return v1

    :cond_2
    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ba;->d()Lcom/google/android/exoplayer2/Z;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/Z;->h()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object p1, p1, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-boolean p1, p1, Lcom/google/android/exoplayer2/aa;->h:Z

    if-eqz p1, :cond_3

    move p1, v1

    goto :goto_0

    :cond_3
    move p1, v0

    :goto_0
    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/google/android/exoplayer2/O;->e:Lcom/google/android/exoplayer2/U;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->l()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->n:Lcom/google/android/exoplayer2/J;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/J;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object v4

    iget v4, v4, Lcom/google/android/exoplayer2/ga;->b:F

    iget-boolean v5, p0, Lcom/google/android/exoplayer2/O;->z:Z

    invoke-interface {p1, v2, v3, v4, v5}, Lcom/google/android/exoplayer2/U;->a(JFZ)Z

    move-result p1

    if-eqz p1, :cond_5

    :cond_4
    move v0, v1

    :cond_5
    return v0
.end method

.method private i()V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;,
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer2/O;->p:Lcom/google/android/exoplayer2/util/e;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/util/e;->a()J

    move-result-wide v1

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->J()V

    iget-object v3, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v3, v3, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 v5, 0x1

    if-eq v3, v5, :cond_1d

    const/4 v6, 0x4

    if-ne v3, v6, :cond_0

    goto/16 :goto_e

    :cond_0
    iget-object v3, v0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v3

    const-wide/16 v7, 0xa

    if-nez v3, :cond_1

    invoke-direct {v0, v1, v2, v7, v8}, Lcom/google/android/exoplayer2/O;->c(JJ)V

    return-void

    :cond_1
    const-string v9, "doSomeWork"

    invoke-static {v9}, Lcom/google/android/exoplayer2/util/D;->a(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->K()V

    iget-boolean v9, v3, Lcom/google/android/exoplayer2/Z;->d:Z

    const-wide/16 v10, 0x3e8

    const/4 v12, 0x0

    if-eqz v9, :cond_a

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v13

    mul-long/2addr v13, v10

    iget-object v9, v3, Lcom/google/android/exoplayer2/Z;->a:Lcom/google/android/exoplayer2/source/x;

    iget-object v15, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v10, v15, Lcom/google/android/exoplayer2/fa;->q:J

    iget-wide v7, v0, Lcom/google/android/exoplayer2/O;->l:J

    sub-long/2addr v10, v7

    iget-boolean v7, v0, Lcom/google/android/exoplayer2/O;->m:Z

    invoke-interface {v9, v10, v11, v7}, Lcom/google/android/exoplayer2/source/x;->a(JZ)V

    move v8, v5

    move v9, v8

    move v7, v12

    :goto_0
    iget-object v10, v0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v11, v10

    if-ge v7, v11, :cond_b

    aget-object v10, v10, v7

    invoke-static {v10}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/Renderer;)Z

    move-result v11

    if-nez v11, :cond_2

    goto :goto_7

    :cond_2
    iget-wide v4, v0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-interface {v10, v4, v5, v13, v14}, Lcom/google/android/exoplayer2/Renderer;->a(JJ)V

    if-eqz v8, :cond_3

    invoke-interface {v10}, Lcom/google/android/exoplayer2/Renderer;->b()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v8, 0x1

    goto :goto_1

    :cond_3
    move v8, v12

    :goto_1
    iget-object v4, v3, Lcom/google/android/exoplayer2/Z;->c:[Lcom/google/android/exoplayer2/source/SampleStream;

    aget-object v4, v4, v7

    invoke-interface {v10}, Lcom/google/android/exoplayer2/Renderer;->j()Lcom/google/android/exoplayer2/source/SampleStream;

    move-result-object v5

    if-eq v4, v5, :cond_4

    const/4 v4, 0x1

    goto :goto_2

    :cond_4
    move v4, v12

    :goto_2
    if-nez v4, :cond_5

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-interface {v10}, Lcom/google/android/exoplayer2/Renderer;->g()Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    goto :goto_3

    :cond_5
    move v5, v12

    :goto_3
    if-nez v4, :cond_7

    if-nez v5, :cond_7

    invoke-interface {v10}, Lcom/google/android/exoplayer2/Renderer;->c()Z

    move-result v4

    if-nez v4, :cond_7

    invoke-interface {v10}, Lcom/google/android/exoplayer2/Renderer;->b()Z

    move-result v4

    if-eqz v4, :cond_6

    goto :goto_4

    :cond_6
    move v4, v12

    goto :goto_5

    :cond_7
    :goto_4
    const/4 v4, 0x1

    :goto_5
    if-eqz v9, :cond_8

    if-eqz v4, :cond_8

    const/4 v5, 0x1

    goto :goto_6

    :cond_8
    move v5, v12

    :goto_6
    if-nez v4, :cond_9

    invoke-interface {v10}, Lcom/google/android/exoplayer2/Renderer;->k()V

    :cond_9
    move v9, v5

    :goto_7
    add-int/lit8 v7, v7, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_a
    iget-object v4, v3, Lcom/google/android/exoplayer2/Z;->a:Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v4}, Lcom/google/android/exoplayer2/source/x;->b()V

    const/4 v8, 0x1

    const/4 v9, 0x1

    :cond_b
    iget-object v4, v3, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-wide v4, v4, Lcom/google/android/exoplayer2/aa;->e:J

    if-eqz v8, :cond_d

    iget-boolean v7, v3, Lcom/google/android/exoplayer2/Z;->d:Z

    if-eqz v7, :cond_d

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v7, v4, v7

    if-eqz v7, :cond_c

    iget-object v7, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v7, v7, Lcom/google/android/exoplayer2/fa;->q:J

    cmp-long v4, v4, v7

    if-gtz v4, :cond_d

    :cond_c
    const/4 v15, 0x1

    goto :goto_8

    :cond_d
    move v15, v12

    :goto_8
    if-eqz v15, :cond_e

    iget-boolean v4, v0, Lcom/google/android/exoplayer2/O;->y:Z

    if-eqz v4, :cond_e

    iput-boolean v12, v0, Lcom/google/android/exoplayer2/O;->y:Z

    iget-object v4, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v4, v4, Lcom/google/android/exoplayer2/fa;->l:I

    const/4 v5, 0x5

    invoke-direct {v0, v12, v4, v12, v5}, Lcom/google/android/exoplayer2/O;->a(ZIZI)V

    :cond_e
    const/4 v4, 0x3

    if-eqz v15, :cond_f

    iget-object v5, v3, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-boolean v5, v5, Lcom/google/android/exoplayer2/aa;->h:Z

    if-eqz v5, :cond_f

    invoke-direct {v0, v6}, Lcom/google/android/exoplayer2/O;->c(I)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->H()V

    goto :goto_9

    :cond_f
    iget-object v5, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v5, v5, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 v7, 0x2

    if-ne v5, v7, :cond_10

    invoke-direct {v0, v9}, Lcom/google/android/exoplayer2/O;->g(Z)Z

    move-result v5

    if-eqz v5, :cond_10

    invoke-direct {v0, v4}, Lcom/google/android/exoplayer2/O;->c(I)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->F()Z

    move-result v5

    if-eqz v5, :cond_13

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->G()V

    goto :goto_9

    :cond_10
    iget-object v5, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v5, v5, Lcom/google/android/exoplayer2/fa;->e:I

    if-ne v5, v4, :cond_13

    iget v5, v0, Lcom/google/android/exoplayer2/O;->G:I

    if-nez v5, :cond_11

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->o()Z

    move-result v5

    if-eqz v5, :cond_12

    goto :goto_9

    :cond_11
    if-nez v9, :cond_13

    :cond_12
    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->F()Z

    move-result v5

    iput-boolean v5, v0, Lcom/google/android/exoplayer2/O;->z:Z

    const/4 v5, 0x2

    invoke-direct {v0, v5}, Lcom/google/android/exoplayer2/O;->c(I)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->H()V

    goto :goto_a

    :cond_13
    :goto_9
    const/4 v5, 0x2

    :goto_a
    iget-object v7, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v7, v7, Lcom/google/android/exoplayer2/fa;->e:I

    if-ne v7, v5, :cond_17

    move v5, v12

    :goto_b
    iget-object v7, v0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v8, v7

    if-ge v5, v8, :cond_15

    aget-object v7, v7, v5

    invoke-static {v7}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/Renderer;)Z

    move-result v7

    if-eqz v7, :cond_14

    iget-object v7, v0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    aget-object v7, v7, v5

    invoke-interface {v7}, Lcom/google/android/exoplayer2/Renderer;->j()Lcom/google/android/exoplayer2/source/SampleStream;

    move-result-object v7

    iget-object v8, v3, Lcom/google/android/exoplayer2/Z;->c:[Lcom/google/android/exoplayer2/source/SampleStream;

    aget-object v8, v8, v5

    if-ne v7, v8, :cond_14

    iget-object v7, v0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    aget-object v7, v7, v5

    invoke-interface {v7}, Lcom/google/android/exoplayer2/Renderer;->k()V

    :cond_14
    add-int/lit8 v5, v5, 0x1

    goto :goto_b

    :cond_15
    iget-boolean v3, v0, Lcom/google/android/exoplayer2/O;->M:Z

    if-eqz v3, :cond_17

    iget-object v3, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-boolean v5, v3, Lcom/google/android/exoplayer2/fa;->g:Z

    if-nez v5, :cond_17

    iget-wide v7, v3, Lcom/google/android/exoplayer2/fa;->p:J

    const-wide/32 v9, 0x7a120

    cmp-long v3, v7, v9

    if-gez v3, :cond_17

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->n()Z

    move-result v3

    if-nez v3, :cond_16

    goto :goto_c

    :cond_16
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Playback stuck buffering and not loading"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_17
    :goto_c
    iget-boolean v3, v0, Lcom/google/android/exoplayer2/O;->F:Z

    iget-object v5, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-boolean v7, v5, Lcom/google/android/exoplayer2/fa;->n:Z

    if-eq v3, v7, :cond_18

    invoke-virtual {v5, v3}, Lcom/google/android/exoplayer2/fa;->b(Z)Lcom/google/android/exoplayer2/fa;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    :cond_18
    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/O;->F()Z

    move-result v3

    if-eqz v3, :cond_19

    iget-object v3, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v3, v3, Lcom/google/android/exoplayer2/fa;->e:I

    if-eq v3, v4, :cond_1a

    :cond_19
    iget-object v3, v0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget v3, v3, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1b

    :cond_1a
    const-wide/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/exoplayer2/O;->a(JJ)V

    goto :goto_d

    :cond_1b
    iget v4, v0, Lcom/google/android/exoplayer2/O;->G:I

    if-eqz v4, :cond_1c

    if-eq v3, v6, :cond_1c

    const-wide/16 v3, 0x3e8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/exoplayer2/O;->c(JJ)V

    goto :goto_d

    :cond_1c
    iget-object v1, v0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Lcom/google/android/exoplayer2/util/l;->c(I)V

    :goto_d
    iput-boolean v12, v0, Lcom/google/android/exoplayer2/O;->E:Z

    invoke-static {}, Lcom/google/android/exoplayer2/util/D;->a()V

    return-void

    :cond_1d
    :goto_e
    const/4 v2, 0x2

    iget-object v1, v0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    invoke-interface {v1, v2}, Lcom/google/android/exoplayer2/util/l;->c(I)V

    return-void
.end method

.method private j()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v0, v0

    new-array v0, v0, [Z

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/O;->a([Z)V

    return-void
.end method

.method private k()J
    .locals 9

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    return-wide v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->d()J

    move-result-wide v1

    iget-boolean v3, v0, Lcom/google/android/exoplayer2/Z;->d:Z

    if-nez v3, :cond_1

    return-wide v1

    :cond_1
    const/4 v3, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v5, v4

    if-ge v3, v5, :cond_5

    aget-object v4, v4, v3

    invoke-static {v4}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/Renderer;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    aget-object v4, v4, v3

    invoke-interface {v4}, Lcom/google/android/exoplayer2/Renderer;->j()Lcom/google/android/exoplayer2/source/SampleStream;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/exoplayer2/Z;->c:[Lcom/google/android/exoplayer2/source/SampleStream;

    aget-object v5, v5, v3

    if-eq v4, v5, :cond_2

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    aget-object v4, v4, v3

    invoke-interface {v4}, Lcom/google/android/exoplayer2/Renderer;->l()J

    move-result-wide v4

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v8, v4, v6

    if-nez v8, :cond_3

    return-wide v6

    :cond_3
    invoke-static {v4, v5, v1, v2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    :cond_4
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    return-wide v1
.end method

.method private l()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/fa;->o:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/O;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private m()Z
    .locals 6

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/Z;->d:Z

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    :cond_0
    move v1, v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v4, v3

    if-ge v1, v4, :cond_3

    aget-object v3, v3, v1

    iget-object v4, v0, Lcom/google/android/exoplayer2/Z;->c:[Lcom/google/android/exoplayer2/source/SampleStream;

    aget-object v4, v4, v1

    invoke-interface {v3}, Lcom/google/android/exoplayer2/Renderer;->j()Lcom/google/android/exoplayer2/source/SampleStream;

    move-result-object v5

    if-ne v5, v4, :cond_2

    if-eqz v4, :cond_1

    invoke-interface {v3}, Lcom/google/android/exoplayer2/Renderer;->g()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v2

    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method private n()Z
    .locals 6

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->d()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->c()J

    move-result-wide v2

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private o()Z
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-wide v1, v1, Lcom/google/android/exoplayer2/aa;->e:J

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/Z;->d:Z

    if-eqz v0, :cond_1

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v1, v3

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-wide v3, v0, Lcom/google/android/exoplayer2/fa;->q:J

    cmp-long v0, v3, v1

    if-ltz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->F()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private p()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->E()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/O;->A:Z

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->d()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/Z;->a(J)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->I()V

    return-void
.end method

.method private q()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/O$d;->a(Lcom/google/android/exoplayer2/fa;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    invoke-static {v0}, Lcom/google/android/exoplayer2/O$d;->a(Lcom/google/android/exoplayer2/O$d;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->q:Lcom/google/android/exoplayer2/O$e;

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/O$e;->a(Lcom/google/android/exoplayer2/O$d;)V

    new-instance v0, Lcom/google/android/exoplayer2/O$d;

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/O$d;-><init>(Lcom/google/android/exoplayer2/fa;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    :cond_0
    return-void
.end method

.method private r()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    iget-wide v1, p0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/ba;->a(J)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    iget-wide v1, p0, Lcom/google/android/exoplayer2/O;->I:J

    iget-object v3, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/ba;->a(JLcom/google/android/exoplayer2/fa;)Lcom/google/android/exoplayer2/aa;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    iget-object v5, p0, Lcom/google/android/exoplayer2/O;->b:[Lcom/google/android/exoplayer2/RendererCapabilities;

    iget-object v6, p0, Lcom/google/android/exoplayer2/O;->c:Lcom/google/android/exoplayer2/trackselection/o;

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->e:Lcom/google/android/exoplayer2/U;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/U;->e()Lcom/google/android/exoplayer2/upstream/e;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    iget-object v10, p0, Lcom/google/android/exoplayer2/O;->d:Lcom/google/android/exoplayer2/trackselection/p;

    move-object v9, v0

    invoke-virtual/range {v4 .. v10}, Lcom/google/android/exoplayer2/ba;->a([Lcom/google/android/exoplayer2/RendererCapabilities;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/upstream/e;Lcom/google/android/exoplayer2/da;Lcom/google/android/exoplayer2/aa;Lcom/google/android/exoplayer2/trackselection/p;)Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    iget-object v2, v1, Lcom/google/android/exoplayer2/Z;->a:Lcom/google/android/exoplayer2/source/x;

    iget-wide v3, v0, Lcom/google/android/exoplayer2/aa;->b:J

    invoke-interface {v2, p0, v3, v4}, Lcom/google/android/exoplayer2/source/x;->a(Lcom/google/android/exoplayer2/source/x$a;J)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-ne v0, v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->e()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/O;->b(J)V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/O;->b(Z)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->A:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->n()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/O;->A:Z

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->I()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->p()V

    :goto_0
    return-void
.end method

.method private s()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->D()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->q()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/ba;->a()Lcom/google/android/exoplayer2/Z;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v4, v2, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v5, v2, Lcom/google/android/exoplayer2/aa;->b:J

    iget-wide v7, v2, Lcom/google/android/exoplayer2/aa;->c:J

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/y$a;JJ)Lcom/google/android/exoplayer2/fa;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-boolean v1, v1, Lcom/google/android/exoplayer2/aa;->f:Z

    if-eqz v1, :cond_1

    move v1, v0

    goto :goto_1

    :cond_1
    const/4 v1, 0x3

    :goto_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer2/O$d;->c(I)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->B()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->K()V

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private t()V
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_9

    iget-boolean v1, p0, Lcom/google/android/exoplayer2/O;->y:Z

    if-eqz v1, :cond_1

    goto/16 :goto_2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->m()Z

    move-result v1

    if-nez v1, :cond_2

    return-void

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/exoplayer2/Z;->d:Z

    if-nez v1, :cond_3

    iget-wide v3, p0, Lcom/google/android/exoplayer2/O;->I:J

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->e()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-gez v1, :cond_3

    return-void

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->g()Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ba;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Z;->g()Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v3

    iget-boolean v4, v1, Lcom/google/android/exoplayer2/Z;->d:Z

    if-eqz v4, :cond_4

    iget-object v1, v1, Lcom/google/android/exoplayer2/Z;->a:Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/source/x;->d()J

    move-result-wide v4

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, v4, v6

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->C()V

    return-void

    :cond_4
    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v4, v4

    if-ge v1, v4, :cond_8

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/trackselection/p;->a(I)Z

    move-result v4

    invoke-virtual {v3, v1}, Lcom/google/android/exoplayer2/trackselection/p;->a(I)Z

    move-result v5

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    aget-object v4, v4, v1

    invoke-interface {v4}, Lcom/google/android/exoplayer2/Renderer;->m()Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->b:[Lcom/google/android/exoplayer2/RendererCapabilities;

    aget-object v4, v4, v1

    invoke-interface {v4}, Lcom/google/android/exoplayer2/RendererCapabilities;->f()I

    move-result v4

    const/4 v6, 0x6

    if-ne v4, v6, :cond_5

    const/4 v4, 0x1

    goto :goto_1

    :cond_5
    move v4, v2

    :goto_1
    iget-object v6, v0, Lcom/google/android/exoplayer2/trackselection/p;->b:[Lcom/google/android/exoplayer2/ka;

    aget-object v6, v6, v1

    iget-object v7, v3, Lcom/google/android/exoplayer2/trackselection/p;->b:[Lcom/google/android/exoplayer2/ka;

    aget-object v7, v7, v1

    if-eqz v5, :cond_6

    invoke-virtual {v7, v6}, Lcom/google/android/exoplayer2/ka;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    if-eqz v4, :cond_7

    :cond_6
    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    aget-object v4, v4, v1

    invoke-interface {v4}, Lcom/google/android/exoplayer2/Renderer;->h()V

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_8
    return-void

    :cond_9
    :goto_2
    iget-object v1, v0, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-boolean v1, v1, Lcom/google/android/exoplayer2/aa;->h:Z

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lcom/google/android/exoplayer2/O;->y:Z

    if-eqz v1, :cond_c

    :cond_a
    :goto_3
    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v3, v1

    if-ge v2, v3, :cond_c

    aget-object v1, v1, v2

    iget-object v3, v0, Lcom/google/android/exoplayer2/Z;->c:[Lcom/google/android/exoplayer2/source/SampleStream;

    aget-object v3, v3, v2

    if-eqz v3, :cond_b

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Renderer;->j()Lcom/google/android/exoplayer2/source/SampleStream;

    move-result-object v4

    if-ne v4, v3, :cond_b

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Renderer;->g()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Renderer;->h()V

    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_c
    return-void
.end method

.method private u()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v1

    if-eq v1, v0, :cond_1

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/Z;->g:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->j()V

    :cond_1
    :goto_0
    return-void
.end method

.method private v()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/da;->a()Lcom/google/android/exoplayer2/sa;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/sa;)V

    return-void
.end method

.method private w()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->g()Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/exoplayer2/trackselection/p;->c:Lcom/google/android/exoplayer2/trackselection/m;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/trackselection/m;->a()[Lcom/google/android/exoplayer2/trackselection/l;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    if-eqz v4, :cond_0

    invoke-interface {v4}, Lcom/google/android/exoplayer2/trackselection/l;->f()V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->b()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private x()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->v:Lcom/google/android/exoplayer2/O$d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/O$d;->a(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0, v1}, Lcom/google/android/exoplayer2/O;->a(ZZZZ)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->e:Lcom/google/android/exoplayer2/U;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/U;->c()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/O;->c(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->s:Lcom/google/android/exoplayer2/da;

    iget-object v2, p0, Lcom/google/android/exoplayer2/O;->f:Lcom/google/android/exoplayer2/upstream/g;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/upstream/g;->a()Lcom/google/android/exoplayer2/upstream/C;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/da;->a(Lcom/google/android/exoplayer2/upstream/C;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/util/l;->b(I)Z

    return-void
.end method

.method private y()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0, v1, v0}, Lcom/google/android/exoplayer2/O;->a(ZZZZ)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->e:Lcom/google/android/exoplayer2/U;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/U;->d()V

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/O;->c(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    monitor-enter p0

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/exoplayer2/O;->w:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private z()Z
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->g()Lcom/google/android/exoplayer2/trackselection/p;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :goto_0
    iget-object v5, p0, Lcom/google/android/exoplayer2/O;->a:[Lcom/google/android/exoplayer2/Renderer;

    array-length v6, v5

    const/4 v7, 0x1

    if-ge v3, v6, :cond_5

    aget-object v8, v5, v3

    invoke-static {v8}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/Renderer;)Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_2

    :cond_0
    invoke-interface {v8}, Lcom/google/android/exoplayer2/Renderer;->j()Lcom/google/android/exoplayer2/source/SampleStream;

    move-result-object v5

    iget-object v6, v0, Lcom/google/android/exoplayer2/Z;->c:[Lcom/google/android/exoplayer2/source/SampleStream;

    aget-object v6, v6, v3

    if-eq v5, v6, :cond_1

    move v5, v7

    goto :goto_1

    :cond_1
    move v5, v2

    :goto_1
    invoke-virtual {v1, v3}, Lcom/google/android/exoplayer2/trackselection/p;->a(I)Z

    move-result v6

    if-eqz v6, :cond_2

    if-nez v5, :cond_2

    goto :goto_2

    :cond_2
    invoke-interface {v8}, Lcom/google/android/exoplayer2/Renderer;->m()Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, v1, Lcom/google/android/exoplayer2/trackselection/p;->c:Lcom/google/android/exoplayer2/trackselection/m;

    invoke-virtual {v5, v3}, Lcom/google/android/exoplayer2/trackselection/m;->a(I)Lcom/google/android/exoplayer2/trackselection/l;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/trackselection/l;)[Lcom/google/android/exoplayer2/Format;

    move-result-object v9

    iget-object v5, v0, Lcom/google/android/exoplayer2/Z;->c:[Lcom/google/android/exoplayer2/source/SampleStream;

    aget-object v10, v5, v3

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->e()J

    move-result-wide v11

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Z;->d()J

    move-result-wide v13

    invoke-interface/range {v8 .. v14}, Lcom/google/android/exoplayer2/Renderer;->a([Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/source/SampleStream;JJ)V

    goto :goto_2

    :cond_3
    invoke-interface {v8}, Lcom/google/android/exoplayer2/Renderer;->b()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-direct {p0, v8}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/Renderer;)V

    goto :goto_2

    :cond_4
    move v4, v7

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    xor-int/lit8 v0, v4, 0x1

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/util/l;->b(I)Z

    return-void
.end method

.method public a(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/exoplayer2/util/l;->a(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/ga;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/ga;Z)V

    return-void
.end method

.method public declared-synchronized a(Lcom/google/android/exoplayer2/ia;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->w:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/16 v1, 0xe

    invoke-interface {v0, v1, p1}, Lcom/google/android/exoplayer2/util/l;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :goto_0
    :try_start_1
    const-string v0, "ExoPlayerImplInternal"

    const-string v1, "Ignoring messages sent after release."

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/util/n;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/ia;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public a(Lcom/google/android/exoplayer2/sa;IJ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    new-instance v1, Lcom/google/android/exoplayer2/O$g;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/O$g;-><init>(Lcom/google/android/exoplayer2/sa;IJ)V

    const/4 p1, 0x3

    invoke-interface {v0, p1, v1}, Lcom/google/android/exoplayer2/util/l;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/source/I;)V
    .locals 0

    check-cast p1, Lcom/google/android/exoplayer2/source/x;

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/source/x;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/x;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/16 v1, 0x8

    invoke-interface {v0, v1, p1}, Lcom/google/android/exoplayer2/util/l;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public a(Ljava/util/List;IJLcom/google/android/exoplayer2/source/J;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/da$c;",
            ">;IJ",
            "Lcom/google/android/exoplayer2/source/J;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    new-instance v8, Lcom/google/android/exoplayer2/O$a;

    const/4 v7, 0x0

    move-object v1, v8

    move-object v2, p1

    move-object v3, p5

    move v4, p2

    move-wide v5, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer2/O$a;-><init>(Ljava/util/List;Lcom/google/android/exoplayer2/source/J;IJLcom/google/android/exoplayer2/N;)V

    const/16 p1, 0x11

    invoke-interface {v0, p1, v8}, Lcom/google/android/exoplayer2/util/l;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public a(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/4 v1, 0x0

    const/16 v2, 0xc

    invoke-interface {v0, v2, p1, v1}, Lcom/google/android/exoplayer2/util/l;->a(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public a(ZI)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/4 v1, 0x1

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/exoplayer2/util/l;->a(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/16 v1, 0x16

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/util/l;->b(I)Z

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/ga;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/4 v1, 0x4

    invoke-interface {v0, v1, p1}, Lcom/google/android/exoplayer2/util/l;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public synthetic b(Lcom/google/android/exoplayer2/ia;)V
    .locals 2

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/ia;)V
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ExoPlaybackException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    const-string v0, "ExoPlayerImplInternal"

    const-string v1, "Unexpected error delivering message on external thread."

    invoke-static {v0, v1, p1}, Lcom/google/android/exoplayer2/util/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public b(Lcom/google/android/exoplayer2/source/x;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/16 v1, 0x9

    invoke-interface {v0, v1, p1}, Lcom/google/android/exoplayer2/util/l;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/O;->M:Z

    return-void
.end method

.method public d()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->i:Landroid/os/Looper;

    return-object v0
.end method

.method public synthetic e()Ljava/lang/Boolean;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->w:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()Ljava/lang/Boolean;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->w:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/util/l;->a(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public declared-synchronized h()Z
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->w:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->g:Lcom/google/android/exoplayer2/util/l;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/util/l;->b(I)Z

    iget-wide v0, p0, Lcom/google/android/exoplayer2/O;->L:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Lcom/google/android/exoplayer2/v;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/v;-><init>(Lcom/google/android/exoplayer2/O;)V

    iget-wide v1, p0, Lcom/google/android/exoplayer2/O;->L:J

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/exoplayer2/O;->a(Lb/a/a/a/h;J)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/exoplayer2/x;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/x;-><init>(Lcom/google/android/exoplayer2/O;)V

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/O;->a(Lb/a/a/a/h;)V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/O;->w:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :cond_2
    :goto_1
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 6

    const-string v0, "Playback error"

    const-string v1, "ExoPlayerImplInternal"

    const/4 v2, 0x0

    const/4 v3, 0x1

    :try_start_0
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    return v2

    :pswitch_0
    iget p1, p1, Landroid/os/Message;->arg1:I

    if-ne p1, v3, :cond_0

    move p1, v3

    goto :goto_0

    :cond_0
    move p1, v2

    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->d(Z)V

    goto/16 :goto_6

    :pswitch_1
    iget p1, p1, Landroid/os/Message;->arg1:I

    if-eqz p1, :cond_1

    move p1, v3

    goto :goto_1

    :cond_1
    move p1, v2

    :goto_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->e(Z)V

    goto/16 :goto_6

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->v()V

    goto/16 :goto_6

    :pswitch_3
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/source/J;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/source/J;)V

    goto/16 :goto_6

    :pswitch_4
    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/source/J;

    invoke-direct {p0, v4, v5, p1}, Lcom/google/android/exoplayer2/O;->a(IILcom/google/android/exoplayer2/source/J;)V

    goto/16 :goto_6

    :pswitch_5
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/O$b;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/O$b;)V

    goto/16 :goto_6

    :pswitch_6
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/exoplayer2/O$a;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v4, p1}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/O$a;I)V

    goto/16 :goto_6

    :pswitch_7
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/O$a;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/O$a;)V

    goto/16 :goto_6

    :pswitch_8
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/exoplayer2/ga;

    iget p1, p1, Landroid/os/Message;->arg1:I

    if-eqz p1, :cond_2

    move p1, v3

    goto :goto_2

    :cond_2
    move p1, v2

    :goto_2
    invoke-direct {p0, v4, p1}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/ga;Z)V

    goto/16 :goto_6

    :pswitch_9
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/ia;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->f(Lcom/google/android/exoplayer2/ia;)V

    goto/16 :goto_6

    :pswitch_a
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/ia;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->d(Lcom/google/android/exoplayer2/ia;)V

    goto/16 :goto_6

    :pswitch_b
    iget v4, p1, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_3

    move v4, v3

    goto :goto_3

    :cond_3
    move v4, v2

    :goto_3
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0, v4, p1}, Lcom/google/android/exoplayer2/O;->a(ZLjava/util/concurrent/atomic/AtomicBoolean;)V

    goto/16 :goto_6

    :pswitch_c
    iget p1, p1, Landroid/os/Message;->arg1:I

    if-eqz p1, :cond_4

    move p1, v3

    goto :goto_4

    :cond_4
    move p1, v2

    :goto_4
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->f(Z)V

    goto :goto_6

    :pswitch_d
    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->b(I)V

    goto :goto_6

    :pswitch_e
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->A()V

    goto :goto_6

    :pswitch_f
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/source/x;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/source/x;)V

    goto :goto_6

    :pswitch_10
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/source/x;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->d(Lcom/google/android/exoplayer2/source/x;)V

    goto :goto_6

    :pswitch_11
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->y()V

    return v3

    :pswitch_12
    invoke-direct {p0, v2, v3}, Lcom/google/android/exoplayer2/O;->a(ZZ)V

    goto :goto_6

    :pswitch_13
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/ma;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/ma;)V

    goto :goto_6

    :pswitch_14
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/ga;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->c(Lcom/google/android/exoplayer2/ga;)V

    goto :goto_6

    :pswitch_15
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/O$g;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/O$g;)V

    goto :goto_6

    :pswitch_16
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->i()V

    goto :goto_6

    :pswitch_17
    iget v4, p1, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_5

    move v4, v3

    goto :goto_5

    :cond_5
    move v4, v2

    :goto_5
    iget p1, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v4, p1, v3, v3}, Lcom/google/android/exoplayer2/O;->a(ZIZI)V

    goto :goto_6

    :pswitch_18
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->x()V

    :goto_6
    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->q()V
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ExoPlaybackException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_9

    :catch_0
    move-exception p1

    goto :goto_7

    :catch_1
    move-exception p1

    :goto_7
    instance-of v4, p1, Ljava/lang/OutOfMemoryError;

    if-eqz v4, :cond_6

    check-cast p1, Ljava/lang/OutOfMemoryError;

    invoke-static {p1}, Lcom/google/android/exoplayer2/ExoPlaybackException;->a(Ljava/lang/OutOfMemoryError;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object p1

    goto :goto_8

    :cond_6
    check-cast p1, Ljava/lang/RuntimeException;

    invoke-static {p1}, Lcom/google/android/exoplayer2/ExoPlaybackException;->a(Ljava/lang/RuntimeException;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object p1

    :goto_8
    invoke-static {v1, v0, p1}, Lcom/google/android/exoplayer2/util/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v3, v2}, Lcom/google/android/exoplayer2/O;->a(ZZ)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/ExoPlaybackException;)Lcom/google/android/exoplayer2/fa;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->q()V

    goto :goto_9

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/google/android/exoplayer2/ExoPlaybackException;->a(Ljava/io/IOException;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object p1

    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/ba;->e()Lcom/google/android/exoplayer2/Z;

    move-result-object v4

    if-eqz v4, :cond_7

    iget-object v4, v4, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v4, v4, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {p1, v4}, Lcom/google/android/exoplayer2/ExoPlaybackException;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object p1

    :cond_7
    invoke-static {v1, v0, p1}, Lcom/google/android/exoplayer2/util/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v2, v2}, Lcom/google/android/exoplayer2/O;->a(ZZ)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/ExoPlaybackException;)Lcom/google/android/exoplayer2/fa;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->q()V

    goto :goto_9

    :catch_3
    move-exception p1

    iget v4, p1, Lcom/google/android/exoplayer2/ExoPlaybackException;->a:I

    if-ne v4, v3, :cond_8

    iget-object v4, p0, Lcom/google/android/exoplayer2/O;->r:Lcom/google/android/exoplayer2/ba;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/ba;->f()Lcom/google/android/exoplayer2/Z;

    move-result-object v4

    if-eqz v4, :cond_8

    iget-object v4, v4, Lcom/google/android/exoplayer2/Z;->f:Lcom/google/android/exoplayer2/aa;

    iget-object v4, v4, Lcom/google/android/exoplayer2/aa;->a:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {p1, v4}, Lcom/google/android/exoplayer2/ExoPlaybackException;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object p1

    :cond_8
    invoke-static {v1, v0, p1}, Lcom/google/android/exoplayer2/util/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v3, v2}, Lcom/google/android/exoplayer2/O;->a(ZZ)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/ExoPlaybackException;)Lcom/google/android/exoplayer2/fa;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/O;->u:Lcom/google/android/exoplayer2/fa;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/O;->q()V

    :goto_9
    return v3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
