.class final Lcom/google/android/exoplayer2/source/G$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/source/G;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field public final a:J

.field public final b:J

.field public c:Z

.field public d:Lcom/google/android/exoplayer2/upstream/d;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/google/android/exoplayer2/source/G$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(JI)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/G$a;->a:J

    int-to-long v0, p3

    add-long/2addr p1, v0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/G$a;->b:J

    return-void
.end method


# virtual methods
.method public a(J)I
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/G$a;->a:J

    sub-long/2addr p1, v0

    long-to-int p1, p1

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/G$a;->d:Lcom/google/android/exoplayer2/upstream/d;

    iget p2, p2, Lcom/google/android/exoplayer2/upstream/d;->b:I

    add-int/2addr p1, p2

    return p1
.end method

.method public a()Lcom/google/android/exoplayer2/source/G$a;
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/G$a;->d:Lcom/google/android/exoplayer2/upstream/d;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/G$a;->e:Lcom/google/android/exoplayer2/source/G$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/G$a;->e:Lcom/google/android/exoplayer2/source/G$a;

    return-object v1
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/d;Lcom/google/android/exoplayer2/source/G$a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/G$a;->d:Lcom/google/android/exoplayer2/upstream/d;

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/G$a;->e:Lcom/google/android/exoplayer2/source/G$a;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/G$a;->c:Z

    return-void
.end method
