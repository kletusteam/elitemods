.class final Lcom/google/android/exoplayer2/source/D;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/source/x;
.implements Lcom/google/android/exoplayer2/extractor/k;
.implements Lcom/google/android/exoplayer2/upstream/z$a;
.implements Lcom/google/android/exoplayer2/upstream/z$e;
.implements Lcom/google/android/exoplayer2/source/H$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/D$d;,
        Lcom/google/android/exoplayer2/source/D$e;,
        Lcom/google/android/exoplayer2/source/D$a;,
        Lcom/google/android/exoplayer2/source/D$c;,
        Lcom/google/android/exoplayer2/source/D$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/exoplayer2/source/x;",
        "Lcom/google/android/exoplayer2/extractor/k;",
        "Lcom/google/android/exoplayer2/upstream/z$a<",
        "Lcom/google/android/exoplayer2/source/D$a;",
        ">;",
        "Lcom/google/android/exoplayer2/upstream/z$e;",
        "Lcom/google/android/exoplayer2/source/H$b;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/google/android/exoplayer2/Format;


# instance fields
.field private A:Lcom/google/android/exoplayer2/extractor/u;

.field private B:J

.field private C:Z

.field private D:I

.field private E:Z

.field private F:Z

.field private G:I

.field private H:J

.field private I:J

.field private J:J

.field private K:Z

.field private L:I

.field private M:Z

.field private N:Z

.field private final c:Landroid/net/Uri;

.field private final d:Lcom/google/android/exoplayer2/upstream/l;

.field private final e:Lcom/google/android/exoplayer2/drm/e;

.field private final f:Lcom/google/android/exoplayer2/upstream/x;

.field private final g:Lcom/google/android/exoplayer2/source/A$a;

.field private final h:Lcom/google/android/exoplayer2/drm/c$a;

.field private final i:Lcom/google/android/exoplayer2/source/D$b;

.field private final j:Lcom/google/android/exoplayer2/upstream/e;

.field private final k:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final l:J

.field private final m:Lcom/google/android/exoplayer2/upstream/z;

.field private final n:Lcom/google/android/exoplayer2/source/C;

.field private final o:Lcom/google/android/exoplayer2/util/h;

.field private final p:Ljava/lang/Runnable;

.field private final q:Ljava/lang/Runnable;

.field private final r:Landroid/os/Handler;

.field private s:Lcom/google/android/exoplayer2/source/x$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private u:[Lcom/google/android/exoplayer2/source/H;

.field private v:[Lcom/google/android/exoplayer2/source/D$d;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Lcom/google/android/exoplayer2/source/D$e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/google/android/exoplayer2/source/D;->p()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/D;->a:Ljava/util/Map;

    new-instance v0, Lcom/google/android/exoplayer2/Format$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/Format$a;-><init>()V

    const-string v1, "icy"

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/Format$a;->b(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    const-string v1, "application/x-icy"

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/Format$a;->e(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Format$a;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/D;->b:Lcom/google/android/exoplayer2/Format;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/l;Lcom/google/android/exoplayer2/extractor/m;Lcom/google/android/exoplayer2/drm/e;Lcom/google/android/exoplayer2/drm/c$a;Lcom/google/android/exoplayer2/upstream/x;Lcom/google/android/exoplayer2/source/A$a;Lcom/google/android/exoplayer2/source/D$b;Lcom/google/android/exoplayer2/upstream/e;Ljava/lang/String;I)V
    .locals 0
    .param p10    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D;->c:Landroid/net/Uri;

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/D;->d:Lcom/google/android/exoplayer2/upstream/l;

    iput-object p4, p0, Lcom/google/android/exoplayer2/source/D;->e:Lcom/google/android/exoplayer2/drm/e;

    iput-object p5, p0, Lcom/google/android/exoplayer2/source/D;->h:Lcom/google/android/exoplayer2/drm/c$a;

    iput-object p6, p0, Lcom/google/android/exoplayer2/source/D;->f:Lcom/google/android/exoplayer2/upstream/x;

    iput-object p7, p0, Lcom/google/android/exoplayer2/source/D;->g:Lcom/google/android/exoplayer2/source/A$a;

    iput-object p8, p0, Lcom/google/android/exoplayer2/source/D;->i:Lcom/google/android/exoplayer2/source/D$b;

    iput-object p9, p0, Lcom/google/android/exoplayer2/source/D;->j:Lcom/google/android/exoplayer2/upstream/e;

    iput-object p10, p0, Lcom/google/android/exoplayer2/source/D;->k:Ljava/lang/String;

    int-to-long p1, p11

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/D;->l:J

    new-instance p1, Lcom/google/android/exoplayer2/upstream/z;

    const-string p2, "Loader:ProgressiveMediaPeriod"

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/upstream/z;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    new-instance p1, Lcom/google/android/exoplayer2/source/k;

    invoke-direct {p1, p3}, Lcom/google/android/exoplayer2/source/k;-><init>(Lcom/google/android/exoplayer2/extractor/m;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D;->n:Lcom/google/android/exoplayer2/source/C;

    new-instance p1, Lcom/google/android/exoplayer2/util/h;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/util/h;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D;->o:Lcom/google/android/exoplayer2/util/h;

    new-instance p1, Lcom/google/android/exoplayer2/source/h;

    invoke-direct {p1, p0}, Lcom/google/android/exoplayer2/source/h;-><init>(Lcom/google/android/exoplayer2/source/D;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D;->p:Ljava/lang/Runnable;

    new-instance p1, Lcom/google/android/exoplayer2/source/g;

    invoke-direct {p1, p0}, Lcom/google/android/exoplayer2/source/g;-><init>(Lcom/google/android/exoplayer2/source/D;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D;->q:Ljava/lang/Runnable;

    invoke-static {}, Lcom/google/android/exoplayer2/util/E;->a()Landroid/os/Handler;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D;->r:Landroid/os/Handler;

    const/4 p1, 0x0

    new-array p2, p1, [Lcom/google/android/exoplayer2/source/D$d;

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/D;->v:[Lcom/google/android/exoplayer2/source/D$d;

    new-array p1, p1, [Lcom/google/android/exoplayer2/source/H;

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/D;->J:J

    const-wide/16 p3, -0x1

    iput-wide p3, p0, Lcom/google/android/exoplayer2/source/D;->H:J

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/D;->B:J

    const/4 p1, 0x1

    iput p1, p0, Lcom/google/android/exoplayer2/source/D;->D:I

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/source/D$d;)Lcom/google/android/exoplayer2/extractor/TrackOutput;
    .locals 6

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/D;->v:[Lcom/google/android/exoplayer2/source/D$d;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/source/D$d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    aget-object p1, p1, v1

    return-object p1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/exoplayer2/source/H;

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/D;->j:Lcom/google/android/exoplayer2/upstream/e;

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/D;->r:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/D;->e:Lcom/google/android/exoplayer2/drm/e;

    iget-object v5, p0, Lcom/google/android/exoplayer2/source/D;->h:Lcom/google/android/exoplayer2/drm/c$a;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/exoplayer2/source/H;-><init>(Lcom/google/android/exoplayer2/upstream/e;Landroid/os/Looper;Lcom/google/android/exoplayer2/drm/e;Lcom/google/android/exoplayer2/drm/c$a;)V

    invoke-virtual {v1, p0}, Lcom/google/android/exoplayer2/source/H;->a(Lcom/google/android/exoplayer2/source/H$b;)V

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/D;->v:[Lcom/google/android/exoplayer2/source/D$d;

    add-int/lit8 v3, v0, 0x1

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/exoplayer2/source/D$d;

    aput-object p1, v2, v0

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/E;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    check-cast v2, [Lcom/google/android/exoplayer2/source/D$d;

    iput-object v2, p0, Lcom/google/android/exoplayer2/source/D;->v:[Lcom/google/android/exoplayer2/source/D$d;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    invoke-static {p1, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/google/android/exoplayer2/source/H;

    aput-object v1, p1, v0

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/E;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    check-cast p1, [Lcom/google/android/exoplayer2/source/H;

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    return-object v1
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/D;Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;)Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D;->t:Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/D;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/source/D;->q:Ljava/lang/Runnable;

    return-object p0
.end method

.method private a(Lcom/google/android/exoplayer2/source/D$a;)V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/D;->H:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/exoplayer2/source/D$a;->e(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/D;->H:J

    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/source/D$a;I)Z
    .locals 6

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/D;->H:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    const/4 v1, 0x1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->A:Lcom/google/android/exoplayer2/extractor/u;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/u;->c()J

    move-result-wide v2

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-boolean p2, p0, Lcom/google/android/exoplayer2/source/D;->x:Z

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->v()Z

    move-result p2

    if-nez p2, :cond_1

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/source/D;->K:Z

    return v0

    :cond_1
    iget-boolean p2, p0, Lcom/google/android/exoplayer2/source/D;->x:Z

    iput-boolean p2, p0, Lcom/google/android/exoplayer2/source/D;->F:Z

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/exoplayer2/source/D;->I:J

    iput v0, p0, Lcom/google/android/exoplayer2/source/D;->L:I

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v4, p2

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, p2, v0

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/source/H;->k()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-static {p1, v2, v3, v2, v3}, Lcom/google/android/exoplayer2/source/D$a;->a(Lcom/google/android/exoplayer2/source/D$a;JJ)V

    return v1

    :cond_3
    :goto_1
    iput p2, p0, Lcom/google/android/exoplayer2/source/D;->L:I

    return v1
.end method

.method private a([ZJ)Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v0, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_2

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    aget-object v3, v3, v2

    invoke-virtual {v3, p2, p3, v1}, Lcom/google/android/exoplayer2/source/H;->b(JZ)Z

    move-result v3

    if-nez v3, :cond_1

    aget-boolean v3, p1, v2

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/exoplayer2/source/D;->y:Z

    if-nez v3, :cond_1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/source/D;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/source/D;->r:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/source/D;)J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->r()J

    move-result-wide v0

    return-wide v0
.end method

.method private c(I)V
    .locals 10

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->o()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->z:Lcom/google/android/exoplayer2/source/D$e;

    iget-object v1, v0, Lcom/google/android/exoplayer2/source/D$e;->d:[Z

    aget-boolean v2, v1, p1

    if-nez v2, :cond_0

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/D$e;->a:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->a(I)Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/source/TrackGroup;->a(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v5

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/D;->g:Lcom/google/android/exoplayer2/source/A$a;

    iget-object v0, v5, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/q;->d(Ljava/lang/String;)I

    move-result v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-wide v8, p0, Lcom/google/android/exoplayer2/source/D;->I:J

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/exoplayer2/source/A$a;->a(ILcom/google/android/exoplayer2/Format;ILjava/lang/Object;J)V

    const/4 v0, 0x1

    aput-boolean v0, v1, p1

    :cond_0
    return-void
.end method

.method private c(Lcom/google/android/exoplayer2/extractor/u;)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->t:Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    if-nez v0, :cond_0

    move-object v0, p1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/extractor/u$b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/u$b;-><init>(J)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/exoplayer2/source/D;->A:Lcom/google/android/exoplayer2/extractor/u;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/u;->c()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/exoplayer2/source/D;->B:J

    iget-wide v3, p0, Lcom/google/android/exoplayer2/source/D;->H:J

    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    const/4 v3, 0x1

    if-nez v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/u;->c()J

    move-result-wide v4

    cmp-long v0, v4, v1

    if-nez v0, :cond_1

    move v0, v3

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->C:Z

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->C:Z

    if-eqz v0, :cond_2

    const/4 v3, 0x7

    :cond_2
    iput v3, p0, Lcom/google/android/exoplayer2/source/D;->D:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->i:Lcom/google/android/exoplayer2/source/D$b;

    iget-wide v1, p0, Lcom/google/android/exoplayer2/source/D;->B:J

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/u;->b()Z

    move-result p1

    iget-boolean v3, p0, Lcom/google/android/exoplayer2/source/D;->C:Z

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/google/android/exoplayer2/source/D$b;->a(JZZ)V

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/source/D;->x:Z

    if-nez p1, :cond_3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->t()V

    :cond_3
    return-void
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/source/D;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/source/D;->k:Ljava/lang/String;

    return-object p0
.end method

.method private d(I)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->o()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->z:Lcom/google/android/exoplayer2/source/D$e;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/D$e;->b:[Z

    iget-boolean v1, p0, Lcom/google/android/exoplayer2/source/D;->K:Z

    if-eqz v1, :cond_2

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    aget-object p1, v0, p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/source/H;->a(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_1

    :cond_0
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/google/android/exoplayer2/source/D;->J:J

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->K:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/D;->F:Z

    iput-wide v1, p0, Lcom/google/android/exoplayer2/source/D;->I:J

    iput v0, p0, Lcom/google/android/exoplayer2/source/D;->L:I

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v1, p1

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/source/H;->k()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->s:Lcom/google/android/exoplayer2/source/x$a;

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/source/x$a;

    invoke-interface {p1, p0}, Lcom/google/android/exoplayer2/source/I$a;->a(Lcom/google/android/exoplayer2/source/I;)V

    :cond_2
    :goto_1
    return-void
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/source/D;)Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/source/D;->t:Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    return-object p0
.end method

.method static synthetic f(Lcom/google/android/exoplayer2/source/D;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/D;->l:J

    return-wide v0
.end method

.method public static synthetic g(Lcom/google/android/exoplayer2/source/D;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->t()V

    return-void
.end method

.method static synthetic i()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/source/D;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic j()Lcom/google/android/exoplayer2/Format;
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/source/D;->b:Lcom/google/android/exoplayer2/Format;

    return-object v0
.end method

.method private o()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->x:Z

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->z:Lcom/google/android/exoplayer2/source/D$e;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->A:Lcom/google/android/exoplayer2/extractor/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static p()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "Icy-MetaData"

    const-string v2, "1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private q()I
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v4, v0, v2

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/source/H;->f()I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return v3
.end method

.method private r()J
    .locals 7

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v1, v0

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_0

    aget-object v5, v0, v4

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/source/H;->b()J

    move-result-wide v5

    invoke-static {v2, v3, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    return-wide v2
.end method

.method private s()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/D;->J:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private t()V
    .locals 11

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->N:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->x:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->w:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->A:Lcom/google/android/exoplayer2/extractor/u;

    if-nez v0, :cond_0

    goto/16 :goto_5

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/source/H;->e()Lcom/google/android/exoplayer2/Format;

    move-result-object v4

    if-nez v4, :cond_1

    return-void

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->o:Lcom/google/android/exoplayer2/util/h;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/h;->b()Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v0, v0

    new-array v1, v0, [Lcom/google/android/exoplayer2/source/TrackGroup;

    new-array v3, v0, [Z

    move v4, v2

    :goto_1
    const/4 v5, 0x1

    if-ge v4, v0, :cond_9

    iget-object v6, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    aget-object v6, v6, v4

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/source/H;->e()Lcom/google/android/exoplayer2/Format;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v6, Lcom/google/android/exoplayer2/Format;

    iget-object v7, v6, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/exoplayer2/util/q;->f(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    invoke-static {v7}, Lcom/google/android/exoplayer2/util/q;->h(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    goto :goto_2

    :cond_3
    move v7, v2

    goto :goto_3

    :cond_4
    :goto_2
    move v7, v5

    :goto_3
    aput-boolean v7, v3, v4

    iget-boolean v9, p0, Lcom/google/android/exoplayer2/source/D;->y:Z

    or-int/2addr v7, v9

    iput-boolean v7, p0, Lcom/google/android/exoplayer2/source/D;->y:Z

    iget-object v7, p0, Lcom/google/android/exoplayer2/source/D;->t:Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    if-eqz v7, :cond_8

    if-nez v8, :cond_5

    iget-object v9, p0, Lcom/google/android/exoplayer2/source/D;->v:[Lcom/google/android/exoplayer2/source/D$d;

    aget-object v9, v9, v4

    iget-boolean v9, v9, Lcom/google/android/exoplayer2/source/D$d;->b:Z

    if-eqz v9, :cond_7

    :cond_5
    iget-object v9, v6, Lcom/google/android/exoplayer2/Format;->j:Lcom/google/android/exoplayer2/metadata/Metadata;

    if-nez v9, :cond_6

    new-instance v9, Lcom/google/android/exoplayer2/metadata/Metadata;

    new-array v10, v5, [Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    aput-object v7, v10, v2

    invoke-direct {v9, v10}, Lcom/google/android/exoplayer2/metadata/Metadata;-><init>([Lcom/google/android/exoplayer2/metadata/Metadata$Entry;)V

    goto :goto_4

    :cond_6
    new-array v10, v5, [Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    aput-object v7, v10, v2

    invoke-virtual {v9, v10}, Lcom/google/android/exoplayer2/metadata/Metadata;->a([Lcom/google/android/exoplayer2/metadata/Metadata$Entry;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object v9

    :goto_4
    invoke-virtual {v6}, Lcom/google/android/exoplayer2/Format;->c()Lcom/google/android/exoplayer2/Format$a;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/google/android/exoplayer2/Format$a;->a(Lcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/Format$a;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object v6

    :cond_7
    if-eqz v8, :cond_8

    iget v8, v6, Lcom/google/android/exoplayer2/Format;->f:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_8

    iget v8, v6, Lcom/google/android/exoplayer2/Format;->g:I

    if-ne v8, v9, :cond_8

    iget v8, v7, Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;->a:I

    if-eq v8, v9, :cond_8

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/Format;->c()Lcom/google/android/exoplayer2/Format$a;

    move-result-object v6

    iget v7, v7, Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;->a:I

    invoke-virtual {v6, v7}, Lcom/google/android/exoplayer2/Format$a;->b(I)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/Format$a;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object v6

    :cond_8
    iget-object v7, p0, Lcom/google/android/exoplayer2/source/D;->e:Lcom/google/android/exoplayer2/drm/e;

    invoke-interface {v7, v6}, Lcom/google/android/exoplayer2/drm/e;->a(Lcom/google/android/exoplayer2/Format;)Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/exoplayer2/Format;->a(Ljava/lang/Class;)Lcom/google/android/exoplayer2/Format;

    move-result-object v6

    new-instance v7, Lcom/google/android/exoplayer2/source/TrackGroup;

    new-array v5, v5, [Lcom/google/android/exoplayer2/Format;

    aput-object v6, v5, v2

    invoke-direct {v7, v5}, Lcom/google/android/exoplayer2/source/TrackGroup;-><init>([Lcom/google/android/exoplayer2/Format;)V

    aput-object v7, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_9
    new-instance v0, Lcom/google/android/exoplayer2/source/D$e;

    new-instance v2, Lcom/google/android/exoplayer2/source/TrackGroupArray;

    invoke-direct {v2, v1}, Lcom/google/android/exoplayer2/source/TrackGroupArray;-><init>([Lcom/google/android/exoplayer2/source/TrackGroup;)V

    invoke-direct {v0, v2, v3}, Lcom/google/android/exoplayer2/source/D$e;-><init>(Lcom/google/android/exoplayer2/source/TrackGroupArray;[Z)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/D;->z:Lcom/google/android/exoplayer2/source/D$e;

    iput-boolean v5, p0, Lcom/google/android/exoplayer2/source/D;->x:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->s:Lcom/google/android/exoplayer2/source/x$a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/x$a;

    invoke-interface {v0, p0}, Lcom/google/android/exoplayer2/source/x$a;->a(Lcom/google/android/exoplayer2/source/x;)V

    :cond_a
    :goto_5
    return-void
.end method

.method private u()V
    .locals 26

    move-object/from16 v7, p0

    new-instance v8, Lcom/google/android/exoplayer2/source/D$a;

    iget-object v2, v7, Lcom/google/android/exoplayer2/source/D;->c:Landroid/net/Uri;

    iget-object v3, v7, Lcom/google/android/exoplayer2/source/D;->d:Lcom/google/android/exoplayer2/upstream/l;

    iget-object v4, v7, Lcom/google/android/exoplayer2/source/D;->n:Lcom/google/android/exoplayer2/source/C;

    iget-object v6, v7, Lcom/google/android/exoplayer2/source/D;->o:Lcom/google/android/exoplayer2/util/h;

    move-object v0, v8

    move-object/from16 v1, p0

    move-object/from16 v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/source/D$a;-><init>(Lcom/google/android/exoplayer2/source/D;Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/l;Lcom/google/android/exoplayer2/source/C;Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/util/h;)V

    iget-boolean v0, v7, Lcom/google/android/exoplayer2/source/D;->x:Z

    if-eqz v0, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/source/D;->s()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-wide v0, v7, Lcom/google/android/exoplayer2/source/D;->B:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    iget-wide v4, v7, Lcom/google/android/exoplayer2/source/D;->J:J

    cmp-long v0, v4, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, v7, Lcom/google/android/exoplayer2/source/D;->M:Z

    iput-wide v2, v7, Lcom/google/android/exoplayer2/source/D;->J:J

    return-void

    :cond_0
    iget-object v0, v7, Lcom/google/android/exoplayer2/source/D;->A:Lcom/google/android/exoplayer2/extractor/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/extractor/u;

    iget-wide v4, v7, Lcom/google/android/exoplayer2/source/D;->J:J

    invoke-interface {v0, v4, v5}, Lcom/google/android/exoplayer2/extractor/u;->b(J)Lcom/google/android/exoplayer2/extractor/u$a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/u$a;->a:Lcom/google/android/exoplayer2/extractor/v;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/extractor/v;->c:J

    iget-wide v4, v7, Lcom/google/android/exoplayer2/source/D;->J:J

    invoke-static {v8, v0, v1, v4, v5}, Lcom/google/android/exoplayer2/source/D$a;->a(Lcom/google/android/exoplayer2/source/D$a;JJ)V

    iget-object v0, v7, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v1, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_1

    aget-object v5, v0, v4

    iget-wide v9, v7, Lcom/google/android/exoplayer2/source/D;->J:J

    invoke-virtual {v5, v9, v10}, Lcom/google/android/exoplayer2/source/H;->a(J)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    iput-wide v2, v7, Lcom/google/android/exoplayer2/source/D;->J:J

    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/source/D;->q()I

    move-result v0

    iput v0, v7, Lcom/google/android/exoplayer2/source/D;->L:I

    iget-object v0, v7, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    iget-object v1, v7, Lcom/google/android/exoplayer2/source/D;->f:Lcom/google/android/exoplayer2/upstream/x;

    iget v2, v7, Lcom/google/android/exoplayer2/source/D;->D:I

    invoke-interface {v1, v2}, Lcom/google/android/exoplayer2/upstream/x;->a(I)I

    move-result v1

    invoke-virtual {v0, v8, v7, v1}, Lcom/google/android/exoplayer2/upstream/z;->a(Lcom/google/android/exoplayer2/upstream/z$d;Lcom/google/android/exoplayer2/upstream/z$a;I)J

    move-result-wide v13

    invoke-static {v8}, Lcom/google/android/exoplayer2/source/D$a;->c(Lcom/google/android/exoplayer2/source/D$a;)Lcom/google/android/exoplayer2/upstream/DataSpec;

    move-result-object v12

    iget-object v15, v7, Lcom/google/android/exoplayer2/source/D;->g:Lcom/google/android/exoplayer2/source/A$a;

    new-instance v16, Lcom/google/android/exoplayer2/source/t;

    invoke-static {v8}, Lcom/google/android/exoplayer2/source/D$a;->b(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v10

    move-object/from16 v9, v16

    invoke-direct/range {v9 .. v14}, Lcom/google/android/exoplayer2/source/t;-><init>(JLcom/google/android/exoplayer2/upstream/DataSpec;J)V

    const/16 v17, 0x1

    const/16 v18, -0x1

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-static {v8}, Lcom/google/android/exoplayer2/source/D$a;->d(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v22

    iget-wide v0, v7, Lcom/google/android/exoplayer2/source/D;->B:J

    move-wide/from16 v24, v0

    invoke-virtual/range {v15 .. v25}, Lcom/google/android/exoplayer2/source/A$a;->c(Lcom/google/android/exoplayer2/source/t;IILcom/google/android/exoplayer2/Format;ILjava/lang/Object;JJ)V

    return-void
.end method

.method private v()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->F:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method a(IJ)I
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {v0, p2}, Lcom/google/android/exoplayer2/source/H;->a(I)V

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/exoplayer2/source/H;->a(JZ)I

    move-result p2

    goto/32 :goto_0

    nop

    :goto_2
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/source/D;->M:Z

    goto/32 :goto_1

    nop

    :goto_3
    aget-object v0, v0, p1

    goto/32 :goto_2

    nop

    :goto_4
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/D;->d(I)V

    :goto_5
    goto/32 :goto_d

    nop

    :goto_6
    const/4 p1, 0x0

    goto/32 :goto_a

    nop

    :goto_7
    if-eqz p2, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_8
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->v()Z

    move-result v0

    goto/32 :goto_e

    nop

    :goto_9
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/D;->c(I)V

    goto/32 :goto_c

    nop

    :goto_a
    return p1

    :goto_b
    goto/32 :goto_9

    nop

    :goto_c
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    goto/32 :goto_3

    nop

    :goto_d
    return p2

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_6

    nop
.end method

.method a(ILcom/google/android/exoplayer2/S;Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;Z)I
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    const/4 v1, -0x3

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    goto/32 :goto_b

    nop

    :goto_3
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->v()Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {v0, p2, p3, p4, v2}, Lcom/google/android/exoplayer2/source/H;->a(Lcom/google/android/exoplayer2/S;Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;ZZ)I

    move-result p2

    goto/32 :goto_d

    nop

    :goto_5
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/D;->c(I)V

    goto/32 :goto_2

    nop

    :goto_6
    return v1

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/source/D;->M:Z

    goto/32 :goto_4

    nop

    :goto_9
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/D;->d(I)V

    :goto_a
    goto/32 :goto_c

    nop

    :goto_b
    aget-object v0, v0, p1

    goto/32 :goto_8

    nop

    :goto_c
    return p2

    :goto_d
    if-eq p2, v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop
.end method

.method public a()J
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer2/source/D;->G:I

    if-nez v0, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/D;->f()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public a(J)J
    .locals 4

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->o()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->z:Lcom/google/android/exoplayer2/source/D$e;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/D$e;->b:[Z

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/D;->A:Lcom/google/android/exoplayer2/extractor/u;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/extractor/u;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 p1, 0x0

    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/source/D;->F:Z

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/D;->I:J

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->s()Z

    move-result v2

    if-eqz v2, :cond_1

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/D;->J:J

    return-wide p1

    :cond_1
    iget v2, p0, Lcom/google/android/exoplayer2/source/D;->D:I

    const/4 v3, 0x7

    if-eq v2, v3, :cond_2

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/exoplayer2/source/D;->a([ZJ)Z

    move-result v0

    if-eqz v0, :cond_2

    return-wide p1

    :cond_2
    iput-boolean v1, p0, Lcom/google/android/exoplayer2/source/D;->K:Z

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/D;->J:J

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/source/D;->M:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/z;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/z;->a()V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/z;->b()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v2, v0

    :goto_1
    if-ge v1, v2, :cond_4

    aget-object v3, v0, v1

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/source/H;->k()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    return-wide p1
.end method

.method public a(JLcom/google/android/exoplayer2/ma;)J
    .locals 9

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->o()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->A:Lcom/google/android/exoplayer2/extractor/u;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 p1, 0x0

    return-wide p1

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->A:Lcom/google/android/exoplayer2/extractor/u;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/u;->b(J)Lcom/google/android/exoplayer2/extractor/u$a;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/exoplayer2/extractor/u$a;->a:Lcom/google/android/exoplayer2/extractor/v;

    iget-wide v5, v1, Lcom/google/android/exoplayer2/extractor/v;->b:J

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/u$a;->b:Lcom/google/android/exoplayer2/extractor/v;

    iget-wide v7, v0, Lcom/google/android/exoplayer2/extractor/v;->b:J

    move-object v2, p3

    move-wide v3, p1

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/exoplayer2/ma;->a(JJJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public a([Lcom/google/android/exoplayer2/trackselection/l;[Z[Lcom/google/android/exoplayer2/source/SampleStream;[ZJ)J
    .locals 8

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->o()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->z:Lcom/google/android/exoplayer2/source/D$e;

    iget-object v1, v0, Lcom/google/android/exoplayer2/source/D$e;->a:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/D$e;->c:[Z

    iget v2, p0, Lcom/google/android/exoplayer2/source/D;->G:I

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    array-length v5, p1

    const/4 v6, 0x1

    if-ge v4, v5, :cond_2

    aget-object v5, p3, v4

    if-eqz v5, :cond_1

    aget-object v5, p1, v4

    if-eqz v5, :cond_0

    aget-boolean v5, p2, v4

    if-nez v5, :cond_1

    :cond_0
    aget-object v5, p3, v4

    check-cast v5, Lcom/google/android/exoplayer2/source/D$c;

    invoke-static {v5}, Lcom/google/android/exoplayer2/source/D$c;->a(Lcom/google/android/exoplayer2/source/D$c;)I

    move-result v5

    aget-boolean v7, v0, v5

    invoke-static {v7}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget v7, p0, Lcom/google/android/exoplayer2/source/D;->G:I

    sub-int/2addr v7, v6

    iput v7, p0, Lcom/google/android/exoplayer2/source/D;->G:I

    aput-boolean v3, v0, v5

    const/4 v5, 0x0

    aput-object v5, p3, v4

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    iget-boolean p2, p0, Lcom/google/android/exoplayer2/source/D;->E:Z

    if-eqz p2, :cond_3

    if-nez v2, :cond_4

    goto :goto_1

    :cond_3
    const-wide/16 v4, 0x0

    cmp-long p2, p5, v4

    if-eqz p2, :cond_4

    :goto_1
    move p2, v6

    goto :goto_2

    :cond_4
    move p2, v3

    :goto_2
    move v2, p2

    move p2, v3

    :goto_3
    array-length v4, p1

    if-ge p2, v4, :cond_9

    aget-object v4, p3, p2

    if-nez v4, :cond_8

    aget-object v4, p1, p2

    if-eqz v4, :cond_8

    aget-object v4, p1, p2

    invoke-interface {v4}, Lcom/google/android/exoplayer2/trackselection/l;->length()I

    move-result v5

    if-ne v5, v6, :cond_5

    move v5, v6

    goto :goto_4

    :cond_5
    move v5, v3

    :goto_4
    invoke-static {v5}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    invoke-interface {v4, v3}, Lcom/google/android/exoplayer2/trackselection/l;->b(I)I

    move-result v5

    if-nez v5, :cond_6

    move v5, v6

    goto :goto_5

    :cond_6
    move v5, v3

    :goto_5
    invoke-static {v5}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    invoke-interface {v4}, Lcom/google/android/exoplayer2/trackselection/l;->a()Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->a(Lcom/google/android/exoplayer2/source/TrackGroup;)I

    move-result v4

    aget-boolean v5, v0, v4

    xor-int/2addr v5, v6

    invoke-static {v5}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget v5, p0, Lcom/google/android/exoplayer2/source/D;->G:I

    add-int/2addr v5, v6

    iput v5, p0, Lcom/google/android/exoplayer2/source/D;->G:I

    aput-boolean v6, v0, v4

    new-instance v5, Lcom/google/android/exoplayer2/source/D$c;

    invoke-direct {v5, p0, v4}, Lcom/google/android/exoplayer2/source/D$c;-><init>(Lcom/google/android/exoplayer2/source/D;I)V

    aput-object v5, p3, p2

    aput-boolean v6, p4, p2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    aget-object v2, v2, v4

    invoke-virtual {v2, p5, p6, v6}, Lcom/google/android/exoplayer2/source/H;->b(JZ)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/source/H;->d()I

    move-result v2

    if-eqz v2, :cond_7

    move v2, v6

    goto :goto_6

    :cond_7
    move v2, v3

    :cond_8
    :goto_6
    add-int/lit8 p2, p2, 0x1

    goto :goto_3

    :cond_9
    iget p1, p0, Lcom/google/android/exoplayer2/source/D;->G:I

    if-nez p1, :cond_c

    iput-boolean v3, p0, Lcom/google/android/exoplayer2/source/D;->K:Z

    iput-boolean v3, p0, Lcom/google/android/exoplayer2/source/D;->F:Z

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/z;->d()Z

    move-result p1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length p2, p1

    :goto_7
    if-ge v3, p2, :cond_a

    aget-object p3, p1, v3

    invoke-virtual {p3}, Lcom/google/android/exoplayer2/source/H;->a()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_a
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/z;->a()V

    goto :goto_a

    :cond_b
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length p2, p1

    :goto_8
    if-ge v3, p2, :cond_e

    aget-object p3, p1, v3

    invoke-virtual {p3}, Lcom/google/android/exoplayer2/source/H;->k()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_c
    if-eqz v2, :cond_e

    invoke-virtual {p0, p5, p6}, Lcom/google/android/exoplayer2/source/D;->a(J)J

    move-result-wide p5

    :goto_9
    array-length p1, p3

    if-ge v3, p1, :cond_e

    aget-object p1, p3, v3

    if-eqz p1, :cond_d

    aput-boolean v6, p4, v3

    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_e
    :goto_a
    iput-boolean v6, p0, Lcom/google/android/exoplayer2/source/D;->E:Z

    return-wide p5
.end method

.method public a(II)Lcom/google/android/exoplayer2/extractor/TrackOutput;
    .locals 1

    new-instance p2, Lcom/google/android/exoplayer2/source/D$d;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/google/android/exoplayer2/source/D$d;-><init>(IZ)V

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/source/D;->a(Lcom/google/android/exoplayer2/source/D$d;)Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/google/android/exoplayer2/source/D$a;JJLjava/io/IOException;I)Lcom/google/android/exoplayer2/upstream/z$b;
    .locals 25

    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p1}, Lcom/google/android/exoplayer2/source/D;->a(Lcom/google/android/exoplayer2/source/D$a;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->a(Lcom/google/android/exoplayer2/source/D$a;)Lcom/google/android/exoplayer2/upstream/B;

    move-result-object v1

    new-instance v14, Lcom/google/android/exoplayer2/source/t;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->b(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v3

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->c(Lcom/google/android/exoplayer2/source/D$a;)Lcom/google/android/exoplayer2/upstream/DataSpec;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/B;->c()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/B;->d()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/B;->b()J

    move-result-wide v12

    move-object v2, v14

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    invoke-direct/range {v2 .. v13}, Lcom/google/android/exoplayer2/source/t;-><init>(JLcom/google/android/exoplayer2/upstream/DataSpec;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    new-instance v1, Lcom/google/android/exoplayer2/source/w;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->d(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v21

    iget-wide v2, v0, Lcom/google/android/exoplayer2/source/D;->B:J

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v23

    const/16 v16, 0x1

    const/16 v17, -0x1

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object v15, v1

    invoke-direct/range {v15 .. v24}, Lcom/google/android/exoplayer2/source/w;-><init>(IILcom/google/android/exoplayer2/Format;ILjava/lang/Object;JJ)V

    iget-object v2, v0, Lcom/google/android/exoplayer2/source/D;->f:Lcom/google/android/exoplayer2/upstream/x;

    new-instance v3, Lcom/google/android/exoplayer2/upstream/x$a;

    move-object/from16 v13, p6

    move/from16 v4, p7

    invoke-direct {v3, v14, v1, v13, v4}, Lcom/google/android/exoplayer2/upstream/x$a;-><init>(Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;Ljava/io/IOException;I)V

    invoke-interface {v2, v3}, Lcom/google/android/exoplayer2/upstream/x;->a(Lcom/google/android/exoplayer2/upstream/x$a;)J

    move-result-wide v1

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v3, v1, v3

    const/4 v4, 0x1

    if-nez v3, :cond_0

    sget-object v1, Lcom/google/android/exoplayer2/upstream/z;->d:Lcom/google/android/exoplayer2/upstream/z$b;

    move-object/from16 v15, p1

    goto :goto_1

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/source/D;->q()I

    move-result v3

    iget v5, v0, Lcom/google/android/exoplayer2/source/D;->L:I

    if-le v3, v5, :cond_1

    move-object/from16 v15, p1

    move v5, v4

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    move-object/from16 v15, p1

    :goto_0
    invoke-direct {v0, v15, v3}, Lcom/google/android/exoplayer2/source/D;->a(Lcom/google/android/exoplayer2/source/D$a;I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v5, v1, v2}, Lcom/google/android/exoplayer2/upstream/z;->a(ZJ)Lcom/google/android/exoplayer2/upstream/z$b;

    move-result-object v1

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/google/android/exoplayer2/upstream/z;->c:Lcom/google/android/exoplayer2/upstream/z$b;

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/z$b;->a()Z

    move-result v2

    xor-int/lit8 v16, v2, 0x1

    iget-object v2, v0, Lcom/google/android/exoplayer2/source/D;->g:Lcom/google/android/exoplayer2/source/A$a;

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->d(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v9

    iget-wide v11, v0, Lcom/google/android/exoplayer2/source/D;->B:J

    move-object v3, v14

    move-object/from16 v13, p6

    move/from16 v14, v16

    invoke-virtual/range {v2 .. v14}, Lcom/google/android/exoplayer2/source/A$a;->a(Lcom/google/android/exoplayer2/source/t;IILcom/google/android/exoplayer2/Format;ILjava/lang/Object;JJLjava/io/IOException;Z)V

    if-eqz v16, :cond_3

    iget-object v2, v0, Lcom/google/android/exoplayer2/source/D;->f:Lcom/google/android/exoplayer2/upstream/x;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->b(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/google/android/exoplayer2/upstream/x;->a(J)V

    :cond_3
    return-object v1
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/z$d;JJLjava/io/IOException;I)Lcom/google/android/exoplayer2/upstream/z$b;
    .locals 0

    check-cast p1, Lcom/google/android/exoplayer2/source/D$a;

    invoke-virtual/range {p0 .. p7}, Lcom/google/android/exoplayer2/source/D;->a(Lcom/google/android/exoplayer2/source/D$a;JJLjava/io/IOException;I)Lcom/google/android/exoplayer2/upstream/z$b;

    move-result-object p1

    return-object p1
.end method

.method public a(JZ)V
    .locals 5

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->o()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->z:Lcom/google/android/exoplayer2/source/D$e;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/D$e;->c:[Z

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v1, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    aget-object v3, v3, v2

    aget-boolean v4, v0, v2

    invoke-virtual {v3, p1, p2, p3, v4}, Lcom/google/android/exoplayer2/source/H;->a(JZZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/Format;)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->r:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->p:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/u;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->r:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer2/source/i;

    invoke-direct {v1, p0, p1}, Lcom/google/android/exoplayer2/source/i;-><init>(Lcom/google/android/exoplayer2/source/D;Lcom/google/android/exoplayer2/extractor/u;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/D$a;JJ)V
    .locals 15

    move-object v0, p0

    iget-wide v1, v0, Lcom/google/android/exoplayer2/source/D;->B:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/google/android/exoplayer2/source/D;->A:Lcom/google/android/exoplayer2/extractor/u;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/google/android/exoplayer2/extractor/u;->b()Z

    move-result v1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->r()J

    move-result-wide v2

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    const-wide/16 v2, 0x0

    goto :goto_0

    :cond_0
    const-wide/16 v4, 0x2710

    add-long/2addr v2, v4

    :goto_0
    iput-wide v2, v0, Lcom/google/android/exoplayer2/source/D;->B:J

    iget-object v2, v0, Lcom/google/android/exoplayer2/source/D;->i:Lcom/google/android/exoplayer2/source/D$b;

    iget-wide v3, v0, Lcom/google/android/exoplayer2/source/D;->B:J

    iget-boolean v5, v0, Lcom/google/android/exoplayer2/source/D;->C:Z

    invoke-interface {v2, v3, v4, v1, v5}, Lcom/google/android/exoplayer2/source/D$b;->a(JZZ)V

    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->a(Lcom/google/android/exoplayer2/source/D$a;)Lcom/google/android/exoplayer2/upstream/B;

    move-result-object v1

    new-instance v14, Lcom/google/android/exoplayer2/source/t;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->b(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v3

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->c(Lcom/google/android/exoplayer2/source/D$a;)Lcom/google/android/exoplayer2/upstream/DataSpec;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/B;->c()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/B;->d()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/B;->b()J

    move-result-wide v12

    move-object v2, v14

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    invoke-direct/range {v2 .. v13}, Lcom/google/android/exoplayer2/source/t;-><init>(JLcom/google/android/exoplayer2/upstream/DataSpec;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/source/D;->f:Lcom/google/android/exoplayer2/upstream/x;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->b(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/google/android/exoplayer2/upstream/x;->a(J)V

    iget-object v2, v0, Lcom/google/android/exoplayer2/source/D;->g:Lcom/google/android/exoplayer2/source/A$a;

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->d(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v9

    iget-wide v11, v0, Lcom/google/android/exoplayer2/source/D;->B:J

    move-object v3, v14

    invoke-virtual/range {v2 .. v12}, Lcom/google/android/exoplayer2/source/A$a;->b(Lcom/google/android/exoplayer2/source/t;IILcom/google/android/exoplayer2/Format;ILjava/lang/Object;JJ)V

    invoke-direct/range {p0 .. p1}, Lcom/google/android/exoplayer2/source/D;->a(Lcom/google/android/exoplayer2/source/D$a;)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/source/D;->M:Z

    iget-object v1, v0, Lcom/google/android/exoplayer2/source/D;->s:Lcom/google/android/exoplayer2/source/x$a;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/google/android/exoplayer2/source/x$a;

    invoke-interface {v1, p0}, Lcom/google/android/exoplayer2/source/I$a;->a(Lcom/google/android/exoplayer2/source/I;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/D$a;JJZ)V
    .locals 15

    move-object v0, p0

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->a(Lcom/google/android/exoplayer2/source/D$a;)Lcom/google/android/exoplayer2/upstream/B;

    move-result-object v1

    new-instance v14, Lcom/google/android/exoplayer2/source/t;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->b(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v3

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->c(Lcom/google/android/exoplayer2/source/D$a;)Lcom/google/android/exoplayer2/upstream/DataSpec;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/B;->c()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/B;->d()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/B;->b()J

    move-result-wide v12

    move-object v2, v14

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    invoke-direct/range {v2 .. v13}, Lcom/google/android/exoplayer2/source/t;-><init>(JLcom/google/android/exoplayer2/upstream/DataSpec;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/source/D;->f:Lcom/google/android/exoplayer2/upstream/x;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->b(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/google/android/exoplayer2/upstream/x;->a(J)V

    iget-object v2, v0, Lcom/google/android/exoplayer2/source/D;->g:Lcom/google/android/exoplayer2/source/A$a;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/source/D$a;->d(Lcom/google/android/exoplayer2/source/D$a;)J

    move-result-wide v9

    iget-wide v11, v0, Lcom/google/android/exoplayer2/source/D;->B:J

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v14

    invoke-virtual/range {v2 .. v12}, Lcom/google/android/exoplayer2/source/A$a;->a(Lcom/google/android/exoplayer2/source/t;IILcom/google/android/exoplayer2/Format;ILjava/lang/Object;JJ)V

    if-nez p6, :cond_1

    invoke-direct/range {p0 .. p1}, Lcom/google/android/exoplayer2/source/D;->a(Lcom/google/android/exoplayer2/source/D$a;)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/source/H;->k()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    iget v1, v0, Lcom/google/android/exoplayer2/source/D;->G:I

    if-lez v1, :cond_1

    iget-object v1, v0, Lcom/google/android/exoplayer2/source/D;->s:Lcom/google/android/exoplayer2/source/x$a;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/google/android/exoplayer2/source/x$a;

    invoke-interface {v1, p0}, Lcom/google/android/exoplayer2/source/I$a;->a(Lcom/google/android/exoplayer2/source/I;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/x$a;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/D;->s:Lcom/google/android/exoplayer2/source/x$a;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->o:Lcom/google/android/exoplayer2/util/h;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/h;->d()Z

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->u()V

    return-void
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/z$d;JJ)V
    .locals 0

    check-cast p1, Lcom/google/android/exoplayer2/source/D$a;

    invoke-virtual/range {p0 .. p5}, Lcom/google/android/exoplayer2/source/D;->a(Lcom/google/android/exoplayer2/source/D$a;JJ)V

    return-void
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/z$d;JJZ)V
    .locals 0

    check-cast p1, Lcom/google/android/exoplayer2/source/D$a;

    invoke-virtual/range {p0 .. p6}, Lcom/google/android/exoplayer2/source/D;->a(Lcom/google/android/exoplayer2/source/D$a;JJZ)V

    return-void
.end method

.method a(I)Z
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->M:Z

    goto/32 :goto_3

    nop

    :goto_1
    const/4 p1, 0x1

    goto/32 :goto_b

    nop

    :goto_2
    if-nez p1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/source/H;->a(Z)Z

    move-result p1

    goto/32 :goto_2

    nop

    :goto_4
    const/4 p1, 0x0

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->v()Z

    move-result v0

    goto/32 :goto_8

    nop

    :goto_7
    aget-object p1, v0, p1

    goto/32 :goto_0

    nop

    :goto_8
    if-eqz v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_a

    nop

    :goto_9
    return p1

    :goto_a
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    goto/32 :goto_7

    nop

    :goto_b
    goto :goto_5

    :goto_c
    goto/32 :goto_4

    nop
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/D;->m()V

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->M:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->x:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/ea;

    const-string v1, "Loading finished before preparation is complete."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ea;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method b(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/H;->h()V

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    goto/32 :goto_2

    nop

    :goto_2
    aget-object p1, v0, p1

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/D;->m()V

    goto/32 :goto_4

    nop

    :goto_4
    return-void
.end method

.method public synthetic b(Lcom/google/android/exoplayer2/extractor/u;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/D;->c(Lcom/google/android/exoplayer2/extractor/u;)V

    return-void
.end method

.method public b(J)Z
    .locals 0

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/source/D;->M:Z

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/z;->c()Z

    move-result p1

    if-nez p1, :cond_2

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/source/D;->K:Z

    if-nez p1, :cond_2

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/source/D;->x:Z

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/google/android/exoplayer2/source/D;->G:I

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/D;->o:Lcom/google/android/exoplayer2/util/h;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/h;->d()Z

    move-result p1

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/upstream/z;->d()Z

    move-result p2

    if-nez p2, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->u()V

    const/4 p1, 0x1

    :cond_1
    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public c(J)V
    .locals 0

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/z;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->o:Lcom/google/android/exoplayer2/util/h;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/h;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d()J
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->F:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->M:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->q()I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer2/source/D;->L:I

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->F:Z

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/D;->I:J

    return-wide v0

    :cond_1
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    return-wide v0
.end method

.method public e()Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->o()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->z:Lcom/google/android/exoplayer2/source/D$e;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/D$e;->a:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    return-object v0
.end method

.method public f()J
    .locals 11

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->o()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->z:Lcom/google/android/exoplayer2/source/D$e;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/D$e;->b:[Z

    iget-boolean v1, p0, Lcom/google/android/exoplayer2/source/D;->M:Z

    const-wide/high16 v2, -0x8000000000000000L

    if-eqz v1, :cond_0

    return-wide v2

    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/D;->J:J

    return-wide v0

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/source/D;->y:Z

    const-wide v4, 0x7fffffffffffffffL

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v1, v1

    const/4 v6, 0x0

    move-wide v7, v4

    :goto_0
    if-ge v6, v1, :cond_4

    aget-boolean v9, v0, v6

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    aget-object v9, v9, v6

    invoke-virtual {v9}, Lcom/google/android/exoplayer2/source/H;->g()Z

    move-result v9

    if-nez v9, :cond_2

    iget-object v9, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    aget-object v9, v9, v6

    invoke-virtual {v9}, Lcom/google/android/exoplayer2/source/H;->b()J

    move-result-wide v9

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    move-wide v7, v4

    :cond_4
    cmp-long v0, v7, v4

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/D;->r()J

    move-result-wide v7

    :cond_5
    cmp-long v0, v7, v2

    if-nez v0, :cond_6

    iget-wide v7, p0, Lcom/google/android/exoplayer2/source/D;->I:J

    :cond_6
    return-wide v7
.end method

.method public g()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->w:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->r:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/D;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public h()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/source/H;->j()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->n:Lcom/google/android/exoplayer2/source/C;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/C;->release()V

    return-void
.end method

.method k()Lcom/google/android/exoplayer2/extractor/TrackOutput;
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_1
    return-object v0

    :goto_2
    new-instance v0, Lcom/google/android/exoplayer2/source/D$d;

    goto/32 :goto_0

    nop

    :goto_3
    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/source/D$d;-><init>(IZ)V

    goto/32 :goto_4

    nop

    :goto_4
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/D;->a(Lcom/google/android/exoplayer2/source/D$d;)Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_5
    const/4 v2, 0x1

    goto/32 :goto_3

    nop
.end method

.method public synthetic l()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->N:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->s:Lcom/google/android/exoplayer2/source/x$a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/x$a;

    invoke-interface {v0, p0}, Lcom/google/android/exoplayer2/source/I$a;->a(Lcom/google/android/exoplayer2/source/I;)V

    :cond_0
    return-void
.end method

.method m()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v1, v2}, Lcom/google/android/exoplayer2/upstream/x;->a(I)I

    move-result v1

    goto/32 :goto_5

    nop

    :goto_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    goto/32 :goto_4

    nop

    :goto_3
    iget v2, p0, Lcom/google/android/exoplayer2/source/D;->D:I

    goto/32 :goto_1

    nop

    :goto_4
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/D;->f:Lcom/google/android/exoplayer2/upstream/x;

    goto/32 :goto_3

    nop

    :goto_5
    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/upstream/z;->a(I)V

    goto/32 :goto_0

    nop
.end method

.method public n()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->u:[Lcom/google/android/exoplayer2/source/H;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/source/H;->i()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->m:Lcom/google/android/exoplayer2/upstream/z;

    invoke-virtual {v0, p0}, Lcom/google/android/exoplayer2/upstream/z;->a(Lcom/google/android/exoplayer2/upstream/z$e;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/D;->r:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/exoplayer2/source/D;->s:Lcom/google/android/exoplayer2/source/x$a;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/D;->N:Z

    return-void
.end method
