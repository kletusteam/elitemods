.class Lcom/google/android/exoplayer2/text/webvtt/h$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/text/webvtt/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/google/android/exoplayer2/text/webvtt/h$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/google/android/exoplayer2/text/webvtt/h$b;

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/text/webvtt/a;->a:Lcom/google/android/exoplayer2/text/webvtt/a;

    sput-object v0, Lcom/google/android/exoplayer2/text/webvtt/h$a;->a:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/exoplayer2/text/webvtt/h$b;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/text/webvtt/h$a;->b:Lcom/google/android/exoplayer2/text/webvtt/h$b;

    iput p2, p0, Lcom/google/android/exoplayer2/text/webvtt/h$a;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/text/webvtt/h$b;ILcom/google/android/exoplayer2/text/webvtt/g;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/text/webvtt/h$a;-><init>(Lcom/google/android/exoplayer2/text/webvtt/h$b;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/text/webvtt/h$a;Lcom/google/android/exoplayer2/text/webvtt/h$a;)I
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/text/webvtt/h$a;->b:Lcom/google/android/exoplayer2/text/webvtt/h$b;

    iget p0, p0, Lcom/google/android/exoplayer2/text/webvtt/h$b;->b:I

    iget-object p1, p1, Lcom/google/android/exoplayer2/text/webvtt/h$a;->b:Lcom/google/android/exoplayer2/text/webvtt/h$b;

    iget p1, p1, Lcom/google/android/exoplayer2/text/webvtt/h$b;->b:I

    invoke-static {p0, p1}, Ljava/lang/Integer;->compare(II)I

    move-result p0

    return p0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/text/webvtt/h$a;)Lcom/google/android/exoplayer2/text/webvtt/h$b;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/text/webvtt/h$a;->b:Lcom/google/android/exoplayer2/text/webvtt/h$b;

    return-object p0
.end method

.method static synthetic a()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/text/webvtt/h$a;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/text/webvtt/h$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/text/webvtt/h$a;->c:I

    return p0
.end method
