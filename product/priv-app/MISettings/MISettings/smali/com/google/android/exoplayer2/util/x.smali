.class public Lcom/google/android/exoplayer2/util/x;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/util/x$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/google/android/exoplayer2/util/x$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/google/android/exoplayer2/util/x$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:I

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/google/android/exoplayer2/util/x$a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:[Lcom/google/android/exoplayer2/util/x$a;

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/util/a;->a:Lcom/google/android/exoplayer2/util/a;

    sput-object v0, Lcom/google/android/exoplayer2/util/x;->a:Ljava/util/Comparator;

    sget-object v0, Lcom/google/android/exoplayer2/util/b;->a:Lcom/google/android/exoplayer2/util/b;

    sput-object v0, Lcom/google/android/exoplayer2/util/x;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/exoplayer2/util/x;->c:I

    const/4 p1, 0x5

    new-array p1, p1, [Lcom/google/android/exoplayer2/util/x$a;

    iput-object p1, p0, Lcom/google/android/exoplayer2/util/x;->e:[Lcom/google/android/exoplayer2/util/x$a;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/util/x;->d:Ljava/util/ArrayList;

    const/4 p1, -0x1

    iput p1, p0, Lcom/google/android/exoplayer2/util/x;->f:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/util/x$a;Lcom/google/android/exoplayer2/util/x$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/util/x$a;->a:I

    iget p1, p1, Lcom/google/android/exoplayer2/util/x$a;->a:I

    sub-int/2addr p0, p1

    return p0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/util/x$a;Lcom/google/android/exoplayer2/util/x$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/util/x$a;->c:F

    iget p1, p1, Lcom/google/android/exoplayer2/util/x$a;->c:F

    invoke-static {p0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result p0

    return p0
.end method

.method private b()V
    .locals 3

    iget v0, p0, Lcom/google/android/exoplayer2/util/x;->f:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/util/x;->d:Ljava/util/ArrayList;

    sget-object v2, Lcom/google/android/exoplayer2/util/x;->a:Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iput v1, p0, Lcom/google/android/exoplayer2/util/x;->f:I

    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer2/util/x;->f:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/util/x;->d:Ljava/util/ArrayList;

    sget-object v1, Lcom/google/android/exoplayer2/util/x;->b:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/util/x;->f:I

    :cond_0
    return-void
.end method


# virtual methods
.method public a(F)F
    .locals 4

    invoke-direct {p0}, Lcom/google/android/exoplayer2/util/x;->c()V

    iget v0, p0, Lcom/google/android/exoplayer2/util/x;->h:I

    int-to-float v0, v0

    mul-float/2addr p1, v0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/util/x;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/exoplayer2/util/x;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/util/x$a;

    iget v3, v2, Lcom/google/android/exoplayer2/util/x$a;->b:I

    add-int/2addr v1, v3

    int-to-float v3, v1

    cmpl-float v3, v3, p1

    if-ltz v3, :cond_0

    iget p1, v2, Lcom/google/android/exoplayer2/util/x$a;->c:F

    return p1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/util/x;->d:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    const/high16 p1, 0x7fc00000    # Float.NaN

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/google/android/exoplayer2/util/x;->d:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/exoplayer2/util/x$a;

    iget p1, p1, Lcom/google/android/exoplayer2/util/x$a;->c:F

    :goto_1
    return p1
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/util/x;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/util/x;->f:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/util/x;->g:I

    iput v0, p0, Lcom/google/android/exoplayer2/util/x;->h:I

    return-void
.end method

.method public a(IF)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/util/x;->b()V

    iget v0, p0, Lcom/google/android/exoplayer2/util/x;->i:I

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/util/x;->e:[Lcom/google/android/exoplayer2/util/x$a;

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/util/x;->i:I

    aget-object v0, v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/util/x$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/x$a;-><init>(Lcom/google/android/exoplayer2/util/w;)V

    :goto_0
    iget v1, p0, Lcom/google/android/exoplayer2/util/x;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/exoplayer2/util/x;->g:I

    iput v1, v0, Lcom/google/android/exoplayer2/util/x$a;->a:I

    iput p1, v0, Lcom/google/android/exoplayer2/util/x$a;->b:I

    iput p2, v0, Lcom/google/android/exoplayer2/util/x$a;->c:F

    iget-object p2, p0, Lcom/google/android/exoplayer2/util/x;->d:Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget p2, p0, Lcom/google/android/exoplayer2/util/x;->h:I

    add-int/2addr p2, p1

    iput p2, p0, Lcom/google/android/exoplayer2/util/x;->h:I

    :cond_1
    :goto_1
    iget p1, p0, Lcom/google/android/exoplayer2/util/x;->h:I

    iget p2, p0, Lcom/google/android/exoplayer2/util/x;->c:I

    if-le p1, p2, :cond_3

    sub-int/2addr p1, p2

    iget-object p2, p0, Lcom/google/android/exoplayer2/util/x;->d:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/google/android/exoplayer2/util/x$a;

    iget v1, p2, Lcom/google/android/exoplayer2/util/x$a;->b:I

    if-gt v1, p1, :cond_2

    iget p1, p0, Lcom/google/android/exoplayer2/util/x;->h:I

    sub-int/2addr p1, v1

    iput p1, p0, Lcom/google/android/exoplayer2/util/x;->h:I

    iget-object p1, p0, Lcom/google/android/exoplayer2/util/x;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget p1, p0, Lcom/google/android/exoplayer2/util/x;->i:I

    const/4 v0, 0x5

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/util/x;->e:[Lcom/google/android/exoplayer2/util/x$a;

    add-int/lit8 v1, p1, 0x1

    iput v1, p0, Lcom/google/android/exoplayer2/util/x;->i:I

    aput-object p2, v0, p1

    goto :goto_1

    :cond_2
    sub-int/2addr v1, p1

    iput v1, p2, Lcom/google/android/exoplayer2/util/x$a;->b:I

    iget p2, p0, Lcom/google/android/exoplayer2/util/x;->h:I

    sub-int/2addr p2, p1

    iput p2, p0, Lcom/google/android/exoplayer2/util/x;->h:I

    goto :goto_1

    :cond_3
    return-void
.end method
