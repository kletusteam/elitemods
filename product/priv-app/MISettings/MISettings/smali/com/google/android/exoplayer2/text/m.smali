.class public final Lcom/google/android/exoplayer2/text/m;
.super Lcom/google/android/exoplayer2/F;

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private final m:Landroid/os/Handler;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final n:Lcom/google/android/exoplayer2/text/l;

.field private final o:Lcom/google/android/exoplayer2/text/i;

.field private final p:Lcom/google/android/exoplayer2/S;

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:Lcom/google/android/exoplayer2/Format;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/google/android/exoplayer2/text/f;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/google/android/exoplayer2/text/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/google/android/exoplayer2/text/k;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/google/android/exoplayer2/text/k;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private z:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/text/l;Landroid/os/Looper;)V
    .locals 1
    .param p2    # Landroid/os/Looper;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    sget-object v0, Lcom/google/android/exoplayer2/text/i;->a:Lcom/google/android/exoplayer2/text/i;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/exoplayer2/text/m;-><init>(Lcom/google/android/exoplayer2/text/l;Landroid/os/Looper;Lcom/google/android/exoplayer2/text/i;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/exoplayer2/text/l;Landroid/os/Looper;Lcom/google/android/exoplayer2/text/i;)V
    .locals 1
    .param p2    # Landroid/os/Looper;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/F;-><init>(I)V

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/text/l;

    iput-object p1, p0, Lcom/google/android/exoplayer2/text/m;->n:Lcom/google/android/exoplayer2/text/l;

    if-nez p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p2, p0}, Lcom/google/android/exoplayer2/util/E;->a(Landroid/os/Looper;Landroid/os/Handler$Callback;)Landroid/os/Handler;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/text/m;->m:Landroid/os/Handler;

    iput-object p3, p0, Lcom/google/android/exoplayer2/text/m;->o:Lcom/google/android/exoplayer2/text/i;

    new-instance p1, Lcom/google/android/exoplayer2/S;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/S;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/text/m;->p:Lcom/google/android/exoplayer2/S;

    return-void
.end method

.method private A()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/text/m;->s:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/m;->o:Lcom/google/android/exoplayer2/text/i;

    iget-object v1, p0, Lcom/google/android/exoplayer2/text/m;->u:Lcom/google/android/exoplayer2/Format;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/google/android/exoplayer2/Format;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/text/i;->b(Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/text/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/m;->v:Lcom/google/android/exoplayer2/text/f;

    return-void
.end method

.method private B()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/m;->w:Lcom/google/android/exoplayer2/text/j;

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/exoplayer2/text/m;->z:I

    iget-object v1, p0, Lcom/google/android/exoplayer2/text/m;->x:Lcom/google/android/exoplayer2/text/k;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/decoder/g;->release()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/m;->x:Lcom/google/android/exoplayer2/text/k;

    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/text/m;->y:Lcom/google/android/exoplayer2/text/k;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/decoder/g;->release()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/m;->y:Lcom/google/android/exoplayer2/text/k;

    :cond_1
    return-void
.end method

.method private C()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->B()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/m;->v:Lcom/google/android/exoplayer2/text/f;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/text/f;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/decoder/d;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/m;->v:Lcom/google/android/exoplayer2/text/f;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/text/m;->t:I

    return-void
.end method

.method private D()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->C()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->A()V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/text/g;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/m;->u:Lcom/google/android/exoplayer2/Format;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x27

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Subtitle decoding failed. streamFormat="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TextRenderer"

    invoke-static {v1, v0, p1}, Lcom/google/android/exoplayer2/util/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->y()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->D()V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/text/Cue;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/m;->n:Lcom/google/android/exoplayer2/text/l;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/text/l;->a(Ljava/util/List;)V

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/text/Cue;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/m;->m:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/text/m;->a(Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method private y()V
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/text/m;->b(Ljava/util/List;)V

    return-void
.end method

.method private z()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/m;->x:Lcom/google/android/exoplayer2/text/k;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lcom/google/android/exoplayer2/text/m;->z:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer2/text/m;->x:Lcom/google/android/exoplayer2/text/k;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/text/k;->a()I

    move-result v1

    if-lt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/text/m;->x:Lcom/google/android/exoplayer2/text/k;

    iget v1, p0, Lcom/google/android/exoplayer2/text/m;->z:I

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/text/k;->a(I)J

    move-result-wide v0

    goto :goto_1

    :cond_1
    :goto_0
    const-wide v0, 0x7fffffffffffffffL

    :goto_1
    return-wide v0
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/Format;)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/m;->o:Lcom/google/android/exoplayer2/text/i;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/text/i;->a(Lcom/google/android/exoplayer2/Format;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p1, Lcom/google/android/exoplayer2/Format;->E:Ljava/lang/Class;

    if-nez p1, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :goto_0
    invoke-static {p1}, Lcom/google/android/exoplayer2/RendererCapabilities;->d(I)I

    move-result p1

    return p1

    :cond_1
    iget-object p1, p1, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/q;->g(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    invoke-static {p1}, Lcom/google/android/exoplayer2/RendererCapabilities;->d(I)I

    move-result p1

    return p1

    :cond_2
    const/4 p1, 0x0

    invoke-static {p1}, Lcom/google/android/exoplayer2/RendererCapabilities;->d(I)I

    move-result p1

    return p1
.end method

.method public a(JJ)V
    .locals 8

    iget-boolean p3, p0, Lcom/google/android/exoplayer2/text/m;->r:Z

    if-eqz p3, :cond_0

    return-void

    :cond_0
    iget-object p3, p0, Lcom/google/android/exoplayer2/text/m;->y:Lcom/google/android/exoplayer2/text/k;

    if-nez p3, :cond_1

    iget-object p3, p0, Lcom/google/android/exoplayer2/text/m;->v:Lcom/google/android/exoplayer2/text/f;

    invoke-static {p3}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p3, Lcom/google/android/exoplayer2/text/f;

    invoke-interface {p3, p1, p2}, Lcom/google/android/exoplayer2/text/f;->a(J)V

    :try_start_0
    iget-object p3, p0, Lcom/google/android/exoplayer2/text/m;->v:Lcom/google/android/exoplayer2/text/f;

    invoke-static {p3}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p3, Lcom/google/android/exoplayer2/text/f;

    invoke-interface {p3}, Lcom/google/android/exoplayer2/decoder/d;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/google/android/exoplayer2/text/k;

    iput-object p3, p0, Lcom/google/android/exoplayer2/text/m;->y:Lcom/google/android/exoplayer2/text/k;
    :try_end_0
    .catch Lcom/google/android/exoplayer2/text/g; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/text/m;->a(Lcom/google/android/exoplayer2/text/g;)V

    return-void

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/F;->getState()I

    move-result p3

    const/4 p4, 0x2

    if-eq p3, p4, :cond_2

    return-void

    :cond_2
    iget-object p3, p0, Lcom/google/android/exoplayer2/text/m;->x:Lcom/google/android/exoplayer2/text/k;

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p3, :cond_3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->z()J

    move-result-wide v2

    move p3, v0

    :goto_1
    cmp-long v2, v2, p1

    if-gtz v2, :cond_4

    iget p3, p0, Lcom/google/android/exoplayer2/text/m;->z:I

    add-int/2addr p3, v1

    iput p3, p0, Lcom/google/android/exoplayer2/text/m;->z:I

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->z()J

    move-result-wide v2

    move p3, v1

    goto :goto_1

    :cond_3
    move p3, v0

    :cond_4
    iget-object v2, p0, Lcom/google/android/exoplayer2/text/m;->y:Lcom/google/android/exoplayer2/text/k;

    const/4 v3, 0x0

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/decoder/a;->isEndOfStream()Z

    move-result v4

    if-eqz v4, :cond_6

    if-nez p3, :cond_8

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->z()J

    move-result-wide v4

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v2, v4, v6

    if-nez v2, :cond_8

    iget v2, p0, Lcom/google/android/exoplayer2/text/m;->t:I

    if-ne v2, p4, :cond_5

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->D()V

    goto :goto_2

    :cond_5
    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->B()V

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/text/m;->r:Z

    goto :goto_2

    :cond_6
    iget-wide v4, v2, Lcom/google/android/exoplayer2/decoder/g;->timeUs:J

    cmp-long v4, v4, p1

    if-gtz v4, :cond_8

    iget-object p3, p0, Lcom/google/android/exoplayer2/text/m;->x:Lcom/google/android/exoplayer2/text/k;

    if-eqz p3, :cond_7

    invoke-virtual {p3}, Lcom/google/android/exoplayer2/decoder/g;->release()V

    :cond_7
    invoke-virtual {v2, p1, p2}, Lcom/google/android/exoplayer2/text/k;->a(J)I

    move-result p3

    iput p3, p0, Lcom/google/android/exoplayer2/text/m;->z:I

    iput-object v2, p0, Lcom/google/android/exoplayer2/text/m;->x:Lcom/google/android/exoplayer2/text/k;

    iput-object v3, p0, Lcom/google/android/exoplayer2/text/m;->y:Lcom/google/android/exoplayer2/text/k;

    move p3, v1

    :cond_8
    :goto_2
    if-eqz p3, :cond_9

    iget-object p3, p0, Lcom/google/android/exoplayer2/text/m;->x:Lcom/google/android/exoplayer2/text/k;

    invoke-static {p3}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p3, p0, Lcom/google/android/exoplayer2/text/m;->x:Lcom/google/android/exoplayer2/text/k;

    invoke-virtual {p3, p1, p2}, Lcom/google/android/exoplayer2/text/k;->b(J)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/text/m;->b(Ljava/util/List;)V

    :cond_9
    iget p1, p0, Lcom/google/android/exoplayer2/text/m;->t:I

    if-ne p1, p4, :cond_a

    return-void

    :cond_a
    :goto_3
    :try_start_1
    iget-boolean p1, p0, Lcom/google/android/exoplayer2/text/m;->q:Z

    if-nez p1, :cond_12

    iget-object p1, p0, Lcom/google/android/exoplayer2/text/m;->w:Lcom/google/android/exoplayer2/text/j;

    if-nez p1, :cond_c

    iget-object p1, p0, Lcom/google/android/exoplayer2/text/m;->v:Lcom/google/android/exoplayer2/text/f;

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/text/f;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/decoder/d;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/exoplayer2/text/j;

    if-nez p1, :cond_b

    return-void

    :cond_b
    iput-object p1, p0, Lcom/google/android/exoplayer2/text/m;->w:Lcom/google/android/exoplayer2/text/j;

    :cond_c
    iget p2, p0, Lcom/google/android/exoplayer2/text/m;->t:I

    if-ne p2, v1, :cond_d

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/decoder/a;->setFlags(I)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/text/m;->v:Lcom/google/android/exoplayer2/text/f;

    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p2, Lcom/google/android/exoplayer2/text/f;

    invoke-interface {p2, p1}, Lcom/google/android/exoplayer2/decoder/d;->a(Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/google/android/exoplayer2/text/m;->w:Lcom/google/android/exoplayer2/text/j;

    iput p4, p0, Lcom/google/android/exoplayer2/text/m;->t:I

    return-void

    :cond_d
    iget-object p2, p0, Lcom/google/android/exoplayer2/text/m;->p:Lcom/google/android/exoplayer2/S;

    invoke-virtual {p0, p2, p1, v0}, Lcom/google/android/exoplayer2/F;->a(Lcom/google/android/exoplayer2/S;Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;Z)I

    move-result p2

    const/4 p3, -0x4

    if-ne p2, p3, :cond_11

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/decoder/a;->isEndOfStream()Z

    move-result p2

    if-eqz p2, :cond_e

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/text/m;->q:Z

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/text/m;->s:Z

    goto :goto_5

    :cond_e
    iget-object p2, p0, Lcom/google/android/exoplayer2/text/m;->p:Lcom/google/android/exoplayer2/S;

    iget-object p2, p2, Lcom/google/android/exoplayer2/S;->b:Lcom/google/android/exoplayer2/Format;

    if-nez p2, :cond_f

    return-void

    :cond_f
    iget-wide p2, p2, Lcom/google/android/exoplayer2/Format;->p:J

    iput-wide p2, p1, Lcom/google/android/exoplayer2/text/j;->h:J

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->b()V

    iget-boolean p2, p0, Lcom/google/android/exoplayer2/text/m;->s:Z

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/decoder/a;->isKeyFrame()Z

    move-result p3

    if-nez p3, :cond_10

    move p3, v1

    goto :goto_4

    :cond_10
    move p3, v0

    :goto_4
    and-int/2addr p2, p3

    iput-boolean p2, p0, Lcom/google/android/exoplayer2/text/m;->s:Z

    :goto_5
    iget-boolean p2, p0, Lcom/google/android/exoplayer2/text/m;->s:Z

    if-nez p2, :cond_a

    iget-object p2, p0, Lcom/google/android/exoplayer2/text/m;->v:Lcom/google/android/exoplayer2/text/f;

    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p2, Lcom/google/android/exoplayer2/text/f;

    invoke-interface {p2, p1}, Lcom/google/android/exoplayer2/decoder/d;->a(Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/google/android/exoplayer2/text/m;->w:Lcom/google/android/exoplayer2/text/j;
    :try_end_1
    .catch Lcom/google/android/exoplayer2/text/g; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :cond_11
    const/4 p1, -0x3

    if-ne p2, p1, :cond_a

    return-void

    :catch_1
    move-exception p1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/text/m;->a(Lcom/google/android/exoplayer2/text/g;)V

    :cond_12
    return-void
.end method

.method protected a(JZ)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->y()V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/text/m;->q:Z

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/text/m;->r:Z

    iget p1, p0, Lcom/google/android/exoplayer2/text/m;->t:I

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->D()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->B()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/text/m;->v:Lcom/google/android/exoplayer2/text/f;

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/text/f;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/decoder/d;->flush()V

    :goto_0
    return-void
.end method

.method protected a([Lcom/google/android/exoplayer2/Format;JJ)V
    .locals 0

    const/4 p2, 0x0

    aget-object p1, p1, p2

    iput-object p1, p0, Lcom/google/android/exoplayer2/text/m;->u:Lcom/google/android/exoplayer2/Format;

    iget-object p1, p0, Lcom/google/android/exoplayer2/text/m;->v:Lcom/google/android/exoplayer2/text/f;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    iput p1, p0, Lcom/google/android/exoplayer2/text/m;->t:I

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->A()V

    :goto_0
    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/text/m;->r:Z

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "TextRenderer"

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 1

    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/text/m;->a(Ljava/util/List;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method protected u()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/m;->u:Lcom/google/android/exoplayer2/Format;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->y()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/text/m;->C()V

    return-void
.end method
