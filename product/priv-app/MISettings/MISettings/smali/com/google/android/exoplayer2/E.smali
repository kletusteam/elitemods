.class public abstract Lcom/google/android/exoplayer2/E;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/Player;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/E$b;,
        Lcom/google/android/exoplayer2/E$a;
    }
.end annotation


# instance fields
.field protected final a:Lcom/google/android/exoplayer2/sa$b;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/exoplayer2/sa$b;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/sa$b;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/E;->a:Lcom/google/android/exoplayer2/sa$b;

    return-void
.end method

.method private z()I
    .locals 2

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->getRepeatMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method


# virtual methods
.method public final g()Z
    .locals 3

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->i()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/E;->a:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/sa$b;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final hasNext()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/E;->o()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final hasPrevious()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/E;->m()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isPlaying()Z
    .locals 2

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->getPlaybackState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->p()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final m()I
    .locals 4

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->i()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/E;->z()I

    move-result v2

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->t()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/sa;->b(IIZ)I

    move-result v0

    :goto_0
    return v0
.end method

.method public final o()I
    .locals 4

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->i()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/E;->z()I

    move-result v2

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->t()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/sa;->a(IIZ)I

    move-result v0

    :goto_0
    return v0
.end method

.method public final y()J
    .locals 3

    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Lcom/google/android/exoplayer2/Player;->i()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/E;->a:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa$b;->c()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method
