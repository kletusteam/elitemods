.class public final Lcom/google/android/exoplayer2/W;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/W$b;,
        Lcom/google/android/exoplayer2/W$d;,
        Lcom/google/android/exoplayer2/W$c;,
        Lcom/google/android/exoplayer2/W$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/google/android/exoplayer2/W$d;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/google/android/exoplayer2/Y;

.field public final d:Lcom/google/android/exoplayer2/W$b;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/exoplayer2/W$b;Lcom/google/android/exoplayer2/W$d;Lcom/google/android/exoplayer2/Y;)V
    .locals 0
    .param p3    # Lcom/google/android/exoplayer2/W$d;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/W;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/exoplayer2/W;->b:Lcom/google/android/exoplayer2/W$d;

    iput-object p4, p0, Lcom/google/android/exoplayer2/W;->c:Lcom/google/android/exoplayer2/Y;

    iput-object p2, p0, Lcom/google/android/exoplayer2/W;->d:Lcom/google/android/exoplayer2/W$b;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/google/android/exoplayer2/W$b;Lcom/google/android/exoplayer2/W$d;Lcom/google/android/exoplayer2/Y;Lcom/google/android/exoplayer2/V;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/W;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer2/W$b;Lcom/google/android/exoplayer2/W$d;Lcom/google/android/exoplayer2/Y;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/google/android/exoplayer2/W;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/google/android/exoplayer2/W;

    iget-object v1, p0, Lcom/google/android/exoplayer2/W;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/exoplayer2/W;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer2/W;->d:Lcom/google/android/exoplayer2/W$b;

    iget-object v3, p1, Lcom/google/android/exoplayer2/W;->d:Lcom/google/android/exoplayer2/W$b;

    invoke-virtual {v1, v3}, Lcom/google/android/exoplayer2/W$b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer2/W;->b:Lcom/google/android/exoplayer2/W$d;

    iget-object v3, p1, Lcom/google/android/exoplayer2/W;->b:Lcom/google/android/exoplayer2/W$d;

    invoke-static {v1, v3}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer2/W;->c:Lcom/google/android/exoplayer2/Y;

    iget-object p1, p1, Lcom/google/android/exoplayer2/W;->c:Lcom/google/android/exoplayer2/Y;

    invoke-static {v1, p1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    move v0, v2

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/W;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/exoplayer2/W;->b:Lcom/google/android/exoplayer2/W$d;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/W$d;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/exoplayer2/W;->d:Lcom/google/android/exoplayer2/W$b;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/W$b;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/exoplayer2/W;->c:Lcom/google/android/exoplayer2/Y;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/Y;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
