.class public final Lcom/google/android/exoplayer2/audio/A;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/audio/AudioSink;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/audio/A$b;,
        Lcom/google/android/exoplayer2/audio/A$f;,
        Lcom/google/android/exoplayer2/audio/A$e;,
        Lcom/google/android/exoplayer2/audio/A$g;,
        Lcom/google/android/exoplayer2/audio/A$c;,
        Lcom/google/android/exoplayer2/audio/A$a;,
        Lcom/google/android/exoplayer2/audio/A$d;
    }
.end annotation


# static fields
.field public static a:Z

.field public static b:Z


# instance fields
.field private A:J

.field private B:J

.field private C:J

.field private D:J

.field private E:I

.field private F:Z

.field private G:Z

.field private H:J

.field private I:F

.field private J:[Lcom/google/android/exoplayer2/audio/r;

.field private K:[Ljava/nio/ByteBuffer;

.field private L:Ljava/nio/ByteBuffer;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private M:I

.field private N:Ljava/nio/ByteBuffer;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private O:[B

.field private P:I

.field private Q:I

.field private R:Z

.field private S:Z

.field private T:Z

.field private U:I

.field private V:Lcom/google/android/exoplayer2/audio/v;

.field private W:Z

.field private X:J

.field private Y:Z

.field private final c:Lcom/google/android/exoplayer2/audio/p;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final d:Lcom/google/android/exoplayer2/audio/A$a;

.field private final e:Z

.field private final f:Lcom/google/android/exoplayer2/audio/x;

.field private final g:Lcom/google/android/exoplayer2/audio/L;

.field private final h:[Lcom/google/android/exoplayer2/audio/r;

.field private final i:[Lcom/google/android/exoplayer2/audio/r;

.field private final j:Landroid/os/ConditionVariable;

.field private final k:Lcom/google/android/exoplayer2/audio/u;

.field private final l:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lcom/google/android/exoplayer2/audio/A$e;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Z

.field private final n:Z

.field private o:Lcom/google/android/exoplayer2/audio/A$g;

.field private p:Lcom/google/android/exoplayer2/audio/AudioSink$c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private q:Landroid/media/AudioTrack;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/google/android/exoplayer2/audio/A$b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/google/android/exoplayer2/audio/A$b;

.field private t:Landroid/media/AudioTrack;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/google/android/exoplayer2/audio/o;

.field private v:Lcom/google/android/exoplayer2/audio/A$e;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/google/android/exoplayer2/audio/A$e;

.field private x:Lcom/google/android/exoplayer2/ga;

.field private y:Ljava/nio/ByteBuffer;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private z:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/audio/p;Lcom/google/android/exoplayer2/audio/A$a;ZZZ)V
    .locals 10
    .param p1    # Lcom/google/android/exoplayer2/audio/p;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->c:Lcom/google/android/exoplayer2/audio/p;

    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object p1, p2

    check-cast p1, Lcom/google/android/exoplayer2/audio/A$a;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->d:Lcom/google/android/exoplayer2/audio/A$a;

    sget p1, Lcom/google/android/exoplayer2/util/E;->a:I

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x15

    if-lt p1, v2, :cond_0

    if-eqz p3, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_0
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/audio/A;->e:Z

    sget p1, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 p3, 0x17

    if-lt p1, p3, :cond_1

    if-eqz p4, :cond_1

    move p1, v0

    goto :goto_1

    :cond_1
    move p1, v1

    :goto_1
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/audio/A;->m:Z

    sget p1, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 p3, 0x1d

    if-lt p1, p3, :cond_2

    if-eqz p5, :cond_2

    move p1, v0

    goto :goto_2

    :cond_2
    move p1, v1

    :goto_2
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/audio/A;->n:Z

    new-instance p1, Landroid/os/ConditionVariable;

    invoke-direct {p1, v0}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->j:Landroid/os/ConditionVariable;

    new-instance p1, Lcom/google/android/exoplayer2/audio/u;

    new-instance p3, Lcom/google/android/exoplayer2/audio/A$f;

    const/4 p4, 0x0

    invoke-direct {p3, p0, p4}, Lcom/google/android/exoplayer2/audio/A$f;-><init>(Lcom/google/android/exoplayer2/audio/A;Lcom/google/android/exoplayer2/audio/y;)V

    invoke-direct {p1, p3}, Lcom/google/android/exoplayer2/audio/u;-><init>(Lcom/google/android/exoplayer2/audio/u$a;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    new-instance p1, Lcom/google/android/exoplayer2/audio/x;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/audio/x;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->f:Lcom/google/android/exoplayer2/audio/x;

    new-instance p1, Lcom/google/android/exoplayer2/audio/L;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/audio/L;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->g:Lcom/google/android/exoplayer2/audio/L;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    const/4 p3, 0x3

    new-array p3, p3, [Lcom/google/android/exoplayer2/audio/w;

    new-instance p4, Lcom/google/android/exoplayer2/audio/H;

    invoke-direct {p4}, Lcom/google/android/exoplayer2/audio/H;-><init>()V

    aput-object p4, p3, v1

    iget-object p4, p0, Lcom/google/android/exoplayer2/audio/A;->f:Lcom/google/android/exoplayer2/audio/x;

    aput-object p4, p3, v0

    const/4 p4, 0x2

    iget-object p5, p0, Lcom/google/android/exoplayer2/audio/A;->g:Lcom/google/android/exoplayer2/audio/L;

    aput-object p5, p3, p4

    invoke-static {p1, p3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-interface {p2}, Lcom/google/android/exoplayer2/audio/A$a;->b()[Lcom/google/android/exoplayer2/audio/r;

    move-result-object p2

    invoke-static {p1, p2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    new-array p2, v1, [Lcom/google/android/exoplayer2/audio/r;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/google/android/exoplayer2/audio/r;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->h:[Lcom/google/android/exoplayer2/audio/r;

    new-array p1, v0, [Lcom/google/android/exoplayer2/audio/r;

    new-instance p2, Lcom/google/android/exoplayer2/audio/C;

    invoke-direct {p2}, Lcom/google/android/exoplayer2/audio/C;-><init>()V

    aput-object p2, p1, v1

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->i:[Lcom/google/android/exoplayer2/audio/r;

    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Lcom/google/android/exoplayer2/audio/A;->I:F

    sget-object p1, Lcom/google/android/exoplayer2/audio/o;->a:Lcom/google/android/exoplayer2/audio/o;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->u:Lcom/google/android/exoplayer2/audio/o;

    iput v1, p0, Lcom/google/android/exoplayer2/audio/A;->U:I

    new-instance p1, Lcom/google/android/exoplayer2/audio/v;

    const/4 p2, 0x0

    invoke-direct {p1, v1, p2}, Lcom/google/android/exoplayer2/audio/v;-><init>(IF)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->V:Lcom/google/android/exoplayer2/audio/v;

    new-instance p1, Lcom/google/android/exoplayer2/audio/A$e;

    sget-object v3, Lcom/google/android/exoplayer2/ga;->a:Lcom/google/android/exoplayer2/ga;

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    move-object v2, p1

    invoke-direct/range {v2 .. v9}, Lcom/google/android/exoplayer2/audio/A$e;-><init>(Lcom/google/android/exoplayer2/ga;ZJJLcom/google/android/exoplayer2/audio/y;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->w:Lcom/google/android/exoplayer2/audio/A$e;

    sget-object p1, Lcom/google/android/exoplayer2/ga;->a:Lcom/google/android/exoplayer2/ga;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->x:Lcom/google/android/exoplayer2/ga;

    const/4 p1, -0x1

    iput p1, p0, Lcom/google/android/exoplayer2/audio/A;->Q:I

    new-array p1, v1, [Lcom/google/android/exoplayer2/audio/r;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->J:[Lcom/google/android/exoplayer2/audio/r;

    new-array p1, v1, [Ljava/nio/ByteBuffer;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->K:[Ljava/nio/ByteBuffer;

    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->l:Ljava/util/ArrayDeque;

    return-void
.end method

.method private static a(ILjava/nio/ByteBuffer;)I
    .locals 2

    const/16 v0, 0x400

    const/4 v1, -0x1

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const/16 v0, 0x26

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Unexpected audio encoding: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_1
    invoke-static {p1}, Lcom/google/android/exoplayer2/audio/m;->a(Ljava/nio/ByteBuffer;)I

    move-result p0

    return p0

    :pswitch_2
    return v0

    :pswitch_3
    const/16 p0, 0x200

    return p0

    :pswitch_4
    invoke-static {p1}, Lcom/google/android/exoplayer2/audio/Ac3Util;->a(Ljava/nio/ByteBuffer;)I

    move-result p0

    if-ne p0, v1, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p1, p0}, Lcom/google/android/exoplayer2/audio/Ac3Util;->a(Ljava/nio/ByteBuffer;I)I

    move-result p0

    mul-int/lit8 p0, p0, 0x10

    :goto_0
    return p0

    :pswitch_5
    const/16 p0, 0x800

    return p0

    :pswitch_6
    return v0

    :pswitch_7
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result p0

    invoke-static {p1, p0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/nio/ByteBuffer;I)I

    move-result p0

    invoke-static {p0}, Lcom/google/android/exoplayer2/audio/F;->c(I)I

    move-result p0

    if-eq p0, v1, :cond_1

    return p0

    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0

    :pswitch_8
    invoke-static {p1}, Lcom/google/android/exoplayer2/audio/B;->a(Ljava/nio/ByteBuffer;)I

    move-result p0

    return p0

    :pswitch_9
    invoke-static {p1}, Lcom/google/android/exoplayer2/audio/Ac3Util;->b(Ljava/nio/ByteBuffer;)I

    move-result p0

    return p0

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_9
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_9
    .end packed-switch
.end method

.method private static a(Landroid/media/AudioTrack;Ljava/nio/ByteBuffer;I)I
    .locals 1
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x15
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    move-result p0

    return p0
.end method

.method private a(Landroid/media/AudioTrack;Ljava/nio/ByteBuffer;IJ)I
    .locals 10
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x15
    .end annotation

    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const-wide/16 v1, 0x3e8

    const/16 v3, 0x1a

    if-lt v0, v3, :cond_0

    const/4 v7, 0x1

    mul-long v8, p4, v1

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    invoke-virtual/range {v4 .. v9}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;IIJ)I

    move-result p1

    return p1

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->y:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_1

    const/16 v0, 0x10

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->y:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->y:Ljava/nio/ByteBuffer;

    sget-object v3, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->y:Ljava/nio/ByteBuffer;

    const v3, 0x55550001

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer2/audio/A;->z:I

    const/4 v3, 0x0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->y:Ljava/nio/ByteBuffer;

    const/4 v4, 0x4

    invoke-virtual {v0, v4, p3}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->y:Ljava/nio/ByteBuffer;

    const/16 v4, 0x8

    mul-long/2addr p4, v1

    invoke-virtual {v0, v4, p4, p5}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    iget-object p4, p0, Lcom/google/android/exoplayer2/audio/A;->y:Ljava/nio/ByteBuffer;

    invoke-virtual {p4, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iput p3, p0, Lcom/google/android/exoplayer2/audio/A;->z:I

    :cond_2
    iget-object p4, p0, Lcom/google/android/exoplayer2/audio/A;->y:Ljava/nio/ByteBuffer;

    invoke-virtual {p4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result p4

    if-lez p4, :cond_4

    iget-object p5, p0, Lcom/google/android/exoplayer2/audio/A;->y:Ljava/nio/ByteBuffer;

    const/4 v0, 0x1

    invoke-virtual {p1, p5, p4, v0}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    move-result p5

    if-gez p5, :cond_3

    iput v3, p0, Lcom/google/android/exoplayer2/audio/A;->z:I

    return p5

    :cond_3
    if-ge p5, p4, :cond_4

    return v3

    :cond_4
    invoke-static {p1, p2, p3}, Lcom/google/android/exoplayer2/audio/A;->a(Landroid/media/AudioTrack;Ljava/nio/ByteBuffer;I)I

    move-result p1

    if-gez p1, :cond_5

    iput v3, p0, Lcom/google/android/exoplayer2/audio/A;->z:I

    return p1

    :cond_5
    iget p2, p0, Lcom/google/android/exoplayer2/audio/A;->z:I

    sub-int/2addr p2, p1

    iput p2, p0, Lcom/google/android/exoplayer2/audio/A;->z:I

    return p1
.end method

.method static synthetic a(III)Landroid/media/AudioFormat;
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/exoplayer2/audio/A;->b(III)Landroid/media/AudioFormat;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/audio/A;)Landroid/os/ConditionVariable;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/audio/A;->j:Landroid/os/ConditionVariable;

    return-object p0
.end method

.method private static a(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/audio/p;)Landroid/util/Pair;
    .locals 5
    .param p1    # Lcom/google/android/exoplayer2/audio/p;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/Format;",
            "Lcom/google/android/exoplayer2/audio/p;",
            ")",
            "Landroid/util/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/exoplayer2/Format;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/exoplayer2/util/q;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x5

    const/16 v3, 0x12

    const/4 v4, 0x6

    if-eq v1, v2, :cond_2

    if-eq v1, v4, :cond_2

    if-eq v1, v3, :cond_2

    const/16 v2, 0x11

    if-eq v1, v2, :cond_2

    const/4 v2, 0x7

    if-eq v1, v2, :cond_2

    const/16 v2, 0x8

    if-eq v1, v2, :cond_2

    const/16 v2, 0xe

    if-ne v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_3

    return-object v0

    :cond_3
    if-ne v1, v3, :cond_4

    move p0, v4

    goto :goto_2

    :cond_4
    iget p0, p0, Lcom/google/android/exoplayer2/Format;->y:I

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/audio/p;->a()I

    move-result v2

    if-le p0, v2, :cond_5

    return-object v0

    :cond_5
    invoke-static {p0}, Lcom/google/android/exoplayer2/audio/A;->d(I)I

    move-result p0

    if-nez p0, :cond_6

    return-object v0

    :cond_6
    invoke-virtual {p1, v1}, Lcom/google/android/exoplayer2/audio/p;->a(I)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-static {p1, p0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p0

    return-object p0

    :cond_7
    if-ne v1, v3, :cond_8

    invoke-virtual {p1, v4}, Lcom/google/android/exoplayer2/audio/p;->a(I)Z

    move-result p1

    if-eqz p1, :cond_8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-static {p1, p0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p0

    return-object p0

    :cond_8
    return-object v0
.end method

.method private a(J)V
    .locals 11

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/audio/A$b;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->d:Lcom/google/android/exoplayer2/audio/A$a;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->l()Lcom/google/android/exoplayer2/ga;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/audio/A$a;->a(Lcom/google/android/exoplayer2/ga;)Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/exoplayer2/ga;->a:Lcom/google/android/exoplayer2/ga;

    :goto_0
    move-object v2, v0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/audio/A$b;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->d:Lcom/google/android/exoplayer2/audio/A$a;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/A;->h()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/audio/A$a;->a(Z)Z

    move v0, v1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v9, p0, Lcom/google/android/exoplayer2/audio/A;->l:Ljava/util/ArrayDeque;

    new-instance v10, Lcom/google/android/exoplayer2/audio/A$e;

    const-wide/16 v3, 0x0

    invoke-static {v3, v4, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iget-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->o()J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Lcom/google/android/exoplayer2/audio/A$b;->b(J)J

    move-result-wide v6

    const/4 v8, 0x0

    move-object v1, v10

    move v3, v0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/exoplayer2/audio/A$e;-><init>(Lcom/google/android/exoplayer2/ga;ZJJLcom/google/android/exoplayer2/audio/y;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->x()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->p:Lcom/google/android/exoplayer2/audio/AudioSink$c;

    if-eqz p1, :cond_2

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/audio/AudioSink$c;->f(Z)V

    :cond_2
    return-void
.end method

.method private static a(Landroid/media/AudioTrack;F)V
    .locals 0
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x15
    .end annotation

    invoke-virtual {p0, p1}, Landroid/media/AudioTrack;->setVolume(F)I

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/ga;Z)V
    .locals 9

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->m()Lcom/google/android/exoplayer2/audio/A$e;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/exoplayer2/audio/A$e;->a:Lcom/google/android/exoplayer2/ga;

    invoke-virtual {p1, v1}, Lcom/google/android/exoplayer2/ga;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/audio/A$e;->b:Z

    if-eq p2, v0, :cond_2

    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/audio/A$e;

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v8, 0x0

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v1 .. v8}, Lcom/google/android/exoplayer2/audio/A$e;-><init>(Lcom/google/android/exoplayer2/ga;ZJJLcom/google/android/exoplayer2/audio/y;)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result p1

    if-eqz p1, :cond_1

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->v:Lcom/google/android/exoplayer2/audio/A$e;

    goto :goto_0

    :cond_1
    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->w:Lcom/google/android/exoplayer2/audio/A$e;

    :cond_2
    :goto_0
    return-void
.end method

.method private a(Ljava/nio/ByteBuffer;J)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$d;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->N:Ljava/nio/ByteBuffer;

    const/16 v1, 0x15

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    if-ne v0, p1, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v3

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    goto :goto_1

    :cond_2
    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->N:Ljava/nio/ByteBuffer;

    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    if-ge v0, v1, :cond_5

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/exoplayer2/audio/A;->O:[B

    if-eqz v4, :cond_3

    array-length v4, v4

    if-ge v4, v0, :cond_4

    :cond_3
    new-array v4, v0, [B

    iput-object v4, p0, Lcom/google/android/exoplayer2/audio/A;->O:[B

    :cond_4
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/exoplayer2/audio/A;->O:[B

    invoke-virtual {p1, v5, v3, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iput v3, p0, Lcom/google/android/exoplayer2/audio/A;->P:I

    :cond_5
    :goto_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    sget v4, Lcom/google/android/exoplayer2/util/E;->a:I

    if-ge v4, v1, :cond_7

    iget-object p2, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/audio/A;->C:J

    invoke-virtual {p2, v4, v5}, Lcom/google/android/exoplayer2/audio/u;->a(J)I

    move-result p2

    if-lez p2, :cond_6

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object p3, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->O:[B

    iget v4, p0, Lcom/google/android/exoplayer2/audio/A;->P:I

    invoke-virtual {p3, v1, v4, p2}, Landroid/media/AudioTrack;->write([BII)I

    move-result p2

    if-lez p2, :cond_a

    iget p3, p0, Lcom/google/android/exoplayer2/audio/A;->P:I

    add-int/2addr p3, p2

    iput p3, p0, Lcom/google/android/exoplayer2/audio/A;->P:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result p3

    add-int/2addr p3, p2

    invoke-virtual {p1, p3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_3

    :cond_6
    move p2, v3

    goto :goto_3

    :cond_7
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/audio/A;->W:Z

    if-eqz v1, :cond_9

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, p2, v4

    if-eqz v1, :cond_8

    move v1, v2

    goto :goto_2

    :cond_8
    move v1, v3

    :goto_2
    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-object v7, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    move-object v6, p0

    move-object v8, p1

    move v9, v0

    move-wide v10, p2

    invoke-direct/range {v6 .. v11}, Lcom/google/android/exoplayer2/audio/A;->a(Landroid/media/AudioTrack;Ljava/nio/ByteBuffer;IJ)I

    move-result p2

    goto :goto_3

    :cond_9
    iget-object p2, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-static {p2, p1, v0}, Lcom/google/android/exoplayer2/audio/A;->a(Landroid/media/AudioTrack;Ljava/nio/ByteBuffer;I)I

    move-result p2

    :cond_a
    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/exoplayer2/audio/A;->X:J

    if-gez p2, :cond_c

    invoke-static {p2}, Lcom/google/android/exoplayer2/audio/A;->g(I)Z

    move-result p1

    if-eqz p1, :cond_b

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->s()V

    :cond_b
    new-instance p1, Lcom/google/android/exoplayer2/audio/AudioSink$d;

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/audio/AudioSink$d;-><init>(I)V

    throw p1

    :cond_c
    iget-boolean p3, p0, Lcom/google/android/exoplayer2/audio/A;->T:Z

    if-eqz p3, :cond_d

    iget-object p3, p0, Lcom/google/android/exoplayer2/audio/A;->p:Lcom/google/android/exoplayer2/audio/AudioSink$c;

    if-eqz p3, :cond_d

    if-ge p2, v0, :cond_d

    iget-object p3, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-static {p3}, Lcom/google/android/exoplayer2/audio/A;->a(Landroid/media/AudioTrack;)Z

    move-result p3

    if-eqz p3, :cond_d

    iget-object p3, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/audio/A;->D:J

    invoke-virtual {p3, v4, v5}, Lcom/google/android/exoplayer2/audio/u;->b(J)J

    move-result-wide v4

    iget-object p3, p0, Lcom/google/android/exoplayer2/audio/A;->p:Lcom/google/android/exoplayer2/audio/AudioSink$c;

    invoke-interface {p3, v4, v5}, Lcom/google/android/exoplayer2/audio/AudioSink$c;->b(J)V

    :cond_d
    iget-object p3, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget p3, p3, Lcom/google/android/exoplayer2/audio/A$b;->c:I

    if-nez p3, :cond_e

    iget-wide v4, p0, Lcom/google/android/exoplayer2/audio/A;->C:J

    int-to-long v6, p2

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/exoplayer2/audio/A;->C:J

    :cond_e
    if-ne p2, v0, :cond_11

    iget-object p2, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget p2, p2, Lcom/google/android/exoplayer2/audio/A$b;->c:I

    if-eqz p2, :cond_10

    iget-object p2, p0, Lcom/google/android/exoplayer2/audio/A;->L:Ljava/nio/ByteBuffer;

    if-ne p1, p2, :cond_f

    goto :goto_4

    :cond_f
    move v2, v3

    :goto_4
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-wide p1, p0, Lcom/google/android/exoplayer2/audio/A;->D:J

    iget p3, p0, Lcom/google/android/exoplayer2/audio/A;->E:I

    iget v0, p0, Lcom/google/android/exoplayer2/audio/A;->M:I

    mul-int/2addr p3, v0

    int-to-long v0, p3

    add-long/2addr p1, v0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/audio/A;->D:J

    :cond_10
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->N:Ljava/nio/ByteBuffer;

    :cond_11
    return-void
.end method

.method private static a(Landroid/media/AudioTrack;)Z
    .locals 2

    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/media/AudioTrack;->isOffloadedPlayback()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static a(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/audio/o;)Z
    .locals 4

    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const/4 v1, 0x0

    const/16 v2, 0x1d

    if-ge v0, v2, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/exoplayer2/Format;->i:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/exoplayer2/util/q;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    iget v2, p0, Lcom/google/android/exoplayer2/Format;->y:I

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/E;->b(I)I

    move-result v2

    if-nez v2, :cond_2

    return v1

    :cond_2
    iget v3, p0, Lcom/google/android/exoplayer2/Format;->z:I

    invoke-static {v3, v2, v0}, Lcom/google/android/exoplayer2/audio/A;->b(III)Landroid/media/AudioFormat;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/audio/o;->a()Landroid/media/AudioAttributes;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/media/AudioManager;->isOffloadedPlaybackSupported(Landroid/media/AudioFormat;Landroid/media/AudioAttributes;)Z

    move-result p1

    if-nez p1, :cond_3

    return v1

    :cond_3
    iget p1, p0, Lcom/google/android/exoplayer2/Format;->B:I

    const/4 v0, 0x1

    if-nez p1, :cond_4

    iget p0, p0, Lcom/google/android/exoplayer2/Format;->C:I

    if-nez p0, :cond_4

    move p0, v0

    goto :goto_0

    :cond_4
    move p0, v1

    :goto_0
    if-nez p0, :cond_6

    invoke-static {}, Lcom/google/android/exoplayer2/audio/A;->r()Z

    move-result p0

    if-eqz p0, :cond_5

    goto :goto_1

    :cond_5
    move v0, v1

    :cond_6
    :goto_1
    return v0
.end method

.method private b(J)J
    .locals 3

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->l:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->l:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/audio/A$e;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/audio/A$e;->d:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->l:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/audio/A$e;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->w:Lcom/google/android/exoplayer2/audio/A$e;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->w:Lcom/google/android/exoplayer2/audio/A$e;

    iget-wide v1, v0, Lcom/google/android/exoplayer2/audio/A$e;->d:J

    sub-long/2addr p1, v1

    iget-object v0, v0, Lcom/google/android/exoplayer2/audio/A$e;->a:Lcom/google/android/exoplayer2/ga;

    sget-object v1, Lcom/google/android/exoplayer2/ga;->a:Lcom/google/android/exoplayer2/ga;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ga;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->l:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->d:Lcom/google/android/exoplayer2/audio/A$a;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/audio/A$a;->a(J)J

    move-result-wide p1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->w:Lcom/google/android/exoplayer2/audio/A$e;

    iget-object v0, v0, Lcom/google/android/exoplayer2/audio/A$e;->a:Lcom/google/android/exoplayer2/ga;

    iget v0, v0, Lcom/google/android/exoplayer2/ga;->b:F

    invoke-static {p1, p2, v0}, Lcom/google/android/exoplayer2/util/E;->a(JF)J

    move-result-wide p1

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->w:Lcom/google/android/exoplayer2/audio/A$e;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/audio/A$e;->c:J

    add-long/2addr v0, p1

    return-wide v0
.end method

.method private static b(III)Landroid/media/AudioFormat;
    .locals 1
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x15
    .end annotation

    new-instance v0, Landroid/media/AudioFormat$Builder;

    invoke-direct {v0}, Landroid/media/AudioFormat$Builder;-><init>()V

    invoke-virtual {v0, p0}, Landroid/media/AudioFormat$Builder;->setSampleRate(I)Landroid/media/AudioFormat$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/media/AudioFormat$Builder;->setChannelMask(I)Landroid/media/AudioFormat$Builder;

    move-result-object p0

    invoke-virtual {p0, p2}, Landroid/media/AudioFormat$Builder;->setEncoding(I)Landroid/media/AudioFormat$Builder;

    move-result-object p0

    invoke-virtual {p0}, Landroid/media/AudioFormat$Builder;->build()Landroid/media/AudioFormat;

    move-result-object p0

    return-object p0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/audio/A;)Landroid/media/AudioTrack;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    return-object p0
.end method

.method private b(Landroid/media/AudioTrack;)V
    .locals 1
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x1d
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->o:Lcom/google/android/exoplayer2/audio/A$g;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/exoplayer2/audio/A$g;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/audio/A$g;-><init>(Lcom/google/android/exoplayer2/audio/A;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->o:Lcom/google/android/exoplayer2/audio/A$g;

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->o:Lcom/google/android/exoplayer2/audio/A$g;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/audio/A$g;->a(Landroid/media/AudioTrack;)V

    return-void
.end method

.method private static b(Landroid/media/AudioTrack;F)V
    .locals 0

    invoke-virtual {p0, p1, p1}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/ga;)V
    .locals 2
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x17
    .end annotation

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/media/PlaybackParams;

    invoke-direct {v0}, Landroid/media/PlaybackParams;-><init>()V

    invoke-virtual {v0}, Landroid/media/PlaybackParams;->allowDefaults()Landroid/media/PlaybackParams;

    move-result-object v0

    iget v1, p1, Lcom/google/android/exoplayer2/ga;->b:F

    invoke-virtual {v0, v1}, Landroid/media/PlaybackParams;->setSpeed(F)Landroid/media/PlaybackParams;

    move-result-object v0

    iget p1, p1, Lcom/google/android/exoplayer2/ga;->c:F

    invoke-virtual {v0, p1}, Landroid/media/PlaybackParams;->setPitch(F)Landroid/media/PlaybackParams;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/media/PlaybackParams;->setAudioFallbackMode(I)Landroid/media/PlaybackParams;

    move-result-object p1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v0, p1}, Landroid/media/AudioTrack;->setPlaybackParams(Landroid/media/PlaybackParams;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "AudioTrack"

    const-string v1, "Failed to set playback params"

    invoke-static {v0, v1, p1}, Lcom/google/android/exoplayer2/util/n;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    new-instance p1, Lcom/google/android/exoplayer2/ga;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlaybackParams()Landroid/media/PlaybackParams;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/PlaybackParams;->getSpeed()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->getPlaybackParams()Landroid/media/PlaybackParams;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/PlaybackParams;->getPitch()F

    move-result v1

    invoke-direct {p1, v0, v1}, Lcom/google/android/exoplayer2/ga;-><init>(FF)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    iget v1, p1, Lcom/google/android/exoplayer2/ga;->b:F

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/audio/u;->a(F)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->x:Lcom/google/android/exoplayer2/ga;

    return-void
.end method

.method private static b(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/audio/p;)Z
    .locals 0
    .param p1    # Lcom/google/android/exoplayer2/audio/p;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-static {p0, p1}, Lcom/google/android/exoplayer2/audio/A;->a(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/audio/p;)Landroid/util/Pair;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic c(I)I
    .locals 0

    invoke-static {p0}, Lcom/google/android/exoplayer2/audio/A;->e(I)I

    move-result p0

    return p0
.end method

.method private c(J)J
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->d:Lcom/google/android/exoplayer2/audio/A$a;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/audio/A$a;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/audio/A$b;->b(J)J

    move-result-wide v0

    add-long/2addr p1, v0

    return-wide p1
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/audio/A;)Lcom/google/android/exoplayer2/audio/AudioSink$c;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/audio/A;->p:Lcom/google/android/exoplayer2/audio/AudioSink$c;

    return-object p0
.end method

.method private static d(I)I
    .locals 2

    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v1, 0x1c

    if-gt v0, v1, :cond_2

    const/4 v0, 0x7

    if-ne p0, v0, :cond_0

    const/16 p0, 0x8

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-ne p0, v0, :cond_2

    :cond_1
    const/4 p0, 0x6

    :cond_2
    :goto_0
    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v1, 0x1a

    if-gt v0, v1, :cond_3

    sget-object v0, Lcom/google/android/exoplayer2/util/E;->b:Ljava/lang/String;

    const-string v1, "fugu"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    if-ne p0, v0, :cond_3

    const/4 p0, 0x2

    :cond_3
    invoke-static {p0}, Lcom/google/android/exoplayer2/util/E;->b(I)I

    move-result p0

    return p0
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/audio/A;)J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->n()J

    move-result-wide v0

    return-wide v0
.end method

.method private d(J)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$d;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->J:[Lcom/google/android/exoplayer2/audio/r;

    array-length v0, v0

    move v1, v0

    :goto_0
    if-ltz v1, :cond_5

    if-lez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/A;->K:[Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v1, -0x1

    aget-object v2, v2, v3

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/A;->L:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    sget-object v2, Lcom/google/android/exoplayer2/audio/r;->a:Ljava/nio/ByteBuffer;

    :goto_1
    if-ne v1, v0, :cond_2

    invoke-direct {p0, v2, p1, p2}, Lcom/google/android/exoplayer2/audio/A;->a(Ljava/nio/ByteBuffer;J)V

    goto :goto_2

    :cond_2
    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/A;->J:[Lcom/google/android/exoplayer2/audio/r;

    aget-object v3, v3, v1

    invoke-interface {v3, v2}, Lcom/google/android/exoplayer2/audio/r;->a(Ljava/nio/ByteBuffer;)V

    invoke-interface {v3}, Lcom/google/android/exoplayer2/audio/r;->a()Ljava/nio/ByteBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/exoplayer2/audio/A;->K:[Ljava/nio/ByteBuffer;

    aput-object v3, v4, v1

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    :goto_2
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-eqz v2, :cond_4

    return-void

    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_5
    return-void
.end method

.method private static e(I)I
    .locals 0

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0

    :pswitch_1
    const p0, 0x52080

    return p0

    :pswitch_2
    const p0, 0x3e800

    return p0

    :pswitch_3
    const/16 p0, 0x1f40

    return p0

    :pswitch_4
    const p0, 0x2ebae4

    return p0

    :pswitch_5
    const/16 p0, 0x1b58

    return p0

    :pswitch_6
    const/16 p0, 0x3e80

    return p0

    :pswitch_7
    const p0, 0x186a0

    return p0

    :pswitch_8
    const p0, 0x9c40

    return p0

    :pswitch_9
    const p0, 0x225510

    return p0

    :pswitch_a
    const p0, 0x2ee00

    return p0

    :pswitch_b
    const p0, 0xbb800

    return p0

    :pswitch_c
    const p0, 0x13880

    return p0

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_b
    .end packed-switch
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/audio/A;)J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->o()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic f(Lcom/google/android/exoplayer2/audio/A;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/audio/A;->X:J

    return-wide v0
.end method

.method private static f(I)Landroid/media/AudioTrack;
    .locals 9

    new-instance v8, Landroid/media/AudioTrack;

    const/16 v2, 0xfa0

    const/4 v3, 0x4

    const/4 v4, 0x2

    const/4 v5, 0x2

    const/4 v1, 0x3

    const/4 v6, 0x0

    move-object v0, v8

    move v7, p0

    invoke-direct/range {v0 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    return-object v8
.end method

.method private static g(I)Z
    .locals 2

    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    const/4 v0, -0x6

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private i()Landroid/media/AudioTrack;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$b;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/audio/A$b;

    iget-boolean v1, p0, Lcom/google/android/exoplayer2/audio/A;->W:Z

    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/A;->u:Lcom/google/android/exoplayer2/audio/o;

    iget v3, p0, Lcom/google/android/exoplayer2/audio/A;->U:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/audio/A$b;->a(ZLcom/google/android/exoplayer2/audio/o;I)Landroid/media/AudioTrack;

    move-result-object v0
    :try_end_0
    .catch Lcom/google/android/exoplayer2/audio/AudioSink$b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->s()V

    throw v0
.end method

.method private j()Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$d;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/exoplayer2/audio/A;->Q:I

    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    iput v3, p0, Lcom/google/android/exoplayer2/audio/A;->Q:I

    :goto_0
    move v0, v2

    goto :goto_1

    :cond_0
    move v0, v3

    :goto_1
    iget v4, p0, Lcom/google/android/exoplayer2/audio/A;->Q:I

    iget-object v5, p0, Lcom/google/android/exoplayer2/audio/A;->J:[Lcom/google/android/exoplayer2/audio/r;

    array-length v6, v5

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    if-ge v4, v6, :cond_3

    aget-object v4, v5, v4

    if-eqz v0, :cond_1

    invoke-interface {v4}, Lcom/google/android/exoplayer2/audio/r;->c()V

    :cond_1
    invoke-direct {p0, v7, v8}, Lcom/google/android/exoplayer2/audio/A;->d(J)V

    invoke-interface {v4}, Lcom/google/android/exoplayer2/audio/r;->b()Z

    move-result v0

    if-nez v0, :cond_2

    return v3

    :cond_2
    iget v0, p0, Lcom/google/android/exoplayer2/audio/A;->Q:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/exoplayer2/audio/A;->Q:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->N:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_4

    invoke-direct {p0, v0, v7, v8}, Lcom/google/android/exoplayer2/audio/A;->a(Ljava/nio/ByteBuffer;J)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->N:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_4

    return v3

    :cond_4
    iput v1, p0, Lcom/google/android/exoplayer2/audio/A;->Q:I

    return v2
.end method

.method private k()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->J:[Lcom/google/android/exoplayer2/audio/r;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/exoplayer2/audio/r;->flush()V

    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/A;->K:[Ljava/nio/ByteBuffer;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/audio/r;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private l()Lcom/google/android/exoplayer2/ga;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->m()Lcom/google/android/exoplayer2/audio/A$e;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/exoplayer2/audio/A$e;->a:Lcom/google/android/exoplayer2/ga;

    return-object v0
.end method

.method private m()Lcom/google/android/exoplayer2/audio/A$e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->v:Lcom/google/android/exoplayer2/audio/A$e;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->l:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->l:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/audio/A$e;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->w:Lcom/google/android/exoplayer2/audio/A$e;

    :goto_0
    return-object v0
.end method

.method private n()J
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget v1, v0, Lcom/google/android/exoplayer2/audio/A$b;->c:I

    if-nez v1, :cond_0

    iget-wide v1, p0, Lcom/google/android/exoplayer2/audio/A;->A:J

    iget v0, v0, Lcom/google/android/exoplayer2/audio/A$b;->b:I

    int-to-long v3, v0

    div-long/2addr v1, v3

    goto :goto_0

    :cond_0
    iget-wide v1, p0, Lcom/google/android/exoplayer2/audio/A;->B:J

    :goto_0
    return-wide v1
.end method

.method private o()J
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget v1, v0, Lcom/google/android/exoplayer2/audio/A$b;->c:I

    if-nez v1, :cond_0

    iget-wide v1, p0, Lcom/google/android/exoplayer2/audio/A;->C:J

    iget v0, v0, Lcom/google/android/exoplayer2/audio/A$b;->d:I

    int-to-long v3, v0

    div-long/2addr v1, v3

    goto :goto_0

    :cond_0
    iget-wide v1, p0, Lcom/google/android/exoplayer2/audio/A;->D:J

    :goto_0
    return-wide v1
.end method

.method private p()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$b;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->j:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->i()Landroid/media/AudioTrack;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-static {v0}, Lcom/google/android/exoplayer2/audio/A;->a(Landroid/media/AudioTrack;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/audio/A;->b(Landroid/media/AudioTrack;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget-object v1, v1, Lcom/google/android/exoplayer2/audio/A$b;->a:Lcom/google/android/exoplayer2/Format;

    iget v2, v1, Lcom/google/android/exoplayer2/Format;->B:I

    iget v1, v1, Lcom/google/android/exoplayer2/Format;->C:I

    invoke-virtual {v0, v2, v1}, Landroid/media/AudioTrack;->setOffloadDelayPadding(II)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v0

    sget-boolean v1, Lcom/google/android/exoplayer2/audio/A;->a:Z

    if-eqz v1, :cond_2

    sget v1, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->q:Landroid/media/AudioTrack;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v1

    if-eq v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->u()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->q:Landroid/media/AudioTrack;

    if-nez v1, :cond_2

    invoke-static {v0}, Lcom/google/android/exoplayer2/audio/A;->f(I)Landroid/media/AudioTrack;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->q:Landroid/media/AudioTrack;

    :cond_2
    iget v1, p0, Lcom/google/android/exoplayer2/audio/A;->U:I

    if-eq v1, v0, :cond_3

    iput v0, p0, Lcom/google/android/exoplayer2/audio/A;->U:I

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->p:Lcom/google/android/exoplayer2/audio/AudioSink$c;

    if-eqz v1, :cond_3

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/audio/AudioSink$c;->d(I)V

    :cond_3
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget v0, v0, Lcom/google/android/exoplayer2/audio/A$b;->c:I

    const/4 v1, 0x2

    const/4 v8, 0x1

    if-ne v0, v1, :cond_4

    move v4, v8

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget v5, v0, Lcom/google/android/exoplayer2/audio/A$b;->g:I

    iget v6, v0, Lcom/google/android/exoplayer2/audio/A$b;->d:I

    iget v7, v0, Lcom/google/android/exoplayer2/audio/A$b;->h:I

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/exoplayer2/audio/u;->a(Landroid/media/AudioTrack;ZIII)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->w()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->V:Lcom/google/android/exoplayer2/audio/v;

    iget v0, v0, Lcom/google/android/exoplayer2/audio/v;->a:I

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v1, v0}, Landroid/media/AudioTrack;->attachAuxEffect(I)I

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->V:Lcom/google/android/exoplayer2/audio/v;

    iget v1, v1, Lcom/google/android/exoplayer2/audio/v;->b:F

    invoke-virtual {v0, v1}, Landroid/media/AudioTrack;->setAuxEffectSendLevel(F)I

    :cond_5
    iput-boolean v8, p0, Lcom/google/android/exoplayer2/audio/A;->G:Z

    return-void
.end method

.method private q()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static r()Z
    .locals 2

    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/google/android/exoplayer2/util/E;->d:Ljava/lang/String;

    const-string v1, "Pixel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private s()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/A$b;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->Y:Z

    return-void
.end method

.method private t()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->S:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->S:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/audio/u;->c(J)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/audio/A;->z:I

    :cond_0
    return-void
.end method

.method private u()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->q:Landroid/media/AudioTrack;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->q:Landroid/media/AudioTrack;

    new-instance v1, Lcom/google/android/exoplayer2/audio/z;

    invoke-direct {v1, p0, v0}, Lcom/google/android/exoplayer2/audio/z;-><init>(Lcom/google/android/exoplayer2/audio/A;Landroid/media/AudioTrack;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private v()V
    .locals 12

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/A;->A:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/A;->B:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/A;->C:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/A;->D:J

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/exoplayer2/audio/A;->E:I

    new-instance v11, Lcom/google/android/exoplayer2/audio/A$e;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->l()Lcom/google/android/exoplayer2/ga;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/A;->h()Z

    move-result v5

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    move-object v3, v11

    invoke-direct/range {v3 .. v10}, Lcom/google/android/exoplayer2/audio/A$e;-><init>(Lcom/google/android/exoplayer2/ga;ZJJLcom/google/android/exoplayer2/audio/y;)V

    iput-object v11, p0, Lcom/google/android/exoplayer2/audio/A;->w:Lcom/google/android/exoplayer2/audio/A$e;

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/A;->H:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->v:Lcom/google/android/exoplayer2/audio/A$e;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->l:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->clear()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->L:Ljava/nio/ByteBuffer;

    iput v2, p0, Lcom/google/android/exoplayer2/audio/A;->M:I

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->N:Ljava/nio/ByteBuffer;

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/audio/A;->S:Z

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/audio/A;->R:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/exoplayer2/audio/A;->Q:I

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->y:Ljava/nio/ByteBuffer;

    iput v2, p0, Lcom/google/android/exoplayer2/audio/A;->z:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->g:Lcom/google/android/exoplayer2/audio/L;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/L;->i()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->k()V

    return-void
.end method

.method private w()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/android/exoplayer2/audio/A;->I:F

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/audio/A;->a(Landroid/media/AudioTrack;F)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/android/exoplayer2/audio/A;->I:F

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/audio/A;->b(Landroid/media/AudioTrack;F)V

    :goto_0
    return-void
.end method

.method private x()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/audio/A$b;->j:[Lcom/google/android/exoplayer2/audio/r;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    invoke-interface {v4}, Lcom/google/android/exoplayer2/audio/r;->isActive()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    invoke-interface {v4}, Lcom/google/android/exoplayer2/audio/r;->flush()V

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Lcom/google/android/exoplayer2/audio/r;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/exoplayer2/audio/r;

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->J:[Lcom/google/android/exoplayer2/audio/r;

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->K:[Ljava/nio/ByteBuffer;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->k()V

    return-void
.end method


# virtual methods
.method public a(Z)J
    .locals 4

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->G:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/audio/u;->a(Z)J

    move-result-wide v0

    iget-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->o()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/exoplayer2/audio/A$b;->b(J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/audio/A;->b(J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/audio/A;->c(J)J

    move-result-wide v0

    return-wide v0

    :cond_1
    :goto_0
    const-wide/high16 v0, -0x8000000000000000L

    return-wide v0
.end method

.method public a()Lcom/google/android/exoplayer2/ga;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->x:Lcom/google/android/exoplayer2/ga;

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->l()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public a(F)V
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/audio/A;->I:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/google/android/exoplayer2/audio/A;->I:F

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->w()V

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/audio/A;->U:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/exoplayer2/audio/A;->U:I

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/A;->flush()V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/Format;I[I)V
    .locals 15
    .param p3    # [I
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$a;
        }
    .end annotation

    move-object v1, p0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    const-string v3, "audio/raw"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v2, :cond_6

    iget v2, v0, Lcom/google/android/exoplayer2/Format;->A:I

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/E;->g(I)Z

    move-result v2

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    iget v2, v0, Lcom/google/android/exoplayer2/Format;->A:I

    iget v3, v0, Lcom/google/android/exoplayer2/Format;->y:I

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/util/E;->b(II)I

    move-result v2

    iget-boolean v3, v1, Lcom/google/android/exoplayer2/audio/A;->e:Z

    if-eqz v3, :cond_0

    iget v3, v0, Lcom/google/android/exoplayer2/Format;->A:I

    invoke-static {v3}, Lcom/google/android/exoplayer2/util/E;->f(I)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    goto :goto_0

    :cond_0
    move v3, v5

    :goto_0
    if-eqz v3, :cond_1

    iget-object v6, v1, Lcom/google/android/exoplayer2/audio/A;->i:[Lcom/google/android/exoplayer2/audio/r;

    goto :goto_1

    :cond_1
    iget-object v6, v1, Lcom/google/android/exoplayer2/audio/A;->h:[Lcom/google/android/exoplayer2/audio/r;

    :goto_1
    xor-int/2addr v3, v4

    iget-object v4, v1, Lcom/google/android/exoplayer2/audio/A;->g:Lcom/google/android/exoplayer2/audio/L;

    iget v7, v0, Lcom/google/android/exoplayer2/Format;->B:I

    iget v8, v0, Lcom/google/android/exoplayer2/Format;->C:I

    invoke-virtual {v4, v7, v8}, Lcom/google/android/exoplayer2/audio/L;->a(II)V

    sget v4, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v7, 0x15

    if-ge v4, v7, :cond_2

    iget v4, v0, Lcom/google/android/exoplayer2/Format;->y:I

    const/16 v7, 0x8

    if-ne v4, v7, :cond_2

    if-nez p3, :cond_2

    const/4 v4, 0x6

    new-array v4, v4, [I

    move v7, v5

    :goto_2
    array-length v8, v4

    if-ge v7, v8, :cond_3

    aput v7, v4, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :cond_3
    iget-object v7, v1, Lcom/google/android/exoplayer2/audio/A;->f:Lcom/google/android/exoplayer2/audio/x;

    invoke-virtual {v7, v4}, Lcom/google/android/exoplayer2/audio/x;->a([I)V

    new-instance v4, Lcom/google/android/exoplayer2/audio/r$a;

    iget v7, v0, Lcom/google/android/exoplayer2/Format;->z:I

    iget v8, v0, Lcom/google/android/exoplayer2/Format;->y:I

    iget v9, v0, Lcom/google/android/exoplayer2/Format;->A:I

    invoke-direct {v4, v7, v8, v9}, Lcom/google/android/exoplayer2/audio/r$a;-><init>(III)V

    array-length v7, v6

    move-object v8, v4

    move v4, v5

    :goto_3
    if-ge v4, v7, :cond_5

    aget-object v9, v6, v4

    :try_start_0
    invoke-interface {v9, v8}, Lcom/google/android/exoplayer2/audio/r;->a(Lcom/google/android/exoplayer2/audio/r$a;)Lcom/google/android/exoplayer2/audio/r$a;

    move-result-object v10

    invoke-interface {v9}, Lcom/google/android/exoplayer2/audio/r;->isActive()Z

    move-result v9
    :try_end_0
    .catch Lcom/google/android/exoplayer2/audio/r$b; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v9, :cond_4

    move-object v8, v10

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :catch_0
    move-exception v0

    new-instance v2, Lcom/google/android/exoplayer2/audio/AudioSink$a;

    invoke-direct {v2, v0}, Lcom/google/android/exoplayer2/audio/AudioSink$a;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_5
    iget v4, v8, Lcom/google/android/exoplayer2/audio/r$a;->d:I

    iget v7, v8, Lcom/google/android/exoplayer2/audio/r$a;->b:I

    iget v9, v8, Lcom/google/android/exoplayer2/audio/r$a;->c:I

    invoke-static {v9}, Lcom/google/android/exoplayer2/util/E;->b(I)I

    move-result v9

    iget v8, v8, Lcom/google/android/exoplayer2/audio/r$a;->c:I

    invoke-static {v4, v8}, Lcom/google/android/exoplayer2/util/E;->b(II)I

    move-result v8

    move v12, v3

    move-object v13, v6

    move v10, v7

    move v7, v8

    move v8, v9

    move v9, v4

    move v6, v5

    move v4, v2

    goto :goto_4

    :cond_6
    new-array v2, v5, [Lcom/google/android/exoplayer2/audio/r;

    iget v6, v0, Lcom/google/android/exoplayer2/Format;->z:I

    iget-boolean v7, v1, Lcom/google/android/exoplayer2/audio/A;->n:Z

    if-eqz v7, :cond_7

    iget-object v7, v1, Lcom/google/android/exoplayer2/audio/A;->u:Lcom/google/android/exoplayer2/audio/o;

    invoke-static {v0, v7}, Lcom/google/android/exoplayer2/audio/A;->a(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/audio/o;)Z

    move-result v7

    if-eqz v7, :cond_7

    iget-object v7, v0, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    iget-object v8, v0, Lcom/google/android/exoplayer2/Format;->i:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/google/android/exoplayer2/util/q;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    iget v8, v0, Lcom/google/android/exoplayer2/Format;->y:I

    invoke-static {v8}, Lcom/google/android/exoplayer2/util/E;->b(I)I

    move-result v8

    move-object v13, v2

    move v12, v5

    move v10, v6

    move v9, v7

    move v7, v3

    move v6, v4

    move v4, v7

    goto :goto_4

    :cond_7
    const/4 v4, 0x2

    iget-object v7, v1, Lcom/google/android/exoplayer2/audio/A;->c:Lcom/google/android/exoplayer2/audio/p;

    invoke-static {v0, v7}, Lcom/google/android/exoplayer2/audio/A;->a(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/audio/p;)Landroid/util/Pair;

    move-result-object v7

    if-eqz v7, :cond_b

    iget-object v8, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iget-object v7, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move-object v13, v2

    move v12, v5

    move v10, v6

    move v9, v8

    move v6, v4

    move v8, v7

    move v4, v3

    move v7, v4

    :goto_4
    const-string v2, ") for: "

    if-eqz v9, :cond_a

    if-eqz v8, :cond_9

    iput-boolean v5, v1, Lcom/google/android/exoplayer2/audio/A;->Y:Z

    new-instance v14, Lcom/google/android/exoplayer2/audio/A$b;

    iget-boolean v11, v1, Lcom/google/android/exoplayer2/audio/A;->m:Z

    move-object v2, v14

    move-object/from16 v3, p1

    move v5, v6

    move v6, v7

    move v7, v10

    move/from16 v10, p2

    invoke-direct/range {v2 .. v13}, Lcom/google/android/exoplayer2/audio/A$b;-><init>(Lcom/google/android/exoplayer2/Format;IIIIIIIZZ[Lcom/google/android/exoplayer2/audio/r;)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v0

    if-eqz v0, :cond_8

    iput-object v14, v1, Lcom/google/android/exoplayer2/audio/A;->r:Lcom/google/android/exoplayer2/audio/A$b;

    goto :goto_5

    :cond_8
    iput-object v14, v1, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    :goto_5
    return-void

    :cond_9
    new-instance v3, Lcom/google/android/exoplayer2/audio/AudioSink$a;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x36

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid output channel config (mode="

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/exoplayer2/audio/AudioSink$a;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_a
    new-instance v3, Lcom/google/android/exoplayer2/audio/AudioSink$a;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x30

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid output encoding (mode="

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/exoplayer2/audio/AudioSink$a;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_b
    new-instance v2, Lcom/google/android/exoplayer2/audio/AudioSink$a;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x25

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to configure passthrough for: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/exoplayer2/audio/AudioSink$a;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public a(Lcom/google/android/exoplayer2/audio/AudioSink$c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->p:Lcom/google/android/exoplayer2/audio/AudioSink$c;

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/audio/o;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->u:Lcom/google/android/exoplayer2/audio/o;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/audio/o;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->u:Lcom/google/android/exoplayer2/audio/o;

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/audio/A;->W:Z

    if-eqz p1, :cond_1

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/A;->flush()V

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/exoplayer2/audio/A;->U:I

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/audio/v;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->V:Lcom/google/android/exoplayer2/audio/v;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/audio/v;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget v0, p1, Lcom/google/android/exoplayer2/audio/v;->a:I

    iget v1, p1, Lcom/google/android/exoplayer2/audio/v;->b:F

    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/A;->V:Lcom/google/android/exoplayer2/audio/v;

    iget v3, v3, Lcom/google/android/exoplayer2/audio/v;->a:I

    if-eq v3, v0, :cond_1

    invoke-virtual {v2, v0}, Landroid/media/AudioTrack;->attachAuxEffect(I)I

    :cond_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v0, v1}, Landroid/media/AudioTrack;->setAuxEffectSendLevel(F)I

    :cond_2
    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A;->V:Lcom/google/android/exoplayer2/audio/v;

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/ga;)V
    .locals 4

    new-instance v0, Lcom/google/android/exoplayer2/ga;

    iget v1, p1, Lcom/google/android/exoplayer2/ga;->b:F

    const/high16 v2, 0x41000000    # 8.0f

    const v3, 0x3dcccccd    # 0.1f

    invoke-static {v1, v3, v2}, Lcom/google/android/exoplayer2/util/E;->a(FFF)F

    move-result v1

    iget p1, p1, Lcom/google/android/exoplayer2/ga;->c:F

    invoke-static {p1, v3, v2}, Lcom/google/android/exoplayer2/util/E;->a(FFF)F

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/google/android/exoplayer2/ga;-><init>(FF)V

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/audio/A;->m:Z

    if-eqz p1, :cond_0

    sget p1, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v1, 0x17

    if-lt p1, v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/audio/A;->b(Lcom/google/android/exoplayer2/ga;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/A;->h()Z

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/google/android/exoplayer2/audio/A;->a(Lcom/google/android/exoplayer2/ga;Z)V

    :goto_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/Format;)Z
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/audio/A;->b(Lcom/google/android/exoplayer2/Format;)I

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public a(Ljava/nio/ByteBuffer;JI)Z
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$b;,
            Lcom/google/android/exoplayer2/audio/AudioSink$d;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    move/from16 v4, p4

    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->L:Ljava/nio/ByteBuffer;

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz v5, :cond_1

    if-ne v1, v5, :cond_0

    goto :goto_0

    :cond_0
    move v5, v7

    goto :goto_1

    :cond_1
    :goto_0
    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->r:Lcom/google/android/exoplayer2/audio/A$b;

    const/4 v8, 0x0

    if-eqz v5, :cond_6

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->j()Z

    move-result v5

    if-nez v5, :cond_2

    return v7

    :cond_2
    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->r:Lcom/google/android/exoplayer2/audio/A$b;

    iget-object v9, v0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    invoke-virtual {v5, v9}, Lcom/google/android/exoplayer2/audio/A$b;->a(Lcom/google/android/exoplayer2/audio/A$b;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->t()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->d()Z

    move-result v5

    if-eqz v5, :cond_3

    return v7

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->flush()V

    goto :goto_2

    :cond_4
    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->r:Lcom/google/android/exoplayer2/audio/A$b;

    iput-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iput-object v8, v0, Lcom/google/android/exoplayer2/audio/A;->r:Lcom/google/android/exoplayer2/audio/A$b;

    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-static {v5}, Lcom/google/android/exoplayer2/audio/A;->a(Landroid/media/AudioTrack;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v5}, Landroid/media/AudioTrack;->setOffloadEndOfStream()V

    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    iget-object v9, v0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget-object v9, v9, Lcom/google/android/exoplayer2/audio/A$b;->a:Lcom/google/android/exoplayer2/Format;

    iget v10, v9, Lcom/google/android/exoplayer2/Format;->B:I

    iget v9, v9, Lcom/google/android/exoplayer2/Format;->C:I

    invoke-virtual {v5, v10, v9}, Landroid/media/AudioTrack;->setOffloadDelayPadding(II)V

    :cond_5
    :goto_2
    invoke-direct {v0, v2, v3}, Lcom/google/android/exoplayer2/audio/A;->a(J)V

    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v5

    if-nez v5, :cond_7

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->p()V

    :cond_7
    iget-boolean v5, v0, Lcom/google/android/exoplayer2/audio/A;->G:Z

    const-wide/16 v9, 0x0

    if-eqz v5, :cond_9

    invoke-static {v9, v10, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v11

    iput-wide v11, v0, Lcom/google/android/exoplayer2/audio/A;->H:J

    iput-boolean v7, v0, Lcom/google/android/exoplayer2/audio/A;->F:Z

    iput-boolean v7, v0, Lcom/google/android/exoplayer2/audio/A;->G:Z

    iget-boolean v5, v0, Lcom/google/android/exoplayer2/audio/A;->m:Z

    if-eqz v5, :cond_8

    sget v5, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v11, 0x17

    if-lt v5, v11, :cond_8

    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->x:Lcom/google/android/exoplayer2/ga;

    invoke-direct {v0, v5}, Lcom/google/android/exoplayer2/audio/A;->b(Lcom/google/android/exoplayer2/ga;)V

    :cond_8
    invoke-direct {v0, v2, v3}, Lcom/google/android/exoplayer2/audio/A;->a(J)V

    iget-boolean v5, v0, Lcom/google/android/exoplayer2/audio/A;->T:Z

    if-eqz v5, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->play()V

    :cond_9
    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->o()J

    move-result-wide v11

    invoke-virtual {v5, v11, v12}, Lcom/google/android/exoplayer2/audio/u;->f(J)Z

    move-result v5

    if-nez v5, :cond_a

    return v7

    :cond_a
    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->L:Ljava/nio/ByteBuffer;

    const-string v11, "AudioTrack"

    if-nez v5, :cond_14

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v5

    sget-object v12, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    if-ne v5, v12, :cond_b

    move v5, v6

    goto :goto_3

    :cond_b
    move v5, v7

    :goto_3
    invoke-static {v5}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v5

    if-nez v5, :cond_c

    return v6

    :cond_c
    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget v12, v5, Lcom/google/android/exoplayer2/audio/A$b;->c:I

    if-eqz v12, :cond_d

    iget v12, v0, Lcom/google/android/exoplayer2/audio/A;->E:I

    if-nez v12, :cond_d

    iget v5, v5, Lcom/google/android/exoplayer2/audio/A$b;->g:I

    invoke-static {v5, v1}, Lcom/google/android/exoplayer2/audio/A;->a(ILjava/nio/ByteBuffer;)I

    move-result v5

    iput v5, v0, Lcom/google/android/exoplayer2/audio/A;->E:I

    iget v5, v0, Lcom/google/android/exoplayer2/audio/A;->E:I

    if-nez v5, :cond_d

    return v6

    :cond_d
    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->v:Lcom/google/android/exoplayer2/audio/A$e;

    if-eqz v5, :cond_f

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->j()Z

    move-result v5

    if-nez v5, :cond_e

    return v7

    :cond_e
    invoke-direct {v0, v2, v3}, Lcom/google/android/exoplayer2/audio/A;->a(J)V

    iput-object v8, v0, Lcom/google/android/exoplayer2/audio/A;->v:Lcom/google/android/exoplayer2/audio/A$e;

    :cond_f
    iget-wide v12, v0, Lcom/google/android/exoplayer2/audio/A;->H:J

    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->n()J

    move-result-wide v14

    iget-object v8, v0, Lcom/google/android/exoplayer2/audio/A;->g:Lcom/google/android/exoplayer2/audio/L;

    invoke-virtual {v8}, Lcom/google/android/exoplayer2/audio/L;->h()J

    move-result-wide v16

    sub-long v14, v14, v16

    invoke-virtual {v5, v14, v15}, Lcom/google/android/exoplayer2/audio/A$b;->c(J)J

    move-result-wide v14

    add-long/2addr v12, v14

    iget-boolean v5, v0, Lcom/google/android/exoplayer2/audio/A;->F:Z

    if-nez v5, :cond_10

    sub-long v14, v12, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(J)J

    move-result-wide v14

    const-wide/32 v16, 0x30d40

    cmp-long v5, v14, v16

    if-lez v5, :cond_10

    const/16 v5, 0x50

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Discontinuity detected [expected "

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v5, ", got "

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v5, "]"

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v11, v5}, Lcom/google/android/exoplayer2/util/n;->b(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v6, v0, Lcom/google/android/exoplayer2/audio/A;->F:Z

    :cond_10
    iget-boolean v5, v0, Lcom/google/android/exoplayer2/audio/A;->F:Z

    if-eqz v5, :cond_12

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->j()Z

    move-result v5

    if-nez v5, :cond_11

    return v7

    :cond_11
    sub-long v12, v2, v12

    iget-wide v14, v0, Lcom/google/android/exoplayer2/audio/A;->H:J

    add-long/2addr v14, v12

    iput-wide v14, v0, Lcom/google/android/exoplayer2/audio/A;->H:J

    iput-boolean v7, v0, Lcom/google/android/exoplayer2/audio/A;->F:Z

    invoke-direct {v0, v2, v3}, Lcom/google/android/exoplayer2/audio/A;->a(J)V

    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->p:Lcom/google/android/exoplayer2/audio/AudioSink$c;

    if-eqz v5, :cond_12

    cmp-long v8, v12, v9

    if-eqz v8, :cond_12

    invoke-interface {v5}, Lcom/google/android/exoplayer2/audio/AudioSink$c;->a()V

    :cond_12
    iget-object v5, v0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget v5, v5, Lcom/google/android/exoplayer2/audio/A$b;->c:I

    if-nez v5, :cond_13

    iget-wide v8, v0, Lcom/google/android/exoplayer2/audio/A;->A:J

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    int-to-long v12, v5

    add-long/2addr v8, v12

    iput-wide v8, v0, Lcom/google/android/exoplayer2/audio/A;->A:J

    goto :goto_4

    :cond_13
    iget-wide v8, v0, Lcom/google/android/exoplayer2/audio/A;->B:J

    iget v5, v0, Lcom/google/android/exoplayer2/audio/A;->E:I

    mul-int/2addr v5, v4

    int-to-long v12, v5

    add-long/2addr v8, v12

    iput-wide v8, v0, Lcom/google/android/exoplayer2/audio/A;->B:J

    :goto_4
    iput-object v1, v0, Lcom/google/android/exoplayer2/audio/A;->L:Ljava/nio/ByteBuffer;

    iput v4, v0, Lcom/google/android/exoplayer2/audio/A;->M:I

    :cond_14
    invoke-direct {v0, v2, v3}, Lcom/google/android/exoplayer2/audio/A;->d(J)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/audio/A;->L:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-nez v1, :cond_15

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/exoplayer2/audio/A;->L:Ljava/nio/ByteBuffer;

    iput v7, v0, Lcom/google/android/exoplayer2/audio/A;->M:I

    return v6

    :cond_15
    iget-object v1, v0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->o()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/exoplayer2/audio/u;->e(J)Z

    move-result v1

    if-eqz v1, :cond_16

    const-string v1, "Resetting stalled audio track"

    invoke-static {v11, v1}, Lcom/google/android/exoplayer2/util/n;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/A;->flush()V

    return v6

    :cond_16
    return v7
.end method

.method public b(Lcom/google/android/exoplayer2/Format;)I
    .locals 3

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    const-string v1, "audio/raw"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-eqz v0, :cond_3

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->A:I

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->g(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget p1, p1, Lcom/google/android/exoplayer2/Format;->A:I

    const/16 v0, 0x21

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Invalid PCM encoding: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "AudioTrack"

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/util/n;->d(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_0
    iget p1, p1, Lcom/google/android/exoplayer2/Format;->A:I

    if-eq p1, v2, :cond_2

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    :goto_0
    return v2

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->n:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->Y:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->u:Lcom/google/android/exoplayer2/audio/o;

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/audio/A;->a(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/audio/o;)Z

    move-result v0

    if-eqz v0, :cond_4

    return v2

    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->c:Lcom/google/android/exoplayer2/audio/p;

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/audio/A;->b(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/audio/p;)Z

    move-result p1

    if-eqz p1, :cond_5

    return v2

    :cond_5
    return v1
.end method

.method public b(I)V
    .locals 3

    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const/4 v1, 0x1

    const/16 v2, 0x15

    if-lt v0, v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->W:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/exoplayer2/audio/A;->U:I

    if-eq v0, p1, :cond_2

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/exoplayer2/audio/A;->W:Z

    iput p1, p0, Lcom/google/android/exoplayer2/audio/A;->U:I

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/A;->flush()V

    :cond_2
    return-void
.end method

.method public b(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->l()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/exoplayer2/audio/A;->a(Lcom/google/android/exoplayer2/ga;Z)V

    return-void
.end method

.method public b()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->R:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/A;->d()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$d;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->R:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->t()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->R:Z

    :cond_0
    return-void
.end method

.method public d()Z
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/audio/u;->d(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public e()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->W:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->W:Z

    iput v0, p0, Lcom/google/android/exoplayer2/audio/A;->U:I

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/A;->flush()V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->F:Z

    return-void
.end method

.method public flush()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->v()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-static {v0}, Lcom/google/android/exoplayer2/audio/A;->a(Landroid/media/AudioTrack;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->o:Lcom/google/android/exoplayer2/audio/A$g;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/audio/A$g;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/audio/A$g;->b(Landroid/media/AudioTrack;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/A;->r:Lcom/google/android/exoplayer2/audio/A$b;

    if-eqz v2, :cond_2

    iput-object v2, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->r:Lcom/google/android/exoplayer2/audio/A$b;

    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/audio/u;->c()V

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->j:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    new-instance v1, Lcom/google/android/exoplayer2/audio/y;

    const-string v2, "ExoPlayer:AudioTrackReleaseThread"

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/exoplayer2/audio/y;-><init>(Lcom/google/android/exoplayer2/audio/A;Ljava/lang/String;Landroid/media/AudioTrack;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_3
    return-void
.end method

.method public g()V
    .locals 8

    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v1, 0x19

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/A;->flush()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->v()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/u;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->flush()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/u;->c()V

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget v0, v0, Lcom/google/android/exoplayer2/audio/A$b;->c:I

    const/4 v3, 0x2

    const/4 v7, 0x1

    if-ne v0, v3, :cond_3

    move v3, v7

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->s:Lcom/google/android/exoplayer2/audio/A$b;

    iget v4, v0, Lcom/google/android/exoplayer2/audio/A$b;->g:I

    iget v5, v0, Lcom/google/android/exoplayer2/audio/A$b;->d:I

    iget v6, v0, Lcom/google/android/exoplayer2/audio/A$b;->h:I

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/exoplayer2/audio/u;->a(Landroid/media/AudioTrack;ZIII)V

    iput-boolean v7, p0, Lcom/google/android/exoplayer2/audio/A;->G:Z

    return-void
.end method

.method public h()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->m()Lcom/google/android/exoplayer2/audio/A$e;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/audio/A$e;->b:Z

    return v0
.end method

.method public pause()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->T:Z

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    :cond_0
    return-void
.end method

.method public play()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/A;->T:Z

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->k:Lcom/google/android/exoplayer2/audio/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/u;->d()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->t:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    :cond_0
    return-void
.end method

.method public reset()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/A;->flush()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/A;->u()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->h:[Lcom/google/android/exoplayer2/audio/r;

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, v0, v3

    invoke-interface {v4}, Lcom/google/android/exoplayer2/audio/r;->reset()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A;->i:[Lcom/google/android/exoplayer2/audio/r;

    array-length v1, v0

    move v3, v2

    :goto_1
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    invoke-interface {v4}, Lcom/google/android/exoplayer2/audio/r;->reset()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    iput v2, p0, Lcom/google/android/exoplayer2/audio/A;->U:I

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/audio/A;->T:Z

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/audio/A;->Y:Z

    return-void
.end method
