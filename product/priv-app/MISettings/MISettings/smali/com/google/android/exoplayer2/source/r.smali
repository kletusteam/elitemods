.class public abstract Lcom/google/android/exoplayer2/source/r;
.super Lcom/google/android/exoplayer2/sa;


# instance fields
.field protected final b:Lcom/google/android/exoplayer2/sa;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/sa;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/sa;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/r;->b:Lcom/google/android/exoplayer2/sa;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/r;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->a()I

    move-result v0

    return v0
.end method

.method public a(IIZ)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/r;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/sa;->a(IIZ)I

    move-result p1

    return p1
.end method

.method public a(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/r;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public a(Z)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/r;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/sa;->a(Z)I

    move-result p1

    return p1
.end method

.method public a(ILcom/google/android/exoplayer2/sa$a;Z)Lcom/google/android/exoplayer2/sa$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/r;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;Z)Lcom/google/android/exoplayer2/sa$a;

    move-result-object p1

    return-object p1
.end method

.method public a(ILcom/google/android/exoplayer2/sa$b;J)Lcom/google/android/exoplayer2/sa$b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/r;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;J)Lcom/google/android/exoplayer2/sa$b;

    move-result-object p1

    return-object p1
.end method

.method public a(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/r;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/sa;->a(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/r;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->b()I

    move-result v0

    return v0
.end method

.method public b(IIZ)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/r;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/sa;->b(IIZ)I

    move-result p1

    return p1
.end method

.method public b(Z)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/r;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/sa;->b(Z)I

    move-result p1

    return p1
.end method
