.class final Lcom/google/android/exoplayer2/J;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/util/p;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/J$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/util/y;

.field private final b:Lcom/google/android/exoplayer2/J$a;

.field private c:Lcom/google/android/exoplayer2/Renderer;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private d:Lcom/google/android/exoplayer2/util/p;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/J$a;Lcom/google/android/exoplayer2/util/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/J;->b:Lcom/google/android/exoplayer2/J$a;

    new-instance p1, Lcom/google/android/exoplayer2/util/y;

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/util/y;-><init>(Lcom/google/android/exoplayer2/util/e;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/J;->e:Z

    return-void
.end method

.method private b(Z)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->c:Lcom/google/android/exoplayer2/Renderer;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Renderer;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->c:Lcom/google/android/exoplayer2/Renderer;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Renderer;->c()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/J;->c:Lcom/google/android/exoplayer2/Renderer;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Renderer;->g()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private c(Z)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/J;->b(Z)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/J;->e:Z

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/J;->f:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/y;->b()V

    :cond_0
    return-void

    :cond_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/J;->d:Lcom/google/android/exoplayer2/util/p;

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/util/p;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/util/p;->e()J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/J;->e:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/y;->e()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    iget-object p1, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/y;->c()V

    return-void

    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/J;->e:Z

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/J;->f:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/y;->b()V

    :cond_3
    iget-object v2, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/exoplayer2/util/y;->a(J)V

    invoke-interface {p1}, Lcom/google/android/exoplayer2/util/p;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/y;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/ga;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/util/y;->a(Lcom/google/android/exoplayer2/ga;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->b:Lcom/google/android/exoplayer2/J$a;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/J$a;->a(Lcom/google/android/exoplayer2/ga;)V

    :cond_4
    return-void
.end method


# virtual methods
.method public a(Z)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/J;->c(Z)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/J;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public a()Lcom/google/android/exoplayer2/ga;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->d:Lcom/google/android/exoplayer2/util/p;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/exoplayer2/util/p;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/y;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public a(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/util/y;->a(J)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/Renderer;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->c:Lcom/google/android/exoplayer2/Renderer;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/google/android/exoplayer2/J;->d:Lcom/google/android/exoplayer2/util/p;

    iput-object p1, p0, Lcom/google/android/exoplayer2/J;->c:Lcom/google/android/exoplayer2/Renderer;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/J;->e:Z

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/ga;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->d:Lcom/google/android/exoplayer2/util/p;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/util/p;->a(Lcom/google/android/exoplayer2/ga;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/J;->d:Lcom/google/android/exoplayer2/util/p;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/util/p;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/util/y;->a(Lcom/google/android/exoplayer2/ga;)V

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/J;->f:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/y;->b()V

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/Renderer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Renderer;->n()Lcom/google/android/exoplayer2/util/p;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer2/J;->d:Lcom/google/android/exoplayer2/util/p;

    if-eq v0, v1, :cond_1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/google/android/exoplayer2/J;->d:Lcom/google/android/exoplayer2/util/p;

    iput-object p1, p0, Lcom/google/android/exoplayer2/J;->c:Lcom/google/android/exoplayer2/Renderer;

    iget-object p1, p0, Lcom/google/android/exoplayer2/J;->d:Lcom/google/android/exoplayer2/util/p;

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/y;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/util/p;->a(Lcom/google/android/exoplayer2/ga;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Multiple renderer media clocks enabled."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/exoplayer2/ExoPlaybackException;->a(Ljava/lang/RuntimeException;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/J;->f:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/y;->c()V

    return-void
.end method

.method public e()J
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/J;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->a:Lcom/google/android/exoplayer2/util/y;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/y;->e()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/J;->d:Lcom/google/android/exoplayer2/util/p;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/util/p;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/util/p;->e()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method
