.class public final Lcom/google/android/exoplayer2/extractor/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/Extractor;


# static fields
.field public static final a:Lcom/google/android/exoplayer2/extractor/m;


# instance fields
.field private final b:Lcom/google/android/exoplayer2/util/t;

.field private final c:Lcom/google/android/exoplayer2/util/t;

.field private final d:Lcom/google/android/exoplayer2/util/t;

.field private final e:Lcom/google/android/exoplayer2/util/t;

.field private final f:Lcom/google/android/exoplayer2/extractor/a/d;

.field private g:Lcom/google/android/exoplayer2/extractor/k;

.field private h:I

.field private i:Z

.field private j:J

.field private k:I

.field private l:I

.field private m:I

.field private n:J

.field private o:Z

.field private p:Lcom/google/android/exoplayer2/extractor/a/b;

.field private q:Lcom/google/android/exoplayer2/extractor/a/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/extractor/a/a;->a:Lcom/google/android/exoplayer2/extractor/a/a;

    sput-object v0, Lcom/google/android/exoplayer2/extractor/a/c;->a:Lcom/google/android/exoplayer2/extractor/m;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->c:Lcom/google/android/exoplayer2/util/t;

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->d:Lcom/google/android/exoplayer2/util/t;

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->e:Lcom/google/android/exoplayer2/util/t;

    new-instance v0, Lcom/google/android/exoplayer2/extractor/a/d;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->f:Lcom/google/android/exoplayer2/extractor/a/d;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->h:I

    return-void
.end method

.method static synthetic a()[Lcom/google/android/exoplayer2/extractor/Extractor;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/exoplayer2/extractor/Extractor;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/a/c;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/extractor/a/c;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method private b(Lcom/google/android/exoplayer2/extractor/i;)Lcom/google/android/exoplayer2/util/t;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->m:I

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/t;->b()I

    move-result v1

    const/4 v2, 0x0

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/c;->m:I

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/util/t;->a([BI)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->e:Lcom/google/android/exoplayer2/util/t;

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->m:I

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/t;->d(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->m:I

    invoke-interface {p1, v0, v2, v1}, Lcom/google/android/exoplayer2/extractor/i;->readFully([BII)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->e:Lcom/google/android/exoplayer2/util/t;

    return-object p1
.end method

.method private b()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->g:Lcom/google/android/exoplayer2/extractor/k;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/u$b;

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    invoke-direct {v1, v2, v3}, Lcom/google/android/exoplayer2/extractor/u$b;-><init>(J)V

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/extractor/k;->a(Lcom/google/android/exoplayer2/extractor/u;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->o:Z

    :cond_0
    return-void
.end method

.method private c()J
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->i:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->j:J

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->n:J

    add-long/2addr v0, v2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->f:Lcom/google/android/exoplayer2/extractor/a/d;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/a/d;->a()J

    move-result-wide v0

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->n:J

    :goto_0
    return-wide v0
.end method

.method private c(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    const/16 v1, 0x9

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {p1, v0, v2, v1, v3}, Lcom/google/android/exoplayer2/extractor/i;->a([BIIZ)Z

    move-result p1

    if-nez p1, :cond_0

    return v2

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->c:Lcom/google/android/exoplayer2/util/t;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result p1

    and-int/lit8 v4, p1, 0x4

    if-eqz v4, :cond_1

    move v4, v3

    goto :goto_0

    :cond_1
    move v4, v2

    :goto_0
    and-int/2addr p1, v3

    if-eqz p1, :cond_2

    move v2, v3

    :cond_2
    if-eqz v4, :cond_3

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->p:Lcom/google/android/exoplayer2/extractor/a/b;

    if-nez p1, :cond_3

    new-instance p1, Lcom/google/android/exoplayer2/extractor/a/b;

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/a/c;->g:Lcom/google/android/exoplayer2/extractor/k;

    const/16 v5, 0x8

    invoke-interface {v4, v5, v3}, Lcom/google/android/exoplayer2/extractor/k;->a(II)Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object v4

    invoke-direct {p1, v4}, Lcom/google/android/exoplayer2/extractor/a/b;-><init>(Lcom/google/android/exoplayer2/extractor/TrackOutput;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->p:Lcom/google/android/exoplayer2/extractor/a/b;

    :cond_3
    const/4 p1, 0x2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->q:Lcom/google/android/exoplayer2/extractor/a/f;

    if-nez v2, :cond_4

    new-instance v2, Lcom/google/android/exoplayer2/extractor/a/f;

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/a/c;->g:Lcom/google/android/exoplayer2/extractor/k;

    invoke-interface {v4, v1, p1}, Lcom/google/android/exoplayer2/extractor/k;->a(II)Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/google/android/exoplayer2/extractor/a/f;-><init>(Lcom/google/android/exoplayer2/extractor/TrackOutput;)V

    iput-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->q:Lcom/google/android/exoplayer2/extractor/a/f;

    :cond_4
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->g:Lcom/google/android/exoplayer2/extractor/k;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/extractor/k;->g()V

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result v2

    sub-int/2addr v2, v1

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->k:I

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->h:I

    return v3
.end method

.method private d(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/a/c;->c()J

    move-result-wide v0

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->l:I

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/16 v7, 0x8

    if-ne v2, v7, :cond_1

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->p:Lcom/google/android/exoplayer2/extractor/a/b;

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/a/c;->b()V

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->p:Lcom/google/android/exoplayer2/extractor/a/b;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/a/c;->b(Lcom/google/android/exoplayer2/extractor/i;)Lcom/google/android/exoplayer2/util/t;

    move-result-object p1

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/exoplayer2/extractor/a/e;->a(Lcom/google/android/exoplayer2/util/t;J)Z

    move-result v5

    :cond_0
    :goto_0
    move p1, v6

    goto :goto_1

    :cond_1
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->l:I

    const/16 v7, 0x9

    if-ne v2, v7, :cond_2

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->q:Lcom/google/android/exoplayer2/extractor/a/f;

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/a/c;->b()V

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->q:Lcom/google/android/exoplayer2/extractor/a/f;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/a/c;->b(Lcom/google/android/exoplayer2/extractor/i;)Lcom/google/android/exoplayer2/util/t;

    move-result-object p1

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/exoplayer2/extractor/a/e;->a(Lcom/google/android/exoplayer2/util/t;J)Z

    move-result v5

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->l:I

    const/16 v7, 0x12

    if-ne v2, v7, :cond_3

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->o:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->f:Lcom/google/android/exoplayer2/extractor/a/d;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/a/c;->b(Lcom/google/android/exoplayer2/extractor/i;)Lcom/google/android/exoplayer2/util/t;

    move-result-object p1

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/exoplayer2/extractor/a/e;->a(Lcom/google/android/exoplayer2/util/t;J)Z

    move-result v5

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->f:Lcom/google/android/exoplayer2/extractor/a/d;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/extractor/a/d;->a()J

    move-result-wide v0

    cmp-long p1, v0, v3

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->g:Lcom/google/android/exoplayer2/extractor/k;

    new-instance v2, Lcom/google/android/exoplayer2/extractor/u$b;

    invoke-direct {v2, v0, v1}, Lcom/google/android/exoplayer2/extractor/u$b;-><init>(J)V

    invoke-interface {p1, v2}, Lcom/google/android/exoplayer2/extractor/k;->a(Lcom/google/android/exoplayer2/extractor/u;)V

    iput-boolean v6, p0, Lcom/google/android/exoplayer2/extractor/a/c;->o:Z

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->m:I

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    move p1, v5

    :goto_1
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->i:Z

    if-nez v0, :cond_5

    if-eqz v5, :cond_5

    iput-boolean v6, p0, Lcom/google/android/exoplayer2/extractor/a/c;->i:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->f:Lcom/google/android/exoplayer2/extractor/a/d;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/a/d;->a()J

    move-result-wide v0

    cmp-long v0, v0, v3

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->n:J

    neg-long v0, v0

    goto :goto_2

    :cond_4
    const-wide/16 v0, 0x0

    :goto_2
    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->j:J

    :cond_5
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->k:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->h:I

    return p1
.end method

.method private e(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0xb

    invoke-interface {p1, v0, v2, v3, v1}, Lcom/google/android/exoplayer2/extractor/i;->a([BIIZ)Z

    move-result p1

    if-nez p1, :cond_0

    return v2

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result p1

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->l:I

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->y()I

    move-result p1

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->m:I

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->y()I

    move-result p1

    int-to-long v2, p1

    iput-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->n:J

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result p1

    shl-int/lit8 p1, p1, 0x18

    int-to-long v2, p1

    iget-wide v4, p0, Lcom/google/android/exoplayer2/extractor/a/c;->n:J

    or-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->n:J

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->d:Lcom/google/android/exoplayer2/util/t;

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    const/4 p1, 0x4

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->h:I

    return v1
.end method

.method private f(Lcom/google/android/exoplayer2/extractor/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->k:I

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->k:I

    const/4 p1, 0x3

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->h:I

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->g:Lcom/google/android/exoplayer2/extractor/k;

    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    iget p2, p0, Lcom/google/android/exoplayer2/extractor/a/c;->h:I

    const/4 v0, 0x1

    const/4 v1, -0x1

    if-eq p2, v0, :cond_4

    const/4 v0, 0x2

    if-eq p2, v0, :cond_3

    const/4 v0, 0x3

    if-eq p2, v0, :cond_2

    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/a/c;->d(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/a/c;->e(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result p2

    if-nez p2, :cond_0

    return v1

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/a/c;->f(Lcom/google/android/exoplayer2/extractor/i;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/a/c;->c(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result p2

    if-nez p2, :cond_0

    return v1
.end method

.method public a(JJ)V
    .locals 0

    const/4 p1, 0x1

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->h:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->i:Z

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->k:I

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/k;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->g:Lcom/google/android/exoplayer2/extractor/k;

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->y()I

    move-result v0

    const v2, 0x464c56

    if-eq v0, v2, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    const/4 v2, 0x2

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->B()I

    move-result v0

    and-int/lit16 v0, v0, 0xfa

    if-eqz v0, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/i;->a(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1, v1}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/c;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result p1

    if-nez p1, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public release()V
    .locals 0

    return-void
.end method
