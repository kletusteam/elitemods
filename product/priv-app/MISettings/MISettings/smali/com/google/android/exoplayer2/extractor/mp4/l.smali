.class public final Lcom/google/android/exoplayer2/extractor/mp4/l;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/mp4/l$a;
    }
.end annotation


# direct methods
.method public static a([B)Ljava/util/UUID;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-static {p0}, Lcom/google/android/exoplayer2/extractor/mp4/l;->b([B)Lcom/google/android/exoplayer2/extractor/mp4/l$a;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    invoke-static {p0}, Lcom/google/android/exoplayer2/extractor/mp4/l$a;->a(Lcom/google/android/exoplayer2/extractor/mp4/l$a;)Ljava/util/UUID;

    move-result-object p0

    return-object p0
.end method

.method private static b([B)Lcom/google/android/exoplayer2/extractor/mp4/l$a;
    .locals 9
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/util/t;-><init>([B)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result p0

    const/4 v1, 0x0

    const/16 v2, 0x20

    if-ge p0, v2, :cond_0

    return-object v1

    :cond_0
    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    if-eq v2, v3, :cond_1

    return-object v1

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result v2

    const v3, 0x70737368    # 3.013775E29f

    if-eq v2, v3, :cond_2

    return-object v1

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/mp4/e;->c(I)I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_3

    const/16 p0, 0x25

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string p0, "Unsupported pssh version: "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "PsshAtomUtil"

    invoke-static {v0, p0}, Lcom/google/android/exoplayer2/util/n;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_3
    new-instance v4, Ljava/util/UUID;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->r()J

    move-result-wide v5

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->r()J

    move-result-wide v7

    invoke-direct {v4, v5, v6, v7, v8}, Ljava/util/UUID;-><init>(JJ)V

    if-ne v2, v3, :cond_4

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->z()I

    move-result v3

    mul-int/lit8 v3, v3, 0x10

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->z()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v5

    if-eq v3, v5, :cond_5

    return-object v1

    :cond_5
    new-array v1, v3, [B

    invoke-virtual {v0, v1, p0, v3}, Lcom/google/android/exoplayer2/util/t;->a([BII)V

    new-instance p0, Lcom/google/android/exoplayer2/extractor/mp4/l$a;

    invoke-direct {p0, v4, v2, v1}, Lcom/google/android/exoplayer2/extractor/mp4/l$a;-><init>(Ljava/util/UUID;I[B)V

    return-object p0
.end method
