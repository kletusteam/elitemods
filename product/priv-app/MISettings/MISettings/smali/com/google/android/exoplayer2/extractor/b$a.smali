.class public Lcom/google/android/exoplayer2/extractor/b$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/u;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/extractor/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/extractor/b$d;

.field private final b:J

.field private final c:J

.field private final d:J

.field private final e:J

.field private final f:J

.field private final g:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/extractor/b$d;JJJJJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b$a;->a:Lcom/google/android/exoplayer2/extractor/b$d;

    iput-wide p2, p0, Lcom/google/android/exoplayer2/extractor/b$a;->b:J

    iput-wide p4, p0, Lcom/google/android/exoplayer2/extractor/b$a;->c:J

    iput-wide p6, p0, Lcom/google/android/exoplayer2/extractor/b$a;->d:J

    iput-wide p8, p0, Lcom/google/android/exoplayer2/extractor/b$a;->e:J

    iput-wide p10, p0, Lcom/google/android/exoplayer2/extractor/b$a;->f:J

    iput-wide p12, p0, Lcom/google/android/exoplayer2/extractor/b$a;->g:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/extractor/b$a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b$a;->c:J

    return-wide v0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/extractor/b$a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b$a;->d:J

    return-wide v0
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/extractor/b$a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b$a;->e:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/extractor/b$a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b$a;->f:J

    return-wide v0
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/extractor/b$a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b$a;->g:J

    return-wide v0
.end method


# virtual methods
.method public b(J)Lcom/google/android/exoplayer2/extractor/u$a;
    .locals 13

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b$a;->a:Lcom/google/android/exoplayer2/extractor/b$d;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/b$d;->a(J)J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/exoplayer2/extractor/b$a;->c:J

    iget-wide v5, p0, Lcom/google/android/exoplayer2/extractor/b$a;->d:J

    iget-wide v7, p0, Lcom/google/android/exoplayer2/extractor/b$a;->e:J

    iget-wide v9, p0, Lcom/google/android/exoplayer2/extractor/b$a;->f:J

    iget-wide v11, p0, Lcom/google/android/exoplayer2/extractor/b$a;->g:J

    invoke-static/range {v1 .. v12}, Lcom/google/android/exoplayer2/extractor/b$c;->a(JJJJJJ)J

    move-result-wide v0

    new-instance v2, Lcom/google/android/exoplayer2/extractor/u$a;

    new-instance v3, Lcom/google/android/exoplayer2/extractor/v;

    invoke-direct {v3, p1, p2, v0, v1}, Lcom/google/android/exoplayer2/extractor/v;-><init>(JJ)V

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/extractor/u$a;-><init>(Lcom/google/android/exoplayer2/extractor/v;)V

    return-object v2
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b$a;->b:J

    return-wide v0
.end method

.method public c(J)J
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b$a;->a:Lcom/google/android/exoplayer2/extractor/b$d;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/b$d;->a(J)J

    move-result-wide p1

    return-wide p1
.end method
