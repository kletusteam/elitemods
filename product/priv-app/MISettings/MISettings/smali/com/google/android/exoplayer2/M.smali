.class final Lcom/google/android/exoplayer2/M;
.super Lcom/google/android/exoplayer2/E;

# interfaces
.implements Lcom/google/android/exoplayer2/K;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/M$a;,
        Lcom/google/android/exoplayer2/M$b;
    }
.end annotation


# instance fields
.field private A:Z

.field private B:Lcom/google/android/exoplayer2/fa;

.field private C:I

.field private D:I

.field private E:J

.field final b:Lcom/google/android/exoplayer2/trackselection/p;

.field private final c:[Lcom/google/android/exoplayer2/Renderer;

.field private final d:Lcom/google/android/exoplayer2/trackselection/o;

.field private final e:Landroid/os/Handler;

.field private final f:Lcom/google/android/exoplayer2/O$e;

.field private final g:Lcom/google/android/exoplayer2/O;

.field private final h:Landroid/os/Handler;

.field private final i:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/google/android/exoplayer2/E$a;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/google/android/exoplayer2/sa$a;

.field private final k:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/M$a;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Z

.field private final n:Lcom/google/android/exoplayer2/source/B;

.field private final o:Lcom/google/android/exoplayer2/a/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final p:Landroid/os/Looper;

.field private final q:Lcom/google/android/exoplayer2/upstream/g;

.field private r:I

.field private s:Z

.field private t:I

.field private u:Z

.field private v:I

.field private w:I

.field private x:Lcom/google/android/exoplayer2/ma;

.field private y:Lcom/google/android/exoplayer2/source/J;

.field private z:Z


# direct methods
.method public constructor <init>([Lcom/google/android/exoplayer2/Renderer;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/source/B;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/upstream/g;Lcom/google/android/exoplayer2/a/a;ZLcom/google/android/exoplayer2/ma;ZLcom/google/android/exoplayer2/util/e;Landroid/os/Looper;)V
    .locals 16
    .param p6    # Lcom/google/android/exoplayer2/a/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move-object/from16 v6, p5

    move-object/from16 v9, p6

    move-object/from16 v12, p11

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/E;-><init>()V

    invoke-static/range {p0 .. p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/android/exoplayer2/util/E;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1e

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Init "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ["

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "ExoPlayerLib/2.12.0"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] ["

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "ExoPlayerImpl"

    invoke-static {v3, v1}, Lcom/google/android/exoplayer2/util/n;->c(Ljava/lang/String;Ljava/lang/String;)V

    array-length v1, v2

    const/4 v3, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v2

    check-cast v1, [Lcom/google/android/exoplayer2/Renderer;

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->c:[Lcom/google/android/exoplayer2/Renderer;

    invoke-static/range {p2 .. p2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v1, p2

    check-cast v1, Lcom/google/android/exoplayer2/trackselection/o;

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->d:Lcom/google/android/exoplayer2/trackselection/o;

    move-object/from16 v1, p3

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->n:Lcom/google/android/exoplayer2/source/B;

    iput-object v6, v0, Lcom/google/android/exoplayer2/M;->q:Lcom/google/android/exoplayer2/upstream/g;

    iput-object v9, v0, Lcom/google/android/exoplayer2/M;->o:Lcom/google/android/exoplayer2/a/a;

    move/from16 v1, p7

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/M;->m:Z

    move-object/from16 v10, p8

    iput-object v10, v0, Lcom/google/android/exoplayer2/M;->x:Lcom/google/android/exoplayer2/ma;

    move/from16 v11, p9

    iput-boolean v11, v0, Lcom/google/android/exoplayer2/M;->z:Z

    iput-object v12, v0, Lcom/google/android/exoplayer2/M;->p:Landroid/os/Looper;

    iput v3, v0, Lcom/google/android/exoplayer2/M;->r:I

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->l:Ljava/util/List;

    new-instance v1, Lcom/google/android/exoplayer2/source/J$a;

    invoke-direct {v1, v3}, Lcom/google/android/exoplayer2/source/J$a;-><init>(I)V

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->y:Lcom/google/android/exoplayer2/source/J;

    new-instance v1, Lcom/google/android/exoplayer2/trackselection/p;

    array-length v3, v2

    new-array v3, v3, [Lcom/google/android/exoplayer2/ka;

    array-length v4, v2

    new-array v4, v4, [Lcom/google/android/exoplayer2/trackselection/l;

    const/4 v5, 0x0

    invoke-direct {v1, v3, v4, v5}, Lcom/google/android/exoplayer2/trackselection/p;-><init>([Lcom/google/android/exoplayer2/ka;[Lcom/google/android/exoplayer2/trackselection/l;Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->b:Lcom/google/android/exoplayer2/trackselection/p;

    new-instance v1, Lcom/google/android/exoplayer2/sa$a;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/sa$a;-><init>()V

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    const/4 v1, -0x1

    iput v1, v0, Lcom/google/android/exoplayer2/M;->C:I

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, v12}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer2/r;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer2/r;-><init>(Lcom/google/android/exoplayer2/M;)V

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->f:Lcom/google/android/exoplayer2/O$e;

    iget-object v1, v0, Lcom/google/android/exoplayer2/M;->b:Lcom/google/android/exoplayer2/trackselection/p;

    invoke-static {v1}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/trackselection/p;)Lcom/google/android/exoplayer2/fa;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->k:Ljava/util/ArrayDeque;

    if-eqz v9, :cond_1

    invoke-virtual {v9, v0}, Lcom/google/android/exoplayer2/a/a;->a(Lcom/google/android/exoplayer2/Player;)V

    invoke-virtual {v0, v9}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/Player$c;)V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, v12}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-interface {v6, v1, v9}, Lcom/google/android/exoplayer2/upstream/g;->a(Landroid/os/Handler;Lcom/google/android/exoplayer2/upstream/g$a;)V

    :cond_1
    new-instance v15, Lcom/google/android/exoplayer2/O;

    iget-object v4, v0, Lcom/google/android/exoplayer2/M;->b:Lcom/google/android/exoplayer2/trackselection/p;

    iget v7, v0, Lcom/google/android/exoplayer2/M;->r:I

    iget-boolean v8, v0, Lcom/google/android/exoplayer2/M;->s:Z

    iget-object v14, v0, Lcom/google/android/exoplayer2/M;->f:Lcom/google/android/exoplayer2/O$e;

    move-object v1, v15

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p8

    move/from16 v11, p9

    move-object/from16 v12, p11

    move-object/from16 v13, p10

    invoke-direct/range {v1 .. v14}, Lcom/google/android/exoplayer2/O;-><init>([Lcom/google/android/exoplayer2/Renderer;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/upstream/g;IZLcom/google/android/exoplayer2/a/a;Lcom/google/android/exoplayer2/ma;ZLandroid/os/Looper;Lcom/google/android/exoplayer2/util/e;Lcom/google/android/exoplayer2/O$e;)V

    iput-object v15, v0, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    new-instance v1, Landroid/os/Handler;

    iget-object v2, v0, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/O;->d()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, v0, Lcom/google/android/exoplayer2/M;->h:Landroid/os/Handler;

    return-void
.end method

.method private C()Lcom/google/android/exoplayer2/sa;
    .locals 3

    new-instance v0, Lcom/google/android/exoplayer2/ja;

    iget-object v1, p0, Lcom/google/android/exoplayer2/M;->l:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->y:Lcom/google/android/exoplayer2/source/J;

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/ja;-><init>(Ljava/util/Collection;Lcom/google/android/exoplayer2/source/J;)V

    return-object v0
.end method

.method private D()I
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/M;->C:I

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    return v0
.end method

.method private a(Lcom/google/android/exoplayer2/source/y$a;J)J
    .locals 2

    invoke-static {p2, p3}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide p2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object p1, p1, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    iget-object p1, p0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/sa$a;->d()J

    move-result-wide v0

    add-long/2addr p2, v0

    return-wide p2
.end method

.method private a(Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/fa;ZIZ)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/fa;",
            "Lcom/google/android/exoplayer2/fa;",
            "ZIZ)",
            "Landroid/util/Pair<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p2, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v1, p1, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v2

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance p1, Landroid/util/Pair;

    invoke-direct {p1, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v6

    const/4 v7, 0x3

    const/4 v8, 0x1

    if-eq v2, v6, :cond_1

    new-instance p1, Landroid/util/Pair;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    :cond_1
    iget-object p2, p2, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object p2, p2, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v0, p2, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object p2

    iget p2, p2, Lcom/google/android/exoplayer2/sa$a;->c:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/E;->a:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v0, p2, v2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object p2

    iget-object p2, p2, Lcom/google/android/exoplayer2/sa$b;->c:Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/E;->a:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/exoplayer2/sa$b;->c:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/exoplayer2/E;->a:Lcom/google/android/exoplayer2/sa$b;

    iget v2, v2, Lcom/google/android/exoplayer2/sa$b;->n:I

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_5

    if-eqz p3, :cond_2

    if-nez p4, :cond_2

    move v7, v8

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_3

    if-ne p4, v8, :cond_3

    const/4 v7, 0x2

    goto :goto_0

    :cond_3
    if-eqz p5, :cond_4

    :goto_0
    new-instance p1, Landroid/util/Pair;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_5
    if-eqz p3, :cond_6

    if-nez p4, :cond_6

    iget-object p1, p1, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object p1, p1, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {v1, p1}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result p1

    if-ne p1, v2, :cond_6

    new-instance p1, Landroid/util/Pair;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    :cond_6
    new-instance p1, Landroid/util/Pair;

    invoke-direct {p1, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method private a(Lcom/google/android/exoplayer2/sa;IJ)Landroid/util/Pair;
    .locals 6
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/sa;",
            "IJ)",
            "Landroid/util/Pair<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iput p2, p0, Lcom/google/android/exoplayer2/M;->C:I

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p1, p3, p1

    if-nez p1, :cond_0

    const-wide/16 p3, 0x0

    :cond_0
    iput-wide p3, p0, Lcom/google/android/exoplayer2/M;->E:J

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/exoplayer2/M;->D:I

    const/4 p1, 0x0

    return-object p1

    :cond_1
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/sa;->b()I

    move-result v0

    if-lt p2, v0, :cond_3

    :cond_2
    iget-boolean p2, p0, Lcom/google/android/exoplayer2/M;->s:Z

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/sa;->a(Z)I

    move-result p2

    iget-object p3, p0, Lcom/google/android/exoplayer2/E;->a:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {p1, p2, p3}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object p3

    invoke-virtual {p3}, Lcom/google/android/exoplayer2/sa$b;->a()J

    move-result-wide p3

    :cond_3
    move v3, p2

    iget-object v1, p0, Lcom/google/android/exoplayer2/E;->a:Lcom/google/android/exoplayer2/sa$b;

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-static {p3, p4}, Lcom/google/android/exoplayer2/C;->a(J)J

    move-result-wide v4

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/sa;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IJ)Landroid/util/Pair;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/sa;Landroid/util/Pair;)Lcom/google/android/exoplayer2/fa;
    .locals 16
    .param p3    # Landroid/util/Pair;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/fa;",
            "Lcom/google/android/exoplayer2/sa;",
            "Landroid/util/Pair<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/google/android/exoplayer2/fa;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v3

    const/4 v4, 0x1

    if-nez v3, :cond_1

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v3, v4

    :goto_1
    invoke-static {v3}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    move-object/from16 v3, p1

    iget-object v5, v3, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual/range {p1 .. p2}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/sa;)Lcom/google/android/exoplayer2/fa;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/google/android/exoplayer2/fa;->a()Lcom/google/android/exoplayer2/source/y$a;

    move-result-object v1

    iget-wide v2, v0, Lcom/google/android/exoplayer2/M;->E:J

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/C;->a(J)J

    move-result-wide v8

    iget-wide v2, v0, Lcom/google/android/exoplayer2/M;->E:J

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/C;->a(J)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    sget-object v14, Lcom/google/android/exoplayer2/source/TrackGroupArray;->a:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v15, v0, Lcom/google/android/exoplayer2/M;->b:Lcom/google/android/exoplayer2/trackselection/p;

    move-object v7, v1

    invoke-virtual/range {v6 .. v15}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/source/y$a;JJJLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;)Lcom/google/android/exoplayer2/fa;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/fa;

    move-result-object v1

    iget-wide v2, v1, Lcom/google/android/exoplayer2/fa;->q:J

    iput-wide v2, v1, Lcom/google/android/exoplayer2/fa;->o:J

    return-object v1

    :cond_2
    iget-object v3, v6, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v3, v3, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-static/range {p3 .. p3}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v7, v2

    check-cast v7, Landroid/util/Pair;

    iget-object v7, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    xor-int/2addr v7, v4

    if-eqz v7, :cond_3

    new-instance v8, Lcom/google/android/exoplayer2/source/y$a;

    iget-object v9, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-direct {v8, v9}, Lcom/google/android/exoplayer2/source/y$a;-><init>(Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    iget-object v8, v6, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    :goto_2
    move-object v15, v8

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/M;->l()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/android/exoplayer2/C;->a(J)J

    move-result-wide v8

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v5, v3, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/sa$a;->e()J

    move-result-wide v2

    sub-long/2addr v8, v2

    :cond_4
    if-nez v7, :cond_a

    cmp-long v2, v12, v8

    if-gez v2, :cond_5

    goto/16 :goto_4

    :cond_5
    if-nez v2, :cond_8

    iget-object v2, v6, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_6

    iget-object v3, v0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v2

    iget v2, v2, Lcom/google/android/exoplayer2/sa$a;->c:I

    iget-object v3, v15, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v3

    iget v3, v3, Lcom/google/android/exoplayer2/sa$a;->c:I

    if-eq v2, v3, :cond_d

    :cond_6
    iget-object v2, v15, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v3, v0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v15}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    iget v2, v15, Lcom/google/android/exoplayer2/source/y$a;->b:I

    iget v3, v15, Lcom/google/android/exoplayer2/source/y$a;->c:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/exoplayer2/sa$a;->a(II)J

    move-result-wide v1

    goto :goto_3

    :cond_7
    iget-object v1, v0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    iget-wide v1, v1, Lcom/google/android/exoplayer2/sa$a;->d:J

    :goto_3
    iget-wide v8, v6, Lcom/google/android/exoplayer2/fa;->q:J

    iget-wide v10, v6, Lcom/google/android/exoplayer2/fa;->q:J

    iget-wide v3, v6, Lcom/google/android/exoplayer2/fa;->q:J

    sub-long v12, v1, v3

    iget-object v14, v6, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v3, v6, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    move-object v7, v15

    move-object v5, v15

    move-object v15, v3

    invoke-virtual/range {v6 .. v15}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/source/y$a;JJJLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;)Lcom/google/android/exoplayer2/fa;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/fa;

    move-result-object v6

    iput-wide v1, v6, Lcom/google/android/exoplayer2/fa;->o:J

    goto/16 :goto_7

    :cond_8
    move-object v5, v15

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v1

    xor-int/2addr v1, v4

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    const-wide/16 v1, 0x0

    iget-wide v3, v6, Lcom/google/android/exoplayer2/fa;->p:J

    sub-long v7, v12, v8

    sub-long/2addr v3, v7

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    iget-wide v3, v6, Lcom/google/android/exoplayer2/fa;->o:J

    iget-object v7, v6, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v8, v6, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v7, v8}, Lcom/google/android/exoplayer2/source/y$a;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    add-long v3, v12, v1

    :cond_9
    iget-object v14, v6, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v15, v6, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    move-object v7, v5

    move-wide v8, v12

    move-wide v10, v12

    move-wide v12, v1

    invoke-virtual/range {v6 .. v15}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/source/y$a;JJJLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;)Lcom/google/android/exoplayer2/fa;

    move-result-object v6

    iput-wide v3, v6, Lcom/google/android/exoplayer2/fa;->o:J

    goto :goto_7

    :cond_a
    :goto_4
    move-object v5, v15

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v1

    xor-int/2addr v1, v4

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    const-wide/16 v1, 0x0

    if-eqz v7, :cond_b

    sget-object v3, Lcom/google/android/exoplayer2/source/TrackGroupArray;->a:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    goto :goto_5

    :cond_b
    iget-object v3, v6, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    :goto_5
    move-object v14, v3

    if-eqz v7, :cond_c

    iget-object v3, v0, Lcom/google/android/exoplayer2/M;->b:Lcom/google/android/exoplayer2/trackselection/p;

    goto :goto_6

    :cond_c
    iget-object v3, v6, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    :goto_6
    move-object v15, v3

    move-object v7, v5

    move-wide v8, v12

    move-wide v10, v12

    move-wide v3, v12

    move-wide v12, v1

    invoke-virtual/range {v6 .. v15}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/source/y$a;JJJLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;)Lcom/google/android/exoplayer2/fa;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/fa;

    move-result-object v6

    iput-wide v3, v6, Lcom/google/android/exoplayer2/fa;->o:J

    :cond_d
    :goto_7
    return-object v6
.end method

.method private a(ILjava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/source/y;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/da$c;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    new-instance v2, Lcom/google/android/exoplayer2/da$c;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/source/y;

    iget-boolean v4, p0, Lcom/google/android/exoplayer2/M;->m:Z

    invoke-direct {v2, v3, v4}, Lcom/google/android/exoplayer2/da$c;-><init>(Lcom/google/android/exoplayer2/source/y;Z)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/exoplayer2/M;->l:Ljava/util/List;

    add-int v4, v1, p1

    new-instance v5, Lcom/google/android/exoplayer2/M$a;

    iget-object v6, v2, Lcom/google/android/exoplayer2/da$c;->b:Ljava/lang/Object;

    iget-object v2, v2, Lcom/google/android/exoplayer2/da$c;->a:Lcom/google/android/exoplayer2/source/v;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/source/v;->i()Lcom/google/android/exoplayer2/sa;

    move-result-object v2

    invoke-direct {v5, v6, v2}, Lcom/google/android/exoplayer2/M$a;-><init>(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa;)V

    invoke-interface {v3, v4, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/google/android/exoplayer2/M;->y:Lcom/google/android/exoplayer2/source/J;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p2, p1, v1}, Lcom/google/android/exoplayer2/source/J;->b(II)Lcom/google/android/exoplayer2/source/J;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/M;->y:Lcom/google/android/exoplayer2/source/J;

    return-object v0
.end method

.method private a(II)V
    .locals 2

    add-int/lit8 v0, p2, -0x1

    :goto_0
    if-lt v0, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/M;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->y:Lcom/google/android/exoplayer2/source/J;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/source/J;->a(II)Lcom/google/android/exoplayer2/source/J;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/M;->y:Lcom/google/android/exoplayer2/source/J;

    iget-object p1, p0, Lcom/google/android/exoplayer2/M;->l:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/M;->A:Z

    :cond_1
    return-void
.end method

.method static synthetic a(ILcom/google/android/exoplayer2/Player$c;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/google/android/exoplayer2/Player$c;->onRepeatModeChanged(I)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/E$b;)V
    .locals 2

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/google/android/exoplayer2/M;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v1, Lcom/google/android/exoplayer2/c;

    invoke-direct {v1, v0, p1}, Lcom/google/android/exoplayer2/c;-><init>(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/fa;ZIIIZ)V
    .locals 20

    move-object/from16 v6, p0

    move-object/from16 v8, p1

    iget-object v9, v6, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iput-object v8, v6, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v9, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v1, v8, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/sa;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v5, v0, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v9

    move/from16 v3, p2

    move/from16 v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/fa;ZIZ)Landroid/util/Pair;

    move-result-object v0

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v16

    const/4 v0, 0x0

    if-eqz v15, :cond_0

    iget-object v1, v8, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v8, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v1, v8, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v2, v6, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/sa$a;->c:I

    iget-object v1, v8, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v2, v6, Lcom/google/android/exoplayer2/E;->a:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/exoplayer2/sa$b;->e:Lcom/google/android/exoplayer2/W;

    :cond_0
    move-object/from16 v17, v0

    new-instance v0, Lcom/google/android/exoplayer2/M$b;

    iget-object v10, v6, Lcom/google/android/exoplayer2/M;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v11, v6, Lcom/google/android/exoplayer2/M;->d:Lcom/google/android/exoplayer2/trackselection/o;

    move-object v7, v0

    move-object/from16 v8, p1

    move/from16 v12, p2

    move/from16 v13, p3

    move/from16 v14, p4

    move/from16 v18, p5

    move/from16 v19, p6

    invoke-direct/range {v7 .. v19}, Lcom/google/android/exoplayer2/M$b;-><init>(Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/fa;Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/trackselection/o;ZIIZILcom/google/android/exoplayer2/W;IZ)V

    invoke-direct {v6, v0}, Lcom/google/android/exoplayer2/M;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->k:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/exoplayer2/M;->k:Ljava/util/ArrayDeque;

    invoke-virtual {v1, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    if-eqz v0, :cond_0

    return-void

    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/M;->k:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/M;->k:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->peekFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/M;->k:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;IJZ)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/source/y;",
            ">;IJZ)V"
        }
    .end annotation

    move-object v7, p0

    move-object/from16 v0, p1

    move/from16 v1, p2

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/List;Z)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/M;->D()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/M;->getCurrentPosition()J

    move-result-wide v4

    iget v6, v7, Lcom/google/android/exoplayer2/M;->t:I

    add-int/2addr v6, v2

    iput v6, v7, Lcom/google/android/exoplayer2/M;->t:I

    iget-object v6, v7, Lcom/google/android/exoplayer2/M;->l:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    const/4 v8, 0x0

    if-nez v6, :cond_0

    iget-object v6, v7, Lcom/google/android/exoplayer2/M;->l:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {p0, v8, v6}, Lcom/google/android/exoplayer2/M;->a(II)V

    :cond_0
    invoke-direct {p0, v8, v0}, Lcom/google/android/exoplayer2/M;->a(ILjava/util/List;)Ljava/util/List;

    move-result-object v10

    invoke-direct {p0}, Lcom/google/android/exoplayer2/M;->C()Lcom/google/android/exoplayer2/sa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->b()I

    move-result v6

    if-ge v1, v6, :cond_1

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/google/android/exoplayer2/T;

    move-wide/from16 v8, p3

    invoke-direct {v2, v0, v1, v8, v9}, Lcom/google/android/exoplayer2/T;-><init>(Lcom/google/android/exoplayer2/sa;IJ)V

    throw v2

    :cond_2
    :goto_0
    move-wide/from16 v8, p3

    const/4 v6, -0x1

    if-eqz p5, :cond_3

    iget-boolean v1, v7, Lcom/google/android/exoplayer2/M;->s:Z

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/sa;->a(Z)I

    move-result v1

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    move v11, v1

    goto :goto_1

    :cond_3
    if-ne v1, v6, :cond_4

    move v11, v3

    move-wide v3, v4

    goto :goto_1

    :cond_4
    move v11, v1

    move-wide v3, v8

    :goto_1
    iget-object v1, v7, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    invoke-direct {p0, v0, v11, v3, v4}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/sa;IJ)Landroid/util/Pair;

    move-result-object v5

    invoke-direct {p0, v1, v0, v5}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/sa;Landroid/util/Pair;)Lcom/google/android/exoplayer2/fa;

    move-result-object v1

    iget v5, v1, Lcom/google/android/exoplayer2/fa;->e:I

    if-eq v11, v6, :cond_7

    if-eq v5, v2, :cond_7

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->b()I

    move-result v0

    if-lt v11, v0, :cond_5

    goto :goto_2

    :cond_5
    const/4 v5, 0x2

    goto :goto_3

    :cond_6
    :goto_2
    const/4 v5, 0x4

    :cond_7
    :goto_3
    invoke-virtual {v1, v5}, Lcom/google/android/exoplayer2/fa;->a(I)Lcom/google/android/exoplayer2/fa;

    move-result-object v1

    iget-object v9, v7, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    invoke-static {v3, v4}, Lcom/google/android/exoplayer2/C;->a(J)J

    move-result-wide v12

    iget-object v14, v7, Lcom/google/android/exoplayer2/M;->y:Lcom/google/android/exoplayer2/source/J;

    invoke-virtual/range {v9 .. v14}, Lcom/google/android/exoplayer2/O;->a(Ljava/util/List;IJLcom/google/android/exoplayer2/source/J;)V

    const/4 v2, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/fa;ZIIIZ)V

    return-void
.end method

.method private a(Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/source/y;",
            ">;Z)V"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M;->A:Z

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    if-eqz p2, :cond_2

    goto :goto_1

    :cond_2
    iget-object p2, p0, Lcom/google/android/exoplayer2/M;->l:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    :goto_1
    const/4 p2, 0x0

    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_3

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/y;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/y;

    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    :cond_3
    return-void
.end method

.method static synthetic a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/exoplayer2/M;->c(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    return-void
.end method

.method static synthetic a(ZLcom/google/android/exoplayer2/Player$c;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/google/android/exoplayer2/Player$c;->c(Z)V

    return-void
.end method

.method static synthetic b(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/exoplayer2/M;->c(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    return-void
.end method

.method private c(Lcom/google/android/exoplayer2/O$d;)V
    .locals 11

    iget v0, p0, Lcom/google/android/exoplayer2/M;->t:I

    iget v1, p1, Lcom/google/android/exoplayer2/O$d;->c:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/exoplayer2/M;->t:I

    iget-boolean v0, p1, Lcom/google/android/exoplayer2/O$d;->d:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/M;->u:Z

    iget v0, p1, Lcom/google/android/exoplayer2/O$d;->e:I

    iput v0, p0, Lcom/google/android/exoplayer2/M;->v:I

    :cond_0
    iget-boolean v0, p1, Lcom/google/android/exoplayer2/O$d;->f:Z

    if-eqz v0, :cond_1

    iget v0, p1, Lcom/google/android/exoplayer2/O$d;->g:I

    iput v0, p0, Lcom/google/android/exoplayer2/M;->w:I

    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer2/M;->t:I

    if-nez v0, :cond_5

    iget-object v0, p1, Lcom/google/android/exoplayer2/O$d;->b:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v2, v2, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/exoplayer2/M;->C:I

    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/android/exoplayer2/M;->E:J

    iput v3, p0, Lcom/google/android/exoplayer2/M;->D:I

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v2

    if-nez v2, :cond_4

    check-cast v0, Lcom/google/android/exoplayer2/ja;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ja;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/exoplayer2/M;->l:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ne v2, v4, :cond_3

    goto :goto_0

    :cond_3
    move v1, v3

    :goto_0
    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    move v1, v3

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->l:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/M$a;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/exoplayer2/sa;

    invoke-static {v2, v4}, Lcom/google/android/exoplayer2/M$a;->a(Lcom/google/android/exoplayer2/M$a;Lcom/google/android/exoplayer2/sa;)Lcom/google/android/exoplayer2/sa;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget-boolean v6, p0, Lcom/google/android/exoplayer2/M;->u:Z

    iput-boolean v3, p0, Lcom/google/android/exoplayer2/M;->u:Z

    iget-object v5, p1, Lcom/google/android/exoplayer2/O$d;->b:Lcom/google/android/exoplayer2/fa;

    iget v7, p0, Lcom/google/android/exoplayer2/M;->v:I

    const/4 v8, 0x1

    iget v9, p0, Lcom/google/android/exoplayer2/M;->w:I

    const/4 v10, 0x0

    move-object v4, p0

    invoke-direct/range {v4 .. v10}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/fa;ZIIIZ)V

    :cond_5
    return-void
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 2

    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "Player release timed out."

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/ExoPlaybackException;->a(Ljava/util/concurrent/TimeoutException;I)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/exoplayer2/Player$c;->a(Lcom/google/android/exoplayer2/ExoPlaybackException;)V

    return-void
.end method

.method private static c(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/google/android/exoplayer2/E$a;",
            ">;",
            "Lcom/google/android/exoplayer2/E$b;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/E$a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/E$a;->a(Lcom/google/android/exoplayer2/E$b;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public A()V
    .locals 10

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget v1, v0, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/ExoPlaybackException;)Lcom/google/android/exoplayer2/fa;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/fa;->a(I)Lcom/google/android/exoplayer2/fa;

    move-result-object v4

    iget v0, p0, Lcom/google/android/exoplayer2/M;->t:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/exoplayer2/M;->t:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/O;->g()V

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/fa;ZIIIZ)V

    return-void
.end method

.method public B()V
    .locals 5

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/exoplayer2/util/E;->e:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/exoplayer2/P;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Release "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " ["

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "ExoPlayerLib/2.12.0"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "] ["

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExoPlayerImpl"

    invoke-static {v1, v0}, Lcom/google/android/exoplayer2/util/n;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/O;->h()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/exoplayer2/d;->a:Lcom/google/android/exoplayer2/d;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/E$b;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->o:Lcom/google/android/exoplayer2/a/a;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer2/M;->q:Lcom/google/android/exoplayer2/upstream/g;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/upstream/g;->a(Lcom/google/android/exoplayer2/upstream/g$a;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/fa;->a(I)Lcom/google/android/exoplayer2/fa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/fa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->q:J

    iput-wide v1, v0, Lcom/google/android/exoplayer2/fa;->o:J

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/android/exoplayer2/fa;->p:J

    return-void
.end method

.method public a(I)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->c:[Lcom/google/android/exoplayer2/Renderer;

    aget-object p1, v0, p1

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Renderer;->f()I

    move-result p1

    return p1
.end method

.method public a()Lcom/google/android/exoplayer2/ga;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    return-object v0
.end method

.method public a(Lcom/google/android/exoplayer2/ia$b;)Lcom/google/android/exoplayer2/ia;
    .locals 7

    new-instance v6, Lcom/google/android/exoplayer2/ia;

    iget-object v1, p0, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v3, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/M;->i()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/exoplayer2/M;->h:Landroid/os/Handler;

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/ia;-><init>(Lcom/google/android/exoplayer2/ia$a;Lcom/google/android/exoplayer2/ia$b;Lcom/google/android/exoplayer2/sa;ILandroid/os/Handler;)V

    return-object v6
.end method

.method public a(IJ)V
    .locals 10

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    if-ltz p1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->b()I

    move-result v1

    if-ge p1, v1, :cond_3

    :cond_0
    iget v1, p0, Lcom/google/android/exoplayer2/M;->t:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/exoplayer2/M;->t:I

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/M;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string p1, "ExoPlayerImpl"

    const-string p2, "seekTo ignored because an ad is playing"

    invoke-static {p1, p2}, Lcom/google/android/exoplayer2/util/n;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/M;->f:Lcom/google/android/exoplayer2/O$e;

    new-instance p2, Lcom/google/android/exoplayer2/O$d;

    iget-object p3, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    invoke-direct {p2, p3}, Lcom/google/android/exoplayer2/O$d;-><init>(Lcom/google/android/exoplayer2/fa;)V

    invoke-interface {p1, p2}, Lcom/google/android/exoplayer2/O$e;->a(Lcom/google/android/exoplayer2/O$d;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/M;->getPlaybackState()I

    move-result v1

    if-ne v1, v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v2, 0x2

    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/fa;->a(I)Lcom/google/android/exoplayer2/fa;

    move-result-object v1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/sa;IJ)Landroid/util/Pair;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/sa;Landroid/util/Pair;)Lcom/google/android/exoplayer2/fa;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    invoke-static {p2, p3}, Lcom/google/android/exoplayer2/C;->a(J)J

    move-result-wide p2

    invoke-virtual {v1, v0, p1, p2, p3}, Lcom/google/android/exoplayer2/O;->a(Lcom/google/android/exoplayer2/sa;IJ)V

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x1

    move-object v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/fa;ZIIIZ)V

    return-void

    :cond_3
    new-instance v1, Lcom/google/android/exoplayer2/T;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/google/android/exoplayer2/T;-><init>(Lcom/google/android/exoplayer2/sa;IJ)V

    throw v1
.end method

.method public synthetic a(Lcom/google/android/exoplayer2/O$d;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/M;->c(Lcom/google/android/exoplayer2/O$d;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/E$a;

    invoke-direct {v1, p1}, Lcom/google/android/exoplayer2/E$a;-><init>(Lcom/google/android/exoplayer2/Player$c;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/ga;)V
    .locals 8
    .param p1    # Lcom/google/android/exoplayer2/ga;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    sget-object p1, Lcom/google/android/exoplayer2/ga;->a:Lcom/google/android/exoplayer2/ga;

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ga;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/fa;->a(Lcom/google/android/exoplayer2/ga;)Lcom/google/android/exoplayer2/fa;

    move-result-object v2

    iget v0, p0, Lcom/google/android/exoplayer2/M;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/M;->t:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/O;->b(Lcom/google/android/exoplayer2/ga;)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/fa;ZIIIZ)V

    return-void
.end method

.method public a(Ljava/util/List;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/source/y;",
            ">;IJ)V"
        }
    .end annotation

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/List;IJZ)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M;->s:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/M;->s:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/O;->a(Z)V

    new-instance v0, Lcom/google/android/exoplayer2/t;

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/t;-><init>(Z)V

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/E$b;)V

    :cond_0
    return-void
.end method

.method public a(ZII)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    if-ne v1, p1, :cond_0

    iget v0, v0, Lcom/google/android/exoplayer2/fa;->l:I

    if-ne v0, p2, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/M;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/M;->t:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/fa;->a(ZI)Lcom/google/android/exoplayer2/fa;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/O;->a(ZI)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/fa;ZIIIZ)V

    return-void
.end method

.method public synthetic b(Lcom/google/android/exoplayer2/O$d;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer2/s;

    invoke-direct {v1, p0, p1}, Lcom/google/android/exoplayer2/s;-><init>(Lcom/google/android/exoplayer2/M;Lcom/google/android/exoplayer2/O$d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/E$a;

    iget-object v2, v1, Lcom/google/android/exoplayer2/E$a;->a:Lcom/google/android/exoplayer2/Player$c;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/E$a;->a()V

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public b(Z)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/exoplayer2/M;->a(ZII)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v0

    return v0
.end method

.method public c()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/fa;->p:J

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    return v0
.end method

.method public e()Lcom/google/android/exoplayer2/trackselection/o;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->d:Lcom/google/android/exoplayer2/trackselection/o;

    return-object v0
.end method

.method public f()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/M;->D:I

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getCurrentPosition()J
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer2/M;->E:J

    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/fa;->q:J

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v0

    return-wide v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v2, v0, Lcom/google/android/exoplayer2/fa;->q:J

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/source/y$a;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getDuration()J
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/M;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v2, v1, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    iget v2, v1, Lcom/google/android/exoplayer2/source/y$a;->b:I

    iget v1, v1, Lcom/google/android/exoplayer2/source/y$a;->c:I

    invoke-virtual {v0, v2, v1}, Lcom/google/android/exoplayer2/sa$a;->a(II)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v0

    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/E;->y()J

    move-result-wide v0

    return-wide v0
.end method

.method public getPlaybackState()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget v0, v0, Lcom/google/android/exoplayer2/fa;->e:I

    return v0
.end method

.method public getRepeatMode()I
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/M;->r:I

    return v0
.end method

.method public h()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/M;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget v0, v0, Lcom/google/android/exoplayer2/source/y$a;->c:I

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public i()I
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/M;->D()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public j()Lcom/google/android/exoplayer2/ExoPlaybackException;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    return-object v0
.end method

.method public k()Lcom/google/android/exoplayer2/Player$f;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public l()J
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/M;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->d:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/M;->i()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/E;->a:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa$b;->a()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa$a;->d()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/fa;->d:J

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/M;->getCurrentPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public n()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/M;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget v0, v0, Lcom/google/android/exoplayer2/source/y$a;->b:I

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public p()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget v0, v0, Lcom/google/android/exoplayer2/fa;->l:I

    return v0
.end method

.method public q()Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    return-object v0
.end method

.method public r()Lcom/google/android/exoplayer2/sa;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    return-object v0
.end method

.method public s()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->p:Landroid/os/Looper;

    return-object v0
.end method

.method public setRepeatMode(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/M;->r:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/exoplayer2/M;->r:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/O;->a(I)V

    new-instance v0, Lcom/google/android/exoplayer2/u;

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/u;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/E$b;)V

    :cond_0
    return-void
.end method

.method public t()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M;->s:Z

    return v0
.end method

.method public u()J
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer2/M;->E:J

    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v1, v1, Lcom/google/android/exoplayer2/source/y$a;->d:J

    iget-object v3, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v3, v3, Lcom/google/android/exoplayer2/source/y$a;->d:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/M;->i()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/E;->a:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa$b;->c()J

    move-result-wide v0

    return-wide v0

    :cond_1
    iget-wide v0, v0, Lcom/google/android/exoplayer2/fa;->o:J

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v2, v2, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/source/y$a;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->j:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/sa;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget v1, v1, Lcom/google/android/exoplayer2/source/y$a;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/sa$a;->b(I)J

    move-result-wide v1

    const-wide/high16 v3, -0x8000000000000000L

    cmp-long v3, v1, v3

    if-nez v3, :cond_2

    iget-wide v0, v0, Lcom/google/android/exoplayer2/sa$a;->d:J

    goto :goto_0

    :cond_2
    move-wide v0, v1

    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v2, v2, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/source/y$a;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()Lcom/google/android/exoplayer2/trackselection/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->B:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object v0, v0, Lcom/google/android/exoplayer2/trackselection/p;->c:Lcom/google/android/exoplayer2/trackselection/m;

    return-object v0
.end method

.method public w()Lcom/google/android/exoplayer2/Player$e;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public z()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M;->g:Lcom/google/android/exoplayer2/O;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/O;->c()V

    return-void
.end method
