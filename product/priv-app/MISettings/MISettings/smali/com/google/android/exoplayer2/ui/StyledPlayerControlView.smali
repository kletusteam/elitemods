.class public Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;
.super Landroid/widget/FrameLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$l;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$a;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$i;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$j;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$g;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$e;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$f;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$c;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$d;,
        Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$m;
    }
.end annotation


# instance fields
.field private final A:Landroid/graphics/drawable/Drawable;

.field private Aa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;

.field private final B:Landroid/graphics/drawable/Drawable;

.field private Ba:Lcom/google/android/exoplayer2/ui/ga;

.field private final C:F

.field private Ca:Landroid/widget/ImageView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final D:F

.field private Da:Landroid/widget/ImageView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final E:Ljava/lang/String;

.field private Ea:Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final F:Ljava/lang/String;

.field private final G:Landroid/graphics/drawable/Drawable;

.field private final H:Landroid/graphics/drawable/Drawable;

.field private final I:Ljava/lang/String;

.field private final J:Ljava/lang/String;

.field private final K:Landroid/graphics/drawable/Drawable;

.field private final L:Landroid/graphics/drawable/Drawable;

.field private final M:Ljava/lang/String;

.field private final N:Ljava/lang/String;

.field private O:Lcom/google/android/exoplayer2/Player;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private P:Lcom/google/android/exoplayer2/G;

.field private Q:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$d;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private R:Lcom/google/android/exoplayer2/ha;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private S:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Z

.field private final a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

.field private aa:Z

.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$m;",
            ">;"
        }
    .end annotation
.end field

.field private ba:I

.field private final c:Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private ca:I

.field private final d:Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private da:I

.field private final e:Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private ea:[J

.field private final f:Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private fa:[Z

.field private final g:Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private ga:[J

.field private final h:Landroid/widget/TextView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private ha:[Z

.field private final i:Landroid/widget/TextView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private ia:J

.field private final j:Landroid/widget/ImageView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private ja:J

.field private final k:Landroid/widget/ImageView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private ka:J

.field private final l:Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private la:Lcom/google/android/exoplayer2/ui/ca;

.field private final m:Landroid/widget/TextView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private ma:Landroid/content/res/Resources;

.field private final n:Landroid/widget/TextView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private na:I

.field private final o:Lcom/google/android/exoplayer2/ui/fa;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private oa:Landroidx/recyclerview/widget/RecyclerView;

.field private final p:Ljava/lang/StringBuilder;

.field private pa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$f;

.field private final q:Ljava/util/Formatter;

.field private qa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;

.field private final r:Lcom/google/android/exoplayer2/sa$a;

.field private ra:Landroid/widget/PopupWindow;

.field private final s:Lcom/google/android/exoplayer2/sa$b;

.field private sa:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/lang/Runnable;

.field private ta:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Landroid/graphics/drawable/Drawable;

.field private ua:I

.field private final v:Landroid/graphics/drawable/Drawable;

.field private va:I

.field private final w:Landroid/graphics/drawable/Drawable;

.field private wa:Z

.field private final x:Ljava/lang/String;

.field private xa:I

.field private final y:Ljava/lang/String;

.field private ya:Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final z:Ljava/lang/String;

.field private za:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "goog.exo.ui"

    invoke-static {v0}, Lcom/google/android/exoplayer2/P;->a(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3, p2}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILandroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILandroid/util/AttributeSet;)V
    .locals 16
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p4

    invoke-direct/range {p0 .. p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget v3, Lcom/google/android/exoplayer2/ui/M;->exo_styled_player_control_view:I

    const-wide/16 v4, 0x1388

    iput-wide v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ja:J

    const-wide/16 v4, 0x3a98

    iput-wide v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ka:J

    const/16 v4, 0x1388

    iput v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ba:I

    const/4 v4, 0x0

    iput v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->da:I

    const/16 v5, 0xc8

    iput v5, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ca:I

    const/4 v5, 0x1

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    sget-object v7, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView:[I

    invoke-virtual {v6, v2, v7, v4, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v6

    :try_start_0
    sget v7, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_rewind_increment:I

    iget-wide v8, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ja:J

    long-to-int v8, v8

    invoke-virtual {v6, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    int-to-long v7, v7

    iput-wide v7, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ja:J

    sget v7, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_fastforward_increment:I

    iget-wide v8, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ka:J

    long-to-int v8, v8

    invoke-virtual {v6, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    int-to-long v7, v7

    iput-wide v7, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ka:J

    sget v7, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_controller_layout_id:I

    invoke-virtual {v6, v7, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    sget v7, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_show_timeout:I

    iget v8, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ba:I

    invoke-virtual {v6, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    iput v7, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ba:I

    iget v7, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->da:I

    invoke-static {v6, v7}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Landroid/content/res/TypedArray;I)I

    move-result v7

    iput v7, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->da:I

    sget v7, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_show_rewind_button:I

    invoke-virtual {v6, v7, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    sget v8, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_show_fastforward_button:I

    invoke-virtual {v6, v8, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v8

    sget v9, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_show_previous_button:I

    invoke-virtual {v6, v9, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v9

    sget v10, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_show_next_button:I

    invoke-virtual {v6, v10, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v10

    sget v11, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_show_shuffle_button:I

    invoke-virtual {v6, v11, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v11

    sget v12, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_show_subtitle_button:I

    invoke-virtual {v6, v12, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    sget v13, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_show_vr_button:I

    invoke-virtual {v6, v13, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v13

    sget v14, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_time_bar_min_update_interval:I

    iget v15, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ca:I

    invoke-virtual {v6, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v14

    invoke-virtual {v1, v14}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->setTimeBarMinUpdateInterval(I)V

    sget v14, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_animation_enabled:I

    invoke-virtual {v6, v14, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    :cond_0
    move v11, v4

    move v12, v11

    move v13, v12

    move v7, v5

    move v8, v7

    move v9, v8

    move v10, v9

    move v14, v10

    :goto_0
    new-instance v6, Lcom/google/android/exoplayer2/ui/ca;

    invoke-direct {v6}, Lcom/google/android/exoplayer2/ui/ca;-><init>()V

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    invoke-virtual {v6, v14}, Lcom/google/android/exoplayer2/ui/ca;->a(Z)V

    new-instance v6, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v6}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v6, Lcom/google/android/exoplayer2/sa$a;

    invoke-direct {v6}, Lcom/google/android/exoplayer2/sa$a;-><init>()V

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->r:Lcom/google/android/exoplayer2/sa$a;

    new-instance v6, Lcom/google/android/exoplayer2/sa$b;

    invoke-direct {v6}, Lcom/google/android/exoplayer2/sa$b;-><init>()V

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s:Lcom/google/android/exoplayer2/sa$b;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->p:Ljava/lang/StringBuilder;

    new-instance v6, Ljava/util/Formatter;

    iget-object v14, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->p:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v15

    invoke-direct {v6, v14, v15}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->q:Ljava/util/Formatter;

    new-array v6, v4, [J

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ea:[J

    new-array v6, v4, [Z

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->fa:[Z

    new-array v6, v4, [J

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ga:[J

    new-array v6, v4, [Z

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ha:[Z

    new-instance v6, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    const/4 v14, 0x0

    invoke-direct {v6, v1, v14}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;-><init>(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;Lcom/google/android/exoplayer2/ui/S;)V

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    new-instance v6, Lcom/google/android/exoplayer2/H;

    iget-wide v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ka:J

    iget-wide v14, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ja:J

    invoke-direct {v6, v4, v5, v14, v15}, Lcom/google/android/exoplayer2/H;-><init>(JJ)V

    iput-object v6, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    new-instance v4, Lcom/google/android/exoplayer2/ui/l;

    invoke-direct {v4, v1}, Lcom/google/android/exoplayer2/ui/l;-><init>(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V

    iput-object v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->t:Ljava/lang/Runnable;

    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    invoke-virtual {v4, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const/high16 v3, 0x40000

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setDescendantFocusability(I)V

    sget v3, Lcom/google/android/exoplayer2/ui/K;->exo_duration:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->m:Landroid/widget/TextView;

    sget v3, Lcom/google/android/exoplayer2/ui/K;->exo_position:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->n:Landroid/widget/TextView;

    sget v3, Lcom/google/android/exoplayer2/ui/K;->exo_subtitle:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ca:Landroid/widget/ImageView;

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ca:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    iget-object v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    sget v3, Lcom/google/android/exoplayer2/ui/K;->exo_fullscreen:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Da:Landroid/widget/ImageView;

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Da:Landroid/widget/ImageView;

    if-eqz v3, :cond_2

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Da:Landroid/widget/ImageView;

    new-instance v4, Lcom/google/android/exoplayer2/ui/n;

    invoke-direct {v4, v1}, Lcom/google/android/exoplayer2/ui/n;-><init>(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    sget v3, Lcom/google/android/exoplayer2/ui/K;->exo_settings:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ea:Landroid/view/View;

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ea:Landroid/view/View;

    if-eqz v3, :cond_3

    iget-object v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    sget v3, Lcom/google/android/exoplayer2/ui/K;->exo_progress:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/ui/fa;

    sget v4, Lcom/google/android/exoplayer2/ui/K;->exo_progress_placeholder:I

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v3, :cond_4

    iput-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->o:Lcom/google/android/exoplayer2/ui/fa;

    goto :goto_1

    :cond_4
    if-eqz v4, :cond_5

    new-instance v3, Lcom/google/android/exoplayer2/ui/DefaultTimeBar;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v3, v0, v5, v6, v2}, Lcom/google/android/exoplayer2/ui/DefaultTimeBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILandroid/util/AttributeSet;)V

    sget v2, Lcom/google/android/exoplayer2/ui/K;->exo_progress:I

    invoke-virtual {v3, v2}, Landroid/view/View;->setId(I)V

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v5

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    invoke-virtual {v2, v3, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iput-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->o:Lcom/google/android/exoplayer2/ui/fa;

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->o:Lcom/google/android/exoplayer2/ui/fa;

    :goto_1
    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->o:Lcom/google/android/exoplayer2/ui/fa;

    if-eqz v2, :cond_6

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-interface {v2, v3}, Lcom/google/android/exoplayer2/ui/fa;->a(Lcom/google/android/exoplayer2/ui/fa$a;)V

    :cond_6
    sget v2, Lcom/google/android/exoplayer2/ui/K;->exo_play_pause:I

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->e:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->e:Landroid/view/View;

    if-eqz v2, :cond_7

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_7
    sget v2, Lcom/google/android/exoplayer2/ui/K;->exo_prev:I

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c:Landroid/view/View;

    if-eqz v2, :cond_8

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_8
    sget v2, Lcom/google/android/exoplayer2/ui/K;->exo_next:I

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->d:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->d:Landroid/view/View;

    if-eqz v2, :cond_9

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_9
    sget v2, Lcom/google/android/exoplayer2/ui/J;->roboto_medium_numbers:I

    invoke-static {v0, v2}, Landroidx/core/content/res/e;->a(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object v2

    sget v3, Lcom/google/android/exoplayer2/ui/K;->exo_rew:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_a

    sget v4, Lcom/google/android/exoplayer2/ui/K;->exo_rew_with_amount:I

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v14, v4

    check-cast v14, Landroid/widget/TextView;

    goto :goto_2

    :cond_a
    const/4 v14, 0x0

    :goto_2
    iput-object v14, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->i:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->i:Landroid/widget/TextView;

    if-eqz v4, :cond_b

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_b
    if-nez v3, :cond_c

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->i:Landroid/widget/TextView;

    :cond_c
    iput-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->g:Landroid/view/View;

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->g:Landroid/view/View;

    if-eqz v3, :cond_d

    iget-object v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_d
    sget v3, Lcom/google/android/exoplayer2/ui/K;->exo_ffwd:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_e

    sget v4, Lcom/google/android/exoplayer2/ui/K;->exo_ffwd_with_amount:I

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v14, v4

    check-cast v14, Landroid/widget/TextView;

    goto :goto_3

    :cond_e
    const/4 v14, 0x0

    :goto_3
    iput-object v14, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->h:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->h:Landroid/widget/TextView;

    if-eqz v4, :cond_f

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_f
    if-nez v3, :cond_10

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->h:Landroid/widget/TextView;

    :cond_10
    iput-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->f:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->f:Landroid/view/View;

    if-eqz v2, :cond_11

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_11
    sget v2, Lcom/google/android/exoplayer2/ui/K;->exo_repeat_toggle:I

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    if-eqz v2, :cond_12

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_12
    sget v2, Lcom/google/android/exoplayer2/ui/K;->exo_shuffle:I

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    if-eqz v2, :cond_13

    iget-object v3, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_13
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/exoplayer2/ui/L;->exo_media_button_opacity_percentage_enabled:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    iput v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->C:F

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/exoplayer2/ui/L;->exo_media_button_opacity_percentage_disabled:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    iput v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->D:F

    sget v2, Lcom/google/android/exoplayer2/ui/K;->exo_vr:I

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->l:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->l:Landroid/view/View;

    if-eqz v2, :cond_14

    invoke-virtual {v1, v13}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->setShowVrButton(Z)V

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->l:Landroid/view/View;

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    goto :goto_4

    :cond_14
    const/4 v3, 0x0

    :goto_4
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    iget-object v5, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/exoplayer2/ui/N;->exo_controls_playback_speed:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    iget-object v5, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_speed:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v2, v3

    iget-object v5, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/exoplayer2/ui/N;->exo_track_selection_title_audio:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    aput-object v5, v4, v6

    iget-object v5, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v14, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_audiotrack:I

    invoke-virtual {v5, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v2, v6

    new-instance v5, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$f;

    invoke-direct {v5, v1, v4, v2}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$f;-><init>(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;[Ljava/lang/String;[Landroid/graphics/drawable/Drawable;)V

    iput-object v5, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->pa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$f;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/exoplayer2/ui/F;->exo_playback_speeds:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->sa:Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ta:Ljava/util/List;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/exoplayer2/ui/F;->exo_speed_multiplied_by_100:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    array-length v4, v2

    move v5, v3

    :goto_5
    if-ge v5, v4, :cond_15

    aget v6, v2, v5

    iget-object v14, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ta:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v14, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_15
    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ta:Ljava/util/List;

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    iput v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->va:I

    const/4 v2, -0x1

    iput v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ua:I

    iget-object v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/exoplayer2/ui/H;->exo_settings_offset:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->xa:I

    new-instance v4, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;

    const/4 v5, 0x0

    invoke-direct {v4, v1, v5}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;-><init>(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;Lcom/google/android/exoplayer2/ui/S;)V

    iput-object v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->qa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;

    iget-object v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->qa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;

    invoke-virtual {v4, v2}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;->a(I)V

    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/google/android/exoplayer2/ui/M;->exo_styled_settings_list:I

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->oa:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->oa:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->pa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$f;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->oa:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$g;)V

    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->oa:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v4, -0x2

    const/4 v5, 0x1

    invoke-direct {v0, v2, v4, v4, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iput-boolean v5, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->wa:Z

    new-instance v0, Lcom/google/android/exoplayer2/ui/C;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer2/ui/C;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ba:Lcom/google/android/exoplayer2/ui/ga;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_subtitle_on:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->G:Landroid/graphics/drawable/Drawable;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_subtitle_off:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->H:Landroid/graphics/drawable/Drawable;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/N;->exo_controls_cc_enabled_description:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->I:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/N;->exo_controls_cc_disabled_description:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->J:Ljava/lang/String;

    new-instance v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$i;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$i;-><init>(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;Lcom/google/android/exoplayer2/ui/S;)V

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->za:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;

    new-instance v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$a;

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$a;-><init>(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;Lcom/google/android/exoplayer2/ui/S;)V

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Aa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_fullscreen_exit:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->K:Landroid/graphics/drawable/Drawable;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_fullscreen_enter:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->L:Landroid/graphics/drawable/Drawable;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_repeat_off:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->u:Landroid/graphics/drawable/Drawable;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_repeat_one:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->v:Landroid/graphics/drawable/Drawable;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_repeat_all:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->w:Landroid/graphics/drawable/Drawable;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_shuffle_on:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->A:Landroid/graphics/drawable/Drawable;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_shuffle_off:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->B:Landroid/graphics/drawable/Drawable;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/N;->exo_controls_fullscreen_exit_description:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->M:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/N;->exo_controls_fullscreen_enter_description:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->N:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/N;->exo_controls_repeat_off_description:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->x:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/N;->exo_controls_repeat_one_description:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->y:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/N;->exo_controls_repeat_all_description:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->z:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/N;->exo_controls_shuffle_on_description:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->E:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/N;->exo_controls_shuffle_off_description:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->F:Ljava/lang/String;

    sget v0, Lcom/google/android/exoplayer2/ui/K;->exo_bottom_bar:I

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    const/4 v6, 0x1

    invoke-virtual {v2, v0, v6}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->f:Landroid/view/View;

    invoke-virtual {v0, v2, v8}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->g:Landroid/view/View;

    invoke-virtual {v0, v2, v7}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c:Landroid/view/View;

    invoke-virtual {v0, v2, v9}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->d:Landroid/view/View;

    invoke-virtual {v0, v2, v10}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v2, v11}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ca:Landroid/widget/ImageView;

    invoke-virtual {v0, v2, v12}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->l:Landroid/view/View;

    invoke-virtual {v0, v2, v13}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    iget-object v0, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v2, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    iget v4, v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->da:I

    if-eqz v4, :cond_16

    move v3, v6

    :cond_16
    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    new-instance v0, Lcom/google/android/exoplayer2/ui/m;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ui/m;-><init>(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method

.method static synthetic A(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ya:Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;

    return-object p0
.end method

.method static synthetic B(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/widget/PopupWindow;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    return-object p0
.end method

.method static synthetic C(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->n:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic D(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Ljava/lang/StringBuilder;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->p:Ljava/lang/StringBuilder;

    return-object p0
.end method

.method static synthetic E(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Ljava/util/Formatter;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->q:Ljava/util/Formatter;

    return-object p0
.end method

.method static synthetic F(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Lcom/google/android/exoplayer2/ui/ca;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    return-object p0
.end method

.method static synthetic G(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Lcom/google/android/exoplayer2/Player;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    return-object p0
.end method

.method public static synthetic H(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->m()V

    return-void
.end method

.method private static a(Landroid/content/res/TypedArray;I)I
    .locals 1

    sget v0, Lcom/google/android/exoplayer2/ui/O;->StyledPlayerControlView_repeat_toggle_modes:I

    invoke-virtual {p0, v0, p1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p0

    return p0
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->S:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$c;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Da:Landroid/widget/ImageView;

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->T:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->T:Z

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->T:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->K:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Da:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->M:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->L:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Da:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->N:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->S:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$c;

    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->T:Z

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$c;->a(Z)V

    :cond_2
    :goto_1
    return-void
.end method

.method private a(Landroid/view/View;IIIIIIII)V
    .locals 0

    sub-int/2addr p4, p2

    sub-int/2addr p5, p3

    sub-int/2addr p8, p6

    sub-int/2addr p9, p7

    if-ne p4, p8, :cond_0

    if-eq p5, p9, :cond_1

    :cond_0
    iget-object p2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    invoke-virtual {p2}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->q()V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result p2

    iget-object p3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    invoke-virtual {p3}, Landroid/widget/PopupWindow;->getWidth()I

    move-result p3

    sub-int/2addr p2, p3

    iget p3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->xa:I

    sub-int p6, p2, p3

    iget-object p2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    invoke-virtual {p2}, Landroid/widget/PopupWindow;->getHeight()I

    move-result p2

    neg-int p2, p2

    iget p3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->xa:I

    sub-int p7, p2, p3

    iget-object p4, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    const/4 p8, -0x1

    const/4 p9, -0x1

    move-object p5, p1

    invoke-virtual/range {p4 .. p9}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    :cond_1
    return-void
.end method

.method private a(Landroidx/recyclerview/widget/RecyclerView$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView$a<",
            "*>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->oa:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->q()V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->wa:Z

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->wa:Z

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v0

    sub-int/2addr p1, v0

    iget v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->xa:I

    sub-int/2addr p1, v0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v0

    neg-int v0, v0

    iget v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->xa:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    invoke-virtual {v1, p0, p1, v0}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/Player;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/exoplayer2/G;->b(Lcom/google/android/exoplayer2/Player;Z)Z

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/Player;J)V
    .locals 6

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->W:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/sa;->b()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/sa$b;->c()J

    move-result-wide v3

    cmp-long v5, p2, v3

    if-gez v5, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v5, v1, -0x1

    if-ne v2, v5, :cond_1

    move-wide p2, v3

    goto :goto_1

    :cond_1
    sub-long/2addr p2, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lcom/google/android/exoplayer2/Player;->i()I

    move-result v2

    :goto_1
    invoke-direct {p0, p1, v2, p2, p3}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Lcom/google/android/exoplayer2/Player;IJ)Z

    move-result p1

    if-nez p1, :cond_3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->m()V

    :cond_3
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/trackselection/k$a;ILjava/util/List;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/trackselection/k$a;",
            "I",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$j;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move/from16 v7, p2

    invoke-virtual/range {p1 .. p2}, Lcom/google/android/exoplayer2/trackselection/k$a;->b(I)Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v8

    iget-object v1, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/google/android/exoplayer2/Player;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->v()Lcom/google/android/exoplayer2/trackselection/m;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/google/android/exoplayer2/trackselection/m;->a(I)Lcom/google/android/exoplayer2/trackselection/l;

    move-result-object v9

    const/4 v10, 0x0

    move v11, v10

    :goto_0
    iget v1, v8, Lcom/google/android/exoplayer2/source/TrackGroupArray;->b:I

    if-ge v11, v1, :cond_3

    invoke-virtual {v8, v11}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->a(I)Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v12

    move v13, v10

    :goto_1
    iget v1, v12, Lcom/google/android/exoplayer2/source/TrackGroup;->a:I

    if-ge v13, v1, :cond_2

    invoke-virtual {v12, v13}, Lcom/google/android/exoplayer2/source/TrackGroup;->a(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    move-object/from16 v14, p1

    invoke-virtual {v14, v7, v11, v13}, Lcom/google/android/exoplayer2/trackselection/k$a;->a(III)I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    if-eqz v9, :cond_0

    invoke-interface {v9, v1}, Lcom/google/android/exoplayer2/trackselection/l;->a(Lcom/google/android/exoplayer2/Format;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    const/4 v2, 0x1

    move v6, v2

    goto :goto_2

    :cond_0
    move v6, v10

    :goto_2
    new-instance v15, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$j;

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ba:Lcom/google/android/exoplayer2/ui/ga;

    invoke-interface {v2, v1}, Lcom/google/android/exoplayer2/ui/ga;->a(Lcom/google/android/exoplayer2/Format;)Ljava/lang/String;

    move-result-object v5

    move-object v1, v15

    move/from16 v2, p2

    move v3, v11

    move v4, v13

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$j;-><init>(IIILjava/lang/String;Z)V

    move-object/from16 v1, p3

    invoke-interface {v1, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_1
    move-object/from16 v1, p3

    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_2
    move-object/from16 v14, p1

    move-object/from16 v1, p3

    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->l()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->b(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;Landroid/view/View;IIIIIIII)V
    .locals 0

    invoke-direct/range {p0 .. p9}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Landroid/view/View;IIIIIIII)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;Landroidx/recyclerview/widget/RecyclerView$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Landroidx/recyclerview/widget/RecyclerView$a;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;Lcom/google/android/exoplayer2/Player;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c(Lcom/google/android/exoplayer2/Player;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;Lcom/google/android/exoplayer2/Player;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Lcom/google/android/exoplayer2/Player;J)V

    return-void
.end method

.method private a(ZLandroid/view/View;)V
    .locals 0
    .param p2    # Landroid/view/View;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2, p1}, Landroid/view/View;->setEnabled(Z)V

    if-eqz p1, :cond_1

    iget p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->C:F

    goto :goto_0

    :cond_1
    iget p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->D:F

    :goto_0
    invoke-virtual {p2, p1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method private static a(I)Z
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    const/16 v0, 0x5a

    if-eq p0, v0, :cond_1

    const/16 v0, 0x59

    if-eq p0, v0, :cond_1

    const/16 v0, 0x55

    if-eq p0, v0, :cond_1

    const/16 v0, 0x4f

    if-eq p0, v0, :cond_1

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_1

    const/16 v0, 0x7f

    if-eq p0, v0, :cond_1

    const/16 v0, 0x57

    if-eq p0, v0, :cond_1

    const/16 v0, 0x58

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private a(Lcom/google/android/exoplayer2/Player;IJ)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/G;->a(Lcom/google/android/exoplayer2/Player;IJ)Z

    move-result p1

    return p1
.end method

.method private static a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/sa$b;)Z
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/sa;->b()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x64

    if-le v0, v2, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/sa;->b()I

    move-result v0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_2

    invoke-virtual {p0, v2, p1}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    move-result-object v3

    iget-wide v3, v3, Lcom/google/android/exoplayer2/sa$b;->q:J

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    return v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 p0, 0x1

    return p0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->aa:Z

    return p1
.end method

.method private b(I)V
    .locals 1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->qa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->sa:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;->a(Ljava/util/List;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->qa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;

    iget v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->va:I

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;->a(I)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->na:I

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->qa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$h;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Landroidx/recyclerview/widget/RecyclerView$a;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iput v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->na:I

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Aa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Landroidx/recyclerview/widget/RecyclerView$a;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/widget/PopupWindow;->dismiss()V

    :goto_0
    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/Player;)V
    .locals 4

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Player;->getPlaybackState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->R:Lcom/google/android/exoplayer2/ha;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/exoplayer2/ha;->a()V

    goto :goto_0

    :cond_0
    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Player;->i()I

    move-result v0

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Lcom/google/android/exoplayer2/Player;IJ)Z

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    invoke-interface {v0, p1, v1}, Lcom/google/android/exoplayer2/G;->b(Lcom/google/android/exoplayer2/Player;Z)Z

    return-void
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->m()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c(I)V

    return-void
.end method

.method private c(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->na:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->va:I

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ta:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-float p1, p1

    const/high16 v0, 0x42c80000    # 100.0f

    div-float/2addr p1, v0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->setPlaybackSpeed(F)V

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/widget/PopupWindow;->dismiss()V

    return-void
.end method

.method private c(Lcom/google/android/exoplayer2/Player;)V
    .locals 2

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Player;->getPlaybackState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Player;->d()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Lcom/google/android/exoplayer2/Player;)V

    goto :goto_1

    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->b(Lcom/google/android/exoplayer2/Player;)V

    :goto_1
    return-void
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->n()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->r()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->p()V

    return-void
.end method

.method private h()V
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->za:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;->a()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Aa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;->a()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ya:Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;

    if-nez v0, :cond_0

    goto :goto_2

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/trackselection/k;->c()Lcom/google/android/exoplayer2/trackselection/k$a;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/trackselection/k$a;->a()I

    move-result v6

    if-ge v5, v6, :cond_4

    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer2/trackselection/k$a;->a(I)I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_2

    iget-object v6, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v7, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ca:Landroid/widget/ImageView;

    invoke-virtual {v6, v7}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-direct {p0, v0, v5, v1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Lcom/google/android/exoplayer2/trackselection/k$a;ILjava/util/List;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer2/trackselection/k$a;->a(I)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    invoke-direct {p0, v0, v5, v2}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Lcom/google/android/exoplayer2/trackselection/k$a;ILjava/util/List;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->za:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;

    invoke-virtual {v5, v3, v1, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;->a(Ljava/util/List;Ljava/util/List;Lcom/google/android/exoplayer2/trackselection/k$a;)V

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Aa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;

    invoke-virtual {v1, v4, v2, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;->a(Ljava/util/List;Ljava/util/List;Lcom/google/android/exoplayer2/trackselection/k$a;)V

    :cond_5
    :goto_2
    return-void
.end method

.method static synthetic h(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->t()V

    return-void
.end method

.method private i()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->getPlaybackState()I

    move-result v0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->getPlaybackState()I

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method static synthetic i(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->wa:Z

    return p0
.end method

.method static synthetic j(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->d:Landroid/view/View;

    return-object p0
.end method

.method private j()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    instance-of v1, v0, Lcom/google/android/exoplayer2/H;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/exoplayer2/H;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/H;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ka:J

    :cond_0
    iget-wide v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ka:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->h:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->f:Landroid/view/View;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/exoplayer2/ui/N;->exo_controls_fastforward_by_amount_description:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method

.method static synthetic k(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Lcom/google/android/exoplayer2/G;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    return-object p0
.end method

.method private k()V
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->U:Z

    if-nez v0, :cond_0

    goto/16 :goto_5

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->b()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->i()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s:Lcom/google/android/exoplayer2/sa$b;

    iget-boolean v3, v2, Lcom/google/android/exoplayer2/sa$b;->j:Z

    const/4 v4, 0x1

    if-nez v3, :cond_2

    iget-boolean v2, v2, Lcom/google/android/exoplayer2/sa$b;->k:Z

    if-eqz v2, :cond_2

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->hasPrevious()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    :goto_0
    move v2, v4

    :goto_1
    if-eqz v3, :cond_3

    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    invoke-interface {v5}, Lcom/google/android/exoplayer2/G;->a()Z

    move-result v5

    if-eqz v5, :cond_3

    move v5, v4

    goto :goto_2

    :cond_3
    move v5, v1

    :goto_2
    if-eqz v3, :cond_4

    iget-object v6, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    invoke-interface {v6}, Lcom/google/android/exoplayer2/G;->b()Z

    move-result v6

    if-eqz v6, :cond_4

    move v6, v4

    goto :goto_3

    :cond_4
    move v6, v1

    :goto_3
    iget-object v7, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s:Lcom/google/android/exoplayer2/sa$b;

    iget-boolean v7, v7, Lcom/google/android/exoplayer2/sa$b;->k:Z

    if-nez v7, :cond_5

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    move v1, v4

    :cond_6
    move v0, v1

    move v1, v5

    goto :goto_4

    :cond_7
    move v0, v1

    move v2, v0

    move v3, v2

    move v6, v3

    :goto_4
    if-eqz v1, :cond_8

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->o()V

    :cond_8
    if-eqz v6, :cond_9

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j()V

    :cond_9
    iget-object v4, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c:Landroid/view/View;

    invoke-direct {p0, v2, v4}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->g:Landroid/view/View;

    invoke-direct {p0, v1, v2}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->f:Landroid/view/View;

    invoke-direct {p0, v6, v1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->d:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->o:Lcom/google/android/exoplayer2/ui/fa;

    if-eqz v0, :cond_a

    invoke-interface {v0, v3}, Lcom/google/android/exoplayer2/ui/fa;->setEnabled(Z)V

    :cond_a
    :goto_5
    return-void
.end method

.method static synthetic l(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c:Landroid/view/View;

    return-object p0
.end method

.method private l()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->U:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->e:Landroid/view/View;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->e:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_pause:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/N;->exo_controls_pause_description:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->e:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/I;->exo_styled_controls_play:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/exoplayer2/ui/N;->exo_controls_play_description:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic m(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->f:Landroid/view/View;

    return-object p0
.end method

.method private m()V
    .locals 13

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->U:Z

    if-nez v0, :cond_0

    goto/16 :goto_3

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_1

    iget-wide v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ia:J

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->l()J

    move-result-wide v3

    add-long/2addr v1, v3

    iget-wide v3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ia:J

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->u()J

    move-result-wide v5

    add-long/2addr v3, v5

    goto :goto_0

    :cond_1
    move-wide v3, v1

    :goto_0
    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->n:Landroid/widget/TextView;

    if-eqz v5, :cond_2

    iget-boolean v6, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->aa:Z

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->p:Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->q:Ljava/util/Formatter;

    invoke-static {v6, v7, v1, v2}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/StringBuilder;Ljava/util/Formatter;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->o:Lcom/google/android/exoplayer2/ui/fa;

    if-eqz v5, :cond_3

    invoke-interface {v5, v1, v2}, Lcom/google/android/exoplayer2/ui/fa;->setPosition(J)V

    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->o:Lcom/google/android/exoplayer2/ui/fa;

    invoke-interface {v5, v3, v4}, Lcom/google/android/exoplayer2/ui/fa;->setBufferedPosition(J)V

    :cond_3
    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Q:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$d;

    if-eqz v5, :cond_4

    invoke-interface {v5, v1, v2, v3, v4}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$d;->a(JJ)V

    :cond_4
    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->t:Ljava/lang/Runnable;

    invoke-virtual {p0, v3}, Landroid/widget/FrameLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v3, 0x1

    if-nez v0, :cond_5

    move v4, v3

    goto :goto_1

    :cond_5
    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->getPlaybackState()I

    move-result v4

    :goto_1
    const-wide/16 v5, 0x3e8

    if-eqz v0, :cond_8

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->isPlaying()Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->o:Lcom/google/android/exoplayer2/ui/fa;

    if-eqz v3, :cond_6

    invoke-interface {v3}, Lcom/google/android/exoplayer2/ui/fa;->getPreferredUpdateDelay()J

    move-result-wide v3

    goto :goto_2

    :cond_6
    move-wide v3, v5

    :goto_2
    rem-long/2addr v1, v5

    sub-long v1, v5, v1

    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/ga;->b:F

    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-lez v3, :cond_7

    long-to-float v1, v1

    div-float/2addr v1, v0

    float-to-long v5, v1

    :cond_7
    move-wide v7, v5

    iget v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ca:I

    int-to-long v9, v0

    const-wide/16 v11, 0x3e8

    invoke-static/range {v7 .. v12}, Lcom/google/android/exoplayer2/util/E;->b(JJJ)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->t:Ljava/lang/Runnable;

    invoke-virtual {p0, v2, v0, v1}, Landroid/widget/FrameLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_3

    :cond_8
    const/4 v0, 0x4

    if-eq v4, v0, :cond_9

    if-eq v4, v3, :cond_9

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->t:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, v5, v6}, Landroid/widget/FrameLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_9
    :goto_3
    return-void
.end method

.method static synthetic n(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->g:Landroid/view/View;

    return-object p0
.end method

.method private n()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->U:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->da:I

    const/4 v2, 0x0

    if-nez v1, :cond_1

    invoke-direct {p0, v2, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    if-nez v1, :cond_2

    invoke-direct {p0, v2, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->u:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_2
    const/4 v2, 0x1

    invoke-direct {p0, v2, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->getRepeatMode()I

    move-result v0

    if-eqz v0, :cond_5

    if-eq v0, v2, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->u:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_6
    :goto_0
    return-void
.end method

.method static synthetic o(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->e:Landroid/view/View;

    return-object p0
.end method

.method private o()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    instance-of v1, v0, Lcom/google/android/exoplayer2/H;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/exoplayer2/H;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/H;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ja:J

    :cond_0
    iget-wide v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ja:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->i:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->g:Landroid/view/View;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/exoplayer2/ui/N;->exo_controls_rewind_by_amount_description:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method

.method static synthetic p(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    return-object p0
.end method

.method private p()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/ga;->b:F

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ta:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    iget v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ua:I

    if-eq v2, v4, :cond_1

    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ta:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->sa:Ljava/util/List;

    iget v5, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ua:I

    invoke-interface {v2, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iput v4, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ua:I

    :cond_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ta:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v2

    neg-int v2, v2

    const/4 v4, 0x1

    sub-int/2addr v2, v4

    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ma:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/exoplayer2/ui/N;->exo_controls_custom_playback_speed:I

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v4, v3

    invoke-virtual {v5, v6, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ta:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->sa:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iput v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ua:I

    :cond_2
    iput v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->va:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->pa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$f;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->sa:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$f;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic q(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->da:I

    return p0
.end method

.method private q()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->oa:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Landroid/view/ViewGroup;->measure(II)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->xa:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->oa:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/PopupWindow;->setWidth(I)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->xa:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->oa:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ra:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/PopupWindow;->setHeight(I)V

    return-void
.end method

.method static synthetic r(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    return-object p0
.end method

.method private r()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->U:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    goto :goto_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    invoke-virtual {v2, v0}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;)Z

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    invoke-direct {p0, v2, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    goto :goto_2

    :cond_1
    if-nez v1, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    invoke-direct {p0, v2, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->B:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v2}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->t()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->A:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->B:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->t()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->E:Ljava/lang/String;

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->F:Ljava/lang/String;

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_2
    return-void
.end method

.method static synthetic s(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ea:Landroid/view/View;

    return-object p0
.end method

.method private s()V
    .locals 21

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-boolean v2, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->V:Z

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v2

    iget-object v5, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s:Lcom/google/android/exoplayer2/sa$b;

    invoke-static {v2, v5}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/sa$b;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v4

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    iput-boolean v2, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->W:Z

    const-wide/16 v5, 0x0

    iput-wide v5, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ia:J

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result v7

    if-nez v7, :cond_e

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->i()I

    move-result v1

    iget-boolean v7, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->W:Z

    if-eqz v7, :cond_2

    const/4 v7, 0x0

    goto :goto_1

    :cond_2
    move v7, v1

    :goto_1
    iget-boolean v8, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->W:Z

    if-eqz v8, :cond_3

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/sa;->b()I

    move-result v8

    sub-int/2addr v8, v4

    goto :goto_2

    :cond_3
    move v8, v1

    :goto_2
    move-wide v9, v5

    const/4 v11, 0x0

    :goto_3
    if-gt v7, v8, :cond_d

    if-ne v7, v1, :cond_4

    invoke-static {v9, v10}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v12

    iput-wide v12, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ia:J

    :cond_4
    iget-object v12, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {v2, v7, v12}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    iget-object v12, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s:Lcom/google/android/exoplayer2/sa$b;

    iget-wide v13, v12, Lcom/google/android/exoplayer2/sa$b;->q:J

    const-wide v15, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v13, v13, v15

    if-nez v13, :cond_5

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->W:Z

    xor-int/2addr v1, v4

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    goto/16 :goto_8

    :cond_5
    iget v12, v12, Lcom/google/android/exoplayer2/sa$b;->n:I

    :goto_4
    iget-object v13, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s:Lcom/google/android/exoplayer2/sa$b;

    iget v14, v13, Lcom/google/android/exoplayer2/sa$b;->o:I

    if-gt v12, v14, :cond_c

    iget-object v13, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->r:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v2, v12, v13}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    iget-object v13, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->r:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v13}, Lcom/google/android/exoplayer2/sa$a;->a()I

    move-result v13

    move v14, v11

    const/4 v11, 0x0

    :goto_5
    if-ge v11, v13, :cond_b

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->r:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v4, v11}, Lcom/google/android/exoplayer2/sa$a;->b(I)J

    move-result-wide v17

    const-wide/high16 v19, -0x8000000000000000L

    cmp-long v4, v17, v19

    if-nez v4, :cond_7

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->r:Lcom/google/android/exoplayer2/sa$a;

    iget-wide v3, v4, Lcom/google/android/exoplayer2/sa$a;->d:J

    cmp-long v17, v3, v15

    if-nez v17, :cond_6

    goto :goto_7

    :cond_6
    move-wide/from16 v17, v3

    :cond_7
    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->r:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/sa$a;->e()J

    move-result-wide v3

    add-long v17, v17, v3

    cmp-long v3, v17, v5

    if-ltz v3, :cond_a

    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ea:[J

    array-length v4, v3

    if-ne v14, v4, :cond_9

    array-length v4, v3

    if-nez v4, :cond_8

    const/4 v4, 0x1

    goto :goto_6

    :cond_8
    array-length v3, v3

    mul-int/lit8 v4, v3, 0x2

    :goto_6
    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ea:[J

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ea:[J

    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->fa:[Z

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([ZI)[Z

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->fa:[Z

    :cond_9
    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ea:[J

    add-long v17, v9, v17

    invoke-static/range {v17 .. v18}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v17

    aput-wide v17, v3, v14

    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->fa:[Z

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->r:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v4, v11}, Lcom/google/android/exoplayer2/sa$a;->d(I)Z

    move-result v4

    aput-boolean v4, v3, v14

    add-int/lit8 v14, v14, 0x1

    :cond_a
    :goto_7
    add-int/lit8 v11, v11, 0x1

    const/4 v4, 0x1

    goto :goto_5

    :cond_b
    add-int/lit8 v12, v12, 0x1

    move v11, v14

    const/4 v4, 0x1

    goto :goto_4

    :cond_c
    iget-wide v3, v13, Lcom/google/android/exoplayer2/sa$b;->q:J

    add-long/2addr v9, v3

    add-int/lit8 v7, v7, 0x1

    const/4 v4, 0x1

    goto/16 :goto_3

    :cond_d
    :goto_8
    move-wide v5, v9

    goto :goto_9

    :cond_e
    const/4 v11, 0x0

    :goto_9
    invoke-static {v5, v6}, Lcom/google/android/exoplayer2/C;->b(J)J

    move-result-wide v1

    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->m:Landroid/widget/TextView;

    if-eqz v3, :cond_f

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->p:Ljava/lang/StringBuilder;

    iget-object v5, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->q:Ljava/util/Formatter;

    invoke-static {v4, v5, v1, v2}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/StringBuilder;Ljava/util/Formatter;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_f
    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->o:Lcom/google/android/exoplayer2/ui/fa;

    if-eqz v3, :cond_11

    invoke-interface {v3, v1, v2}, Lcom/google/android/exoplayer2/ui/fa;->setDuration(J)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ga:[J

    array-length v1, v1

    add-int v2, v11, v1

    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ea:[J

    array-length v4, v3

    if-le v2, v4, :cond_10

    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ea:[J

    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->fa:[Z

    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([ZI)[Z

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->fa:[Z

    :cond_10
    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ga:[J

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ea:[J

    const/4 v5, 0x0

    invoke-static {v3, v5, v4, v11, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ha:[Z

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->fa:[Z

    invoke-static {v3, v5, v4, v11, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->o:Lcom/google/android/exoplayer2/ui/fa;

    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ea:[J

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->fa:[Z

    invoke-interface {v1, v3, v4, v2}, Lcom/google/android/exoplayer2/ui/fa;->setAdGroupTimesMs([J[ZI)V

    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->m()V

    return-void
.end method

.method private setPlaybackSpeed(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v1, Lcom/google/android/exoplayer2/ga;

    invoke-direct {v1, p1}, Lcom/google/android/exoplayer2/ga;-><init>(F)V

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/Player;->a(Lcom/google/android/exoplayer2/ga;)V

    return-void
.end method

.method static synthetic t(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$f;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->pa:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$f;

    return-object p0
.end method

.method private t()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->h()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->za:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;->getItemCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ca:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    return-void
.end method

.method static synthetic u(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ca:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic v(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->za:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$k;

    return-object p0
.end method

.method static synthetic w(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/graphics/drawable/Drawable;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->G:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method static synthetic x(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Landroid/graphics/drawable/Drawable;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->H:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method static synthetic y(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->I:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic z(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->J:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/ca;->a()V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$m;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Landroid/view/KeyEvent;)Z
    .locals 3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    if-eqz v1, :cond_9

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(I)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_8

    const/16 v2, 0x5a

    if-ne v0, v2, :cond_1

    invoke-interface {v1}, Lcom/google/android/exoplayer2/Player;->getPlaybackState()I

    move-result p1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_8

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    invoke-interface {p1, v1}, Lcom/google/android/exoplayer2/G;->a(Lcom/google/android/exoplayer2/Player;)Z

    goto :goto_0

    :cond_1
    const/16 v2, 0x59

    if-ne v0, v2, :cond_2

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    invoke-interface {p1, v1}, Lcom/google/android/exoplayer2/G;->b(Lcom/google/android/exoplayer2/Player;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result p1

    if-nez p1, :cond_8

    const/16 p1, 0x4f

    if-eq v0, p1, :cond_7

    const/16 p1, 0x55

    if-eq v0, p1, :cond_7

    const/16 p1, 0x57

    if-eq v0, p1, :cond_6

    const/16 p1, 0x58

    if-eq v0, p1, :cond_5

    const/16 p1, 0x7e

    if-eq v0, p1, :cond_4

    const/16 p1, 0x7f

    if-eq v0, p1, :cond_3

    goto :goto_0

    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Lcom/google/android/exoplayer2/Player;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->b(Lcom/google/android/exoplayer2/Player;)V

    goto :goto_0

    :cond_5
    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    invoke-interface {p1, v1}, Lcom/google/android/exoplayer2/G;->c(Lcom/google/android/exoplayer2/Player;)Z

    goto :goto_0

    :cond_6
    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    invoke-interface {p1, v1}, Lcom/google/android/exoplayer2/G;->d(Lcom/google/android/exoplayer2/Player;)Z

    goto :goto_0

    :cond_7
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c(Lcom/google/android/exoplayer2/Player;)V

    :cond_8
    :goto_0
    const/4 p1, 0x1

    return p1

    :cond_9
    :goto_1
    const/4 p1, 0x0

    return p1
.end method

.method public b(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$m;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/ca;->b()Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method d()V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_1
    goto :goto_7

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    invoke-interface {v1, v2}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$m;->d(I)V

    goto/32 :goto_1

    nop

    :goto_4
    return-void

    :goto_5
    if-nez v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_9

    nop

    :goto_6
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    goto/32 :goto_0

    nop

    :goto_8
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v2

    goto/32 :goto_3

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_a
    check-cast v1, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$m;

    goto/32 :goto_8

    nop

    :goto_b
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_6

    nop
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method e()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->e:Landroid/view/View;

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :goto_4
    goto/32 :goto_1

    nop
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/ca;->e()V

    return-void
.end method

.method g()V
    .locals 0

    goto/32 :goto_6

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->t()V

    goto/32 :goto_2

    nop

    :goto_2
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s()V

    goto/32 :goto_0

    nop

    :goto_3
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->n()V

    goto/32 :goto_5

    nop

    :goto_4
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k()V

    goto/32 :goto_3

    nop

    :goto_5
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->r()V

    goto/32 :goto_1

    nop

    :goto_6
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->l()V

    goto/32 :goto_4

    nop
.end method

.method public getPlayer()Lcom/google/android/exoplayer2/Player;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    return-object v0
.end method

.method public getRepeatToggleModes()I
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->da:I

    return v0
.end method

.method public getShowShuffleButton()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public getShowSubtitleButton()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ca:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public getShowTimeoutMs()I
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ba:I

    return v0
.end method

.method public getShowVrButton()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    invoke-virtual {v0, p0}, Lcom/google/android/exoplayer2/ui/ca;->a(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->U:Z

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/ca;->d()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->g()V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    invoke-virtual {v0, p0}, Lcom/google/android/exoplayer2/ui/ca;->b(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->U:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->t:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/ca;->c()V

    return-void
.end method

.method public setAnimationEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/ca;->a(Z)V

    return-void
.end method

.method public setControlDispatcher(Lcom/google/android/exoplayer2/G;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k()V

    :cond_0
    return-void
.end method

.method public setExtraAdGroupMarkers([J[Z)V
    .locals 3
    .param p1    # [J
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # [Z
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-array p1, v0, [J

    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ga:[J

    new-array p1, v0, [Z

    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ha:[Z

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p2, [Z

    array-length v1, p1

    array-length v2, p2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ga:[J

    iput-object p2, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ha:[Z

    :goto_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s()V

    return-void
.end method

.method public setOnFullScreenModeChangedListener(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$c;)V
    .locals 1
    .param p1    # Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Da:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->S:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$c;

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->S:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$c;

    if-nez p1, :cond_1

    const/16 p1, 0x8

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public setPlaybackPreparer(Lcom/google/android/exoplayer2/ha;)V
    .locals 0
    .param p1    # Lcom/google/android/exoplayer2/ha;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->R:Lcom/google/android/exoplayer2/ha;

    return-void
.end method

.method public setPlayer(Lcom/google/android/exoplayer2/Player;)V
    .locals 4
    .param p1    # Lcom/google/android/exoplayer2/Player;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Player;->s()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :cond_2
    :goto_1
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    if-ne v0, p1, :cond_3

    return-void

    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/Player;->b(Lcom/google/android/exoplayer2/Player$c;)V

    :cond_4
    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$b;

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/Player;->a(Lcom/google/android/exoplayer2/Player$c;)V

    :cond_5
    if-eqz p1, :cond_6

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Player;->e()Lcom/google/android/exoplayer2/trackselection/o;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;

    if-eqz v0, :cond_6

    invoke-interface {p1}, Lcom/google/android/exoplayer2/Player;->e()Lcom/google/android/exoplayer2/trackselection/o;

    move-result-object p1

    check-cast p1, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;

    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ya:Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;

    goto :goto_2

    :cond_6
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ya:Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->g()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->p()V

    return-void
.end method

.method public setProgressUpdateListener(Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$d;)V
    .locals 0
    .param p1    # Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$d;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Q:Lcom/google/android/exoplayer2/ui/StyledPlayerControlView$d;

    return-void
.end method

.method public setRepeatToggleModes(I)V
    .locals 5

    iput p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->da:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Player;->getRepeatMode()I

    move-result v0

    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v0, v3, v1}, Lcom/google/android/exoplayer2/G;->a(Lcom/google/android/exoplayer2/Player;I)Z

    goto :goto_0

    :cond_0
    const/4 v3, 0x2

    if-ne p1, v2, :cond_1

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v0, v3, v2}, Lcom/google/android/exoplayer2/G;->a(Lcom/google/android/exoplayer2/Player;I)Z

    goto :goto_0

    :cond_1
    if-ne p1, v3, :cond_2

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->P:Lcom/google/android/exoplayer2/G;

    iget-object v4, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->O:Lcom/google/android/exoplayer2/Player;

    invoke-interface {v0, v4, v3}, Lcom/google/android/exoplayer2/G;->a(Lcom/google/android/exoplayer2/Player;I)Z

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->j:Landroid/widget/ImageView;

    if-eqz p1, :cond_3

    move v1, v2

    :cond_3
    invoke-virtual {v0, v3, v1}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->n()V

    return-void
.end method

.method public setShowFastForwardButton(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->f:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k()V

    return-void
.end method

.method public setShowMultiWindowTimeBar(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->V:Z

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->s()V

    return-void
.end method

.method public setShowNextButton(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->d:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k()V

    return-void
.end method

.method public setShowPreviousButton(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->c:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k()V

    return-void
.end method

.method public setShowRewindButton(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->g:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k()V

    return-void
.end method

.method public setShowShuffleButton(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->r()V

    return-void
.end method

.method public setShowSubtitleButton(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->Ca:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    return-void
.end method

.method public setShowTimeoutMs(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ba:I

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/ca;->d()V

    :cond_0
    return-void
.end method

.method public setShowVrButton(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->la:Lcom/google/android/exoplayer2/ui/ca;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->l:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/ui/ca;->a(Landroid/view/View;Z)V

    return-void
.end method

.method public setTimeBarMinUpdateInterval(I)V
    .locals 2

    const/16 v0, 0x10

    const/16 v1, 0x3e8

    invoke-static {p1, v0, v1}, Lcom/google/android/exoplayer2/util/E;->a(III)I

    move-result p1

    iput p1, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->ca:I

    return-void
.end method

.method public setVrButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->l:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->l:Landroid/view/View;

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/ui/StyledPlayerControlView;->a(ZLandroid/view/View;)V

    :cond_1
    return-void
.end method
