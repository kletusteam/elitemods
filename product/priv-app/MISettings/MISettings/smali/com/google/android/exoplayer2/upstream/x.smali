.class public interface abstract Lcom/google/android/exoplayer2/upstream/x;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/upstream/x$a;
    }
.end annotation


# virtual methods
.method public abstract a(I)I
.end method

.method public a(IJLjava/io/IOException;I)J
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/x$a;)J
    .locals 7

    iget-object v0, p1, Lcom/google/android/exoplayer2/upstream/x$a;->b:Lcom/google/android/exoplayer2/source/w;

    iget v2, v0, Lcom/google/android/exoplayer2/source/w;->a:I

    iget-object v0, p1, Lcom/google/android/exoplayer2/upstream/x$a;->a:Lcom/google/android/exoplayer2/source/t;

    iget-wide v3, v0, Lcom/google/android/exoplayer2/source/t;->g:J

    iget-object v5, p1, Lcom/google/android/exoplayer2/upstream/x$a;->c:Ljava/io/IOException;

    iget v6, p1, Lcom/google/android/exoplayer2/upstream/x$a;->d:I

    move-object v1, p0

    invoke-interface/range {v1 .. v6}, Lcom/google/android/exoplayer2/upstream/x;->a(IJLjava/io/IOException;I)J

    const/4 p1, 0x0

    throw p1
.end method

.method public a(J)V
    .locals 0

    return-void
.end method
