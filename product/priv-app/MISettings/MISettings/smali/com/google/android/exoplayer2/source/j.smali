.class public abstract Lcom/google/android/exoplayer2/source/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/source/y;


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/google/android/exoplayer2/source/y$b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lcom/google/android/exoplayer2/source/y$b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/exoplayer2/source/A$a;

.field private final d:Lcom/google/android/exoplayer2/drm/c$a;

.field private e:Landroid/os/Looper;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/google/android/exoplayer2/sa;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/j;->a:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/j;->b:Ljava/util/HashSet;

    new-instance v0, Lcom/google/android/exoplayer2/source/A$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/source/A$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/j;->c:Lcom/google/android/exoplayer2/source/A$a;

    new-instance v0, Lcom/google/android/exoplayer2/drm/c$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/drm/c$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/j;->d:Lcom/google/android/exoplayer2/drm/c$a;

    return-void
.end method


# virtual methods
.method protected final a(ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/drm/c$a;
    .locals 1
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->d:Lcom/google/android/exoplayer2/drm/c$a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/drm/c$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/drm/c$a;

    move-result-object p1

    return-object p1
.end method

.method protected final a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/drm/c$a;
    .locals 2
    .param p1    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->d:Lcom/google/android/exoplayer2/drm/c$a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/drm/c$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/drm/c$a;

    move-result-object p1

    return-object p1
.end method

.method protected final a(ILcom/google/android/exoplayer2/source/y$a;J)Lcom/google/android/exoplayer2/source/A$a;
    .locals 1
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->c:Lcom/google/android/exoplayer2/source/A$a;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/source/A$a;->a(ILcom/google/android/exoplayer2/source/y$a;J)Lcom/google/android/exoplayer2/source/A$a;

    move-result-object p1

    return-object p1
.end method

.method public final a(Landroid/os/Handler;Lcom/google/android/exoplayer2/drm/c;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->d:Lcom/google/android/exoplayer2/drm/c$a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/drm/c$a;->a(Landroid/os/Handler;Lcom/google/android/exoplayer2/drm/c;)V

    return-void
.end method

.method public final a(Landroid/os/Handler;Lcom/google/android/exoplayer2/source/A;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->c:Lcom/google/android/exoplayer2/source/A$a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/source/A$a;->a(Landroid/os/Handler;Lcom/google/android/exoplayer2/source/A;)V

    return-void
.end method

.method protected final a(Lcom/google/android/exoplayer2/sa;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/j;->f:Lcom/google/android/exoplayer2/sa;

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/source/y$b;

    invoke-interface {v1, p0, p1}, Lcom/google/android/exoplayer2/source/y$b;->a(Lcom/google/android/exoplayer2/source/y;Lcom/google/android/exoplayer2/sa;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/source/A;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->c:Lcom/google/android/exoplayer2/source/A$a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/source/A$a;->a(Lcom/google/android/exoplayer2/source/A;)V

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/source/y$b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/j;->e:Landroid/os/Looper;

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/j;->f:Lcom/google/android/exoplayer2/sa;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/j;->b:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashSet;->clear()V

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/j;->h()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/source/j;->b(Lcom/google/android/exoplayer2/source/y$b;)V

    :goto_0
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/source/y$b;Lcom/google/android/exoplayer2/upstream/C;)V
    .locals 3
    .param p2    # Lcom/google/android/exoplayer2/upstream/C;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/j;->e:Landroid/os/Looper;

    if-eqz v1, :cond_1

    if-ne v1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/j;->f:Lcom/google/android/exoplayer2/sa;

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/j;->e:Landroid/os/Looper;

    if-nez v2, :cond_2

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/j;->e:Landroid/os/Looper;

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p2}, Lcom/google/android/exoplayer2/source/j;->a(Lcom/google/android/exoplayer2/upstream/C;)V

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/source/j;->c(Lcom/google/android/exoplayer2/source/y$b;)V

    invoke-interface {p1, p0, v1}, Lcom/google/android/exoplayer2/source/y$b;->a(Lcom/google/android/exoplayer2/source/y;Lcom/google/android/exoplayer2/sa;)V

    :cond_3
    :goto_2
    return-void
.end method

.method protected abstract a(Lcom/google/android/exoplayer2/upstream/C;)V
    .param p1    # Lcom/google/android/exoplayer2/upstream/C;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method protected final b(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/source/A$a;
    .locals 4
    .param p1    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->c:Lcom/google/android/exoplayer2/source/A$a;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/google/android/exoplayer2/source/A$a;->a(ILcom/google/android/exoplayer2/source/y$a;J)Lcom/google/android/exoplayer2/source/A$a;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lcom/google/android/exoplayer2/source/y$b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/j;->b:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/j;->b:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/j;->e()V

    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/exoplayer2/source/y$b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->e:Landroid/os/Looper;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/j;->b:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/j;->f()V

    :cond_0
    return-void
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method protected f()V
    .locals 0

    return-void
.end method

.method protected final g()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/j;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected abstract h()V
.end method
