.class public final Lcom/google/android/exoplayer2/source/K;
.super Lcom/google/android/exoplayer2/sa;


# static fields
.field private static final b:Ljava/lang/Object;

.field private static final c:Lcom/google/android/exoplayer2/W;


# instance fields
.field private final d:J

.field private final e:J

.field private final f:J

.field private final g:J

.field private final h:J

.field private final i:J

.field private final j:J

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private final n:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final o:Lcom/google/android/exoplayer2/W;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer2/source/K;->b:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/exoplayer2/W$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/W$a;-><init>()V

    const-string v1, "com.google.android.exoplayer2.source.SinglePeriodTimeline"

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/W$a;->b(Ljava/lang/String;)Lcom/google/android/exoplayer2/W$a;

    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/W$a;->a(Landroid/net/Uri;)Lcom/google/android/exoplayer2/W$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/W$a;->a()Lcom/google/android/exoplayer2/W;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/K;->c:Lcom/google/android/exoplayer2/W;

    return-void
.end method

.method public constructor <init>(JJJJJJJZZZLjava/lang/Object;Lcom/google/android/exoplayer2/W;)V
    .locals 3
    .param p18    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object v0, p0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/sa;-><init>()V

    move-wide v1, p1

    iput-wide v1, v0, Lcom/google/android/exoplayer2/source/K;->d:J

    move-wide v1, p3

    iput-wide v1, v0, Lcom/google/android/exoplayer2/source/K;->e:J

    move-wide v1, p5

    iput-wide v1, v0, Lcom/google/android/exoplayer2/source/K;->f:J

    move-wide v1, p7

    iput-wide v1, v0, Lcom/google/android/exoplayer2/source/K;->g:J

    move-wide v1, p9

    iput-wide v1, v0, Lcom/google/android/exoplayer2/source/K;->h:J

    move-wide v1, p11

    iput-wide v1, v0, Lcom/google/android/exoplayer2/source/K;->i:J

    move-wide/from16 v1, p13

    iput-wide v1, v0, Lcom/google/android/exoplayer2/source/K;->j:J

    move/from16 v1, p15

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/source/K;->k:Z

    move/from16 v1, p16

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/source/K;->l:Z

    move/from16 v1, p17

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/source/K;->m:Z

    move-object/from16 v1, p18

    iput-object v1, v0, Lcom/google/android/exoplayer2/source/K;->n:Ljava/lang/Object;

    invoke-static/range {p19 .. p19}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v1, p19

    check-cast v1, Lcom/google/android/exoplayer2/W;

    iput-object v1, v0, Lcom/google/android/exoplayer2/source/K;->o:Lcom/google/android/exoplayer2/W;

    return-void
.end method

.method public constructor <init>(JJJJZZZLjava/lang/Object;Lcom/google/android/exoplayer2/W;)V
    .locals 20
    .param p12    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-wide/from16 v7, p1

    move-wide/from16 v9, p3

    move-wide/from16 v11, p5

    move-wide/from16 v13, p7

    move/from16 v15, p9

    move/from16 v16, p10

    move/from16 v17, p11

    move-object/from16 v18, p12

    move-object/from16 v19, p13

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    invoke-direct/range {v0 .. v19}, Lcom/google/android/exoplayer2/source/K;-><init>(JJJJJJJZZZLjava/lang/Object;Lcom/google/android/exoplayer2/W;)V

    return-void
.end method

.method public constructor <init>(JZZZLjava/lang/Object;Lcom/google/android/exoplayer2/W;)V
    .locals 14
    .param p6    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p1

    move/from16 v9, p3

    move/from16 v10, p4

    move/from16 v11, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    invoke-direct/range {v0 .. v13}, Lcom/google/android/exoplayer2/source/K;-><init>(JJJJZZZLjava/lang/Object;Lcom/google/android/exoplayer2/W;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/Object;)I
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/source/K;->b:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public a(ILcom/google/android/exoplayer2/sa$a;Z)Lcom/google/android/exoplayer2/sa$a;
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/google/android/exoplayer2/util/d;->a(III)I

    if-eqz p3, :cond_0

    sget-object p1, Lcom/google/android/exoplayer2/source/K;->b:Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v2, p1

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/K;->g:J

    iget-wide v6, p0, Lcom/google/android/exoplayer2/source/K;->i:J

    neg-long v6, v6

    move-object v0, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/exoplayer2/sa$a;->a(Ljava/lang/Object;Ljava/lang/Object;IJJ)Lcom/google/android/exoplayer2/sa$a;

    return-object p2
.end method

.method public a(ILcom/google/android/exoplayer2/sa$b;J)Lcom/google/android/exoplayer2/sa$b;
    .locals 27

    move-object/from16 v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    move/from16 v3, p1

    invoke-static {v3, v1, v2}, Lcom/google/android/exoplayer2/util/d;->a(III)I

    iget-wide v1, v0, Lcom/google/android/exoplayer2/source/K;->j:J

    iget-boolean v3, v0, Lcom/google/android/exoplayer2/source/K;->l:Z

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz v3, :cond_1

    const-wide/16 v6, 0x0

    cmp-long v3, p3, v6

    if-eqz v3, :cond_1

    iget-wide v6, v0, Lcom/google/android/exoplayer2/source/K;->h:J

    cmp-long v3, v6, v4

    if-nez v3, :cond_0

    :goto_0
    move-wide/from16 v19, v4

    goto :goto_1

    :cond_0
    add-long v1, v1, p3

    cmp-long v3, v1, v6

    if-lez v3, :cond_1

    goto :goto_0

    :cond_1
    move-wide/from16 v19, v1

    :goto_1
    sget-object v7, Lcom/google/android/exoplayer2/sa$b;->a:Ljava/lang/Object;

    iget-object v8, v0, Lcom/google/android/exoplayer2/source/K;->o:Lcom/google/android/exoplayer2/W;

    iget-object v9, v0, Lcom/google/android/exoplayer2/source/K;->n:Ljava/lang/Object;

    iget-wide v10, v0, Lcom/google/android/exoplayer2/source/K;->d:J

    iget-wide v12, v0, Lcom/google/android/exoplayer2/source/K;->e:J

    iget-wide v14, v0, Lcom/google/android/exoplayer2/source/K;->f:J

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/source/K;->k:Z

    move/from16 v16, v1

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/source/K;->l:Z

    move/from16 v17, v1

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/source/K;->m:Z

    move/from16 v18, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/source/K;->h:J

    move-wide/from16 v21, v1

    const/16 v23, 0x0

    const/16 v24, 0x0

    iget-wide v1, v0, Lcom/google/android/exoplayer2/source/K;->i:J

    move-wide/from16 v25, v1

    move-object/from16 v6, p2

    invoke-virtual/range {v6 .. v26}, Lcom/google/android/exoplayer2/sa$b;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/W;Ljava/lang/Object;JJJZZZJJIIJ)Lcom/google/android/exoplayer2/sa$b;

    return-object p2
.end method

.method public a(I)Ljava/lang/Object;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/google/android/exoplayer2/util/d;->a(III)I

    sget-object p1, Lcom/google/android/exoplayer2/source/K;->b:Ljava/lang/Object;

    return-object p1
.end method

.method public b()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
