.class public final Lcom/google/android/exoplayer2/source/u;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/source/x;
.implements Lcom/google/android/exoplayer2/source/x$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/u$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/google/android/exoplayer2/source/y;

.field public final b:Lcom/google/android/exoplayer2/source/y$a;

.field private final c:Lcom/google/android/exoplayer2/upstream/e;

.field private d:Lcom/google/android/exoplayer2/source/x;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/google/android/exoplayer2/source/x$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/google/android/exoplayer2/source/u$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/source/y;Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/upstream/e;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/u;->b:Lcom/google/android/exoplayer2/source/y$a;

    iput-object p3, p0, Lcom/google/android/exoplayer2/source/u;->c:Lcom/google/android/exoplayer2/upstream/e;

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/u;->a:Lcom/google/android/exoplayer2/source/y;

    iput-wide p4, p0, Lcom/google/android/exoplayer2/source/u;->f:J

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/u;->i:J

    return-void
.end method

.method private e(J)J
    .locals 4

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/u;->i:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    move-wide p1, v0

    :cond_0
    return-wide p1
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/x;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(J)J
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/source/x;->a(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(JLcom/google/android/exoplayer2/ma;)J
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/source/x;->a(JLcom/google/android/exoplayer2/ma;)J

    move-result-wide p1

    return-wide p1
.end method

.method public a([Lcom/google/android/exoplayer2/trackselection/l;[Z[Lcom/google/android/exoplayer2/source/SampleStream;[ZJ)J
    .locals 13

    move-object v0, p0

    iget-wide v1, v0, Lcom/google/android/exoplayer2/source/u;->i:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v5, v1, v3

    if-eqz v5, :cond_0

    iget-wide v5, v0, Lcom/google/android/exoplayer2/source/u;->f:J

    cmp-long v5, p5, v5

    if-nez v5, :cond_0

    iput-wide v3, v0, Lcom/google/android/exoplayer2/source/u;->i:J

    move-wide v11, v1

    goto :goto_0

    :cond_0
    move-wide/from16 v11, p5

    :goto_0
    iget-object v1, v0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v6, v1

    check-cast v6, Lcom/google/android/exoplayer2/source/x;

    move-object v7, p1

    move-object v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    invoke-interface/range {v6 .. v12}, Lcom/google/android/exoplayer2/source/x;->a([Lcom/google/android/exoplayer2/trackselection/l;[Z[Lcom/google/android/exoplayer2/source/SampleStream;[ZJ)J

    move-result-wide v1

    return-wide v1
.end method

.method public a(JZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/source/x;->a(JZ)V

    return-void
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/source/I;)V
    .locals 0

    check-cast p1, Lcom/google/android/exoplayer2/source/x;

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/source/u;->b(Lcom/google/android/exoplayer2/source/x;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/x$a;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/u;->e:Lcom/google/android/exoplayer2/source/x$a;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    if-eqz p1, :cond_0

    iget-wide p2, p0, Lcom/google/android/exoplayer2/source/u;->f:J

    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer2/source/u;->e(J)J

    move-result-wide p2

    invoke-interface {p1, p0, p2, p3}, Lcom/google/android/exoplayer2/source/x;->a(Lcom/google/android/exoplayer2/source/x$a;J)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/x;)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/u;->e:Lcom/google/android/exoplayer2/source/x$a;

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/source/x$a;

    invoke-interface {p1, p0}, Lcom/google/android/exoplayer2/source/x$a;->a(Lcom/google/android/exoplayer2/source/x;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/u;->g:Lcom/google/android/exoplayer2/source/u$a;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->b:Lcom/google/android/exoplayer2/source/y$a;

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/source/u$a;->a(Lcom/google/android/exoplayer2/source/y$a;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/y$a;)V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/u;->f:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/source/u;->e(J)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/u;->a:Lcom/google/android/exoplayer2/source/y;

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/u;->c:Lcom/google/android/exoplayer2/upstream/e;

    invoke-interface {v2, p1, v3, v0, v1}, Lcom/google/android/exoplayer2/source/y;->a(Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/upstream/e;J)Lcom/google/android/exoplayer2/source/x;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/u;->e:Lcom/google/android/exoplayer2/source/x$a;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    invoke-interface {p1, p0, v0, v1}, Lcom/google/android/exoplayer2/source/x;->a(Lcom/google/android/exoplayer2/source/x$a;J)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/x;->b()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->a:Lcom/google/android/exoplayer2/source/y;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/y;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/u;->g:Lcom/google/android/exoplayer2/source/u$a;

    if-eqz v1, :cond_2

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/source/u;->h:Z

    if-nez v2, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/source/u;->h:Z

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/u;->b:Lcom/google/android/exoplayer2/source/y$a;

    invoke-interface {v1, v2, v0}, Lcom/google/android/exoplayer2/source/u$a;->a(Lcom/google/android/exoplayer2/source/y$a;Ljava/io/IOException;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    throw v0
.end method

.method public b(Lcom/google/android/exoplayer2/source/x;)V
    .locals 0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/u;->e:Lcom/google/android/exoplayer2/source/x$a;

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/source/x$a;

    invoke-interface {p1, p0}, Lcom/google/android/exoplayer2/source/I$a;->a(Lcom/google/android/exoplayer2/source/I;)V

    return-void
.end method

.method public b(J)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/source/x;->b(J)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public c(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/source/x;->c(J)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/x;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/x;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public d(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/u;->i:J

    return-void
.end method

.method public e()Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/x;->e()Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/x;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/x;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public g()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/u;->i:J

    return-wide v0
.end method

.method public h()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/u;->f:J

    return-wide v0
.end method

.method public i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/u;->d:Lcom/google/android/exoplayer2/source/x;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/u;->a:Lcom/google/android/exoplayer2/source/y;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/source/y;->a(Lcom/google/android/exoplayer2/source/x;)V

    :cond_0
    return-void
.end method
