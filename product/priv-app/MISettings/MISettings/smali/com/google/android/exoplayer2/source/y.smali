.class public interface abstract Lcom/google/android/exoplayer2/source/y;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/y$a;,
        Lcom/google/android/exoplayer2/source/y$b;
    }
.end annotation


# virtual methods
.method public abstract a()Lcom/google/android/exoplayer2/W;
.end method

.method public abstract a(Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/upstream/e;J)Lcom/google/android/exoplayer2/source/x;
.end method

.method public abstract a(Landroid/os/Handler;Lcom/google/android/exoplayer2/drm/c;)V
.end method

.method public abstract a(Landroid/os/Handler;Lcom/google/android/exoplayer2/source/A;)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/source/A;)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/source/x;)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/source/y$b;)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/source/y$b;Lcom/google/android/exoplayer2/upstream/C;)V
    .param p2    # Lcom/google/android/exoplayer2/upstream/C;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract b()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract b(Lcom/google/android/exoplayer2/source/y$b;)V
.end method

.method public abstract c(Lcom/google/android/exoplayer2/source/y$b;)V
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()Lcom/google/android/exoplayer2/sa;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method
