.class final Lcom/google/android/exoplayer2/ui/spherical/e;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/ui/spherical/e$a;
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[F

.field private static final d:[F

.field private static final e:[F

.field private static final f:[F

.field private static final g:[F


# instance fields
.field private h:I

.field private i:Lcom/google/android/exoplayer2/ui/spherical/e$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/google/android/exoplayer2/ui/spherical/e$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const-string v0, "uniform mat4 uMvpMatrix;"

    const-string v1, "uniform mat3 uTexMatrix;"

    const-string v2, "attribute vec4 aPosition;"

    const-string v3, "attribute vec2 aTexCoords;"

    const-string v4, "varying vec2 vTexCoords;"

    const-string v5, "void main() {"

    const-string v6, "  gl_Position = uMvpMatrix * aPosition;"

    const-string v7, "  vTexCoords = (uTexMatrix * vec3(aTexCoords, 1)).xy;"

    const-string v8, "}"

    filled-new-array/range {v0 .. v8}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/ui/spherical/e;->a:[Ljava/lang/String;

    const-string v1, "#extension GL_OES_EGL_image_external : require"

    const-string v2, "precision mediump float;"

    const-string v3, "uniform samplerExternalOES uTexture;"

    const-string v4, "varying vec2 vTexCoords;"

    const-string v5, "void main() {"

    const-string v6, "  gl_FragColor = texture2D(uTexture, vTexCoords);"

    const-string v7, "}"

    filled-new-array/range {v1 .. v7}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/ui/spherical/e;->b:[Ljava/lang/String;

    const/16 v0, 0x9

    new-array v1, v0, [F

    fill-array-data v1, :array_0

    sput-object v1, Lcom/google/android/exoplayer2/ui/spherical/e;->c:[F

    new-array v1, v0, [F

    fill-array-data v1, :array_1

    sput-object v1, Lcom/google/android/exoplayer2/ui/spherical/e;->d:[F

    new-array v1, v0, [F

    fill-array-data v1, :array_2

    sput-object v1, Lcom/google/android/exoplayer2/ui/spherical/e;->e:[F

    new-array v1, v0, [F

    fill-array-data v1, :array_3

    sput-object v1, Lcom/google/android/exoplayer2/ui/spherical/e;->f:[F

    new-array v0, v0, [F

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/exoplayer2/ui/spherical/e;->g:[F

    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        -0x40800000    # -1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        -0x41000000    # -0.5f
        0x0
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        -0x41000000    # -0.5f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    :array_3
    .array-data 4
        0x3f000000    # 0.5f
        0x0
        0x0
        0x0
        -0x40800000    # -1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    :array_4
    .array-data 4
        0x3f000000    # 0.5f
        0x0
        0x0
        0x0
        -0x40800000    # -1.0f
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/exoplayer2/video/spherical/Projection;)Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/video/spherical/Projection;->a:Lcom/google/android/exoplayer2/video/spherical/Projection$a;

    iget-object p0, p0, Lcom/google/android/exoplayer2/video/spherical/Projection;->b:Lcom/google/android/exoplayer2/video/spherical/Projection$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/video/spherical/Projection$a;->a()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/video/spherical/Projection$a;->a(I)Lcom/google/android/exoplayer2/video/spherical/Projection$b;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/video/spherical/Projection$b;->a:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/video/spherical/Projection$a;->a()I

    move-result v0

    if-ne v0, v3, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer2/video/spherical/Projection$a;->a(I)Lcom/google/android/exoplayer2/video/spherical/Projection$b;

    move-result-object p0

    iget p0, p0, Lcom/google/android/exoplayer2/video/spherical/Projection$b;->a:I

    if-nez p0, :cond_0

    move v2, v3

    :cond_0
    return v2
.end method


# virtual methods
.method a()V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    goto/32 :goto_17

    nop

    :goto_1
    iget v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->k:I

    goto/32 :goto_15

    nop

    :goto_2
    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/util/k;->a([Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/32 :goto_9

    nop

    :goto_3
    const-string v1, "uTexMatrix"

    goto/32 :goto_4

    nop

    :goto_4
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    goto/32 :goto_12

    nop

    :goto_5
    iput v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->n:I

    goto/32 :goto_11

    nop

    :goto_6
    iput v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->l:I

    goto/32 :goto_f

    nop

    :goto_7
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_8
    iget v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->k:I

    goto/32 :goto_10

    nop

    :goto_9
    iput v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->k:I

    goto/32 :goto_1

    nop

    :goto_a
    sget-object v0, Lcom/google/android/exoplayer2/ui/spherical/e;->a:[Ljava/lang/String;

    goto/32 :goto_14

    nop

    :goto_b
    const-string v1, "aPosition"

    goto/32 :goto_c

    nop

    :goto_c
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    goto/32 :goto_5

    nop

    :goto_d
    iput v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->o:I

    goto/32 :goto_8

    nop

    :goto_e
    iget v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->k:I

    goto/32 :goto_b

    nop

    :goto_f
    iget v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->k:I

    goto/32 :goto_3

    nop

    :goto_10
    const-string v1, "uTexture"

    goto/32 :goto_0

    nop

    :goto_11
    iget v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->k:I

    goto/32 :goto_18

    nop

    :goto_12
    iput v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->m:I

    goto/32 :goto_e

    nop

    :goto_13
    return-void

    :goto_14
    sget-object v1, Lcom/google/android/exoplayer2/ui/spherical/e;->b:[Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_15
    const-string v1, "uMvpMatrix"

    goto/32 :goto_7

    nop

    :goto_16
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    goto/32 :goto_d

    nop

    :goto_17
    iput v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->p:I

    goto/32 :goto_13

    nop

    :goto_18
    const-string v1, "aTexCoords"

    goto/32 :goto_16

    nop
.end method

.method a(I[FZ)V
    .locals 18

    goto/32 :goto_a

    nop

    :goto_0
    sget-object v2, Lcom/google/android/exoplayer2/ui/spherical/e;->d:[F

    goto/32 :goto_3f

    nop

    :goto_1
    invoke-static/range {v12 .. v17}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    goto/32 :goto_34

    nop

    :goto_2
    const v2, 0x8d65

    goto/32 :goto_20

    nop

    :goto_3
    iget-object v1, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->i:Lcom/google/android/exoplayer2/ui/spherical/e$a;

    :goto_4
    goto/32 :goto_30

    nop

    :goto_5
    if-eq v2, v4, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_18

    nop

    :goto_6
    invoke-static {v2, v3, v5, v4, v5}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    goto/32 :goto_39

    nop

    :goto_7
    const/4 v9, 0x0

    goto/32 :goto_36

    nop

    :goto_8
    goto/16 :goto_23

    :goto_9
    goto/32 :goto_22

    nop

    :goto_a
    move-object/from16 v0, p0

    goto/32 :goto_2b

    nop

    :goto_b
    invoke-static {}, Lcom/google/android/exoplayer2/util/k;->a()V

    goto/32 :goto_1a

    nop

    :goto_c
    goto :goto_23

    :goto_d
    goto/32 :goto_2a

    nop

    :goto_e
    iget v12, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->o:I

    goto/32 :goto_16

    nop

    :goto_f
    invoke-static {v2}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    goto/32 :goto_b

    nop

    :goto_10
    iget v2, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->l:I

    goto/32 :goto_33

    nop

    :goto_11
    iget v2, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->n:I

    goto/32 :goto_42

    nop

    :goto_12
    return-void

    :goto_13
    goto/32 :goto_24

    nop

    :goto_14
    invoke-static {v2, v5}, Landroid/opengl/GLES20;->glUniform1i(II)V

    goto/32 :goto_4c

    nop

    :goto_15
    invoke-static {v2}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    goto/32 :goto_2

    nop

    :goto_16
    const/4 v13, 0x2

    goto/32 :goto_46

    nop

    :goto_17
    const/4 v5, 0x0

    goto/32 :goto_38

    nop

    :goto_18
    if-nez p3, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_4d

    nop

    :goto_19
    invoke-static {v1}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    goto/32 :goto_3a

    nop

    :goto_1a
    iget v2, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->h:I

    goto/32 :goto_47

    nop

    :goto_1b
    goto :goto_23

    :goto_1c
    goto/32 :goto_0

    nop

    :goto_1d
    invoke-static {v1}, Lcom/google/android/exoplayer2/ui/spherical/e$a;->a(Lcom/google/android/exoplayer2/ui/spherical/e$a;)Ljava/nio/FloatBuffer;

    move-result-object v11

    goto/32 :goto_29

    nop

    :goto_1e
    const/4 v4, 0x2

    goto/32 :goto_5

    nop

    :goto_1f
    iget v2, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->p:I

    goto/32 :goto_14

    nop

    :goto_20
    move/from16 v3, p1

    goto/32 :goto_4b

    nop

    :goto_21
    if-nez p3, :cond_2

    goto/32 :goto_1c

    :cond_2
    goto/32 :goto_37

    nop

    :goto_22
    sget-object v2, Lcom/google/android/exoplayer2/ui/spherical/e;->c:[F

    :goto_23
    goto/32 :goto_3c

    nop

    :goto_24
    iget v2, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->k:I

    goto/32 :goto_2c

    nop

    :goto_25
    invoke-static {v1}, Lcom/google/android/exoplayer2/ui/spherical/e$a;->d(Lcom/google/android/exoplayer2/ui/spherical/e$a;)I

    move-result v1

    goto/32 :goto_31

    nop

    :goto_26
    invoke-static {v1}, Lcom/google/android/exoplayer2/ui/spherical/e$a;->c(Lcom/google/android/exoplayer2/ui/spherical/e$a;)I

    move-result v2

    goto/32 :goto_25

    nop

    :goto_27
    iget v1, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->n:I

    goto/32 :goto_19

    nop

    :goto_28
    iget-object v1, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->j:Lcom/google/android/exoplayer2/ui/spherical/e$a;

    goto/32 :goto_2e

    nop

    :goto_29
    invoke-static/range {v6 .. v11}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    goto/32 :goto_3e

    nop

    :goto_2a
    sget-object v2, Lcom/google/android/exoplayer2/ui/spherical/e;->f:[F

    goto/32 :goto_8

    nop

    :goto_2b
    if-nez p3, :cond_3

    goto/32 :goto_2f

    :cond_3
    goto/32 :goto_28

    nop

    :goto_2c
    invoke-static {v2}, Landroid/opengl/GLES20;->glUseProgram(I)V

    goto/32 :goto_48

    nop

    :goto_2d
    if-eq v2, v3, :cond_4

    goto/32 :goto_40

    :cond_4
    goto/32 :goto_21

    nop

    :goto_2e
    goto/16 :goto_4

    :goto_2f
    goto/32 :goto_3

    nop

    :goto_30
    if-eqz v1, :cond_5

    goto/32 :goto_13

    :cond_5
    goto/32 :goto_12

    nop

    :goto_31
    invoke-static {v2, v5, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    goto/32 :goto_35

    nop

    :goto_32
    const/16 v16, 0x8

    goto/32 :goto_4a

    nop

    :goto_33
    move-object/from16 v4, p2

    goto/32 :goto_6

    nop

    :goto_34
    invoke-static {}, Lcom/google/android/exoplayer2/util/k;->a()V

    goto/32 :goto_26

    nop

    :goto_35
    invoke-static {}, Lcom/google/android/exoplayer2/util/k;->a()V

    goto/32 :goto_27

    nop

    :goto_36
    const/16 v10, 0xc

    goto/32 :goto_1d

    nop

    :goto_37
    sget-object v2, Lcom/google/android/exoplayer2/ui/spherical/e;->e:[F

    goto/32 :goto_1b

    nop

    :goto_38
    invoke-static {v4, v3, v5, v2, v5}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZ[FI)V

    goto/32 :goto_10

    nop

    :goto_39
    const v2, 0x84c0

    goto/32 :goto_15

    nop

    :goto_3a
    iget v1, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->o:I

    goto/32 :goto_49

    nop

    :goto_3b
    return-void

    :goto_3c
    iget v4, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->m:I

    goto/32 :goto_17

    nop

    :goto_3d
    iget v2, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->o:I

    goto/32 :goto_f

    nop

    :goto_3e
    invoke-static {}, Lcom/google/android/exoplayer2/util/k;->a()V

    goto/32 :goto_e

    nop

    :goto_3f
    goto/16 :goto_23

    :goto_40
    goto/32 :goto_1e

    nop

    :goto_41
    const/4 v7, 0x3

    goto/32 :goto_43

    nop

    :goto_42
    invoke-static {v2}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    goto/32 :goto_3d

    nop

    :goto_43
    const/16 v8, 0x1406

    goto/32 :goto_7

    nop

    :goto_44
    iget v6, v0, Lcom/google/android/exoplayer2/ui/spherical/e;->n:I

    goto/32 :goto_41

    nop

    :goto_45
    const/4 v15, 0x0

    goto/32 :goto_32

    nop

    :goto_46
    const/16 v14, 0x1406

    goto/32 :goto_45

    nop

    :goto_47
    const/4 v3, 0x1

    goto/32 :goto_2d

    nop

    :goto_48
    invoke-static {}, Lcom/google/android/exoplayer2/util/k;->a()V

    goto/32 :goto_11

    nop

    :goto_49
    invoke-static {v1}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    goto/32 :goto_3b

    nop

    :goto_4a
    invoke-static {v1}, Lcom/google/android/exoplayer2/ui/spherical/e$a;->b(Lcom/google/android/exoplayer2/ui/spherical/e$a;)Ljava/nio/FloatBuffer;

    move-result-object v17

    goto/32 :goto_1

    nop

    :goto_4b
    invoke-static {v2, v3}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto/32 :goto_1f

    nop

    :goto_4c
    invoke-static {}, Lcom/google/android/exoplayer2/util/k;->a()V

    goto/32 :goto_44

    nop

    :goto_4d
    sget-object v2, Lcom/google/android/exoplayer2/ui/spherical/e;->g:[F

    goto/32 :goto_c

    nop
.end method

.method public b(Lcom/google/android/exoplayer2/video/spherical/Projection;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/exoplayer2/ui/spherical/e;->a(Lcom/google/android/exoplayer2/video/spherical/Projection;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v0, p1, Lcom/google/android/exoplayer2/video/spherical/Projection;->c:I

    iput v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->h:I

    new-instance v0, Lcom/google/android/exoplayer2/ui/spherical/e$a;

    iget-object v1, p1, Lcom/google/android/exoplayer2/video/spherical/Projection;->a:Lcom/google/android/exoplayer2/video/spherical/Projection$a;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/video/spherical/Projection$a;->a(I)Lcom/google/android/exoplayer2/video/spherical/Projection$b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ui/spherical/e$a;-><init>(Lcom/google/android/exoplayer2/video/spherical/Projection$b;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->i:Lcom/google/android/exoplayer2/ui/spherical/e$a;

    iget-boolean v0, p1, Lcom/google/android/exoplayer2/video/spherical/Projection;->d:Z

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->i:Lcom/google/android/exoplayer2/ui/spherical/e$a;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/exoplayer2/ui/spherical/e$a;

    iget-object p1, p1, Lcom/google/android/exoplayer2/video/spherical/Projection;->b:Lcom/google/android/exoplayer2/video/spherical/Projection$a;

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/video/spherical/Projection$a;->a(I)Lcom/google/android/exoplayer2/video/spherical/Projection$b;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/ui/spherical/e$a;-><init>(Lcom/google/android/exoplayer2/video/spherical/Projection$b;)V

    move-object p1, v0

    :goto_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/spherical/e;->j:Lcom/google/android/exoplayer2/ui/spherical/e$a;

    return-void
.end method
