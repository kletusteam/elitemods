.class final Lcom/google/android/exoplayer2/ui/Q$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/Q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/google/android/exoplayer2/ui/Q$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/google/android/exoplayer2/ui/Q$b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:I

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/ui/e;->a:Lcom/google/android/exoplayer2/ui/e;

    sput-object v0, Lcom/google/android/exoplayer2/ui/Q$b;->a:Ljava/util/Comparator;

    sget-object v0, Lcom/google/android/exoplayer2/ui/f;->a:Lcom/google/android/exoplayer2/ui/f;

    sput-object v0, Lcom/google/android/exoplayer2/ui/Q$b;->b:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(IILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/exoplayer2/ui/Q$b;->c:I

    iput p2, p0, Lcom/google/android/exoplayer2/ui/Q$b;->d:I

    iput-object p3, p0, Lcom/google/android/exoplayer2/ui/Q$b;->e:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/exoplayer2/ui/Q$b;->f:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/exoplayer2/ui/P;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/ui/Q$b;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/Q$b;Lcom/google/android/exoplayer2/ui/Q$b;)I
    .locals 2

    iget v0, p1, Lcom/google/android/exoplayer2/ui/Q$b;->d:I

    iget v1, p0, Lcom/google/android/exoplayer2/ui/Q$b;->d:I

    invoke-static {v0, v1}, Ljava/lang/Integer;->compare(II)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/Q$b;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/exoplayer2/ui/Q$b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    :cond_1
    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/Q$b;->f:Ljava/lang/String;

    iget-object p1, p1, Lcom/google/android/exoplayer2/ui/Q$b;->f:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method static synthetic a()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/ui/Q$b;->b:Ljava/util/Comparator;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/ui/Q$b;Lcom/google/android/exoplayer2/ui/Q$b;)I
    .locals 2

    iget v0, p1, Lcom/google/android/exoplayer2/ui/Q$b;->c:I

    iget v1, p0, Lcom/google/android/exoplayer2/ui/Q$b;->c:I

    invoke-static {v0, v1}, Ljava/lang/Integer;->compare(II)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/exoplayer2/ui/Q$b;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/Q$b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    :cond_1
    iget-object p1, p1, Lcom/google/android/exoplayer2/ui/Q$b;->f:Ljava/lang/String;

    iget-object p0, p0, Lcom/google/android/exoplayer2/ui/Q$b;->f:Ljava/lang/String;

    invoke-virtual {p1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method static synthetic b()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/ui/Q$b;->a:Ljava/util/Comparator;

    return-object v0
.end method
