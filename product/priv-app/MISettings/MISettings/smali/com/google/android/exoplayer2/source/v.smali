.class public final Lcom/google/android/exoplayer2/source/v;
.super Lcom/google/android/exoplayer2/source/m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/v$b;,
        Lcom/google/android/exoplayer2/source/v$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/exoplayer2/source/m<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final j:Lcom/google/android/exoplayer2/source/y;

.field private final k:Z

.field private final l:Lcom/google/android/exoplayer2/sa$b;

.field private final m:Lcom/google/android/exoplayer2/sa$a;

.field private n:Lcom/google/android/exoplayer2/source/v$a;

.field private o:Lcom/google/android/exoplayer2/source/u;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:Z

.field private r:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/source/y;Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/m;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/v;->j:Lcom/google/android/exoplayer2/source/y;

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/google/android/exoplayer2/source/y;->c()Z

    move-result p2

    if-eqz p2, :cond_0

    move p2, v0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    iput-boolean p2, p0, Lcom/google/android/exoplayer2/source/v;->k:Z

    new-instance p2, Lcom/google/android/exoplayer2/sa$b;

    invoke-direct {p2}, Lcom/google/android/exoplayer2/sa$b;-><init>()V

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/v;->l:Lcom/google/android/exoplayer2/sa$b;

    new-instance p2, Lcom/google/android/exoplayer2/sa$a;

    invoke-direct {p2}, Lcom/google/android/exoplayer2/sa$a;-><init>()V

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/v;->m:Lcom/google/android/exoplayer2/sa$a;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/source/y;->d()Lcom/google/android/exoplayer2/sa;

    move-result-object p2

    if-eqz p2, :cond_1

    const/4 p1, 0x0

    invoke-static {p2, p1, p1}, Lcom/google/android/exoplayer2/source/v$a;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/exoplayer2/source/v$a;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/v;->r:Z

    goto :goto_1

    :cond_1
    invoke-interface {p1}, Lcom/google/android/exoplayer2/source/y;->a()Lcom/google/android/exoplayer2/W;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/exoplayer2/source/v$a;->a(Lcom/google/android/exoplayer2/W;)Lcom/google/android/exoplayer2/source/v$a;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    :goto_1
    return-void
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/source/v$a;->a(Lcom/google/android/exoplayer2/source/v$a;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/source/v$a;->a(Lcom/google/android/exoplayer2/source/v$a;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/google/android/exoplayer2/source/v$a;->c:Ljava/lang/Object;

    :cond_0
    return-object p1
.end method

.method private a(J)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/v;->o:Lcom/google/android/exoplayer2/source/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    iget-object v2, v0, Lcom/google/android/exoplayer2/source/u;->b:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/source/v$a;->a(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/v;->m:Lcom/google/android/exoplayer2/sa$a;

    invoke-virtual {v2, v1, v3}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$a;)Lcom/google/android/exoplayer2/sa$a;

    move-result-object v1

    iget-wide v1, v1, Lcom/google/android/exoplayer2/sa$a;->d:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v3, v1, v3

    if-eqz v3, :cond_1

    cmp-long v3, p1, v1

    if-ltz v3, :cond_1

    const-wide/16 p1, 0x0

    const-wide/16 v3, 0x1

    sub-long/2addr v1, v3

    invoke-static {p1, p2, v1, v2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    :cond_1
    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/source/u;->d(J)V

    return-void
.end method

.method private b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/source/v$a;->a(Lcom/google/android/exoplayer2/source/v$a;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/exoplayer2/source/v$a;->c:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    invoke-static {p1}, Lcom/google/android/exoplayer2/source/v$a;->a(Lcom/google/android/exoplayer2/source/v$a;)Ljava/lang/Object;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public a()Lcom/google/android/exoplayer2/W;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/v;->j:Lcom/google/android/exoplayer2/source/y;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/y;->a()Lcom/google/android/exoplayer2/W;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/upstream/e;J)Lcom/google/android/exoplayer2/source/u;
    .locals 7

    new-instance v6, Lcom/google/android/exoplayer2/source/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/v;->j:Lcom/google/android/exoplayer2/source/y;

    move-object v0, v6

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/u;-><init>(Lcom/google/android/exoplayer2/source/y;Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/upstream/e;J)V

    iget-boolean p2, p0, Lcom/google/android/exoplayer2/source/v;->q:Z

    if-eqz p2, :cond_0

    iget-object p2, p1, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/source/v;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/source/y$a;->a(Ljava/lang/Object;)Lcom/google/android/exoplayer2/source/y$a;

    move-result-object p1

    invoke-virtual {v6, p1}, Lcom/google/android/exoplayer2/source/u;->a(Lcom/google/android/exoplayer2/source/y$a;)V

    goto :goto_0

    :cond_0
    iput-object v6, p0, Lcom/google/android/exoplayer2/source/v;->o:Lcom/google/android/exoplayer2/source/u;

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/source/v;->p:Z

    if-nez p1, :cond_1

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/v;->p:Z

    const/4 p1, 0x0

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/v;->j:Lcom/google/android/exoplayer2/source/y;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/source/m;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/source/y;)V

    :cond_1
    :goto_0
    return-object v6
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/upstream/e;J)Lcom/google/android/exoplayer2/source/x;
    .locals 0

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/source/v;->a(Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/upstream/e;J)Lcom/google/android/exoplayer2/source/u;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/source/y$a;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/source/v;->a(Ljava/lang/Void;Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/source/y$a;

    move-result-object p1

    return-object p1
.end method

.method protected a(Ljava/lang/Void;Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/source/y$a;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object p1, p2, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/v;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/google/android/exoplayer2/source/y$a;->a(Ljava/lang/Object;)Lcom/google/android/exoplayer2/source/y$a;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/google/android/exoplayer2/source/x;)V
    .locals 1

    move-object v0, p1

    check-cast v0, Lcom/google/android/exoplayer2/source/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/u;->i()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/v;->o:Lcom/google/android/exoplayer2/source/u;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/v;->o:Lcom/google/android/exoplayer2/source/u;

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/C;)V
    .locals 1
    .param p1    # Lcom/google/android/exoplayer2/upstream/C;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/google/android/exoplayer2/source/m;->a(Lcom/google/android/exoplayer2/upstream/C;)V

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/source/v;->k:Z

    if-nez p1, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/v;->p:Z

    const/4 p1, 0x0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/v;->j:Lcom/google/android/exoplayer2/source/y;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/exoplayer2/source/m;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/source/y;)V

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Void;Lcom/google/android/exoplayer2/source/y;Lcom/google/android/exoplayer2/sa;)V
    .locals 9

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/source/v;->q:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    invoke-virtual {p1, p3}, Lcom/google/android/exoplayer2/source/v$a;->a(Lcom/google/android/exoplayer2/sa;)Lcom/google/android/exoplayer2/source/v$a;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/v;->o:Lcom/google/android/exoplayer2/source/u;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/u;->g()J

    move-result-wide p1

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/v;->a(J)V

    goto/16 :goto_3

    :cond_0
    invoke-virtual {p3}, Lcom/google/android/exoplayer2/sa;->c()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/source/v;->r:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    invoke-virtual {p1, p3}, Lcom/google/android/exoplayer2/source/v$a;->a(Lcom/google/android/exoplayer2/sa;)Lcom/google/android/exoplayer2/source/v$a;

    move-result-object p1

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/google/android/exoplayer2/sa$b;->a:Ljava/lang/Object;

    sget-object p2, Lcom/google/android/exoplayer2/source/v$a;->c:Ljava/lang/Object;

    invoke-static {p3, p1, p2}, Lcom/google/android/exoplayer2/source/v$a;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/exoplayer2/source/v$a;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    goto :goto_3

    :cond_2
    const/4 p1, 0x0

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/v;->l:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {p3, p1, p2}, Lcom/google/android/exoplayer2/sa;->a(ILcom/google/android/exoplayer2/sa$b;)Lcom/google/android/exoplayer2/sa$b;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/v;->l:Lcom/google/android/exoplayer2/sa$b;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/sa$b;->b()J

    move-result-wide p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/v;->o:Lcom/google/android/exoplayer2/source/u;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/u;->h()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    move-wide v7, v0

    goto :goto_1

    :cond_3
    move-wide v7, p1

    :goto_1
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/v;->l:Lcom/google/android/exoplayer2/sa$b;

    iget-object p1, v4, Lcom/google/android/exoplayer2/sa$b;->c:Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/exoplayer2/source/v;->m:Lcom/google/android/exoplayer2/sa$a;

    const/4 v6, 0x0

    move-object v3, p3

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/exoplayer2/sa;->a(Lcom/google/android/exoplayer2/sa$b;Lcom/google/android/exoplayer2/sa$a;IJ)Landroid/util/Pair;

    move-result-object p2

    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object p2, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-boolean p2, p0, Lcom/google/android/exoplayer2/source/v;->r:Z

    if-eqz p2, :cond_4

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    invoke-virtual {p1, p3}, Lcom/google/android/exoplayer2/source/v$a;->a(Lcom/google/android/exoplayer2/sa;)Lcom/google/android/exoplayer2/source/v$a;

    move-result-object p1

    goto :goto_2

    :cond_4
    invoke-static {p3, p1, v0}, Lcom/google/android/exoplayer2/source/v$a;->a(Lcom/google/android/exoplayer2/sa;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/exoplayer2/source/v$a;

    move-result-object p1

    :goto_2
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/v;->o:Lcom/google/android/exoplayer2/source/u;

    if-eqz p1, :cond_5

    invoke-direct {p0, v1, v2}, Lcom/google/android/exoplayer2/source/v;->a(J)V

    iget-object p1, p1, Lcom/google/android/exoplayer2/source/u;->b:Lcom/google/android/exoplayer2/source/y$a;

    iget-object p2, p1, Lcom/google/android/exoplayer2/source/y$a;->a:Ljava/lang/Object;

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/source/v;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/source/y$a;->a(Ljava/lang/Object;)Lcom/google/android/exoplayer2/source/y$a;

    move-result-object p1

    goto :goto_4

    :cond_5
    :goto_3
    const/4 p1, 0x0

    :goto_4
    const/4 p2, 0x1

    iput-boolean p2, p0, Lcom/google/android/exoplayer2/source/v;->r:Z

    iput-boolean p2, p0, Lcom/google/android/exoplayer2/source/v;->q:Z

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    invoke-virtual {p0, p2}, Lcom/google/android/exoplayer2/source/j;->a(Lcom/google/android/exoplayer2/sa;)V

    if-eqz p1, :cond_6

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/v;->o:Lcom/google/android/exoplayer2/source/u;

    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p2, Lcom/google/android/exoplayer2/source/u;

    invoke-virtual {p2, p1}, Lcom/google/android/exoplayer2/source/u;->a(Lcom/google/android/exoplayer2/source/y$a;)V

    :cond_6
    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method protected bridge synthetic b(Ljava/lang/Object;Lcom/google/android/exoplayer2/source/y;Lcom/google/android/exoplayer2/sa;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/source/v;->a(Ljava/lang/Void;Lcom/google/android/exoplayer2/source/y;Lcom/google/android/exoplayer2/sa;)V

    return-void
.end method

.method public h()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/v;->q:Z

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/v;->p:Z

    invoke-super {p0}, Lcom/google/android/exoplayer2/source/m;->h()V

    return-void
.end method

.method public i()Lcom/google/android/exoplayer2/sa;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/v;->n:Lcom/google/android/exoplayer2/source/v$a;

    return-object v0
.end method
