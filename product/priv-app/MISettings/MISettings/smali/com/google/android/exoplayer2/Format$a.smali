.class public final Lcom/google/android/exoplayer2/Format$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/Format;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:Ljava/lang/Class;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lcom/google/android/exoplayer2/drm/g;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/google/android/exoplayer2/metadata/Metadata;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private l:I

.field private m:Ljava/util/List;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[B>;"
        }
    .end annotation
.end field

.field private n:Lcom/google/android/exoplayer2/drm/DrmInitData;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private o:J

.field private p:I

.field private q:I

.field private r:F

.field private s:I

.field private t:F

.field private u:[B
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private v:I

.field private w:Lcom/google/android/exoplayer2/video/ColorInfo;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->f:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->g:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->l:I

    const-wide v1, 0x7fffffffffffffffL

    iput-wide v1, p0, Lcom/google/android/exoplayer2/Format$a;->o:J

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->p:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->q:I

    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/google/android/exoplayer2/Format$a;->r:F

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/google/android/exoplayer2/Format$a;->t:F

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->v:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->x:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->y:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->z:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->C:I

    return-void
.end method

.method private constructor <init>(Lcom/google/android/exoplayer2/Format;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/exoplayer2/Format$a;->a:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/exoplayer2/Format$a;->b:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/exoplayer2/Format$a;->c:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->d:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->d:I

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->e:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->e:I

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->f:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->f:I

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->g:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->g:I

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/exoplayer2/Format$a;->h:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->j:Lcom/google/android/exoplayer2/metadata/Metadata;

    iput-object v0, p0, Lcom/google/android/exoplayer2/Format$a;->i:Lcom/google/android/exoplayer2/metadata/Metadata;

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/exoplayer2/Format$a;->j:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/exoplayer2/Format$a;->k:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->m:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->l:I

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->n:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/exoplayer2/Format$a;->m:Ljava/util/List;

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->o:Lcom/google/android/exoplayer2/drm/DrmInitData;

    iput-object v0, p0, Lcom/google/android/exoplayer2/Format$a;->n:Lcom/google/android/exoplayer2/drm/DrmInitData;

    iget-wide v0, p1, Lcom/google/android/exoplayer2/Format;->p:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/Format$a;->o:J

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->q:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->p:I

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->r:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->q:I

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->s:F

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->r:F

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->t:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->s:I

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->u:F

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->t:F

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->v:[B

    iput-object v0, p0, Lcom/google/android/exoplayer2/Format$a;->u:[B

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->w:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->v:I

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->x:Lcom/google/android/exoplayer2/video/ColorInfo;

    iput-object v0, p0, Lcom/google/android/exoplayer2/Format$a;->w:Lcom/google/android/exoplayer2/video/ColorInfo;

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->y:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->x:I

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->z:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->y:I

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->A:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->z:I

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->B:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->A:I

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->C:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->B:I

    iget v0, p1, Lcom/google/android/exoplayer2/Format;->D:I

    iput v0, p0, Lcom/google/android/exoplayer2/Format$a;->C:I

    iget-object p1, p1, Lcom/google/android/exoplayer2/Format;->E:Ljava/lang/Class;

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->D:Ljava/lang/Class;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/Q;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/Format$a;-><init>(Lcom/google/android/exoplayer2/Format;)V

    return-void
.end method

.method static synthetic A(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->f:I

    return p0
.end method

.method static synthetic B(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->g:I

    return p0
.end method

.method static synthetic C(Lcom/google/android/exoplayer2/Format$a;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->h:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic D(Lcom/google/android/exoplayer2/Format$a;)Lcom/google/android/exoplayer2/metadata/Metadata;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->i:Lcom/google/android/exoplayer2/metadata/Metadata;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/Format$a;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->a:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/Format$a;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->j:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/Format$a;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->k:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->l:I

    return p0
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/Format$a;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->m:Ljava/util/List;

    return-object p0
.end method

.method static synthetic f(Lcom/google/android/exoplayer2/Format$a;)Lcom/google/android/exoplayer2/drm/DrmInitData;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->n:Lcom/google/android/exoplayer2/drm/DrmInitData;

    return-object p0
.end method

.method static synthetic g(Lcom/google/android/exoplayer2/Format$a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/Format$a;->o:J

    return-wide v0
.end method

.method static synthetic h(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->p:I

    return p0
.end method

.method static synthetic i(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->q:I

    return p0
.end method

.method static synthetic j(Lcom/google/android/exoplayer2/Format$a;)F
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->r:F

    return p0
.end method

.method static synthetic k(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->s:I

    return p0
.end method

.method static synthetic l(Lcom/google/android/exoplayer2/Format$a;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic m(Lcom/google/android/exoplayer2/Format$a;)F
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->t:F

    return p0
.end method

.method static synthetic n(Lcom/google/android/exoplayer2/Format$a;)[B
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->u:[B

    return-object p0
.end method

.method static synthetic o(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->v:I

    return p0
.end method

.method static synthetic p(Lcom/google/android/exoplayer2/Format$a;)Lcom/google/android/exoplayer2/video/ColorInfo;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->w:Lcom/google/android/exoplayer2/video/ColorInfo;

    return-object p0
.end method

.method static synthetic q(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->x:I

    return p0
.end method

.method static synthetic r(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->y:I

    return p0
.end method

.method static synthetic s(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->z:I

    return p0
.end method

.method static synthetic t(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->A:I

    return p0
.end method

.method static synthetic u(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->B:I

    return p0
.end method

.method static synthetic v(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->C:I

    return p0
.end method

.method static synthetic w(Lcom/google/android/exoplayer2/Format$a;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->c:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic x(Lcom/google/android/exoplayer2/Format$a;)Ljava/lang/Class;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/Format$a;->D:Ljava/lang/Class;

    return-object p0
.end method

.method static synthetic y(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->d:I

    return p0
.end method

.method static synthetic z(Lcom/google/android/exoplayer2/Format$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/Format$a;->e:I

    return p0
.end method


# virtual methods
.method public a(F)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->r:F

    return-object p0
.end method

.method public a(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->C:I

    return-object p0
.end method

.method public a(J)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/Format$a;->o:J

    return-object p0
.end method

.method public a(Lcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format$a;
    .locals 0
    .param p1    # Lcom/google/android/exoplayer2/drm/DrmInitData;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->n:Lcom/google/android/exoplayer2/drm/DrmInitData;

    return-object p0
.end method

.method public a(Lcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/Format$a;
    .locals 0
    .param p1    # Lcom/google/android/exoplayer2/metadata/Metadata;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->i:Lcom/google/android/exoplayer2/metadata/Metadata;

    return-object p0
.end method

.method public a(Lcom/google/android/exoplayer2/video/ColorInfo;)Lcom/google/android/exoplayer2/Format$a;
    .locals 0
    .param p1    # Lcom/google/android/exoplayer2/video/ColorInfo;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->w:Lcom/google/android/exoplayer2/video/ColorInfo;

    return-object p0
.end method

.method public a(Ljava/lang/Class;)Lcom/google/android/exoplayer2/Format$a;
    .locals 0
    .param p1    # Ljava/lang/Class;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/google/android/exoplayer2/drm/g;",
            ">;)",
            "Lcom/google/android/exoplayer2/Format$a;"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->D:Ljava/lang/Class;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->h:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/google/android/exoplayer2/Format$a;
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;)",
            "Lcom/google/android/exoplayer2/Format$a;"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->m:Ljava/util/List;

    return-object p0
.end method

.method public a([B)Lcom/google/android/exoplayer2/Format$a;
    .locals 0
    .param p1    # [B
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->u:[B

    return-object p0
.end method

.method public a()Lcom/google/android/exoplayer2/Format;
    .locals 2

    new-instance v0, Lcom/google/android/exoplayer2/Format;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/exoplayer2/Format;-><init>(Lcom/google/android/exoplayer2/Format$a;Lcom/google/android/exoplayer2/Q;)V

    return-object v0
.end method

.method public b(F)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->t:F

    return-object p0
.end method

.method public b(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->f:I

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->a:Ljava/lang/String;

    return-object p0
.end method

.method public c(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->x:I

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method public d(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->A:I

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->c:Ljava/lang/String;

    return-object p0
.end method

.method public e(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->B:I

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->k:Ljava/lang/String;

    return-object p0
.end method

.method public f(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->q:I

    return-object p0
.end method

.method public g(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/Format$a;->a:Ljava/lang/String;

    return-object p0
.end method

.method public h(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->l:I

    return-object p0
.end method

.method public i(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->z:I

    return-object p0
.end method

.method public j(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->g:I

    return-object p0
.end method

.method public k(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->s:I

    return-object p0
.end method

.method public l(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->y:I

    return-object p0
.end method

.method public m(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->d:I

    return-object p0
.end method

.method public n(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->v:I

    return-object p0
.end method

.method public o(I)Lcom/google/android/exoplayer2/Format$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/Format$a;->p:I

    return-object p0
.end method
