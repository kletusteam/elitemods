.class public final Lcom/google/android/exoplayer2/source/q;
.super Lcom/google/android/exoplayer2/source/m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/q$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/exoplayer2/source/m<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final j:Lcom/google/android/exoplayer2/source/F;


# direct methods
.method private constructor <init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/l$a;Lcom/google/android/exoplayer2/extractor/m;Lcom/google/android/exoplayer2/upstream/x;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 8
    .param p5    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/m;-><init>()V

    new-instance v7, Lcom/google/android/exoplayer2/source/F;

    new-instance v0, Lcom/google/android/exoplayer2/W$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/W$a;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/W$a;->a(Landroid/net/Uri;)Lcom/google/android/exoplayer2/W$a;

    invoke-virtual {v0, p5}, Lcom/google/android/exoplayer2/W$a;->a(Ljava/lang/String;)Lcom/google/android/exoplayer2/W$a;

    invoke-virtual {v0, p7}, Lcom/google/android/exoplayer2/W$a;->a(Ljava/lang/Object;)Lcom/google/android/exoplayer2/W$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/W$a;->a()Lcom/google/android/exoplayer2/W;

    move-result-object v1

    invoke-static {}, Lcom/google/android/exoplayer2/drm/e;->a()Lcom/google/android/exoplayer2/drm/e;

    move-result-object v4

    move-object v0, v7

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/source/F;-><init>(Lcom/google/android/exoplayer2/W;Lcom/google/android/exoplayer2/upstream/l$a;Lcom/google/android/exoplayer2/extractor/m;Lcom/google/android/exoplayer2/drm/e;Lcom/google/android/exoplayer2/upstream/x;I)V

    iput-object v7, p0, Lcom/google/android/exoplayer2/source/q;->j:Lcom/google/android/exoplayer2/source/F;

    return-void
.end method

.method synthetic constructor <init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/l$a;Lcom/google/android/exoplayer2/extractor/m;Lcom/google/android/exoplayer2/upstream/x;Ljava/lang/String;ILjava/lang/Object;Lcom/google/android/exoplayer2/source/p;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/google/android/exoplayer2/source/q;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/l$a;Lcom/google/android/exoplayer2/extractor/m;Lcom/google/android/exoplayer2/upstream/x;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/exoplayer2/W;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/q;->j:Lcom/google/android/exoplayer2/source/F;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/F;->a()Lcom/google/android/exoplayer2/W;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/upstream/e;J)Lcom/google/android/exoplayer2/source/x;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/q;->j:Lcom/google/android/exoplayer2/source/F;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/source/F;->a(Lcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/upstream/e;J)Lcom/google/android/exoplayer2/source/x;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/google/android/exoplayer2/source/x;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/q;->j:Lcom/google/android/exoplayer2/source/F;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/source/F;->a(Lcom/google/android/exoplayer2/source/x;)V

    return-void
.end method

.method protected a(Lcom/google/android/exoplayer2/upstream/C;)V
    .locals 1
    .param p1    # Lcom/google/android/exoplayer2/upstream/C;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/google/android/exoplayer2/source/m;->a(Lcom/google/android/exoplayer2/upstream/C;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/q;->j:Lcom/google/android/exoplayer2/source/F;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/exoplayer2/source/m;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/source/y;)V

    return-void
.end method

.method protected a(Ljava/lang/Void;Lcom/google/android/exoplayer2/source/y;Lcom/google/android/exoplayer2/sa;)V
    .locals 0
    .param p1    # Ljava/lang/Void;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-virtual {p0, p3}, Lcom/google/android/exoplayer2/source/j;->a(Lcom/google/android/exoplayer2/sa;)V

    return-void
.end method

.method protected bridge synthetic b(Ljava/lang/Object;Lcom/google/android/exoplayer2/source/y;Lcom/google/android/exoplayer2/sa;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/source/q;->a(Ljava/lang/Void;Lcom/google/android/exoplayer2/source/y;Lcom/google/android/exoplayer2/sa;)V

    return-void
.end method
