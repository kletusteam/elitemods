.class final Lcom/google/android/exoplayer2/qa;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/qa$b;,
        Lcom/google/android/exoplayer2/qa$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;

.field private final c:Lcom/google/android/exoplayer2/qa$a;

.field private final d:Landroid/media/AudioManager;

.field private final e:Lcom/google/android/exoplayer2/qa$b;

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/exoplayer2/qa$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/qa;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/exoplayer2/qa;->b:Landroid/os/Handler;

    iput-object p3, p0, Lcom/google/android/exoplayer2/qa;->c:Lcom/google/android/exoplayer2/qa$a;

    iget-object p1, p0, Lcom/google/android/exoplayer2/qa;->a:Landroid/content/Context;

    const-string p2, "audio"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/AudioManager;

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Landroid/media/AudioManager;

    iput-object p1, p0, Lcom/google/android/exoplayer2/qa;->d:Landroid/media/AudioManager;

    const/4 p1, 0x3

    iput p1, p0, Lcom/google/android/exoplayer2/qa;->f:I

    iget-object p1, p0, Lcom/google/android/exoplayer2/qa;->d:Landroid/media/AudioManager;

    iget p2, p0, Lcom/google/android/exoplayer2/qa;->f:I

    invoke-static {p1, p2}, Lcom/google/android/exoplayer2/qa;->b(Landroid/media/AudioManager;I)I

    move-result p1

    iput p1, p0, Lcom/google/android/exoplayer2/qa;->g:I

    iget-object p1, p0, Lcom/google/android/exoplayer2/qa;->d:Landroid/media/AudioManager;

    iget p2, p0, Lcom/google/android/exoplayer2/qa;->f:I

    invoke-static {p1, p2}, Lcom/google/android/exoplayer2/qa;->a(Landroid/media/AudioManager;I)Z

    move-result p1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/qa;->h:Z

    new-instance p1, Lcom/google/android/exoplayer2/qa$b;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/google/android/exoplayer2/qa$b;-><init>(Lcom/google/android/exoplayer2/qa;Lcom/google/android/exoplayer2/pa;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/qa;->e:Lcom/google/android/exoplayer2/qa$b;

    new-instance p1, Landroid/content/IntentFilter;

    const-string p2, "android.media.VOLUME_CHANGED_ACTION"

    invoke-direct {p1, p2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/qa;->a:Landroid/content/Context;

    iget-object p3, p0, Lcom/google/android/exoplayer2/qa;->e:Lcom/google/android/exoplayer2/qa$b;

    invoke-virtual {p2, p3, p1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/qa;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/qa;->b:Landroid/os/Handler;

    return-object p0
.end method

.method private static a(Landroid/media/AudioManager;I)Z
    .locals 2

    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, p1}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result p0

    return p0

    :cond_0
    invoke-virtual {p0, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result p0

    if-nez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static b(Landroid/media/AudioManager;I)I
    .locals 0

    invoke-virtual {p0, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/qa;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/qa;->d()V

    return-void
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/qa;->d:Landroid/media/AudioManager;

    iget v1, p0, Lcom/google/android/exoplayer2/qa;->f:I

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/qa;->b(Landroid/media/AudioManager;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/qa;->d:Landroid/media/AudioManager;

    iget v2, p0, Lcom/google/android/exoplayer2/qa;->f:I

    invoke-static {v1, v2}, Lcom/google/android/exoplayer2/qa;->a(Landroid/media/AudioManager;I)Z

    move-result v1

    iget v2, p0, Lcom/google/android/exoplayer2/qa;->g:I

    if-ne v2, v0, :cond_0

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/qa;->h:Z

    if-eq v2, v1, :cond_1

    :cond_0
    iput v0, p0, Lcom/google/android/exoplayer2/qa;->g:I

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/qa;->h:Z

    iget-object v2, p0, Lcom/google/android/exoplayer2/qa;->c:Lcom/google/android/exoplayer2/qa$a;

    invoke-interface {v2, v0, v1}, Lcom/google/android/exoplayer2/qa$a;->a(IZ)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/qa;->d:Landroid/media/AudioManager;

    iget v1, p0, Lcom/google/android/exoplayer2/qa;->f:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    return v0
.end method

.method public a(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/qa;->f:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/exoplayer2/qa;->f:I

    invoke-direct {p0}, Lcom/google/android/exoplayer2/qa;->d()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/qa;->c:Lcom/google/android/exoplayer2/qa$a;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/qa$a;->e(I)V

    return-void
.end method

.method public b()I
    .locals 2

    sget v0, Lcom/google/android/exoplayer2/util/E;->a:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/qa;->d:Landroid/media/AudioManager;

    iget v1, p0, Lcom/google/android/exoplayer2/qa;->f:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMinVolume(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/qa;->i:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/qa;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/exoplayer2/qa;->e:Lcom/google/android/exoplayer2/qa$b;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/qa;->i:Z

    return-void
.end method
