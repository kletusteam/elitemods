.class Lcom/google/android/exoplayer2/source/G;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/G$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/upstream/e;

.field private final b:I

.field private final c:Lcom/google/android/exoplayer2/util/t;

.field private d:Lcom/google/android/exoplayer2/source/G$a;

.field private e:Lcom/google/android/exoplayer2/source/G$a;

.field private f:Lcom/google/android/exoplayer2/source/G$a;

.field private g:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/upstream/e;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/G;->a:Lcom/google/android/exoplayer2/upstream/e;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/upstream/e;->c()I

    move-result p1

    iput p1, p0, Lcom/google/android/exoplayer2/source/G;->b:I

    new-instance p1, Lcom/google/android/exoplayer2/util/t;

    const/16 v0, 0x20

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    new-instance p1, Lcom/google/android/exoplayer2/source/G$a;

    iget v0, p0, Lcom/google/android/exoplayer2/source/G;->b:I

    const-wide/16 v1, 0x0

    invoke-direct {p1, v1, v2, v0}, Lcom/google/android/exoplayer2/source/G$a;-><init>(JI)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/G;->d:Lcom/google/android/exoplayer2/source/G$a;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/G;->d:Lcom/google/android/exoplayer2/source/G$a;

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/G;->f:Lcom/google/android/exoplayer2/source/G$a;

    return-void
.end method

.method private a(I)V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/G;->g:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/G;->g:J

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/G;->g:J

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/G;->f:Lcom/google/android/exoplayer2/source/G$a;

    iget-wide v2, p1, Lcom/google/android/exoplayer2/source/G$a;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object p1, p1, Lcom/google/android/exoplayer2/source/G$a;->e:Lcom/google/android/exoplayer2/source/G$a;

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/G;->f:Lcom/google/android/exoplayer2/source/G$a;

    :cond_0
    return-void
.end method

.method private a(JLjava/nio/ByteBuffer;I)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/G;->b(J)V

    :cond_0
    :goto_0
    if-lez p4, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/G$a;->b:J

    sub-long/2addr v0, p1

    long-to-int v0, v0

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    iget-object v2, v1, Lcom/google/android/exoplayer2/source/G$a;->d:Lcom/google/android/exoplayer2/upstream/d;

    iget-object v2, v2, Lcom/google/android/exoplayer2/upstream/d;->a:[B

    invoke-virtual {v1, p1, p2}, Lcom/google/android/exoplayer2/source/G$a;->a(J)I

    move-result v1

    invoke-virtual {p3, v2, v1, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    sub-int/2addr p4, v0

    int-to-long v0, v0

    add-long/2addr p1, v0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    iget-wide v1, v0, Lcom/google/android/exoplayer2/source/G$a;->b:J

    cmp-long v1, p1, v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/G$a;->e:Lcom/google/android/exoplayer2/source/G$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(J[BI)V
    .locals 5

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/G;->b(J)V

    move-wide v0, p1

    move p1, p4

    :cond_0
    :goto_0
    if-lez p1, :cond_1

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    iget-wide v2, p2, Lcom/google/android/exoplayer2/source/G$a;->b:J

    sub-long/2addr v2, v0

    long-to-int p2, v2

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    iget-object v3, v2, Lcom/google/android/exoplayer2/source/G$a;->d:Lcom/google/android/exoplayer2/upstream/d;

    iget-object v3, v3, Lcom/google/android/exoplayer2/upstream/d;->a:[B

    invoke-virtual {v2, v0, v1}, Lcom/google/android/exoplayer2/source/G$a;->a(J)I

    move-result v2

    sub-int v4, p4, p1

    invoke-static {v3, v2, p3, v4, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sub-int/2addr p1, p2

    int-to-long v2, p2

    add-long/2addr v0, v2

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    iget-wide v2, p2, Lcom/google/android/exoplayer2/source/G$a;->b:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-object p2, p2, Lcom/google/android/exoplayer2/source/G$a;->e:Lcom/google/android/exoplayer2/source/G$a;

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/source/G$a;)V
    .locals 6

    iget-boolean v0, p1, Lcom/google/android/exoplayer2/source/G$a;->c:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->f:Lcom/google/android/exoplayer2/source/G$a;

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/source/G$a;->c:Z

    iget-wide v2, v0, Lcom/google/android/exoplayer2/source/G$a;->a:J

    iget-wide v4, p1, Lcom/google/android/exoplayer2/source/G$a;->a:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    iget v2, p0, Lcom/google/android/exoplayer2/source/G;->b:I

    div-int/2addr v0, v2

    add-int/2addr v1, v0

    new-array v0, v1, [Lcom/google/android/exoplayer2/upstream/d;

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    iget-object v2, p1, Lcom/google/android/exoplayer2/source/G$a;->d:Lcom/google/android/exoplayer2/upstream/d;

    aput-object v2, v0, v1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/G$a;->a()Lcom/google/android/exoplayer2/source/G$a;

    move-result-object p1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/G;->a:Lcom/google/android/exoplayer2/upstream/e;

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/upstream/e;->a([Lcom/google/android/exoplayer2/upstream/d;)V

    return-void
.end method

.method private b(I)I
    .locals 6

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->f:Lcom/google/android/exoplayer2/source/G$a;

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/source/G$a;->c:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/G;->a:Lcom/google/android/exoplayer2/upstream/e;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/upstream/e;->a()Lcom/google/android/exoplayer2/upstream/d;

    move-result-object v1

    new-instance v2, Lcom/google/android/exoplayer2/source/G$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/G;->f:Lcom/google/android/exoplayer2/source/G$a;

    iget-wide v3, v3, Lcom/google/android/exoplayer2/source/G$a;->b:J

    iget v5, p0, Lcom/google/android/exoplayer2/source/G;->b:I

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/exoplayer2/source/G$a;-><init>(JI)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/source/G$a;->a(Lcom/google/android/exoplayer2/upstream/d;Lcom/google/android/exoplayer2/source/G$a;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->f:Lcom/google/android/exoplayer2/source/G$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/G$a;->b:J

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/G;->g:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    return p1
.end method

.method private b(J)V
    .locals 3

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    iget-wide v1, v0, Lcom/google/android/exoplayer2/source/G$a;->b:J

    cmp-long v1, p1, v1

    if-ltz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/G$a;->e:Lcom/google/android/exoplayer2/source/G$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;Lcom/google/android/exoplayer2/source/H$a;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    iget-wide v2, v1, Lcom/google/android/exoplayer2/source/H$a;->b:J

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/exoplayer2/source/G;->a(J[BI)V

    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    const/4 v6, 0x0

    aget-byte v4, v4, v6

    and-int/lit16 v7, v4, 0x80

    if-eqz v7, :cond_0

    move v7, v5

    goto :goto_0

    :cond_0
    move v7, v6

    :goto_0
    and-int/lit8 v4, v4, 0x7f

    move-object/from16 v8, p1

    iget-object v8, v8, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->a:Lcom/google/android/exoplayer2/decoder/c;

    iget-object v9, v8, Lcom/google/android/exoplayer2/decoder/c;->a:[B

    if-nez v9, :cond_1

    const/16 v9, 0x10

    new-array v9, v9, [B

    iput-object v9, v8, Lcom/google/android/exoplayer2/decoder/c;->a:[B

    goto :goto_1

    :cond_1
    invoke-static {v9, v6}, Ljava/util/Arrays;->fill([BB)V

    :goto_1
    iget-object v9, v8, Lcom/google/android/exoplayer2/decoder/c;->a:[B

    invoke-direct {v0, v2, v3, v9, v4}, Lcom/google/android/exoplayer2/source/G;->a(J[BI)V

    int-to-long v9, v4

    add-long/2addr v2, v9

    if-eqz v7, :cond_2

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/exoplayer2/source/G;->a(J[BI)V

    const-wide/16 v4, 0x2

    add-long/2addr v2, v4

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->B()I

    move-result v5

    :cond_2
    move v9, v5

    iget-object v4, v8, Lcom/google/android/exoplayer2/decoder/c;->d:[I

    if-eqz v4, :cond_3

    array-length v5, v4

    if-ge v5, v9, :cond_4

    :cond_3
    new-array v4, v9, [I

    :cond_4
    move-object v10, v4

    iget-object v4, v8, Lcom/google/android/exoplayer2/decoder/c;->e:[I

    if-eqz v4, :cond_5

    array-length v5, v4

    if-ge v5, v9, :cond_6

    :cond_5
    new-array v4, v9, [I

    :cond_6
    move-object v11, v4

    if-eqz v7, :cond_7

    mul-int/lit8 v4, v9, 0x6

    iget-object v5, v0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v5, v4}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    iget-object v5, v0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v5

    invoke-direct {v0, v2, v3, v5, v4}, Lcom/google/android/exoplayer2/source/G;->a(J[BI)V

    int-to-long v4, v4

    add-long/2addr v2, v4

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4, v6}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    :goto_2
    if-ge v6, v9, :cond_8

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->B()I

    move-result v4

    aput v4, v10, v6

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->z()I

    move-result v4

    aput v4, v11, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_7
    aput v6, v10, v6

    iget v4, v1, Lcom/google/android/exoplayer2/source/H$a;->a:I

    iget-wide v12, v1, Lcom/google/android/exoplayer2/source/H$a;->b:J

    sub-long v12, v2, v12

    long-to-int v5, v12

    sub-int/2addr v4, v5

    aput v4, v11, v6

    :cond_8
    iget-object v4, v1, Lcom/google/android/exoplayer2/source/H$a;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

    invoke-static {v4}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v4, Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

    iget-object v12, v4, Lcom/google/android/exoplayer2/extractor/TrackOutput$a;->b:[B

    iget-object v13, v8, Lcom/google/android/exoplayer2/decoder/c;->a:[B

    iget v14, v4, Lcom/google/android/exoplayer2/extractor/TrackOutput$a;->a:I

    iget v15, v4, Lcom/google/android/exoplayer2/extractor/TrackOutput$a;->c:I

    iget v4, v4, Lcom/google/android/exoplayer2/extractor/TrackOutput$a;->d:I

    move/from16 v16, v4

    invoke-virtual/range {v8 .. v16}, Lcom/google/android/exoplayer2/decoder/c;->a(I[I[I[B[BIII)V

    iget-wide v4, v1, Lcom/google/android/exoplayer2/source/H$a;->b:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    int-to-long v6, v2

    add-long/2addr v4, v6

    iput-wide v4, v1, Lcom/google/android/exoplayer2/source/H$a;->b:J

    iget v3, v1, Lcom/google/android/exoplayer2/source/H$a;->a:I

    sub-int/2addr v3, v2

    iput v3, v1, Lcom/google/android/exoplayer2/source/H$a;->a:I

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/upstream/j;IZ)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/source/G;->b(I)I

    move-result p2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->f:Lcom/google/android/exoplayer2/source/G$a;

    iget-object v1, v0, Lcom/google/android/exoplayer2/source/G$a;->d:Lcom/google/android/exoplayer2/upstream/d;

    iget-object v1, v1, Lcom/google/android/exoplayer2/upstream/d;->a:[B

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/G;->g:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/source/G$a;->a(J)I

    move-result v0

    invoke-interface {p1, v1, v0, p2}, Lcom/google/android/exoplayer2/upstream/j;->read([BII)I

    move-result p1

    const/4 p2, -0x1

    if-ne p1, p2, :cond_1

    if-eqz p3, :cond_0

    return p2

    :cond_0
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/G;->a(I)V

    return p1
.end method

.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/G;->g:J

    return-wide v0
.end method

.method public a(J)V
    .locals 3

    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->d:Lcom/google/android/exoplayer2/source/G$a;

    iget-wide v1, v0, Lcom/google/android/exoplayer2/source/G$a;->b:J

    cmp-long v1, p1, v1

    if-ltz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/G;->a:Lcom/google/android/exoplayer2/upstream/e;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/G$a;->d:Lcom/google/android/exoplayer2/upstream/d;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/upstream/e;->a(Lcom/google/android/exoplayer2/upstream/d;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->d:Lcom/google/android/exoplayer2/source/G$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/G$a;->a()Lcom/google/android/exoplayer2/source/G$a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/G;->d:Lcom/google/android/exoplayer2/source/G$a;

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    iget-wide p1, p1, Lcom/google/android/exoplayer2/source/G$a;->a:J

    iget-wide v1, v0, Lcom/google/android/exoplayer2/source/G$a;->a:J

    cmp-long p1, p1, v1

    if-gez p1, :cond_2

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    :cond_2
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;Lcom/google/android/exoplayer2/source/H$a;)V
    .locals 6

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/G;->b(Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;Lcom/google/android/exoplayer2/source/H$a;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/decoder/a;->hasSupplementalData()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    iget-wide v2, p2, Lcom/google/android/exoplayer2/source/H$a;->b:J

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/exoplayer2/source/G;->a(J[BI)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->z()I

    move-result v0

    iget-wide v2, p2, Lcom/google/android/exoplayer2/source/H$a;->b:J

    const-wide/16 v4, 0x4

    add-long/2addr v2, v4

    iput-wide v2, p2, Lcom/google/android/exoplayer2/source/H$a;->b:J

    iget v2, p2, Lcom/google/android/exoplayer2/source/H$a;->a:I

    sub-int/2addr v2, v1

    iput v2, p2, Lcom/google/android/exoplayer2/source/H$a;->a:I

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->b(I)V

    iget-wide v1, p2, Lcom/google/android/exoplayer2/source/H$a;->b:J

    iget-object v3, p1, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->b:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/google/android/exoplayer2/source/G;->a(JLjava/nio/ByteBuffer;I)V

    iget-wide v1, p2, Lcom/google/android/exoplayer2/source/H$a;->b:J

    int-to-long v3, v0

    add-long/2addr v1, v3

    iput-wide v1, p2, Lcom/google/android/exoplayer2/source/H$a;->b:J

    iget v1, p2, Lcom/google/android/exoplayer2/source/H$a;->a:I

    sub-int/2addr v1, v0

    iput v1, p2, Lcom/google/android/exoplayer2/source/H$a;->a:I

    iget v0, p2, Lcom/google/android/exoplayer2/source/H$a;->a:I

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->c(I)V

    iget-wide v0, p2, Lcom/google/android/exoplayer2/source/H$a;->b:J

    iget-object p1, p1, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->e:Ljava/nio/ByteBuffer;

    iget p2, p2, Lcom/google/android/exoplayer2/source/H$a;->a:I

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/exoplayer2/source/G;->a(JLjava/nio/ByteBuffer;I)V

    goto :goto_0

    :cond_1
    iget v0, p2, Lcom/google/android/exoplayer2/source/H$a;->a:I

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->b(I)V

    iget-wide v0, p2, Lcom/google/android/exoplayer2/source/H$a;->b:J

    iget-object p1, p1, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->b:Ljava/nio/ByteBuffer;

    iget p2, p2, Lcom/google/android/exoplayer2/source/H$a;->a:I

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/exoplayer2/source/G;->a(JLjava/nio/ByteBuffer;I)V

    :goto_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/util/t;I)V
    .locals 5

    :goto_0
    if-lez p2, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/source/G;->b(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/G;->f:Lcom/google/android/exoplayer2/source/G$a;

    iget-object v2, v1, Lcom/google/android/exoplayer2/source/G$a;->d:Lcom/google/android/exoplayer2/upstream/d;

    iget-object v2, v2, Lcom/google/android/exoplayer2/upstream/d;->a:[B

    iget-wide v3, p0, Lcom/google/android/exoplayer2/source/G;->g:J

    invoke-virtual {v1, v3, v4}, Lcom/google/android/exoplayer2/source/G$a;->a(J)I

    move-result v1

    invoke-virtual {p1, v2, v1, v0}, Lcom/google/android/exoplayer2/util/t;->a([BII)V

    sub-int/2addr p2, v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/G;->a(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->d:Lcom/google/android/exoplayer2/source/G$a;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/G;->a(Lcom/google/android/exoplayer2/source/G$a;)V

    new-instance v0, Lcom/google/android/exoplayer2/source/G$a;

    iget v1, p0, Lcom/google/android/exoplayer2/source/G;->b:I

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/exoplayer2/source/G$a;-><init>(JI)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/G;->d:Lcom/google/android/exoplayer2/source/G$a;

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->d:Lcom/google/android/exoplayer2/source/G$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/G;->f:Lcom/google/android/exoplayer2/source/G$a;

    iput-wide v2, p0, Lcom/google/android/exoplayer2/source/G;->g:J

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->a:Lcom/google/android/exoplayer2/upstream/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/upstream/e;->b()V

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/G;->d:Lcom/google/android/exoplayer2/source/G$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/G;->e:Lcom/google/android/exoplayer2/source/G$a;

    return-void
.end method
