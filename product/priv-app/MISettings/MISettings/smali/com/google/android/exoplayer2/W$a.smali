.class public final Lcom/google/android/exoplayer2/W$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/W;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private b:Landroid/net/Uri;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private d:J

.field private e:J

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Landroid/net/Uri;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/UUID;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private p:[B
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/offline/StreamKey;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private t:Landroid/net/Uri;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private u:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/google/android/exoplayer2/Y;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/exoplayer2/W$a;->e:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/W$a;->o:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/W$a;->j:Ljava/util/Map;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/W$a;->q:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/W$a;->s:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Lcom/google/android/exoplayer2/W$a;
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/W$a;->b:Landroid/net/Uri;

    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lcom/google/android/exoplayer2/W$a;
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/W$a;->u:Ljava/lang/Object;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/android/exoplayer2/W$a;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/W$a;->r:Ljava/lang/String;

    return-object p0
.end method

.method public a()Lcom/google/android/exoplayer2/W;
    .locals 23

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer2/W$a;->i:Landroid/net/Uri;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/exoplayer2/W$a;->k:Ljava/util/UUID;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-object v3, v0, Lcom/google/android/exoplayer2/W$a;->b:Landroid/net/Uri;

    const/4 v1, 0x0

    if-eqz v3, :cond_4

    new-instance v12, Lcom/google/android/exoplayer2/W$d;

    iget-object v4, v0, Lcom/google/android/exoplayer2/W$a;->c:Ljava/lang/String;

    iget-object v14, v0, Lcom/google/android/exoplayer2/W$a;->k:Ljava/util/UUID;

    if-eqz v14, :cond_2

    new-instance v1, Lcom/google/android/exoplayer2/W$c;

    iget-object v15, v0, Lcom/google/android/exoplayer2/W$a;->i:Landroid/net/Uri;

    iget-object v2, v0, Lcom/google/android/exoplayer2/W$a;->j:Ljava/util/Map;

    iget-boolean v5, v0, Lcom/google/android/exoplayer2/W$a;->l:Z

    iget-boolean v6, v0, Lcom/google/android/exoplayer2/W$a;->n:Z

    iget-boolean v7, v0, Lcom/google/android/exoplayer2/W$a;->m:Z

    iget-object v8, v0, Lcom/google/android/exoplayer2/W$a;->o:Ljava/util/List;

    iget-object v9, v0, Lcom/google/android/exoplayer2/W$a;->p:[B

    const/16 v22, 0x0

    move-object v13, v1

    move-object/from16 v16, v2

    move/from16 v17, v5

    move/from16 v18, v6

    move/from16 v19, v7

    move-object/from16 v20, v8

    move-object/from16 v21, v9

    invoke-direct/range {v13 .. v22}, Lcom/google/android/exoplayer2/W$c;-><init>(Ljava/util/UUID;Landroid/net/Uri;Ljava/util/Map;ZZZLjava/util/List;[BLcom/google/android/exoplayer2/V;)V

    :cond_2
    move-object v5, v1

    iget-object v6, v0, Lcom/google/android/exoplayer2/W$a;->q:Ljava/util/List;

    iget-object v7, v0, Lcom/google/android/exoplayer2/W$a;->r:Ljava/lang/String;

    iget-object v8, v0, Lcom/google/android/exoplayer2/W$a;->s:Ljava/util/List;

    iget-object v9, v0, Lcom/google/android/exoplayer2/W$a;->t:Landroid/net/Uri;

    iget-object v10, v0, Lcom/google/android/exoplayer2/W$a;->u:Ljava/lang/Object;

    const/4 v11, 0x0

    move-object v2, v12

    invoke-direct/range {v2 .. v11}, Lcom/google/android/exoplayer2/W$d;-><init>(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/exoplayer2/W$c;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Landroid/net/Uri;Ljava/lang/Object;Lcom/google/android/exoplayer2/V;)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/W$a;->a:Ljava/lang/String;

    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    iget-object v1, v0, Lcom/google/android/exoplayer2/W$a;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    iput-object v1, v0, Lcom/google/android/exoplayer2/W$a;->a:Ljava/lang/String;

    move-object v5, v12

    goto :goto_3

    :cond_4
    move-object v5, v1

    :goto_3
    new-instance v1, Lcom/google/android/exoplayer2/W;

    iget-object v2, v0, Lcom/google/android/exoplayer2/W$a;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    new-instance v4, Lcom/google/android/exoplayer2/W$b;

    iget-wide v7, v0, Lcom/google/android/exoplayer2/W$a;->d:J

    iget-wide v9, v0, Lcom/google/android/exoplayer2/W$a;->e:J

    iget-boolean v11, v0, Lcom/google/android/exoplayer2/W$a;->f:Z

    iget-boolean v12, v0, Lcom/google/android/exoplayer2/W$a;->g:Z

    iget-boolean v13, v0, Lcom/google/android/exoplayer2/W$a;->h:Z

    const/4 v14, 0x0

    move-object v6, v4

    invoke-direct/range {v6 .. v14}, Lcom/google/android/exoplayer2/W$b;-><init>(JJZZZLcom/google/android/exoplayer2/V;)V

    iget-object v2, v0, Lcom/google/android/exoplayer2/W$a;->v:Lcom/google/android/exoplayer2/Y;

    if-eqz v2, :cond_5

    goto :goto_4

    :cond_5
    new-instance v2, Lcom/google/android/exoplayer2/Y$a;

    invoke-direct {v2}, Lcom/google/android/exoplayer2/Y$a;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/Y$a;->a()Lcom/google/android/exoplayer2/Y;

    move-result-object v2

    :goto_4
    move-object v6, v2

    const/4 v7, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/google/android/exoplayer2/W;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer2/W$b;Lcom/google/android/exoplayer2/W$d;Lcom/google/android/exoplayer2/Y;Lcom/google/android/exoplayer2/V;)V

    return-object v1
.end method

.method public b(Ljava/lang/String;)Lcom/google/android/exoplayer2/W$a;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/W$a;->a:Ljava/lang/String;

    return-object p0
.end method
