.class final Lcom/google/android/exoplayer2/extractor/b/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/b/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/b/c$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/extractor/b/g;

.field private final b:J

.field private final c:J

.field private final d:Lcom/google/android/exoplayer2/extractor/b/k;

.field private e:I

.field private f:J

.field private g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:J

.field private l:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/extractor/b/k;JJJJZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    const/4 v1, 0x0

    if-ltz v0, :cond_0

    cmp-long v0, p4, p2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->d:Lcom/google/android/exoplayer2/extractor/b/k;

    iput-wide p2, p0, Lcom/google/android/exoplayer2/extractor/b/c;->b:J

    iput-wide p4, p0, Lcom/google/android/exoplayer2/extractor/b/c;->c:J

    sub-long/2addr p4, p2

    cmp-long p1, p6, p4

    if-eqz p1, :cond_2

    if-eqz p10, :cond_1

    goto :goto_1

    :cond_1
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->e:I

    goto :goto_2

    :cond_2
    :goto_1
    iput-wide p8, p0, Lcom/google/android/exoplayer2/extractor/b/c;->f:J

    const/4 p1, 0x4

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->e:I

    :goto_2
    new-instance p1, Lcom/google/android/exoplayer2/extractor/b/g;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/extractor/b/g;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/extractor/b/c;)Lcom/google/android/exoplayer2/extractor/b/k;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->d:Lcom/google/android/exoplayer2/extractor/b/k;

    return-object p0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/extractor/b/c;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->b:J

    return-wide v0
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/extractor/b/c;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->c:J

    return-wide v0
.end method

.method private c(Lcom/google/android/exoplayer2/extractor/i;)J
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p1

    iget-wide v2, v0, Lcom/google/android/exoplayer2/extractor/b/c;->i:J

    iget-wide v4, v0, Lcom/google/android/exoplayer2/extractor/b/c;->j:J

    cmp-long v2, v2, v4

    const-wide/16 v3, -0x1

    if-nez v2, :cond_0

    return-wide v3

    :cond_0
    invoke-interface/range {p1 .. p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v5

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    iget-wide v7, v0, Lcom/google/android/exoplayer2/extractor/b/c;->j:J

    invoke-virtual {v2, v1, v7, v8}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;J)Z

    move-result v2

    if-nez v2, :cond_2

    iget-wide v1, v0, Lcom/google/android/exoplayer2/extractor/b/c;->i:J

    cmp-long v3, v1, v5

    if-eqz v3, :cond_1

    return-wide v1

    :cond_1
    new-instance v1, Ljava/io/IOException;

    const-string v2, "No ogg page can be found."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    const/4 v7, 0x0

    invoke-virtual {v2, v1, v7}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;Z)Z

    invoke-interface/range {p1 .. p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    iget-wide v7, v0, Lcom/google/android/exoplayer2/extractor/b/c;->h:J

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    iget-wide v9, v2, Lcom/google/android/exoplayer2/extractor/b/g;->c:J

    sub-long/2addr v7, v9

    iget v9, v2, Lcom/google/android/exoplayer2/extractor/b/g;->h:I

    iget v2, v2, Lcom/google/android/exoplayer2/extractor/b/g;->i:I

    add-int/2addr v9, v2

    const-wide/16 v10, 0x0

    cmp-long v2, v10, v7

    if-gtz v2, :cond_3

    const-wide/32 v12, 0x11940

    cmp-long v2, v7, v12

    if-gez v2, :cond_3

    return-wide v3

    :cond_3
    cmp-long v2, v7, v10

    if-gez v2, :cond_4

    iput-wide v5, v0, Lcom/google/android/exoplayer2/extractor/b/c;->j:J

    iget-object v3, v0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    iget-wide v3, v3, Lcom/google/android/exoplayer2/extractor/b/g;->c:J

    iput-wide v3, v0, Lcom/google/android/exoplayer2/extractor/b/c;->l:J

    goto :goto_0

    :cond_4
    invoke-interface/range {p1 .. p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v3

    int-to-long v5, v9

    add-long/2addr v3, v5

    iput-wide v3, v0, Lcom/google/android/exoplayer2/extractor/b/c;->i:J

    iget-object v3, v0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    iget-wide v3, v3, Lcom/google/android/exoplayer2/extractor/b/g;->c:J

    iput-wide v3, v0, Lcom/google/android/exoplayer2/extractor/b/c;->k:J

    :goto_0
    iget-wide v3, v0, Lcom/google/android/exoplayer2/extractor/b/c;->j:J

    iget-wide v5, v0, Lcom/google/android/exoplayer2/extractor/b/c;->i:J

    sub-long/2addr v3, v5

    const-wide/32 v10, 0x186a0

    cmp-long v3, v3, v10

    if-gez v3, :cond_5

    iput-wide v5, v0, Lcom/google/android/exoplayer2/extractor/b/c;->j:J

    return-wide v5

    :cond_5
    int-to-long v3, v9

    const-wide/16 v5, 0x1

    if-gtz v2, :cond_6

    const-wide/16 v9, 0x2

    goto :goto_1

    :cond_6
    move-wide v9, v5

    :goto_1
    mul-long/2addr v3, v9

    invoke-interface/range {p1 .. p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v1

    sub-long/2addr v1, v3

    iget-wide v3, v0, Lcom/google/android/exoplayer2/extractor/b/c;->j:J

    iget-wide v11, v0, Lcom/google/android/exoplayer2/extractor/b/c;->i:J

    sub-long v9, v3, v11

    mul-long/2addr v7, v9

    iget-wide v9, v0, Lcom/google/android/exoplayer2/extractor/b/c;->l:J

    iget-wide v13, v0, Lcom/google/android/exoplayer2/extractor/b/c;->k:J

    sub-long/2addr v9, v13

    div-long/2addr v7, v9

    add-long v9, v1, v7

    sub-long v13, v3, v5

    invoke-static/range {v9 .. v14}, Lcom/google/android/exoplayer2/util/E;->b(JJJ)J

    move-result-wide v1

    return-wide v1
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/extractor/b/c;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->f:J

    return-wide v0
.end method

.method private d(Lcom/google/android/exoplayer2/extractor/i;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;)Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;Z)Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    iget-wide v1, v0, Lcom/google/android/exoplayer2/extractor/b/g;->c:J

    iget-wide v3, p0, Lcom/google/android/exoplayer2/extractor/b/c;->h:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    return-void

    :cond_0
    iget v1, v0, Lcom/google/android/exoplayer2/extractor/b/g;->h:I

    iget v0, v0, Lcom/google/android/exoplayer2/extractor/b/g;->i:I

    add-int/2addr v1, v0

    invoke-interface {p1, v1}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->i:J

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/extractor/b/g;->c:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->k:J

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/i;)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->e:I

    const/4 v1, 0x1

    const/4 v2, 0x4

    if-eqz v0, :cond_4

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    const-wide/16 v3, -0x1

    const/4 v5, 0x3

    if-eq v0, v1, :cond_1

    if-eq v0, v5, :cond_3

    if-ne v0, v2, :cond_0

    return-wide v3

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/b/c;->c(Lcom/google/android/exoplayer2/extractor/i;)J

    move-result-wide v0

    cmp-long v3, v0, v3

    if-eqz v3, :cond_2

    return-wide v0

    :cond_2
    iput v5, p0, Lcom/google/android/exoplayer2/extractor/b/c;->e:I

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/b/c;->d(Lcom/google/android/exoplayer2/extractor/i;)V

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/b/c;->e:I

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->k:J

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    neg-long v0, v0

    return-wide v0

    :cond_4
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/exoplayer2/extractor/b/c;->g:J

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->e:I

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->c:J

    const-wide/32 v3, 0xff1b

    sub-long/2addr v0, v3

    iget-wide v3, p0, Lcom/google/android/exoplayer2/extractor/b/c;->g:J

    cmp-long v3, v0, v3

    if-lez v3, :cond_5

    return-wide v0

    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/extractor/b/c;->b(Lcom/google/android/exoplayer2/extractor/i;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->f:J

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/b/c;->e:I

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->g:J

    return-wide v0
.end method

.method public a()Lcom/google/android/exoplayer2/extractor/b/c$a;
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/exoplayer2/extractor/b/c$a;

    invoke-direct {v0, p0, v1}, Lcom/google/android/exoplayer2/extractor/b/c$a;-><init>(Lcom/google/android/exoplayer2/extractor/b/c;Lcom/google/android/exoplayer2/extractor/b/b;)V

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public bridge synthetic a()Lcom/google/android/exoplayer2/extractor/u;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/extractor/b/c;->a()Lcom/google/android/exoplayer2/extractor/b/c$a;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 10

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->f:J

    const-wide/16 v2, 0x1

    sub-long v8, v0, v2

    const-wide/16 v6, 0x0

    move-wide v4, p1

    invoke-static/range {v4 .. v9}, Lcom/google/android/exoplayer2/util/E;->b(JJJ)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->h:J

    const/4 p1, 0x2

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->e:I

    iget-wide p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->b:J

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->i:J

    iget-wide p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->c:J

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->j:J

    const-wide/16 p1, 0x0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->k:J

    iget-wide p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->f:J

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->l:J

    return-void
.end method

.method b(Lcom/google/android/exoplayer2/extractor/i;)J
    .locals 4
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_16

    nop

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    goto/32 :goto_1e

    nop

    :goto_1
    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_2
    and-int/2addr v1, v2

    goto/32 :goto_7

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_18

    :cond_0
    :goto_4
    goto/32 :goto_14

    nop

    :goto_5
    throw p1

    :goto_6
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v0

    goto/32 :goto_10

    nop

    :goto_7
    if-ne v1, v2, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_12

    nop

    :goto_8
    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    goto/32 :goto_5

    nop

    :goto_9
    iget-wide v0, p1, Lcom/google/android/exoplayer2/extractor/b/g;->c:J

    goto/32 :goto_17

    nop

    :goto_a
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    goto/32 :goto_9

    nop

    :goto_b
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    goto/32 :goto_1

    nop

    :goto_c
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    goto/32 :goto_1a

    nop

    :goto_d
    if-gez v0, :cond_2

    goto/32 :goto_4

    :cond_2
    :goto_e
    goto/32 :goto_a

    nop

    :goto_f
    const/4 v2, 0x4

    goto/32 :goto_2

    nop

    :goto_10
    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/b/c;->c:J

    goto/32 :goto_1f

    nop

    :goto_11
    iget v0, v0, Lcom/google/android/exoplayer2/extractor/b/g;->i:I

    goto/32 :goto_1d

    nop

    :goto_12
    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result v0

    goto/32 :goto_15

    nop

    :goto_13
    invoke-interface {p1, v1}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    goto/32 :goto_c

    nop

    :goto_14
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    goto/32 :goto_1b

    nop

    :goto_15
    if-nez v0, :cond_3

    goto/32 :goto_e

    :cond_3
    goto/32 :goto_6

    nop

    :goto_16
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/c;->a:Lcom/google/android/exoplayer2/extractor/b/g;

    goto/32 :goto_1c

    nop

    :goto_17
    return-wide v0

    :goto_18
    goto/32 :goto_20

    nop

    :goto_19
    invoke-virtual {v0, p1, v1}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;Z)Z

    goto/32 :goto_0

    nop

    :goto_1a
    iget v1, v0, Lcom/google/android/exoplayer2/extractor/b/g;->b:I

    goto/32 :goto_f

    nop

    :goto_1b
    const/4 v1, 0x0

    goto/32 :goto_19

    nop

    :goto_1c
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/b/g;->a()V

    goto/32 :goto_b

    nop

    :goto_1d
    add-int/2addr v1, v0

    goto/32 :goto_13

    nop

    :goto_1e
    iget v1, v0, Lcom/google/android/exoplayer2/extractor/b/g;->h:I

    goto/32 :goto_11

    nop

    :goto_1f
    cmp-long v0, v0, v2

    goto/32 :goto_d

    nop

    :goto_20
    new-instance p1, Ljava/io/EOFException;

    goto/32 :goto_8

    nop
.end method
