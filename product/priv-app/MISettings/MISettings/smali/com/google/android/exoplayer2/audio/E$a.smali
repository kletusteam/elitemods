.class final Lcom/google/android/exoplayer2/audio/E$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/audio/AudioSink$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/audio/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/exoplayer2/audio/E;


# direct methods
.method private constructor <init>(Lcom/google/android/exoplayer2/audio/E;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/E$a;->a:Lcom/google/android/exoplayer2/audio/E;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/audio/E;Lcom/google/android/exoplayer2/audio/D;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/audio/E$a;-><init>(Lcom/google/android/exoplayer2/audio/E;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/E$a;->a:Lcom/google/android/exoplayer2/audio/E;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/E;->R()V

    return-void
.end method

.method public a(IJJ)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/E$a;->a:Lcom/google/android/exoplayer2/audio/E;

    invoke-static {v0}, Lcom/google/android/exoplayer2/audio/E;->a(Lcom/google/android/exoplayer2/audio/E;)Lcom/google/android/exoplayer2/audio/s$a;

    move-result-object v1

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/exoplayer2/audio/s$a;->b(IJJ)V

    return-void
.end method

.method public a(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/E$a;->a:Lcom/google/android/exoplayer2/audio/E;

    invoke-static {v0}, Lcom/google/android/exoplayer2/audio/E;->a(Lcom/google/android/exoplayer2/audio/E;)Lcom/google/android/exoplayer2/audio/s$a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/audio/s$a;->b(J)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/E$a;->a:Lcom/google/android/exoplayer2/audio/E;

    invoke-static {v0}, Lcom/google/android/exoplayer2/audio/E;->b(Lcom/google/android/exoplayer2/audio/E;)Lcom/google/android/exoplayer2/Renderer$a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/E$a;->a:Lcom/google/android/exoplayer2/audio/E;

    invoke-static {v0}, Lcom/google/android/exoplayer2/audio/E;->b(Lcom/google/android/exoplayer2/audio/E;)Lcom/google/android/exoplayer2/Renderer$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/exoplayer2/Renderer$a;->a()V

    :cond_0
    return-void
.end method

.method public b(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/E$a;->a:Lcom/google/android/exoplayer2/audio/E;

    invoke-static {v0}, Lcom/google/android/exoplayer2/audio/E;->b(Lcom/google/android/exoplayer2/audio/E;)Lcom/google/android/exoplayer2/Renderer$a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/E$a;->a:Lcom/google/android/exoplayer2/audio/E;

    invoke-static {v0}, Lcom/google/android/exoplayer2/audio/E;->b(Lcom/google/android/exoplayer2/audio/E;)Lcom/google/android/exoplayer2/Renderer$a;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/Renderer$a;->a(J)V

    :cond_0
    return-void
.end method

.method public d(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/E$a;->a:Lcom/google/android/exoplayer2/audio/E;

    invoke-static {v0}, Lcom/google/android/exoplayer2/audio/E;->a(Lcom/google/android/exoplayer2/audio/E;)Lcom/google/android/exoplayer2/audio/s$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/audio/s$a;->a(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/E$a;->a:Lcom/google/android/exoplayer2/audio/E;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/audio/E;->g(I)V

    return-void
.end method

.method public f(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/E$a;->a:Lcom/google/android/exoplayer2/audio/E;

    invoke-static {v0}, Lcom/google/android/exoplayer2/audio/E;->a(Lcom/google/android/exoplayer2/audio/E;)Lcom/google/android/exoplayer2/audio/s$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/audio/s$a;->b(Z)V

    return-void
.end method
