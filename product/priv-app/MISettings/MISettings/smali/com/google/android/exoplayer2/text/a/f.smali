.class abstract Lcom/google/android/exoplayer2/text/a/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/text/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/text/a/f$b;,
        Lcom/google/android/exoplayer2/text/a/f$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lcom/google/android/exoplayer2/text/a/f$a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lcom/google/android/exoplayer2/text/k;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue<",
            "Lcom/google/android/exoplayer2/text/a/f$a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/android/exoplayer2/text/a/f$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->a:Ljava/util/ArrayDeque;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/exoplayer2/text/a/f;->a:Ljava/util/ArrayDeque;

    new-instance v3, Lcom/google/android/exoplayer2/text/a/f$a;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/exoplayer2/text/a/f$a;-><init>(Lcom/google/android/exoplayer2/text/a/e;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v1, p0, Lcom/google/android/exoplayer2/text/a/f;->b:Ljava/util/ArrayDeque;

    :goto_1
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer2/text/a/f;->b:Ljava/util/ArrayDeque;

    new-instance v2, Lcom/google/android/exoplayer2/text/a/f$b;

    new-instance v3, Lcom/google/android/exoplayer2/text/a/b;

    invoke-direct {v3, p0}, Lcom/google/android/exoplayer2/text/a/b;-><init>(Lcom/google/android/exoplayer2/text/a/f;)V

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/text/a/f$b;-><init>(Lcom/google/android/exoplayer2/decoder/g$a;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->c:Ljava/util/PriorityQueue;

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/text/a/f$a;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->clear()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/exoplayer2/text/k;
    .locals 9
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/text/g;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/text/a/f$a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/text/a/f$a;

    iget-wide v2, v0, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->d:J

    iget-wide v4, p0, Lcom/google/android/exoplayer2/text/a/f;->e:J

    cmp-long v0, v2, v4

    if-gtz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/text/a/f$a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/text/a/f$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/decoder/a;->isEndOfStream()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer2/text/a/f;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->pollFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/text/k;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/google/android/exoplayer2/text/k;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/decoder/a;->addFlag(I)V

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/text/a/f;->a(Lcom/google/android/exoplayer2/text/a/f$a;)V

    return-object v1

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/text/a/f;->a(Lcom/google/android/exoplayer2/text/j;)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/text/a/f;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/text/a/f;->c()Lcom/google/android/exoplayer2/text/e;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/exoplayer2/text/a/f;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->pollFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/text/k;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/google/android/exoplayer2/text/k;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->d:J

    const-wide v7, 0x7fffffffffffffffL

    move-object v3, v1

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/exoplayer2/text/k;->a(JLcom/google/android/exoplayer2/text/e;J)V

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/text/a/f;->a(Lcom/google/android/exoplayer2/text/a/f$a;)V

    return-object v1

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/text/a/f;->a(Lcom/google/android/exoplayer2/text/a/f$a;)V

    goto :goto_0

    :cond_3
    return-object v1
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/decoder/f;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/text/a/f;->a()Lcom/google/android/exoplayer2/text/k;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/text/a/f;->e:J

    return-void
.end method

.method protected abstract a(Lcom/google/android/exoplayer2/text/j;)V
.end method

.method protected a(Lcom/google/android/exoplayer2/text/k;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/text/k;->clear()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/decoder/f;
        }
    .end annotation

    check-cast p1, Lcom/google/android/exoplayer2/text/j;

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/text/a/f;->b(Lcom/google/android/exoplayer2/text/j;)V

    return-void
.end method

.method public b()Lcom/google/android/exoplayer2/text/j;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/text/g;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->d:Lcom/google/android/exoplayer2/text/a/f$a;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/text/a/f$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->d:Lcom/google/android/exoplayer2/text/a/f$a;

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->d:Lcom/google/android/exoplayer2/text/a/f$a;

    return-object v0
.end method

.method public bridge synthetic b()Ljava/lang/Object;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/decoder/f;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/text/a/f;->b()Lcom/google/android/exoplayer2/text/j;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/exoplayer2/text/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/text/g;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->d:Lcom/google/android/exoplayer2/text/a/f$a;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    check-cast p1, Lcom/google/android/exoplayer2/text/a/f$a;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/decoder/a;->isDecodeOnly()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/text/a/f;->a(Lcom/google/android/exoplayer2/text/a/f$a;)V

    goto :goto_1

    :cond_1
    iget-wide v0, p0, Lcom/google/android/exoplayer2/text/a/f;->f:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/android/exoplayer2/text/a/f;->f:J

    invoke-static {p1, v0, v1}, Lcom/google/android/exoplayer2/text/a/f$a;->a(Lcom/google/android/exoplayer2/text/a/f$a;J)J

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0, p1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    :goto_1
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/google/android/exoplayer2/text/a/f;->d:Lcom/google/android/exoplayer2/text/a/f$a;

    return-void
.end method

.method protected abstract c()Lcom/google/android/exoplayer2/text/e;
.end method

.method protected final d()Lcom/google/android/exoplayer2/text/k;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/text/k;

    return-object v0
.end method

.method protected final e()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/text/a/f;->e:J

    return-wide v0
.end method

.method protected abstract f()Z
.end method

.method public flush()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/text/a/f;->f:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/text/a/f;->e:J

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/text/a/f$a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/text/a/f$a;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/text/a/f;->a(Lcom/google/android/exoplayer2/text/a/f$a;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->d:Lcom/google/android/exoplayer2/text/a/f$a;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/text/a/f;->a(Lcom/google/android/exoplayer2/text/a/f$a;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/a/f;->d:Lcom/google/android/exoplayer2/text/a/f$a;

    :cond_1
    return-void
.end method

.method public release()V
    .locals 0

    return-void
.end method
