.class public interface abstract Lcom/google/android/exoplayer2/extractor/Extractor;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/Extractor$ReadResult;
    }
.end annotation


# virtual methods
.method public abstract a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract a(JJ)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/extractor/k;)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/extractor/i;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract release()V
.end method
