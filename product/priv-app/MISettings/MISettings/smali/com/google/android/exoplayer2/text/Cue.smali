.class public final Lcom/google/android/exoplayer2/text/Cue;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/text/Cue$a;,
        Lcom/google/android/exoplayer2/text/Cue$VerticalType;,
        Lcom/google/android/exoplayer2/text/Cue$TextSizeType;,
        Lcom/google/android/exoplayer2/text/Cue$LineType;,
        Lcom/google/android/exoplayer2/text/Cue$AnchorType;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/exoplayer2/text/Cue;


# instance fields
.field public final b:Ljava/lang/CharSequence;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final c:Landroid/text/Layout$Alignment;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final d:Landroid/graphics/Bitmap;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final e:F

.field public final f:I

.field public final g:I

.field public final h:F

.field public final i:I

.field public final j:F

.field public final k:F

.field public final l:Z

.field public final m:I

.field public final n:I

.field public final o:F

.field public final p:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/exoplayer2/text/Cue$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/text/Cue$a;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/text/Cue$a;->a(Ljava/lang/CharSequence;)Lcom/google/android/exoplayer2/text/Cue$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/text/Cue$a;->a()Lcom/google/android/exoplayer2/text/Cue;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/text/Cue;->a:Lcom/google/android/exoplayer2/text/Cue;

    return-void
.end method

.method private constructor <init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;Landroid/graphics/Bitmap;FIIFIIFFFZII)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/text/Layout$Alignment;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Bitmap;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez v1, :cond_0

    invoke-static {p3}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    if-nez v2, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    invoke-static {v3}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    :goto_1
    iput-object v1, v0, Lcom/google/android/exoplayer2/text/Cue;->b:Ljava/lang/CharSequence;

    move-object v1, p2

    iput-object v1, v0, Lcom/google/android/exoplayer2/text/Cue;->c:Landroid/text/Layout$Alignment;

    iput-object v2, v0, Lcom/google/android/exoplayer2/text/Cue;->d:Landroid/graphics/Bitmap;

    move v1, p4

    iput v1, v0, Lcom/google/android/exoplayer2/text/Cue;->e:F

    move v1, p5

    iput v1, v0, Lcom/google/android/exoplayer2/text/Cue;->f:I

    move v1, p6

    iput v1, v0, Lcom/google/android/exoplayer2/text/Cue;->g:I

    move v1, p7

    iput v1, v0, Lcom/google/android/exoplayer2/text/Cue;->h:F

    move v1, p8

    iput v1, v0, Lcom/google/android/exoplayer2/text/Cue;->i:I

    move v1, p11

    iput v1, v0, Lcom/google/android/exoplayer2/text/Cue;->j:F

    move/from16 v1, p12

    iput v1, v0, Lcom/google/android/exoplayer2/text/Cue;->k:F

    move/from16 v1, p13

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/text/Cue;->l:Z

    move/from16 v1, p14

    iput v1, v0, Lcom/google/android/exoplayer2/text/Cue;->m:I

    move v1, p9

    iput v1, v0, Lcom/google/android/exoplayer2/text/Cue;->n:I

    move v1, p10

    iput v1, v0, Lcom/google/android/exoplayer2/text/Cue;->o:F

    move/from16 v1, p15

    iput v1, v0, Lcom/google/android/exoplayer2/text/Cue;->p:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;Landroid/graphics/Bitmap;FIIFIIFFFZIILcom/google/android/exoplayer2/text/b;)V
    .locals 0

    invoke-direct/range {p0 .. p15}, Lcom/google/android/exoplayer2/text/Cue;-><init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;Landroid/graphics/Bitmap;FIIFIIFFFZII)V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 2

    new-instance v0, Lcom/google/android/exoplayer2/text/Cue$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/exoplayer2/text/Cue$a;-><init>(Lcom/google/android/exoplayer2/text/Cue;Lcom/google/android/exoplayer2/text/b;)V

    return-object v0
.end method
