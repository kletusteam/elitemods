.class public final Lcom/google/android/exoplayer2/extractor/ts/n;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/ts/m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/ts/n$a;
    }
.end annotation


# static fields
.field private static final a:[D


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

.field private final d:Lcom/google/android/exoplayer2/extractor/ts/I;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final e:Lcom/google/android/exoplayer2/util/t;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final f:Lcom/google/android/exoplayer2/extractor/ts/v;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final g:[Z

.field private final h:Lcom/google/android/exoplayer2/extractor/ts/n$a;

.field private i:J

.field private j:Z

.field private k:Z

.field private l:J

.field private m:J

.field private n:J

.field private o:J

.field private p:Z

.field private q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer2/extractor/ts/n;->a:[D

    return-void

    :array_0
    .array-data 8
        0x4037f9dcb5112287L    # 23.976023976023978
        0x4038000000000000L    # 24.0
        0x4039000000000000L    # 25.0
        0x403df853e2556b28L    # 29.97002997002997
        0x403e000000000000L    # 30.0
        0x4049000000000000L    # 50.0
        0x404df853e2556b28L    # 59.94005994005994
        0x404e000000000000L    # 60.0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/ts/n;-><init>(Lcom/google/android/exoplayer2/extractor/ts/I;)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/exoplayer2/extractor/ts/I;)V
    .locals 2
    .param p1    # Lcom/google/android/exoplayer2/extractor/ts/I;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->d:Lcom/google/android/exoplayer2/extractor/ts/I;

    const/4 v0, 0x4

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->g:[Z

    new-instance v0, Lcom/google/android/exoplayer2/extractor/ts/n$a;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/extractor/ts/n$a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->h:Lcom/google/android/exoplayer2/extractor/ts/n$a;

    if-eqz p1, :cond_0

    new-instance p1, Lcom/google/android/exoplayer2/extractor/ts/v;

    const/16 v0, 0xb2

    invoke-direct {p1, v0, v1}, Lcom/google/android/exoplayer2/extractor/ts/v;-><init>(II)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    new-instance p1, Lcom/google/android/exoplayer2/util/t;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/util/t;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->e:Lcom/google/android/exoplayer2/util/t;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->e:Lcom/google/android/exoplayer2/util/t;

    :goto_0
    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/extractor/ts/n$a;Ljava/lang/String;)Landroid/util/Pair;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/extractor/ts/n$a;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair<",
            "Lcom/google/android/exoplayer2/Format;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/n$a;->e:[B

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/ts/n$a;->c:I

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    const/4 v1, 0x4

    aget-byte v2, v0, v1

    and-int/lit16 v2, v2, 0xff

    const/4 v3, 0x5

    aget-byte v4, v0, v3

    and-int/lit16 v4, v4, 0xff

    const/4 v5, 0x6

    aget-byte v5, v0, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/2addr v2, v1

    shr-int/lit8 v6, v4, 0x4

    or-int/2addr v2, v6

    and-int/lit8 v4, v4, 0xf

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v4, v5

    const/4 v5, 0x7

    aget-byte v6, v0, v5

    and-int/lit16 v6, v6, 0xf0

    shr-int/2addr v6, v1

    const/4 v7, 0x2

    if-eq v6, v7, :cond_2

    const/4 v7, 0x3

    if-eq v6, v7, :cond_1

    if-eq v6, v1, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_1

    :cond_0
    mul-int/lit8 v1, v4, 0x79

    int-to-float v1, v1

    mul-int/lit8 v6, v2, 0x64

    goto :goto_0

    :cond_1
    mul-int/lit8 v1, v4, 0x10

    int-to-float v1, v1

    mul-int/lit8 v6, v2, 0x9

    goto :goto_0

    :cond_2
    mul-int/lit8 v1, v4, 0x4

    int-to-float v1, v1

    mul-int/lit8 v6, v2, 0x3

    :goto_0
    int-to-float v6, v6

    div-float/2addr v1, v6

    :goto_1
    new-instance v6, Lcom/google/android/exoplayer2/Format$a;

    invoke-direct {v6}, Lcom/google/android/exoplayer2/Format$a;-><init>()V

    invoke-virtual {v6, p1}, Lcom/google/android/exoplayer2/Format$a;->b(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    const-string p1, "video/mpeg2"

    invoke-virtual {v6, p1}, Lcom/google/android/exoplayer2/Format$a;->e(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v6, v2}, Lcom/google/android/exoplayer2/Format$a;->o(I)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v6, v4}, Lcom/google/android/exoplayer2/Format$a;->f(I)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v6, v1}, Lcom/google/android/exoplayer2/Format$a;->b(F)Lcom/google/android/exoplayer2/Format$a;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v6, p1}, Lcom/google/android/exoplayer2/Format$a;->a(Ljava/util/List;)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/Format$a;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object p1

    const-wide/16 v1, 0x0

    aget-byte v4, v0, v5

    and-int/lit8 v4, v4, 0xf

    add-int/lit8 v4, v4, -0x1

    if-ltz v4, :cond_4

    sget-object v5, Lcom/google/android/exoplayer2/extractor/ts/n;->a:[D

    array-length v6, v5

    if-ge v4, v6, :cond_4

    aget-wide v1, v5, v4

    iget p0, p0, Lcom/google/android/exoplayer2/extractor/ts/n$a;->d:I

    add-int/lit8 p0, p0, 0x9

    aget-byte v4, v0, p0

    and-int/lit8 v4, v4, 0x60

    shr-int/lit8 v3, v4, 0x5

    aget-byte p0, v0, p0

    and-int/lit8 p0, p0, 0x1f

    if-eq v3, p0, :cond_3

    int-to-double v3, v3

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    add-double/2addr v3, v5

    add-int/lit8 p0, p0, 0x1

    int-to-double v5, p0

    div-double/2addr v3, v5

    mul-double/2addr v1, v3

    :cond_3
    const-wide v3, 0x412e848000000000L    # 1000000.0

    div-double/2addr v3, v1

    double-to-long v1, v3

    :cond_4
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-static {p1, p0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->g:[Z

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/r;->a([Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->h:Lcom/google/android/exoplayer2/extractor/ts/n$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/n$a;->a()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/ts/v;->b()V

    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->i:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->j:Z

    return-void
.end method

.method public a(JI)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->m:J

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;)V
    .locals 2

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->a()V

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->b:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->c()I

    move-result v0

    const/4 v1, 0x2

    invoke-interface {p1, v0, v1}, Lcom/google/android/exoplayer2/extractor/k;->a(II)Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/n;->d:Lcom/google/android/exoplayer2/extractor/ts/I;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/ts/I;->a(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/util/t;)V
    .locals 19

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v3

    iget-wide v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->i:J

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v4, v6

    iput-wide v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->i:J

    iget-object v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v5

    move-object/from16 v6, p1

    invoke-interface {v4, v6, v5}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/util/t;I)V

    :goto_0
    iget-object v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->g:[Z

    invoke-static {v3, v1, v2, v4}, Lcom/google/android/exoplayer2/util/r;->a([BII[Z)I

    move-result v4

    if-ne v4, v2, :cond_2

    iget-boolean v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->k:Z

    if-nez v4, :cond_0

    iget-object v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->h:Lcom/google/android/exoplayer2/extractor/ts/n$a;

    invoke-virtual {v4, v3, v1, v2}, Lcom/google/android/exoplayer2/extractor/ts/n$a;->a([BII)V

    :cond_0
    iget-object v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    if-eqz v4, :cond_1

    invoke-virtual {v4, v3, v1, v2}, Lcom/google/android/exoplayer2/extractor/ts/v;->a([BII)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v5

    add-int/lit8 v7, v4, 0x3

    aget-byte v5, v5, v7

    and-int/lit16 v5, v5, 0xff

    sub-int v8, v4, v1

    iget-boolean v9, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->k:Z

    const/4 v10, 0x0

    const/4 v11, 0x1

    if-nez v9, :cond_5

    if-lez v8, :cond_3

    iget-object v9, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->h:Lcom/google/android/exoplayer2/extractor/ts/n$a;

    invoke-virtual {v9, v3, v1, v4}, Lcom/google/android/exoplayer2/extractor/ts/n$a;->a([BII)V

    :cond_3
    if-gez v8, :cond_4

    neg-int v9, v8

    goto :goto_1

    :cond_4
    move v9, v10

    :goto_1
    iget-object v12, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->h:Lcom/google/android/exoplayer2/extractor/ts/n$a;

    invoke-virtual {v12, v5, v9}, Lcom/google/android/exoplayer2/extractor/ts/n$a;->a(II)Z

    move-result v9

    if-eqz v9, :cond_5

    iget-object v9, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->h:Lcom/google/android/exoplayer2/extractor/ts/n$a;

    iget-object v12, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->b:Ljava/lang/String;

    invoke-static {v12}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v12, Ljava/lang/String;

    invoke-static {v9, v12}, Lcom/google/android/exoplayer2/extractor/ts/n;->a(Lcom/google/android/exoplayer2/extractor/ts/n$a;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v9

    iget-object v12, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-object v13, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v13, Lcom/google/android/exoplayer2/Format;

    invoke-interface {v12, v13}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/Format;)V

    iget-object v9, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    iput-wide v12, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->l:J

    iput-boolean v11, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->k:Z

    :cond_5
    iget-object v9, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    if-eqz v9, :cond_8

    if-lez v8, :cond_6

    invoke-virtual {v9, v3, v1, v4}, Lcom/google/android/exoplayer2/extractor/ts/v;->a([BII)V

    move v1, v10

    goto :goto_2

    :cond_6
    neg-int v1, v8

    :goto_2
    iget-object v8, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v8, v1}, Lcom/google/android/exoplayer2/extractor/ts/v;->a(I)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    iget-object v8, v1, Lcom/google/android/exoplayer2/extractor/ts/v;->d:[B

    iget v1, v1, Lcom/google/android/exoplayer2/extractor/ts/v;->e:I

    invoke-static {v8, v1}, Lcom/google/android/exoplayer2/util/r;->c([BI)I

    move-result v1

    iget-object v8, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-static {v8}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v8, Lcom/google/android/exoplayer2/util/t;

    iget-object v9, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    iget-object v9, v9, Lcom/google/android/exoplayer2/extractor/ts/v;->d:[B

    invoke-virtual {v8, v9, v1}, Lcom/google/android/exoplayer2/util/t;->a([BI)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->d:Lcom/google/android/exoplayer2/extractor/ts/I;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/google/android/exoplayer2/extractor/ts/I;

    iget-wide v8, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->o:J

    iget-object v12, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v1, v8, v9, v12}, Lcom/google/android/exoplayer2/extractor/ts/I;->a(JLcom/google/android/exoplayer2/util/t;)V

    :cond_7
    const/16 v1, 0xb2

    if-ne v5, v1, :cond_8

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v1

    add-int/lit8 v8, v4, 0x2

    aget-byte v1, v1, v8

    if-ne v1, v11, :cond_8

    iget-object v1, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->f:Lcom/google/android/exoplayer2/extractor/ts/v;

    invoke-virtual {v1, v5}, Lcom/google/android/exoplayer2/extractor/ts/v;->b(I)V

    :cond_8
    if-eqz v5, :cond_a

    const/16 v1, 0xb3

    if-ne v5, v1, :cond_9

    goto :goto_3

    :cond_9
    const/16 v1, 0xb8

    if-ne v5, v1, :cond_11

    iput-boolean v11, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->p:Z

    goto :goto_5

    :cond_a
    :goto_3
    sub-int v1, v2, v4

    iget-boolean v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->j:Z

    if-eqz v4, :cond_b

    iget-boolean v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->q:Z

    if-eqz v4, :cond_b

    iget-boolean v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->k:Z

    if-eqz v4, :cond_b

    iget-boolean v15, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->p:Z

    iget-wide v8, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->i:J

    iget-wide v12, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->n:J

    sub-long/2addr v8, v12

    long-to-int v4, v8

    sub-int v16, v4, v1

    iget-object v12, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-wide v13, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->o:J

    const/16 v18, 0x0

    move/from16 v17, v1

    invoke-interface/range {v12 .. v18}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(JIIILcom/google/android/exoplayer2/extractor/TrackOutput$a;)V

    :cond_b
    iget-boolean v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->j:Z

    if-eqz v4, :cond_c

    iget-boolean v4, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->q:Z

    if-eqz v4, :cond_f

    :cond_c
    iget-wide v8, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->i:J

    int-to-long v12, v1

    sub-long/2addr v8, v12

    iput-wide v8, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->n:J

    iget-wide v8, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->m:J

    const-wide v12, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, v8, v12

    if-eqz v1, :cond_d

    goto :goto_4

    :cond_d
    iget-boolean v1, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->j:Z

    if-eqz v1, :cond_e

    iget-wide v8, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->o:J

    iget-wide v14, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->l:J

    add-long/2addr v8, v14

    goto :goto_4

    :cond_e
    const-wide/16 v8, 0x0

    :goto_4
    iput-wide v8, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->o:J

    iput-boolean v10, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->p:Z

    iput-wide v12, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->m:J

    iput-boolean v11, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->j:Z

    :cond_f
    if-nez v5, :cond_10

    move v10, v11

    :cond_10
    iput-boolean v10, v0, Lcom/google/android/exoplayer2/extractor/ts/n;->q:Z

    :cond_11
    :goto_5
    move v1, v7

    goto/16 :goto_0
.end method

.method public b()V
    .locals 0

    return-void
.end method
