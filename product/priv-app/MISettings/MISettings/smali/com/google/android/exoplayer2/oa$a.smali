.class public final Lcom/google/android/exoplayer2/oa$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/oa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/exoplayer2/la;

.field private c:Lcom/google/android/exoplayer2/util/e;

.field private d:Lcom/google/android/exoplayer2/trackselection/o;

.field private e:Lcom/google/android/exoplayer2/source/B;

.field private f:Lcom/google/android/exoplayer2/U;

.field private g:Lcom/google/android/exoplayer2/upstream/g;

.field private h:Lcom/google/android/exoplayer2/a/a;

.field private i:Landroid/os/Looper;

.field private j:Lcom/google/android/exoplayer2/util/v;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/google/android/exoplayer2/audio/o;

.field private l:Z

.field private m:I

.field private n:Z

.field private o:Z

.field private p:I

.field private q:Z

.field private r:Lcom/google/android/exoplayer2/ma;

.field private s:Z

.field private t:Z

.field private u:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/exoplayer2/la;)V
    .locals 1

    new-instance v0, Lcom/google/android/exoplayer2/extractor/g;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/g;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/exoplayer2/oa$a;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/extractor/m;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/extractor/m;)V
    .locals 8

    new-instance v3, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;

    invoke-direct {v3, p1}, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/google/android/exoplayer2/source/n;

    invoke-direct {v4, p1, p3}, Lcom/google/android/exoplayer2/source/n;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer2/extractor/m;)V

    new-instance v5, Lcom/google/android/exoplayer2/I;

    invoke-direct {v5}, Lcom/google/android/exoplayer2/I;-><init>()V

    invoke-static {p1}, Lcom/google/android/exoplayer2/upstream/q;->a(Landroid/content/Context;)Lcom/google/android/exoplayer2/upstream/q;

    move-result-object v6

    new-instance v7, Lcom/google/android/exoplayer2/a/a;

    sget-object p3, Lcom/google/android/exoplayer2/util/e;->a:Lcom/google/android/exoplayer2/util/e;

    invoke-direct {v7, p3}, Lcom/google/android/exoplayer2/a/a;-><init>(Lcom/google/android/exoplayer2/util/e;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer2/oa$a;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/source/B;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/upstream/g;Lcom/google/android/exoplayer2/a/a;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/source/B;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/upstream/g;Lcom/google/android/exoplayer2/a/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa$a;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/exoplayer2/oa$a;->b:Lcom/google/android/exoplayer2/la;

    iput-object p3, p0, Lcom/google/android/exoplayer2/oa$a;->d:Lcom/google/android/exoplayer2/trackselection/o;

    iput-object p4, p0, Lcom/google/android/exoplayer2/oa$a;->e:Lcom/google/android/exoplayer2/source/B;

    iput-object p5, p0, Lcom/google/android/exoplayer2/oa$a;->f:Lcom/google/android/exoplayer2/U;

    iput-object p6, p0, Lcom/google/android/exoplayer2/oa$a;->g:Lcom/google/android/exoplayer2/upstream/g;

    iput-object p7, p0, Lcom/google/android/exoplayer2/oa$a;->h:Lcom/google/android/exoplayer2/a/a;

    invoke-static {}, Lcom/google/android/exoplayer2/util/E;->c()Landroid/os/Looper;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa$a;->i:Landroid/os/Looper;

    sget-object p1, Lcom/google/android/exoplayer2/audio/o;->a:Lcom/google/android/exoplayer2/audio/o;

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa$a;->k:Lcom/google/android/exoplayer2/audio/o;

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/exoplayer2/oa$a;->m:I

    const/4 p1, 0x1

    iput p1, p0, Lcom/google/android/exoplayer2/oa$a;->p:I

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/oa$a;->q:Z

    sget-object p2, Lcom/google/android/exoplayer2/ma;->e:Lcom/google/android/exoplayer2/ma;

    iput-object p2, p0, Lcom/google/android/exoplayer2/oa$a;->r:Lcom/google/android/exoplayer2/ma;

    sget-object p2, Lcom/google/android/exoplayer2/util/e;->a:Lcom/google/android/exoplayer2/util/e;

    iput-object p2, p0, Lcom/google/android/exoplayer2/oa$a;->c:Lcom/google/android/exoplayer2/util/e;

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/oa$a;->t:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/a/a;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->h:Lcom/google/android/exoplayer2/a/a;

    return-object p0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/util/v;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->j:Lcom/google/android/exoplayer2/util/v;

    return-object p0
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/U;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->f:Lcom/google/android/exoplayer2/U;

    return-object p0
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/upstream/g;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->g:Lcom/google/android/exoplayer2/upstream/g;

    return-object p0
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/oa$a;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/google/android/exoplayer2/oa$a;->q:Z

    return p0
.end method

.method static synthetic f(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/ma;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->r:Lcom/google/android/exoplayer2/ma;

    return-object p0
.end method

.method static synthetic g(Lcom/google/android/exoplayer2/oa$a;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/google/android/exoplayer2/oa$a;->s:Z

    return p0
.end method

.method static synthetic h(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/util/e;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->c:Lcom/google/android/exoplayer2/util/e;

    return-object p0
.end method

.method static synthetic i(Lcom/google/android/exoplayer2/oa$a;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic j(Lcom/google/android/exoplayer2/oa$a;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/google/android/exoplayer2/oa$a;->n:Z

    return p0
.end method

.method static synthetic k(Lcom/google/android/exoplayer2/oa$a;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/google/android/exoplayer2/oa$a;->l:Z

    return p0
.end method

.method static synthetic l(Lcom/google/android/exoplayer2/oa$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/oa$a;->m:I

    return p0
.end method

.method static synthetic m(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/audio/o;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->k:Lcom/google/android/exoplayer2/audio/o;

    return-object p0
.end method

.method static synthetic n(Lcom/google/android/exoplayer2/oa$a;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/google/android/exoplayer2/oa$a;->t:Z

    return p0
.end method

.method static synthetic o(Lcom/google/android/exoplayer2/oa$a;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/oa$a;->p:I

    return p0
.end method

.method static synthetic p(Lcom/google/android/exoplayer2/oa$a;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/google/android/exoplayer2/oa$a;->o:Z

    return p0
.end method

.method static synthetic q(Lcom/google/android/exoplayer2/oa$a;)Landroid/os/Looper;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->i:Landroid/os/Looper;

    return-object p0
.end method

.method static synthetic r(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/la;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->b:Lcom/google/android/exoplayer2/la;

    return-object p0
.end method

.method static synthetic s(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/trackselection/o;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->d:Lcom/google/android/exoplayer2/trackselection/o;

    return-object p0
.end method

.method static synthetic t(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/source/B;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa$a;->e:Lcom/google/android/exoplayer2/source/B;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/os/Looper;)Lcom/google/android/exoplayer2/oa$a;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa$a;->u:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa$a;->i:Landroid/os/Looper;

    return-object p0
.end method

.method public a(Lcom/google/android/exoplayer2/U;)Lcom/google/android/exoplayer2/oa$a;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa$a;->u:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa$a;->f:Lcom/google/android/exoplayer2/U;

    return-object p0
.end method

.method public a(Lcom/google/android/exoplayer2/a/a;)Lcom/google/android/exoplayer2/oa$a;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa$a;->u:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa$a;->h:Lcom/google/android/exoplayer2/a/a;

    return-object p0
.end method

.method public a(Lcom/google/android/exoplayer2/source/B;)Lcom/google/android/exoplayer2/oa$a;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa$a;->u:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa$a;->e:Lcom/google/android/exoplayer2/source/B;

    return-object p0
.end method

.method public a(Lcom/google/android/exoplayer2/trackselection/o;)Lcom/google/android/exoplayer2/oa$a;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa$a;->u:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa$a;->d:Lcom/google/android/exoplayer2/trackselection/o;

    return-object p0
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/g;)Lcom/google/android/exoplayer2/oa$a;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa$a;->u:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa$a;->g:Lcom/google/android/exoplayer2/upstream/g;

    return-object p0
.end method

.method public a(Lcom/google/android/exoplayer2/util/e;)Lcom/google/android/exoplayer2/oa$a;
    .locals 1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa$a;->u:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa$a;->c:Lcom/google/android/exoplayer2/util/e;

    return-object p0
.end method

.method public a(Z)Lcom/google/android/exoplayer2/oa$a;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa$a;->u:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/oa$a;->q:Z

    return-object p0
.end method
