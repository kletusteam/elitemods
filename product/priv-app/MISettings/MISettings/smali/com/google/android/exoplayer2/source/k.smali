.class final Lcom/google/android/exoplayer2/source/k;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/source/C;


# instance fields
.field private final a:Lcom/google/android/exoplayer2/extractor/m;

.field private b:Lcom/google/android/exoplayer2/extractor/Extractor;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private c:Lcom/google/android/exoplayer2/extractor/i;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/extractor/m;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/k;->a:Lcom/google/android/exoplayer2/extractor/m;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/t;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/extractor/Extractor;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/k;->c:Lcom/google/android/exoplayer2/extractor/i;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/google/android/exoplayer2/extractor/i;

    invoke-interface {v0, v1, p1}, Lcom/google/android/exoplayer2/extractor/Extractor;->a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I

    move-result p1

    return p1
.end method

.method public a()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/k;->c:Lcom/google/android/exoplayer2/extractor/i;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0
.end method

.method public a(JJ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/extractor/Extractor;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/extractor/Extractor;->a(JJ)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/j;Landroid/net/Uri;Ljava/util/Map;JJLcom/google/android/exoplayer2/extractor/k;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/upstream/j;",
            "Landroid/net/Uri;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;JJ",
            "Lcom/google/android/exoplayer2/extractor/k;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v6, Lcom/google/android/exoplayer2/extractor/f;

    move-object v0, v6

    move-object v1, p1

    move-wide v2, p4

    move-wide v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/extractor/f;-><init>(Lcom/google/android/exoplayer2/upstream/j;JJ)V

    iput-object v6, p0, Lcom/google/android/exoplayer2/source/k;->c:Lcom/google/android/exoplayer2/extractor/i;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    if-eqz p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/k;->a:Lcom/google/android/exoplayer2/extractor/m;

    invoke-interface {p1, p2, p3}, Lcom/google/android/exoplayer2/extractor/m;->a(Landroid/net/Uri;Ljava/util/Map;)[Lcom/google/android/exoplayer2/extractor/Extractor;

    move-result-object p1

    array-length p3, p1

    const/4 p6, 0x0

    const/4 p7, 0x1

    if-ne p3, p7, :cond_1

    aget-object p1, p1, p6

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    goto/16 :goto_4

    :cond_1
    array-length p3, p1

    move v0, p6

    :goto_0
    if-ge v0, p3, :cond_9

    aget-object v1, p1, v0

    :try_start_0
    invoke-interface {v1, v6}, Lcom/google/android/exoplayer2/extractor/Extractor;->a(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result v2

    if-eqz v2, :cond_4

    iput-object v1, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p3, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    if-nez p3, :cond_2

    invoke-interface {v6}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v0

    cmp-long p3, v0, p4

    if-nez p3, :cond_3

    :cond_2
    move p6, p7

    :cond_3
    invoke-static {p6}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    invoke-interface {v6}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    if-nez v1, :cond_8

    invoke-interface {v6}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v1

    cmp-long v1, v1, p4

    if-nez v1, :cond_7

    goto :goto_1

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    if-nez p2, :cond_5

    invoke-interface {v6}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide p2

    cmp-long p2, p2, p4

    if-nez p2, :cond_6

    :cond_5
    move p6, p7

    :cond_6
    invoke-static {p6}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    invoke-interface {v6}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    throw p1

    :catch_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    if-nez v1, :cond_8

    invoke-interface {v6}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v1

    cmp-long v1, v1, p4

    if-nez v1, :cond_7

    goto :goto_1

    :cond_7
    move v1, p6

    goto :goto_2

    :cond_8
    :goto_1
    move v1, p7

    :goto_2
    invoke-static {v1}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    invoke-interface {v6}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_9
    :goto_3
    iget-object p3, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    if-eqz p3, :cond_a

    :goto_4
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    invoke-interface {p1, p8}, Lcom/google/android/exoplayer2/extractor/Extractor;->a(Lcom/google/android/exoplayer2/extractor/k;)V

    return-void

    :cond_a
    new-instance p3, Lcom/google/android/exoplayer2/source/N;

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/E;->b([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result p4

    add-int/lit8 p4, p4, 0x3a

    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5, p4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string p4, "None of the available extractors ("

    invoke-virtual {p5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ") could read the stream."

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p2, Landroid/net/Uri;

    invoke-direct {p3, p1, p2}, Lcom/google/android/exoplayer2/source/N;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    throw p3
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    instance-of v1, v0, Lcom/google/android/exoplayer2/extractor/mp3/Mp3Extractor;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/mp3/Mp3Extractor;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/mp3/Mp3Extractor;->a()V

    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/Extractor;->release()V

    iput-object v1, p0, Lcom/google/android/exoplayer2/source/k;->b:Lcom/google/android/exoplayer2/extractor/Extractor;

    :cond_0
    iput-object v1, p0, Lcom/google/android/exoplayer2/source/k;->c:Lcom/google/android/exoplayer2/extractor/i;

    return-void
.end method
