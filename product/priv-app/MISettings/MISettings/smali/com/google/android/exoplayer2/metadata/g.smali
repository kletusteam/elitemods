.class public final Lcom/google/android/exoplayer2/metadata/g;
.super Lcom/google/android/exoplayer2/F;

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private final m:Lcom/google/android/exoplayer2/metadata/d;

.field private final n:Lcom/google/android/exoplayer2/metadata/f;

.field private final o:Landroid/os/Handler;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final p:Lcom/google/android/exoplayer2/metadata/e;

.field private final q:[Lcom/google/android/exoplayer2/metadata/Metadata;

.field private final r:[J

.field private s:I

.field private t:I

.field private u:Lcom/google/android/exoplayer2/metadata/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private v:Z

.field private w:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/metadata/f;Landroid/os/Looper;)V
    .locals 1
    .param p2    # Landroid/os/Looper;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    sget-object v0, Lcom/google/android/exoplayer2/metadata/d;->a:Lcom/google/android/exoplayer2/metadata/d;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/exoplayer2/metadata/g;-><init>(Lcom/google/android/exoplayer2/metadata/f;Landroid/os/Looper;Lcom/google/android/exoplayer2/metadata/d;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/exoplayer2/metadata/f;Landroid/os/Looper;Lcom/google/android/exoplayer2/metadata/d;)V
    .locals 1
    .param p2    # Landroid/os/Looper;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/F;-><init>(I)V

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/metadata/f;

    iput-object p1, p0, Lcom/google/android/exoplayer2/metadata/g;->n:Lcom/google/android/exoplayer2/metadata/f;

    if-nez p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p2, p0}, Lcom/google/android/exoplayer2/util/E;->a(Landroid/os/Looper;Landroid/os/Handler$Callback;)Landroid/os/Handler;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/metadata/g;->o:Landroid/os/Handler;

    invoke-static {p3}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p3, Lcom/google/android/exoplayer2/metadata/d;

    iput-object p3, p0, Lcom/google/android/exoplayer2/metadata/g;->m:Lcom/google/android/exoplayer2/metadata/d;

    new-instance p1, Lcom/google/android/exoplayer2/metadata/e;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/metadata/e;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    const/4 p1, 0x5

    new-array p2, p1, [Lcom/google/android/exoplayer2/metadata/Metadata;

    iput-object p2, p0, Lcom/google/android/exoplayer2/metadata/g;->q:[Lcom/google/android/exoplayer2/metadata/Metadata;

    new-array p1, p1, [J

    iput-object p1, p0, Lcom/google/android/exoplayer2/metadata/g;->r:[J

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/metadata/Metadata;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/metadata/g;->o:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/metadata/g;->b(Lcom/google/android/exoplayer2/metadata/Metadata;)V

    :goto_0
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/metadata/Metadata;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/metadata/Metadata;",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/metadata/Metadata$Entry;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/metadata/Metadata;->c()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/metadata/Metadata;->a(I)Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/exoplayer2/metadata/Metadata$Entry;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/exoplayer2/metadata/g;->m:Lcom/google/android/exoplayer2/metadata/d;

    invoke-interface {v2, v1}, Lcom/google/android/exoplayer2/metadata/d;->a(Lcom/google/android/exoplayer2/Format;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/exoplayer2/metadata/g;->m:Lcom/google/android/exoplayer2/metadata/d;

    invoke-interface {v2, v1}, Lcom/google/android/exoplayer2/metadata/d;->b(Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/metadata/b;

    move-result-object v1

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/metadata/Metadata;->a(I)Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/exoplayer2/metadata/Metadata$Entry;->b()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v2, [B

    iget-object v3, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->clear()V

    iget-object v3, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    array-length v4, v2

    invoke-virtual {v3, v4}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->b(I)V

    iget-object v3, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    iget-object v3, v3, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->b:Ljava/nio/ByteBuffer;

    invoke-static {v3}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v3, Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->b()V

    iget-object v2, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    invoke-interface {v1, v2}, Lcom/google/android/exoplayer2/metadata/b;->a(Lcom/google/android/exoplayer2/metadata/e;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v1, p2}, Lcom/google/android/exoplayer2/metadata/g;->a(Lcom/google/android/exoplayer2/metadata/Metadata;Ljava/util/List;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/metadata/Metadata;->a(I)Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/metadata/Metadata;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/metadata/g;->n:Lcom/google/android/exoplayer2/metadata/f;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/metadata/f;->a(Lcom/google/android/exoplayer2/metadata/Metadata;)V

    return-void
.end method

.method private y()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/metadata/g;->q:[Lcom/google/android/exoplayer2/metadata/Metadata;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/metadata/g;->s:I

    iput v0, p0, Lcom/google/android/exoplayer2/metadata/g;->t:I

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/Format;)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/metadata/g;->m:Lcom/google/android/exoplayer2/metadata/d;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/metadata/d;->a(Lcom/google/android/exoplayer2/Format;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p1, Lcom/google/android/exoplayer2/Format;->E:Ljava/lang/Class;

    if-nez p1, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :goto_0
    invoke-static {p1}, Lcom/google/android/exoplayer2/RendererCapabilities;->d(I)I

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x0

    invoke-static {p1}, Lcom/google/android/exoplayer2/RendererCapabilities;->d(I)I

    move-result p1

    return p1
.end method

.method public a(JJ)V
    .locals 5

    iget-boolean p3, p0, Lcom/google/android/exoplayer2/metadata/g;->v:Z

    const/4 p4, 0x5

    const/4 v0, 0x1

    if-nez p3, :cond_2

    iget p3, p0, Lcom/google/android/exoplayer2/metadata/g;->t:I

    if-ge p3, p4, :cond_2

    iget-object p3, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    invoke-virtual {p3}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->clear()V

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/F;->q()Lcom/google/android/exoplayer2/S;

    move-result-object p3

    iget-object v1, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    const/4 v2, 0x0

    invoke-virtual {p0, p3, v1, v2}, Lcom/google/android/exoplayer2/F;->a(Lcom/google/android/exoplayer2/S;Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;Z)I

    move-result v1

    const/4 v2, -0x4

    if-ne v1, v2, :cond_1

    iget-object p3, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    invoke-virtual {p3}, Lcom/google/android/exoplayer2/decoder/a;->isEndOfStream()Z

    move-result p3

    if-eqz p3, :cond_0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/metadata/g;->v:Z

    goto :goto_0

    :cond_0
    iget-object p3, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    iget-wide v1, p0, Lcom/google/android/exoplayer2/metadata/g;->w:J

    iput-wide v1, p3, Lcom/google/android/exoplayer2/metadata/e;->h:J

    invoke-virtual {p3}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->b()V

    iget-object p3, p0, Lcom/google/android/exoplayer2/metadata/g;->u:Lcom/google/android/exoplayer2/metadata/b;

    invoke-static {p3}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p3, Lcom/google/android/exoplayer2/metadata/b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    invoke-interface {p3, v1}, Lcom/google/android/exoplayer2/metadata/b;->a(Lcom/google/android/exoplayer2/metadata/e;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object p3

    if-eqz p3, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p3}, Lcom/google/android/exoplayer2/metadata/Metadata;->c()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {p0, p3, v1}, Lcom/google/android/exoplayer2/metadata/g;->a(Lcom/google/android/exoplayer2/metadata/Metadata;Ljava/util/List;)V

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result p3

    if-nez p3, :cond_2

    new-instance p3, Lcom/google/android/exoplayer2/metadata/Metadata;

    invoke-direct {p3, v1}, Lcom/google/android/exoplayer2/metadata/Metadata;-><init>(Ljava/util/List;)V

    iget v1, p0, Lcom/google/android/exoplayer2/metadata/g;->s:I

    iget v2, p0, Lcom/google/android/exoplayer2/metadata/g;->t:I

    add-int/2addr v1, v2

    rem-int/2addr v1, p4

    iget-object v3, p0, Lcom/google/android/exoplayer2/metadata/g;->q:[Lcom/google/android/exoplayer2/metadata/Metadata;

    aput-object p3, v3, v1

    iget-object p3, p0, Lcom/google/android/exoplayer2/metadata/g;->r:[J

    iget-object v3, p0, Lcom/google/android/exoplayer2/metadata/g;->p:Lcom/google/android/exoplayer2/metadata/e;

    iget-wide v3, v3, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->d:J

    aput-wide v3, p3, v1

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/exoplayer2/metadata/g;->t:I

    goto :goto_0

    :cond_1
    const/4 v2, -0x5

    if-ne v1, v2, :cond_2

    iget-object p3, p3, Lcom/google/android/exoplayer2/S;->b:Lcom/google/android/exoplayer2/Format;

    invoke-static {p3}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p3, Lcom/google/android/exoplayer2/Format;

    iget-wide v1, p3, Lcom/google/android/exoplayer2/Format;->p:J

    iput-wide v1, p0, Lcom/google/android/exoplayer2/metadata/g;->w:J

    :cond_2
    :goto_0
    iget p3, p0, Lcom/google/android/exoplayer2/metadata/g;->t:I

    if-lez p3, :cond_3

    iget-object p3, p0, Lcom/google/android/exoplayer2/metadata/g;->r:[J

    iget v1, p0, Lcom/google/android/exoplayer2/metadata/g;->s:I

    aget-wide v2, p3, v1

    cmp-long p1, v2, p1

    if-gtz p1, :cond_3

    iget-object p1, p0, Lcom/google/android/exoplayer2/metadata/g;->q:[Lcom/google/android/exoplayer2/metadata/Metadata;

    aget-object p1, p1, v1

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/metadata/Metadata;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/metadata/g;->a(Lcom/google/android/exoplayer2/metadata/Metadata;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/metadata/g;->q:[Lcom/google/android/exoplayer2/metadata/Metadata;

    iget p2, p0, Lcom/google/android/exoplayer2/metadata/g;->s:I

    const/4 p3, 0x0

    aput-object p3, p1, p2

    add-int/2addr p2, v0

    rem-int/2addr p2, p4

    iput p2, p0, Lcom/google/android/exoplayer2/metadata/g;->s:I

    iget p1, p0, Lcom/google/android/exoplayer2/metadata/g;->t:I

    sub-int/2addr p1, v0

    iput p1, p0, Lcom/google/android/exoplayer2/metadata/g;->t:I

    :cond_3
    return-void
.end method

.method protected a(JZ)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/metadata/g;->y()V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/metadata/g;->v:Z

    return-void
.end method

.method protected a([Lcom/google/android/exoplayer2/Format;JJ)V
    .locals 0

    iget-object p2, p0, Lcom/google/android/exoplayer2/metadata/g;->m:Lcom/google/android/exoplayer2/metadata/d;

    const/4 p3, 0x0

    aget-object p1, p1, p3

    invoke-interface {p2, p1}, Lcom/google/android/exoplayer2/metadata/d;->b(Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/metadata/b;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/metadata/g;->u:Lcom/google/android/exoplayer2/metadata/b;

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/metadata/g;->v:Z

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "MetadataRenderer"

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 1

    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/metadata/Metadata;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/metadata/g;->b(Lcom/google/android/exoplayer2/metadata/Metadata;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method protected u()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/metadata/g;->y()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/metadata/g;->u:Lcom/google/android/exoplayer2/metadata/b;

    return-void
.end method
