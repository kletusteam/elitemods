.class public Lcom/google/android/exoplayer2/trackselection/e;
.super Lcom/google/android/exoplayer2/trackselection/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/trackselection/e$b;,
        Lcom/google/android/exoplayer2/trackselection/e$a;,
        Lcom/google/android/exoplayer2/trackselection/e$c;
    }
.end annotation


# instance fields
.field private final g:Lcom/google/android/exoplayer2/trackselection/e$a;

.field private final h:J

.field private final i:J

.field private final j:J

.field private final k:F

.field private final l:Lcom/google/android/exoplayer2/util/e;

.field private m:F

.field private n:I

.field private o:I

.field private p:J

.field private q:Lcom/google/android/exoplayer2/source/a/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/exoplayer2/source/TrackGroup;[ILcom/google/android/exoplayer2/trackselection/e$a;JJJFLcom/google/android/exoplayer2/util/e;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/trackselection/f;-><init>(Lcom/google/android/exoplayer2/source/TrackGroup;[I)V

    iput-object p3, p0, Lcom/google/android/exoplayer2/trackselection/e;->g:Lcom/google/android/exoplayer2/trackselection/e$a;

    const-wide/16 p1, 0x3e8

    mul-long/2addr p4, p1

    iput-wide p4, p0, Lcom/google/android/exoplayer2/trackselection/e;->h:J

    mul-long/2addr p6, p1

    iput-wide p6, p0, Lcom/google/android/exoplayer2/trackselection/e;->i:J

    mul-long/2addr p8, p1

    iput-wide p8, p0, Lcom/google/android/exoplayer2/trackselection/e;->j:J

    iput p10, p0, Lcom/google/android/exoplayer2/trackselection/e;->k:F

    iput-object p11, p0, Lcom/google/android/exoplayer2/trackselection/e;->l:Lcom/google/android/exoplayer2/util/e;

    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Lcom/google/android/exoplayer2/trackselection/e;->m:F

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/exoplayer2/trackselection/e;->o:I

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lcom/google/android/exoplayer2/trackselection/e;->p:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/source/TrackGroup;[ILcom/google/android/exoplayer2/trackselection/e$a;JJJFLcom/google/android/exoplayer2/util/e;Lcom/google/android/exoplayer2/trackselection/d;)V
    .locals 0

    invoke-direct/range {p0 .. p11}, Lcom/google/android/exoplayer2/trackselection/e;-><init>(Lcom/google/android/exoplayer2/source/TrackGroup;[ILcom/google/android/exoplayer2/trackselection/e$a;JJJFLcom/google/android/exoplayer2/util/e;)V

    return-void
.end method

.method private static a([[D)I
    .locals 4

    array-length v0, p0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v3, p0, v1

    array-length v3, v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return v2
.end method

.method private static a([[[JI[[J[I)V
    .locals 8

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    move-wide v2, v1

    move v1, v0

    :goto_0
    array-length v4, p0

    if-ge v1, v4, :cond_0

    aget-object v4, p0, v1

    aget-object v4, v4, p1

    aget-object v5, p2, v1

    aget v6, p3, v1

    aget-wide v6, v5, v6

    const/4 v5, 0x1

    aput-wide v6, v4, v5

    aget-object v4, p0, v1

    aget-object v4, v4, p1

    aget-wide v5, v4, v5

    add-long/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    array-length p2, p0

    move p3, v0

    :goto_1
    if-ge p3, p2, :cond_1

    aget-object v1, p0, p3

    aget-object v1, v1, p1

    aput-wide v2, v1, v0

    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method static synthetic a([[J)[[[J
    .locals 0

    invoke-static {p0}, Lcom/google/android/exoplayer2/trackselection/e;->c([[J)[[[J

    move-result-object p0

    return-object p0
.end method

.method private static b([[D)[[D
    .locals 14

    array-length v0, p0

    new-array v0, v0, [[D

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_3

    aget-object v3, p0, v2

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    new-array v3, v3, [D

    aput-object v3, v0, v2

    aget-object v3, v0, v2

    array-length v3, v3

    if-nez v3, :cond_0

    goto :goto_3

    :cond_0
    aget-object v3, p0, v2

    aget-object v4, p0, v2

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-wide v4, v3, v4

    aget-object v3, p0, v2

    aget-wide v6, v3, v1

    sub-double/2addr v4, v6

    move v3, v1

    :goto_1
    aget-object v6, p0, v2

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v3, v6, :cond_2

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    aget-object v8, p0, v2

    aget-wide v9, v8, v3

    aget-object v8, p0, v2

    add-int/lit8 v11, v3, 0x1

    aget-wide v12, v8, v11

    add-double/2addr v9, v12

    mul-double/2addr v9, v6

    aget-object v6, v0, v2

    const-wide/16 v7, 0x0

    cmpl-double v7, v4, v7

    if-nez v7, :cond_1

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    goto :goto_2

    :cond_1
    aget-object v7, p0, v2

    aget-wide v12, v7, v1

    sub-double/2addr v9, v12

    div-double v7, v9, v4

    :goto_2
    aput-wide v7, v6, v3

    move v3, v11

    goto :goto_1

    :cond_2
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method private static c([[J)[[[J
    .locals 15

    invoke-static {p0}, Lcom/google/android/exoplayer2/trackselection/e;->d([[J)[[D

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/exoplayer2/trackselection/e;->b([[D)[[D

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/exoplayer2/trackselection/e;->a([[D)I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    array-length v3, v0

    const/4 v4, 0x2

    filled-new-array {v3, v2, v4}, [I

    move-result-object v3

    const-class v5, J

    invoke-static {v5, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[[J

    array-length v5, v0

    new-array v5, v5, [I

    const/4 v6, 0x1

    invoke-static {v3, v6, p0, v5}, Lcom/google/android/exoplayer2/trackselection/e;->a([[[JI[[J[I)V

    move v7, v4

    :goto_0
    add-int/lit8 v8, v2, -0x1

    const/4 v9, 0x0

    if-ge v7, v8, :cond_3

    const-wide v10, 0x7fefffffffffffffL    # Double.MAX_VALUE

    move v8, v9

    :goto_1
    array-length v12, v0

    if-ge v9, v12, :cond_2

    aget v12, v5, v9

    add-int/2addr v12, v6

    aget-object v13, v0, v9

    array-length v13, v13

    if-ne v12, v13, :cond_0

    goto :goto_2

    :cond_0
    aget-object v12, v1, v9

    aget v13, v5, v9

    aget-wide v13, v12, v13

    cmpg-double v12, v13, v10

    if-gez v12, :cond_1

    move v8, v9

    move-wide v10, v13

    :cond_1
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_2
    aget v9, v5, v8

    add-int/2addr v9, v6

    aput v9, v5, v8

    invoke-static {v3, v7, p0, v5}, Lcom/google/android/exoplayer2/trackselection/e;->a([[[JI[[J[I)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_3
    array-length p0, v3

    move v0, v9

    :goto_3
    if-ge v0, p0, :cond_4

    aget-object v1, v3, v0

    aget-object v5, v1, v8

    add-int/lit8 v7, v2, -0x2

    aget-object v10, v1, v7

    aget-wide v11, v10, v9

    const-wide/16 v13, 0x2

    mul-long/2addr v11, v13

    aput-wide v11, v5, v9

    aget-object v5, v1, v8

    aget-object v1, v1, v7

    aget-wide v10, v1, v6

    mul-long/2addr v10, v13

    aput-wide v10, v5, v6

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    return-object v3
.end method

.method private static d([[J)[[D
    .locals 10

    array-length v0, p0

    new-array v0, v0, [[D

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_2

    aget-object v3, p0, v2

    array-length v3, v3

    new-array v3, v3, [D

    aput-object v3, v0, v2

    move v3, v1

    :goto_1
    aget-object v4, p0, v2

    array-length v4, v4

    if-ge v3, v4, :cond_1

    aget-object v4, v0, v2

    aget-object v5, p0, v2

    aget-wide v6, v5, v3

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    const-wide/16 v5, 0x0

    goto :goto_2

    :cond_0
    aget-object v5, p0, v2

    aget-wide v6, v5, v3

    long-to-double v5, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->log(D)D

    move-result-wide v5

    :goto_2
    aput-wide v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method


# virtual methods
.method public a(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/trackselection/e;->m:F

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/trackselection/e;->n:I

    return v0
.end method

.method public b([[J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/trackselection/e;->g:Lcom/google/android/exoplayer2/trackselection/e$a;

    check-cast v0, Lcom/google/android/exoplayer2/trackselection/e$b;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/trackselection/e$b;->a([[J)V

    return-void
.end method

.method public c()V
    .locals 2
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/trackselection/e;->p:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/trackselection/e;->q:Lcom/google/android/exoplayer2/source/a/b;

    return-void
.end method

.method public d()V
    .locals 1
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/trackselection/e;->q:Lcom/google/android/exoplayer2/source/a/b;

    return-void
.end method
