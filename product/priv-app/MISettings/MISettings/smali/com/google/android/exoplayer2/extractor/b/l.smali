.class final Lcom/google/android/exoplayer2/extractor/b/l;
.super Lcom/google/android/exoplayer2/extractor/b/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/b/l$a;
    }
.end annotation


# instance fields
.field private n:Lcom/google/android/exoplayer2/extractor/b/l$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private o:I

.field private p:Z

.field private q:Lcom/google/android/exoplayer2/extractor/x$d;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/google/android/exoplayer2/extractor/x$b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/b/k;-><init>()V

    return-void
.end method

.method static a(BII)I
    .locals 0
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    shr-int/2addr p0, p2

    rsub-int/lit8 p1, p1, 0x8

    const/16 p2, 0xff

    ushr-int p1, p2, p1

    and-int/2addr p0, p1

    return p0
.end method

.method private static a(BLcom/google/android/exoplayer2/extractor/b/l$a;)I
    .locals 2

    iget v0, p1, Lcom/google/android/exoplayer2/extractor/b/l$a;->e:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/exoplayer2/extractor/b/l;->a(BII)I

    move-result p0

    iget-object v0, p1, Lcom/google/android/exoplayer2/extractor/b/l$a;->d:[Lcom/google/android/exoplayer2/extractor/x$c;

    aget-object p0, v0, p0

    iget-boolean p0, p0, Lcom/google/android/exoplayer2/extractor/x$c;->a:Z

    if-nez p0, :cond_0

    iget-object p0, p1, Lcom/google/android/exoplayer2/extractor/b/l$a;->a:Lcom/google/android/exoplayer2/extractor/x$d;

    iget p0, p0, Lcom/google/android/exoplayer2/extractor/x$d;->g:I

    goto :goto_0

    :cond_0
    iget-object p0, p1, Lcom/google/android/exoplayer2/extractor/b/l$a;->a:Lcom/google/android/exoplayer2/extractor/x$d;

    iget p0, p0, Lcom/google/android/exoplayer2/extractor/x$d;->h:I

    :goto_0
    return p0
.end method

.method static a(Lcom/google/android/exoplayer2/util/t;J)V
    .locals 6
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/t;->d(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    const-wide/16 v2, 0xff

    and-long v4, p1, v2

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v1

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    const/16 v4, 0x8

    ushr-long v4, p1, v4

    and-long/2addr v4, v2

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v1

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    const/16 v4, 0x10

    ushr-long v4, p1, v4

    and-long/2addr v4, v2

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v1

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    const/16 v1, 0x18

    ushr-long/2addr p1, v1

    and-long/2addr p1, v2

    long-to-int p1, p1

    int-to-byte p1, p1

    aput-byte p1, v0, p0

    return-void
.end method

.method public static c(Lcom/google/android/exoplayer2/util/t;)Z
    .locals 1

    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0, p0, v0}, Lcom/google/android/exoplayer2/extractor/x;->a(ILcom/google/android/exoplayer2/util/t;Z)Z

    move-result p0
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ea; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method protected a(Lcom/google/android/exoplayer2/util/t;)J
    .locals 5

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    const/4 v2, 0x1

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    const-wide/16 v0, -0x1

    return-wide v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    aget-byte v0, v0, v1

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/b/l;->n:Lcom/google/android/exoplayer2/extractor/b/l$a;

    invoke-static {v0, v3}, Lcom/google/android/exoplayer2/extractor/b/l;->a(BLcom/google/android/exoplayer2/extractor/b/l$a;)I

    move-result v0

    iget-boolean v3, p0, Lcom/google/android/exoplayer2/extractor/b/l;->p:Z

    if-eqz v3, :cond_1

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->o:I

    add-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x4

    :cond_1
    int-to-long v3, v1

    invoke-static {p1, v3, v4}, Lcom/google/android/exoplayer2/extractor/b/l;->a(Lcom/google/android/exoplayer2/util/t;J)V

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/b/l;->p:Z

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/b/l;->o:I

    return-wide v3
.end method

.method protected a(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/exoplayer2/extractor/b/k;->a(Z)V

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->n:Lcom/google/android/exoplayer2/extractor/b/l$a;

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->q:Lcom/google/android/exoplayer2/extractor/x$d;

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->r:Lcom/google/android/exoplayer2/extractor/x$b;

    :cond_0
    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->o:I

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->p:Z

    return-void
.end method

.method protected a(Lcom/google/android/exoplayer2/util/t;JLcom/google/android/exoplayer2/extractor/b/k$a;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/b/l;->n:Lcom/google/android/exoplayer2/extractor/b/l$a;

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/extractor/b/l;->b(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/extractor/b/l$a;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->n:Lcom/google/android/exoplayer2/extractor/b/l$a;

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->n:Lcom/google/android/exoplayer2/extractor/b/l$a;

    const/4 p2, 0x1

    if-nez p1, :cond_1

    return p2

    :cond_1
    iget-object p1, p1, Lcom/google/android/exoplayer2/extractor/b/l$a;->a:Lcom/google/android/exoplayer2/extractor/x$d;

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/google/android/exoplayer2/extractor/x$d;->j:[B

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/l;->n:Lcom/google/android/exoplayer2/extractor/b/l$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/b/l$a;->c:[B

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/exoplayer2/Format$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/Format$a;-><init>()V

    const-string v1, "audio/vorbis"

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/Format$a;->e(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    iget v1, p1, Lcom/google/android/exoplayer2/extractor/x$d;->e:I

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/Format$a;->b(I)Lcom/google/android/exoplayer2/Format$a;

    iget v1, p1, Lcom/google/android/exoplayer2/extractor/x$d;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/Format$a;->j(I)Lcom/google/android/exoplayer2/Format$a;

    iget v1, p1, Lcom/google/android/exoplayer2/extractor/x$d;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/Format$a;->c(I)Lcom/google/android/exoplayer2/Format$a;

    iget p1, p1, Lcom/google/android/exoplayer2/extractor/x$d;->c:I

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/Format$a;->l(I)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v0, p3}, Lcom/google/android/exoplayer2/Format$a;->a(Ljava/util/List;)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Format$a;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object p1

    iput-object p1, p4, Lcom/google/android/exoplayer2/extractor/b/k$a;->a:Lcom/google/android/exoplayer2/Format;

    return p2
.end method

.method b(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/extractor/b/l$a;
    .locals 7
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1c

    nop

    :goto_0
    new-array v4, v0, [B

    goto/32 :goto_1e

    nop

    :goto_1
    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/32 :goto_8

    nop

    :goto_2
    return-object p1

    :goto_3
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_4
    add-int/lit8 p1, p1, -0x1

    goto/32 :goto_15

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_9

    nop

    :goto_6
    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/extractor/x;->a(Lcom/google/android/exoplayer2/util/t;I)[Lcom/google/android/exoplayer2/extractor/x$c;

    move-result-object v5

    goto/32 :goto_d

    nop

    :goto_7
    const/4 v1, 0x0

    goto/32 :goto_e

    nop

    :goto_8
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/l;->q:Lcom/google/android/exoplayer2/extractor/x$d;

    goto/32 :goto_16

    nop

    :goto_9
    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/x;->a(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/extractor/x$b;

    move-result-object p1

    goto/32 :goto_f

    nop

    :goto_a
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v1

    goto/32 :goto_c

    nop

    :goto_b
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/b/l;->q:Lcom/google/android/exoplayer2/extractor/x$d;

    goto/32 :goto_17

    nop

    :goto_c
    const/4 v2, 0x0

    goto/32 :goto_1

    nop

    :goto_d
    array-length p1, v5

    goto/32 :goto_4

    nop

    :goto_e
    if-eqz v0, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_12

    nop

    :goto_f
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->r:Lcom/google/android/exoplayer2/extractor/x$b;

    goto/32 :goto_10

    nop

    :goto_10
    return-object v1

    :goto_11
    goto/32 :goto_3

    nop

    :goto_12
    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/x;->b(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/extractor/x$d;

    move-result-object p1

    goto/32 :goto_1a

    nop

    :goto_13
    return-object v1

    :goto_14
    goto/32 :goto_1b

    nop

    :goto_15
    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/x;->a(I)I

    move-result v6

    goto/32 :goto_1d

    nop

    :goto_16
    iget v0, v0, Lcom/google/android/exoplayer2/extractor/x$d;->b:I

    goto/32 :goto_6

    nop

    :goto_17
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/b/l;->r:Lcom/google/android/exoplayer2/extractor/x$b;

    goto/32 :goto_19

    nop

    :goto_18
    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/extractor/b/l$a;-><init>(Lcom/google/android/exoplayer2/extractor/x$d;Lcom/google/android/exoplayer2/extractor/x$b;[B[Lcom/google/android/exoplayer2/extractor/x$c;I)V

    goto/32 :goto_2

    nop

    :goto_19
    move-object v1, p1

    goto/32 :goto_18

    nop

    :goto_1a
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->q:Lcom/google/android/exoplayer2/extractor/x$d;

    goto/32 :goto_13

    nop

    :goto_1b
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/l;->r:Lcom/google/android/exoplayer2/extractor/x$b;

    goto/32 :goto_5

    nop

    :goto_1c
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/l;->q:Lcom/google/android/exoplayer2/extractor/x$d;

    goto/32 :goto_7

    nop

    :goto_1d
    new-instance p1, Lcom/google/android/exoplayer2/extractor/b/l$a;

    goto/32 :goto_b

    nop

    :goto_1e
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    goto/32 :goto_a

    nop
.end method

.method protected c(J)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer2/extractor/b/k;->c(J)V

    const-wide/16 v0, 0x0

    cmp-long p1, p1, v0

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    move p1, p2

    :goto_0
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->p:Z

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/l;->q:Lcom/google/android/exoplayer2/extractor/x$d;

    if-eqz p1, :cond_1

    iget p2, p1, Lcom/google/android/exoplayer2/extractor/x$d;->g:I

    :cond_1
    iput p2, p0, Lcom/google/android/exoplayer2/extractor/b/l;->o:I

    return-void
.end method
