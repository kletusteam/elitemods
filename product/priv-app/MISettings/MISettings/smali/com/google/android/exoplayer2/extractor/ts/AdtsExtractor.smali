.class public final Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/Extractor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor$Flags;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/exoplayer2/extractor/m;


# instance fields
.field private final b:I

.field private final c:Lcom/google/android/exoplayer2/extractor/ts/j;

.field private final d:Lcom/google/android/exoplayer2/util/t;

.field private final e:Lcom/google/android/exoplayer2/util/t;

.field private final f:Lcom/google/android/exoplayer2/util/s;

.field private g:Lcom/google/android/exoplayer2/extractor/k;

.field private h:J

.field private i:J

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/extractor/ts/c;->a:Lcom/google/android/exoplayer2/extractor/ts/c;

    sput-object v0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->a:Lcom/google/android/exoplayer2/extractor/m;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->b:I

    new-instance p1, Lcom/google/android/exoplayer2/extractor/ts/j;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/extractor/ts/j;-><init>(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->c:Lcom/google/android/exoplayer2/extractor/ts/j;

    new-instance p1, Lcom/google/android/exoplayer2/util/t;

    const/16 v0, 0x800

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->d:Lcom/google/android/exoplayer2/util/t;

    const/4 p1, -0x1

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->j:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->i:J

    new-instance p1, Lcom/google/android/exoplayer2/util/t;

    const/16 v0, 0xa

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    new-instance p1, Lcom/google/android/exoplayer2/util/s;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/util/s;-><init>([B)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->f:Lcom/google/android/exoplayer2/util/s;

    return-void
.end method

.method private static a(IJ)I
    .locals 4

    mul-int/lit8 p0, p0, 0x8

    int-to-long v0, p0

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    div-long/2addr v0, p1

    long-to-int p0, v0

    return p0
.end method

.method private a(J)Lcom/google/android/exoplayer2/extractor/u;
    .locals 10

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->j:I

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->c:Lcom/google/android/exoplayer2/extractor/ts/j;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/extractor/ts/j;->c()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->a(IJ)I

    move-result v8

    new-instance v0, Lcom/google/android/exoplayer2/extractor/e;

    iget-wide v6, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->i:J

    iget v9, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->j:I

    move-object v3, v0

    move-wide v4, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/exoplayer2/extractor/e;-><init>(JJII)V

    return-object v0
.end method

.method private a(JZZ)V
    .locals 5

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->m:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    if-eqz p3, :cond_1

    iget p3, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->j:I

    if-lez p3, :cond_1

    move p3, v0

    goto :goto_0

    :cond_1
    const/4 p3, 0x0

    :goto_0
    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz p3, :cond_2

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->c:Lcom/google/android/exoplayer2/extractor/ts/j;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/extractor/ts/j;->c()J

    move-result-wide v3

    cmp-long v3, v3, v1

    if-nez v3, :cond_2

    if-nez p4, :cond_2

    return-void

    :cond_2
    if-eqz p3, :cond_3

    iget-object p3, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->c:Lcom/google/android/exoplayer2/extractor/ts/j;

    invoke-virtual {p3}, Lcom/google/android/exoplayer2/extractor/ts/j;->c()J

    move-result-wide p3

    cmp-long p3, p3, v1

    if-eqz p3, :cond_3

    iget-object p3, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->g:Lcom/google/android/exoplayer2/extractor/k;

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->a(J)Lcom/google/android/exoplayer2/extractor/u;

    move-result-object p1

    invoke-interface {p3, p1}, Lcom/google/android/exoplayer2/extractor/k;->a(Lcom/google/android/exoplayer2/extractor/u;)V

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->g:Lcom/google/android/exoplayer2/extractor/k;

    new-instance p2, Lcom/google/android/exoplayer2/extractor/u$b;

    invoke-direct {p2, v1, v2}, Lcom/google/android/exoplayer2/extractor/u$b;-><init>(J)V

    invoke-interface {p1, p2}, Lcom/google/android/exoplayer2/extractor/k;->a(Lcom/google/android/exoplayer2/extractor/u;)V

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->m:Z

    return-void
.end method

.method static synthetic a()[Lcom/google/android/exoplayer2/extractor/Extractor;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/exoplayer2/extractor/Extractor;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method private b(Lcom/google/android/exoplayer2/extractor/i;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->k:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->j:I

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->c(Lcom/google/android/exoplayer2/extractor/i;)I

    :cond_1
    const/4 v1, 0x0

    move v2, v1

    :cond_2
    const/4 v5, 0x1

    :try_start_0
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {p1, v6, v1, v7, v5}, Lcom/google/android/exoplayer2/extractor/i;->b([BIIZ)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v6, v1}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/util/t;->B()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/exoplayer2/extractor/ts/j;->a(I)Z

    move-result v6

    if-nez v6, :cond_3

    move v2, v1

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v6

    const/4 v7, 0x4

    invoke-interface {p1, v6, v1, v7, v5}, Lcom/google/android/exoplayer2/extractor/i;->b([BIIZ)Z

    move-result v6

    if-nez v6, :cond_4

    goto :goto_0

    :cond_4
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->f:Lcom/google/android/exoplayer2/util/s;

    const/16 v7, 0xe

    invoke-virtual {v6, v7}, Lcom/google/android/exoplayer2/util/s;->c(I)V

    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->f:Lcom/google/android/exoplayer2/util/s;

    const/16 v7, 0xd

    invoke-virtual {v6, v7}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v6

    const/4 v7, 0x6

    if-le v6, v7, :cond_6

    int-to-long v7, v6

    add-long/2addr v3, v7

    add-int/lit8 v2, v2, 0x1

    const/16 v7, 0x3e8

    if-ne v2, v7, :cond_5

    goto :goto_0

    :cond_5
    add-int/lit8 v6, v6, -0x6

    invoke-interface {p1, v6, v5}, Lcom/google/android/exoplayer2/extractor/i;->a(IZ)Z

    move-result v6

    if-nez v6, :cond_2

    goto :goto_0

    :cond_6
    iput-boolean v5, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->k:Z

    new-instance v1, Lcom/google/android/exoplayer2/ea;

    const-string v6, "Malformed ADTS stream"

    invoke-direct {v1, v6}, Lcom/google/android/exoplayer2/ea;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_7
    :goto_0
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    if-lez v2, :cond_8

    int-to-long v0, v2

    div-long/2addr v3, v0

    long-to-int p1, v3

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->j:I

    goto :goto_1

    :cond_8
    iput v0, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->j:I

    :goto_1
    iput-boolean v5, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->k:Z

    return-void
.end method

.method private c(Lcom/google/android/exoplayer2/extractor/i;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v2

    const/16 v3, 0xa

    invoke-interface {p1, v2, v0, v3}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/t;->y()I

    move-result v2

    const v3, 0x494433

    if-eq v2, v3, :cond_1

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    invoke-interface {p1, v1}, Lcom/google/android/exoplayer2/extractor/i;->a(I)V

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->i:J

    const-wide/16 v4, -0x1

    cmp-long p1, v2, v4

    if-nez p1, :cond_0

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->i:J

    :cond_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/t;->u()I

    move-result v2

    add-int/lit8 v3, v2, 0xa

    add-int/2addr v1, v3

    invoke-interface {p1, v2}, Lcom/google/android/exoplayer2/extractor/i;->a(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->g:Lcom/google/android/exoplayer2/extractor/k;

    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getLength()J

    move-result-wide v0

    iget p2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->b:I

    const/4 v2, 0x1

    and-int/2addr p2, v2

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    const-wide/16 v4, -0x1

    cmp-long p2, v0, v4

    if-eqz p2, :cond_0

    move p2, v2

    goto :goto_0

    :cond_0
    move p2, v3

    :goto_0
    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->b(Lcom/google/android/exoplayer2/extractor/i;)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    const/16 v5, 0x800

    invoke-interface {p1, v4, v3, v5}, Lcom/google/android/exoplayer2/extractor/i;->read([BII)I

    move-result p1

    const/4 v4, -0x1

    if-ne p1, v4, :cond_2

    move v5, v2

    goto :goto_1

    :cond_2
    move v5, v3

    :goto_1
    invoke-direct {p0, v0, v1, p2, v5}, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->a(JZZ)V

    if-eqz v5, :cond_3

    return v4

    :cond_3
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, v3}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, p1}, Lcom/google/android/exoplayer2/util/t;->d(I)V

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->l:Z

    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->c:Lcom/google/android/exoplayer2/extractor/ts/j;

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->h:J

    const/4 p2, 0x4

    invoke-virtual {p1, v0, v1, p2}, Lcom/google/android/exoplayer2/extractor/ts/j;->a(JI)V

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->l:Z

    :cond_4
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->c:Lcom/google/android/exoplayer2/extractor/ts/j;

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->d:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/extractor/ts/j;->a(Lcom/google/android/exoplayer2/util/t;)V

    return v3
.end method

.method public a(JJ)V
    .locals 0

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->l:Z

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->c:Lcom/google/android/exoplayer2/extractor/ts/j;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/extractor/ts/j;->a()V

    iput-wide p3, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->h:J

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/k;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->g:Lcom/google/android/exoplayer2/extractor/k;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->c:Lcom/google/android/exoplayer2/extractor/ts/j;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/exoplayer2/extractor/ts/j;->a(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;)V

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/k;->g()V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->c(Lcom/google/android/exoplayer2/extractor/i;)I

    move-result v0

    const/4 v1, 0x0

    move v3, v0

    :goto_0
    move v2, v1

    move v4, v2

    :goto_1
    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {p1, v5, v1, v6}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v5, v1}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/t;->B()I

    move-result v5

    invoke-static {v5}, Lcom/google/android/exoplayer2/extractor/ts/j;->a(I)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    add-int/lit8 v3, v3, 0x1

    sub-int v2, v3, v0

    const/16 v4, 0x2000

    if-lt v2, v4, :cond_0

    return v1

    :cond_0
    invoke-interface {p1, v3}, Lcom/google/android/exoplayer2/extractor/i;->a(I)V

    goto :goto_0

    :cond_1
    const/4 v5, 0x1

    add-int/2addr v2, v5

    const/4 v6, 0x4

    if-lt v2, v6, :cond_2

    const/16 v7, 0xbc

    if-le v4, v7, :cond_2

    return v5

    :cond_2
    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->e:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v5

    invoke-interface {p1, v5, v1, v6}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->f:Lcom/google/android/exoplayer2/util/s;

    const/16 v6, 0xe

    invoke-virtual {v5, v6}, Lcom/google/android/exoplayer2/util/s;->c(I)V

    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/ts/AdtsExtractor;->f:Lcom/google/android/exoplayer2/util/s;

    const/16 v6, 0xd

    invoke-virtual {v5, v6}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v5

    const/4 v6, 0x6

    if-gt v5, v6, :cond_3

    return v1

    :cond_3
    add-int/lit8 v6, v5, -0x6

    invoke-interface {p1, v6}, Lcom/google/android/exoplayer2/extractor/i;->a(I)V

    add-int/2addr v4, v5

    goto :goto_1
.end method

.method public release()V
    .locals 0

    return-void
.end method
