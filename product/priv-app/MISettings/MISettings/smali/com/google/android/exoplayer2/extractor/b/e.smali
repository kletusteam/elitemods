.class public Lcom/google/android/exoplayer2/extractor/b/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/Extractor;


# static fields
.field public static final a:Lcom/google/android/exoplayer2/extractor/m;


# instance fields
.field private b:Lcom/google/android/exoplayer2/extractor/k;

.field private c:Lcom/google/android/exoplayer2/extractor/b/k;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/extractor/b/a;->a:Lcom/google/android/exoplayer2/extractor/b/a;

    sput-object v0, Lcom/google/android/exoplayer2/extractor/b/e;->a:Lcom/google/android/exoplayer2/extractor/m;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/util/t;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    return-object p0
.end method

.method static synthetic a()[Lcom/google/android/exoplayer2/extractor/Extractor;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/exoplayer2/extractor/Extractor;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/b/e;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/extractor/b/e;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method private b(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/extractor/b/g;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/b/g;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;Z)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/b/g;->b:I

    const/4 v4, 0x2

    and-int/2addr v2, v4

    if-eq v2, v4, :cond_0

    goto :goto_1

    :cond_0
    iget v0, v0, Lcom/google/android/exoplayer2/extractor/b/g;->i:I

    const/16 v2, 0x8

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-instance v2, Lcom/google/android/exoplayer2/util/t;

    invoke-direct {v2, v0}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    invoke-interface {p1, v4, v3, v0}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/b/e;->a(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/util/t;

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/b/d;->b(Lcom/google/android/exoplayer2/util/t;)Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/google/android/exoplayer2/extractor/b/d;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/extractor/b/d;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/e;->c:Lcom/google/android/exoplayer2/extractor/b/k;

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/b/e;->a(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/util/t;

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/b/l;->c(Lcom/google/android/exoplayer2/util/t;)Z

    move-result p1

    if-eqz p1, :cond_2

    new-instance p1, Lcom/google/android/exoplayer2/extractor/b/l;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/extractor/b/l;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/e;->c:Lcom/google/android/exoplayer2/extractor/b/k;

    goto :goto_0

    :cond_2
    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/b/e;->a(Lcom/google/android/exoplayer2/util/t;)Lcom/google/android/exoplayer2/util/t;

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/b/i;->b(Lcom/google/android/exoplayer2/util/t;)Z

    move-result p1

    if-eqz p1, :cond_3

    new-instance p1, Lcom/google/android/exoplayer2/extractor/b/i;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/extractor/b/i;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/e;->c:Lcom/google/android/exoplayer2/extractor/b/k;

    :goto_0
    return v1

    :cond_3
    :goto_1
    return v3
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/e;->b:Lcom/google/android/exoplayer2/extractor/k;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/e;->c:Lcom/google/android/exoplayer2/extractor/b/k;

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/b/e;->b(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/google/android/exoplayer2/ea;

    const-string p2, "Failed to determine bitstream type"

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ea;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/b/e;->d:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/e;->b:Lcom/google/android/exoplayer2/extractor/k;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/k;->a(II)Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/b/e;->b:Lcom/google/android/exoplayer2/extractor/k;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/extractor/k;->g()V

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/b/e;->c:Lcom/google/android/exoplayer2/extractor/b/k;

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/b/e;->b:Lcom/google/android/exoplayer2/extractor/k;

    invoke-virtual {v1, v3, v0}, Lcom/google/android/exoplayer2/extractor/b/k;->a(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/TrackOutput;)V

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/b/e;->d:Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/e;->c:Lcom/google/android/exoplayer2/extractor/b/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/b/k;->a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I

    move-result p1

    return p1
.end method

.method public a(JJ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/e;->c:Lcom/google/android/exoplayer2/extractor/b/k;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/extractor/b/k;->a(JJ)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/k;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/e;->b:Lcom/google/android/exoplayer2/extractor/k;

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/b/e;->b(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result p1
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ea; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    const/4 p1, 0x0

    return p1
.end method

.method public release()V
    .locals 0

    return-void
.end method
