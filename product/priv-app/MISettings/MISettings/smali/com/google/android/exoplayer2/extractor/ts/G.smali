.class final Lcom/google/android/exoplayer2/extractor/ts/G;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/android/exoplayer2/util/C;

.field private final b:Lcom/google/android/exoplayer2/util/t;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:J

.field private g:J

.field private h:J


# direct methods
.method constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/exoplayer2/util/C;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/C;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->a:Lcom/google/android/exoplayer2/util/C;

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->f:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->g:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->h:J

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->b:Lcom/google/android/exoplayer2/util/t;

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/i;)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->b:Lcom/google/android/exoplayer2/util/t;

    sget-object v1, Lcom/google/android/exoplayer2/util/E;->f:[B

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/t;->a([B)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->c:Z

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    const/4 p1, 0x0

    return p1
.end method

.method private a(Lcom/google/android/exoplayer2/util/t;I)J
    .locals 6

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v1

    :goto_0
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    if-ge v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    aget-byte v4, v4, v0

    const/16 v5, 0x47

    if-eq v4, v5, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {p1, v0, p2}, Lcom/google/android/exoplayer2/extractor/ts/H;->a(Lcom/google/android/exoplayer2/util/t;II)J

    move-result-wide v4

    cmp-long v2, v4, v2

    if-eqz v2, :cond_1

    return-wide v4

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-wide v2
.end method

.method private b(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;I)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getLength()J

    move-result-wide v0

    const-wide/32 v2, 0x1b8a0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v1

    const/4 v3, 0x0

    int-to-long v4, v3

    cmp-long v1, v1, v4

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iput-wide v4, p2, Lcom/google/android/exoplayer2/extractor/t;->a:J

    return v2

    :cond_0
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, v0}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p2

    invoke-interface {p1, p2, v3, v0}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-direct {p0, p1, p3}, Lcom/google/android/exoplayer2/extractor/ts/G;->a(Lcom/google/android/exoplayer2/util/t;I)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->f:J

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->d:Z

    return v3
.end method

.method private b(Lcom/google/android/exoplayer2/util/t;I)J
    .locals 6

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    if-lt v1, v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    aget-byte v4, v4, v1

    const/16 v5, 0x47

    if-eq v4, v5, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {p1, v1, p2}, Lcom/google/android/exoplayer2/extractor/ts/H;->a(Lcom/google/android/exoplayer2/util/t;II)J

    move-result-wide v4

    cmp-long v2, v4, v2

    if-eqz v2, :cond_1

    return-wide v4

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    return-wide v2
.end method

.method private c(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;I)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getLength()J

    move-result-wide v0

    const-wide/32 v2, 0x1b8a0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-long v3, v2

    sub-long/2addr v0, v3

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v3

    cmp-long v3, v3, v0

    const/4 v4, 0x1

    if-eqz v3, :cond_0

    iput-wide v0, p2, Lcom/google/android/exoplayer2/extractor/t;->a:J

    return v4

    :cond_0
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2, v2}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p2

    const/4 v0, 0x0

    invoke-interface {p1, p2, v0, v2}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-direct {p0, p1, p3}, Lcom/google/android/exoplayer2/extractor/ts/G;->b(Lcom/google/android/exoplayer2/util/t;I)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->g:J

    iput-boolean v4, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->e:Z

    return v0
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;I)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-gtz p3, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/G;->a(Lcom/google/android/exoplayer2/extractor/i;)I

    move-result p1

    return p1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->e:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/extractor/ts/G;->c(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;I)I

    move-result p1

    return p1

    :cond_1
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->g:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/G;->a(Lcom/google/android/exoplayer2/extractor/i;)I

    move-result p1

    return p1

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->d:Z

    if-nez v0, :cond_3

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/extractor/ts/G;->b(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;I)I

    move-result p1

    return p1

    :cond_3
    iget-wide p2, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->f:J

    cmp-long v0, p2, v2

    if-nez v0, :cond_4

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/G;->a(Lcom/google/android/exoplayer2/extractor/i;)I

    move-result p1

    return p1

    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->a:Lcom/google/android/exoplayer2/util/C;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/exoplayer2/util/C;->b(J)J

    move-result-wide p2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->a:Lcom/google/android/exoplayer2/util/C;

    iget-wide v1, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->g:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/util/C;->b(J)J

    move-result-wide v0

    sub-long/2addr v0, p2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->h:J

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/G;->a(Lcom/google/android/exoplayer2/extractor/i;)I

    move-result p1

    return p1
.end method

.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->h:J

    return-wide v0
.end method

.method public b()Lcom/google/android/exoplayer2/util/C;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->a:Lcom/google/android/exoplayer2/util/C;

    return-object v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/G;->c:Z

    return v0
.end method
