.class final Lcom/google/android/exoplayer2/text/a/d$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/text/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field public final a:Lcom/google/android/exoplayer2/text/Cue;

.field public final b:I


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;FIIFIFZII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/exoplayer2/text/Cue$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/text/Cue$a;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/text/Cue$a;->a(Ljava/lang/CharSequence;)Lcom/google/android/exoplayer2/text/Cue$a;

    invoke-virtual {v0, p2}, Lcom/google/android/exoplayer2/text/Cue$a;->a(Landroid/text/Layout$Alignment;)Lcom/google/android/exoplayer2/text/Cue$a;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/exoplayer2/text/Cue$a;->a(FI)Lcom/google/android/exoplayer2/text/Cue$a;

    invoke-virtual {v0, p5}, Lcom/google/android/exoplayer2/text/Cue$a;->a(I)Lcom/google/android/exoplayer2/text/Cue$a;

    invoke-virtual {v0, p6}, Lcom/google/android/exoplayer2/text/Cue$a;->b(F)Lcom/google/android/exoplayer2/text/Cue$a;

    invoke-virtual {v0, p7}, Lcom/google/android/exoplayer2/text/Cue$a;->b(I)Lcom/google/android/exoplayer2/text/Cue$a;

    invoke-virtual {v0, p8}, Lcom/google/android/exoplayer2/text/Cue$a;->c(F)Lcom/google/android/exoplayer2/text/Cue$a;

    if-eqz p9, :cond_0

    invoke-virtual {v0, p10}, Lcom/google/android/exoplayer2/text/Cue$a;->d(I)Lcom/google/android/exoplayer2/text/Cue$a;

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/text/Cue$a;->a()Lcom/google/android/exoplayer2/text/Cue;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/text/a/d$a;->a:Lcom/google/android/exoplayer2/text/Cue;

    iput p11, p0, Lcom/google/android/exoplayer2/text/a/d$a;->b:I

    return-void
.end method
