.class public Lcom/google/android/exoplayer2/source/H;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/TrackOutput;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/H$a;,
        Lcom/google/android/exoplayer2/source/H$b;
    }
.end annotation


# instance fields
.field private A:Z

.field private B:Lcom/google/android/exoplayer2/Format;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/google/android/exoplayer2/Format;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/google/android/exoplayer2/Format;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private E:I

.field private F:Z

.field private G:Z

.field private H:J

.field private I:Z

.field private final a:Lcom/google/android/exoplayer2/source/G;

.field private final b:Lcom/google/android/exoplayer2/source/H$a;

.field private final c:Landroid/os/Looper;

.field private final d:Lcom/google/android/exoplayer2/drm/e;

.field private final e:Lcom/google/android/exoplayer2/drm/c$a;

.field private f:Lcom/google/android/exoplayer2/source/H$b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/google/android/exoplayer2/Format;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/google/android/exoplayer2/drm/DrmSession;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private i:I

.field private j:[I

.field private k:[J

.field private l:[I

.field private m:[I

.field private n:[J

.field private o:[Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

.field private p:[Lcom/google/android/exoplayer2/Format;

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:J

.field private v:J

.field private w:J

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/upstream/e;Landroid/os/Looper;Lcom/google/android/exoplayer2/drm/e;Lcom/google/android/exoplayer2/drm/c$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/H;->c:Landroid/os/Looper;

    iput-object p3, p0, Lcom/google/android/exoplayer2/source/H;->d:Lcom/google/android/exoplayer2/drm/e;

    iput-object p4, p0, Lcom/google/android/exoplayer2/source/H;->e:Lcom/google/android/exoplayer2/drm/c$a;

    new-instance p2, Lcom/google/android/exoplayer2/source/G;

    invoke-direct {p2, p1}, Lcom/google/android/exoplayer2/source/G;-><init>(Lcom/google/android/exoplayer2/upstream/e;)V

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/H;->a:Lcom/google/android/exoplayer2/source/G;

    new-instance p1, Lcom/google/android/exoplayer2/source/H$a;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/source/H$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/H;->b:Lcom/google/android/exoplayer2/source/H$a;

    const/16 p1, 0x3e8

    iput p1, p0, Lcom/google/android/exoplayer2/source/H;->i:I

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->i:I

    new-array p2, p1, [I

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/H;->j:[I

    new-array p2, p1, [J

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/H;->k:[J

    new-array p2, p1, [J

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    new-array p2, p1, [I

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/H;->m:[I

    new-array p2, p1, [I

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/H;->l:[I

    new-array p2, p1, [Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/H;->o:[Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

    new-array p1, p1, [Lcom/google/android/exoplayer2/Format;

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/H;->p:[Lcom/google/android/exoplayer2/Format;

    const-wide/high16 p1, -0x8000000000000000L

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/H;->u:J

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/H;->v:J

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/H;->w:J

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/H;->z:Z

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/H;->y:Z

    return-void
.end method

.method private a(IIJZ)I
    .locals 6

    const/4 v0, 0x0

    const/4 v1, -0x1

    move v2, v1

    move v1, p1

    move p1, v0

    :goto_0
    if-ge p1, p2, :cond_3

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    aget-wide v4, v3, v1

    cmp-long v3, v4, p3

    if-gtz v3, :cond_3

    if-eqz p5, :cond_0

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->m:[I

    aget v3, v3, v1

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    :cond_0
    move v2, p1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    iget v3, p0, Lcom/google/android/exoplayer2/source/H;->i:I

    if-ne v1, v3, :cond_2

    move v1, v0

    :cond_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_3
    return v2
.end method

.method private declared-synchronized a(Lcom/google/android/exoplayer2/S;Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;ZZLcom/google/android/exoplayer2/source/H$a;)I
    .locals 6

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p2, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->c:Z

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/H;->m()Z

    move-result v0

    const/4 v1, -0x5

    const/4 v2, -0x3

    const/4 v3, -0x4

    if-nez v0, :cond_4

    if-nez p4, :cond_3

    iget-boolean p4, p0, Lcom/google/android/exoplayer2/source/H;->x:Z

    if-eqz p4, :cond_0

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    if-eqz p2, :cond_2

    if-nez p3, :cond_1

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    iget-object p3, p0, Lcom/google/android/exoplayer2/source/H;->g:Lcom/google/android/exoplayer2/Format;

    if-eq p2, p3, :cond_2

    :cond_1
    iget-object p2, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    invoke-static {p2}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p2, Lcom/google/android/exoplayer2/Format;

    invoke-direct {p0, p2, p1}, Lcom/google/android/exoplayer2/source/H;->a(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/S;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v1

    :cond_2
    monitor-exit p0

    return v2

    :cond_3
    :goto_0
    const/4 p1, 0x4

    :try_start_1
    invoke-virtual {p2, p1}, Lcom/google/android/exoplayer2/decoder/a;->setFlags(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v3

    :cond_4
    :try_start_2
    iget p4, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    invoke-direct {p0, p4}, Lcom/google/android/exoplayer2/source/H;->e(I)I

    move-result p4

    if-nez p3, :cond_9

    iget-object p3, p0, Lcom/google/android/exoplayer2/source/H;->p:[Lcom/google/android/exoplayer2/Format;

    aget-object p3, p3, p4

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->g:Lcom/google/android/exoplayer2/Format;

    if-eq p3, v0, :cond_5

    goto :goto_1

    :cond_5
    invoke-direct {p0, p4}, Lcom/google/android/exoplayer2/source/H;->f(I)Z

    move-result p1

    const/4 p3, 0x1

    if-nez p1, :cond_6

    iput-boolean p3, p2, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->c:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return v2

    :cond_6
    :try_start_3
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->m:[I

    aget p1, p1, p4

    invoke-virtual {p2, p1}, Lcom/google/android/exoplayer2/decoder/a;->setFlags(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    aget-wide v0, p1, p4

    iput-wide v0, p2, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->d:J

    iget-wide v0, p2, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->d:J

    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/H;->u:J

    cmp-long p1, v0, v4

    if-gez p1, :cond_7

    const/high16 p1, -0x80000000

    invoke-virtual {p2, p1}, Lcom/google/android/exoplayer2/decoder/a;->addFlag(I)V

    :cond_7
    invoke-virtual {p2}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->d()Z

    move-result p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz p1, :cond_8

    monitor-exit p0

    return v3

    :cond_8
    :try_start_4
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->l:[I

    aget p1, p1, p4

    iput p1, p5, Lcom/google/android/exoplayer2/source/H$a;->a:I

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->k:[J

    aget-wide v0, p1, p4

    iput-wide v0, p5, Lcom/google/android/exoplayer2/source/H$a;->b:J

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->o:[Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

    aget-object p1, p1, p4

    iput-object p1, p5, Lcom/google/android/exoplayer2/source/H$a;->c:Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/google/android/exoplayer2/source/H;->t:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return v3

    :cond_9
    :goto_1
    :try_start_5
    iget-object p2, p0, Lcom/google/android/exoplayer2/source/H;->p:[Lcom/google/android/exoplayer2/Format;

    aget-object p2, p2, p4

    invoke-direct {p0, p2, p1}, Lcom/google/android/exoplayer2/source/H;->a(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/S;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized a(JIJILcom/google/android/exoplayer2/extractor/TrackOutput$a;)V
    .locals 8
    .param p7    # Lcom/google/android/exoplayer2/extractor/TrackOutput$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/H;->e(I)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->k:[J

    aget-wide v4, v3, v0

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->l:[I

    aget v0, v3, v0

    int-to-long v6, v0

    add-long/2addr v4, v6

    cmp-long v0, v4, p4

    if-gtz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    :cond_1
    const/high16 v0, 0x20000000

    and-int/2addr v0, p3

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/H;->x:Z

    iget-wide v3, p0, Lcom/google/android/exoplayer2/source/H;->w:J

    invoke-static {v3, v4, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/exoplayer2/source/H;->w:J

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/H;->e(I)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    aput-wide p1, v3, v0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->k:[J

    aput-wide p4, p1, v0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->l:[I

    aput p6, p1, v0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->m:[I

    aput p3, p1, v0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->o:[Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

    aput-object p7, p1, v0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->p:[Lcom/google/android/exoplayer2/Format;

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    aput-object p2, p1, v0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->j:[I

    iget p2, p0, Lcom/google/android/exoplayer2/source/H;->E:I

    aput p2, p1, v0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/H;->D:Lcom/google/android/exoplayer2/Format;

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    add-int/2addr p1, v1

    iput p1, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    iget p2, p0, Lcom/google/android/exoplayer2/source/H;->i:I

    if-ne p1, p2, :cond_3

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->i:I

    add-int/lit16 p1, p1, 0x3e8

    new-array p2, p1, [I

    new-array p3, p1, [J

    new-array p4, p1, [J

    new-array p5, p1, [I

    new-array p6, p1, [I

    new-array p7, p1, [Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

    new-array v0, p1, [Lcom/google/android/exoplayer2/Format;

    iget v1, p0, Lcom/google/android/exoplayer2/source/H;->i:I

    iget v3, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    sub-int/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->k:[J

    iget v4, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    invoke-static {v3, v4, p3, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    iget v4, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    invoke-static {v3, v4, p4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->m:[I

    iget v4, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    invoke-static {v3, v4, p5, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->l:[I

    iget v4, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    invoke-static {v3, v4, p6, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->o:[Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

    iget v4, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    invoke-static {v3, v4, p7, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->p:[Lcom/google/android/exoplayer2/Format;

    iget v4, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    invoke-static {v3, v4, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->j:[I

    iget v4, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    invoke-static {v3, v4, p2, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v3, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/H;->k:[J

    invoke-static {v4, v2, p3, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    invoke-static {v4, v2, p4, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/H;->m:[I

    invoke-static {v4, v2, p5, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/H;->l:[I

    invoke-static {v4, v2, p6, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/H;->o:[Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

    invoke-static {v4, v2, p7, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/H;->p:[Lcom/google/android/exoplayer2/Format;

    invoke-static {v4, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/H;->j:[I

    invoke-static {v4, v2, p2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p3, p0, Lcom/google/android/exoplayer2/source/H;->k:[J

    iput-object p4, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    iput-object p5, p0, Lcom/google/android/exoplayer2/source/H;->m:[I

    iput-object p6, p0, Lcom/google/android/exoplayer2/source/H;->l:[I

    iput-object p7, p0, Lcom/google/android/exoplayer2/source/H;->o:[Lcom/google/android/exoplayer2/extractor/TrackOutput$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/H;->p:[Lcom/google/android/exoplayer2/Format;

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/H;->j:[I

    iput v2, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    iput p1, p0, Lcom/google/android/exoplayer2/source/H;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private a(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/S;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->g:Lcom/google/android/exoplayer2/Format;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/H;->g:Lcom/google/android/exoplayer2/Format;

    iget-object v1, v1, Lcom/google/android/exoplayer2/Format;->o:Lcom/google/android/exoplayer2/drm/DrmInitData;

    :goto_1
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/H;->g:Lcom/google/android/exoplayer2/Format;

    iget-object v2, p1, Lcom/google/android/exoplayer2/Format;->o:Lcom/google/android/exoplayer2/drm/DrmInitData;

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->d:Lcom/google/android/exoplayer2/drm/e;

    invoke-interface {v3, p1}, Lcom/google/android/exoplayer2/drm/e;->a(Lcom/google/android/exoplayer2/Format;)Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/Format;->a(Ljava/lang/Class;)Lcom/google/android/exoplayer2/Format;

    move-result-object v3

    iput-object v3, p2, Lcom/google/android/exoplayer2/S;->b:Lcom/google/android/exoplayer2/Format;

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->h:Lcom/google/android/exoplayer2/drm/DrmSession;

    iput-object v3, p2, Lcom/google/android/exoplayer2/S;->a:Lcom/google/android/exoplayer2/drm/DrmSession;

    if-nez v0, :cond_2

    invoke-static {v1, v2}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->h:Lcom/google/android/exoplayer2/drm/DrmSession;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/H;->d:Lcom/google/android/exoplayer2/drm/e;

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/H;->c:Landroid/os/Looper;

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/H;->e:Lcom/google/android/exoplayer2/drm/c$a;

    invoke-interface {v1, v2, v3, p1}, Lcom/google/android/exoplayer2/drm/e;->a(Landroid/os/Looper;Lcom/google/android/exoplayer2/drm/c$a;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/drm/DrmSession;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/H;->h:Lcom/google/android/exoplayer2/drm/DrmSession;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->h:Lcom/google/android/exoplayer2/drm/DrmSession;

    iput-object p1, p2, Lcom/google/android/exoplayer2/S;->a:Lcom/google/android/exoplayer2/drm/DrmSession;

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->e:Lcom/google/android/exoplayer2/drm/c$a;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/drm/DrmSession;->b(Lcom/google/android/exoplayer2/drm/c$a;)V

    :cond_3
    return-void
.end method

.method private b(I)J
    .locals 5

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/H;->v:J

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/H;->d(I)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/H;->v:J

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->r:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/exoplayer2/source/H;->r:I

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    iget v1, p0, Lcom/google/android/exoplayer2/source/H;->i:I

    if-lt v0, v1, :cond_0

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    if-gez p1, :cond_1

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    :cond_1
    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    if-nez p1, :cond_3

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    if-nez p1, :cond_2

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->i:I

    :cond_2
    add-int/lit8 p1, p1, -0x1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->k:[J

    aget-wide v1, v0, p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->l:[I

    aget p1, v0, p1

    int-to-long v3, p1

    add-long/2addr v1, v3

    return-wide v1

    :cond_3
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->k:[J

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    aget-wide v0, p1, v0

    return-wide v0
.end method

.method private declared-synchronized b(JZZ)J
    .locals 9

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    const-wide/16 v1, -0x1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    iget v3, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    aget-wide v3, v0, v3

    cmp-long v0, p1, v3

    if-gez v0, :cond_0

    goto :goto_1

    :cond_0
    if-eqz p4, :cond_1

    iget p4, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    if-eq p4, v0, :cond_1

    iget p4, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    :cond_1
    iget p4, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    :goto_0
    move v5, p4

    iget v4, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    move-object v3, p0

    move-wide v6, p1

    move v8, p3

    invoke-direct/range {v3 .. v8}, Lcom/google/android/exoplayer2/source/H;->a(IIJZ)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p2, -0x1

    if-ne p1, p2, :cond_2

    monitor-exit p0

    return-wide v1

    :cond_2
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/H;->b(I)J

    move-result-wide p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-wide p1

    :cond_3
    :goto_1
    monitor-exit p0

    return-wide v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized b(J)Z
    .locals 5

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    iget-wide v3, p0, Lcom/google/android/exoplayer2/source/H;->v:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long p1, p1, v3

    if-lez p1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    monitor-exit p0

    return v1

    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/H;->c()J

    move-result-wide v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v3, p1

    if-ltz v0, :cond_2

    monitor-exit p0

    return v2

    :cond_2
    :try_start_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/H;->c(J)I

    move-result p1

    iget p2, p0, Lcom/google/android/exoplayer2/source/H;->r:I

    add-int/2addr p2, p1

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/source/H;->c(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private c(J)I
    .locals 5

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/source/H;->e(I)I

    move-result v1

    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    if-le v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    aget-wide v3, v2, v1

    cmp-long v2, v3, p1

    if-ltz v2, :cond_1

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/exoplayer2/source/H;->i:I

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private c(I)J
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/H;->f()I

    move-result v0

    sub-int/2addr v0, p1

    const/4 p1, 0x0

    const/4 v1, 0x1

    if-ltz v0, :cond_0

    iget v2, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    iget v3, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    sub-int/2addr v2, v3

    if-gt v0, v2, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    iget v2, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    sub-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/H;->v:J

    iget v4, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    invoke-direct {p0, v4}, Lcom/google/android/exoplayer2/source/H;->d(I)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/exoplayer2/source/H;->w:J

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/H;->x:Z

    if-eqz v0, :cond_1

    move p1, v1

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/H;->x:Z

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    if-eqz p1, :cond_2

    sub-int/2addr p1, v1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/H;->e(I)I

    move-result p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->k:[J

    aget-wide v1, v0, p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->l:[I

    aget p1, v0, p1

    int-to-long v3, p1

    add-long/2addr v1, v3

    return-wide v1

    :cond_2
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method private declared-synchronized c(Lcom/google/android/exoplayer2/Format;)Z
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/H;->z:Z

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    invoke-static {p1, v1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/H;->D:Lcom/google/android/exoplayer2/Format;

    invoke-static {p1, v1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->D:Lcom/google/android/exoplayer2/Format;

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    :goto_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    iget-object p1, p1, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    iget-object v1, v1, Lcom/google/android/exoplayer2/Format;->i:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/android/exoplayer2/util/q;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/H;->F:Z

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/H;->G:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 p1, 0x1

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private d(I)J
    .locals 7

    const-wide/high16 v0, -0x8000000000000000L

    if-nez p1, :cond_0

    return-wide v0

    :cond_0
    add-int/lit8 v2, p1, -0x1

    invoke-direct {p0, v2}, Lcom/google/android/exoplayer2/source/H;->e(I)I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, p1, :cond_3

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    aget-wide v5, v4, v2

    invoke-static {v0, v1, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/H;->m:[I

    aget v4, v4, v2

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, -0x1

    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    iget v2, p0, Lcom/google/android/exoplayer2/source/H;->i:I

    add-int/lit8 v2, v2, -0x1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    return-wide v0
.end method

.method private e(I)I
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    add-int/2addr v0, p1

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->i:I

    if-ge v0, p1, :cond_0

    goto :goto_0

    :cond_0
    sub-int/2addr v0, p1

    :goto_0
    return v0
.end method

.method private f(I)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->h:Lcom/google/android/exoplayer2/drm/DrmSession;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/exoplayer2/drm/DrmSession;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->m:[I

    aget p1, v0, p1

    const/high16 v0, 0x40000000    # 2.0f

    and-int/2addr p1, v0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->h:Lcom/google/android/exoplayer2/drm/DrmSession;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/drm/DrmSession;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private declared-synchronized l()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/H;->b(I)J

    move-result-wide v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private m()Z
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    iget v1, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private n()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->h:Lcom/google/android/exoplayer2/drm/DrmSession;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/H;->e:Lcom/google/android/exoplayer2/drm/c$a;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/drm/DrmSession;->b(Lcom/google/android/exoplayer2/drm/c$a;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/H;->h:Lcom/google/android/exoplayer2/drm/DrmSession;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/H;->g:Lcom/google/android/exoplayer2/Format;

    :cond_0
    return-void
.end method

.method private declared-synchronized o()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->a:Lcom/google/android/exoplayer2/source/G;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/G;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(JZ)I
    .locals 8

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/H;->e(I)I

    move-result v2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/H;->m()Z

    move-result v0

    const/4 v7, 0x0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    aget-wide v3, v0, v2

    cmp-long v0, p1, v3

    if-gez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/H;->w:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    if-eqz p3, :cond_1

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    iget p2, p0, Lcom/google/android/exoplayer2/source/H;->t:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-int/2addr p1, p2

    monitor-exit p0

    return p1

    :cond_1
    :try_start_1
    iget p3, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    sub-int v3, p3, v0

    const/4 v6, 0x1

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/source/H;->a(IIJZ)I

    move-result p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 p2, -0x1

    if-ne p1, p2, :cond_2

    monitor-exit p0

    return v7

    :cond_2
    monitor-exit p0

    return p1

    :cond_3
    :goto_0
    monitor-exit p0

    return v7

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public a(Lcom/google/android/exoplayer2/S;Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;ZZ)I
    .locals 6
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    iget-object v5, p0, Lcom/google/android/exoplayer2/source/H;->b:Lcom/google/android/exoplayer2/source/H$a;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/H;->a(Lcom/google/android/exoplayer2/S;Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;ZZLcom/google/android/exoplayer2/source/H$a;)I

    move-result p1

    const/4 p3, -0x4

    if-ne p1, p3, :cond_0

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/decoder/a;->isEndOfStream()Z

    move-result p3

    if-nez p3, :cond_0

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->d()Z

    move-result p3

    if-nez p3, :cond_0

    iget-object p3, p0, Lcom/google/android/exoplayer2/source/H;->a:Lcom/google/android/exoplayer2/source/G;

    iget-object p4, p0, Lcom/google/android/exoplayer2/source/H;->b:Lcom/google/android/exoplayer2/source/H$a;

    invoke-virtual {p3, p2, p4}, Lcom/google/android/exoplayer2/source/G;->a(Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;Lcom/google/android/exoplayer2/source/H$a;)V

    :cond_0
    return p1
.end method

.method public final a(Lcom/google/android/exoplayer2/upstream/j;IZI)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p4, p0, Lcom/google/android/exoplayer2/source/H;->a:Lcom/google/android/exoplayer2/source/G;

    invoke-virtual {p4, p1, p2, p3}, Lcom/google/android/exoplayer2/source/G;->a(Lcom/google/android/exoplayer2/upstream/j;IZ)I

    move-result p1

    return p1
.end method

.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->a:Lcom/google/android/exoplayer2/source/G;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/H;->l()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/source/G;->a(J)V

    return-void
.end method

.method public final declared-synchronized a(I)V
    .locals 2

    monitor-enter p0

    if-ltz p1, :cond_0

    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public final a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/H;->u:J

    return-void
.end method

.method public a(JIIILcom/google/android/exoplayer2/extractor/TrackOutput$a;)V
    .locals 11
    .param p6    # Lcom/google/android/exoplayer2/extractor/TrackOutput$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object v8, p0

    iget-boolean v0, v8, Lcom/google/android/exoplayer2/source/H;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, v8, Lcom/google/android/exoplayer2/source/H;->B:Lcom/google/android/exoplayer2/Format;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/Format;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/source/H;->a(Lcom/google/android/exoplayer2/Format;)V

    :cond_0
    and-int/lit8 v0, p3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    move v3, v2

    goto :goto_0

    :cond_1
    move v3, v1

    :goto_0
    iget-boolean v4, v8, Lcom/google/android/exoplayer2/source/H;->y:Z

    if-eqz v4, :cond_3

    if-nez v3, :cond_2

    return-void

    :cond_2
    iput-boolean v1, v8, Lcom/google/android/exoplayer2/source/H;->y:Z

    :cond_3
    iget-wide v4, v8, Lcom/google/android/exoplayer2/source/H;->H:J

    add-long/2addr v4, p1

    iget-boolean v6, v8, Lcom/google/android/exoplayer2/source/H;->F:Z

    if-eqz v6, :cond_6

    iget-wide v6, v8, Lcom/google/android/exoplayer2/source/H;->u:J

    cmp-long v6, v4, v6

    if-gez v6, :cond_4

    return-void

    :cond_4
    if-nez v0, :cond_6

    iget-boolean v0, v8, Lcom/google/android/exoplayer2/source/H;->G:Z

    if-nez v0, :cond_5

    iget-object v0, v8, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x32

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Overriding unexpected non-sync sample for format: "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v6, "SampleQueue"

    invoke-static {v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, v8, Lcom/google/android/exoplayer2/source/H;->G:Z

    :cond_5
    or-int/lit8 v0, p3, 0x1

    move v6, v0

    goto :goto_1

    :cond_6
    move v6, p3

    :goto_1
    iget-boolean v0, v8, Lcom/google/android/exoplayer2/source/H;->I:Z

    if-eqz v0, :cond_9

    if-eqz v3, :cond_8

    invoke-direct {p0, v4, v5}, Lcom/google/android/exoplayer2/source/H;->b(J)Z

    move-result v0

    if-nez v0, :cond_7

    goto :goto_2

    :cond_7
    iput-boolean v1, v8, Lcom/google/android/exoplayer2/source/H;->I:Z

    goto :goto_3

    :cond_8
    :goto_2
    return-void

    :cond_9
    :goto_3
    iget-object v0, v8, Lcom/google/android/exoplayer2/source/H;->a:Lcom/google/android/exoplayer2/source/G;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/G;->a()J

    move-result-wide v0

    move v7, p4

    int-to-long v2, v7

    sub-long/2addr v0, v2

    move/from16 v2, p5

    int-to-long v2, v2

    sub-long v9, v0, v2

    move-object v0, p0

    move-wide v1, v4

    move v3, v6

    move-wide v4, v9

    move v6, p4

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer2/source/H;->a(JIJILcom/google/android/exoplayer2/extractor/TrackOutput$a;)V

    return-void
.end method

.method public final a(JZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->a:Lcom/google/android/exoplayer2/source/G;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/source/H;->b(JZZ)J

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/source/G;->a(J)V

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/Format;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/source/H;->b(Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/source/H;->A:Z

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/H;->B:Lcom/google/android/exoplayer2/Format;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/H;->c(Lcom/google/android/exoplayer2/Format;)Z

    move-result p1

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/H;->f:Lcom/google/android/exoplayer2/source/H$b;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/source/H$b;->a(Lcom/google/android/exoplayer2/Format;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/source/H$b;)V
    .locals 0
    .param p1    # Lcom/google/android/exoplayer2/source/H$b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/H;->f:Lcom/google/android/exoplayer2/source/H$b;

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer2/util/t;II)V
    .locals 0

    iget-object p3, p0, Lcom/google/android/exoplayer2/source/H;->a:Lcom/google/android/exoplayer2/source/G;

    invoke-virtual {p3, p1, p2}, Lcom/google/android/exoplayer2/source/G;->a(Lcom/google/android/exoplayer2/util/t;I)V

    return-void
.end method

.method public declared-synchronized a(Z)Z
    .locals 3
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/H;->m()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_2

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lcom/google/android/exoplayer2/source/H;->x:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->g:Lcom/google/android/exoplayer2/Format;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    monitor-exit p0

    return v1

    :cond_2
    :try_start_1
    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/H;->e(I)I

    move-result p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->p:[Lcom/google/android/exoplayer2/Format;

    aget-object v0, v0, p1

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/H;->g:Lcom/google/android/exoplayer2/Format;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v0, v2, :cond_3

    monitor-exit p0

    return v1

    :cond_3
    :try_start_2
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/H;->f(I)Z

    move-result p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized b()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/H;->w:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected b(Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/Format;
    .locals 5
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/H;->H:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p1, Lcom/google/android/exoplayer2/Format;->p:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/Format;->c()Lcom/google/android/exoplayer2/Format$a;

    move-result-object v0

    iget-wide v1, p1, Lcom/google/android/exoplayer2/Format;->p:J

    iget-wide v3, p0, Lcom/google/android/exoplayer2/source/H;->H:J

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/Format$a;->a(J)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/Format$a;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public b(Z)V
    .locals 4
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->a:Lcom/google/android/exoplayer2/source/G;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/G;->b()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    iput v0, p0, Lcom/google/android/exoplayer2/source/H;->r:I

    iput v0, p0, Lcom/google/android/exoplayer2/source/H;->s:I

    iput v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/source/H;->y:Z

    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, Lcom/google/android/exoplayer2/source/H;->u:J

    iput-wide v2, p0, Lcom/google/android/exoplayer2/source/H;->v:J

    iput-wide v2, p0, Lcom/google/android/exoplayer2/source/H;->w:J

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/H;->x:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/H;->D:Lcom/google/android/exoplayer2/Format;

    if-eqz p1, :cond_0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/H;->B:Lcom/google/android/exoplayer2/Format;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/source/H;->z:Z

    :cond_0
    return-void
.end method

.method public final declared-synchronized b(JZ)Z
    .locals 8

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/H;->o()V

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/H;->e(I)I

    move-result v2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/H;->m()Z

    move-result v0

    const/4 v7, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->n:[J

    aget-wide v3, v0, v2

    cmp-long v0, p1, v3

    if-ltz v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/H;->w:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    iget p3, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    sub-int v3, p3, v0

    const/4 v6, 0x1

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/source/H;->a(IIJZ)I

    move-result p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_1

    monitor-exit p0

    return v7

    :cond_1
    :try_start_1
    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/H;->u:J

    iget p1, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/google/android/exoplayer2/source/H;->t:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 p1, 0x1

    monitor-exit p0

    return p1

    :cond_2
    :goto_0
    monitor-exit p0

    return v7

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized c()J
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/H;->v:J

    iget v2, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    invoke-direct {p0, v2}, Lcom/google/android/exoplayer2/source/H;->d(I)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()I
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->r:I

    iget v1, p0, Lcom/google/android/exoplayer2/source/H;->t:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final declared-synchronized e()Lcom/google/android/exoplayer2/Format;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/H;->z:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->C:Lcom/google/android/exoplayer2/Format;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()I
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer2/source/H;->r:I

    iget v1, p0, Lcom/google/android/exoplayer2/source/H;->q:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final declared-synchronized g()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/H;->x:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()V
    .locals 2
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->h:Lcom/google/android/exoplayer2/drm/DrmSession;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/exoplayer2/drm/DrmSession;->getState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/H;->h:Lcom/google/android/exoplayer2/drm/DrmSession;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/drm/DrmSession;->c()Lcom/google/android/exoplayer2/drm/DrmSession$a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/drm/DrmSession$a;

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method public i()V
    .locals 0
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/H;->a()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/H;->n()V

    return-void
.end method

.method public j()V
    .locals 1
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/source/H;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/H;->n()V

    return-void
.end method

.method public final k()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/source/H;->b(Z)V

    return-void
.end method
