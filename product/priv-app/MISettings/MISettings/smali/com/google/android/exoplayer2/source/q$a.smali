.class public final Lcom/google/android/exoplayer2/source/q$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/source/B;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/source/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/upstream/l$a;

.field private b:Lcom/google/android/exoplayer2/extractor/m;

.field private c:Lcom/google/android/exoplayer2/upstream/x;

.field private d:I

.field private e:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/upstream/l$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/q$a;->a:Lcom/google/android/exoplayer2/upstream/l$a;

    new-instance p1, Lcom/google/android/exoplayer2/extractor/g;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/extractor/g;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/q$a;->b:Lcom/google/android/exoplayer2/extractor/m;

    new-instance p1, Lcom/google/android/exoplayer2/upstream/v;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/upstream/v;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/q$a;->c:Lcom/google/android/exoplayer2/upstream/x;

    const/high16 p1, 0x100000

    iput p1, p0, Lcom/google/android/exoplayer2/source/q$a;->d:I

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Lcom/google/android/exoplayer2/source/q;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/W$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/W$a;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/W$a;->a(Landroid/net/Uri;)Lcom/google/android/exoplayer2/W$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/W$a;->a()Lcom/google/android/exoplayer2/W;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/source/q$a;->a(Lcom/google/android/exoplayer2/W;)Lcom/google/android/exoplayer2/source/q;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/google/android/exoplayer2/W;)Lcom/google/android/exoplayer2/source/q;
    .locals 10

    iget-object v0, p1, Lcom/google/android/exoplayer2/W;->b:Lcom/google/android/exoplayer2/W$d;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/exoplayer2/source/q;

    iget-object p1, p1, Lcom/google/android/exoplayer2/W;->b:Lcom/google/android/exoplayer2/W$d;

    iget-object v2, p1, Lcom/google/android/exoplayer2/W$d;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/q$a;->a:Lcom/google/android/exoplayer2/upstream/l$a;

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/q$a;->b:Lcom/google/android/exoplayer2/extractor/m;

    iget-object v5, p0, Lcom/google/android/exoplayer2/source/q$a;->c:Lcom/google/android/exoplayer2/upstream/x;

    iget-object v6, p0, Lcom/google/android/exoplayer2/source/q$a;->e:Ljava/lang/String;

    iget v7, p0, Lcom/google/android/exoplayer2/source/q$a;->d:I

    iget-object p1, p1, Lcom/google/android/exoplayer2/W$d;->h:Ljava/lang/Object;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/q$a;->f:Ljava/lang/Object;

    :goto_0
    move-object v8, p1

    const/4 v9, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/exoplayer2/source/q;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/l$a;Lcom/google/android/exoplayer2/extractor/m;Lcom/google/android/exoplayer2/upstream/x;Ljava/lang/String;ILjava/lang/Object;Lcom/google/android/exoplayer2/source/p;)V

    return-object v0
.end method
