.class final Lcom/google/android/exoplayer2/source/m$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/source/A;
.implements Lcom/google/android/exoplayer2/drm/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/source/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;
    .annotation build Lcom/google/android/exoplayer2/util/UnknownNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private b:Lcom/google/android/exoplayer2/source/A$a;

.field private c:Lcom/google/android/exoplayer2/drm/c$a;

.field final synthetic d:Lcom/google/android/exoplayer2/source/m;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/source/m;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lcom/google/android/exoplayer2/source/m;
        .annotation build Lcom/google/android/exoplayer2/util/UnknownNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/m$a;->d:Lcom/google/android/exoplayer2/source/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/source/j;->b(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/source/A$a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/source/m$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/source/j;->a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/drm/c$a;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/m$a;->c:Lcom/google/android/exoplayer2/drm/c$a;

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/m$a;->a:Ljava/lang/Object;

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/source/w;)Lcom/google/android/exoplayer2/source/w;
    .locals 12

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/m$a;->d:Lcom/google/android/exoplayer2/source/m;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/m$a;->a:Ljava/lang/Object;

    iget-wide v8, p1, Lcom/google/android/exoplayer2/source/w;->f:J

    invoke-virtual {v0, v1, v8, v9}, Lcom/google/android/exoplayer2/source/m;->a(Ljava/lang/Object;J)J

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/m$a;->d:Lcom/google/android/exoplayer2/source/m;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/m$a;->a:Ljava/lang/Object;

    iget-wide v10, p1, Lcom/google/android/exoplayer2/source/w;->g:J

    invoke-virtual {v0, v1, v10, v11}, Lcom/google/android/exoplayer2/source/m;->a(Ljava/lang/Object;J)J

    iget-wide v0, p1, Lcom/google/android/exoplayer2/source/w;->f:J

    cmp-long v0, v8, v0

    if-nez v0, :cond_0

    iget-wide v0, p1, Lcom/google/android/exoplayer2/source/w;->g:J

    cmp-long v0, v10, v0

    if-nez v0, :cond_0

    return-object p1

    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/source/w;

    iget v3, p1, Lcom/google/android/exoplayer2/source/w;->a:I

    iget v4, p1, Lcom/google/android/exoplayer2/source/w;->b:I

    iget-object v5, p1, Lcom/google/android/exoplayer2/source/w;->c:Lcom/google/android/exoplayer2/Format;

    iget v6, p1, Lcom/google/android/exoplayer2/source/w;->d:I

    iget-object v7, p1, Lcom/google/android/exoplayer2/source/w;->e:Ljava/lang/Object;

    move-object v2, v0

    invoke-direct/range {v2 .. v11}, Lcom/google/android/exoplayer2/source/w;-><init>(IILcom/google/android/exoplayer2/Format;ILjava/lang/Object;JJ)V

    return-object v0
.end method

.method private a(ILcom/google/android/exoplayer2/source/y$a;)Z
    .locals 3
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/m$a;->d:Lcom/google/android/exoplayer2/source/m;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/m$a;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/exoplayer2/source/m;->a(Ljava/lang/Object;Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/source/y$a;

    move-result-object p2

    if-nez p2, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p2, 0x0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/m$a;->d:Lcom/google/android/exoplayer2/source/m;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/m$a;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/exoplayer2/source/m;->a(Ljava/lang/Object;I)I

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/m$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    iget v1, v0, Lcom/google/android/exoplayer2/source/A$a;->a:I

    if-ne v1, p1, :cond_2

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/A$a;->b:Lcom/google/android/exoplayer2/source/y$a;

    invoke-static {v0, p2}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/m$a;->d:Lcom/google/android/exoplayer2/source/m;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/android/exoplayer2/source/j;->a(ILcom/google/android/exoplayer2/source/y$a;J)Lcom/google/android/exoplayer2/source/A$a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/m$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/m$a;->c:Lcom/google/android/exoplayer2/drm/c$a;

    iget v1, v0, Lcom/google/android/exoplayer2/drm/c$a;->a:I

    if-ne v1, p1, :cond_4

    iget-object v0, v0, Lcom/google/android/exoplayer2/drm/c$a;->b:Lcom/google/android/exoplayer2/source/y$a;

    invoke-static {v0, p2}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/m$a;->d:Lcom/google/android/exoplayer2/source/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/source/j;->a(ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/drm/c$a;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/m$a;->c:Lcom/google/android/exoplayer2/drm/c$a;

    :cond_5
    const/4 p1, 0x1

    return p1
.end method


# virtual methods
.method public a(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/m$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/m$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-direct {p0, p4}, Lcom/google/android/exoplayer2/source/m$a;->a(Lcom/google/android/exoplayer2/source/w;)Lcom/google/android/exoplayer2/source/w;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Lcom/google/android/exoplayer2/source/A$a;->a(Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V

    :cond_0
    return-void
.end method

.method public a(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;Ljava/io/IOException;Z)V
    .locals 0
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/m$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/m$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-direct {p0, p4}, Lcom/google/android/exoplayer2/source/m$a;->a(Lcom/google/android/exoplayer2/source/w;)Lcom/google/android/exoplayer2/source/w;

    move-result-object p2

    invoke-virtual {p1, p3, p2, p5, p6}, Lcom/google/android/exoplayer2/source/A$a;->a(Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;Ljava/io/IOException;Z)V

    :cond_0
    return-void
.end method

.method public a(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/m$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/m$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-direct {p0, p3}, Lcom/google/android/exoplayer2/source/m$a;->a(Lcom/google/android/exoplayer2/source/w;)Lcom/google/android/exoplayer2/source/w;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/source/A$a;->a(Lcom/google/android/exoplayer2/source/w;)V

    :cond_0
    return-void
.end method

.method public b(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/m$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/m$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-direct {p0, p4}, Lcom/google/android/exoplayer2/source/m$a;->a(Lcom/google/android/exoplayer2/source/w;)Lcom/google/android/exoplayer2/source/w;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Lcom/google/android/exoplayer2/source/A$a;->c(Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V

    :cond_0
    return-void
.end method

.method public c(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/m$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/m$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-direct {p0, p4}, Lcom/google/android/exoplayer2/source/m$a;->a(Lcom/google/android/exoplayer2/source/w;)Lcom/google/android/exoplayer2/source/w;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Lcom/google/android/exoplayer2/source/A$a;->b(Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V

    :cond_0
    return-void
.end method
