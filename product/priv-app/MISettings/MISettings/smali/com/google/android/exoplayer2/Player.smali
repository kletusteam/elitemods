.class public interface abstract Lcom/google/android/exoplayer2/Player;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/Player$MediaItemTransitionReason;,
        Lcom/google/android/exoplayer2/Player$TimelineChangeReason;,
        Lcom/google/android/exoplayer2/Player$DiscontinuityReason;,
        Lcom/google/android/exoplayer2/Player$RepeatMode;,
        Lcom/google/android/exoplayer2/Player$PlaybackSuppressionReason;,
        Lcom/google/android/exoplayer2/Player$PlayWhenReadyChangeReason;,
        Lcom/google/android/exoplayer2/Player$State;,
        Lcom/google/android/exoplayer2/Player$c;,
        Lcom/google/android/exoplayer2/Player$b;,
        Lcom/google/android/exoplayer2/Player$d;,
        Lcom/google/android/exoplayer2/Player$e;,
        Lcom/google/android/exoplayer2/Player$f;,
        Lcom/google/android/exoplayer2/Player$a;
    }
.end annotation


# virtual methods
.method public abstract a(I)I
.end method

.method public abstract a()Lcom/google/android/exoplayer2/ga;
.end method

.method public abstract a(IJ)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/Player$c;)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/ga;)V
    .param p1    # Lcom/google/android/exoplayer2/ga;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(Z)V
.end method

.method public abstract b(Lcom/google/android/exoplayer2/Player$c;)V
.end method

.method public abstract b(Z)V
.end method

.method public abstract b()Z
.end method

.method public abstract c()J
.end method

.method public abstract d()Z
.end method

.method public abstract e()Lcom/google/android/exoplayer2/trackselection/o;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract f()I
.end method

.method public abstract g()Z
.end method

.method public abstract getCurrentPosition()J
.end method

.method public abstract getDuration()J
.end method

.method public abstract getPlaybackState()I
.end method

.method public abstract getRepeatMode()I
.end method

.method public abstract h()I
.end method

.method public abstract hasNext()Z
.end method

.method public abstract hasPrevious()Z
.end method

.method public abstract i()I
.end method

.method public abstract isPlaying()Z
.end method

.method public abstract j()Lcom/google/android/exoplayer2/ExoPlaybackException;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()Lcom/google/android/exoplayer2/Player$f;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()J
.end method

.method public abstract m()I
.end method

.method public abstract n()I
.end method

.method public abstract o()I
.end method

.method public abstract p()I
.end method

.method public abstract q()Lcom/google/android/exoplayer2/source/TrackGroupArray;
.end method

.method public abstract r()Lcom/google/android/exoplayer2/sa;
.end method

.method public abstract s()Landroid/os/Looper;
.end method

.method public abstract setRepeatMode(I)V
.end method

.method public abstract t()Z
.end method

.method public abstract u()J
.end method

.method public abstract v()Lcom/google/android/exoplayer2/trackselection/m;
.end method

.method public abstract w()Lcom/google/android/exoplayer2/Player$e;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method
