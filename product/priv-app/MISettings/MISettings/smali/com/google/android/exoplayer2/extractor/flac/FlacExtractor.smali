.class public final Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/Extractor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor$Flags;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/exoplayer2/extractor/m;


# instance fields
.field private final b:[B

.field private final c:Lcom/google/android/exoplayer2/util/t;

.field private final d:Z

.field private final e:Lcom/google/android/exoplayer2/extractor/n$a;

.field private f:Lcom/google/android/exoplayer2/extractor/k;

.field private g:Lcom/google/android/exoplayer2/extractor/TrackOutput;

.field private h:I

.field private i:Lcom/google/android/exoplayer2/metadata/Metadata;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/google/android/exoplayer2/extractor/q;

.field private k:I

.field private l:I

.field private m:Lcom/google/android/exoplayer2/extractor/flac/d;

.field private n:I

.field private o:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/extractor/flac/a;->a:Lcom/google/android/exoplayer2/extractor/flac/a;

    sput-object v0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->a:Lcom/google/android/exoplayer2/extractor/m;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x2a

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->b:[B

    new-instance v0, Lcom/google/android/exoplayer2/util/t;

    const v1, 0x8000

    new-array v1, v1, [B

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/t;-><init>([BI)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    const/4 v0, 0x1

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->d:Z

    new-instance p1, Lcom/google/android/exoplayer2/extractor/n$a;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/extractor/n$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->e:Lcom/google/android/exoplayer2/extractor/n$a;

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->h:I

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/util/t;Z)J
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x10

    if-gt v0, v1, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->l:I

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->e:Lcom/google/android/exoplayer2/extractor/n$a;

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/exoplayer2/extractor/n;->a(Lcom/google/android/exoplayer2/util/t;Lcom/google/android/exoplayer2/extractor/q;ILcom/google/android/exoplayer2/extractor/n$a;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->e:Lcom/google/android/exoplayer2/extractor/n$a;

    iget-wide p1, p1, Lcom/google/android/exoplayer2/extractor/n$a;->a:J

    return-wide p1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_5

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result p2

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->k:I

    sub-int/2addr p2, v1

    if-gt v0, p2, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    const/4 p2, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->l:I

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->e:Lcom/google/android/exoplayer2/extractor/n$a;

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/exoplayer2/extractor/n;->a(Lcom/google/android/exoplayer2/util/t;Lcom/google/android/exoplayer2/extractor/q;ILcom/google/android/exoplayer2/extractor/n$a;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move v1, p2

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v3

    if-le v2, v3, :cond_2

    goto :goto_3

    :cond_2
    move p2, v1

    :goto_3
    if-eqz p2, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->e:Lcom/google/android/exoplayer2/extractor/n$a;

    iget-wide p1, p1, Lcom/google/android/exoplayer2/extractor/n$a;->a:J

    return-wide p1

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    goto :goto_4

    :cond_5
    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    :goto_4
    const-wide/16 p1, -0x1

    return-wide p1
.end method

.method static synthetic a()[Lcom/google/android/exoplayer2/extractor/Extractor;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/exoplayer2/extractor/Extractor;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method private b(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->g:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->m:Lcom/google/android/exoplayer2/extractor/flac/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->m:Lcom/google/android/exoplayer2/extractor/flac/d;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/b;->a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I

    move-result p1

    return p1

    :cond_0
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->o:J

    const-wide/16 v2, -0x1

    cmp-long p2, v0, v2

    const/4 v0, 0x0

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    invoke-static {p1, p2}, Lcom/google/android/exoplayer2/extractor/n;->a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/q;)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->o:J

    return v0

    :cond_1
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result p2

    const v1, 0x8000

    if-ge p2, v1, :cond_4

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v4

    sub-int/2addr v1, p2

    invoke-interface {p1, v4, p2, v1}, Lcom/google/android/exoplayer2/extractor/i;->read([BII)I

    move-result p1

    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    move v4, v0

    :goto_0
    if-nez v4, :cond_3

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    add-int/2addr p2, p1

    invoke-virtual {v1, p2}, Lcom/google/android/exoplayer2/util/t;->d(I)V

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result p1

    if-nez p1, :cond_5

    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->b()V

    return v1

    :cond_4
    move v4, v0

    :cond_5
    :goto_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result p1

    iget p2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->n:I

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->k:I

    if-ge p2, v1, :cond_6

    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    sub-int/2addr v1, p2

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result p2

    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-virtual {v5, p2}, Lcom/google/android/exoplayer2/util/t;->f(I)V

    :cond_6
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-direct {p0, p2, v4}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->a(Lcom/google/android/exoplayer2/util/t;Z)J

    move-result-wide v4

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result p2

    sub-int/2addr p2, p1

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v1, p1}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->g:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-interface {p1, v1, p2}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/util/t;I)V

    iget p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->n:I

    add-int/2addr p1, p2

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->n:I

    cmp-long p1, v4, v2

    if-eqz p1, :cond_7

    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->b()V

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->n:I

    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->o:J

    :cond_7
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result p1

    const/16 p2, 0x10

    if-ge p1, p2, :cond_8

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p1

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result p2

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v2

    invoke-static {p1, p2, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    :cond_8
    return v0
.end method

.method private b(JJ)Lcom/google/android/exoplayer2/extractor/u;
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    iget-object v0, v2, Lcom/google/android/exoplayer2/extractor/q;->k:Lcom/google/android/exoplayer2/extractor/q$a;

    if-eqz v0, :cond_0

    new-instance p3, Lcom/google/android/exoplayer2/extractor/p;

    invoke-direct {p3, v2, p1, p2}, Lcom/google/android/exoplayer2/extractor/p;-><init>(Lcom/google/android/exoplayer2/extractor/q;J)V

    return-object p3

    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p3, v0

    if-eqz v0, :cond_1

    iget-wide v0, v2, Lcom/google/android/exoplayer2/extractor/q;->j:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-lez v0, :cond_1

    new-instance v0, Lcom/google/android/exoplayer2/extractor/flac/d;

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->l:I

    move-object v1, v0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer2/extractor/flac/d;-><init>(Lcom/google/android/exoplayer2/extractor/q;IJJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->m:Lcom/google/android/exoplayer2/extractor/flac/d;

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->m:Lcom/google/android/exoplayer2/extractor/flac/d;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/extractor/b;->a()Lcom/google/android/exoplayer2/extractor/u;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Lcom/google/android/exoplayer2/extractor/u$b;

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/q;->b()J

    move-result-wide p2

    invoke-direct {p1, p2, p3}, Lcom/google/android/exoplayer2/extractor/u$b;-><init>(J)V

    return-object p1
.end method

.method private b()V
    .locals 11

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->o:J

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v2, Lcom/google/android/exoplayer2/extractor/q;

    iget v2, v2, Lcom/google/android/exoplayer2/extractor/q;->e:I

    int-to-long v2, v2

    div-long v5, v0, v2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->g:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v0

    check-cast v4, Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget v8, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->n:I

    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-interface/range {v4 .. v10}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(JIIILcom/google/android/exoplayer2/extractor/TrackOutput$a;)V

    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/extractor/i;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/o;->b(Lcom/google/android/exoplayer2/extractor/i;)I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->l:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->f:Lcom/google/android/exoplayer2/extractor/k;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/extractor/k;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v1

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getLength()J

    move-result-wide v3

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->b(JJ)Lcom/google/android/exoplayer2/extractor/u;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/extractor/k;->a(Lcom/google/android/exoplayer2/extractor/u;)V

    const/4 p1, 0x5

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->h:I

    return-void
.end method

.method private c(Lcom/google/android/exoplayer2/extractor/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->b:[B

    array-length v1, v0

    const/4 v2, 0x0

    invoke-interface {p1, v0, v2, v1}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    const/4 p1, 0x2

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->h:I

    return-void
.end method

.method private d(Lcom/google/android/exoplayer2/extractor/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->d:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/extractor/o;->b(Lcom/google/android/exoplayer2/extractor/i;Z)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->i:Lcom/google/android/exoplayer2/metadata/Metadata;

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->h:I

    return-void
.end method

.method private e(Lcom/google/android/exoplayer2/extractor/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/extractor/o$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/extractor/o$a;-><init>(Lcom/google/android/exoplayer2/extractor/q;)V

    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_0

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/extractor/o;->a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/o$a;)Z

    move-result v1

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/o$a;->a:Lcom/google/android/exoplayer2/extractor/q;

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v2, Lcom/google/android/exoplayer2/extractor/q;

    iput-object v2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    iget p1, p1, Lcom/google/android/exoplayer2/extractor/q;->c:I

    const/4 v0, 0x6

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->k:I

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->g:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->j:Lcom/google/android/exoplayer2/extractor/q;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->b:[B

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->i:Lcom/google/android/exoplayer2/metadata/Metadata;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/q;->a([BLcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/Format;)V

    const/4 p1, 0x4

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->h:I

    return-void
.end method

.method private f(Lcom/google/android/exoplayer2/extractor/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/o;->c(Lcom/google/android/exoplayer2/extractor/i;)V

    const/4 p1, 0x3

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->h:I

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->h:I

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->b(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I

    move-result p1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->b(Lcom/google/android/exoplayer2/extractor/i;)V

    return v1

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->e(Lcom/google/android/exoplayer2/extractor/i;)V

    return v1

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->f(Lcom/google/android/exoplayer2/extractor/i;)V

    return v1

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c(Lcom/google/android/exoplayer2/extractor/i;)V

    return v1

    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->d(Lcom/google/android/exoplayer2/extractor/i;)V

    return v1
.end method

.method public a(JJ)V
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long p1, p1, v0

    const/4 p2, 0x0

    if-nez p1, :cond_0

    iput p2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->h:I

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->m:Lcom/google/android/exoplayer2/extractor/flac/d;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p3, p4}, Lcom/google/android/exoplayer2/extractor/b;->b(J)V

    :cond_1
    :goto_0
    cmp-long p1, p3, v0

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    const-wide/16 v0, -0x1

    :goto_1
    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->o:J

    iput p2, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->n:I

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->c:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/k;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->f:Lcom/google/android/exoplayer2/extractor/k;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lcom/google/android/exoplayer2/extractor/k;->a(II)Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/flac/FlacExtractor;->g:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/k;->g()V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/extractor/o;->a(Lcom/google/android/exoplayer2/extractor/i;Z)Lcom/google/android/exoplayer2/metadata/Metadata;

    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/o;->a(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result p1

    return p1
.end method

.method public release()V
    .locals 0

    return-void
.end method
