.class public interface abstract Lcom/google/android/exoplayer2/audio/AudioSink;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/audio/AudioSink$SinkFormatSupport;,
        Lcom/google/android/exoplayer2/audio/AudioSink$d;,
        Lcom/google/android/exoplayer2/audio/AudioSink$b;,
        Lcom/google/android/exoplayer2/audio/AudioSink$a;,
        Lcom/google/android/exoplayer2/audio/AudioSink$c;
    }
.end annotation


# virtual methods
.method public abstract a(Z)J
.end method

.method public abstract a()Lcom/google/android/exoplayer2/ga;
.end method

.method public abstract a(F)V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/Format;I[I)V
    .param p3    # [I
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$a;
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/exoplayer2/audio/AudioSink$c;)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/audio/o;)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/audio/v;)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/ga;)V
.end method

.method public abstract a(Lcom/google/android/exoplayer2/Format;)Z
.end method

.method public abstract a(Ljava/nio/ByteBuffer;JI)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$b;,
            Lcom/google/android/exoplayer2/audio/AudioSink$d;
        }
    .end annotation
.end method

.method public abstract b(Lcom/google/android/exoplayer2/Format;)I
.end method

.method public abstract b(I)V
.end method

.method public abstract b(Z)V
.end method

.method public abstract b()Z
.end method

.method public abstract c()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$d;
        }
    .end annotation
.end method

.method public abstract d()Z
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public abstract flush()V
.end method

.method public abstract g()V
.end method

.method public abstract pause()V
.end method

.method public abstract play()V
.end method

.method public abstract reset()V
.end method
