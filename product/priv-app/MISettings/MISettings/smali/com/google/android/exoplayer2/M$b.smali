.class final Lcom/google/android/exoplayer2/M$b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/M;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/fa;

.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/google/android/exoplayer2/E$a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/exoplayer2/trackselection/o;

.field private final d:Z

.field private final e:I

.field private final f:I

.field private final g:Z

.field private final h:I

.field private final i:Lcom/google/android/exoplayer2/W;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final j:I

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:Z

.field private final q:Z

.field private final r:Z

.field private final s:Z

.field private final t:Z

.field private final u:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/fa;Lcom/google/android/exoplayer2/fa;Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/trackselection/o;ZIIZILcom/google/android/exoplayer2/W;IZ)V
    .locals 1
    .param p10    # Lcom/google/android/exoplayer2/W;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/fa;",
            "Lcom/google/android/exoplayer2/fa;",
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/google/android/exoplayer2/E$a;",
            ">;",
            "Lcom/google/android/exoplayer2/trackselection/o;",
            "ZIIZI",
            "Lcom/google/android/exoplayer2/W;",
            "IZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0, p3}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput-object p4, p0, Lcom/google/android/exoplayer2/M$b;->c:Lcom/google/android/exoplayer2/trackselection/o;

    iput-boolean p5, p0, Lcom/google/android/exoplayer2/M$b;->d:Z

    iput p6, p0, Lcom/google/android/exoplayer2/M$b;->e:I

    iput p7, p0, Lcom/google/android/exoplayer2/M$b;->f:I

    iput-boolean p8, p0, Lcom/google/android/exoplayer2/M$b;->g:Z

    iput p9, p0, Lcom/google/android/exoplayer2/M$b;->h:I

    iput-object p10, p0, Lcom/google/android/exoplayer2/M$b;->i:Lcom/google/android/exoplayer2/W;

    iput p11, p0, Lcom/google/android/exoplayer2/M$b;->j:I

    iput-boolean p12, p0, Lcom/google/android/exoplayer2/M$b;->k:Z

    iget p3, p2, Lcom/google/android/exoplayer2/fa;->e:I

    iget p4, p1, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 p5, 0x0

    const/4 p6, 0x1

    if-eq p3, p4, :cond_0

    move p3, p6

    goto :goto_0

    :cond_0
    move p3, p5

    :goto_0
    iput-boolean p3, p0, Lcom/google/android/exoplayer2/M$b;->l:Z

    iget-object p3, p2, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-object p4, p1, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    if-eq p3, p4, :cond_1

    if-eqz p4, :cond_1

    move p3, p6

    goto :goto_1

    :cond_1
    move p3, p5

    :goto_1
    iput-boolean p3, p0, Lcom/google/android/exoplayer2/M$b;->m:Z

    iget-boolean p3, p2, Lcom/google/android/exoplayer2/fa;->g:Z

    iget-boolean p4, p1, Lcom/google/android/exoplayer2/fa;->g:Z

    if-eq p3, p4, :cond_2

    move p3, p6

    goto :goto_2

    :cond_2
    move p3, p5

    :goto_2
    iput-boolean p3, p0, Lcom/google/android/exoplayer2/M$b;->n:Z

    iget-object p3, p2, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object p4, p1, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    invoke-virtual {p3, p4}, Lcom/google/android/exoplayer2/sa;->equals(Ljava/lang/Object;)Z

    move-result p3

    xor-int/2addr p3, p6

    iput-boolean p3, p0, Lcom/google/android/exoplayer2/M$b;->o:Z

    iget-object p3, p2, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object p4, p1, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    if-eq p3, p4, :cond_3

    move p3, p6

    goto :goto_3

    :cond_3
    move p3, p5

    :goto_3
    iput-boolean p3, p0, Lcom/google/android/exoplayer2/M$b;->p:Z

    iget-boolean p3, p2, Lcom/google/android/exoplayer2/fa;->k:Z

    iget-boolean p4, p1, Lcom/google/android/exoplayer2/fa;->k:Z

    if-eq p3, p4, :cond_4

    move p3, p6

    goto :goto_4

    :cond_4
    move p3, p5

    :goto_4
    iput-boolean p3, p0, Lcom/google/android/exoplayer2/M$b;->q:Z

    iget p3, p2, Lcom/google/android/exoplayer2/fa;->l:I

    iget p4, p1, Lcom/google/android/exoplayer2/fa;->l:I

    if-eq p3, p4, :cond_5

    move p3, p6

    goto :goto_5

    :cond_5
    move p3, p5

    :goto_5
    iput-boolean p3, p0, Lcom/google/android/exoplayer2/M$b;->r:Z

    invoke-static {p2}, Lcom/google/android/exoplayer2/M$b;->a(Lcom/google/android/exoplayer2/fa;)Z

    move-result p3

    invoke-static {p1}, Lcom/google/android/exoplayer2/M$b;->a(Lcom/google/android/exoplayer2/fa;)Z

    move-result p4

    if-eq p3, p4, :cond_6

    move p3, p6

    goto :goto_6

    :cond_6
    move p3, p5

    :goto_6
    iput-boolean p3, p0, Lcom/google/android/exoplayer2/M$b;->s:Z

    iget-object p3, p2, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    iget-object p4, p1, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    invoke-virtual {p3, p4}, Lcom/google/android/exoplayer2/ga;->equals(Ljava/lang/Object;)Z

    move-result p3

    xor-int/2addr p3, p6

    iput-boolean p3, p0, Lcom/google/android/exoplayer2/M$b;->t:Z

    iget-boolean p2, p2, Lcom/google/android/exoplayer2/fa;->n:Z

    iget-boolean p1, p1, Lcom/google/android/exoplayer2/fa;->n:Z

    if-eq p2, p1, :cond_7

    move p5, p6

    :cond_7
    iput-boolean p5, p0, Lcom/google/android/exoplayer2/M$b;->u:Z

    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/fa;)Z
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer2/fa;->e:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/fa;->k:Z

    if-eqz v0, :cond_0

    iget p0, p0, Lcom/google/android/exoplayer2/fa;->l:I

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public synthetic a(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget v1, p0, Lcom/google/android/exoplayer2/M$b;->f:I

    invoke-interface {p1, v0, v1}, Lcom/google/android/exoplayer2/Player$c;->a(Lcom/google/android/exoplayer2/sa;I)V

    return-void
.end method

.method public synthetic b(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/M$b;->e:I

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/Player$c;->b(I)V

    return-void
.end method

.method public synthetic c(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    invoke-static {v0}, Lcom/google/android/exoplayer2/M$b;->a(Lcom/google/android/exoplayer2/fa;)Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/Player$c;->e(Z)V

    return-void
.end method

.method public synthetic d(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/Player$c;->a(Lcom/google/android/exoplayer2/ga;)V

    return-void
.end method

.method public synthetic e(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/fa;->n:Z

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/Player$c;->d(Z)V

    return-void
.end method

.method public synthetic f(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->i:Lcom/google/android/exoplayer2/W;

    iget v1, p0, Lcom/google/android/exoplayer2/M$b;->h:I

    invoke-interface {p1, v0, v1}, Lcom/google/android/exoplayer2/Player$c;->a(Lcom/google/android/exoplayer2/W;I)V

    return-void
.end method

.method public synthetic g(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/Player$c;->a(Lcom/google/android/exoplayer2/ExoPlaybackException;)V

    return-void
.end method

.method public synthetic h(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v0, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object v0, v0, Lcom/google/android/exoplayer2/trackselection/p;->c:Lcom/google/android/exoplayer2/trackselection/m;

    invoke-interface {p1, v1, v0}, Lcom/google/android/exoplayer2/Player$c;->a(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/m;)V

    return-void
.end method

.method public synthetic i(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/fa;->g:Z

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/Player$c;->b(Z)V

    return-void
.end method

.method public synthetic j(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    iget v0, v0, Lcom/google/android/exoplayer2/fa;->e:I

    invoke-interface {p1, v1, v0}, Lcom/google/android/exoplayer2/Player$c;->a(ZI)V

    return-void
.end method

.method public synthetic k(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    iget v0, v0, Lcom/google/android/exoplayer2/fa;->e:I

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/Player$c;->c(I)V

    return-void
.end method

.method public synthetic l(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    iget v1, p0, Lcom/google/android/exoplayer2/M$b;->j:I

    invoke-interface {p1, v0, v1}, Lcom/google/android/exoplayer2/Player$c;->b(ZI)V

    return-void
.end method

.method public synthetic m(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    iget v0, v0, Lcom/google/android/exoplayer2/fa;->l:I

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/Player$c;->a(I)V

    return-void
.end method

.method public run()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/g;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/g;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/f;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/f;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/l;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/l;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->m:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/k;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/k;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->p:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->c:Lcom/google/android/exoplayer2/trackselection/o;

    iget-object v1, p0, Lcom/google/android/exoplayer2/M$b;->a:Lcom/google/android/exoplayer2/fa;

    iget-object v1, v1, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object v1, v1, Lcom/google/android/exoplayer2/trackselection/p;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/trackselection/o;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/p;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/p;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->n:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/e;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/e;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->l:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->q:Z

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/h;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/h;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->l:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/q;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/q;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_8
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->q:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/o;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/o;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_9
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->r:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/m;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/m;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_a
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->s:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/j;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/j;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_b
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->t:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/n;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/n;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_c
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->k:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    sget-object v1, Lcom/google/android/exoplayer2/a;->a:Lcom/google/android/exoplayer2/a;

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_d
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/M$b;->u:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/exoplayer2/M$b;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/google/android/exoplayer2/i;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer2/i;-><init>(Lcom/google/android/exoplayer2/M$b;)V

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/google/android/exoplayer2/E$b;)V

    :cond_e
    return-void
.end method
