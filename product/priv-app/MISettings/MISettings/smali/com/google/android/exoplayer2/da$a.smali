.class final Lcom/google/android/exoplayer2/da$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/source/A;
.implements Lcom/google/android/exoplayer2/drm/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/da;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/da$c;

.field private b:Lcom/google/android/exoplayer2/source/A$a;

.field private c:Lcom/google/android/exoplayer2/drm/c$a;

.field final synthetic d:Lcom/google/android/exoplayer2/da;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/da;Lcom/google/android/exoplayer2/da$c;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/exoplayer2/da$a;->d:Lcom/google/android/exoplayer2/da;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/exoplayer2/da;->a(Lcom/google/android/exoplayer2/da;)Lcom/google/android/exoplayer2/source/A$a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/da$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-static {p1}, Lcom/google/android/exoplayer2/da;->b(Lcom/google/android/exoplayer2/da;)Lcom/google/android/exoplayer2/drm/c$a;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/da$a;->c:Lcom/google/android/exoplayer2/drm/c$a;

    iput-object p2, p0, Lcom/google/android/exoplayer2/da$a;->a:Lcom/google/android/exoplayer2/da$c;

    return-void
.end method

.method private a(ILcom/google/android/exoplayer2/source/y$a;)Z
    .locals 3
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/da$a;->a:Lcom/google/android/exoplayer2/da$c;

    invoke-static {v0, p2}, Lcom/google/android/exoplayer2/da;->a(Lcom/google/android/exoplayer2/da$c;Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/source/y$a;

    move-result-object p2

    if-nez p2, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p2, 0x0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/da$a;->a:Lcom/google/android/exoplayer2/da$c;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/da;->a(Lcom/google/android/exoplayer2/da$c;I)I

    move-result p1

    iget-object v0, p0, Lcom/google/android/exoplayer2/da$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    iget v1, v0, Lcom/google/android/exoplayer2/source/A$a;->a:I

    if-ne v1, p1, :cond_2

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/A$a;->b:Lcom/google/android/exoplayer2/source/y$a;

    invoke-static {v0, p2}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/da$a;->d:Lcom/google/android/exoplayer2/da;

    invoke-static {v0}, Lcom/google/android/exoplayer2/da;->a(Lcom/google/android/exoplayer2/da;)Lcom/google/android/exoplayer2/source/A$a;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/android/exoplayer2/source/A$a;->a(ILcom/google/android/exoplayer2/source/y$a;J)Lcom/google/android/exoplayer2/source/A$a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/da$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/da$a;->c:Lcom/google/android/exoplayer2/drm/c$a;

    iget v1, v0, Lcom/google/android/exoplayer2/drm/c$a;->a:I

    if-ne v1, p1, :cond_4

    iget-object v0, v0, Lcom/google/android/exoplayer2/drm/c$a;->b:Lcom/google/android/exoplayer2/source/y$a;

    invoke-static {v0, p2}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/da$a;->d:Lcom/google/android/exoplayer2/da;

    invoke-static {v0}, Lcom/google/android/exoplayer2/da;->b(Lcom/google/android/exoplayer2/da;)Lcom/google/android/exoplayer2/drm/c$a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/drm/c$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/drm/c$a;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/da$a;->c:Lcom/google/android/exoplayer2/drm/c$a;

    :cond_5
    const/4 p1, 0x1

    return p1
.end method


# virtual methods
.method public a(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/da$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/da$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-virtual {p1, p3, p4}, Lcom/google/android/exoplayer2/source/A$a;->a(Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V

    :cond_0
    return-void
.end method

.method public a(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;Ljava/io/IOException;Z)V
    .locals 0
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/da$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/da$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-virtual {p1, p3, p4, p5, p6}, Lcom/google/android/exoplayer2/source/A$a;->a(Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;Ljava/io/IOException;Z)V

    :cond_0
    return-void
.end method

.method public a(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/da$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/da$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-virtual {p1, p3}, Lcom/google/android/exoplayer2/source/A$a;->a(Lcom/google/android/exoplayer2/source/w;)V

    :cond_0
    return-void
.end method

.method public b(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/da$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/da$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-virtual {p1, p3, p4}, Lcom/google/android/exoplayer2/source/A$a;->c(Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V

    :cond_0
    return-void
.end method

.method public c(ILcom/google/android/exoplayer2/source/y$a;Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V
    .locals 0
    .param p2    # Lcom/google/android/exoplayer2/source/y$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/da$a;->a(ILcom/google/android/exoplayer2/source/y$a;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/da$a;->b:Lcom/google/android/exoplayer2/source/A$a;

    invoke-virtual {p1, p3, p4}, Lcom/google/android/exoplayer2/source/A$a;->b(Lcom/google/android/exoplayer2/source/t;Lcom/google/android/exoplayer2/source/w;)V

    :cond_0
    return-void
.end method
