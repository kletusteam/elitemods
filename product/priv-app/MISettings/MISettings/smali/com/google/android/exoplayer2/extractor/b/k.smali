.class abstract Lcom/google/android/exoplayer2/extractor/b/k;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/b/k$b;,
        Lcom/google/android/exoplayer2/extractor/b/k$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/extractor/b/f;

.field private b:Lcom/google/android/exoplayer2/extractor/TrackOutput;

.field private c:Lcom/google/android/exoplayer2/extractor/k;

.field private d:Lcom/google/android/exoplayer2/extractor/b/h;

.field private e:J

.field private f:J

.field private g:J

.field private h:I

.field private i:I

.field private j:Lcom/google/android/exoplayer2/extractor/b/k$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private k:J

.field private l:Z

.field private m:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/exoplayer2/extractor/b/f;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/b/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/k;->a:Lcom/google/android/exoplayer2/extractor/b/f;

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/i;)I
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v11, p0

    const/4 v0, 0x1

    move v1, v0

    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    iget-object v1, v11, Lcom/google/android/exoplayer2/extractor/b/k;->a:Lcom/google/android/exoplayer2/extractor/b/f;

    move-object/from16 v2, p1

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/extractor/b/f;->a(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x3

    iput v0, v11, Lcom/google/android/exoplayer2/extractor/b/k;->h:I

    const/4 v0, -0x1

    return v0

    :cond_1
    invoke-interface/range {p1 .. p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v3

    iget-wide v5, v11, Lcom/google/android/exoplayer2/extractor/b/k;->f:J

    sub-long/2addr v3, v5

    iput-wide v3, v11, Lcom/google/android/exoplayer2/extractor/b/k;->k:J

    iget-object v1, v11, Lcom/google/android/exoplayer2/extractor/b/k;->a:Lcom/google/android/exoplayer2/extractor/b/f;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/extractor/b/f;->b()Lcom/google/android/exoplayer2/util/t;

    move-result-object v1

    iget-wide v3, v11, Lcom/google/android/exoplayer2/extractor/b/k;->f:J

    iget-object v5, v11, Lcom/google/android/exoplayer2/extractor/b/k;->j:Lcom/google/android/exoplayer2/extractor/b/k$a;

    invoke-virtual {v11, v1, v3, v4, v5}, Lcom/google/android/exoplayer2/extractor/b/k;->a(Lcom/google/android/exoplayer2/util/t;JLcom/google/android/exoplayer2/extractor/b/k$a;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface/range {p1 .. p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v3

    iput-wide v3, v11, Lcom/google/android/exoplayer2/extractor/b/k;->f:J

    goto :goto_0

    :cond_2
    move-object/from16 v2, p1

    iget-object v1, v11, Lcom/google/android/exoplayer2/extractor/b/k;->j:Lcom/google/android/exoplayer2/extractor/b/k$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/extractor/b/k$a;->a:Lcom/google/android/exoplayer2/Format;

    iget v3, v1, Lcom/google/android/exoplayer2/Format;->z:I

    iput v3, v11, Lcom/google/android/exoplayer2/extractor/b/k;->i:I

    iget-boolean v3, v11, Lcom/google/android/exoplayer2/extractor/b/k;->m:Z

    if-nez v3, :cond_3

    iget-object v3, v11, Lcom/google/android/exoplayer2/extractor/b/k;->b:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-interface {v3, v1}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/Format;)V

    iput-boolean v0, v11, Lcom/google/android/exoplayer2/extractor/b/k;->m:Z

    :cond_3
    iget-object v1, v11, Lcom/google/android/exoplayer2/extractor/b/k;->j:Lcom/google/android/exoplayer2/extractor/b/k$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/extractor/b/k$a;->b:Lcom/google/android/exoplayer2/extractor/b/h;

    const/4 v12, 0x0

    const/4 v13, 0x0

    if-eqz v1, :cond_4

    iput-object v1, v11, Lcom/google/android/exoplayer2/extractor/b/k;->d:Lcom/google/android/exoplayer2/extractor/b/h;

    goto :goto_2

    :cond_4
    invoke-interface/range {p1 .. p1}, Lcom/google/android/exoplayer2/extractor/i;->getLength()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v1, v3, v5

    if-nez v1, :cond_5

    new-instance v0, Lcom/google/android/exoplayer2/extractor/b/k$b;

    invoke-direct {v0, v13}, Lcom/google/android/exoplayer2/extractor/b/k$b;-><init>(Lcom/google/android/exoplayer2/extractor/b/j;)V

    iput-object v0, v11, Lcom/google/android/exoplayer2/extractor/b/k;->d:Lcom/google/android/exoplayer2/extractor/b/h;

    goto :goto_2

    :cond_5
    iget-object v1, v11, Lcom/google/android/exoplayer2/extractor/b/k;->a:Lcom/google/android/exoplayer2/extractor/b/f;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/extractor/b/f;->a()Lcom/google/android/exoplayer2/extractor/b/g;

    move-result-object v1

    iget v3, v1, Lcom/google/android/exoplayer2/extractor/b/g;->b:I

    and-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_6

    move v10, v0

    goto :goto_1

    :cond_6
    move v10, v12

    :goto_1
    new-instance v14, Lcom/google/android/exoplayer2/extractor/b/c;

    iget-wide v3, v11, Lcom/google/android/exoplayer2/extractor/b/k;->f:J

    invoke-interface/range {p1 .. p1}, Lcom/google/android/exoplayer2/extractor/i;->getLength()J

    move-result-wide v5

    iget v0, v1, Lcom/google/android/exoplayer2/extractor/b/g;->h:I

    iget v2, v1, Lcom/google/android/exoplayer2/extractor/b/g;->i:I

    add-int/2addr v0, v2

    int-to-long v7, v0

    iget-wide v1, v1, Lcom/google/android/exoplayer2/extractor/b/g;->c:J

    move-object v0, v14

    move-wide v15, v1

    move-object/from16 v1, p0

    move-wide v2, v3

    move-wide v4, v5

    move-wide v6, v7

    move-wide v8, v15

    invoke-direct/range {v0 .. v10}, Lcom/google/android/exoplayer2/extractor/b/c;-><init>(Lcom/google/android/exoplayer2/extractor/b/k;JJJJZ)V

    iput-object v14, v11, Lcom/google/android/exoplayer2/extractor/b/k;->d:Lcom/google/android/exoplayer2/extractor/b/h;

    :goto_2
    iput-object v13, v11, Lcom/google/android/exoplayer2/extractor/b/k;->j:Lcom/google/android/exoplayer2/extractor/b/k$a;

    const/4 v0, 0x2

    iput v0, v11, Lcom/google/android/exoplayer2/extractor/b/k;->h:I

    iget-object v0, v11, Lcom/google/android/exoplayer2/extractor/b/k;->a:Lcom/google/android/exoplayer2/extractor/b/f;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/b/f;->d()V

    return v12
.end method

.method private b(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/b/k;->d:Lcom/google/android/exoplayer2/extractor/b/h;

    invoke-interface {v2, v1}, Lcom/google/android/exoplayer2/extractor/b/h;->a(Lcom/google/android/exoplayer2/extractor/i;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    const/4 v7, 0x1

    if-ltz v6, :cond_0

    move-object/from16 v6, p2

    iput-wide v2, v6, Lcom/google/android/exoplayer2/extractor/t;->a:J

    return v7

    :cond_0
    const-wide/16 v8, -0x1

    cmp-long v6, v2, v8

    if-gez v6, :cond_1

    const-wide/16 v10, 0x2

    add-long/2addr v2, v10

    neg-long v2, v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/extractor/b/k;->c(J)V

    :cond_1
    iget-boolean v2, v0, Lcom/google/android/exoplayer2/extractor/b/k;->l:Z

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/b/k;->d:Lcom/google/android/exoplayer2/extractor/b/h;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/extractor/b/h;->a()Lcom/google/android/exoplayer2/extractor/u;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v2, Lcom/google/android/exoplayer2/extractor/u;

    iget-object v3, v0, Lcom/google/android/exoplayer2/extractor/b/k;->c:Lcom/google/android/exoplayer2/extractor/k;

    invoke-interface {v3, v2}, Lcom/google/android/exoplayer2/extractor/k;->a(Lcom/google/android/exoplayer2/extractor/u;)V

    iput-boolean v7, v0, Lcom/google/android/exoplayer2/extractor/b/k;->l:Z

    :cond_2
    iget-wide v2, v0, Lcom/google/android/exoplayer2/extractor/b/k;->k:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_4

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/b/k;->a:Lcom/google/android/exoplayer2/extractor/b/f;

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer2/extractor/b/f;->a(Lcom/google/android/exoplayer2/extractor/i;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v1, 0x3

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/b/k;->h:I

    const/4 v1, -0x1

    return v1

    :cond_4
    :goto_0
    iput-wide v4, v0, Lcom/google/android/exoplayer2/extractor/b/k;->k:J

    iget-object v1, v0, Lcom/google/android/exoplayer2/extractor/b/k;->a:Lcom/google/android/exoplayer2/extractor/b/f;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/extractor/b/f;->b()Lcom/google/android/exoplayer2/util/t;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/extractor/b/k;->a(Lcom/google/android/exoplayer2/util/t;)J

    move-result-wide v2

    cmp-long v4, v2, v4

    if-ltz v4, :cond_5

    iget-wide v4, v0, Lcom/google/android/exoplayer2/extractor/b/k;->g:J

    add-long v6, v4, v2

    iget-wide v10, v0, Lcom/google/android/exoplayer2/extractor/b/k;->e:J

    cmp-long v6, v6, v10

    if-ltz v6, :cond_5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/exoplayer2/extractor/b/k;->a(J)J

    move-result-wide v11

    iget-object v4, v0, Lcom/google/android/exoplayer2/extractor/b/k;->b:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v5

    invoke-interface {v4, v1, v5}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/util/t;I)V

    iget-object v10, v0, Lcom/google/android/exoplayer2/extractor/b/k;->b:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    const/4 v13, 0x1

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-interface/range {v10 .. v16}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(JIIILcom/google/android/exoplayer2/extractor/TrackOutput$a;)V

    iput-wide v8, v0, Lcom/google/android/exoplayer2/extractor/b/k;->e:J

    :cond_5
    iget-wide v4, v0, Lcom/google/android/exoplayer2/extractor/b/k;->g:J

    add-long/2addr v4, v2

    iput-wide v4, v0, Lcom/google/android/exoplayer2/extractor/b/k;->g:J

    const/4 v1, 0x0

    return v1
.end method


# virtual methods
.method final a(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_11

    nop

    :goto_0
    const/4 p1, 0x0

    goto/32 :goto_b

    nop

    :goto_1
    long-to-int p2, v0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {p1, p2}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    goto/32 :goto_10

    nop

    :goto_3
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/b/k;->a(Lcom/google/android/exoplayer2/extractor/i;)I

    move-result p1

    goto/32 :goto_5

    nop

    :goto_4
    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    goto/32 :goto_6

    nop

    :goto_5
    return p1

    :goto_6
    throw p1

    :goto_7
    goto/32 :goto_d

    nop

    :goto_8
    return p1

    :goto_9
    goto/32 :goto_12

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_e

    nop

    :goto_b
    return p1

    :goto_c
    goto/32 :goto_3

    nop

    :goto_d
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/k;->f:J

    goto/32 :goto_1

    nop

    :goto_e
    const/4 v1, 0x1

    goto/32 :goto_14

    nop

    :goto_f
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/extractor/b/k;->b(Lcom/google/android/exoplayer2/extractor/i;Lcom/google/android/exoplayer2/extractor/t;)I

    move-result p1

    goto/32 :goto_8

    nop

    :goto_10
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/b/k;->h:I

    goto/32 :goto_0

    nop

    :goto_11
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/b/k;->h:I

    goto/32 :goto_a

    nop

    :goto_12
    new-instance p1, Ljava/lang/IllegalStateException;

    goto/32 :goto_4

    nop

    :goto_13
    if-ne v0, v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_15

    nop

    :goto_14
    const/4 v2, 0x2

    goto/32 :goto_13

    nop

    :goto_15
    if-eq v0, v2, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_f

    nop
.end method

.method protected a(J)J
    .locals 2

    const-wide/32 v0, 0xf4240

    mul-long/2addr p1, v0

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/b/k;->i:I

    int-to-long v0, v0

    div-long/2addr p1, v0

    return-wide p1
.end method

.method protected abstract a(Lcom/google/android/exoplayer2/util/t;)J
.end method

.method final a(JJ)V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    iput p1, p0, Lcom/google/android/exoplayer2/extractor/b/k;->h:I

    :goto_1
    goto/32 :goto_7

    nop

    :goto_2
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/k;->d:Lcom/google/android/exoplayer2/extractor/b/h;

    goto/32 :goto_3

    nop

    :goto_3
    iget-wide p2, p0, Lcom/google/android/exoplayer2/extractor/b/k;->e:J

    goto/32 :goto_6

    nop

    :goto_4
    cmp-long p1, p1, v0

    goto/32 :goto_f

    nop

    :goto_5
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/b/f;->c()V

    goto/32 :goto_9

    nop

    :goto_6
    invoke-interface {p1, p2, p3}, Lcom/google/android/exoplayer2/extractor/b/h;->a(J)V

    goto/32 :goto_11

    nop

    :goto_7
    return-void

    :goto_8
    if-nez p1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_d

    nop

    :goto_9
    const-wide/16 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_a
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/k;->a:Lcom/google/android/exoplayer2/extractor/b/f;

    goto/32 :goto_5

    nop

    :goto_b
    iget p1, p0, Lcom/google/android/exoplayer2/extractor/b/k;->h:I

    goto/32 :goto_8

    nop

    :goto_c
    xor-int/lit8 p1, p1, 0x1

    goto/32 :goto_e

    nop

    :goto_d
    invoke-virtual {p0, p3, p4}, Lcom/google/android/exoplayer2/extractor/b/k;->b(J)J

    move-result-wide p1

    goto/32 :goto_14

    nop

    :goto_e
    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/extractor/b/k;->a(Z)V

    goto/32 :goto_12

    nop

    :goto_f
    if-eqz p1, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_10

    nop

    :goto_10
    iget-boolean p1, p0, Lcom/google/android/exoplayer2/extractor/b/k;->l:Z

    goto/32 :goto_c

    nop

    :goto_11
    const/4 p1, 0x2

    goto/32 :goto_0

    nop

    :goto_12
    goto :goto_1

    :goto_13
    goto/32 :goto_b

    nop

    :goto_14
    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/b/k;->e:J

    goto/32 :goto_2

    nop
.end method

.method a(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/TrackOutput;)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    iput-object p2, p0, Lcom/google/android/exoplayer2/extractor/b/k;->b:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/extractor/b/k;->a(Z)V

    goto/32 :goto_3

    nop

    :goto_2
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/k;->c:Lcom/google/android/exoplayer2/extractor/k;

    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    const/4 p1, 0x1

    goto/32 :goto_1

    nop
.end method

.method protected a(Z)V
    .locals 4

    const-wide/16 v0, 0x0

    if-eqz p1, :cond_0

    new-instance p1, Lcom/google/android/exoplayer2/extractor/b/k$a;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/extractor/b/k$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/k;->j:Lcom/google/android/exoplayer2/extractor/b/k$a;

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/k;->f:J

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/b/k;->h:I

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/b/k;->h:I

    :goto_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/exoplayer2/extractor/b/k;->e:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/k;->g:J

    return-void
.end method

.method protected abstract a(Lcom/google/android/exoplayer2/util/t;JLcom/google/android/exoplayer2/extractor/b/k$a;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected b(J)J
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/b/k;->i:I

    int-to-long v0, v0

    mul-long/2addr v0, p1

    const-wide/32 p1, 0xf4240

    div-long/2addr v0, p1

    return-wide v0
.end method

.method protected c(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/b/k;->g:J

    return-void
.end method
