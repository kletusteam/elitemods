.class public final Lcom/google/android/exoplayer2/extractor/ts/t;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/ts/m;


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final b:Lcom/google/android/exoplayer2/util/t;

.field private final c:Lcom/google/android/exoplayer2/util/s;

.field private d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/android/exoplayer2/Format;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:J

.field private l:Z

.field private m:I

.field private n:I

.field private o:I

.field private p:Z

.field private q:J

.field private r:I

.field private s:J

.field private t:I

.field private u:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->a:Ljava/lang/String;

    new-instance p1, Lcom/google/android/exoplayer2/util/t;

    const/16 v0, 0x400

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->b:Lcom/google/android/exoplayer2/util/t;

    new-instance p1, Lcom/google/android/exoplayer2/util/s;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/google/android/exoplayer2/util/s;-><init>([B)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->c:Lcom/google/android/exoplayer2/util/s;

    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/util/s;)J
    .locals 2

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result p0

    int-to-long v0, p0

    return-wide v0
.end method

.method private a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->c:Lcom/google/android/exoplayer2/util/s;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/s;->a([B)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/util/s;I)V
    .locals 8

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/s;->d()I

    move-result v0

    and-int/lit8 v1, v0, 0x7

    if-nez v1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->b:Lcom/google/android/exoplayer2/util/t;

    shr-int/lit8 v0, v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    mul-int/lit8 v1, p2, 0x8

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Lcom/google/android/exoplayer2/util/s;->a([BII)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    :goto_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-interface {p1, v0, p2}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/util/t;I)V

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->k:J

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v5, p2

    invoke-interface/range {v1 .. v7}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(JIIILcom/google/android/exoplayer2/extractor/TrackOutput$a;)V

    iget-wide p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->k:J

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->s:J

    add-long/2addr p1, v0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->k:J

    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/util/s;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ea;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/s;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->l:Z

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/t;->f(Lcom/google/android/exoplayer2/util/s;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->l:Z

    if-nez v0, :cond_1

    return-void

    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->m:I

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->n:I

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/t;->e(Lcom/google/android/exoplayer2/util/s;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/extractor/ts/t;->a(Lcom/google/android/exoplayer2/util/s;I)V

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->p:Z

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->q:J

    long-to-int v0, v0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/s;->d(I)V

    :cond_2
    return-void

    :cond_3
    new-instance p1, Lcom/google/android/exoplayer2/ea;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/ea;-><init>()V

    throw p1

    :cond_4
    new-instance p1, Lcom/google/android/exoplayer2/ea;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/ea;-><init>()V

    throw p1
.end method

.method private c(Lcom/google/android/exoplayer2/util/s;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ea;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/s;->a()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/google/android/exoplayer2/audio/AacUtil;->a(Lcom/google/android/exoplayer2/util/s;Z)Lcom/google/android/exoplayer2/audio/AacUtil$a;

    move-result-object v1

    iget-object v2, v1, Lcom/google/android/exoplayer2/audio/AacUtil$a;->c:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->u:Ljava/lang/String;

    iget v2, v1, Lcom/google/android/exoplayer2/audio/AacUtil$a;->a:I

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->r:I

    iget v1, v1, Lcom/google/android/exoplayer2/audio/AacUtil$a;->b:I

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->t:I

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/s;->a()I

    move-result p1

    sub-int/2addr v0, p1

    return v0
.end method

.method private d(Lcom/google/android/exoplayer2/util/s;)V
    .locals 4

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->o:I

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->o:I

    if-eqz v1, :cond_4

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 v3, 0x6

    if-eq v1, v0, :cond_2

    const/4 v0, 0x4

    if-eq v1, v0, :cond_2

    const/4 v0, 0x5

    if-eq v1, v0, :cond_2

    if-eq v1, v3, :cond_1

    const/4 v0, 0x7

    if-ne v1, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/util/s;->d(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/util/s;->d(I)V

    goto :goto_1

    :cond_3
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/s;->d(I)V

    goto :goto_1

    :cond_4
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/s;->d(I)V

    :goto_1
    return-void
.end method

.method private e(Lcom/google/android/exoplayer2/util/s;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ea;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->o:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v1

    add-int/2addr v0, v1

    const/16 v2, 0xff

    if-eq v1, v2, :cond_0

    return v0

    :cond_1
    new-instance p1, Lcom/google/android/exoplayer2/ea;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/ea;-><init>()V

    throw p1
.end method

.method private f(Lcom/google/android/exoplayer2/util/s;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ea;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v1

    const/4 v2, 0x0

    if-ne v1, v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v3

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    iput v3, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->m:I

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->m:I

    if-nez v3, :cond_9

    if-ne v1, v0, :cond_1

    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/ts/t;->a(Lcom/google/android/exoplayer2/util/s;)J

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/s;->e()Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x6

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->n:I

    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v3

    const/4 v4, 0x3

    invoke-virtual {p1, v4}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v4

    if-nez v3, :cond_7

    if-nez v4, :cond_7

    const/16 v3, 0x8

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/s;->d()I

    move-result v4

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/t;->c(Lcom/google/android/exoplayer2/util/s;)I

    move-result v5

    invoke-virtual {p1, v4}, Lcom/google/android/exoplayer2/util/s;->c(I)V

    add-int/lit8 v4, v5, 0x7

    div-int/2addr v4, v3

    new-array v4, v4, [B

    invoke-virtual {p1, v4, v2, v5}, Lcom/google/android/exoplayer2/util/s;->a([BII)V

    new-instance v2, Lcom/google/android/exoplayer2/Format$a;

    invoke-direct {v2}, Lcom/google/android/exoplayer2/Format$a;-><init>()V

    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->e:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/google/android/exoplayer2/Format$a;->b(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    const-string v5, "audio/mp4a-latm"

    invoke-virtual {v2, v5}, Lcom/google/android/exoplayer2/Format$a;->e(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->u:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/google/android/exoplayer2/Format$a;->a(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    iget v5, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->t:I

    invoke-virtual {v2, v5}, Lcom/google/android/exoplayer2/Format$a;->c(I)Lcom/google/android/exoplayer2/Format$a;

    iget v5, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->r:I

    invoke-virtual {v2, v5}, Lcom/google/android/exoplayer2/Format$a;->l(I)Lcom/google/android/exoplayer2/Format$a;

    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/exoplayer2/Format$a;->a(Ljava/util/List;)Lcom/google/android/exoplayer2/Format$a;

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/exoplayer2/Format$a;->d(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/Format$a;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->f:Lcom/google/android/exoplayer2/Format;

    invoke-virtual {v2, v4}, Lcom/google/android/exoplayer2/Format;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iput-object v2, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->f:Lcom/google/android/exoplayer2/Format;

    const-wide/32 v4, 0x3d090000

    iget v6, v2, Lcom/google/android/exoplayer2/Format;->z:I

    int-to-long v6, v6

    div-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->s:J

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-interface {v4, v2}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/Format;)V

    goto :goto_1

    :cond_2
    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/ts/t;->a(Lcom/google/android/exoplayer2/util/s;)J

    move-result-wide v4

    long-to-int v2, v4

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/t;->c(Lcom/google/android/exoplayer2/util/s;)I

    move-result v4

    sub-int/2addr v2, v4

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/util/s;->d(I)V

    :cond_3
    :goto_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/t;->d(Lcom/google/android/exoplayer2/util/s;)V

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/s;->e()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->p:Z

    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->q:J

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->p:Z

    if-eqz v2, :cond_5

    if-ne v1, v0, :cond_4

    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/ts/t;->a(Lcom/google/android/exoplayer2/util/s;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->q:J

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/s;->e()Z

    move-result v0

    iget-wide v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->q:J

    shl-long/2addr v1, v3

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/util/s;->a(I)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v1, v4

    iput-wide v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->q:J

    if-nez v0, :cond_4

    :cond_5
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/s;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/util/s;->d(I)V

    :cond_6
    return-void

    :cond_7
    new-instance p1, Lcom/google/android/exoplayer2/ea;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/ea;-><init>()V

    throw p1

    :cond_8
    new-instance p1, Lcom/google/android/exoplayer2/ea;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/ea;-><init>()V

    throw p1

    :cond_9
    new-instance p1, Lcom/google/android/exoplayer2/ea;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/ea;-><init>()V

    throw p1
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->g:I

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->l:Z

    return-void
.end method

.method public a(JI)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->k:J

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;)V
    .locals 2

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->a()V

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->c()I

    move-result v0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lcom/google/android/exoplayer2/extractor/k;->a(II)Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->b()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->e:Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/util/t;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ea;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v0

    if-lez v0, :cond_7

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->g:I

    const/16 v1, 0x56

    const/4 v2, 0x1

    if-eqz v0, :cond_6

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eq v0, v2, :cond_4

    const/4 v1, 0x3

    if-eq v0, v3, :cond_2

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->i:I

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->h:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->c:Lcom/google/android/exoplayer2/util/s;

    iget-object v1, v1, Lcom/google/android/exoplayer2/util/s;->a:[B

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->h:I

    invoke-virtual {p1, v1, v2, v0}, Lcom/google/android/exoplayer2/util/t;->a([BII)V

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->h:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->h:I

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->h:I

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->i:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->c:Lcom/google/android/exoplayer2/util/s;

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/s;->c(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->c:Lcom/google/android/exoplayer2/util/s;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/ts/t;->b(Lcom/google/android/exoplayer2/util/s;)V

    iput v4, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->g:I

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_2
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->j:I

    and-int/lit16 v0, v0, -0xe1

    shl-int/lit8 v0, v0, 0x8

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result v2

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->i:I

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->i:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->b:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v2

    array-length v2, v2

    if-le v0, v2, :cond_3

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->i:I

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/ts/t;->a(I)V

    :cond_3
    iput v4, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->h:I

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->g:I

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result v0

    and-int/lit16 v2, v0, 0xe0

    const/16 v5, 0xe0

    if-ne v2, v5, :cond_5

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->j:I

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->g:I

    goto :goto_0

    :cond_5
    if-eq v0, v1, :cond_0

    iput v4, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->g:I

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result v0

    if-ne v0, v1, :cond_0

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/ts/t;->g:I

    goto/16 :goto_0

    :cond_7
    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method
