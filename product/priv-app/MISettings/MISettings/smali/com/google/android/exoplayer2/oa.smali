.class public Lcom/google/android/exoplayer2/oa;
.super Lcom/google/android/exoplayer2/E;

# interfaces
.implements Lcom/google/android/exoplayer2/K;
.implements Lcom/google/android/exoplayer2/Player$a;
.implements Lcom/google/android/exoplayer2/Player$f;
.implements Lcom/google/android/exoplayer2/Player$e;
.implements Lcom/google/android/exoplayer2/Player$d;
.implements Lcom/google/android/exoplayer2/Player$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/oa$b;,
        Lcom/google/android/exoplayer2/oa$a;
    }
.end annotation


# instance fields
.field private A:I

.field private B:Lcom/google/android/exoplayer2/decoder/e;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/google/android/exoplayer2/decoder/e;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private D:I

.field private E:Lcom/google/android/exoplayer2/audio/o;

.field private F:F

.field private G:Z

.field private H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/text/Cue;",
            ">;"
        }
    .end annotation
.end field

.field private I:Lcom/google/android/exoplayer2/video/r;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private J:Lcom/google/android/exoplayer2/video/spherical/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private K:Z

.field private L:Z

.field private M:Lcom/google/android/exoplayer2/util/v;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private N:Z

.field private O:Z

.field private P:Lcom/google/android/exoplayer2/device/DeviceInfo;

.field protected final b:[Lcom/google/android/exoplayer2/Renderer;

.field private final c:Lcom/google/android/exoplayer2/M;

.field private final d:Lcom/google/android/exoplayer2/oa$b;

.field private final e:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lcom/google/android/exoplayer2/video/t;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lcom/google/android/exoplayer2/audio/q;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lcom/google/android/exoplayer2/text/l;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lcom/google/android/exoplayer2/metadata/f;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lcom/google/android/exoplayer2/device/a;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lcom/google/android/exoplayer2/video/u;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lcom/google/android/exoplayer2/audio/s;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/google/android/exoplayer2/a/a;

.field private final m:Lcom/google/android/exoplayer2/D;

.field private final n:Lcom/google/android/exoplayer2/AudioFocusManager;

.field private final o:Lcom/google/android/exoplayer2/qa;

.field private final p:Lcom/google/android/exoplayer2/ta;

.field private final q:Lcom/google/android/exoplayer2/ua;

.field private r:Lcom/google/android/exoplayer2/Format;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/google/android/exoplayer2/Format;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/google/android/exoplayer2/video/q;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private u:Landroid/view/Surface;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private v:Z

.field private w:I

.field private x:Landroid/view/SurfaceHolder;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private y:Landroid/view/TextureView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private z:I


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/exoplayer2/la;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/source/B;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/upstream/g;Lcom/google/android/exoplayer2/a/a;ZLcom/google/android/exoplayer2/util/e;Landroid/os/Looper;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lcom/google/android/exoplayer2/oa$a;

    invoke-direct {v0, p1, p2}, Lcom/google/android/exoplayer2/oa$a;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer2/la;)V

    invoke-virtual {v0, p3}, Lcom/google/android/exoplayer2/oa$a;->a(Lcom/google/android/exoplayer2/trackselection/o;)Lcom/google/android/exoplayer2/oa$a;

    invoke-virtual {v0, p4}, Lcom/google/android/exoplayer2/oa$a;->a(Lcom/google/android/exoplayer2/source/B;)Lcom/google/android/exoplayer2/oa$a;

    invoke-virtual {v0, p5}, Lcom/google/android/exoplayer2/oa$a;->a(Lcom/google/android/exoplayer2/U;)Lcom/google/android/exoplayer2/oa$a;

    invoke-virtual {v0, p6}, Lcom/google/android/exoplayer2/oa$a;->a(Lcom/google/android/exoplayer2/upstream/g;)Lcom/google/android/exoplayer2/oa$a;

    invoke-virtual {v0, p7}, Lcom/google/android/exoplayer2/oa$a;->a(Lcom/google/android/exoplayer2/a/a;)Lcom/google/android/exoplayer2/oa$a;

    invoke-virtual {v0, p8}, Lcom/google/android/exoplayer2/oa$a;->a(Z)Lcom/google/android/exoplayer2/oa$a;

    invoke-virtual {v0, p9}, Lcom/google/android/exoplayer2/oa$a;->a(Lcom/google/android/exoplayer2/util/e;)Lcom/google/android/exoplayer2/oa$a;

    invoke-virtual {v0, p10}, Lcom/google/android/exoplayer2/oa$a;->a(Landroid/os/Looper;)Lcom/google/android/exoplayer2/oa$a;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/oa;-><init>(Lcom/google/android/exoplayer2/oa$a;)V

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/exoplayer2/oa$a;)V
    .locals 17

    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/E;-><init>()V

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->a(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/a/a;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->l:Lcom/google/android/exoplayer2/a/a;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->b(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/util/v;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->M:Lcom/google/android/exoplayer2/util/v;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->m(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/audio/o;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->E:Lcom/google/android/exoplayer2/audio/o;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->o(Lcom/google/android/exoplayer2/oa$a;)I

    move-result v1

    iput v1, v0, Lcom/google/android/exoplayer2/oa;->w:I

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->p(Lcom/google/android/exoplayer2/oa$a;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/oa;->G:Z

    new-instance v1, Lcom/google/android/exoplayer2/oa$b;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/exoplayer2/oa$b;-><init>(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/na;)V

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->d:Lcom/google/android/exoplayer2/oa$b;

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->e:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v1, Landroid/os/Handler;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->q(Lcom/google/android/exoplayer2/oa$a;)Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->r(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/la;

    move-result-object v3

    iget-object v8, v0, Lcom/google/android/exoplayer2/oa;->d:Lcom/google/android/exoplayer2/oa$b;

    move-object v4, v1

    move-object v5, v8

    move-object v6, v8

    move-object v7, v8

    invoke-interface/range {v3 .. v8}, Lcom/google/android/exoplayer2/la;->a(Landroid/os/Handler;Lcom/google/android/exoplayer2/video/u;Lcom/google/android/exoplayer2/audio/s;Lcom/google/android/exoplayer2/text/l;Lcom/google/android/exoplayer2/metadata/f;)[Lcom/google/android/exoplayer2/Renderer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/exoplayer2/oa;->b:[Lcom/google/android/exoplayer2/Renderer;

    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, v0, Lcom/google/android/exoplayer2/oa;->F:F

    const/4 v3, 0x0

    iput v3, v0, Lcom/google/android/exoplayer2/oa;->D:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    iput-object v4, v0, Lcom/google/android/exoplayer2/oa;->H:Ljava/util/List;

    new-instance v4, Lcom/google/android/exoplayer2/M;

    iget-object v6, v0, Lcom/google/android/exoplayer2/oa;->b:[Lcom/google/android/exoplayer2/Renderer;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->s(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/trackselection/o;

    move-result-object v7

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->t(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/source/B;

    move-result-object v8

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->c(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/U;

    move-result-object v9

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->d(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/upstream/g;

    move-result-object v10

    iget-object v11, v0, Lcom/google/android/exoplayer2/oa;->l:Lcom/google/android/exoplayer2/a/a;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->e(Lcom/google/android/exoplayer2/oa$a;)Z

    move-result v12

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->f(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/ma;

    move-result-object v13

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->g(Lcom/google/android/exoplayer2/oa$a;)Z

    move-result v14

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->h(Lcom/google/android/exoplayer2/oa$a;)Lcom/google/android/exoplayer2/util/e;

    move-result-object v15

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->q(Lcom/google/android/exoplayer2/oa$a;)Landroid/os/Looper;

    move-result-object v16

    move-object v5, v4

    invoke-direct/range {v5 .. v16}, Lcom/google/android/exoplayer2/M;-><init>([Lcom/google/android/exoplayer2/Renderer;Lcom/google/android/exoplayer2/trackselection/o;Lcom/google/android/exoplayer2/source/B;Lcom/google/android/exoplayer2/U;Lcom/google/android/exoplayer2/upstream/g;Lcom/google/android/exoplayer2/a/a;ZLcom/google/android/exoplayer2/ma;ZLcom/google/android/exoplayer2/util/e;Landroid/os/Looper;)V

    iput-object v4, v0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    iget-object v4, v0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    iget-object v5, v0, Lcom/google/android/exoplayer2/oa;->d:Lcom/google/android/exoplayer2/oa$b;

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/Player$c;)V

    iget-object v4, v0, Lcom/google/android/exoplayer2/oa;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    iget-object v5, v0, Lcom/google/android/exoplayer2/oa;->l:Lcom/google/android/exoplayer2/a/a;

    invoke-virtual {v4, v5}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    iget-object v4, v0, Lcom/google/android/exoplayer2/oa;->e:Ljava/util/concurrent/CopyOnWriteArraySet;

    iget-object v5, v0, Lcom/google/android/exoplayer2/oa;->l:Lcom/google/android/exoplayer2/a/a;

    invoke-virtual {v4, v5}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    iget-object v4, v0, Lcom/google/android/exoplayer2/oa;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    iget-object v5, v0, Lcom/google/android/exoplayer2/oa;->l:Lcom/google/android/exoplayer2/a/a;

    invoke-virtual {v4, v5}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    iget-object v4, v0, Lcom/google/android/exoplayer2/oa;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    iget-object v5, v0, Lcom/google/android/exoplayer2/oa;->l:Lcom/google/android/exoplayer2/a/a;

    invoke-virtual {v4, v5}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    iget-object v4, v0, Lcom/google/android/exoplayer2/oa;->l:Lcom/google/android/exoplayer2/a/a;

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/metadata/f;)V

    new-instance v4, Lcom/google/android/exoplayer2/D;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->i(Lcom/google/android/exoplayer2/oa$a;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, v0, Lcom/google/android/exoplayer2/oa;->d:Lcom/google/android/exoplayer2/oa$b;

    invoke-direct {v4, v5, v1, v6}, Lcom/google/android/exoplayer2/D;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/exoplayer2/D$b;)V

    iput-object v4, v0, Lcom/google/android/exoplayer2/oa;->m:Lcom/google/android/exoplayer2/D;

    iget-object v4, v0, Lcom/google/android/exoplayer2/oa;->m:Lcom/google/android/exoplayer2/D;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->j(Lcom/google/android/exoplayer2/oa$a;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/D;->a(Z)V

    new-instance v4, Lcom/google/android/exoplayer2/AudioFocusManager;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->i(Lcom/google/android/exoplayer2/oa$a;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, v0, Lcom/google/android/exoplayer2/oa;->d:Lcom/google/android/exoplayer2/oa$b;

    invoke-direct {v4, v5, v1, v6}, Lcom/google/android/exoplayer2/AudioFocusManager;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/exoplayer2/AudioFocusManager$b;)V

    iput-object v4, v0, Lcom/google/android/exoplayer2/oa;->n:Lcom/google/android/exoplayer2/AudioFocusManager;

    iget-object v4, v0, Lcom/google/android/exoplayer2/oa;->n:Lcom/google/android/exoplayer2/AudioFocusManager;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->k(Lcom/google/android/exoplayer2/oa$a;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v2, v0, Lcom/google/android/exoplayer2/oa;->E:Lcom/google/android/exoplayer2/audio/o;

    :cond_0
    invoke-virtual {v4, v2}, Lcom/google/android/exoplayer2/AudioFocusManager;->a(Lcom/google/android/exoplayer2/audio/o;)V

    new-instance v2, Lcom/google/android/exoplayer2/qa;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->i(Lcom/google/android/exoplayer2/oa$a;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/exoplayer2/oa;->d:Lcom/google/android/exoplayer2/oa$b;

    invoke-direct {v2, v4, v1, v5}, Lcom/google/android/exoplayer2/qa;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/exoplayer2/qa$a;)V

    iput-object v2, v0, Lcom/google/android/exoplayer2/oa;->o:Lcom/google/android/exoplayer2/qa;

    iget-object v1, v0, Lcom/google/android/exoplayer2/oa;->o:Lcom/google/android/exoplayer2/qa;

    iget-object v2, v0, Lcom/google/android/exoplayer2/oa;->E:Lcom/google/android/exoplayer2/audio/o;

    iget v2, v2, Lcom/google/android/exoplayer2/audio/o;->d:I

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/E;->e(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/qa;->a(I)V

    new-instance v1, Lcom/google/android/exoplayer2/ta;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->i(Lcom/google/android/exoplayer2/oa$a;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ta;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->p:Lcom/google/android/exoplayer2/ta;

    iget-object v1, v0, Lcom/google/android/exoplayer2/oa;->p:Lcom/google/android/exoplayer2/ta;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->l(Lcom/google/android/exoplayer2/oa$a;)I

    move-result v2

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    move v2, v4

    goto :goto_0

    :cond_1
    move v2, v3

    :goto_0
    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/ta;->a(Z)V

    new-instance v1, Lcom/google/android/exoplayer2/ua;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->i(Lcom/google/android/exoplayer2/oa$a;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ua;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->q:Lcom/google/android/exoplayer2/ua;

    iget-object v1, v0, Lcom/google/android/exoplayer2/oa;->q:Lcom/google/android/exoplayer2/ua;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->l(Lcom/google/android/exoplayer2/oa$a;)I

    move-result v2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_2

    move v3, v4

    :cond_2
    invoke-virtual {v1, v3}, Lcom/google/android/exoplayer2/ua;->a(Z)V

    iget-object v1, v0, Lcom/google/android/exoplayer2/oa;->o:Lcom/google/android/exoplayer2/qa;

    invoke-static {v1}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/qa;)Lcom/google/android/exoplayer2/device/DeviceInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/exoplayer2/oa;->P:Lcom/google/android/exoplayer2/device/DeviceInfo;

    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer2/oa$a;->n(Lcom/google/android/exoplayer2/oa$a;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/M;->z()V

    :cond_3
    const/4 v1, 0x3

    iget-object v2, v0, Lcom/google/android/exoplayer2/oa;->E:Lcom/google/android/exoplayer2/audio/o;

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/exoplayer2/oa;->a(IILjava/lang/Object;)V

    const/4 v1, 0x4

    iget v2, v0, Lcom/google/android/exoplayer2/oa;->w:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v5, v1, v2}, Lcom/google/android/exoplayer2/oa;->a(IILjava/lang/Object;)V

    const/16 v1, 0x65

    iget-boolean v2, v0, Lcom/google/android/exoplayer2/oa;->G:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/exoplayer2/oa;->a(IILjava/lang/Object;)V

    return-void
.end method

.method private D()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/audio/q;

    iget-object v2, p0, Lcom/google/android/exoplayer2/oa;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, Lcom/google/android/exoplayer2/oa;->D:I

    invoke-interface {v1, v2}, Lcom/google/android/exoplayer2/audio/q;->d(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/audio/s;

    iget v2, p0, Lcom/google/android/exoplayer2/oa;->D:I

    invoke-interface {v1, v2}, Lcom/google/android/exoplayer2/audio/s;->d(I)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private E()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/audio/q;

    iget-object v2, p0, Lcom/google/android/exoplayer2/oa;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/oa;->G:Z

    invoke-interface {v1, v2}, Lcom/google/android/exoplayer2/audio/q;->f(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/audio/s;

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/oa;->G:Z

    invoke-interface {v1, v2}, Lcom/google/android/exoplayer2/audio/s;->f(Z)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private F()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->y:Landroid/view/TextureView;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/exoplayer2/oa;->d:Lcom/google/android/exoplayer2/oa$b;

    if-eq v0, v2, :cond_0

    const-string v0, "SimpleExoPlayer"

    const-string v2, "SurfaceTextureListener already unset or replaced."

    invoke-static {v0, v2}, Lcom/google/android/exoplayer2/util/n;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->y:Landroid/view/TextureView;

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    :goto_0
    iput-object v1, p0, Lcom/google/android/exoplayer2/oa;->y:Landroid/view/TextureView;

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->x:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/exoplayer2/oa;->d:Lcom/google/android/exoplayer2/oa$b;

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    iput-object v1, p0, Lcom/google/android/exoplayer2/oa;->x:Landroid/view/SurfaceHolder;

    :cond_2
    return-void
.end method

.method private G()V
    .locals 3

    iget v0, p0, Lcom/google/android/exoplayer2/oa;->F:F

    iget-object v1, p0, Lcom/google/android/exoplayer2/oa;->n:Lcom/google/android/exoplayer2/AudioFocusManager;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/AudioFocusManager;->a()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/exoplayer2/oa;->a(IILjava/lang/Object;)V

    return-void
.end method

.method private H()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->getPlaybackState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->p:Lcom/google/android/exoplayer2/ta;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ta;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->q:Lcom/google/android/exoplayer2/ua;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ua;->b(Z)V

    goto :goto_1

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->p:Lcom/google/android/exoplayer2/ta;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ta;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->q:Lcom/google/android/exoplayer2/ua;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ua;->b(Z)V

    :goto_1
    return-void
.end method

.method private I()V
    .locals 3

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->s()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa;->K:Z

    const-string v1, "Player is accessed on the wrong thread. See https://exoplayer.dev/issues/player-accessed-on-wrong-thread"

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa;->L:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    :goto_0
    const-string v2, "SimpleExoPlayer"

    invoke-static {v2, v1, v0}, Lcom/google/android/exoplayer2/util/n;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/oa;->L:Z

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/oa;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/oa;->D:I

    return p1
.end method

.method static synthetic a(ZI)I
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/exoplayer2/oa;->b(ZI)I

    move-result p0

    return p0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/Format;
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->r:Lcom/google/android/exoplayer2/Format;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/decoder/e;)Lcom/google/android/exoplayer2/decoder/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->B:Lcom/google/android/exoplayer2/decoder/e;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/device/DeviceInfo;)Lcom/google/android/exoplayer2/device/DeviceInfo;
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->P:Lcom/google/android/exoplayer2/device/DeviceInfo;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/qa;)Lcom/google/android/exoplayer2/device/DeviceInfo;
    .locals 0

    invoke-static {p0}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/qa;)Lcom/google/android/exoplayer2/device/DeviceInfo;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/oa;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->H:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    return-object p0
.end method

.method private a(II)V
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer2/oa;->z:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/oa;->A:I

    if-eq p2, v0, :cond_1

    :cond_0
    iput p1, p0, Lcom/google/android/exoplayer2/oa;->z:I

    iput p2, p0, Lcom/google/android/exoplayer2/oa;->A:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->e:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/video/t;

    invoke-interface {v1, p1, p2}, Lcom/google/android/exoplayer2/video/t;->a(II)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(IILjava/lang/Object;)V
    .locals 5
    .param p3    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->b:[Lcom/google/android/exoplayer2/Renderer;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    invoke-interface {v3}, Lcom/google/android/exoplayer2/Renderer;->f()I

    move-result v4

    if-ne v4, p1, :cond_0

    iget-object v4, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v4, v3}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/ia$b;)Lcom/google/android/exoplayer2/ia;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/google/android/exoplayer2/ia;->a(I)Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v3, p3}, Lcom/google/android/exoplayer2/ia;->a(Ljava/lang/Object;)Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/ia;->k()Lcom/google/android/exoplayer2/ia;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Landroid/view/Surface;Z)V
    .locals 7
    .param p1    # Landroid/view/Surface;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/exoplayer2/oa;->b:[Lcom/google/android/exoplayer2/Renderer;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    invoke-interface {v4}, Lcom/google/android/exoplayer2/Renderer;->f()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v5, v4}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/ia$b;)Lcom/google/android/exoplayer2/ia;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/ia;->a(I)Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v4, p1}, Lcom/google/android/exoplayer2/ia;->a(Ljava/lang/Object;)Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/ia;->k()Lcom/google/android/exoplayer2/ia;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/oa;->u:Landroid/view/Surface;

    if-eqz v1, :cond_3

    if-eq v1, p1, :cond_3

    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/ia;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ia;->a()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa;->v:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->u:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_3
    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->u:Landroid/view/Surface;

    iput-boolean p2, p0, Lcom/google/android/exoplayer2/oa;->v:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/oa;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/oa;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/oa;Landroid/view/Surface;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/oa;->a(Landroid/view/Surface;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/oa;ZII)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/oa;->a(ZII)V

    return-void
.end method

.method private a(ZII)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    const/4 p1, -0x1

    if-eq p2, p1, :cond_0

    move p1, v1

    goto :goto_0

    :cond_0
    move p1, v0

    :goto_0
    if-eqz p1, :cond_1

    if-eq p2, v1, :cond_1

    move v0, v1

    :cond_1
    iget-object p2, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {p2, p1, v0, p3}, Lcom/google/android/exoplayer2/M;->a(ZII)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/oa;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/oa;->G:Z

    return p1
.end method

.method private static b(ZI)I
    .locals 1

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    :cond_0
    return v0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/Format;
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->s:Lcom/google/android/exoplayer2/Format;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/oa;Lcom/google/android/exoplayer2/decoder/e;)Lcom/google/android/exoplayer2/decoder/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->C:Lcom/google/android/exoplayer2/decoder/e;

    return-object p1
.end method

.method private static b(Lcom/google/android/exoplayer2/qa;)Lcom/google/android/exoplayer2/device/DeviceInfo;
    .locals 3

    new-instance v0, Lcom/google/android/exoplayer2/device/DeviceInfo;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/qa;->b()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/qa;->a()I

    move-result p0

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1, p0}, Lcom/google/android/exoplayer2/device/DeviceInfo;-><init>(III)V

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa;->e:Ljava/util/concurrent/CopyOnWriteArraySet;

    return-object p0
.end method

.method private b(Lcom/google/android/exoplayer2/video/q;)V
    .locals 2
    .param p1    # Lcom/google/android/exoplayer2/video/q;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x2

    const/16 v1, 0x8

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/exoplayer2/oa;->a(IILjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->t:Lcom/google/android/exoplayer2/video/q;

    return-void
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/oa;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/oa;->N:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/oa;)Landroid/view/Surface;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa;->u:Landroid/view/Surface;

    return-object p0
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    return-object p0
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/oa;)I
    .locals 0

    iget p0, p0, Lcom/google/android/exoplayer2/oa;->D:I

    return p0
.end method

.method static synthetic f(Lcom/google/android/exoplayer2/oa;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->D()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/exoplayer2/oa;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/google/android/exoplayer2/oa;->G:Z

    return p0
.end method

.method static synthetic h(Lcom/google/android/exoplayer2/oa;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->E()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    return-object p0
.end method

.method static synthetic j(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    return-object p0
.end method

.method static synthetic k(Lcom/google/android/exoplayer2/oa;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->G()V

    return-void
.end method

.method static synthetic l(Lcom/google/android/exoplayer2/oa;)Lcom/google/android/exoplayer2/qa;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa;->o:Lcom/google/android/exoplayer2/qa;

    return-object p0
.end method

.method static synthetic m(Lcom/google/android/exoplayer2/oa;)Lcom/google/android/exoplayer2/device/DeviceInfo;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa;->P:Lcom/google/android/exoplayer2/device/DeviceInfo;

    return-object p0
.end method

.method static synthetic n(Lcom/google/android/exoplayer2/oa;)Ljava/util/concurrent/CopyOnWriteArraySet;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    return-object p0
.end method

.method static synthetic o(Lcom/google/android/exoplayer2/oa;)Lcom/google/android/exoplayer2/util/v;
    .locals 0

    iget-object p0, p0, Lcom/google/android/exoplayer2/oa;->M:Lcom/google/android/exoplayer2/util/v;

    return-object p0
.end method

.method static synthetic p(Lcom/google/android/exoplayer2/oa;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/google/android/exoplayer2/oa;->N:Z

    return p0
.end method

.method static synthetic q(Lcom/google/android/exoplayer2/oa;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->H()V

    return-void
.end method


# virtual methods
.method public A()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->F()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/exoplayer2/oa;->a(Landroid/view/Surface;Z)V

    invoke-direct {p0, v0, v0}, Lcom/google/android/exoplayer2/oa;->a(II)V

    return-void
.end method

.method public B()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->d()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/oa;->n:Lcom/google/android/exoplayer2/AudioFocusManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/AudioFocusManager;->a(ZI)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/oa;->b(ZI)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/exoplayer2/oa;->a(ZII)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->A()V

    return-void
.end method

.method public C()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->m:Lcom/google/android/exoplayer2/D;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/D;->a(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->o:Lcom/google/android/exoplayer2/qa;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/qa;->c()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->p:Lcom/google/android/exoplayer2/ta;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ta;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->q:Lcom/google/android/exoplayer2/ua;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ua;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->n:Lcom/google/android/exoplayer2/AudioFocusManager;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/AudioFocusManager;->b()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->B()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->F()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->u:Landroid/view/Surface;

    if-eqz v0, :cond_1

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/oa;->v:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/oa;->u:Landroid/view/Surface;

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa;->N:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->M:Lcom/google/android/exoplayer2/util/v;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/util/v;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/v;->b(I)V

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/oa;->N:Z

    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/oa;->H:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/oa;->O:Z

    return-void
.end method

.method public a(I)I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/M;->a(I)I

    move-result p1

    return p1
.end method

.method public a()Lcom/google/android/exoplayer2/ga;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->a()Lcom/google/android/exoplayer2/ga;

    move-result-object v0

    return-object v0
.end method

.method public a(F)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p1, v0, v1}, Lcom/google/android/exoplayer2/util/E;->a(FFF)F

    move-result p1

    iget v0, p0, Lcom/google/android/exoplayer2/oa;->F:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/exoplayer2/oa;->F:F

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->G()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/audio/q;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/audio/q;->a(F)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(IJ)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->l:Lcom/google/android/exoplayer2/a/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/a/a;->c()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/M;->a(IJ)V

    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 1
    .param p1    # Landroid/view/Surface;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->F()V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->z()V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/oa;->a(Landroid/view/Surface;Z)V

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    :goto_0
    invoke-direct {p0, v0, v0}, Lcom/google/android/exoplayer2/oa;->a(II)V

    return-void
.end method

.method public a(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->x:Landroid/view/SurfaceHolder;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/oa;->b(Landroid/view/SurfaceHolder;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/SurfaceView;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceView;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object p1

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/oa;->b(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public a(Landroid/view/TextureView;)V
    .locals 1
    .param p1    # Landroid/view/TextureView;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->y:Landroid/view/TextureView;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/oa;->b(Landroid/view/TextureView;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/Player$c;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/audio/o;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/audio/o;Z)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/audio/o;Z)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/oa;->O:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->E:Lcom/google/android/exoplayer2/audio/o;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/util/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->E:Lcom/google/android/exoplayer2/audio/o;

    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/exoplayer2/oa;->a(IILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->o:Lcom/google/android/exoplayer2/qa;

    iget v1, p1, Lcom/google/android/exoplayer2/audio/o;->d:I

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/E;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/qa;->a(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/audio/q;

    invoke-interface {v1, p1}, Lcom/google/android/exoplayer2/audio/q;->a(Lcom/google/android/exoplayer2/audio/o;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->n:Lcom/google/android/exoplayer2/AudioFocusManager;

    if-eqz p2, :cond_2

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/AudioFocusManager;->a(Lcom/google/android/exoplayer2/audio/o;)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->d()Z

    move-result p1

    iget-object p2, p0, Lcom/google/android/exoplayer2/oa;->n:Lcom/google/android/exoplayer2/AudioFocusManager;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->getPlaybackState()I

    move-result v0

    invoke-virtual {p2, p1, v0}, Lcom/google/android/exoplayer2/AudioFocusManager;->a(ZI)I

    move-result p2

    invoke-static {p1, p2}, Lcom/google/android/exoplayer2/oa;->b(ZI)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/exoplayer2/oa;->a(ZII)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/ga;)V
    .locals 1
    .param p1    # Lcom/google/android/exoplayer2/ga;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/M;->a(Lcom/google/android/exoplayer2/ga;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/metadata/f;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/y;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v0}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/source/y;ZZ)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/y;ZZ)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    if-eqz p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    const/4 p2, -0x1

    :goto_0
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/exoplayer2/oa;->a(Ljava/util/List;IJ)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->B()V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/text/l;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/video/q;)V
    .locals 0
    .param p1    # Lcom/google/android/exoplayer2/video/q;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->A()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/video/q;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/video/r;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->I:Lcom/google/android/exoplayer2/video/r;

    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/exoplayer2/oa;->a(IILjava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/video/spherical/a;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->J:Lcom/google/android/exoplayer2/video/spherical/a;

    const/4 v0, 0x5

    const/4 v1, 0x7

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/exoplayer2/oa;->a(IILjava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/video/t;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->e:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Ljava/util/List;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/source/y;",
            ">;IJ)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->l:Lcom/google/android/exoplayer2/a/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/a/a;->d()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/M;->a(Ljava/util/List;IJ)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/M;->a(Z)V

    return-void
.end method

.method public b(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/E;->c(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/E;->a(I)I

    move-result p1

    new-instance v1, Lcom/google/android/exoplayer2/audio/o$a;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/audio/o$a;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/audio/o$a;->b(I)Lcom/google/android/exoplayer2/audio/o$a;

    invoke-virtual {v1, p1}, Lcom/google/android/exoplayer2/audio/o$a;->a(I)Lcom/google/android/exoplayer2/audio/o$a;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/audio/o$a;->a()Lcom/google/android/exoplayer2/audio/o;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/oa;->a(Lcom/google/android/exoplayer2/audio/o;)V

    return-void
.end method

.method public b(Landroid/view/Surface;)V
    .locals 1
    .param p1    # Landroid/view/Surface;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->u:Landroid/view/Surface;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->A()V

    :cond_0
    return-void
.end method

.method public b(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1    # Landroid/view/SurfaceHolder;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->F()V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->z()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->x:Landroid/view/SurfaceHolder;

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-nez p1, :cond_1

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/oa;->a(Landroid/view/Surface;Z)V

    invoke-direct {p0, v1, v1}, Lcom/google/android/exoplayer2/oa;->a(II)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/oa;->d:Lcom/google/android/exoplayer2/oa$b;

    invoke-interface {p1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/view/Surface;->isValid()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, v2, v1}, Lcom/google/android/exoplayer2/oa;->a(Landroid/view/Surface;Z)V

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/google/android/exoplayer2/oa;->a(II)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/oa;->a(Landroid/view/Surface;Z)V

    invoke-direct {p0, v1, v1}, Lcom/google/android/exoplayer2/oa;->a(II)V

    :goto_0
    return-void
.end method

.method public b(Landroid/view/SurfaceView;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceView;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object p1

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/oa;->a(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public b(Landroid/view/TextureView;)V
    .locals 5
    .param p1    # Landroid/view/TextureView;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->F()V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->z()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/oa;->y:Landroid/view/TextureView;

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-nez p1, :cond_1

    invoke-direct {p0, v1, v0}, Lcom/google/android/exoplayer2/oa;->a(Landroid/view/Surface;Z)V

    invoke-direct {p0, v2, v2}, Lcom/google/android/exoplayer2/oa;->a(II)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v3

    if-eqz v3, :cond_2

    const-string v3, "SimpleExoPlayer"

    const-string v4, "Replacing existing SurfaceTextureListener."

    invoke-static {v3, v4}, Lcom/google/android/exoplayer2/util/n;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/exoplayer2/oa;->d:Lcom/google/android/exoplayer2/oa$b;

    invoke-virtual {p1, v3}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    invoke-virtual {p1}, Landroid/view/TextureView;->isAvailable()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v3

    goto :goto_0

    :cond_3
    move-object v3, v1

    :goto_0
    if-nez v3, :cond_4

    invoke-direct {p0, v1, v0}, Lcom/google/android/exoplayer2/oa;->a(Landroid/view/Surface;Z)V

    invoke-direct {p0, v2, v2}, Lcom/google/android/exoplayer2/oa;->a(II)V

    goto :goto_1

    :cond_4
    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, v3}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-direct {p0, v1, v0}, Lcom/google/android/exoplayer2/oa;->a(Landroid/view/Surface;Z)V

    invoke-virtual {p1}, Landroid/view/TextureView;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/TextureView;->getHeight()I

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/google/android/exoplayer2/oa;->a(II)V

    :goto_1
    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/Player$c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/M;->b(Lcom/google/android/exoplayer2/Player$c;)V

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/text/l;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/video/r;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->I:Lcom/google/android/exoplayer2/video/r;

    if-eq v0, p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x2

    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/exoplayer2/oa;->a(IILjava/lang/Object;)V

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/video/spherical/a;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->J:Lcom/google/android/exoplayer2/video/spherical/a;

    if-eq v0, p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x5

    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/exoplayer2/oa;->a(IILjava/lang/Object;)V

    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/video/t;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->e:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->n:Lcom/google/android/exoplayer2/AudioFocusManager;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/oa;->getPlaybackState()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/exoplayer2/AudioFocusManager;->a(ZI)I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/oa;->b(ZI)I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/exoplayer2/oa;->a(ZII)V

    return-void
.end method

.method public b()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->b()Z

    move-result v0

    return v0
.end method

.method public c()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public d()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->d()Z

    move-result v0

    return v0
.end method

.method public e()Lcom/google/android/exoplayer2/trackselection/o;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->e()Lcom/google/android/exoplayer2/trackselection/o;

    move-result-object v0

    return-object v0
.end method

.method public f()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->f()I

    move-result v0

    return v0
.end method

.method public getCurrentPosition()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->getCurrentPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public getDuration()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->getDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getPlaybackState()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->getPlaybackState()I

    move-result v0

    return v0
.end method

.method public getRepeatMode()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->getRepeatMode()I

    move-result v0

    return v0
.end method

.method public h()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->h()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->i()I

    move-result v0

    return v0
.end method

.method public j()Lcom/google/android/exoplayer2/ExoPlaybackException;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->j()Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object v0

    return-object v0
.end method

.method public k()Lcom/google/android/exoplayer2/Player$f;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    return-object p0
.end method

.method public l()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->l()J

    move-result-wide v0

    return-wide v0
.end method

.method public n()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->n()I

    move-result v0

    return v0
.end method

.method public p()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->p()I

    move-result v0

    return v0
.end method

.method public q()Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->q()Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v0

    return-object v0
.end method

.method public r()Lcom/google/android/exoplayer2/sa;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->r()Lcom/google/android/exoplayer2/sa;

    move-result-object v0

    return-object v0
.end method

.method public s()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->s()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method public setRepeatMode(I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/M;->setRepeatMode(I)V

    return-void
.end method

.method public t()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->t()Z

    move-result v0

    return v0
.end method

.method public u()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->u()J

    move-result-wide v0

    return-wide v0
.end method

.method public v()Lcom/google/android/exoplayer2/trackselection/m;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->c:Lcom/google/android/exoplayer2/M;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/M;->v()Lcom/google/android/exoplayer2/trackselection/m;

    move-result-object v0

    return-object v0
.end method

.method public w()Lcom/google/android/exoplayer2/Player$e;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    return-object p0
.end method

.method public x()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/text/Cue;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/oa;->H:Ljava/util/List;

    return-object v0
.end method

.method public z()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer2/oa;->I()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/oa;->b(Lcom/google/android/exoplayer2/video/q;)V

    return-void
.end method
