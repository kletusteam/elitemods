.class final Lcom/google/android/exoplayer2/fa;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/google/android/exoplayer2/source/y$a;


# instance fields
.field public final b:Lcom/google/android/exoplayer2/sa;

.field public final c:Lcom/google/android/exoplayer2/source/y$a;

.field public final d:J

.field public final e:I

.field public final f:Lcom/google/android/exoplayer2/ExoPlaybackException;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final g:Z

.field public final h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

.field public final i:Lcom/google/android/exoplayer2/trackselection/p;

.field public final j:Lcom/google/android/exoplayer2/source/y$a;

.field public final k:Z

.field public final l:I

.field public final m:Lcom/google/android/exoplayer2/ga;

.field public final n:Z

.field public volatile o:J

.field public volatile p:J

.field public volatile q:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/exoplayer2/source/y$a;

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/source/y$a;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/google/android/exoplayer2/fa;->a:Lcom/google/android/exoplayer2/source/y$a;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V
    .locals 3
    .param p6    # Lcom/google/android/exoplayer2/ExoPlaybackException;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    iput-object v1, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    move-object v1, p2

    iput-object v1, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    move-wide v1, p3

    iput-wide v1, v0, Lcom/google/android/exoplayer2/fa;->d:J

    move v1, p5

    iput v1, v0, Lcom/google/android/exoplayer2/fa;->e:I

    move-object v1, p6

    iput-object v1, v0, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    move v1, p7

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->g:Z

    move-object v1, p8

    iput-object v1, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-object v1, p9

    iput-object v1, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    move-object v1, p10

    iput-object v1, v0, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    move v1, p11

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    move v1, p12

    iput v1, v0, Lcom/google/android/exoplayer2/fa;->l:I

    move-object/from16 v1, p13

    iput-object v1, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    move-wide/from16 v1, p14

    iput-wide v1, v0, Lcom/google/android/exoplayer2/fa;->o:J

    move-wide/from16 v1, p16

    iput-wide v1, v0, Lcom/google/android/exoplayer2/fa;->p:J

    move-wide/from16 v1, p18

    iput-wide v1, v0, Lcom/google/android/exoplayer2/fa;->q:J

    move/from16 v1, p20

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->n:Z

    return-void
.end method

.method public static a(Lcom/google/android/exoplayer2/trackselection/p;)Lcom/google/android/exoplayer2/fa;
    .locals 22

    move-object/from16 v9, p0

    new-instance v21, Lcom/google/android/exoplayer2/fa;

    move-object/from16 v0, v21

    sget-object v1, Lcom/google/android/exoplayer2/sa;->a:Lcom/google/android/exoplayer2/sa;

    sget-object v2, Lcom/google/android/exoplayer2/fa;->a:Lcom/google/android/exoplayer2/source/y$a;

    sget-object v8, Lcom/google/android/exoplayer2/source/TrackGroupArray;->a:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    sget-object v10, Lcom/google/android/exoplayer2/fa;->a:Lcom/google/android/exoplayer2/source/y$a;

    sget-object v13, Lcom/google/android/exoplayer2/ga;->a:Lcom/google/android/exoplayer2/ga;

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    const-wide/16 v18, 0x0

    const/16 v20, 0x0

    invoke-direct/range {v0 .. v20}, Lcom/google/android/exoplayer2/fa;-><init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V

    return-object v21
.end method

.method public static a()Lcom/google/android/exoplayer2/source/y$a;
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer2/fa;->a:Lcom/google/android/exoplayer2/source/y$a;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/google/android/exoplayer2/fa;
    .locals 24
    .annotation build Landroidx/annotation/CheckResult;
    .end annotation

    move-object/from16 v0, p0

    move/from16 v6, p1

    new-instance v22, Lcom/google/android/exoplayer2/fa;

    move-object/from16 v1, v22

    iget-object v2, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v3, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/fa;->d:J

    iget-object v7, v0, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-boolean v8, v0, Lcom/google/android/exoplayer2/fa;->g:Z

    iget-object v9, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v10, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object v11, v0, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-boolean v12, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    iget v13, v0, Lcom/google/android/exoplayer2/fa;->l:I

    iget-object v14, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    move-object/from16 p1, v1

    move-object/from16 v23, v2

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->o:J

    move-wide v15, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->p:J

    move-wide/from16 v17, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->q:J

    move-wide/from16 v19, v1

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->n:Z

    move/from16 v21, v1

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-direct/range {v1 .. v21}, Lcom/google/android/exoplayer2/fa;-><init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V

    return-object v22
.end method

.method public a(Lcom/google/android/exoplayer2/ExoPlaybackException;)Lcom/google/android/exoplayer2/fa;
    .locals 24
    .param p1    # Lcom/google/android/exoplayer2/ExoPlaybackException;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/CheckResult;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    new-instance v22, Lcom/google/android/exoplayer2/fa;

    move-object/from16 v1, v22

    iget-object v2, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v3, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/fa;->d:J

    iget v6, v0, Lcom/google/android/exoplayer2/fa;->e:I

    iget-boolean v8, v0, Lcom/google/android/exoplayer2/fa;->g:Z

    iget-object v9, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v10, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object v11, v0, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-boolean v12, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    iget v13, v0, Lcom/google/android/exoplayer2/fa;->l:I

    iget-object v14, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    move-object/from16 p1, v1

    move-object/from16 v23, v2

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->o:J

    move-wide v15, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->p:J

    move-wide/from16 v17, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->q:J

    move-wide/from16 v19, v1

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->n:Z

    move/from16 v21, v1

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-direct/range {v1 .. v21}, Lcom/google/android/exoplayer2/fa;-><init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V

    return-object v22
.end method

.method public a(Lcom/google/android/exoplayer2/ga;)Lcom/google/android/exoplayer2/fa;
    .locals 24
    .annotation build Landroidx/annotation/CheckResult;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v14, p1

    new-instance v22, Lcom/google/android/exoplayer2/fa;

    move-object/from16 v1, v22

    iget-object v2, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v3, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/fa;->d:J

    iget v6, v0, Lcom/google/android/exoplayer2/fa;->e:I

    iget-object v7, v0, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-boolean v8, v0, Lcom/google/android/exoplayer2/fa;->g:Z

    iget-object v9, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v10, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object v11, v0, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-boolean v12, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    iget v13, v0, Lcom/google/android/exoplayer2/fa;->l:I

    move-object/from16 p1, v1

    move-object/from16 v23, v2

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->o:J

    move-wide v15, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->p:J

    move-wide/from16 v17, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->q:J

    move-wide/from16 v19, v1

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->n:Z

    move/from16 v21, v1

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-direct/range {v1 .. v21}, Lcom/google/android/exoplayer2/fa;-><init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V

    return-object v22
.end method

.method public a(Lcom/google/android/exoplayer2/sa;)Lcom/google/android/exoplayer2/fa;
    .locals 24
    .annotation build Landroidx/annotation/CheckResult;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    new-instance v22, Lcom/google/android/exoplayer2/fa;

    move-object/from16 v1, v22

    iget-object v3, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/fa;->d:J

    iget v6, v0, Lcom/google/android/exoplayer2/fa;->e:I

    iget-object v7, v0, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-boolean v8, v0, Lcom/google/android/exoplayer2/fa;->g:Z

    iget-object v9, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v10, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object v11, v0, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-boolean v12, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    iget v13, v0, Lcom/google/android/exoplayer2/fa;->l:I

    iget-object v14, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    move-object/from16 v23, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->o:J

    move-wide v15, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->p:J

    move-wide/from16 v17, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->q:J

    move-wide/from16 v19, v1

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->n:Z

    move/from16 v21, v1

    move-object/from16 v2, p1

    move-object/from16 v1, v23

    invoke-direct/range {v1 .. v21}, Lcom/google/android/exoplayer2/fa;-><init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V

    return-object v22
.end method

.method public a(Lcom/google/android/exoplayer2/source/y$a;)Lcom/google/android/exoplayer2/fa;
    .locals 24
    .annotation build Landroidx/annotation/CheckResult;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v11, p1

    new-instance v22, Lcom/google/android/exoplayer2/fa;

    move-object/from16 v1, v22

    iget-object v2, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v3, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/fa;->d:J

    iget v6, v0, Lcom/google/android/exoplayer2/fa;->e:I

    iget-object v7, v0, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-boolean v8, v0, Lcom/google/android/exoplayer2/fa;->g:Z

    iget-object v9, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v10, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-boolean v12, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    iget v13, v0, Lcom/google/android/exoplayer2/fa;->l:I

    iget-object v14, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    move-object/from16 p1, v1

    move-object/from16 v23, v2

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->o:J

    move-wide v15, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->p:J

    move-wide/from16 v17, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->q:J

    move-wide/from16 v19, v1

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->n:Z

    move/from16 v21, v1

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-direct/range {v1 .. v21}, Lcom/google/android/exoplayer2/fa;-><init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V

    return-object v22
.end method

.method public a(Lcom/google/android/exoplayer2/source/y$a;JJJLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;)Lcom/google/android/exoplayer2/fa;
    .locals 23
    .annotation build Landroidx/annotation/CheckResult;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v3, p1

    move-wide/from16 v19, p2

    move-wide/from16 v4, p4

    move-wide/from16 v17, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    new-instance v22, Lcom/google/android/exoplayer2/fa;

    move-object/from16 v1, v22

    iget-object v2, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget v6, v0, Lcom/google/android/exoplayer2/fa;->e:I

    iget-object v7, v0, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-boolean v8, v0, Lcom/google/android/exoplayer2/fa;->g:Z

    iget-object v11, v0, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-boolean v12, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    iget v13, v0, Lcom/google/android/exoplayer2/fa;->l:I

    iget-object v14, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    move-object/from16 p1, v1

    move-object/from16 p2, v2

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->o:J

    move-wide v15, v1

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->n:Z

    move/from16 v21, v1

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v21}, Lcom/google/android/exoplayer2/fa;-><init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V

    return-object v22
.end method

.method public a(Z)Lcom/google/android/exoplayer2/fa;
    .locals 24
    .annotation build Landroidx/annotation/CheckResult;
    .end annotation

    move-object/from16 v0, p0

    move/from16 v8, p1

    new-instance v22, Lcom/google/android/exoplayer2/fa;

    move-object/from16 v1, v22

    iget-object v2, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v3, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/fa;->d:J

    iget v6, v0, Lcom/google/android/exoplayer2/fa;->e:I

    iget-object v7, v0, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-object v9, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v10, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object v11, v0, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-boolean v12, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    iget v13, v0, Lcom/google/android/exoplayer2/fa;->l:I

    iget-object v14, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    move-object/from16 p1, v1

    move-object/from16 v23, v2

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->o:J

    move-wide v15, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->p:J

    move-wide/from16 v17, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->q:J

    move-wide/from16 v19, v1

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->n:Z

    move/from16 v21, v1

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-direct/range {v1 .. v21}, Lcom/google/android/exoplayer2/fa;-><init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V

    return-object v22
.end method

.method public a(ZI)Lcom/google/android/exoplayer2/fa;
    .locals 23
    .annotation build Landroidx/annotation/CheckResult;
    .end annotation

    move-object/from16 v0, p0

    move/from16 v12, p1

    move/from16 v13, p2

    new-instance v22, Lcom/google/android/exoplayer2/fa;

    move-object/from16 v1, v22

    iget-object v2, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v3, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/fa;->d:J

    iget v6, v0, Lcom/google/android/exoplayer2/fa;->e:I

    iget-object v7, v0, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-boolean v8, v0, Lcom/google/android/exoplayer2/fa;->g:Z

    iget-object v9, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v10, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object v11, v0, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-object v14, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    move-object/from16 p1, v1

    move-object/from16 p2, v2

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->o:J

    move-wide v15, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->p:J

    move-wide/from16 v17, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->q:J

    move-wide/from16 v19, v1

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/fa;->n:Z

    move/from16 v21, v1

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v21}, Lcom/google/android/exoplayer2/fa;-><init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V

    return-object v22
.end method

.method public b(Z)Lcom/google/android/exoplayer2/fa;
    .locals 24
    .annotation build Landroidx/annotation/CheckResult;
    .end annotation

    move-object/from16 v0, p0

    move/from16 v21, p1

    new-instance v22, Lcom/google/android/exoplayer2/fa;

    move-object/from16 v1, v22

    iget-object v2, v0, Lcom/google/android/exoplayer2/fa;->b:Lcom/google/android/exoplayer2/sa;

    iget-object v3, v0, Lcom/google/android/exoplayer2/fa;->c:Lcom/google/android/exoplayer2/source/y$a;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/fa;->d:J

    iget v6, v0, Lcom/google/android/exoplayer2/fa;->e:I

    iget-object v7, v0, Lcom/google/android/exoplayer2/fa;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-boolean v8, v0, Lcom/google/android/exoplayer2/fa;->g:Z

    iget-object v9, v0, Lcom/google/android/exoplayer2/fa;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v10, v0, Lcom/google/android/exoplayer2/fa;->i:Lcom/google/android/exoplayer2/trackselection/p;

    iget-object v11, v0, Lcom/google/android/exoplayer2/fa;->j:Lcom/google/android/exoplayer2/source/y$a;

    iget-boolean v12, v0, Lcom/google/android/exoplayer2/fa;->k:Z

    iget v13, v0, Lcom/google/android/exoplayer2/fa;->l:I

    iget-object v14, v0, Lcom/google/android/exoplayer2/fa;->m:Lcom/google/android/exoplayer2/ga;

    move-object/from16 p1, v1

    move-object/from16 v23, v2

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->o:J

    move-wide v15, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->p:J

    move-wide/from16 v17, v1

    iget-wide v1, v0, Lcom/google/android/exoplayer2/fa;->q:J

    move-wide/from16 v19, v1

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-direct/range {v1 .. v21}, Lcom/google/android/exoplayer2/fa;-><init>(Lcom/google/android/exoplayer2/sa;Lcom/google/android/exoplayer2/source/y$a;JILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/p;Lcom/google/android/exoplayer2/source/y$a;ZILcom/google/android/exoplayer2/ga;JJJZ)V

    return-object v22
.end method
