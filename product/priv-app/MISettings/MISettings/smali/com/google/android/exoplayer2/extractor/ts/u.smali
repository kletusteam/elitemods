.class public final Lcom/google/android/exoplayer2/extractor/ts/u;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/ts/m;


# instance fields
.field private final a:Lcom/google/android/exoplayer2/util/t;

.field private final b:Lcom/google/android/exoplayer2/audio/F$a;

.field private final c:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z

.field private j:J

.field private k:I

.field private l:J


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/ts/u;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->f:I

    new-instance v1, Lcom/google/android/exoplayer2/util/t;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->a:Lcom/google/android/exoplayer2/util/t;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->a:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v1

    const/4 v2, -0x1

    aput-byte v2, v1, v0

    new-instance v0, Lcom/google/android/exoplayer2/audio/F$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/audio/F$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->b:Lcom/google/android/exoplayer2/audio/F$a;

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->c:Ljava/lang/String;

    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/util/t;)V
    .locals 8

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->d()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->e()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_3

    aget-byte v3, v0, v1

    const/16 v4, 0xff

    and-int/2addr v3, v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-ne v3, v4, :cond_0

    move v3, v6

    goto :goto_1

    :cond_0
    move v3, v5

    :goto_1
    iget-boolean v4, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->i:Z

    if-eqz v4, :cond_1

    aget-byte v4, v0, v1

    const/16 v7, 0xe0

    and-int/2addr v4, v7

    if-ne v4, v7, :cond_1

    move v4, v6

    goto :goto_2

    :cond_1
    move v4, v5

    :goto_2
    iput-boolean v3, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->i:Z

    if-eqz v4, :cond_2

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iput-boolean v5, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->i:Z

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->a:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p1

    aget-byte v0, v0, v1

    aput-byte v0, p1, v6

    const/4 p1, 0x2

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    iput v6, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->f:I

    return-void

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v2}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    return-void
.end method

.method private c(Lcom/google/android/exoplayer2/util/t;)V
    .locals 7

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->k:I

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-interface {v1, p1, v0}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/util/t;I)V

    iget p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    add-int/2addr p1, v0

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    iget p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    iget v4, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->k:I

    if-ge p1, v4, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-wide v1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->l:J

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(JIIILcom/google/android/exoplayer2/extractor/TrackOutput$a;)V

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->l:J

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->j:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->l:J

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->f:I

    return-void
.end method

.method private d(Lcom/google/android/exoplayer2/util/t;)V
    .locals 7

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    const/4 v2, 0x4

    rsub-int/lit8 v1, v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->a:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v1

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    invoke-virtual {p1, v1, v3, v0}, Lcom/google/android/exoplayer2/util/t;->a([BII)V

    iget p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    add-int/2addr p1, v0

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    iget p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    if-ge p1, v2, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->a:Lcom/google/android/exoplayer2/util/t;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->b:Lcom/google/android/exoplayer2/audio/F$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->a:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/t;->j()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/exoplayer2/audio/F$a;->a(I)Z

    move-result p1

    const/4 v1, 0x1

    if-nez p1, :cond_1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->f:I

    return-void

    :cond_1
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->b:Lcom/google/android/exoplayer2/audio/F$a;

    iget v3, p1, Lcom/google/android/exoplayer2/audio/F$a;->c:I

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->k:I

    iget-boolean v3, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->h:Z

    if-nez v3, :cond_2

    const-wide/32 v3, 0xf4240

    iget v5, p1, Lcom/google/android/exoplayer2/audio/F$a;->g:I

    int-to-long v5, v5

    mul-long/2addr v5, v3

    iget p1, p1, Lcom/google/android/exoplayer2/audio/F$a;->d:I

    int-to-long v3, p1

    div-long/2addr v5, v3

    iput-wide v5, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->j:J

    new-instance p1, Lcom/google/android/exoplayer2/Format$a;

    invoke-direct {p1}, Lcom/google/android/exoplayer2/Format$a;-><init>()V

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->e:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/Format$a;->b(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->b:Lcom/google/android/exoplayer2/audio/F$a;

    iget-object v3, v3, Lcom/google/android/exoplayer2/audio/F$a;->b:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/Format$a;->e(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    const/16 v3, 0x1000

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/Format$a;->h(I)Lcom/google/android/exoplayer2/Format$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->b:Lcom/google/android/exoplayer2/audio/F$a;

    iget v3, v3, Lcom/google/android/exoplayer2/audio/F$a;->e:I

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/Format$a;->c(I)Lcom/google/android/exoplayer2/Format$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->b:Lcom/google/android/exoplayer2/audio/F$a;

    iget v3, v3, Lcom/google/android/exoplayer2/audio/F$a;->d:I

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/Format$a;->l(I)Lcom/google/android/exoplayer2/Format$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->c:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/Format$a;->d(Ljava/lang/String;)Lcom/google/android/exoplayer2/Format$a;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/Format$a;->a()Lcom/google/android/exoplayer2/Format;

    move-result-object p1

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-interface {v3, p1}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/Format;)V

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->h:Z

    :cond_2
    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->a:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/t;->e(I)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->a:Lcom/google/android/exoplayer2/util/t;

    invoke-interface {p1, v0, v2}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->a(Lcom/google/android/exoplayer2/util/t;I)V

    const/4 p1, 0x2

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->f:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->f:I

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->g:I

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->i:Z

    return-void
.end method

.method public a(JI)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->l:J

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/k;Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;)V
    .locals 1

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->a()V

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->e:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/ts/TsPayloadReader$d;->c()I

    move-result p2

    const/4 v0, 0x1

    invoke-interface {p1, p2, v0}, Lcom/google/android/exoplayer2/extractor/k;->a(II)Lcom/google/android/exoplayer2/extractor/TrackOutput;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/util/t;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->d:Lcom/google/android/exoplayer2/extractor/TrackOutput;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/t;->a()I

    move-result v0

    if-lez v0, :cond_3

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/ts/u;->f:I

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/u;->c(Lcom/google/android/exoplayer2/util/t;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/u;->d(Lcom/google/android/exoplayer2/util/t;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/ts/u;->b(Lcom/google/android/exoplayer2/util/t;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method
