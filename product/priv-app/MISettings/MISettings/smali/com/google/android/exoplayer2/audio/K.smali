.class public final Lcom/google/android/exoplayer2/audio/K;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/exoplayer2/audio/r;


# instance fields
.field private a:I

.field private b:F

.field private c:F

.field private d:Lcom/google/android/exoplayer2/audio/r$a;

.field private e:Lcom/google/android/exoplayer2/audio/r$a;

.field private f:Lcom/google/android/exoplayer2/audio/r$a;

.field private g:Lcom/google/android/exoplayer2/audio/r$a;

.field private h:Z

.field private i:Lcom/google/android/exoplayer2/audio/J;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/nio/ByteBuffer;

.field private k:Ljava/nio/ShortBuffer;

.field private l:Ljava/nio/ByteBuffer;

.field private m:J

.field private n:J

.field private o:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/exoplayer2/audio/K;->b:F

    iput v0, p0, Lcom/google/android/exoplayer2/audio/K;->c:F

    sget-object v0, Lcom/google/android/exoplayer2/audio/r$a;->a:Lcom/google/android/exoplayer2/audio/r$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->d:Lcom/google/android/exoplayer2/audio/r$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->e:Lcom/google/android/exoplayer2/audio/r$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->f:Lcom/google/android/exoplayer2/audio/r$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->g:Lcom/google/android/exoplayer2/audio/r$a;

    sget-object v0, Lcom/google/android/exoplayer2/audio/r;->a:Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->j:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->k:Ljava/nio/ShortBuffer;

    sget-object v0, Lcom/google/android/exoplayer2/audio/r;->a:Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->l:Ljava/nio/ByteBuffer;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/audio/K;->a:I

    return-void
.end method


# virtual methods
.method public a(F)F
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/audio/K;->c:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/google/android/exoplayer2/audio/K;->c:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/K;->h:Z

    :cond_0
    return p1
.end method

.method public a(J)J
    .locals 15

    move-object v0, p0

    iget-wide v5, v0, Lcom/google/android/exoplayer2/audio/K;->n:J

    const-wide/16 v1, 0x400

    cmp-long v1, v5, v1

    if-ltz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/exoplayer2/audio/K;->g:Lcom/google/android/exoplayer2/audio/r$a;

    iget v1, v1, Lcom/google/android/exoplayer2/audio/r$a;->b:I

    iget-object v2, v0, Lcom/google/android/exoplayer2/audio/K;->f:Lcom/google/android/exoplayer2/audio/r$a;

    iget v2, v2, Lcom/google/android/exoplayer2/audio/r$a;->b:I

    if-ne v1, v2, :cond_0

    iget-wide v3, v0, Lcom/google/android/exoplayer2/audio/K;->m:J

    move-wide/from16 v1, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/exoplayer2/util/E;->c(JJJ)J

    move-result-wide v1

    goto :goto_0

    :cond_0
    iget-wide v3, v0, Lcom/google/android/exoplayer2/audio/K;->m:J

    int-to-long v7, v1

    mul-long v11, v3, v7

    int-to-long v1, v2

    mul-long v13, v5, v1

    move-wide/from16 v9, p1

    invoke-static/range {v9 .. v14}, Lcom/google/android/exoplayer2/util/E;->c(JJJ)J

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_1
    iget v1, v0, Lcom/google/android/exoplayer2/audio/K;->b:F

    float-to-double v1, v1

    move-wide/from16 v3, p1

    long-to-double v3, v3

    mul-double/2addr v1, v3

    double-to-long v1, v1

    return-wide v1
.end method

.method public a(Lcom/google/android/exoplayer2/audio/r$a;)Lcom/google/android/exoplayer2/audio/r$a;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/r$b;
        }
    .end annotation

    iget v0, p1, Lcom/google/android/exoplayer2/audio/r$a;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/exoplayer2/audio/K;->a:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    iget v0, p1, Lcom/google/android/exoplayer2/audio/r$a;->b:I

    :cond_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/K;->d:Lcom/google/android/exoplayer2/audio/r$a;

    new-instance v2, Lcom/google/android/exoplayer2/audio/r$a;

    iget p1, p1, Lcom/google/android/exoplayer2/audio/r$a;->c:I

    invoke-direct {v2, v0, p1, v1}, Lcom/google/android/exoplayer2/audio/r$a;-><init>(III)V

    iput-object v2, p0, Lcom/google/android/exoplayer2/audio/K;->e:Lcom/google/android/exoplayer2/audio/r$a;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/audio/K;->h:Z

    iget-object p1, p0, Lcom/google/android/exoplayer2/audio/K;->e:Lcom/google/android/exoplayer2/audio/r$a;

    return-object p1

    :cond_1
    new-instance v0, Lcom/google/android/exoplayer2/audio/r$b;

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/audio/r$b;-><init>(Lcom/google/android/exoplayer2/audio/r$a;)V

    throw v0
.end method

.method public a()Ljava/nio/ByteBuffer;
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->l:Ljava/nio/ByteBuffer;

    sget-object v1, Lcom/google/android/exoplayer2/audio/r;->a:Ljava/nio/ByteBuffer;

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->l:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->i:Lcom/google/android/exoplayer2/audio/J;

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/audio/J;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    iget-wide v3, p0, Lcom/google/android/exoplayer2/audio/K;->m:J

    int-to-long v5, v2

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/android/exoplayer2/audio/K;->m:J

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/audio/J;->b(Ljava/nio/ShortBuffer;)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/J;->b()I

    move-result p1

    if-lez p1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-ge v1, p1, :cond_1

    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->j:Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->k:Ljava/nio/ShortBuffer;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->k:Ljava/nio/ShortBuffer;

    invoke-virtual {v1}, Ljava/nio/ShortBuffer;->clear()Ljava/nio/Buffer;

    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->k:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/audio/J;->a(Ljava/nio/ShortBuffer;)V

    iget-wide v0, p0, Lcom/google/android/exoplayer2/audio/K;->n:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/K;->n:J

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/google/android/exoplayer2/audio/K;->j:Ljava/nio/ByteBuffer;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/K;->l:Ljava/nio/ByteBuffer;

    :cond_2
    return-void
.end method

.method public b(F)F
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/audio/K;->b:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/google/android/exoplayer2/audio/K;->b:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/K;->h:Z

    :cond_0
    return p1
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/K;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->i:Lcom/google/android/exoplayer2/audio/J;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/J;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->i:Lcom/google/android/exoplayer2/audio/J;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/J;->c()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/K;->o:Z

    return-void
.end method

.method public flush()V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/K;->isActive()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->d:Lcom/google/android/exoplayer2/audio/r$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->f:Lcom/google/android/exoplayer2/audio/r$a;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->e:Lcom/google/android/exoplayer2/audio/r$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->g:Lcom/google/android/exoplayer2/audio/r$a;

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/K;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/exoplayer2/audio/J;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->f:Lcom/google/android/exoplayer2/audio/r$a;

    iget v2, v1, Lcom/google/android/exoplayer2/audio/r$a;->b:I

    iget v3, v1, Lcom/google/android/exoplayer2/audio/r$a;->c:I

    iget v4, p0, Lcom/google/android/exoplayer2/audio/K;->b:F

    iget v5, p0, Lcom/google/android/exoplayer2/audio/K;->c:F

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->g:Lcom/google/android/exoplayer2/audio/r$a;

    iget v6, v1, Lcom/google/android/exoplayer2/audio/r$a;->b:I

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer2/audio/J;-><init>(IIFFI)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->i:Lcom/google/android/exoplayer2/audio/J;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->i:Lcom/google/android/exoplayer2/audio/J;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/J;->a()V

    :cond_1
    :goto_0
    sget-object v0, Lcom/google/android/exoplayer2/audio/r;->a:Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->l:Ljava/nio/ByteBuffer;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/K;->m:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/K;->n:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/K;->o:Z

    return-void
.end method

.method public isActive()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->e:Lcom/google/android/exoplayer2/audio/r$a;

    iget v0, v0, Lcom/google/android/exoplayer2/audio/r$a;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/exoplayer2/audio/K;->b:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v2, 0x3c23d70a    # 0.01f

    cmpl-float v0, v0, v2

    if-gez v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/audio/K;->c:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->e:Lcom/google/android/exoplayer2/audio/r$a;

    iget v0, v0, Lcom/google/android/exoplayer2/audio/r$a;->b:I

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->d:Lcom/google/android/exoplayer2/audio/r$a;

    iget v1, v1, Lcom/google/android/exoplayer2/audio/r$a;->b:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public reset()V
    .locals 3

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/exoplayer2/audio/K;->b:F

    iput v0, p0, Lcom/google/android/exoplayer2/audio/K;->c:F

    sget-object v0, Lcom/google/android/exoplayer2/audio/r$a;->a:Lcom/google/android/exoplayer2/audio/r$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->d:Lcom/google/android/exoplayer2/audio/r$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->e:Lcom/google/android/exoplayer2/audio/r$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->f:Lcom/google/android/exoplayer2/audio/r$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->g:Lcom/google/android/exoplayer2/audio/r$a;

    sget-object v0, Lcom/google/android/exoplayer2/audio/r;->a:Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->j:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->k:Ljava/nio/ShortBuffer;

    sget-object v0, Lcom/google/android/exoplayer2/audio/r;->a:Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/K;->l:Ljava/nio/ByteBuffer;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/audio/K;->a:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/K;->h:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/K;->i:Lcom/google/android/exoplayer2/audio/J;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/google/android/exoplayer2/audio/K;->m:J

    iput-wide v1, p0, Lcom/google/android/exoplayer2/audio/K;->n:J

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/K;->o:Z

    return-void
.end method
