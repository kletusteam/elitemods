.class final Lcom/google/android/exoplayer2/extractor/b/g;
.super Ljava/lang/Object;


# instance fields
.field public a:I

.field public b:I

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:I

.field public h:I

.field public i:I

.field public final j:[I

.field private final k:Lcom/google/android/exoplayer2/util/t;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xff

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/google/android/exoplayer2/extractor/b/g;->j:[I

    new-instance v1, Lcom/google/android/exoplayer2/util/t;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer2/util/t;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/extractor/i;[BIIZ)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-interface {p0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/extractor/i;->b([BIIZ)Z

    move-result p0
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    move-exception p0

    if-eqz p4, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    throw p0
.end method


# virtual methods
.method public a()V
    .locals 3

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->a:I

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->b:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/google/android/exoplayer2/extractor/b/g;->c:J

    iput-wide v1, p0, Lcom/google/android/exoplayer2/extractor/b/g;->d:J

    iput-wide v1, p0, Lcom/google/android/exoplayer2/extractor/b/g;->e:J

    iput-wide v1, p0, Lcom/google/android/exoplayer2/extractor/b/g;->f:J

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->g:I

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->h:I

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->i:I

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;J)Z

    move-result p1

    return p1
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;J)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/d;->a(Z)V

    :goto_1
    const-wide/16 v3, -0x1

    cmp-long v0, p2, v3

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v3

    const-wide/16 v5, 0x4

    add-long/2addr v3, v5

    cmp-long v3, v3, p2

    if-gez v3, :cond_3

    :cond_1
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v3

    const/4 v4, 0x4

    invoke-static {p1, v3, v1, v4, v2}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;[BIIZ)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->x()J

    move-result-wide v3

    const-wide/32 v5, 0x4f676753

    cmp-long v0, v3, v5

    if-nez v0, :cond_2

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->c()V

    return v2

    :cond_2
    invoke-interface {p1, v2}, Lcom/google/android/exoplayer2/extractor/i;->c(I)V

    goto :goto_1

    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/i;->getPosition()J

    move-result-wide v3

    cmp-long v3, v3, p2

    if-gez v3, :cond_5

    :cond_4
    invoke-interface {p1, v2}, Lcom/google/android/exoplayer2/extractor/i;->b(I)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    goto :goto_2

    :cond_5
    return v1
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/i;Z)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/extractor/b/g;->a()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    const/16 v1, 0x1b

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, p2}, Lcom/google/android/exoplayer2/extractor/b/g;->a(Lcom/google/android/exoplayer2/extractor/i;[BIIZ)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->x()J

    move-result-wide v0

    const-wide/32 v3, 0x4f676753

    cmp-long v0, v0, v3

    if-eqz v0, :cond_0

    goto/16 :goto_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->a:I

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->a:I

    if-eqz v0, :cond_2

    if-eqz p2, :cond_1

    return v2

    :cond_1
    new-instance p1, Lcom/google/android/exoplayer2/ea;

    const-string p2, "unsupported bit stream revision"

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ea;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result p2

    iput p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->b:I

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->n()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->c:J

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->d:J

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->e:J

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->f:J

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result p2

    iput p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->g:I

    iget p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->g:I

    add-int/lit8 v0, p2, 0x1b

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->h:I

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {v0, p2}, Lcom/google/android/exoplayer2/util/t;->c(I)V

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->c()[B

    move-result-object p2

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/b/g;->g:I

    invoke-interface {p1, p2, v2, v0}, Lcom/google/android/exoplayer2/extractor/i;->b([BII)V

    :goto_0
    iget p1, p0, Lcom/google/android/exoplayer2/extractor/b/g;->g:I

    if-ge v2, p1, :cond_3

    iget-object p1, p0, Lcom/google/android/exoplayer2/extractor/b/g;->j:[I

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->k:Lcom/google/android/exoplayer2/util/t;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/util/t;->v()I

    move-result p2

    aput p2, p1, v2

    iget p1, p0, Lcom/google/android/exoplayer2/extractor/b/g;->i:I

    iget-object p2, p0, Lcom/google/android/exoplayer2/extractor/b/g;->j:[I

    aget p2, p2, v2

    add-int/2addr p1, p2

    iput p1, p0, Lcom/google/android/exoplayer2/extractor/b/g;->i:I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    return p1

    :cond_4
    :goto_1
    return v2
.end method
