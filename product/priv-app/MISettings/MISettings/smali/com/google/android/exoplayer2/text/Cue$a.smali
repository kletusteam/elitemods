.class public final Lcom/google/android/exoplayer2/text/Cue$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/text/Cue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/CharSequence;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private b:Landroid/graphics/Bitmap;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private c:Landroid/text/Layout$Alignment;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private d:F

.field private e:I

.field private f:I

.field private g:F

.field private h:I

.field private i:I

.field private j:F

.field private k:F

.field private l:F

.field private m:Z

.field private n:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field private o:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->a:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->b:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->c:Landroid/text/Layout$Alignment;

    const v0, -0x800001

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->d:F

    const/high16 v1, -0x80000000

    iput v1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->e:I

    iput v1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->f:I

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->g:F

    iput v1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->h:I

    iput v1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->i:I

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->j:F

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->k:F

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->l:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->m:Z

    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->n:I

    iput v1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->o:I

    return-void
.end method

.method private constructor <init>(Lcom/google/android/exoplayer2/text/Cue;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/android/exoplayer2/text/Cue;->b:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->a:Ljava/lang/CharSequence;

    iget-object v0, p1, Lcom/google/android/exoplayer2/text/Cue;->d:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->b:Landroid/graphics/Bitmap;

    iget-object v0, p1, Lcom/google/android/exoplayer2/text/Cue;->c:Landroid/text/Layout$Alignment;

    iput-object v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->c:Landroid/text/Layout$Alignment;

    iget v0, p1, Lcom/google/android/exoplayer2/text/Cue;->e:F

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->d:F

    iget v0, p1, Lcom/google/android/exoplayer2/text/Cue;->f:I

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->e:I

    iget v0, p1, Lcom/google/android/exoplayer2/text/Cue;->g:I

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->f:I

    iget v0, p1, Lcom/google/android/exoplayer2/text/Cue;->h:F

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->g:F

    iget v0, p1, Lcom/google/android/exoplayer2/text/Cue;->i:I

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->h:I

    iget v0, p1, Lcom/google/android/exoplayer2/text/Cue;->n:I

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->i:I

    iget v0, p1, Lcom/google/android/exoplayer2/text/Cue;->o:F

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->j:F

    iget v0, p1, Lcom/google/android/exoplayer2/text/Cue;->j:F

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->k:F

    iget v0, p1, Lcom/google/android/exoplayer2/text/Cue;->k:F

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->l:F

    iget-boolean v0, p1, Lcom/google/android/exoplayer2/text/Cue;->l:Z

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->m:Z

    iget v0, p1, Lcom/google/android/exoplayer2/text/Cue;->m:I

    iput v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->n:I

    iget p1, p1, Lcom/google/android/exoplayer2/text/Cue;->p:I

    iput p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->o:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/text/Cue;Lcom/google/android/exoplayer2/text/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/text/Cue$a;-><init>(Lcom/google/android/exoplayer2/text/Cue;)V

    return-void
.end method


# virtual methods
.method public a(F)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->l:F

    return-object p0
.end method

.method public a(FI)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->d:F

    iput p2, p0, Lcom/google/android/exoplayer2/text/Cue$a;->e:I

    return-object p0
.end method

.method public a(I)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->f:I

    return-object p0
.end method

.method public a(Landroid/graphics/Bitmap;)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->b:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method public a(Landroid/text/Layout$Alignment;)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0
    .param p1    # Landroid/text/Layout$Alignment;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->c:Landroid/text/Layout$Alignment;

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->a:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public a()Lcom/google/android/exoplayer2/text/Cue;
    .locals 20

    move-object/from16 v0, p0

    new-instance v18, Lcom/google/android/exoplayer2/text/Cue;

    move-object/from16 v1, v18

    iget-object v2, v0, Lcom/google/android/exoplayer2/text/Cue$a;->a:Ljava/lang/CharSequence;

    iget-object v3, v0, Lcom/google/android/exoplayer2/text/Cue$a;->c:Landroid/text/Layout$Alignment;

    iget-object v4, v0, Lcom/google/android/exoplayer2/text/Cue$a;->b:Landroid/graphics/Bitmap;

    iget v5, v0, Lcom/google/android/exoplayer2/text/Cue$a;->d:F

    iget v6, v0, Lcom/google/android/exoplayer2/text/Cue$a;->e:I

    iget v7, v0, Lcom/google/android/exoplayer2/text/Cue$a;->f:I

    iget v8, v0, Lcom/google/android/exoplayer2/text/Cue$a;->g:F

    iget v9, v0, Lcom/google/android/exoplayer2/text/Cue$a;->h:I

    iget v10, v0, Lcom/google/android/exoplayer2/text/Cue$a;->i:I

    iget v11, v0, Lcom/google/android/exoplayer2/text/Cue$a;->j:F

    iget v12, v0, Lcom/google/android/exoplayer2/text/Cue$a;->k:F

    iget v13, v0, Lcom/google/android/exoplayer2/text/Cue$a;->l:F

    iget-boolean v14, v0, Lcom/google/android/exoplayer2/text/Cue$a;->m:Z

    iget v15, v0, Lcom/google/android/exoplayer2/text/Cue$a;->n:I

    move-object/from16 v19, v1

    iget v1, v0, Lcom/google/android/exoplayer2/text/Cue$a;->o:I

    move/from16 v16, v1

    const/16 v17, 0x0

    move-object/from16 v1, v19

    invoke-direct/range {v1 .. v17}, Lcom/google/android/exoplayer2/text/Cue;-><init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;Landroid/graphics/Bitmap;FIIFIIFFFZIILcom/google/android/exoplayer2/text/b;)V

    return-object v18
.end method

.method public b()Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->m:Z

    return-object p0
.end method

.method public b(F)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->g:F

    return-object p0
.end method

.method public b(FI)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->j:F

    iput p2, p0, Lcom/google/android/exoplayer2/text/Cue$a;->i:I

    return-object p0
.end method

.method public b(I)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->h:I

    return-object p0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->f:I

    return v0
.end method

.method public c(F)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->k:F

    return-object p0
.end method

.method public c(I)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->o:I

    return-object p0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->h:I

    return v0
.end method

.method public d(I)Lcom/google/android/exoplayer2/text/Cue$a;
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    iput p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->n:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/android/exoplayer2/text/Cue$a;->m:Z

    return-object p0
.end method

.method public e()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/text/Cue$a;->a:Ljava/lang/CharSequence;

    return-object v0
.end method
