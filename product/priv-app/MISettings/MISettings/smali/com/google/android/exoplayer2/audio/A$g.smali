.class final Lcom/google/android/exoplayer2/audio/A$g;
.super Landroid/media/AudioTrack$StreamEventCallback;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x1d
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/audio/A;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "g"
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field final synthetic b:Lcom/google/android/exoplayer2/audio/A;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/audio/A;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A$g;->b:Lcom/google/android/exoplayer2/audio/A;

    invoke-direct {p0}, Landroid/media/AudioTrack$StreamEventCallback;-><init>()V

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/A$g;->a:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public a(Landroid/media/AudioTrack;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/A$g;->a:Landroid/os/Handler;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/exoplayer2/audio/i;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer2/audio/i;-><init>(Landroid/os/Handler;)V

    invoke-virtual {p1, v1, p0}, Landroid/media/AudioTrack;->registerStreamEventCallback(Ljava/util/concurrent/Executor;Landroid/media/AudioTrack$StreamEventCallback;)V

    return-void
.end method

.method public b(Landroid/media/AudioTrack;)V
    .locals 1

    invoke-virtual {p1, p0}, Landroid/media/AudioTrack;->unregisterStreamEventCallback(Landroid/media/AudioTrack$StreamEventCallback;)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/audio/A$g;->a:Landroid/os/Handler;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public onDataRequest(Landroid/media/AudioTrack;I)V
    .locals 0

    iget-object p2, p0, Lcom/google/android/exoplayer2/audio/A$g;->b:Lcom/google/android/exoplayer2/audio/A;

    invoke-static {p2}, Lcom/google/android/exoplayer2/audio/A;->b(Lcom/google/android/exoplayer2/audio/A;)Landroid/media/AudioTrack;

    move-result-object p2

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Lcom/google/android/exoplayer2/util/d;->b(Z)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/audio/A$g;->b:Lcom/google/android/exoplayer2/audio/A;

    invoke-static {p1}, Lcom/google/android/exoplayer2/audio/A;->c(Lcom/google/android/exoplayer2/audio/A;)Lcom/google/android/exoplayer2/audio/AudioSink$c;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/google/android/exoplayer2/audio/A$g;->b:Lcom/google/android/exoplayer2/audio/A;

    invoke-static {p1}, Lcom/google/android/exoplayer2/audio/A;->c(Lcom/google/android/exoplayer2/audio/A;)Lcom/google/android/exoplayer2/audio/AudioSink$c;

    move-result-object p1

    invoke-interface {p1}, Lcom/google/android/exoplayer2/audio/AudioSink$c;->b()V

    :cond_1
    return-void
.end method
