.class Lcom/google/common/collect/r$c;
.super Lcom/google/common/collect/r;
.source "ImmutableList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/common/collect/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/common/collect/r<",
        "TE;>;"
    }
.end annotation


# instance fields
.field final transient c:I

.field final transient d:I

.field final synthetic e:Lcom/google/common/collect/r;


# direct methods
.method constructor <init>(Lcom/google/common/collect/r;II)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/r$c;->e:Lcom/google/common/collect/r;

    invoke-direct {p0}, Lcom/google/common/collect/r;-><init>()V

    iput p2, p0, Lcom/google/common/collect/r$c;->c:I

    iput p3, p0, Lcom/google/common/collect/r$c;->d:I

    return-void
.end method


# virtual methods
.method b()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/r$c;->e:Lcom/google/common/collect/r;

    invoke-virtual {v0}, Lcom/google/common/collect/q;->b()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method c()I
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/r$c;->e:Lcom/google/common/collect/r;

    invoke-virtual {v0}, Lcom/google/common/collect/q;->d()I

    move-result v0

    iget v1, p0, Lcom/google/common/collect/r$c;->c:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/common/collect/r$c;->d:I

    add-int/2addr v0, v1

    return v0
.end method

.method d()I
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/r$c;->e:Lcom/google/common/collect/r;

    invoke-virtual {v0}, Lcom/google/common/collect/q;->d()I

    move-result v0

    iget v1, p0, Lcom/google/common/collect/r$c;->c:I

    add-int/2addr v0, v1

    return v0
.end method

.method e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    iget v0, p0, Lcom/google/common/collect/r$c;->d:I

    invoke-static {p1, v0}, Lb/a/a/a/e;->a(II)I

    iget-object v0, p0, Lcom/google/common/collect/r$c;->e:Lcom/google/common/collect/r;

    iget v1, p0, Lcom/google/common/collect/r$c;->c:I

    add-int/2addr p1, v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-super {p0}, Lcom/google/common/collect/r;->iterator()Lcom/google/common/collect/S;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    invoke-super {p0}, Lcom/google/common/collect/r;->listIterator()Lcom/google/common/collect/T;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 0

    invoke-super {p0, p1}, Lcom/google/common/collect/r;->listIterator(I)Lcom/google/common/collect/T;

    move-result-object p1

    return-object p1
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lcom/google/common/collect/r$c;->d:I

    return v0
.end method

.method public subList(II)Lcom/google/common/collect/r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/google/common/collect/r<",
            "TE;>;"
        }
    .end annotation

    iget v0, p0, Lcom/google/common/collect/r$c;->d:I

    invoke-static {p1, p2, v0}, Lb/a/a/a/e;->a(III)V

    iget-object v0, p0, Lcom/google/common/collect/r$c;->e:Lcom/google/common/collect/r;

    iget v1, p0, Lcom/google/common/collect/r$c;->c:I

    add-int/2addr p1, v1

    add-int/2addr p2, v1

    invoke-virtual {v0, p1, p2}, Lcom/google/common/collect/r;->subList(II)Lcom/google/common/collect/r;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic subList(II)Ljava/util/List;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/r$c;->subList(II)Lcom/google/common/collect/r;

    move-result-object p1

    return-object p1
.end method
