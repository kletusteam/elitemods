.class public abstract Lcom/google/common/collect/n;
.super Ljava/lang/Object;
.source "ComparisonChain.java"


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/common/collect/n$a;
    }
.end annotation


# static fields
.field private static final a:Lcom/google/common/collect/n;

.field private static final b:Lcom/google/common/collect/n;

.field private static final c:Lcom/google/common/collect/n;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/common/collect/m;

    invoke-direct {v0}, Lcom/google/common/collect/m;-><init>()V

    sput-object v0, Lcom/google/common/collect/n;->a:Lcom/google/common/collect/n;

    new-instance v0, Lcom/google/common/collect/n$a;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/common/collect/n$a;-><init>(I)V

    sput-object v0, Lcom/google/common/collect/n;->b:Lcom/google/common/collect/n;

    new-instance v0, Lcom/google/common/collect/n$a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/common/collect/n$a;-><init>(I)V

    sput-object v0, Lcom/google/common/collect/n;->c:Lcom/google/common/collect/n;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/common/collect/m;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/common/collect/n;-><init>()V

    return-void
.end method

.method static synthetic a()Lcom/google/common/collect/n;
    .locals 1

    sget-object v0, Lcom/google/common/collect/n;->b:Lcom/google/common/collect/n;

    return-object v0
.end method

.method static synthetic b()Lcom/google/common/collect/n;
    .locals 1

    sget-object v0, Lcom/google/common/collect/n;->c:Lcom/google/common/collect/n;

    return-object v0
.end method

.method static synthetic c()Lcom/google/common/collect/n;
    .locals 1

    sget-object v0, Lcom/google/common/collect/n;->a:Lcom/google/common/collect/n;

    return-object v0
.end method

.method public static e()Lcom/google/common/collect/n;
    .locals 1

    sget-object v0, Lcom/google/common/collect/n;->a:Lcom/google/common/collect/n;

    return-object v0
.end method


# virtual methods
.method public abstract a(II)Lcom/google/common/collect/n;
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/n;
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lorg/checkerframework/checker/nullness/compatqual/NullableDecl;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lorg/checkerframework/checker/nullness/compatqual/NullableDecl;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;",
            "Ljava/util/Comparator<",
            "TT;>;)",
            "Lcom/google/common/collect/n;"
        }
    .end annotation
.end method

.method public abstract a(ZZ)Lcom/google/common/collect/n;
.end method

.method public abstract b(ZZ)Lcom/google/common/collect/n;
.end method

.method public abstract d()I
.end method
