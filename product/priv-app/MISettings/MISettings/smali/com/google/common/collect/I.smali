.class public abstract Lcom/google/common/collect/I;
.super Ljava/lang/Object;
.source "Ordering.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "TT;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/common/collect/I;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
        serializable = true
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Ljava/lang/Comparable;",
            ">()",
            "Lcom/google/common/collect/I<",
            "TC;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/common/collect/G;->a:Lcom/google/common/collect/G;

    return-object v0
.end method

.method public static a(Ljava/util/Comparator;)Lcom/google/common/collect/I;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
        serializable = true
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator<",
            "TT;>;)",
            "Lcom/google/common/collect/I<",
            "TT;>;"
        }
    .end annotation

    instance-of v0, p0, Lcom/google/common/collect/I;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/common/collect/I;

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/common/collect/l;

    invoke-direct {v0, p0}, Lcom/google/common/collect/l;-><init>(Ljava/util/Comparator;)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method


# virtual methods
.method public a(Lb/a/a/a/c;)Lcom/google/common/collect/I;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
        serializable = true
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            ">(",
            "Lb/a/a/a/c<",
            "TF;+TT;>;)",
            "Lcom/google/common/collect/I<",
            "TF;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/common/collect/e;

    invoke-direct {v0, p1, p0}, Lcom/google/common/collect/e;-><init>(Lb/a/a/a/c;Lcom/google/common/collect/I;)V

    return-object v0
.end method

.method public a(Ljava/lang/Iterable;)Lcom/google/common/collect/r;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:TT;>(",
            "Ljava/lang/Iterable<",
            "TE;>;)",
            "Lcom/google/common/collect/r<",
            "TE;>;"
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/google/common/collect/r;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/google/common/collect/r;

    move-result-object p1

    return-object p1
.end method

.method b()Lcom/google/common/collect/I;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T2:TT;>()",
            "Lcom/google/common/collect/I<",
            "Ljava/util/Map$Entry<",
            "TT2;*>;>;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/D;->a()Lb/a/a/a/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/common/collect/I;->a(Lb/a/a/a/c;)Lcom/google/common/collect/I;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/common/collect/I;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
        serializable = true
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "Lcom/google/common/collect/I<",
            "TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/common/collect/O;

    invoke-direct {v0, p0}, Lcom/google/common/collect/O;-><init>(Lcom/google/common/collect/I;)V

    return-object v0
.end method

.method public abstract compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lorg/checkerframework/checker/nullness/compatqual/NullableDecl;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lorg/checkerframework/checker/nullness/compatqual/NullableDecl;
        .end annotation
    .end param
    .annotation build Lcom/google/errorprone/annotations/CanIgnoreReturnValue;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation
.end method
