.class public final Lmiuix/core/util/l;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/core/util/l$h;,
        Lmiuix/core/util/l$f;,
        Lmiuix/core/util/l$a;,
        Lmiuix/core/util/l$g;,
        Lmiuix/core/util/l$c;,
        Lmiuix/core/util/l$b;,
        Lmiuix/core/util/l$d;,
        Lmiuix/core/util/l$e;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Class<",
            "*>;",
            "Lmiuix/core/util/l$c<",
            "*>;>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Class<",
            "*>;",
            "Lmiuix/core/util/l$g<",
            "*>;>;"
        }
    .end annotation
.end field

.field private static final c:Lmiuix/core/util/l$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/core/util/l$e<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmiuix/core/util/l;->a:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmiuix/core/util/l;->b:Ljava/util/HashMap;

    new-instance v0, Lmiuix/core/util/j;

    invoke-direct {v0}, Lmiuix/core/util/j;-><init>()V

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lmiuix/core/util/l;->b(Lmiuix/core/util/l$d;I)Lmiuix/core/util/l$h;

    move-result-object v0

    sput-object v0, Lmiuix/core/util/l;->c:Lmiuix/core/util/l$e;

    return-void
.end method

.method static synthetic a()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lmiuix/core/util/l;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method static a(Ljava/lang/Class;I)Lmiuix/core/util/l$c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;I)",
            "Lmiuix/core/util/l$c<",
            "TT;>;"
        }
    .end annotation

    sget-object v0, Lmiuix/core/util/l;->a:Ljava/util/HashMap;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lmiuix/core/util/l;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/core/util/l$c;

    if-nez v1, :cond_0

    new-instance v1, Lmiuix/core/util/l$c;

    invoke-direct {v1, p0, p1}, Lmiuix/core/util/l$c;-><init>(Ljava/lang/Class;I)V

    sget-object p1, Lmiuix/core/util/l;->a:Ljava/util/HashMap;

    invoke-virtual {p1, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p1}, Lmiuix/core/util/l$c;->a(I)V

    :goto_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static a(Lmiuix/core/util/l$d;I)Lmiuix/core/util/l$f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lmiuix/core/util/l$d<",
            "TT;>;I)",
            "Lmiuix/core/util/l$f<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lmiuix/core/util/l$f;

    invoke-direct {v0, p0, p1}, Lmiuix/core/util/l$f;-><init>(Lmiuix/core/util/l$d;I)V

    return-object v0
.end method

.method static a(Lmiuix/core/util/l$c;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lmiuix/core/util/l$c<",
            "TT;>;I)V"
        }
    .end annotation

    sget-object v0, Lmiuix/core/util/l;->a:Ljava/util/HashMap;

    monitor-enter v0

    neg-int p1, p1

    :try_start_0
    invoke-virtual {p0, p1}, Lmiuix/core/util/l$c;->a(I)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method static a(Lmiuix/core/util/l$g;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lmiuix/core/util/l$g<",
            "TT;>;I)V"
        }
    .end annotation

    sget-object v0, Lmiuix/core/util/l;->b:Ljava/util/HashMap;

    monitor-enter v0

    neg-int p1, p1

    :try_start_0
    invoke-virtual {p0, p1}, Lmiuix/core/util/l$g;->a(I)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method static synthetic b()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lmiuix/core/util/l;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method static b(Ljava/lang/Class;I)Lmiuix/core/util/l$g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;I)",
            "Lmiuix/core/util/l$g<",
            "TT;>;"
        }
    .end annotation

    sget-object v0, Lmiuix/core/util/l;->b:Ljava/util/HashMap;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lmiuix/core/util/l;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/core/util/l$g;

    if-nez v1, :cond_0

    new-instance v1, Lmiuix/core/util/l$g;

    invoke-direct {v1, p0, p1}, Lmiuix/core/util/l$g;-><init>(Ljava/lang/Class;I)V

    sget-object p1, Lmiuix/core/util/l;->b:Ljava/util/HashMap;

    invoke-virtual {p1, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p1}, Lmiuix/core/util/l$g;->a(I)V

    :goto_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static b(Lmiuix/core/util/l$d;I)Lmiuix/core/util/l$h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lmiuix/core/util/l$d<",
            "TT;>;I)",
            "Lmiuix/core/util/l$h<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lmiuix/core/util/l$h;

    invoke-direct {v0, p0, p1}, Lmiuix/core/util/l$h;-><init>(Lmiuix/core/util/l$d;I)V

    return-object v0
.end method

.method public static c()Lmiuix/core/util/l$e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmiuix/core/util/l$e<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation

    sget-object v0, Lmiuix/core/util/l;->c:Lmiuix/core/util/l$e;

    return-object v0
.end method
