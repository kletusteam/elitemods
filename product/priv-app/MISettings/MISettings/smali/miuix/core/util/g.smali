.class public Lmiuix/core/util/g;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/lang/ref/SoftReference<",
            "[B>;>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/lang/ref/SoftReference<",
            "[C>;>;"
        }
    .end annotation
.end field

.field private static final c:Lmiuix/core/util/l$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/core/util/l$e<",
            "Ljava/io/ByteArrayOutputStream;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Lmiuix/core/util/l$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/core/util/l$e<",
            "Ljava/io/CharArrayWriter;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lmiuix/core/util/l$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/core/util/l$e<",
            "Ljava/io/StringWriter;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lmiuix/core/util/g;->a:Ljava/lang/ThreadLocal;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lmiuix/core/util/g;->b:Ljava/lang/ThreadLocal;

    new-instance v0, Lmiuix/core/util/d;

    invoke-direct {v0}, Lmiuix/core/util/d;-><init>()V

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lmiuix/core/util/l;->b(Lmiuix/core/util/l$d;I)Lmiuix/core/util/l$h;

    move-result-object v0

    sput-object v0, Lmiuix/core/util/g;->c:Lmiuix/core/util/l$e;

    new-instance v0, Lmiuix/core/util/e;

    invoke-direct {v0}, Lmiuix/core/util/e;-><init>()V

    invoke-static {v0, v1}, Lmiuix/core/util/l;->b(Lmiuix/core/util/l$d;I)Lmiuix/core/util/l$h;

    move-result-object v0

    sput-object v0, Lmiuix/core/util/g;->d:Lmiuix/core/util/l$e;

    new-instance v0, Lmiuix/core/util/f;

    invoke-direct {v0}, Lmiuix/core/util/f;-><init>()V

    invoke-static {v0, v1}, Lmiuix/core/util/l;->b(Lmiuix/core/util/l$d;I)Lmiuix/core/util/l$h;

    move-result-object v0

    sput-object v0, Lmiuix/core/util/g;->e:Lmiuix/core/util/l$e;

    sget-object v0, Lmiuix/core/util/g;->e:Lmiuix/core/util/l$e;

    invoke-interface {v0}, Lmiuix/core/util/l$e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/StringWriter;

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v1}, Ljava/io/PrintWriter;->println()V

    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lmiuix/core/util/g;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    sget-object v1, Lmiuix/core/util/g;->e:Lmiuix/core/util/l$e;

    invoke-interface {v1, v0}, Lmiuix/core/util/l$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static a(Ljava/io/Closeable;)V
    .locals 0

    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method
