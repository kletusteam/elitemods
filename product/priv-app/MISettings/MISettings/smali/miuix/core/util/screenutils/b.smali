.class public Lmiuix/core/util/screenutils/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/core/util/screenutils/b$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;)Lmiuix/core/util/screenutils/b$a;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {p0}, Lmiuix/core/util/screenutils/a;->b(Landroid/content/Context;)Lmiuix/core/util/screenutils/b$a;

    move-result-object v0

    iget v1, v0, Lmiuix/core/util/screenutils/b$a;->a:I

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_0

    invoke-static {p0}, Lmiuix/core/util/screenutils/SplitScreenModeHelper;->b(Landroid/content/Context;)Lmiuix/core/util/screenutils/b$a;

    move-result-object v0

    iget p0, v0, Lmiuix/core/util/screenutils/b$a;->a:I

    const/16 v1, 0x1004

    if-ne p0, v1, :cond_0

    const/4 p0, 0x0

    iput p0, v0, Lmiuix/core/util/screenutils/b$a;->a:I

    :cond_0
    return-object v0
.end method

.method public static a(I)Z
    .locals 0

    and-int/lit16 p0, p0, 0x2000

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static b(Landroid/content/Context;)I
    .locals 0

    invoke-static {p0}, Lmiuix/core/util/screenutils/b;->a(Landroid/content/Context;)Lmiuix/core/util/screenutils/b$a;

    move-result-object p0

    iget p0, p0, Lmiuix/core/util/screenutils/b$a;->a:I

    return p0
.end method

.method public static b(I)Z
    .locals 0

    and-int/lit16 p0, p0, 0x1000

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
