.class public abstract Lmiuix/animation/g/A;
.super Lmiuix/animation/g/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/animation/g/b<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Lmiuix/animation/g/A;

.field public static final c:Lmiuix/animation/g/A;

.field public static final d:Lmiuix/animation/g/A;

.field public static final e:Lmiuix/animation/g/A;

.field public static final f:Lmiuix/animation/g/A;

.field public static final g:Lmiuix/animation/g/A;

.field public static final h:Lmiuix/animation/g/A;

.field public static final i:Lmiuix/animation/g/A;

.field public static final j:Lmiuix/animation/g/A;

.field public static final k:Lmiuix/animation/g/A;

.field public static final l:Lmiuix/animation/g/A;

.field public static final m:Lmiuix/animation/g/A;

.field public static final n:Lmiuix/animation/g/A;

.field public static final o:Lmiuix/animation/g/A;

.field public static final p:Lmiuix/animation/g/A;

.field public static final q:Lmiuix/animation/g/A;

.field public static final r:Lmiuix/animation/g/A;

.field public static final s:Lmiuix/animation/g/A;

.field public static final t:Lmiuix/animation/g/A;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmiuix/animation/g/r;

    const-string v1, "translationX"

    invoke-direct {v0, v1}, Lmiuix/animation/g/r;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/s;

    const-string v1, "translationY"

    invoke-direct {v0, v1}, Lmiuix/animation/g/s;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/t;

    const-string v1, "translationZ"

    invoke-direct {v0, v1}, Lmiuix/animation/g/t;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->d:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/u;

    const-string v1, "scaleX"

    invoke-direct {v0, v1}, Lmiuix/animation/g/u;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/v;

    const-string v1, "scaleY"

    invoke-direct {v0, v1}, Lmiuix/animation/g/v;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/w;

    const-string v1, "rotation"

    invoke-direct {v0, v1}, Lmiuix/animation/g/w;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->g:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/x;

    const-string v1, "rotationX"

    invoke-direct {v0, v1}, Lmiuix/animation/g/x;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->h:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/y;

    const-string v1, "rotationY"

    invoke-direct {v0, v1}, Lmiuix/animation/g/y;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->i:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/z;

    const-string v1, "x"

    invoke-direct {v0, v1}, Lmiuix/animation/g/z;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->j:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/h;

    const-string v1, "y"

    invoke-direct {v0, v1}, Lmiuix/animation/g/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->k:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/i;

    const-string v1, "z"

    invoke-direct {v0, v1}, Lmiuix/animation/g/i;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->l:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/j;

    const-string v1, "height"

    invoke-direct {v0, v1}, Lmiuix/animation/g/j;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->m:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/k;

    const-string v1, "width"

    invoke-direct {v0, v1}, Lmiuix/animation/g/k;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->n:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/l;

    const-string v1, "alpha"

    invoke-direct {v0, v1}, Lmiuix/animation/g/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/m;

    const-string v1, "autoAlpha"

    invoke-direct {v0, v1}, Lmiuix/animation/g/m;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->p:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/n;

    const-string v1, "scrollX"

    invoke-direct {v0, v1}, Lmiuix/animation/g/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->q:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/o;

    const-string v1, "scrollY"

    invoke-direct {v0, v1}, Lmiuix/animation/g/o;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->r:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/p;

    const-string v1, "deprecated_foreground"

    invoke-direct {v0, v1}, Lmiuix/animation/g/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->s:Lmiuix/animation/g/A;

    new-instance v0, Lmiuix/animation/g/q;

    const-string v1, "deprecated_background"

    invoke-direct {v0, v1}, Lmiuix/animation/g/q;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/g/A;->t:Lmiuix/animation/g/A;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/animation/g/b;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Landroid/view/View;)Z
    .locals 0

    invoke-static {p0}, Lmiuix/animation/g/A;->b(Landroid/view/View;)Z

    move-result p0

    return p0
.end method

.method private static b(Landroid/view/View;)Z
    .locals 1

    sget v0, Lmiuix/animation/p;->miuix_animation_tag_init_layout:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ViewProperty{mPropertyName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/g/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
