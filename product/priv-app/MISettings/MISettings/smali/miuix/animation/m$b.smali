.class public final enum Lmiuix/animation/m$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/animation/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lmiuix/animation/m$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lmiuix/animation/m$b;

.field public static final enum b:Lmiuix/animation/m$b;

.field private static final synthetic c:[Lmiuix/animation/m$b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lmiuix/animation/m$b;

    const/4 v1, 0x0

    const-string v2, "UP"

    invoke-direct {v0, v2, v1}, Lmiuix/animation/m$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiuix/animation/m$b;->a:Lmiuix/animation/m$b;

    new-instance v0, Lmiuix/animation/m$b;

    const/4 v2, 0x1

    const-string v3, "DOWN"

    invoke-direct {v0, v3, v2}, Lmiuix/animation/m$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    const/4 v0, 0x2

    new-array v0, v0, [Lmiuix/animation/m$b;

    sget-object v3, Lmiuix/animation/m$b;->a:Lmiuix/animation/m$b;

    aput-object v3, v0, v1

    sget-object v1, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    aput-object v1, v0, v2

    sput-object v0, Lmiuix/animation/m$b;->c:[Lmiuix/animation/m$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmiuix/animation/m$b;
    .locals 1

    const-class v0, Lmiuix/animation/m$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lmiuix/animation/m$b;

    return-object p0
.end method

.method public static values()[Lmiuix/animation/m$b;
    .locals 1

    sget-object v0, Lmiuix/animation/m$b;->c:[Lmiuix/animation/m$b;

    invoke-virtual {v0}, [Lmiuix/animation/m$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiuix/animation/m$b;

    return-object v0
.end method
