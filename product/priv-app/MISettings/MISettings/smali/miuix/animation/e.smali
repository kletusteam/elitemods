.class public abstract Lmiuix/animation/e;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final a:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final b:Lmiuix/animation/d/s;

.field public final c:Lmiuix/animation/d/d;

.field d:Lmiuix/animation/d/m;

.field e:F

.field f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field g:J

.field h:J

.field public final i:I

.field final j:Lmiuix/animation/d/u;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const v1, 0x7fffffff

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lmiuix/animation/e;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmiuix/animation/d/s;

    invoke-direct {v0, p0}, Lmiuix/animation/d/s;-><init>(Lmiuix/animation/e;)V

    iput-object v0, p0, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    new-instance v0, Lmiuix/animation/d/d;

    invoke-direct {v0}, Lmiuix/animation/d/d;-><init>()V

    iput-object v0, p0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    new-instance v0, Lmiuix/animation/d/m;

    invoke-direct {v0, p0}, Lmiuix/animation/d/m;-><init>(Lmiuix/animation/e;)V

    iput-object v0, p0, Lmiuix/animation/e;->d:Lmiuix/animation/d/m;

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiuix/animation/e;->e:F

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiuix/animation/e;->f:Ljava/util/Map;

    sget-object v0, Lmiuix/animation/e;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    iput v0, p0, Lmiuix/animation/e;->i:I

    new-instance v0, Lmiuix/animation/d/u;

    invoke-direct {v0}, Lmiuix/animation/d/u;-><init>()V

    iput-object v0, p0, Lmiuix/animation/e;->j:Lmiuix/animation/d/u;

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "IAnimTarget create ! "

    invoke-static {v2, v0}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    invoke-virtual {v0, p0}, Lmiuix/animation/d/d;->a(Lmiuix/animation/e;)V

    const v0, 0x3dcccccd    # 0.1f

    const/4 v2, 0x3

    new-array v3, v2, [Lmiuix/animation/g/b;

    sget-object v4, Lmiuix/animation/g/A;->g:Lmiuix/animation/g/A;

    aput-object v4, v3, v1

    sget-object v4, Lmiuix/animation/g/A;->h:Lmiuix/animation/g/A;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    sget-object v4, Lmiuix/animation/g/A;->i:Lmiuix/animation/g/A;

    const/4 v6, 0x2

    aput-object v4, v3, v6

    invoke-virtual {p0, v0, v3}, Lmiuix/animation/e;->a(F[Lmiuix/animation/g/b;)Lmiuix/animation/e;

    const/high16 v0, 0x3b800000    # 0.00390625f

    const/4 v3, 0x4

    new-array v3, v3, [Lmiuix/animation/g/b;

    sget-object v4, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    aput-object v4, v3, v1

    sget-object v4, Lmiuix/animation/g/A;->p:Lmiuix/animation/g/A;

    aput-object v4, v3, v5

    sget-object v4, Lmiuix/animation/g/C;->a:Lmiuix/animation/g/C$b;

    aput-object v4, v3, v6

    sget-object v4, Lmiuix/animation/g/C;->b:Lmiuix/animation/g/C$a;

    aput-object v4, v3, v2

    invoke-virtual {p0, v0, v3}, Lmiuix/animation/e;->a(F[Lmiuix/animation/g/b;)Lmiuix/animation/e;

    const v0, 0x3b03126f    # 0.002f

    new-array v2, v6, [Lmiuix/animation/g/b;

    sget-object v3, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    aput-object v3, v2, v1

    sget-object v1, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    aput-object v1, v2, v5

    invoke-virtual {p0, v0, v2}, Lmiuix/animation/e;->a(F[Lmiuix/animation/g/b;)Lmiuix/animation/e;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)F
    .locals 1

    iget-object v0, p0, Lmiuix/animation/e;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    return p1

    :cond_0
    iget p1, p0, Lmiuix/animation/e;->e:F

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    return p1

    :cond_1
    invoke-virtual {p0}, Lmiuix/animation/e;->b()F

    move-result p1

    return p1
.end method

.method public a(Lmiuix/animation/g/b;)F
    .locals 1

    invoke-virtual {p0}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lmiuix/animation/g/b;->b(Ljava/lang/Object;)F

    move-result p1

    return p1

    :cond_0
    const p1, 0x7f7fffff    # Float.MAX_VALUE

    return p1
.end method

.method public a(Lmiuix/animation/g/c;)I
    .locals 1

    invoke-virtual {p0}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, Lmiuix/animation/g/c;->a(Ljava/lang/Object;)I

    move-result p1

    return p1

    :cond_0
    const p1, 0x7fffffff

    return p1
.end method

.method public varargs a(F[Lmiuix/animation/g/b;)Lmiuix/animation/e;
    .locals 5

    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p2, v1

    iget-object v3, p0, Lmiuix/animation/e;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public abstract a()V
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/animation/e;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/e;->d:Lmiuix/animation/d/m;

    invoke-virtual {v0, p1, p2}, Lmiuix/animation/d/m;->a(Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V

    return-void
.end method

.method public a(Lmiuix/animation/g/b;D)V
    .locals 2

    const-wide v0, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpl-double v0, p2, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    double-to-float p2, p2

    invoke-virtual {v0, p1, p2}, Lmiuix/animation/d/d;->a(Lmiuix/animation/g/b;F)V

    :cond_0
    return-void
.end method

.method public a(Lmiuix/animation/g/b;F)V
    .locals 3

    invoke-virtual {p0}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0, p2}, Lmiuix/animation/g/b;->a(Ljava/lang/Object;F)V

    :cond_0
    return-void
.end method

.method public a(Lmiuix/animation/g/c;I)V
    .locals 3

    invoke-virtual {p0}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    invoke-interface {p1, v0, p2}, Lmiuix/animation/g/c;->a(Ljava/lang/Object;I)V

    :cond_0
    return-void
.end method

.method public a(J)Z
    .locals 2

    iget-wide v0, p0, Lmiuix/animation/e;->g:J

    invoke-static {v0, v1, p1, p2}, Lmiuix/animation/h/a;->a(JJ)Z

    move-result p1

    return p1
.end method

.method public varargs a([Lmiuix/animation/g/b;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    invoke-virtual {v0, p1}, Lmiuix/animation/d/d;->a([Lmiuix/animation/g/b;)Z

    move-result p1

    return p1
.end method

.method public b()F
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public b(J)V
    .locals 0

    iput-wide p1, p0, Lmiuix/animation/e;->g:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p1

    iput-wide p1, p0, Lmiuix/animation/e;->h:J

    return-void
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 4

    iget-object v0, p0, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    iget-wide v0, v0, Lmiuix/animation/d/s;->c:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public b(Lmiuix/animation/g/b;D)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/e;->j:Lmiuix/animation/d/u;

    invoke-virtual {v0, p0, p1, p2, p3}, Lmiuix/animation/d/u;->a(Lmiuix/animation/e;Lmiuix/animation/g/b;D)V

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lmiuix/animation/e;->i:I

    return v0
.end method

.method public d()Lmiuix/animation/e/a;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/e;->d:Lmiuix/animation/d/m;

    invoke-virtual {v0}, Lmiuix/animation/d/m;->a()Lmiuix/animation/e/a;

    move-result-object v0

    return-object v0
.end method

.method public abstract e()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public f()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "IAnimTarget was destroyed \uff01"

    invoke-static {v1, v0}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method

.method public g()Z
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lmiuix/animation/e;->h:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IAnimTarget{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
