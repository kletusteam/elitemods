.class public Lmiuix/animation/r;
.super Lmiuix/animation/e;


# static fields
.field static k:Lmiuix/animation/l;


# instance fields
.field private l:Lmiuix/animation/g/g;

.field private m:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmiuix/animation/q;

    invoke-direct {v0}, Lmiuix/animation/q;-><init>()V

    sput-object v0, Lmiuix/animation/r;->k:Lmiuix/animation/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiuix/animation/r;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 2

    invoke-direct {p0}, Lmiuix/animation/e;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v1, 0x3e8

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lmiuix/animation/r;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Lmiuix/animation/g/g;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lmiuix/animation/e;->c()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :cond_0
    invoke-direct {v0, p1}, Lmiuix/animation/g/g;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiuix/animation/r;->l:Lmiuix/animation/g/g;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Object;Lmiuix/animation/q;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/animation/r;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method private b(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lmiuix/animation/g/f;

    if-nez v0, :cond_1

    instance-of v0, p1, Lmiuix/animation/g/A;

    if-nez v0, :cond_1

    instance-of p1, p1, Lmiuix/animation/g/a;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public a(Ljava/lang/Object;)F
    .locals 1

    instance-of v0, p1, Lmiuix/animation/g/c;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lmiuix/animation/g/a;

    if-nez v0, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    return p1

    :cond_0
    invoke-super {p0, p1}, Lmiuix/animation/e;->a(Ljava/lang/Object;)F

    move-result p1

    return p1
.end method

.method public a(Lmiuix/animation/g/b;)F
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/animation/r;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/r;->l:Lmiuix/animation/g/g;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, p1, v1}, Lmiuix/animation/g/g;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    if-nez p1, :cond_0

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    :goto_0
    return p1

    :cond_1
    iget-object v0, p0, Lmiuix/animation/r;->l:Lmiuix/animation/g/g;

    invoke-virtual {v0}, Lmiuix/animation/g/g;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiuix/animation/g/b;->b(Ljava/lang/Object;)F

    move-result p1

    return p1
.end method

.method public a(Lmiuix/animation/g/c;)I
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/animation/r;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/r;->l:Lmiuix/animation/g/g;

    invoke-interface {p1}, Lmiuix/animation/g/c;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, p1, v1}, Lmiuix/animation/g/g;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-nez p1, :cond_0

    const p1, 0x7fffffff

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :goto_0
    return p1

    :cond_1
    iget-object v0, p0, Lmiuix/animation/r;->l:Lmiuix/animation/g/g;

    invoke-virtual {v0}, Lmiuix/animation/g/g;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lmiuix/animation/g/c;->a(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public a(Ljava/lang/String;)Lmiuix/animation/g/b;
    .locals 1

    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-virtual {p0, p1, v0}, Lmiuix/animation/r;->a(Ljava/lang/String;Ljava/lang/Class;)Lmiuix/animation/g/b;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/Class;)Lmiuix/animation/g/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "*>;)",
            "Lmiuix/animation/g/b;"
        }
    .end annotation

    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-eq p2, v0, :cond_1

    const-class v0, Ljava/lang/Integer;

    if-ne p2, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, Lmiuix/animation/g/f;

    invoke-direct {p2, p1}, Lmiuix/animation/g/f;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    :goto_0
    new-instance p2, Lmiuix/animation/g/e;

    invoke-direct {p2, p1}, Lmiuix/animation/g/e;-><init>(Ljava/lang/String;)V

    :goto_1
    return-object p2
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(Lmiuix/animation/g/b;F)V
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/animation/r;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/r;->l:Lmiuix/animation/g/g;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    invoke-virtual {v0, p1, v1, p2}, Lmiuix/animation/g/g;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/r;->l:Lmiuix/animation/g/g;

    invoke-virtual {v0}, Lmiuix/animation/g/g;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lmiuix/animation/g/b;->a(Ljava/lang/Object;F)V

    :goto_0
    return-void
.end method

.method public a(Lmiuix/animation/g/c;I)V
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/animation/r;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/r;->l:Lmiuix/animation/g/g;

    invoke-interface {p1}, Lmiuix/animation/g/c;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p1, v1, p2}, Lmiuix/animation/g/g;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/r;->l:Lmiuix/animation/g/g;

    invoke-virtual {v0}, Lmiuix/animation/g/g;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0, p2}, Lmiuix/animation/g/c;->a(Ljava/lang/Object;I)V

    :goto_0
    return-void
.end method

.method public b()F
    .locals 1

    const v0, 0x3b03126f    # 0.002f

    return v0
.end method

.method public e()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/r;->l:Lmiuix/animation/g/g;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lmiuix/animation/r;->l:Lmiuix/animation/g/g;

    invoke-virtual {v0}, Lmiuix/animation/g/g;->b()Z

    move-result v0

    return v0
.end method
