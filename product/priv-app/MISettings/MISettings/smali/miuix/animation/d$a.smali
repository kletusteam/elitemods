.class Lmiuix/animation/d$a;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/animation/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/animation/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private a:Lmiuix/animation/k;

.field private b:Lmiuix/animation/m;

.field private c:Lmiuix/animation/o;

.field private d:Lmiuix/animation/i;

.field private e:Lmiuix/animation/f;

.field private f:[Lmiuix/animation/e;


# direct methods
.method private varargs constructor <init>([Lmiuix/animation/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiuix/animation/d$a;->f:[Lmiuix/animation/e;

    const/4 p1, 0x0

    invoke-static {p1}, Lmiuix/animation/d;->a(Z)V

    invoke-static {}, Lmiuix/animation/d;->a()V

    return-void
.end method

.method synthetic constructor <init>([Lmiuix/animation/e;Lmiuix/animation/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/animation/d$a;-><init>([Lmiuix/animation/e;)V

    return-void
.end method


# virtual methods
.method public a()Lmiuix/animation/k;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/d$a;->a:Lmiuix/animation/k;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/d$a;->f:[Lmiuix/animation/e;

    invoke-static {v0}, Lmiuix/animation/b/z;->a([Lmiuix/animation/e;)Lmiuix/animation/b/w;

    move-result-object v0

    iput-object v0, p0, Lmiuix/animation/d$a;->a:Lmiuix/animation/k;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/d$a;->a:Lmiuix/animation/k;

    return-object v0
.end method

.method public b()Lmiuix/animation/o;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/d$a;->c:Lmiuix/animation/o;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/b/v;

    iget-object v1, p0, Lmiuix/animation/d$a;->f:[Lmiuix/animation/e;

    invoke-direct {v0, v1}, Lmiuix/animation/b/v;-><init>([Lmiuix/animation/e;)V

    iput-object v0, p0, Lmiuix/animation/d$a;->c:Lmiuix/animation/o;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/d$a;->c:Lmiuix/animation/o;

    return-object v0
.end method

.method public c()Lmiuix/animation/i;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/d$a;->d:Lmiuix/animation/i;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/b/m;

    iget-object v1, p0, Lmiuix/animation/d$a;->f:[Lmiuix/animation/e;

    invoke-direct {v0, v1}, Lmiuix/animation/b/m;-><init>([Lmiuix/animation/e;)V

    iput-object v0, p0, Lmiuix/animation/d$a;->d:Lmiuix/animation/i;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/d$a;->d:Lmiuix/animation/i;

    return-object v0
.end method

.method public d()Lmiuix/animation/m;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/d$a;->b:Lmiuix/animation/m;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/b/t;

    iget-object v1, p0, Lmiuix/animation/d$a;->f:[Lmiuix/animation/e;

    invoke-direct {v0, v1}, Lmiuix/animation/b/t;-><init>([Lmiuix/animation/e;)V

    new-instance v1, Lmiuix/animation/b/j;

    invoke-direct {v1}, Lmiuix/animation/b/j;-><init>()V

    invoke-virtual {v0, v1}, Lmiuix/animation/b/t;->a(Lmiuix/animation/b/j;)V

    iput-object v0, p0, Lmiuix/animation/d$a;->b:Lmiuix/animation/m;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/d$a;->b:Lmiuix/animation/m;

    return-object v0
.end method

.method public e()Lmiuix/animation/f;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/d$a;->e:Lmiuix/animation/f;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/b/i;

    iget-object v1, p0, Lmiuix/animation/d$a;->f:[Lmiuix/animation/e;

    invoke-direct {v0, v1}, Lmiuix/animation/b/i;-><init>([Lmiuix/animation/e;)V

    iput-object v0, p0, Lmiuix/animation/d$a;->e:Lmiuix/animation/f;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/d$a;->e:Lmiuix/animation/f;

    return-object v0
.end method

.method f()V
    .locals 1

    goto/32 :goto_e

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_1
    iget-object v0, p0, Lmiuix/animation/d$a;->c:Lmiuix/animation/o;

    goto/32 :goto_d

    nop

    :goto_2
    if-nez v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_b

    nop

    :goto_3
    return-void

    :goto_4
    iget-object v0, p0, Lmiuix/animation/d$a;->a:Lmiuix/animation/k;

    goto/32 :goto_2

    nop

    :goto_5
    invoke-interface {v0}, Lmiuix/animation/j;->a()V

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    iget-object v0, p0, Lmiuix/animation/d$a;->d:Lmiuix/animation/i;

    goto/32 :goto_8

    nop

    :goto_8
    if-nez v0, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_f

    nop

    :goto_9
    invoke-interface {v0}, Lmiuix/animation/j;->a()V

    :goto_a
    goto/32 :goto_4

    nop

    :goto_b
    invoke-interface {v0}, Lmiuix/animation/j;->a()V

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    if-nez v0, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_9

    nop

    :goto_e
    iget-object v0, p0, Lmiuix/animation/d$a;->b:Lmiuix/animation/m;

    goto/32 :goto_0

    nop

    :goto_f
    invoke-interface {v0}, Lmiuix/animation/j;->a()V

    :goto_10
    goto/32 :goto_3

    nop
.end method

.method g()V
    .locals 3

    goto/32 :goto_e

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/animation/d$a;->c:Lmiuix/animation/o;

    goto/32 :goto_10

    nop

    :goto_1
    new-array v2, v1, [Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_2
    invoke-interface {v0, v1}, Lmiuix/animation/g;->a([Ljava/lang/Object;)V

    :goto_3
    goto/32 :goto_11

    nop

    :goto_4
    invoke-interface {v0, v2}, Lmiuix/animation/g;->a([Ljava/lang/Object;)V

    :goto_5
    goto/32 :goto_8

    nop

    :goto_6
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_15

    nop

    :goto_8
    iget-object v0, p0, Lmiuix/animation/d$a;->a:Lmiuix/animation/k;

    goto/32 :goto_7

    nop

    :goto_9
    invoke-interface {v0, v2}, Lmiuix/animation/g;->a([Ljava/lang/Object;)V

    :goto_a
    goto/32 :goto_f

    nop

    :goto_b
    invoke-interface {v0, v2}, Lmiuix/animation/g;->a([Ljava/lang/Object;)V

    :goto_c
    goto/32 :goto_0

    nop

    :goto_d
    const/4 v1, 0x0

    goto/32 :goto_13

    nop

    :goto_e
    iget-object v0, p0, Lmiuix/animation/d$a;->b:Lmiuix/animation/m;

    goto/32 :goto_d

    nop

    :goto_f
    iget-object v0, p0, Lmiuix/animation/d$a;->d:Lmiuix/animation/i;

    goto/32 :goto_14

    nop

    :goto_10
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_12

    nop

    :goto_11
    return-void

    :goto_12
    new-array v2, v1, [Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_13
    if-nez v0, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_1

    nop

    :goto_14
    if-nez v0, :cond_3

    goto/32 :goto_3

    :cond_3
    goto/32 :goto_6

    nop

    :goto_15
    new-array v2, v1, [Ljava/lang/Object;

    goto/32 :goto_9

    nop
.end method
