.class Lmiuix/animation/e/a$g;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/animation/e/a$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/animation/e/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "g"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lmiuix/animation/e/b;Ljava/util/Collection;Lmiuix/animation/e/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lmiuix/animation/e/b;",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e/c;",
            ">;",
            "Lmiuix/animation/e/c;",
            ")V"
        }
    .end annotation

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_0
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lmiuix/animation/e/c;

    iget-boolean v0, p4, Lmiuix/animation/e/c;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p4, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-boolean v0, v0, Lmiuix/animation/d/c;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p4, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lmiuix/animation/d/c;->k:Z

    iget-byte v0, v0, Lmiuix/animation/d/c;->a:B

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-virtual {p2, p1, p4}, Lmiuix/animation/e/b;->onComplete(Ljava/lang/Object;Lmiuix/animation/e/c;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2, p1, p4}, Lmiuix/animation/e/b;->onCancel(Ljava/lang/Object;Lmiuix/animation/e/c;)V

    goto :goto_0

    :cond_2
    return-void
.end method
