.class public Lmiuix/animation/e/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/e/a$c;,
        Lmiuix/animation/e/a$b;,
        Lmiuix/animation/e/a$g;,
        Lmiuix/animation/e/a$h;,
        Lmiuix/animation/e/a$e;,
        Lmiuix/animation/e/a$f;,
        Lmiuix/animation/e/a$a;,
        Lmiuix/animation/e/a$d;
    }
.end annotation


# static fields
.field static final a:Lmiuix/animation/e/a$a;

.field static final b:Lmiuix/animation/e/a$f;

.field static final c:Lmiuix/animation/e/a$e;

.field static final d:Lmiuix/animation/e/a$h;

.field static final e:Lmiuix/animation/e/a$g;

.field static final f:Lmiuix/animation/e/a$b;

.field static final g:Lmiuix/animation/e/a$c;


# instance fields
.field final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lmiuix/animation/e/b;",
            ">;>;"
        }
    .end annotation
.end field

.field final i:Lmiuix/animation/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmiuix/animation/e/a$a;

    invoke-direct {v0}, Lmiuix/animation/e/a$a;-><init>()V

    sput-object v0, Lmiuix/animation/e/a;->a:Lmiuix/animation/e/a$a;

    new-instance v0, Lmiuix/animation/e/a$f;

    invoke-direct {v0}, Lmiuix/animation/e/a$f;-><init>()V

    sput-object v0, Lmiuix/animation/e/a;->b:Lmiuix/animation/e/a$f;

    new-instance v0, Lmiuix/animation/e/a$e;

    invoke-direct {v0}, Lmiuix/animation/e/a$e;-><init>()V

    sput-object v0, Lmiuix/animation/e/a;->c:Lmiuix/animation/e/a$e;

    new-instance v0, Lmiuix/animation/e/a$h;

    invoke-direct {v0}, Lmiuix/animation/e/a$h;-><init>()V

    sput-object v0, Lmiuix/animation/e/a;->d:Lmiuix/animation/e/a$h;

    new-instance v0, Lmiuix/animation/e/a$g;

    invoke-direct {v0}, Lmiuix/animation/e/a$g;-><init>()V

    sput-object v0, Lmiuix/animation/e/a;->e:Lmiuix/animation/e/a$g;

    new-instance v0, Lmiuix/animation/e/a$b;

    invoke-direct {v0}, Lmiuix/animation/e/a$b;-><init>()V

    sput-object v0, Lmiuix/animation/e/a;->f:Lmiuix/animation/e/a$b;

    new-instance v0, Lmiuix/animation/e/a$c;

    invoke-direct {v0}, Lmiuix/animation/e/a$c;-><init>()V

    sput-object v0, Lmiuix/animation/e/a;->g:Lmiuix/animation/e/a$c;

    return-void
.end method

.method public constructor <init>(Lmiuix/animation/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lmiuix/animation/e/a;->h:Ljava/util/Map;

    iput-object p1, p0, Lmiuix/animation/e/a;->i:Lmiuix/animation/e;

    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Lmiuix/animation/e/a$d;Ljava/util/Collection;Lmiuix/animation/e/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Lmiuix/animation/e/a$d;",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e/c;",
            ">;",
            "Lmiuix/animation/e/c;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lmiuix/animation/e/a;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2, p1, p3, p4, p5}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/util/List;Lmiuix/animation/e/a$d;Ljava/util/Collection;Lmiuix/animation/e/c;)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/util/List;Lmiuix/animation/e/a$d;Ljava/util/Collection;Lmiuix/animation/e/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lmiuix/animation/e/b;",
            ">;",
            "Lmiuix/animation/e/a$d;",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e/c;",
            ">;",
            "Lmiuix/animation/e/c;",
            ")V"
        }
    .end annotation

    const-class v0, Ljava/util/HashSet;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiuix/animation/h/j;->b(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/e/b;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2, p0, v1, p3, p4}, Lmiuix/animation/e/a$d;->a(Ljava/lang/Object;Lmiuix/animation/e/b;Ljava/util/Collection;Lmiuix/animation/e/c;)V

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lmiuix/animation/h/j;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private b(Ljava/lang/Object;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List<",
            "Lmiuix/animation/e/b;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lmiuix/animation/e/a;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    const-class v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiuix/animation/h/j;->b(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lmiuix/animation/e/a;->h:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/e/a;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lmiuix/animation/h/j;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lmiuix/animation/e/a;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/e/a;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-static {p1}, Lmiuix/animation/h/j;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    sget-object v3, Lmiuix/animation/e/a;->a:Lmiuix/animation/e/a$a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/lang/Object;Lmiuix/animation/e/a$d;Ljava/util/Collection;Lmiuix/animation/e/c;)V

    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e/c;",
            ">;)V"
        }
    .end annotation

    sget-object v3, Lmiuix/animation/e/a;->b:Lmiuix/animation/e/a$f;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/lang/Object;Lmiuix/animation/e/a$d;Ljava/util/Collection;Lmiuix/animation/e/c;)V

    return-void
.end method

.method public a(Ljava/lang/Object;Lmiuix/animation/a/a;)Z
    .locals 1

    iget-object v0, p2, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-direct {p0, p1}, Lmiuix/animation/e/a;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iget-object p2, p2, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    invoke-static {p2, p1}, Lmiuix/animation/h/a;->a(Ljava/util/Collection;Ljava/util/Collection;)V

    const/4 p1, 0x1

    return p1
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    sget-object v3, Lmiuix/animation/e/a;->f:Lmiuix/animation/e/a$b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/lang/Object;Lmiuix/animation/e/a$d;Ljava/util/Collection;Lmiuix/animation/e/c;)V

    return-void
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e/c;",
            ">;)V"
        }
    .end annotation

    sget-object v3, Lmiuix/animation/e/a;->e:Lmiuix/animation/e/a$g;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/lang/Object;Lmiuix/animation/e/a$d;Ljava/util/Collection;Lmiuix/animation/e/c;)V

    return-void
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    sget-object v3, Lmiuix/animation/e/a;->g:Lmiuix/animation/e/a$c;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/lang/Object;Lmiuix/animation/e/a$d;Ljava/util/Collection;Lmiuix/animation/e/c;)V

    return-void
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e/c;",
            ">;)V"
        }
    .end annotation

    sget-object v3, Lmiuix/animation/e/a;->d:Lmiuix/animation/e/a$h;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/lang/Object;Lmiuix/animation/e/a$d;Ljava/util/Collection;Lmiuix/animation/e/c;)V

    return-void
.end method

.method public d(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    sget-object v3, Lmiuix/animation/e/a;->c:Lmiuix/animation/e/a$e;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/lang/Object;Lmiuix/animation/e/a$d;Ljava/util/Collection;Lmiuix/animation/e/c;)V

    return-void
.end method
