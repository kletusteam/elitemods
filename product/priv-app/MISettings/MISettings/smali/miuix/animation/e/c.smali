.class public Lmiuix/animation/e/c;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lmiuix/animation/g/b;

.field public final b:Z

.field public volatile c:D

.field public volatile d:I

.field public volatile e:Z

.field public final f:Lmiuix/animation/d/c;


# direct methods
.method public constructor <init>(Lmiuix/animation/g/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmiuix/animation/d/c;

    invoke-direct {v0}, Lmiuix/animation/d/c;-><init>()V

    iput-object v0, p0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iput-object p1, p0, Lmiuix/animation/e/c;->a:Lmiuix/animation/g/b;

    instance-of p1, p1, Lmiuix/animation/g/c;

    iput-boolean p1, p0, Lmiuix/animation/e/c;->b:Z

    return-void
.end method

.method public static a(Ljava/util/Collection;Ljava/lang/String;)Lmiuix/animation/e/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e/c;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lmiuix/animation/e/c;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/e/c;

    iget-object v1, v0, Lmiuix/animation/e/c;->a:Lmiuix/animation/g/b;

    invoke-virtual {v1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static a(Ljava/util/Collection;Lmiuix/animation/g/b;)Lmiuix/animation/e/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e/c;",
            ">;",
            "Lmiuix/animation/g/b;",
            ")",
            "Lmiuix/animation/e/c;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/e/c;

    iget-object v1, v0, Lmiuix/animation/e/c;->a:Lmiuix/animation/g/b;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public a()F
    .locals 5

    iget-object v0, p0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v0, v0, Lmiuix/animation/d/c;->j:D

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v4, v0, v2

    if-eqz v4, :cond_0

    double-to-float v0, v0

    return v0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v0, v0, Lmiuix/animation/d/c;->i:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v0, v0, Lmiuix/animation/d/c;->i:D

    double-to-float v0, v0

    :goto_0
    return v0
.end method

.method public a(B)V
    .locals 2

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    const/4 v1, 0x2

    if-le p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v1, v0

    :goto_1
    iput-boolean v1, p0, Lmiuix/animation/e/c;->e:Z

    iget-boolean v1, p0, Lmiuix/animation/e/c;->e:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-byte v1, v1, Lmiuix/animation/d/c;->a:B

    invoke-static {v1}, Lmiuix/animation/d/k;->a(B)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iput-boolean v0, v1, Lmiuix/animation/d/c;->k:Z

    :cond_2
    iget-object v0, p0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iput-byte p1, v0, Lmiuix/animation/d/c;->a:B

    return-void
.end method

.method public a(Lmiuix/animation/e;)V
    .locals 2

    iget-boolean v0, p0, Lmiuix/animation/e/c;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/e/c;->a:Lmiuix/animation/g/b;

    check-cast v0, Lmiuix/animation/g/c;

    invoke-virtual {p0}, Lmiuix/animation/e/c;->b()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmiuix/animation/e;->a(Lmiuix/animation/g/c;I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/e/c;->a:Lmiuix/animation/g/b;

    invoke-virtual {p0}, Lmiuix/animation/e/c;->a()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;F)V

    :goto_0
    return-void
.end method

.method public b()I
    .locals 5

    iget-object v0, p0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v0, v0, Lmiuix/animation/d/c;->j:D

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v4, v0, v2

    if-eqz v4, :cond_0

    double-to-int v0, v0

    return v0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v0, v0, Lmiuix/animation/d/c;->i:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    const v0, 0x7fffffff

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v0, v0, Lmiuix/animation/d/c;->i:D

    double-to-int v0, v0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateInfo{, property="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/e/c;->a:Lmiuix/animation/g/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", velocity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lmiuix/animation/e/c;->c:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", value = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v1, v1, Lmiuix/animation/d/c;->i:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", useInt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lmiuix/animation/e/c;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", frameCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lmiuix/animation/e/c;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isCompleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lmiuix/animation/e/c;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
