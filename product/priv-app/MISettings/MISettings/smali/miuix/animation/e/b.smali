.class public Lmiuix/animation/e/b;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBegin(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public onBegin(Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e/c;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onBegin(Ljava/lang/Object;Lmiuix/animation/e/c;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public onCancel(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public onCancel(Ljava/lang/Object;Lmiuix/animation/e/c;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public onComplete(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public onComplete(Ljava/lang/Object;Lmiuix/animation/e/c;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public onUpdate(Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e/c;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onUpdate(Ljava/lang/Object;Lmiuix/animation/g/b;FFZ)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public onUpdate(Ljava/lang/Object;Lmiuix/animation/g/b;FZ)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public onUpdate(Ljava/lang/Object;Lmiuix/animation/g/c;IFZ)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method
