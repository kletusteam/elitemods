.class Lmiuix/animation/b/B;
.super Ljava/lang/Object;


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Lmiuix/animation/b/a;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/lang/Object;

.field final c:Lmiuix/animation/b/a;

.field final d:Lmiuix/animation/b/a;

.field final e:Lmiuix/animation/b/a;

.field f:Lmiuix/animation/b/A;


# direct methods
.method constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiuix/animation/b/B;->a:Ljava/util/Map;

    new-instance v0, Lmiuix/animation/b/a;

    const/4 v1, 0x1

    const-string v2, "defaultTo"

    invoke-direct {v0, v2, v1}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Lmiuix/animation/b/B;->c:Lmiuix/animation/b/a;

    new-instance v0, Lmiuix/animation/b/a;

    const-string v2, "defaultSetTo"

    invoke-direct {v0, v2, v1}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Lmiuix/animation/b/B;->d:Lmiuix/animation/b/a;

    new-instance v0, Lmiuix/animation/b/a;

    const-string v2, "autoSetTo"

    invoke-direct {v0, v2, v1}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Lmiuix/animation/b/B;->e:Lmiuix/animation/b/a;

    new-instance v0, Lmiuix/animation/b/A;

    invoke-direct {v0}, Lmiuix/animation/b/A;-><init>()V

    iput-object v0, p0, Lmiuix/animation/b/B;->f:Lmiuix/animation/b/A;

    return-void
.end method

.method private a(Ljava/lang/Object;Z)Lmiuix/animation/b/a;
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    instance-of v0, p1, Lmiuix/animation/b/a;

    if-eqz v0, :cond_1

    check-cast p1, Lmiuix/animation/b/a;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmiuix/animation/b/B;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/b/a;

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    new-instance p2, Lmiuix/animation/b/a;

    invoke-direct {p2, p1}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0, p2}, Lmiuix/animation/b/B;->a(Lmiuix/animation/b/a;)V

    move-object p1, p2

    goto :goto_0

    :cond_2
    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method private varargs a(Ljava/lang/Object;[Ljava/lang/Object;)Lmiuix/animation/b/a;
    .locals 2

    array-length v0, p2

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object v1, p2, v0

    invoke-direct {p0, v1, v0}, Lmiuix/animation/b/B;->a(Ljava/lang/Object;Z)Lmiuix/animation/b/a;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Lmiuix/animation/b/B;->a([Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    if-nez v0, :cond_2

    invoke-virtual {p0, p1}, Lmiuix/animation/b/B;->a(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method private varargs a([Ljava/lang/Object;)Lmiuix/animation/b/a;
    .locals 4

    const/4 v0, 0x0

    aget-object v0, p1, v0

    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-le v1, v3, :cond_0

    aget-object p1, p1, v3

    goto :goto_0

    :cond_0
    move-object p1, v2

    :goto_0
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    instance-of p1, p1, Ljava/lang/String;

    if-eqz p1, :cond_1

    invoke-direct {p0, v0, v3}, Lmiuix/animation/b/B;->a(Ljava/lang/Object;Z)Lmiuix/animation/b/a;

    move-result-object p1

    return-object p1

    :cond_1
    return-object v2
.end method

.method private varargs a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/a/b;[Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/B;->f:Lmiuix/animation/b/A;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmiuix/animation/b/A;->a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/a/b;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a()Lmiuix/animation/b/a;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/B;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/b/B;->c:Lmiuix/animation/b/a;

    iput-object v0, p0, Lmiuix/animation/b/B;->b:Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/b/B;->b:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lmiuix/animation/b/B;->a(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Lmiuix/animation/b/a;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmiuix/animation/b/B;->a(Ljava/lang/Object;Z)Lmiuix/animation/b/a;

    move-result-object p1

    return-object p1
.end method

.method public varargs a(Lmiuix/animation/e;Lmiuix/animation/a/b;[Ljava/lang/Object;)Lmiuix/animation/b/a;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/B;->d:Lmiuix/animation/b/a;

    invoke-direct {p0, v0, p3}, Lmiuix/animation/b/B;->a(Ljava/lang/Object;[Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lmiuix/animation/b/B;->a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/a/b;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;I)V
    .locals 3

    invoke-virtual {p0}, Lmiuix/animation/b/B;->a()Lmiuix/animation/b/a;

    move-result-object v0

    int-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-void
.end method

.method public a(Lmiuix/animation/b/a;)V
    .locals 2

    iget-object v0, p0, Lmiuix/animation/b/B;->a:Ljava/util/Map;

    invoke-virtual {p1}, Lmiuix/animation/b/a;->c()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/B;->c:Lmiuix/animation/b/a;

    if-eq p1, v0, :cond_0

    invoke-virtual {v0}, Lmiuix/animation/b/a;->b()Lmiuix/animation/a/a;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Z

    invoke-virtual {p2, p1, v0}, Lmiuix/animation/a/b;->a(Lmiuix/animation/a/a;[Z)V

    :cond_0
    return-void
.end method

.method public a(Lmiuix/animation/g/b;F)V
    .locals 3

    invoke-virtual {p0}, Lmiuix/animation/b/B;->a()Lmiuix/animation/b/a;

    move-result-object v0

    float-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-void
.end method

.method public varargs b(Lmiuix/animation/e;Lmiuix/animation/a/b;[Ljava/lang/Object;)Lmiuix/animation/b/a;
    .locals 1

    invoke-virtual {p0}, Lmiuix/animation/b/B;->a()Lmiuix/animation/b/a;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lmiuix/animation/b/B;->a(Ljava/lang/Object;[Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lmiuix/animation/b/B;->a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/a/b;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public b(Lmiuix/animation/b/a;)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/B;->c:Lmiuix/animation/b/a;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/b/B;->d:Lmiuix/animation/b/a;

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lmiuix/animation/b/a;->a()V

    :cond_1
    return-void
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/B;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public c(Ljava/lang/Object;)Lmiuix/animation/b/a;
    .locals 1

    instance-of v0, p1, Lmiuix/animation/b/a;

    if-eqz v0, :cond_0

    check-cast p1, Lmiuix/animation/b/a;

    move-object v0, p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/b/B;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/b/a;

    if-nez v0, :cond_1

    new-instance v0, Lmiuix/animation/b/a;

    invoke-direct {v0, p1}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lmiuix/animation/b/B;->a(Lmiuix/animation/b/a;)V

    :cond_1
    :goto_0
    iput-object v0, p0, Lmiuix/animation/b/B;->b:Ljava/lang/Object;

    return-object v0
.end method
