.class public Lmiuix/animation/b/v;
.super Lmiuix/animation/b/b;

# interfaces
.implements Lmiuix/animation/o;


# instance fields
.field private b:Z

.field private c:Z

.field private d:Z

.field private final e:Lmiuix/animation/a/a;


# direct methods
.method public varargs constructor <init>([Lmiuix/animation/e;)V
    .locals 4

    invoke-direct {p0, p1}, Lmiuix/animation/b/b;-><init>([Lmiuix/animation/e;)V

    new-instance p1, Lmiuix/animation/a/a;

    invoke-direct {p1}, Lmiuix/animation/a/a;-><init>()V

    const/4 v0, 0x1

    new-array v1, v0, [Lmiuix/animation/e/b;

    new-instance v2, Lmiuix/animation/b/u;

    invoke-direct {v2, p0}, Lmiuix/animation/b/u;-><init>(Lmiuix/animation/b/v;)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1, v1}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    iput-object p1, p0, Lmiuix/animation/b/v;->e:Lmiuix/animation/a/a;

    invoke-virtual {p0, v0}, Lmiuix/animation/b/v;->a(Z)Lmiuix/animation/o;

    return-void
.end method

.method private varargs a([Lmiuix/animation/o$a;)Lmiuix/animation/o$a;
    .locals 1

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object p1, p1, v0

    goto :goto_0

    :cond_0
    sget-object p1, Lmiuix/animation/o$a;->b:Lmiuix/animation/o$a;

    :goto_0
    return-object p1
.end method

.method static synthetic a(Lmiuix/animation/b/v;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/animation/b/v;->d:Z

    return p0
.end method

.method private varargs a(Lmiuix/animation/o$a;[Lmiuix/animation/a/a;)[Lmiuix/animation/a/a;
    .locals 6

    iget-boolean v0, p0, Lmiuix/animation/b/v;->c:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, -0x2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lmiuix/animation/b/v;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/b/v;->e:Lmiuix/animation/a/a;

    sget-object v5, Lmiuix/animation/o$a;->a:Lmiuix/animation/o$a;

    if-ne p1, v5, :cond_0

    const/16 p1, 0x10

    new-array v3, v2, [F

    const/high16 v4, 0x43960000    # 300.0f

    aput v4, v3, v1

    invoke-static {p1, v3}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-array p1, v3, [F

    fill-array-data p1, :array_0

    invoke-static {v4, p1}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    goto :goto_4

    :cond_1
    iget-boolean v0, p0, Lmiuix/animation/b/v;->c:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lmiuix/animation/b/v;->b:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lmiuix/animation/b/v;->e:Lmiuix/animation/a/a;

    sget-object v5, Lmiuix/animation/o$a;->a:Lmiuix/animation/o$a;

    if-ne p1, v5, :cond_2

    new-array p1, v3, [F

    fill-array-data p1, :array_1

    invoke-static {v4, p1}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p1

    goto :goto_1

    :cond_2
    new-array p1, v3, [F

    fill-array-data p1, :array_2

    invoke-static {v4, p1}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p1

    :goto_1
    invoke-virtual {v0, p1}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    goto :goto_4

    :cond_3
    iget-boolean v0, p0, Lmiuix/animation/b/v;->c:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lmiuix/animation/b/v;->e:Lmiuix/animation/a/a;

    sget-object v5, Lmiuix/animation/o$a;->a:Lmiuix/animation/o$a;

    if-ne p1, v5, :cond_4

    new-array p1, v3, [F

    fill-array-data p1, :array_3

    invoke-static {v4, p1}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p1

    goto :goto_2

    :cond_4
    new-array p1, v3, [F

    fill-array-data p1, :array_4

    invoke-static {v4, p1}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p1

    :goto_2
    invoke-virtual {v0, p1}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lmiuix/animation/b/v;->e:Lmiuix/animation/a/a;

    sget-object v5, Lmiuix/animation/o$a;->a:Lmiuix/animation/o$a;

    if-ne p1, v5, :cond_6

    new-array p1, v3, [F

    fill-array-data p1, :array_5

    invoke-static {v4, p1}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p1

    goto :goto_3

    :cond_6
    new-array p1, v3, [F

    fill-array-data p1, :array_6

    invoke-static {v4, p1}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p1

    :goto_3
    invoke-virtual {v0, p1}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    :goto_4
    new-array p1, v2, [Lmiuix/animation/a/a;

    iget-object v0, p0, Lmiuix/animation/b/v;->e:Lmiuix/animation/a/a;

    aput-object v0, p1, v1

    invoke-static {p2, p1}, Lmiuix/animation/h/a;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lmiuix/animation/a/a;

    return-object p1

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e19999a    # 0.15f
    .end array-data

    :array_1
    .array-data 4
        0x3f19999a    # 0.6f
        0x3eb33333    # 0.35f
    .end array-data

    :array_2
    .array-data 4
        0x3f400000    # 0.75f
        0x3e4ccccd    # 0.2f
    .end array-data

    :array_3
    .array-data 4
        0x3f400000    # 0.75f
        0x3eb33333    # 0.35f
    .end array-data

    :array_4
    .array-data 4
        0x3f400000    # 0.75f
        0x3e800000    # 0.25f
    .end array-data

    :array_5
    .array-data 4
        0x3f266666    # 0.65f
        0x3eb33333    # 0.35f
    .end array-data

    :array_6
    .array-data 4
        0x3f400000    # 0.75f
        0x3e800000    # 0.25f
    .end array-data
.end method


# virtual methods
.method public varargs a(F[Lmiuix/animation/o$a;)Lmiuix/animation/o;
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/b/v;->c:Z

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-direct {p0, p2}, Lmiuix/animation/b/v;->a([Lmiuix/animation/o$a;)Lmiuix/animation/o$a;

    move-result-object p2

    invoke-interface {v0, p2}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p2

    sget-object v0, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    float-to-double v1, p1

    invoke-virtual {p2, v0, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object p1, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    invoke-virtual {p2, p1, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-object p0
.end method

.method public a(J)Lmiuix/animation/o;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v0, p1, p2}, Lmiuix/animation/k;->a(J)Lmiuix/animation/k;

    return-object p0
.end method

.method public a(Z)Lmiuix/animation/o;
    .locals 7

    sget-object v0, Lmiuix/animation/g/A;->p:Lmiuix/animation/g/A;

    sget-object v1, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v6, Lmiuix/animation/o$a;->a:Lmiuix/animation/o$a;

    invoke-interface {p1, v6}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    invoke-virtual {p1, v1}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    invoke-virtual {p1, v0, v4, v5}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v4, Lmiuix/animation/o$a;->b:Lmiuix/animation/o$a;

    invoke-interface {p1, v4}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    invoke-virtual {p1, v1}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    invoke-virtual {p1, v0, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v6, Lmiuix/animation/o$a;->a:Lmiuix/animation/o$a;

    invoke-interface {p1, v6}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    invoke-virtual {p1, v0}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    invoke-virtual {p1, v1, v4, v5}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v4, Lmiuix/animation/o$a;->b:Lmiuix/animation/o$a;

    invoke-interface {p1, v4}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    invoke-virtual {p1, v0}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    invoke-virtual {p1, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    :goto_0
    return-object p0
.end method

.method public a()V
    .locals 1

    invoke-super {p0}, Lmiuix/animation/b/b;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/animation/b/v;->c:Z

    iput-boolean v0, p0, Lmiuix/animation/b/v;->b:Z

    return-void
.end method

.method public varargs b([Lmiuix/animation/a/a;)V
    .locals 2

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/o$a;->b:Lmiuix/animation/o$a;

    invoke-direct {p0, v1, p1}, Lmiuix/animation/b/v;->a(Lmiuix/animation/o$a;[Lmiuix/animation/a/a;)[Lmiuix/animation/a/a;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method public varargs e([Lmiuix/animation/a/a;)V
    .locals 2

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/o$a;->a:Lmiuix/animation/o$a;

    invoke-direct {p0, v1, p1}, Lmiuix/animation/b/v;->a(Lmiuix/animation/o$a;[Lmiuix/animation/a/a;)[Lmiuix/animation/a/a;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method
